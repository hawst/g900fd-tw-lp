.class public Lcom/samsung/android/sdk/pen/engine/SpenView;
.super Landroid/view/View;
.source "SpenView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;
    }
.end annotation


# static fields
.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field public static final REPLAY_STATE_PAUSED:I = 0x2

.field public static final REPLAY_STATE_PLAYING:I = 0x1

.field public static final REPLAY_STATE_STOPPED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SpenView"


# instance fields
.field private mIsScreenFramebuffer:Z

.field private mMetricsRect:Landroid/graphics/Rect;

.field private mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

.field private mThreadId:J

.field private mUpdateRect:Landroid/graphics/RectF;

.field private mViewUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 169
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 51
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 52
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mThreadId:J

    .line 53
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mMetricsRect:Landroid/graphics/Rect;

    .line 2900
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdateRect:Landroid/graphics/RectF;

    .line 2901
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mIsScreenFramebuffer:Z

    .line 170
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenView;->construct(Landroid/content/Context;)V

    .line 171
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 197
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 52
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mThreadId:J

    .line 53
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mMetricsRect:Landroid/graphics/Rect;

    .line 2900
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdateRect:Landroid/graphics/RectF;

    .line 2901
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mIsScreenFramebuffer:Z

    .line 198
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenView;->construct(Landroid/content/Context;)V

    .line 199
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 234
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 52
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mThreadId:J

    .line 53
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mMetricsRect:Landroid/graphics/Rect;

    .line 2900
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdateRect:Landroid/graphics/RectF;

    .line 2901
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mIsScreenFramebuffer:Z

    .line 235
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenView;->construct(Landroid/content/Context;)V

    .line 236
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenView;Z)V
    .locals 0

    .prologue
    .line 2901
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mIsScreenFramebuffer:Z

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenView;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mThreadId:J

    return-wide v0
.end method

.method private construct(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 239
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;

    invoke-direct {v2, p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenView;Lcom/samsung/android/sdk/pen/engine/SpenView;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mViewUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;

    .line 240
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mViewUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;

    invoke-direct {v2, p1, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;Z)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 241
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v2, :cond_0

    .line 242
    const/16 v2, 0x9

    const-string v3, "failed to create SpenInView"

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 271
    :goto_0
    return-void

    .line 245
    :cond_0
    if-nez p1, :cond_1

    .line 246
    const/16 v2, 0x8

    const-string v3, " : context must not be null"

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v2, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setView(Landroid/view/View;)V

    .line 251
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 252
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    const/4 v1, 0x0

    .line 254
    .local v1, "width":I
    if-eqz v0, :cond_2

    .line 255
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v2, v3, :cond_3

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 259
    :cond_2
    :goto_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v4, v4, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mMetricsRect:Landroid/graphics/Rect;

    .line 261
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mThreadId:J

    .line 263
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenView$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenView$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenView;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    .line 270
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdateRect:Landroid/graphics/RectF;

    goto :goto_0

    .line 256
    :cond_3
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_1
.end method

.method private getScreenPointOfView(Landroid/view/View;)Landroid/graphics/Point;
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2847
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2848
    .local v1, "screenPointOfView":Landroid/graphics/Point;
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 2849
    .local v0, "screenOffsetOfView":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2850
    const/4 v2, 0x0

    aget v2, v0, v2

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 2851
    const/4 v2, 0x1

    aget v2, v0, v2

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 2852
    return-object v1
.end method


# virtual methods
.method public cancelStroke()V
    .locals 1

    .prologue
    .line 2284
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2288
    :goto_0
    return-void

    .line 2287
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->cancelStroke()V

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "includeBlank"    # Z

    .prologue
    .line 587
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 588
    const/4 v0, 0x0

    .line 590
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 620
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 621
    const/4 v0, 0x0

    .line 623
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->capturePage(F)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public capturePage(FZ)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "ratio"    # F
    .param p2, "isTransparent"    # Z

    .prologue
    .line 649
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 650
    const/4 v0, 0x0

    .line 652
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->capturePage(FZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public clearHighlight()V
    .locals 1

    .prologue
    .line 1798
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1802
    :goto_0
    return-void

    .line 1801
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->clearHighlight()V

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 282
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 293
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->close()V

    .line 286
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    .line 287
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mViewUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;

    if-eqz v0, :cond_1

    .line 288
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mViewUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->close()V

    .line 289
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mViewUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;

    .line 291
    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mMetricsRect:Landroid/graphics/Rect;

    .line 292
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdateRect:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method public closeControl()V
    .locals 1

    .prologue
    .line 1582
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1586
    :goto_0
    return-void

    .line 1585
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    goto :goto_0
.end method

.method public drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 2272
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2273
    const/4 v0, 0x0

    .line 2275
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public extractSmartClipData(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I
    .locals 13
    .param p1, "resultElement"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;
    .param p2, "croppedArea"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;

    .prologue
    .line 2861
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v9, :cond_0

    .line 2862
    const/4 v9, 0x0

    .line 2897
    :goto_0
    return v9

    .line 2865
    :cond_0
    invoke-interface {p2}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 2868
    .local v1, "cropRect":Landroid/graphics/Rect;
    invoke-direct {p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->getScreenPointOfView(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v5

    .line 2869
    .local v5, "leftTopOfView":Landroid/graphics/Point;
    iget v9, v5, Landroid/graphics/Point;->x:I

    neg-int v9, v9

    iget v10, v5, Landroid/graphics/Point;->y:I

    neg-int v10, v10

    invoke-virtual {v1, v9, v10}, Landroid/graphics/Rect;->offset(II)V

    .line 2872
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v10, 0x1

    .line 2873
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    const/4 v12, 0x1

    .line 2872
    invoke-virtual {v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->findObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;

    move-result-object v7

    .line 2875
    .local v7, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    new-instance v4, Lcom/samsung/android/hermes/object/HermesStrokes;

    invoke-direct {v4}, Lcom/samsung/android/hermes/object/HermesStrokes;-><init>()V

    .line 2876
    .local v4, "hermesStrokes":Lcom/samsung/android/hermes/object/HermesStrokes;
    if-eqz v7, :cond_2

    .line 2877
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 2887
    :cond_2
    invoke-virtual {v4}, Lcom/samsung/android/hermes/object/HermesStrokes;->size()I

    move-result v9

    if-lez v9, :cond_3

    .line 2889
    new-instance v8, Lcom/samsung/android/smartclip/SmartClipMetaTagImpl;

    const-string/jumbo v9, "stroke"

    const-string v10, ""

    invoke-direct {v8, v9, v10, v4}, Lcom/samsung/android/smartclip/SmartClipMetaTagImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2890
    .local v8, "tag":Lcom/samsung/android/smartclip/SmartClipMetaTagImpl;
    invoke-interface {p1, v8}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->addTag(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;)V

    .line 2897
    .end local v8    # "tag":Lcom/samsung/android/smartclip/SmartClipMetaTagImpl;
    :cond_3
    const/4 v9, 0x1

    goto :goto_0

    .line 2877
    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 2878
    .local v6, "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object v0, v6

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-object v9, v0

    const-string/jumbo v11, "strokeType"

    invoke-virtual {v9, v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getExtraDataInt(Ljava/lang/String;)I

    move-result v9

    const/4 v11, 0x1

    if-eq v9, v11, :cond_1

    .line 2880
    new-instance v3, Lcom/samsung/android/hermes/object/HermesStroke;

    invoke-direct {v3}, Lcom/samsung/android/hermes/object/HermesStroke;-><init>()V

    .line 2881
    .local v3, "hermesStroke":Lcom/samsung/android/hermes/object/HermesStroke;
    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local v6    # "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/samsung/android/hermes/object/HermesStroke;->setPoints([Landroid/graphics/PointF;)V

    .line 2882
    invoke-virtual {v4, v3}, Lcom/samsung/android/hermes/object/HermesStrokes;->addStroke(Lcom/samsung/android/hermes/object/HermesStroke;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2893
    .end local v3    # "hermesStroke":Lcom/samsung/android/hermes/object/HermesStroke;
    .end local v4    # "hermesStrokes":Lcom/samsung/android/hermes/object/HermesStrokes;
    .end local v7    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :catch_0
    move-exception v2

    .line 2894
    .local v2, "e":Ljava/lang/NoClassDefFoundError;
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public getBlankColor()I
    .locals 1

    .prologue
    .line 990
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 991
    const/4 v0, 0x0

    .line 993
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getBlankColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 1

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1024
    const/4 v0, 0x0

    .line 1026
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 1

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1010
    const/4 v0, 0x0

    .line 1012
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 1

    .prologue
    .line 1567
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1568
    const/4 v0, 0x0

    .line 1570
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 1

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1168
    const/4 v0, 0x0

    .line 1170
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getFrameStartPosition()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 837
    const/4 v0, 0x0

    .line 839
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 876
    const/4 v0, 0x0

    .line 878
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getMaxZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 1

    .prologue
    .line 914
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 915
    const/4 v0, 0x0

    .line 917
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getMinZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 952
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 953
    const/4 v0, 0x0

    .line 955
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 1

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1124
    const/4 v0, 0x0

    .line 1126
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 1211
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1212
    const/4 v0, 0x0

    .line 1214
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getReplayState()I
    .locals 1

    .prologue
    .line 1658
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1659
    const/4 v0, 0x0

    .line 1661
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    goto :goto_0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    .prologue
    .line 1255
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1256
    const/4 v0, 0x0

    .line 1258
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getTemporaryStroke()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1846
    const/4 v0, 0x0

    .line 1848
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getTemporaryStroke()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 1079
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1080
    const/4 v0, 0x0

    .line 1082
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getToolTypeAction(I)I
    .locals 1
    .param p1, "toolType"    # I

    .prologue
    .line 731
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 732
    const/4 v0, 0x0

    .line 734
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadBoxHeight()F
    .locals 1

    .prologue
    .line 2111
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2112
    const/4 v0, 0x0

    .line 2114
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadBoxHeight()F

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadBoxPosition()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2140
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2141
    const/4 v0, 0x0

    .line 2143
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadBoxPosition()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomPadBoxRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 2042
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2043
    const/4 v0, 0x0

    .line 2045
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadBoxRect()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomPadPosition()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2168
    const/4 v0, 0x0

    .line 2170
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadPosition()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomPadRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 2052
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2053
    const/4 v0, 0x0

    .line 2055
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadRect()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 819
    const/4 v0, 0x0

    .line 821
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public isDottedLineEnabled()Z
    .locals 1

    .prologue
    .line 1764
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1765
    const/4 v0, 0x0

    .line 1767
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isDottedLineEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isHorizontalScrollBarEnabled()Z
    .locals 1

    .prologue
    .line 1917
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->isScrollBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1918
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isHorizontalScrollBarEnabled()Z

    move-result v0

    .line 1920
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/View;->isHorizontalScrollBarEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isHorizontalSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 1476
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1477
    const/4 v0, 0x0

    .line 1479
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isHorizontalSmartScrollEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isHyperTextViewEnabled()Z
    .locals 1

    .prologue
    .line 2001
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2002
    const/4 v0, 0x0

    .line 2004
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isHyperTextViewEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isLongPressEnabled()Z
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 548
    const/4 v0, 0x0

    .line 550
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isLongPressEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isPenButtonSelectionEnabled()Z
    .locals 1

    .prologue
    .line 2314
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2315
    const/4 v0, 0x0

    .line 2317
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isPenButtonSelectionEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isScrollBarEnabled()Z
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1883
    const/4 v0, 0x0

    .line 1885
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isScrollBarEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isSmartScaleEnabled()Z
    .locals 1

    .prologue
    .line 1423
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1424
    const/4 v0, 0x0

    .line 1426
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSmartScaleEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isToolTipEnabled()Z
    .locals 1

    .prologue
    .line 2029
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2030
    const/4 v0, 0x0

    .line 2032
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isToolTipEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isVerticalScrollBarEnabled()Z
    .locals 1

    .prologue
    .line 1953
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->isScrollBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1954
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isVerticalScrollBarEnabled()Z

    move-result v0

    .line 1956
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/View;->isVerticalScrollBarEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isVerticalSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 1528
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1529
    const/4 v0, 0x0

    .line 1531
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isVerticalSmartScrollEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomPadDrawing()Z
    .locals 1

    .prologue
    .line 2081
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2082
    const/4 v0, 0x0

    .line 2084
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomPadStroking()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomPadEnabled()Z
    .locals 1

    .prologue
    .line 2201
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2202
    const/4 v0, 0x0

    .line 2204
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomPadEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomPadStroking()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2068
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2069
    const/4 v0, 0x0

    .line 2071
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomPadStroking()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomable()Z
    .locals 1

    .prologue
    .line 770
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 771
    const/4 v0, 0x0

    .line 773
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomable()Z

    move-result v0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 363
    :goto_0
    return-void

    .line 361
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setParent(Landroid/view/ViewGroup;)V

    .line 362
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 377
    :goto_0
    return-void

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setParent(Landroid/view/ViewGroup;)V

    .line 376
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2910
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2911
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2920
    :goto_0
    return-void

    .line 2915
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEditableTextBox()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2916
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mIsScreenFramebuffer:Z

    .line 2918
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdateRect:Landroid/graphics/RectF;

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mIsScreenFramebuffer:Z

    invoke-virtual {v0, p1, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->UpdateCanvas(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 536
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 537
    const/4 v0, 0x0

    .line 539
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation",
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 329
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 330
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_1

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 335
    .local v6, "parentLayoutRect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 337
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 338
    .local v7, "windowVisibleRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 339
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mMetricsRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mMetricsRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 340
    :cond_2
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    iget v1, v6, Landroid/graphics/Rect;->bottom:I

    if-ge v0, v1, :cond_3

    .line 341
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    iget v1, v6, Landroid/graphics/Rect;->top:I

    if-gt v0, v1, :cond_4

    .line 342
    :cond_3
    const/4 v7, 0x0

    .line 345
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onLayout(ZIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 346
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdateRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdateRect:Landroid/graphics/RectF;

    int-to-float v1, p2

    int-to-float v2, p3

    int-to-float v3, p4

    int-to-float v4, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 318
    const-string v0, "SpenView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSizeChanged("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 512
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 513
    const/4 v0, 0x0

    .line 515
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 304
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 309
    :goto_0
    return-void

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 308
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 395
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 399
    :goto_0
    return-void

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onWindowFocusChanged(Z)V

    goto :goto_0
.end method

.method public pauseReplay()V
    .locals 1

    .prologue
    .line 1625
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1629
    :goto_0
    return-void

    .line 1628
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->pauseReplay()V

    goto :goto_0
.end method

.method public requestPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 2
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1370
    const/16 v0, 0xd

    const-string v1, " : requestPageDoc not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 1371
    const/4 v0, 0x0

    return v0
.end method

.method public resumeReplay()V
    .locals 1

    .prologue
    .line 1639
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1643
    :goto_0
    return-void

    .line 1642
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resumeReplay()V

    goto :goto_0
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2335
    const/16 v0, 0xd

    const-string v1, " : setBackground not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2336
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2371
    const/16 v0, 0xd

    const-string v1, " : setBackgroundColor not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2372
    return-void
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1039
    :goto_0
    return-void

    .line 1038
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V

    goto :goto_0
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2353
    const/16 v0, 0xd

    const-string v1, " : setBackgroundDrawable not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2354
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 2
    .param p1, "resid"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2389
    const/16 v0, 0xd

    const-string v1, " : setBackgroundResource not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2390
    return-void
.end method

.method public setBlankColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 972
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 976
    :goto_0
    return-void

    .line 975
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setBlankColor(I)V

    goto :goto_0
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .prologue
    .line 2506
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2510
    :goto_0
    return-void

    .line 2509
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V

    goto :goto_0
.end method

.method public setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 1
    .param p1, "control"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1552
    :goto_0
    return-void

    .line 1551
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    goto :goto_0
.end method

.method public setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .prologue
    .line 2686
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2690
    :goto_0
    return-void

    .line 2689
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V

    goto :goto_0
.end method

.method public setDottedLineEnabled(ZIII[FF)V
    .locals 7
    .param p1, "enable"    # Z
    .param p2, "intervalHeight"    # I
    .param p3, "color"    # I
    .param p4, "thickness"    # I
    .param p5, "pathIntervals"    # [F
    .param p6, "phase"    # F

    .prologue
    .line 1745
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1749
    :goto_0
    return-void

    .line 1748
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setDottedLineEnabled(ZIII[FF)V

    goto :goto_0
.end method

.method public setDoubleTapZoomable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2840
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2844
    :goto_0
    return-void

    .line 2843
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setDoubleTapZoomable(Z)V

    goto :goto_0
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .prologue
    .line 2606
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2610
    :goto_0
    return-void

    .line 2609
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V

    goto :goto_0
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1150
    :goto_0
    return-void

    .line 1149
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_0
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .prologue
    .line 2706
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2710
    :goto_0
    return-void

    .line 2709
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V

    goto :goto_0
.end method

.method public setForceStretchView(ZII)Z
    .locals 1
    .param p1, "enable"    # Z
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 2824
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-eqz v0, :cond_0

    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 2825
    :cond_0
    const/4 v0, 0x0

    .line 2827
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setForceStretchView(ZII)Z

    move-result v0

    goto :goto_0
.end method

.method public setHighlight(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1784
    .local p1, "highlightInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1788
    :goto_0
    return-void

    .line 1787
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHighlight(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public setHorizontalScrollBarEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1900
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-eqz v0, :cond_0

    .line 1901
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHorizontalScrollBarEnabled(Z)V

    .line 1903
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->setHorizontalScrollBarEnabled(Z)V

    .line 1904
    return-void
.end method

.method public setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "leftScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "rightScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 1456
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1461
    :goto_0
    return-void

    .line 1459
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 1460
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .prologue
    .line 2446
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2450
    :goto_0
    return-void

    .line 2449
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V

    goto :goto_0
.end method

.method public setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    .prologue
    .line 2760
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2764
    :goto_0
    return-void

    .line 2763
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V

    goto :goto_0
.end method

.method public setHyperTextViewEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1987
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1991
    :goto_0
    return-void

    .line 1990
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHyperTextViewEnabled(Z)V

    goto :goto_0
.end method

.method public setLongPressEnabled(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 558
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 559
    const/4 v0, 0x0

    .line 561
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setLongPressEnabled(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    .prologue
    .line 2466
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2470
    :goto_0
    return-void

    .line 2469
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 857
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 858
    const/4 v0, 0x0

    .line 860
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setMaxZoomRatio(F)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 896
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 897
    const/4 v0, 0x0

    .line 899
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setMinZoomRatio(F)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 1
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "direction"    # I
    .param p3, "type"    # I
    .param p4, "centerY"    # F

    .prologue
    .line 1334
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1335
    const/4 v0, 0x0

    .line 1337
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 1
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1288
    const/4 v0, 0x0

    .line 1290
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .prologue
    .line 2546
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2550
    :goto_0
    return-void

    .line 2549
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V

    goto :goto_0
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 1
    .param p1, "position"    # Landroid/graphics/PointF;

    .prologue
    .line 934
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 938
    :goto_0
    return-void

    .line 937
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPan(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setPenButtonSelectionEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2298
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2302
    :goto_0
    return-void

    .line 2301
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenButtonSelectionEnabled(Z)V

    goto :goto_0
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .prologue
    .line 2586
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2590
    :goto_0
    return-void

    .line 2589
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V

    goto :goto_0
.end method

.method public setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .prologue
    .line 2566
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2570
    :goto_0
    return-void

    .line 2569
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 1102
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1106
    :goto_0
    return-void

    .line 1105
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .prologue
    .line 2746
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2750
    :goto_0
    return-void

    .line 2749
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    goto :goto_0
.end method

.method public setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .prologue
    .line 2726
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2730
    :goto_0
    return-void

    .line 2729
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    goto :goto_0
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 2406
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2410
    :goto_0
    return-void

    .line 2409
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V

    goto :goto_0
.end method

.method public setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .prologue
    .line 2626
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2630
    :goto_0
    return-void

    .line 2629
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1194
    :goto_0
    return-void

    .line 1193
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0
.end method

.method public setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    .prologue
    .line 2486
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2490
    :goto_0
    return-void

    .line 2489
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V

    goto :goto_0
.end method

.method public setReplayPosition(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1704
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1708
    :goto_0
    return-void

    .line 1707
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplayPosition(I)V

    goto :goto_0
.end method

.method public setReplaySpeed(I)V
    .locals 1
    .param p1, "speed"    # I

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1684
    :goto_0
    return-void

    .line 1683
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplaySpeed(I)V

    goto :goto_0
.end method

.method public setRequestPageDocListener(Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2802
    const/16 v0, 0xd

    const-string v1, " : setRequestPageDocListener not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2808
    return-void
.end method

.method public setScrollBarEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1865
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1869
    :goto_0
    return-void

    .line 1868
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method public setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    .prologue
    .line 2666
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2670
    :goto_0
    return-void

    .line 2669
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V

    goto :goto_0
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    .line 1234
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1238
    :goto_0
    return-void

    .line 1237
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "region"    # Landroid/graphics/Rect;
    .param p3, "effectFrame"    # I
    .param p4, "zoomOutResponseTime"    # I
    .param p5, "zoomRatio"    # F

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1408
    :goto_0
    return-void

    .line 1407
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V

    goto :goto_0
.end method

.method public setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    .prologue
    .line 2646
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2650
    :goto_0
    return-void

    .line 2649
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V

    goto :goto_0
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 1058
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1062
    :goto_0
    return-void

    .line 1061
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method

.method public setToolTipEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2019
    :goto_0
    return-void

    .line 2018
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setToolTipEnabled(Z)V

    goto :goto_0
.end method

.method public setToolTypeAction(II)V
    .locals 1
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    .line 691
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 695
    :goto_0
    return-void

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setToolTypeAction(II)V

    goto :goto_0
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 2426
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2430
    :goto_0
    return-void

    .line 2429
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V

    goto :goto_0
.end method

.method public setVerticalScrollBarEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1936
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-eqz v0, :cond_0

    .line 1937
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setVerticalScrollBarEnabled(Z)V

    .line 1939
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    .line 1940
    return-void
.end method

.method public setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "topScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "bottomScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 1509
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1513
    :goto_0
    return-void

    .line 1512
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setZoom(FFF)V
    .locals 1
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 800
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 804
    :goto_0
    return-void

    .line 803
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .prologue
    .line 2526
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2530
    :goto_0
    return-void

    .line 2529
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V

    goto :goto_0
.end method

.method public setZoomPadBoxHeight(F)V
    .locals 1
    .param p1, "height"    # F

    .prologue
    .line 2096
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2100
    :goto_0
    return-void

    .line 2099
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadBoxHeight(F)V

    goto :goto_0
.end method

.method public setZoomPadBoxHeightEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2217
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2221
    :goto_0
    return-void

    .line 2220
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadBoxHeightEnabled(Z)V

    goto :goto_0
.end method

.method public setZoomPadBoxPosition(Landroid/graphics/PointF;)V
    .locals 1
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 2126
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2130
    :goto_0
    return-void

    .line 2129
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadBoxPosition(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setZoomPadButtonEnabled(ZZZZZ)V
    .locals 6
    .param p1, "left"    # Z
    .param p2, "right"    # Z
    .param p3, "enter"    # Z
    .param p4, "up"    # Z
    .param p5, "down"    # Z

    .prologue
    .line 2245
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2249
    :goto_0
    return-void

    .line 2248
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadButtonEnabled(ZZZZZ)V

    goto :goto_0
.end method

.method public setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    .prologue
    .line 2774
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2778
    :goto_0
    return-void

    .line 2777
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V

    goto :goto_0
.end method

.method public setZoomPadPosition(Landroid/graphics/PointF;)V
    .locals 1
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 2154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2158
    :goto_0
    return-void

    .line 2157
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadPosition(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setZoomable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 751
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 755
    :goto_0
    return-void

    .line 754
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomable(Z)V

    goto :goto_0
.end method

.method public startReplay()V
    .locals 1

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1601
    :goto_0
    return-void

    .line 1600
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startReplay()V

    goto :goto_0
.end method

.method public startTemporaryStroke()V
    .locals 1

    .prologue
    .line 1812
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1816
    :goto_0
    return-void

    .line 1815
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startTemporaryStroke()V

    goto :goto_0
.end method

.method public startZoomPad()V
    .locals 1

    .prologue
    .line 2178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2182
    :goto_0
    return-void

    .line 2181
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startZoomPad()V

    goto :goto_0
.end method

.method public stopReplay()V
    .locals 1

    .prologue
    .line 1611
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1615
    :goto_0
    return-void

    .line 1614
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopReplay()V

    goto :goto_0
.end method

.method public stopTemporaryStroke()V
    .locals 1

    .prologue
    .line 1826
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 1830
    :goto_0
    return-void

    .line 1829
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopTemporaryStroke()V

    goto :goto_0
.end method

.method public stopZoomPad()V
    .locals 1

    .prologue
    .line 2189
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 2193
    :goto_0
    return-void

    .line 2192
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopZoomPad()V

    goto :goto_0
.end method

.method public update()V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 420
    :goto_0
    return-void

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 1
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 488
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 492
    :goto_0
    return-void

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    goto :goto_0
.end method

.method public updateScreen()V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 448
    :goto_0
    return-void

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreen()V

    goto :goto_0
.end method

.method public updateScreenFrameBuffer()V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 434
    :goto_0
    return-void

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 1
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 466
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    .line 470
    :goto_0
    return-void

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    goto :goto_0
.end method
