.class Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
.super Ljava/lang/Object;
.source "SPenImageUtil.java"


# instance fields
.field protected mContextResources:Landroid/content/res/Resources;

.field protected mCustom_imagepath:Ljava/lang/String;

.field protected mDrawableContext:Landroid/content/Context;

.field protected mOnePT:F

.field protected mSdkResources:Landroid/content/res/Resources;

.field protected mSdkVersion:I

.field protected resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 5
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    const/4 v4, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    .line 33
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 34
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    .line 35
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    .line 36
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkVersion:I

    .line 49
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 54
    .local v1, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, p3

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    .line 55
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->getInstance()Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    .line 57
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 58
    .local v2, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    .line 59
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    .end local v2    # "manager":Landroid/content/pm/PackageManager;
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected getColorStateList(I)Landroid/content/res/ColorStateList;
    .locals 8
    .param p1, "color"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 751
    new-array v0, v7, [[I

    .line 752
    .local v0, "arrayOfStates":[[I
    new-array v1, v6, [I

    .line 753
    .local v1, "arrayOfStates1":[I
    const v4, -0x10100a7

    aput v4, v1, v5

    .line 754
    aput-object v1, v0, v5

    .line 755
    new-array v2, v6, [I

    .line 756
    .local v2, "arrayOfStates2":[I
    const v4, 0x10100a7

    aput v4, v2, v5

    .line 757
    aput-object v2, v0, v6

    .line 758
    new-array v3, v7, [I

    .line 759
    .local v3, "textColor":[I
    aput p1, v3, v5

    .line 760
    aput p1, v3, v6

    .line 762
    new-instance v4, Landroid/content/res/ColorStateList;

    invoke-direct {v4, v0, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v4
.end method

.method protected getIntValueAppliedDensity(F)I
    .locals 1
    .param p1, "paramFloat"    # F

    .prologue
    .line 526
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;
    .locals 20
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "iconWidth"    # I
    .param p4, "iconHeight"    # I
    .param p5, "forceResize"    # Z

    .prologue
    .line 163
    invoke-virtual/range {p1 .. p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v14

    .line 164
    .local v14, "stream":Ljava/io/InputStream;
    invoke-static {v14}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 167
    .local v2, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 168
    const/4 v3, 0x0

    .line 199
    :goto_0
    return-object v3

    .line 170
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 171
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 172
    .local v6, "height":I
    move/from16 v0, p3

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    .line 173
    .local v10, "newWidth":I
    move/from16 v0, p4

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 176
    .local v9, "newHeight":I
    int-to-float v3, v10

    int-to-float v4, v5

    div-float v13, v3, v4

    .line 177
    .local v13, "scaleWidth":F
    int-to-float v3, v9

    int-to-float v4, v6

    div-float v12, v3, v4

    .line 179
    .local v12, "scaleHeight":F
    float-to-double v0, v13

    move-wide/from16 v16, v0

    const-wide v18, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v3, v16, v18

    if-lez v3, :cond_1

    float-to-double v0, v12

    move-wide/from16 v16, v0

    const-wide v18, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v3, v16, v18

    if-lez v3, :cond_1

    float-to-double v0, v13

    move-wide/from16 v16, v0

    const-wide v18, 0x3ff199999999999aL    # 1.1

    cmpg-double v3, v16, v18

    if-gez v3, :cond_1

    float-to-double v0, v12

    move-wide/from16 v16, v0

    const-wide v18, 0x3ff199999999999aL    # 1.1

    cmpg-double v3, v16, v18

    if-gez v3, :cond_1

    if-nez p5, :cond_1

    .line 180
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 184
    :cond_1
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 186
    .local v7, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v7, v13, v12}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 192
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 196
    .local v11, "resizedBitmap":Landroid/graphics/Bitmap;
    if-eqz p5, :cond_2

    .line 197
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    invoke-direct {v3, v4, v11}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 199
    :cond_2
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v11}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 327
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 329
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 330
    new-array v1, v4, [I

    const v2, -0x10100a0

    aput v2, v1, v3

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 332
    :cond_0
    if-eqz p2, :cond_1

    .line 333
    new-array v1, v4, [I

    const v2, 0x10100a0

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 335
    :cond_1
    return-object v0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 340
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 342
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 343
    new-array v1, v4, [I

    const v2, -0x10100a0

    aput v2, v1, v3

    .line 344
    invoke-virtual {p0, p1, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 343
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 346
    :cond_0
    if-eqz p2, :cond_1

    .line 347
    new-array v1, v4, [I

    const v2, 0x10100a0

    aput v2, v1, v3

    .line 348
    invoke-virtual {p0, p2, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 347
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 350
    :cond_1
    return-object v0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;
    .param p3, "focusCheckImg"    # Ljava/lang/String;
    .param p4, "focusUncheckImg"    # Ljava/lang/String;
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x3

    .line 356
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 358
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p2, :cond_0

    .line 359
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 360
    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 359
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 363
    :cond_0
    if-eqz p1, :cond_1

    .line 364
    new-array v1, v3, [I

    fill-array-data v1, :array_1

    .line 365
    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 364
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 368
    :cond_1
    if-eqz p3, :cond_2

    .line 369
    new-array v1, v3, [I

    fill-array-data v1, :array_2

    .line 370
    invoke-virtual {p0, p3, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 369
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 373
    :cond_2
    if-eqz p4, :cond_3

    .line 374
    new-array v1, v3, [I

    fill-array-data v1, :array_3

    .line 375
    invoke-virtual {p0, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 374
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 378
    :cond_3
    if-eqz p1, :cond_4

    .line 379
    new-array v1, v4, [I

    fill-array-data v1, :array_4

    .line 380
    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 379
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 383
    :cond_4
    if-eqz p2, :cond_5

    .line 384
    new-array v1, v4, [I

    fill-array-data v1, :array_5

    .line 385
    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 384
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 388
    :cond_5
    return-object v0

    .line 359
    :array_0
    .array-data 4
        0x10100a0
        -0x101009c
        0x101009e
    .end array-data

    .line 364
    :array_1
    .array-data 4
        -0x10100a0
        -0x101009c
        0x101009e
    .end array-data

    .line 369
    :array_2
    .array-data 4
        0x10100a0
        0x101009c
        0x101009e
    .end array-data

    .line 374
    :array_3
    .array-data 4
        -0x10100a0
        0x101009c
        0x101009e
    .end array-data

    .line 379
    :array_4
    .array-data 4
        -0x10100a0
        0x101009e
    .end array-data

    .line 384
    :array_5
    .array-data 4
        0x10100a0
        0x101009e
    .end array-data
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;
    .param p3, "focusCheckImg"    # Ljava/lang/String;
    .param p4, "focusUncheckImg"    # Ljava/lang/String;
    .param p5, "pressCheckImg"    # Ljava/lang/String;
    .param p6, "pressUncheckImg"    # Ljava/lang/String;
    .param p7, "width"    # I
    .param p8, "height"    # I

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 394
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 396
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p2, :cond_0

    .line 397
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    .line 399
    invoke-virtual {p0, p2, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 397
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 402
    :cond_0
    if-eqz p1, :cond_1

    .line 403
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    .line 405
    invoke-virtual {p0, p1, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 403
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 408
    :cond_1
    if-eqz p3, :cond_2

    .line 409
    new-array v1, v4, [I

    fill-array-data v1, :array_2

    .line 411
    invoke-virtual {p0, p3, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 409
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 414
    :cond_2
    if-eqz p4, :cond_3

    .line 415
    new-array v1, v4, [I

    fill-array-data v1, :array_3

    .line 417
    invoke-virtual {p0, p4, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 415
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 419
    :cond_3
    if-eqz p5, :cond_4

    .line 420
    new-array v1, v3, [I

    fill-array-data v1, :array_4

    .line 421
    invoke-virtual {p0, p5, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 420
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 424
    :cond_4
    if-eqz p6, :cond_5

    .line 425
    new-array v1, v3, [I

    fill-array-data v1, :array_5

    .line 426
    invoke-virtual {p0, p6, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 425
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 429
    :cond_5
    if-eqz p1, :cond_6

    .line 430
    new-array v1, v3, [I

    fill-array-data v1, :array_6

    .line 431
    invoke-virtual {p0, p1, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 430
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 434
    :cond_6
    if-eqz p2, :cond_7

    .line 435
    new-array v1, v3, [I

    fill-array-data v1, :array_7

    .line 436
    invoke-virtual {p0, p2, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 435
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 439
    :cond_7
    return-object v0

    .line 397
    :array_0
    .array-data 4
        0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 403
    :array_1
    .array-data 4
        -0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 409
    :array_2
    .array-data 4
        0x10100a0
        0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 415
    :array_3
    .array-data 4
        -0x10100a0
        0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 420
    :array_4
    .array-data 4
        0x10100a0
        0x10100a7
        0x101009e
    .end array-data

    .line 425
    :array_5
    .array-data 4
        -0x10100a0
        0x10100a7
        0x101009e
    .end array-data

    .line 430
    :array_6
    .array-data 4
        -0x10100a0
        -0x10100a7
        0x101009e
    .end array-data

    .line 435
    :array_7
    .array-data 4
        0x10100a0
        -0x10100a7
        0x101009e
    .end array-data
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;
    .param p3, "focusCheckImg"    # Ljava/lang/String;
    .param p4, "focusUncheckImg"    # Ljava/lang/String;
    .param p5, "pressCheckImg"    # Ljava/lang/String;
    .param p6, "pressUncheckImg"    # Ljava/lang/String;
    .param p7, "width"    # I
    .param p8, "height"    # I
    .param p9, "forceRezie"    # Z

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 577
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 579
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p2, :cond_0

    .line 580
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    .line 582
    invoke-virtual {p0, p2, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 580
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 585
    :cond_0
    if-eqz p1, :cond_1

    .line 586
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    .line 588
    invoke-virtual {p0, p1, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 586
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 591
    :cond_1
    if-eqz p3, :cond_2

    .line 592
    new-array v1, v4, [I

    fill-array-data v1, :array_2

    .line 594
    invoke-virtual {p0, p3, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 592
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 597
    :cond_2
    if-eqz p4, :cond_3

    .line 598
    new-array v1, v4, [I

    fill-array-data v1, :array_3

    .line 600
    invoke-virtual {p0, p4, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 598
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 602
    :cond_3
    if-eqz p5, :cond_4

    .line 603
    new-array v1, v3, [I

    fill-array-data v1, :array_4

    .line 604
    invoke-virtual {p0, p5, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 603
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 607
    :cond_4
    if-eqz p6, :cond_5

    .line 608
    new-array v1, v3, [I

    fill-array-data v1, :array_5

    .line 609
    invoke-virtual {p0, p6, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 608
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 612
    :cond_5
    if-eqz p1, :cond_6

    .line 613
    new-array v1, v3, [I

    fill-array-data v1, :array_6

    .line 614
    invoke-virtual {p0, p1, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 613
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 617
    :cond_6
    if-eqz p2, :cond_7

    .line 618
    new-array v1, v3, [I

    fill-array-data v1, :array_7

    .line 619
    invoke-virtual {p0, p2, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 618
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 622
    :cond_7
    return-object v0

    .line 580
    :array_0
    .array-data 4
        0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 586
    :array_1
    .array-data 4
        -0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 592
    :array_2
    .array-data 4
        0x10100a0
        0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 598
    :array_3
    .array-data 4
        -0x10100a0
        0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 603
    :array_4
    .array-data 4
        0x10100a0
        0x10100a7
        0x101009e
    .end array-data

    .line 608
    :array_5
    .array-data 4
        -0x10100a0
        0x10100a7
        0x101009e
    .end array-data

    .line 613
    :array_6
    .array-data 4
        -0x10100a0
        -0x10100a7
        0x101009e
    .end array-data

    .line 618
    :array_7
    .array-data 4
        0x10100a0
        -0x10100a7
        0x101009e
    .end array-data
.end method

.method protected setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "dimImg"    # Ljava/lang/String;
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 292
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 294
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 295
    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 297
    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 295
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 300
    :cond_0
    if-eqz p2, :cond_1

    .line 301
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    .line 302
    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 301
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 305
    :cond_1
    if-eqz p3, :cond_2

    .line 306
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    .line 307
    invoke-virtual {p0, p3, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 306
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 309
    :cond_2
    if-eqz p4, :cond_3

    .line 310
    new-array v1, v4, [I

    const v2, -0x101009e

    aput v2, v1, v3

    .line 311
    invoke-virtual {p0, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 310
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 313
    :cond_3
    return-object v0

    .line 295
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
        0x101009e
    .end array-data
.end method

.method protected setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 76
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkResInHash(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->hash:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    move-object v0, v2

    .line 114
    :cond_0
    :goto_0
    return-object v0

    .line 79
    :cond_1
    const/4 v0, 0x0

    .line 81
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    const-string/jumbo v4, "spen_sdk_resource_custom"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 82
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    const-string v4, "drawable"

    .line 83
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 82
    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 85
    .local v1, "mDrawableResID":I
    if-nez v1, :cond_3

    .line 86
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v4, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 87
    if-nez v1, :cond_2

    move-object v0, v2

    .line 88
    goto :goto_0

    .line 92
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 93
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v2, p1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 99
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 100
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v2, p1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 105
    .end local v1    # "mDrawableResID":I
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v4, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 106
    .restart local v1    # "mDrawableResID":I
    if-nez v1, :cond_5

    move-object v0, v2

    .line 107
    goto :goto_0

    .line 110
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 111
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v2, p1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 121
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkResInHash(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->getDrawableFromKey(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 155
    :cond_0
    :goto_0
    return-object v6

    .line 124
    :cond_1
    const/4 v6, 0x0

    .line 126
    .local v6, "drawable":Landroid/graphics/drawable/Drawable;
    const-string/jumbo v1, "spen_sdk_resource_custom"

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 127
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    .line 128
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 127
    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 130
    .local v2, "mDrawableResID":I
    if-nez v2, :cond_3

    .line 131
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 132
    if-nez v2, :cond_2

    move-object v6, v0

    .line 133
    goto :goto_0

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 142
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 147
    .end local v2    # "mDrawableResID":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 148
    .restart local v2    # "mDrawableResID":I
    if-nez v2, :cond_5

    move-object v6, v0

    .line 149
    goto :goto_0

    .line 151
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 152
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method protected setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "forceResize"    # Z

    .prologue
    const/4 v0, 0x0

    .line 651
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkResInHash(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 652
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->getDrawableFromKey(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 685
    :cond_0
    :goto_0
    return-object v6

    .line 654
    :cond_1
    const/4 v6, 0x0

    .line 656
    .local v6, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    const-string/jumbo v3, "spen_sdk_resource_custom"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 657
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    .line 658
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 657
    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 660
    .local v2, "mDrawableResID":I
    if-nez v2, :cond_3

    .line 661
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 662
    if-nez v2, :cond_2

    move-object v6, v0

    .line 663
    goto :goto_0

    .line 665
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 666
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 671
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 672
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 677
    .end local v2    # "mDrawableResID":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 678
    .restart local v2    # "mDrawableResID":I
    if-nez v2, :cond_5

    move-object v6, v0

    .line 679
    goto :goto_0

    .line 681
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 682
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->checkString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resManager:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-virtual {v0, p1, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method protected setDrawableSelectColor(III)Landroid/graphics/drawable/StateListDrawable;
    .locals 7
    .param p1, "defaultColor"    # I
    .param p2, "selectedColor"    # I
    .param p3, "pressColor"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 501
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 503
    .local v1, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 504
    .local v0, "defaultState":[I
    const v4, -0x10100a7

    aput v4, v0, v5

    .line 505
    const v4, -0x10100a1

    aput v4, v0, v6

    .line 507
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v0, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 508
    new-array v3, v6, [I

    .line 509
    .local v3, "selecteState":[I
    const v4, 0x10100a7

    aput v4, v3, v5

    .line 510
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 511
    new-array v2, v6, [I

    .line 512
    .local v2, "pressState":[I
    const v4, 0x10100a1

    aput v4, v2, v5

    .line 513
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, p3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 515
    return-object v1
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 218
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 220
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 221
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 222
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 221
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 224
    :cond_0
    if-eqz p2, :cond_1

    .line 225
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 227
    :cond_1
    if-eqz p3, :cond_2

    .line 228
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 230
    :cond_2
    if-eqz p2, :cond_3

    .line 231
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 234
    :cond_3
    return-object v0

    .line 221
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 240
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 241
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 242
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 243
    invoke-virtual {p0, p1, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 242
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 246
    :cond_0
    if-eqz p2, :cond_1

    .line 247
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    .line 248
    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 247
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 250
    :cond_1
    if-eqz p3, :cond_2

    .line 251
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    .line 252
    invoke-virtual {p0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 251
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 254
    :cond_2
    if-eqz p2, :cond_3

    .line 255
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    .line 256
    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 255
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 259
    :cond_3
    return-object v0

    .line 242
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "forceResize"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 628
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 629
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 630
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 631
    invoke-virtual {p0, p1, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 630
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 634
    :cond_0
    if-eqz p2, :cond_1

    .line 635
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    .line 636
    invoke-virtual {p0, p2, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 635
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 638
    :cond_1
    if-eqz p3, :cond_2

    .line 639
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    .line 640
    invoke-virtual {p0, p3, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 639
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 642
    :cond_2
    if-eqz p2, :cond_3

    .line 643
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    .line 644
    invoke-virtual {p0, p2, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 643
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 647
    :cond_3
    return-object v0

    .line 630
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 9
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "selectedImg"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 457
    new-instance v4, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 458
    .local v4, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 460
    const/4 v5, 0x3

    new-array v3, v5, [I

    .line 461
    .local v3, "arrayOfInt4":[I
    const v5, -0x10100a7

    aput v5, v3, v7

    .line 462
    const v5, -0x10100a1

    aput v5, v3, v8

    .line 463
    const/4 v5, 0x2

    const v6, -0x101009c

    aput v6, v3, v5

    .line 464
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 466
    .end local v3    # "arrayOfInt4":[I
    :cond_0
    if-eqz p3, :cond_1

    .line 468
    new-array v0, v8, [I

    .line 469
    .local v0, "arrayOfInt1":[I
    const v5, 0x101009c

    aput v5, v0, v7

    .line 470
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 472
    .end local v0    # "arrayOfInt1":[I
    :cond_1
    if-eqz p2, :cond_2

    .line 474
    new-array v2, v8, [I

    .line 475
    .local v2, "arrayOfInt3":[I
    const v5, 0x10100a7

    aput v5, v2, v7

    .line 476
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 479
    .end local v2    # "arrayOfInt3":[I
    :cond_2
    if-eqz p4, :cond_3

    .line 481
    new-array v1, v8, [I

    .line 482
    .local v1, "arrayOfInt2":[I
    const v5, 0x10100a1

    aput v5, v1, v7

    .line 483
    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 486
    .end local v1    # "arrayOfInt2":[I
    :cond_3
    return-object v4
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 12
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "selectedImg"    # Ljava/lang/String;
    .param p5, "dimImg"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I

    .prologue
    .line 693
    new-instance v9, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v9}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 694
    .local v9, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 696
    const/4 v10, 0x4

    new-array v7, v10, [I

    .line 697
    .local v7, "arrayOfInt4":[I
    const/4 v10, 0x0

    const v11, -0x10100a7

    aput v11, v7, v10

    .line 698
    const/4 v10, 0x1

    const v11, -0x10100a1

    aput v11, v7, v10

    .line 699
    const/4 v10, 0x2

    const v11, -0x101009c

    aput v11, v7, v10

    .line 700
    const/4 v10, 0x3

    const v11, 0x101009e

    aput v11, v7, v10

    .line 702
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v7, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 704
    if-nez p2, :cond_0

    .line 705
    const/4 v10, 0x1

    new-array v8, v10, [I

    .line 706
    .local v8, "arrayOfInt5":[I
    const/4 v10, 0x0

    const v11, 0x10100a7

    aput v11, v8, v10

    .line 708
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 713
    .end local v7    # "arrayOfInt4":[I
    .end local v8    # "arrayOfInt5":[I
    :cond_0
    if-eqz p5, :cond_1

    .line 714
    const/4 v10, 0x1

    new-array v3, v10, [I

    .line 715
    .local v3, "arrayOfInt0":[I
    const/4 v10, 0x0

    const v11, -0x101009e

    aput v11, v3, v10

    .line 716
    move-object/from16 v0, p5

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v3, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 719
    .end local v3    # "arrayOfInt0":[I
    :cond_1
    if-eqz p3, :cond_2

    .line 721
    const/4 v10, 0x1

    new-array v4, v10, [I

    .line 722
    .local v4, "arrayOfInt1":[I
    const/4 v10, 0x0

    const v11, 0x101009c

    aput v11, v4, v10

    .line 724
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkVersion:I

    const/16 v11, 0x15

    if-ge v10, v11, :cond_5

    .line 725
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v4, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 733
    .end local v4    # "arrayOfInt1":[I
    :cond_2
    :goto_0
    if-eqz p2, :cond_3

    .line 735
    const/4 v10, 0x1

    new-array v6, v10, [I

    .line 736
    .local v6, "arrayOfInt3":[I
    const/4 v10, 0x0

    const v11, 0x10100a7

    aput v11, v6, v10

    .line 737
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p2, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v6, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 740
    .end local v6    # "arrayOfInt3":[I
    :cond_3
    if-eqz p4, :cond_4

    .line 742
    const/4 v10, 0x1

    new-array v5, v10, [I

    .line 743
    .local v5, "arrayOfInt2":[I
    const/4 v10, 0x0

    const v11, 0x10100a1

    aput v11, v5, v10

    .line 744
    move-object/from16 v0, p4

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v5, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 747
    .end local v5    # "arrayOfInt2":[I
    :cond_4
    return-object v9

    .line 727
    .restart local v4    # "arrayOfInt1":[I
    :cond_5
    if-eqz p1, :cond_2

    .line 728
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v4, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected setDrawableSelectedFocusImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 6
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "selectedImg"    # Ljava/lang/String;
    .param p4, "selectfocusedImg"    # Ljava/lang/String;
    .param p5, "unselectfocusedImg"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 265
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 267
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 268
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 269
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 268
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 271
    :cond_0
    if-eqz p2, :cond_1

    .line 272
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 274
    :cond_1
    if-eqz p4, :cond_2

    .line 275
    new-array v1, v5, [I

    fill-array-data v1, :array_1

    .line 276
    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 275
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 278
    :cond_2
    if-eqz p5, :cond_3

    .line 279
    new-array v1, v5, [I

    fill-array-data v1, :array_2

    .line 280
    invoke-virtual {p0, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 279
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 282
    :cond_3
    if-eqz p3, :cond_4

    .line 283
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 286
    :cond_4
    return-object v0

    .line 268
    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data

    .line 275
    :array_1
    .array-data 4
        0x101009c
        0x10100a1
    .end array-data

    .line 279
    :array_2
    .array-data 4
        0x101009c
        -0x10100a1
    .end array-data
.end method

.method public unbindDrawables(Landroid/view/View;)V
    .locals 4
    .param p1, "root"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 531
    if-nez p1, :cond_1

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 535
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_7

    .line 536
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 541
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 542
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 544
    :cond_2
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    .line 545
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_8

    .line 548
    instance-of v1, p1, Landroid/widget/AdapterView;

    if-nez v1, :cond_9

    move-object v1, p1

    .line 549
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 555
    .end local v0    # "i":I
    :cond_3
    :goto_3
    instance-of v1, p1, Landroid/widget/SeekBar;

    if-eqz v1, :cond_4

    move-object v1, p1

    .line 556
    check-cast v1, Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 559
    :cond_4
    instance-of v1, p1, Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    move-object v1, p1

    .line 560
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, p1

    .line 561
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 563
    :cond_5
    instance-of v1, p1, Landroid/widget/ImageButton;

    if-eqz v1, :cond_6

    move-object v1, p1

    .line 564
    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, p1

    .line 565
    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 568
    :cond_6
    if-eqz p1, :cond_0

    .line 569
    const/4 p1, 0x0

    goto :goto_0

    .line 538
    :cond_7
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .restart local v0    # "i":I
    :cond_8
    move-object v1, p1

    .line 546
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 545
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 551
    :cond_9
    check-cast p1, Landroid/widget/AdapterView;

    .end local p1    # "root":Landroid/view/View;
    invoke-virtual {p1, v3}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 552
    const/4 p1, 0x0

    .restart local p1    # "root":Landroid/view/View;
    goto :goto_3
.end method
