.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRDefaultShapeStrokesBuilder;
.super Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;
.source "NRRDefaultShapeStrokesBuilder.java"


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V
    .locals 0
    .param p1, "penSettings"    # Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;
    .param p2, "shapeInfo"    # Lcom/samsung/recognitionengine/ShapeInfo;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V

    .line 26
    return-void
.end method


# virtual methods
.method public buildLayoutObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2

    .prologue
    .line 29
    const/16 v0, 0x64

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRDefaultShapeStrokesBuilder;->buildLayoutObject(IZ)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    return-object v0
.end method

.method public buildLayoutObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 1
    .param p1, "pointAmount"    # I

    .prologue
    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRDefaultShapeStrokesBuilder;->buildLayoutObject(IZ)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    return-object v0
.end method

.method public buildLayoutObject(IZ)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 18
    .param p1, "pointAmount"    # I
    .param p2, "curveEnabled"    # Z

    .prologue
    .line 38
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRDefaultShapeStrokesBuilder;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    move/from16 v0, p1

    int-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Lcom/samsung/recognitionengine/ShapeInfo;->generatePoints(J)Lcom/samsung/recognitionengine/VectorPointFVectors;

    move-result-object v4

    .line 39
    .local v4, "gestures":Lcom/samsung/recognitionengine/VectorPointFVectors;
    invoke-virtual {v4}, Lcom/samsung/recognitionengine/VectorPointFVectors;->size()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v3, v0

    .line 40
    .local v3, "gestureCount":I
    if-nez v3, :cond_1

    .line 41
    const/4 v2, 0x0

    .line 73
    :cond_0
    :goto_0
    return-object v2

    .line 43
    :cond_1
    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 44
    .local v2, "container":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_1
    if-lt v5, v3, :cond_2

    .line 70
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 71
    const/4 v2, 0x0

    goto :goto_0

    .line 45
    :cond_2
    invoke-virtual {v4, v5}, Lcom/samsung/recognitionengine/VectorPointFVectors;->get(I)Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v10

    .line 46
    .local v10, "points":Lcom/samsung/recognitionengine/PointFVector;
    invoke-virtual {v10}, Lcom/samsung/recognitionengine/PointFVector;->size()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v8, v0

    .line 48
    .local v8, "pointCount":I
    if-nez v8, :cond_3

    .line 44
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 51
    :cond_3
    new-array v11, v8, [Landroid/graphics/PointF;

    .line 52
    .local v11, "pointsToAdd":[Landroid/graphics/PointF;
    new-array v12, v8, [F

    .line 53
    .local v12, "pressures":[F
    new-array v14, v8, [I

    .line 54
    .local v14, "timestamps":[I
    const/high16 v15, 0x3f800000    # 1.0f

    invoke-static {v12, v15}, Ljava/util/Arrays;->fill([FF)V

    .line 55
    const/4 v9, 0x0

    .line 56
    .local v9, "pointIndex":I
    const/4 v6, 0x0

    .local v6, "l":I
    :goto_3
    if-lt v6, v8, :cond_4

    .line 62
    new-instance v13, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    const/4 v15, 0x0

    invoke-direct {v13, v15, v11, v12, v14}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)V

    .line 64
    .local v13, "stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRDefaultShapeStrokesBuilder;->setStrokeStyle(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    .line 65
    const/4 v15, 0x0

    invoke-virtual {v13, v15}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setToolType(I)V

    .line 66
    move/from16 v0, p2

    invoke-virtual {v13, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setCurveEnabled(Z)V

    .line 67
    invoke-virtual {v2, v13}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_2

    .line 57
    .end local v13    # "stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    :cond_4
    invoke-virtual {v10, v6}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v7

    .line 58
    .local v7, "p":Lcom/samsung/recognitionengine/PointF;
    new-instance v15, Landroid/graphics/PointF;

    invoke-virtual {v7}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v16

    invoke-virtual {v7}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v17

    invoke-direct/range {v15 .. v17}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v15, v11, v9

    .line 59
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v15, v0

    aput v15, v14, v9

    .line 60
    add-int/lit8 v9, v9, 0x1

    .line 56
    add-int/lit8 v6, v6, 0x1

    goto :goto_3
.end method
