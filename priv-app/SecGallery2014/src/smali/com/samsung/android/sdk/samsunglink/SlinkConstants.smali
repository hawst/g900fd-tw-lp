.class public Lcom/samsung/android/sdk/samsunglink/SlinkConstants;
.super Ljava/lang/Object;
.source "SlinkConstants.java"


# static fields
.field public static final ACTION_CLOUD_AUTHENTICATION_FAILURE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkConstants.ACTION_CLOUD_AUTHENTICATION_FAILURE"

.field public static final BROADCAST_LOCAL_MEDIA_INSERTED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.LOCAL_MEDIA_INSERTED"

.field public static final CHANGE_OPTION_EXTRA_KEY:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.CHANGE_OPTION_EXTRA_KEY"

.field public static final DEVICE_ID_EXTRA_KEY:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.DEVICE_ID_EXTRA_KEY"

.field public static final SAMSUNG_LINK_BROADCAST_PERMISSION:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.permission.BROADCAST_SAMSUNG_LINK"

.field public static final SAMSUNG_LINK_PRIVATE_ACCESS_PERMISSION:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.permission.PRIVATE_ACCESS"

.field public static final SLINK_HOMESYNC_IS_PRIVATE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SLINK_HOMESYNC_IS_PRIVATE"

.field public static final SLINK_UI_APP_THEME:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
