.class public Lcom/samsung/android/sdk/pen/engine/SpenControlImage;
.super Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.source "SpenControlImage.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;
    }
.end annotation


# static fields
.field private static CONFIG_DRAW_MOVING_OBJECT:Z = false

.field private static final DEFAULT_MOVE_COLOR:I = 0x55ffffff


# instance fields
.field mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->CONFIG_DRAW_MOVING_OBJECT:Z

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 56
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->initialize()V

    .line 57
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    .line 61
    return-void
.end method


# virtual methods
.method public getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-object v0

    .line 170
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    goto :goto_0
.end method

.method protected onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 103
    sget-boolean v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->CONFIG_DRAW_MOVING_OBJECT:Z

    if-eqz v1, :cond_2

    .line 104
    if-eqz p3, :cond_0

    .line 105
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 106
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, p2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 107
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 108
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    const/16 v2, 0x7f

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 111
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 112
    const v1, 0x55ffffff    # 3.518437E13f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 113
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 114
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 119
    .end local v0    # "paint":Landroid/graphics/Paint;
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method protected onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 11
    .param p1, "flipDirection"    # I
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    const/4 v10, 0x1

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v1, 0x0

    .line 69
    sget-boolean v2, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->CONFIG_DRAW_MOVING_OBJECT:Z

    if-eqz v2, :cond_3

    .line 70
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v2, :cond_0

    .line 95
    :goto_0
    return-void

    .line 74
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 76
    .local v5, "m":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    .line 77
    .local v8, "src":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 78
    .local v0, "map":Landroid/graphics/Bitmap;
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v8, v2, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 80
    and-int/lit8 v2, p1, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 81
    invoke-virtual {v5, v6, v9}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 84
    :cond_1
    and-int/lit8 v2, p1, 0x1

    if-ne v2, v10, :cond_2

    .line 85
    invoke-virtual {v5, v9, v6}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 88
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 89
    .local v7, "dst":Landroid/graphics/Bitmap;
    const/16 v1, 0xa0

    invoke-virtual {v7, v1}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 91
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 94
    .end local v0    # "map":Landroid/graphics/Bitmap;
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "dst":Landroid/graphics/Bitmap;
    .end local v8    # "src":Landroid/graphics/Bitmap;
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method protected onObjectChanged()V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    .line 130
    return-void
.end method

.method public setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V
    .locals 5
    .param p1, "objectImage"    # Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .prologue
    .line 142
    if-nez p1, :cond_0

    .line 156
    :goto_0
    return-void

    .line 146
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getRotation()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mRotateAngle:F

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->setObjectList(Ljava/util/ArrayList;)V

    .line 151
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getSorInfo()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 152
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getImage()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0
.end method
