.class Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;
.super Ljava/lang/Object;
.source "SPenSeekBarView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    .line 473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 476
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 477
    .local v0, "action":I
    if-eq v0, v4, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 478
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v2

    .line 479
    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 480
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 481
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    invoke-static {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;Z)V

    .line 482
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSPenSeekBarChangeListner:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-interface {v1, v4, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$SPenSeekBarChangeListner;->onUpdate(ZI)V

    .line 483
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 484
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 485
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mPlusButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 488
    :cond_1
    return v3
.end method
