.class Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;
.super Landroid/widget/LinearLayout;
.source "SpenThumbControlBackGround.java"


# instance fields
.field private mPaint:Landroid/graphics/Paint;

.field private mTopPadding:I

.field private mTrackWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method


# virtual methods
.method public initPaint()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    const v1, -0xd4d4d5

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTrackWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 30
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 31
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "mCanvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->getWidth()I

    move-result v0

    div-int/lit8 v6, v0, 0x2

    .line 43
    .local v6, "mWidthHalf":I
    int-to-float v1, v6

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTopPadding:I

    add-int/lit8 v0, v0, 0x3

    int-to-float v2, v0

    int-to-float v3, v6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTopPadding:I

    sub-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x5

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 44
    return-void
.end method

.method public setTopPadding(I)V
    .locals 0
    .param p1, "topPadding"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTopPadding:I

    .line 54
    return-void
.end method

.method public setTrackWidth(I)V
    .locals 0
    .param p1, "trackWidth"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTrackWidth:I

    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->initPaint()V

    .line 65
    return-void
.end method
