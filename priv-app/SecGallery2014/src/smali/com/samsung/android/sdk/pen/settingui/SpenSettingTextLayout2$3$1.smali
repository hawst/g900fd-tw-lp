.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout2.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    .line 341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectItem(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 344
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_1

    .line 345
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v3

    iput v3, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    .line 349
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    int-to-double v2, v2

    const-wide v4, 0x4076800000000000L    # 360.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 350
    .local v0, "fontPointPixel":F
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v3, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iput v2, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 351
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextSize(F)V

    .line 352
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Ljava/lang/String;)V

    .line 353
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/widget/Button;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->invalidate()V

    .line 356
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_0

    .line 357
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 358
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 359
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 362
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_0
    return-void

    .line 347
    .end local v0    # "fontPointPixel":F
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$3;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    const/16 v3, 0x404

    iput v3, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    goto/16 :goto_0
.end method
