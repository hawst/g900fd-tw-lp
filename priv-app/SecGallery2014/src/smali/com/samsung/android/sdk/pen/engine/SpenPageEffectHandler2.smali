.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;
.super Landroid/os/Handler;
.source "SpenPageEffectHandler2.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# static fields
.field private static final PAGE_EFFECT_IN:I = 0x1

.field private static final PAGE_EFFECT_OUT:I = 0x2

.field private static final PAGE_EFFECT_TIMER_INTERVAL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "PageEffectHandler"


# instance fields
.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCount:I

.field private mCurrentX:F

.field private mDirection:I

.field private mDistanceTable:[F

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mScreenshot:Landroid/graphics/Bitmap;

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mWorking:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    .line 21
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    .line 22
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    .line 24
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    .line 25
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    .line 28
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 54
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    .line 58
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 61
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .line 62
    return-void

    .line 54
    nop

    :array_0
    .array-data 4
        0x3e19999a    # 0.15f
        0x3e4f0414
        0x3e8b8100
        0x3ebc048d
        0x3efd6720
        0x3f2ac35b
        0x3f6625ce
        0x3f9b1788
        0x3fd106d5
        0x400cdbe1
        0x403dd810
        0x407fdd38
        0x40ac6bf6
        0x40e86213
        0x411c992c
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
    .end array-data
.end method

.method private endAnimation()V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onFinish()V

    .line 222
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 68
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    .line 70
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    .line 71
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .line 73
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    .line 74
    return-void
.end method

.method public drawAnimation(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 134
    const-string v0, "PageEffectHandler"

    const-string v1, "Start draw animation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->LIGHTEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 140
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 141
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    .line 142
    const-string v0, "PageEffectHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v1, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 148
    :goto_1
    const-string v0, "PageEffectHandler"

    const-string v1, "Start draw animation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    .line 145
    :cond_1
    const-string v0, "PageEffectHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "(0, 0) ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x0

    const v8, 0x3c23d70a    # 0.01f

    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    .line 153
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    if-nez v2, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v4, :cond_5

    .line 163
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x41000000    # 8.0f

    div-float v1, v2, v3

    .line 165
    .local v1, "distance":F
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-nez v2, :cond_3

    .line 166
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 172
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdate()V

    .line 176
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_4

    .line 177
    const/16 v2, 0x13

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 178
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 179
    .local v0, "canvas":Landroid/graphics/Canvas;
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v9, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 180
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer(Landroid/graphics/Canvas;)V

    .line 181
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    neg-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 182
    invoke-virtual {p0, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 167
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :cond_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-ne v2, v5, :cond_2

    .line 168
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    sub-float/2addr v2, v1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    goto :goto_1

    .line 184
    :cond_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 185
    invoke-virtual {p0, v4, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 187
    .end local v1    # "distance":F
    :cond_5
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v5, :cond_0

    .line 189
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    array-length v3, v3

    if-lt v2, v3, :cond_7

    .line 190
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    mul-float/2addr v2, v3

    mul-float v1, v2, v8

    .line 195
    .restart local v1    # "distance":F
    :goto_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-nez v2, :cond_8

    .line 196
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 203
    :cond_6
    :goto_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    if-nez v2, :cond_9

    .line 205
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 206
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->endAnimation()V

    .line 208
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    .line 209
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 210
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 192
    .end local v1    # "distance":F
    :cond_7
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    aget v3, v3, v4

    mul-float/2addr v2, v3

    mul-float v1, v2, v8

    .restart local v1    # "distance":F
    goto :goto_2

    .line 197
    :cond_8
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-ne v2, v5, :cond_6

    .line 198
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    sub-float/2addr v2, v1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    goto :goto_3

    .line 212
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdate()V

    .line 213
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 214
    invoke-virtual {p0, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0
.end method

.method public isWorking()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    return v0
.end method

.method public saveScreenshot()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 90
    :try_start_0
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 97
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 98
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer(Landroid/graphics/Canvas;)V

    .line 99
    const/4 v0, 0x0

    .line 101
    const/4 v2, 0x1

    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :goto_0
    return v2

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 93
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    goto :goto_0
.end method

.method public setCanvasInformation(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 107
    return-void
.end method

.method public setCanvasInformation1(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 112
    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    .line 79
    return-void
.end method

.method public setScreenResolution(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    .line 84
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    .line 85
    return-void
.end method

.method public startAnimation(I)Z
    .locals 4
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 116
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 124
    :goto_0
    return v0

    .line 119
    :cond_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    .line 121
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    .line 122
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    .line 123
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 124
    goto :goto_0
.end method
