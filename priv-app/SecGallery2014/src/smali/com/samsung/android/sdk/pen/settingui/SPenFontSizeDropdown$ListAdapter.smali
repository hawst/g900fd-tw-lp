.class Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SPenFontSizeDropdown.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    .line 56
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    .line 57
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    .line 58
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    .line 59
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/high16 v8, 0x40a00000    # 5.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const/16 v6, 0x640

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 63
    if-nez p2, :cond_0

    .line 64
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 65
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x1090008

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 67
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 69
    .local v1, "tv":Landroid/widget/TextView;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 70
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 71
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 73
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    if-ne v2, v6, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v5

    if-nez v2, :cond_6

    .line 74
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const/high16 v3, 0x40400000    # 3.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_5

    .line 75
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 82
    :goto_0
    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 84
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    if-ne v2, v6, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v5

    if-eqz v2, :cond_2

    .line 85
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    const/16 v3, 0x300

    if-ne v2, v3, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_8

    .line 86
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v5

    if-nez v2, :cond_7

    .line 87
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 88
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 87
    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 96
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    const/16 v3, 0x300

    if-ne v2, v3, :cond_9

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_9

    .line 97
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const v3, 0x42466666    # 49.6f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 106
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSelectedIndex:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I

    move-result v2

    if-ne v2, p1, :cond_b

    .line 107
    const v2, -0x2fbf00

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 112
    :goto_3
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_4

    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_4

    .line 113
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 114
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const-string v3, ""

    const-string/jumbo v4, "tw_spinner_list_pressed_holo_light"

    .line 115
    const-string/jumbo v5, "tw_spinner_list_focused_holo_light"

    .line 114
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 117
    :cond_4
    return-object p2

    .line 77
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const v3, 0x414dc28f    # 12.86f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 80
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 90
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const v3, 0x419e6666    # 19.8f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 91
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 90
    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 94
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 99
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const v3, 0x40333333    # 2.8f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    if-ne v2, v6, :cond_a

    .line 100
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v5

    if-nez v2, :cond_a

    .line 101
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const v3, 0x42146666    # 37.1f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 103
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x42200000    # 40.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 109
    :cond_b
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/high16 v8, 0x40a00000    # 5.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const/16 v6, 0x640

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 124
    if-nez p2, :cond_0

    .line 125
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 126
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x1090008

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 128
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 130
    .local v1, "tv":Landroid/widget/TextView;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 131
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 132
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 134
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    if-ne v2, v6, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v5

    if-nez v2, :cond_6

    .line 135
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const/high16 v3, 0x40400000    # 3.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_5

    .line 136
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 143
    :goto_0
    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 145
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    if-ne v2, v6, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v5

    if-eqz v2, :cond_2

    .line 146
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    const/16 v3, 0x300

    if-ne v2, v3, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_8

    .line 147
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v5

    if-nez v2, :cond_7

    .line 148
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 149
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 148
    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 157
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    const/16 v3, 0x300

    if-ne v2, v3, :cond_9

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_9

    .line 158
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const v3, 0x42466666    # 49.6f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 168
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSelectedIndex:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I

    move-result v2

    if-ne v2, p1, :cond_b

    .line 169
    const v2, -0x2fbf00

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 174
    :goto_3
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_4

    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mSdkVersion:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_4

    .line 175
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 176
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const-string v3, ""

    const-string/jumbo v4, "tw_spinner_list_pressed_holo_light"

    .line 177
    const-string/jumbo v5, "tw_spinner_list_focused_holo_light"

    .line 176
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 179
    :cond_4
    return-object p2

    .line 138
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const v3, 0x414dc28f    # 12.86f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 141
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 151
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const v3, 0x419e6666    # 19.8f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 152
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 151
    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 155
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 160
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const v3, 0x40333333    # 2.8f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v2

    if-ne v2, v6, :cond_a

    .line 161
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v2

    cmpl-float v2, v2, v5

    if-nez v2, :cond_a

    .line 162
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const v3, 0x42146666    # 37.1f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 164
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v2

    const/high16 v3, 0x42200000    # 40.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 171
    :cond_b
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3
.end method
