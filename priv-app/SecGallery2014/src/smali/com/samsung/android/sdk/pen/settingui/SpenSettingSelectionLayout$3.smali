.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;
.super Ljava/lang/Object;
.source "SpenSettingSelectionLayout.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 359
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 362
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v1, v3

    .line 363
    .local v1, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v2, v3

    .line 365
    .local v2, "y":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 366
    .local v0, "Params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v4, v1, v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$20(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;I)V

    .line 367
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v4, v2, v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;I)V

    .line 369
    const/4 v3, 0x1

    return v3
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;
    .param p2, "arg1"    # Landroid/view/MotionEvent;
    .param p3, "arg2"    # F
    .param p4, "arg3"    # F

    .prologue
    .line 354
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 322
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mNeedCalculateMargin:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 323
    new-array v0, v8, [I

    .line 324
    .local v0, "location":[I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 326
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    aget v5, v0, v6

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;I)V

    .line 327
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    aget v5, v0, v7

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;I)V

    .line 329
    new-array v2, v8, [I

    .line 330
    .local v2, "rootLocation":[I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 332
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-static {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;Z)V

    .line 335
    .end local v0    # "location":[I
    .end local v2    # "rootLocation":[I
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mFirstLongPress:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 337
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mTitleLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 339
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-static {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;Z)V

    .line 342
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v1

    .line 344
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 346
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-static {v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;Z)V

    .line 347
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mIndicator:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 348
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mRightIndicator:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$19(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 349
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;
    .param p2, "arg1"    # Landroid/view/MotionEvent;
    .param p3, "arg2"    # F
    .param p4, "arg3"    # F

    .prologue
    .line 316
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 311
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method
