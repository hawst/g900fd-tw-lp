.class Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;
.super Ljava/lang/Object;
.source "SPenDropdownView.java"


# instance fields
.field protected final TabA_SCREEN_WIDTH:I

.field protected final VIENNA_SCREEN_WIDTH:I

.field protected final anchor:Landroid/view/View;

.field private background:Landroid/graphics/drawable/Drawable;

.field private mDensity:F

.field private mScreenWidth:I

.field protected final mdpi_density:I

.field private root:Landroid/view/View;

.field protected final window:Landroid/widget/PopupWindow;

.field protected final windowManager:Landroid/view/WindowManager;

.field protected final xhdpi_density:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "anchor"    # Landroid/view/View;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/16 v0, 0x5a0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->mScreenWidth:I

    .line 22
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->mDensity:F

    .line 23
    const/16 v0, 0x640

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->VIENNA_SCREEN_WIDTH:I

    .line 24
    const/16 v0, 0x300

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->TabA_SCREEN_WIDTH:I

    .line 25
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->xhdpi_density:I

    .line 26
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->mdpi_density:I

    .line 30
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->anchor:Landroid/view/View;

    .line 31
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    .line 32
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->windowManager:Landroid/view/WindowManager;

    .line 34
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->onCreate()V

    .line 35
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 92
    return-void
.end method

.method public getDensity()F
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->mDensity:F

    return v0
.end method

.method public getScreenWidth()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->mScreenWidth:I

    return v0
.end method

.method protected onCreate()V
    .locals 4

    .prologue
    .line 39
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;)V

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 50
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 51
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 52
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 53
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_0

    .line 54
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->mScreenWidth:I

    .line 58
    :goto_0
    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->mDensity:F

    .line 60
    return-void

    .line 56
    :cond_0
    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->mScreenWidth:I

    goto :goto_0
.end method

.method protected setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->background:Landroid/graphics/drawable/Drawable;

    .line 84
    return-void
.end method

.method protected setContentView(Landroid/view/View;)V
    .locals 1
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->root:Landroid/view/View;

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/PopupWindow$OnDismissListener;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 88
    return-void
.end method

.method protected show()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->root:Landroid/view/View;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "illegalStateException preShow"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->background:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->background:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->root:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 75
    return-void
.end method
