.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
.super Landroid/view/View;
.source "SpenColorPaletteView2.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;
    }
.end annotation


# static fields
.field private static final COLOR_COLUMN_NUM:I = 0x7

.field private static final COLOR_NUM_MAX:I = 0xe

.field private static final COLOR_ROW_NUM:I = 0x2

.field private static final CUSTOM_COLOR_IDX:I = 0xc

.field protected static final DEFAULT_COLOR:I = -0x1000000

.field protected static IS_COLOR_GRADATION_SELECT:Z = false

.field private static ITEM_BORDER_WIDTH:I = 0x0

.field private static ITEM_GAPX:I = 0x0

.field private static ITEM_GAPY:I = 0x0

.field private static final SPOID_ICON:I = -0x1100001

.field private static final USER_COLOR:I = -0x1000002

.field private static final WINDOW_BORDER_WIDTH:I


# instance fields
.field private currentTableIndex:I

.field isRainbow:Z

.field private mBorderPaint:Landroid/graphics/Paint;

.field private mCanvasRect:Landroid/graphics/Rect;

.field private mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

.field private final mColorContentDescritionTable1:[Ljava/lang/String;

.field private final mColorContentDescritionTable2:[Ljava/lang/String;

.field private final mColorContentDescritionTable3:[Ljava/lang/String;

.field mColorHoverIdx:I

.field private mColorPaint:Landroid/graphics/Paint;

.field private mColorTable:[I

.field private final mColorTable1:[I

.field private final mColorTable2:[I

.field private final mColorTable3:[I

.field private mCustom_imagepath:Ljava/lang/String;

.field private mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

.field mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mFirstExecuted:Z

.field private mItemRect:Landroid/graphics/Rect;

.field mPreColorHoverIdx:I

.field private mSelectRect:Landroid/graphics/Rect;

.field private mSeletedItem:I

.field private mcurrentFocus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->IS_COLOR_GRADATION_SELECT:Z

    .line 37
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    .line 40
    const/4 v0, 0x3

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    .line 41
    const/4 v0, 0x5

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPY:I

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 10
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "custom_imagepath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0xff

    .line 122
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 47
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 50
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->isRainbow:Z

    .line 52
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mcurrentFocus:I

    .line 55
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    .line 57
    const/16 v0, 0xe

    new-array v0, v0, [I

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xfd

    const/16 v2, 0x2d

    invoke-static {v1, v5, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0x83

    const/16 v3, 0x5d

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 58
    const/16 v2, 0x3b

    const/16 v3, 0x5b

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x49

    const/16 v2, 0xc9

    invoke-static {v5, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0xca

    const/16 v3, 0x85

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x7a

    const/16 v3, 0x37

    const/16 v4, 0xde

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    .line 59
    const/16 v2, 0x94

    const/16 v3, 0x2e

    invoke-static {v7, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x38

    const/16 v3, 0xa8

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x33

    const/16 v3, 0x67

    const/16 v4, 0xfd

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa6

    const/16 v2, 0xa5

    const/16 v3, 0xa5

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0xb

    .line 60
    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    .line 61
    const/16 v0, 0xe

    new-array v0, v0, [I

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xfd

    const/16 v2, 0x2d

    invoke-static {v1, v5, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0x83

    const/16 v3, 0x5d

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 62
    const/16 v2, 0x3b

    const/16 v3, 0x5b

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x49

    const/16 v2, 0xc9

    invoke-static {v5, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0xca

    const/16 v3, 0x85

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x7a

    const/16 v3, 0x37

    const/16 v4, 0xde

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    .line 63
    const/16 v2, 0x94

    const/16 v3, 0x2e

    invoke-static {v7, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x38

    const/16 v3, 0xa8

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x33

    const/16 v3, 0x67

    const/16 v4, 0xfd

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa6

    const/16 v2, 0xa5

    const/16 v3, 0xa5

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0xb

    .line 64
    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    .line 65
    const/16 v0, 0xe

    new-array v0, v0, [I

    const/16 v1, 0xcd

    const/16 v2, 0xc1

    invoke-static {v5, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xf9

    const/16 v2, 0xa4

    const/16 v3, 0x8f

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0xf5

    const/16 v3, 0x7e

    const/16 v4, 0x60

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 66
    const/16 v2, 0x95

    const/16 v3, 0xa5

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf5

    const/16 v2, 0x4d

    const/16 v3, 0x67

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0xed

    const/16 v3, 0x1a

    const/16 v4, 0x3b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0xbc

    const/16 v3, 0x21

    invoke-static {v2, v8, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    .line 67
    const/16 v2, 0x8b

    const/16 v3, 0x7c

    invoke-static {v2, v6, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xbc

    const/16 v3, 0x5b

    invoke-static {v2, v8, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x8c

    const/16 v3, 0x53

    const/16 v4, 0x7a

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x59

    const/16 v2, 0x42

    const/16 v3, 0x54

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0xb

    .line 68
    const/16 v2, 0x66

    const/16 v3, 0x30

    invoke-static {v2, v3, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    .line 69
    const/16 v0, 0xe

    new-array v0, v0, [I

    const/16 v1, 0xfc

    const/16 v2, 0xe3

    const/16 v3, 0x3c

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xfa

    const/16 v2, 0xc7

    const/16 v3, 0x4b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0xcb

    const/16 v3, 0xe3

    const/16 v4, 0x6c

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 70
    const/16 v2, 0x5b

    const/16 v3, 0xdd

    const/16 v4, 0x38

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x41

    const/16 v2, 0xa5

    const/16 v3, 0x26

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0xe

    const/16 v3, 0x80

    const/16 v4, 0x19

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x66

    const/16 v3, 0x7f

    const/16 v4, 0x2a

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    .line 71
    const/16 v2, 0xe

    const/16 v3, 0x80

    const/16 v4, 0x64

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x13

    const/16 v3, 0x9b

    const/16 v4, 0x69

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/4 v2, 0x5

    const/16 v3, 0x59

    const/16 v4, 0x75

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x4b

    const/16 v3, 0x9d

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0xb

    .line 72
    const/16 v2, 0x29

    const/16 v3, 0x6c

    invoke-static {v9, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    .line 77
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "White, Tap to apply"

    aput-object v1, v0, v6

    const-string v1, "Yellow, Tap to apply"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    .line 78
    const-string v2, "Coral, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Tomato, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Hotpink, Tap to apply"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "Plum, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 79
    const-string v2, "Blueviolet, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Forestgreen, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Dodgeblue, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 80
    const-string v2, "Royalblue, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Darkgray, Tap to apply"

    aput-object v1, v0, v9

    const/16 v1, 0xb

    const-string v2, "Black, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 81
    const-string v2, "Custom colour palette, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Spuit, Button"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable1:[Ljava/lang/String;

    .line 82
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Peachpuff, Tap to apply"

    aput-object v1, v0, v6

    const-string v1, "Lightsalmon, Tap to apply"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    .line 83
    const-string v2, "Darksalmon, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "linghtpink, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Palevioletred, Tap to apply"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    .line 84
    const-string v2, "Crimson, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Red, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Mediumvioletred, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Purple, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 85
    const-string v2, "MediumOrchid, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Dimgray, Tap to apply"

    aput-object v1, v0, v9

    const/16 v1, 0xb

    const-string v2, "Saddlebrown, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 86
    const-string v2, "Custom colour palette, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Spuit, Button"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable2:[Ljava/lang/String;

    .line 87
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Gold, Tap to apply"

    aput-object v1, v0, v6

    const-string v1, "Orange, Tap to apply"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    .line 88
    const-string v2, "Greenyellow, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Limegreen, Tap to apply"

    aput-object v2, v0, v1

    const-string v1, "Seegreen, Tap to apply"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "Green, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 89
    const-string v2, "OliverDrab, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DarkCyan, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Teal, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Steelblue, Tap to apply"

    aput-object v2, v0, v1

    .line 90
    const-string v1, "Blue, Tap to apply"

    aput-object v1, v0, v9

    const/16 v1, 0xb

    const-string v2, "Darkblue, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Custom colour palette, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Spuit, Button"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable3:[Ljava/lang/String;

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    .line 109
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCustom_imagepath:Ljava/lang/String;

    .line 362
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mFirstExecuted:Z

    .line 123
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCustom_imagepath:Ljava/lang/String;

    .line 124
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCustom_imagepath:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 125
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->initView()V

    .line 126
    return-void
.end method

.method private drawSeleteBox(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 450
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 451
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    rem-int/lit8 v0, v2, 0x7

    .line 452
    .local v0, "m":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    div-int/lit8 v1, v2, 0x7

    .line 454
    .local v1, "n":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 455
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    add-int/2addr v3, v4

    mul-int/2addr v3, v0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPY:I

    add-int/2addr v4, v5

    mul-int/2addr v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 457
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 458
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 461
    .end local v0    # "m":I
    .end local v1    # "n":I
    :cond_0
    return-void
.end method

.method private getColorIdx(FF)I
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x2

    .line 579
    const/4 v0, 0x1

    .local v0, "m":I
    :goto_0
    if-le v0, v6, :cond_4

    .line 584
    :cond_0
    const/4 v1, 0x1

    .local v1, "n":I
    :goto_1
    if-le v1, v5, :cond_5

    .line 589
    :cond_1
    if-le v0, v6, :cond_2

    .line 590
    const/4 v0, 0x7

    .line 592
    :cond_2
    if-le v1, v5, :cond_3

    .line 593
    const/4 v1, 0x2

    .line 595
    :cond_3
    add-int/lit8 v2, v1, -0x1

    mul-int/lit8 v2, v2, 0x7

    add-int/lit8 v3, v0, -0x1

    add-int/2addr v2, v3

    return v2

    .line 580
    .end local v1    # "n":I
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    mul-int/2addr v2, v0

    add-int/lit8 v3, v0, -0x1

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, p1, v2

    if-lez v2, :cond_0

    .line 579
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 585
    .restart local v1    # "n":I
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/2addr v2, v1

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-ltz v2, :cond_1

    .line 584
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private initView()V
    .locals 3

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/16 v2, 0x26

    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPY:I

    .line 223
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    .line 226
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    .line 227
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 228
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 229
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 231
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    .line 232
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorchip_select_box"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    .line 234
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorchip_shadow"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 236
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorbox_mini"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    .line 238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_spoid_normal"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    .line 239
    return-void
.end method

.method private onDrawColorSet(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 408
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCanvasRect:Landroid/graphics/Rect;

    if-eqz v4, :cond_0

    .line 409
    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-direct {v3, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 411
    .local v3, "localRect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x2

    if-lt v1, v4, :cond_1

    .line 439
    .end local v1    # "i":I
    .end local v3    # "localRect":Landroid/graphics/Rect;
    :cond_0
    return-void

    .line 412
    .restart local v1    # "i":I
    .restart local v3    # "localRect":Landroid/graphics/Rect;
    :cond_1
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    const/4 v4, 0x7

    if-lt v2, v4, :cond_2

    .line 436
    iget v4, v3, Landroid/graphics/Rect;->left:I

    neg-int v4, v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v5

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPY:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 411
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 413
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    mul-int/lit8 v5, v1, 0x7

    add-int/2addr v5, v2

    aget v0, v4, v5

    .line 414
    .local v0, "color":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 415
    const v4, -0x1000002

    if-ne v0, v4, :cond_4

    .line 416
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->isRainbow:Z

    if-eqz v4, :cond_3

    .line 417
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 418
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 434
    :goto_2
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    add-int/2addr v4, v5

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 412
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 420
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 421
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 422
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 426
    :cond_4
    const v4, -0x1100001

    if-ne v0, v4, :cond_5

    .line 427
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 428
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 430
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 431
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 432
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 129
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCanvasRect:Landroid/graphics/Rect;

    .line 130
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    .line 131
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    .line 132
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    .line 133
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    .line 134
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    .line 136
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 138
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 141
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    .line 143
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_3

    .line 144
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 145
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 146
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 148
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 150
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_5

    .line 151
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 152
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    .line 153
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 155
    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    .line 158
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_7

    .line 159
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 160
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    .line 161
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 163
    :cond_6
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    .line 166
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_7
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCustom_imagepath:Ljava/lang/String;

    .line 167
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 168
    return-void
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 717
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    aget v0, v0, v1

    return v0
.end method

.method protected getCurrentTableIndex()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 373
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 374
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingLeft()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingTop()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 376
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mFirstExecuted:Z

    if-eqz v5, :cond_1

    .line 377
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 378
    .local v1, "localLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getWidth()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    div-int/lit8 v2, v5, 0x7

    .line 379
    .local v2, "m":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v5, v5

    int-to-float v5, v5

    const/high16 v6, 0x40e00000    # 7.0f

    div-float v4, v5, v6

    .line 380
    .local v4, "rowNumF":F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v5, v5

    div-int/lit8 v3, v5, 0x7

    .line 382
    .local v3, "rowNum":I
    int-to-float v5, v3

    cmpl-float v5, v4, v5

    if-lez v5, :cond_0

    .line 383
    add-int/lit8 v3, v3, 0x1

    .line 385
    :cond_0
    mul-int v5, v2, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 386
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 387
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mFirstExecuted:Z

    .line 391
    .end local v1    # "localLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "m":I
    .end local v3    # "rowNum":I
    .end local v4    # "rowNumF":F
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->onDrawColorSet(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :goto_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->drawSeleteBox(Landroid/graphics/Canvas;)V

    .line 396
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 397
    return-void

    .line 392
    :catch_0
    move-exception v0

    .line 393
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 631
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 632
    if-eqz p1, :cond_0

    .line 633
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mcurrentFocus:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 634
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    .line 640
    :goto_0
    return-void

    .line 636
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 637
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 527
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 529
    .local v0, "result":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 530
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 532
    .local v2, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 547
    :goto_0
    :pswitch_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mPreColorHoverIdx:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    if-eq v4, v5, :cond_1

    .line 548
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-le v4, v5, :cond_1

    .line 549
    const v4, 0x8000

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->sendAccessibilityEvent(I)V

    .line 550
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    if-ne v4, v3, :cond_2

    .line 551
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable1:[Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 557
    :cond_0
    :goto_1
    const/high16 v3, 0x10000

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->sendAccessibilityEvent(I)V

    .line 559
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mPreColorHoverIdx:I

    .line 562
    .end local v0    # "result":Z
    :cond_1
    :goto_2
    return v0

    .line 534
    .restart local v0    # "result":Z
    :pswitch_1
    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getColorIdx(FF)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    goto :goto_0

    .line 537
    :pswitch_2
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    .line 538
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mPreColorHoverIdx:I

    move v0, v3

    .line 539
    goto :goto_2

    .line 541
    :pswitch_3
    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getColorIdx(FF)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    goto :goto_0

    .line 552
    :cond_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 553
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable2:[Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 554
    :cond_3
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 555
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable3:[Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 532
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0xe

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 304
    sparse-switch p1, :sswitch_data_0

    .line 359
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 306
    :sswitch_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    if-ge v1, v2, :cond_0

    .line 309
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 310
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mcurrentFocus:I

    .line 311
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->playSoundEffect(I)V

    .line 313
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0

    .line 317
    :sswitch_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    if-ge v1, v2, :cond_0

    .line 320
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 321
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mcurrentFocus:I

    .line 322
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->playSoundEffect(I)V

    .line 324
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0

    .line 328
    :sswitch_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    if-ge v1, v2, :cond_0

    .line 331
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 332
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mcurrentFocus:I

    .line 333
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->playSoundEffect(I)V

    .line 335
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0

    .line 339
    :sswitch_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v2, :cond_0

    .line 342
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 343
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mcurrentFocus:I

    .line 344
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->playSoundEffect(I)V

    .line 346
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0

    .line 350
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    if-eqz v1, :cond_1

    .line 351
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    aget v2, v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;->colorChanged(II)V

    .line 354
    :cond_1
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->playSoundEffect(I)V

    goto/16 :goto_0

    .line 304
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x42 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onSizeChanged(IIII)V
    .locals 12
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 270
    invoke-super/range {p0 .. p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 272
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingLeft()I

    move-result v3

    .line 273
    .local v3, "mPaddingLeft":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingRight()I

    move-result v4

    .line 274
    .local v4, "mPaddingRight":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingTop()I

    move-result v5

    .line 275
    .local v5, "mPaddingTop":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingBottom()I

    move-result v2

    .line 277
    .local v2, "mPaddingBottom":I
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getLeft()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getTop()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getRight()I

    move-result v9

    sub-int/2addr v9, v4

    .line 278
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getBottom()I

    move-result v10

    sub-int/2addr v10, v2

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 277
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCanvasRect:Landroid/graphics/Rect;

    .line 281
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCanvasRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    mul-int/lit8 v7, v7, 0x6

    sub-int/2addr v6, v7

    div-int/lit8 v1, v6, 0x7

    .line 282
    .local v1, "itemW":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    if-eqz v6, :cond_0

    move v0, v1

    .line 283
    .local v0, "itemH":I
    :goto_0
    new-instance v6, Landroid/graphics/Rect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    .line 286
    new-instance v6, Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sget v9, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    .line 287
    sget v10, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    sget v11, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    add-int/2addr v10, v11

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 286
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    .line 288
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 289
    return-void

    .line 282
    .end local v0    # "itemH":I
    :cond_0
    add-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 473
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 474
    .local v1, "colorIdx":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 475
    .local v0, "action":I
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 476
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 479
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 491
    :goto_0
    const/16 v2, 0xc

    if-ne v1, v2, :cond_3

    .line 492
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 493
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 494
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->isRainbow:Z

    if-eqz v2, :cond_1

    .line 495
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->isRainbow:Z

    .line 504
    :cond_1
    :goto_1
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    .line 507
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    if-eqz v2, :cond_2

    .line 508
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 514
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    .line 516
    return v5

    .line 481
    :pswitch_0
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->playSoundEffect(I)V

    goto :goto_0

    .line 485
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getColorIdx(FF)I

    move-result v1

    .line 486
    goto :goto_0

    .line 499
    :cond_3
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 500
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mcurrentFocus:I

    goto :goto_1

    .line 510
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    aget v3, v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;->colorChanged(II)V

    goto :goto_2

    .line 479
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 508
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method protected setBackColorTable(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 195
    add-int/lit8 p1, p1, -0x1

    .line 196
    if-nez p1, :cond_0

    .line 197
    const/4 p1, 0x3

    .line 199
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    .line 200
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    packed-switch v0, :pswitch_data_0

    .line 211
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    .line 213
    return-void

    .line 202
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    .line 205
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    .line 208
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setColor(I)V
    .locals 6
    .param p1, "color"    # I

    .prologue
    const/4 v5, 0x1

    const/high16 v2, -0x1000000

    const/16 v4, 0xc

    .line 654
    or-int/2addr p1, v2

    .line 656
    and-int/2addr v2, p1

    const/high16 v3, -0x2000000

    if-eq v2, v3, :cond_6

    .line 657
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v2, v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "m":I
    :goto_0
    if-gez v1, :cond_1

    .line 664
    :goto_1
    if-gez v1, :cond_0

    .line 665
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v2, v2

    add-int/lit8 v0, v2, -0x2

    .local v0, "i":I
    :goto_2
    if-gez v0, :cond_3

    .line 671
    :goto_3
    if-gez v0, :cond_0

    .line 672
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 673
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aput p1, v2, v4

    .line 674
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    aput p1, v2, v4

    .line 675
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    aput p1, v2, v4

    .line 676
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    aput p1, v2, v4

    .line 677
    sput-boolean v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->IS_COLOR_GRADATION_SELECT:Z

    .line 689
    .end local v0    # "i":I
    .end local v1    # "m":I
    :cond_0
    :goto_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    .line 690
    return-void

    .line 658
    .restart local v1    # "m":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aget v2, v2, v1

    if-ne p1, v2, :cond_2

    .line 659
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 660
    const/4 v2, 0x0

    sput-boolean v2, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->IS_COLOR_GRADATION_SELECT:Z

    goto :goto_1

    .line 657
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 666
    .restart local v0    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    aget v2, v2, v0

    if-eq p1, v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    aget v2, v2, v0

    if-eq p1, v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    aget v2, v2, v0

    if-ne p1, v2, :cond_5

    .line 667
    :cond_4
    const/4 v2, -0x1

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    goto :goto_3

    .line 665
    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 681
    .end local v0    # "i":I
    .end local v1    # "m":I
    :cond_6
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 682
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aput p1, v2, v4

    .line 683
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    aput p1, v2, v4

    .line 684
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    aput p1, v2, v4

    .line 685
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    aput p1, v2, v4

    .line 686
    sput-boolean v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->IS_COLOR_GRADATION_SELECT:Z

    goto :goto_4
.end method

.method public setColorPickerColor(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    const/16 v2, 0xc

    const/high16 v1, -0x1000000

    .line 700
    and-int v0, p1, v1

    if-nez v0, :cond_0

    .line 701
    or-int/2addr p1, v1

    .line 704
    :cond_0
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 705
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aput p1, v0, v2

    .line 707
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    .line 708
    return-void
.end method

.method public setColorPickerMode()V
    .locals 1

    .prologue
    .line 722
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 723
    return-void
.end method

.method public setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;I)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;
    .param p2, "color"    # I

    .prologue
    .line 251
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    .line 252
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setSelectBoxPos(I)V

    .line 253
    return-void
.end method

.method protected setNextColorTable(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 172
    add-int/lit8 p1, p1, 0x1

    .line 173
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 174
    const/4 p1, 0x1

    .line 176
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    .line 177
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    packed-switch v0, :pswitch_data_0

    .line 188
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    .line 190
    const/16 v0, 0x105

    const/16 v1, 0x53

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getHeight()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->onSizeChanged(IIII)V

    .line 191
    return-void

    .line 179
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    .line 182
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    .line 185
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setSelectBoxPos(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    const/high16 v1, -0x1000000

    .line 607
    if-ne p1, v1, :cond_0

    .line 608
    const/high16 p1, -0x1000000

    .line 612
    :cond_0
    or-int/2addr p1, v1

    .line 613
    const/4 v0, 0x0

    .line 614
    .local v0, "m":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-gez v0, :cond_1

    .line 620
    :goto_1
    if-gez v0, :cond_3

    .line 621
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    .line 626
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    .line 627
    return-void

    .line 615
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_2

    .line 616
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    goto :goto_1

    .line 614
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 623
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mcurrentFocus:I

    goto :goto_2
.end method
