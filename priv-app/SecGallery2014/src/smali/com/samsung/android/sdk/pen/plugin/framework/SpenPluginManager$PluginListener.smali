.class public Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;
.super Ljava/lang/Object;
.source "SpenPluginManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PluginListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native native_Installed(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native native_Uninstalled(Ljava/lang/String;Ljava/lang/String;)V
.end method


# virtual methods
.method public onInstalled(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "pluginType"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;->native_Installed(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public onUninstalled(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "pluginType"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 175
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;->native_Uninstalled(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    return-void
.end method
