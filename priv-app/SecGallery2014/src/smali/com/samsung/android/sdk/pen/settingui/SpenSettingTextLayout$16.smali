.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    .line 972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x0

    .line 975
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 993
    :cond_0
    :goto_0
    return-void

    .line 978
    :cond_1
    invoke-virtual {p1, v5}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 979
    invoke-virtual {p1, v5}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 980
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    .line 979
    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 981
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$69(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 982
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-static {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$70(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    goto :goto_0

    .line 985
    :cond_2
    const/16 v2, 0x1e

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 987
    .local v0, "arrChange":[I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 988
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 989
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iput v5, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    .line 990
    aget v2, v0, p3

    int-to-float v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 991
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0

    .line 985
    nop

    :array_0
    .array-data 4
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
        0x16
        0x18
        0x1a
        0x1c
        0x1e
        0x20
        0x24
        0x28
        0x2b
        0x2c
        0x30
        0x34
        0x38
        0x3c
        0x40
        0x44
        0x48
        0x50
        0x58
    .end array-data
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 998
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
