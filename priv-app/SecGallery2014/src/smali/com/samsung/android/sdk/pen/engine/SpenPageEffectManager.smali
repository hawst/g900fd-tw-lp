.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
.super Ljava/lang/Object;
.source "SpenPageEffectHandler.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# instance fields
.field private mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

.field private mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    aput-object v1, v0, v3

    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;

    invoke-direct {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    aput-object v2, v0, v1

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 59
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v2

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->close()V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aput-object v1, v0, v2

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v3

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->close()V

    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aput-object v1, v0, v3

    .line 68
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 70
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 71
    return-void
.end method

.method public drawAnimation(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->drawAnimation(Landroid/graphics/Canvas;)V

    .line 128
    return-void
.end method

.method public isWorking()Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->isWorking()Z

    move-result v0

    return v0
.end method

.method public saveScreenshot()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->saveScreenshot()Z

    move-result v0

    return v0
.end method

.method public setCanvasInformation(IIII)V
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 98
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 101
    return-void

    .line 98
    :cond_0
    aget-object v0, v2, v1

    .line 99
    .local v0, "handler":Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setCanvasInformation(IIII)V

    .line 98
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setCanvasInformation1(IIII)V
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 105
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 108
    return-void

    .line 105
    :cond_0
    aget-object v0, v2, v1

    .line 106
    .local v0, "handler":Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setCanvasInformation1(IIII)V

    .line 105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 4
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 84
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 87
    return-void

    .line 84
    :cond_0
    aget-object v0, v2, v1

    .line 85
    .local v0, "handler":Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setPaint(Landroid/graphics/Paint;)V

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setScreenResolution(II)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 91
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 94
    return-void

    .line 91
    :cond_0
    aget-object v0, v2, v1

    .line 92
    .local v0, "handler":Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setScreenResolution(II)V

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 74
    if-gez p1, :cond_1

    .line 75
    const/4 p1, 0x0

    .line 79
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    .line 80
    return-void

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v0, v0

    if-lt p1, v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v0, v0

    add-int/lit8 p1, v0, -0x1

    goto :goto_0
.end method

.method public startAnimation(I)Z
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->startAnimation(I)Z

    move-result v0

    return v0
.end method
