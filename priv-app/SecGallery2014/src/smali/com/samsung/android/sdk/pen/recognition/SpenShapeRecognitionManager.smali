.class public final Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;
.super Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;
.source "SpenShapeRecognitionManager.java"


# static fields
.field public static final SPEN_SHAPE:Ljava/lang/String; = "com.samsung.android.sdk.pen.recognition.preload.ShapeRecognitionPlugin"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;-><init>(Landroid/content/Context;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    .line 38
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 319
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->close()V

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    .line 322
    return-void
.end method

.method public createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    move-result-object v0

    return-object v0
.end method

.method public createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 150
    :cond_0
    const-string v4, "ShapeRecognition"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->getInfoList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 151
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    if-nez v2, :cond_1

    .line 152
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v5, "There is no available ShapeRecognition engine"

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 155
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 158
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 173
    new-instance v4, Ljava/lang/ClassNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "The class \'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' is not founded"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 161
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    .line 163
    .local v3, "recognitionInfo":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    iget-object v4, p1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    iget-object v5, v3, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 165
    :try_start_0
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v6, v3, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->createPluginObject(Landroid/content/Context;Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    .line 166
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/reflect/UndeclaredThrowableException;
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v5, "ShapeRecognizer is not loaded"

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public createRecognition(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;
    .locals 8
    .param p1, "className"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 200
    if-nez p1, :cond_0

    .line 201
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "E_INVALID_ARG : parameter \'className\' is null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 204
    :cond_0
    const-string v4, "ShapeRecognition"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->getInfoList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 205
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    if-nez v2, :cond_1

    .line 206
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v5, "There is no available ShapeRecognition engine"

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 209
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 212
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 228
    new-instance v4, Ljava/lang/ClassNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "The class \'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' is not founded"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 215
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    .line 217
    .local v3, "recognitionInfo":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    iget-object v4, v3, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 219
    :try_start_0
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v3, v7}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->createPluginObject(Landroid/content/Context;Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    .line 220
    :catch_0
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/reflect/UndeclaredThrowableException;
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v5, "ShapeRecognizer is not loaded"

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public createRecognition(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;
    .locals 7
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 259
    if-nez p1, :cond_0

    .line 260
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "E_INVALID_ARG : parameter \'className\' is null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 263
    :cond_0
    const-string v4, "ShapeRecognition"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->getInfoList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 264
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    if-nez v2, :cond_1

    .line 265
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v5, "There is no available ShapeRecognition engine"

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 268
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 271
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 287
    new-instance v4, Ljava/lang/ClassNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "The class \'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' is not founded"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 274
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    .line 276
    .local v3, "recognitionInfo":Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    iget-object v4, v3, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 278
    :try_start_0
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v6, v3, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->createPluginObject(Landroid/content/Context;Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    .line 279
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Ljava/lang/reflect/UndeclaredThrowableException;
    new-instance v4, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string v5, "ShapeRecognizer is not loaded"

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public destroyRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;)V
    .locals 2
    .param p1, "recognition"    # Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    .prologue
    .line 306
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;->getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v0

    if-nez v0, :cond_1

    .line 307
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_STATE : parameter \'recognition\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;->getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->destroyPluginObject(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V

    .line 311
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 330
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->finalize()V

    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->mContext:Landroid/content/Context;

    .line 333
    return-void
.end method

.method public getInfoList(II)Ljava/util/List;
    .locals 1
    .param p1, "inputType"    # I
    .param p2, "outputType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    const-string v0, "ShapeRecognition"

    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->getInfoList(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPrivateKeyHint(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->getPrivateKeyHint(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
