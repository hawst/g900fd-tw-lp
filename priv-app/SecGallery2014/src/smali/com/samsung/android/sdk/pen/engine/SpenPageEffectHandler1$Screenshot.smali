.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;
.super Ljava/lang/Object;
.source "SpenPageEffectHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Screenshot"
.end annotation


# instance fields
.field public bmp:Landroid/graphics/Bitmap;

.field public dst:Landroid/graphics/Rect;

.field public src:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)V
    .locals 1

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    .line 146
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    .line 147
    return-void
.end method


# virtual methods
.method public clean()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    .line 192
    return-void
.end method

.method public saveScreenshot()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 151
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth0:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight0:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 160
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 161
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer2(Landroid/graphics/Canvas;)V

    .line 162
    const/4 v0, 0x0

    .line 164
    const/4 v2, 0x1

    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :goto_0
    return v2

    .line 152
    :catch_0
    move-exception v1

    .line 153
    .local v1, "e":Ljava/lang/Exception;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 155
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 156
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public saveScreenshot1()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 169
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth1:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight1:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 178
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 179
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer2(Landroid/graphics/Canvas;)V

    .line 180
    const/4 v0, 0x0

    .line 182
    const/4 v2, 0x1

    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :goto_0
    return v2

    .line 170
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e":Ljava/lang/Exception;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 173
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 174
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    goto :goto_0
.end method
