.class Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;
.super Ljava/lang/Object;
.source "SpenSurfaceView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HolderCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;)V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 269
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEditableTextBox()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 280
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 284
    return-void
.end method
