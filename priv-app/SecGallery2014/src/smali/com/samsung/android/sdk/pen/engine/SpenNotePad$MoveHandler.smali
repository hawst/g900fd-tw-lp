.class Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;
.super Landroid/os/Handler;
.source "SpenNotePad.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MoveHandler"
.end annotation


# static fields
.field private static final MOVING_DELAY:I = 0x258

.field private static final MOVING_MESSAGE:I = 0x1


# instance fields
.field private mIsMoving:Z

.field private mLastX:F

.field private final mSpenNotePad:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenNotePad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V
    .locals 1
    .param p1, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .prologue
    .line 2296
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2291
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    .line 2297
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mSpenNotePad:Ljava/lang/ref/WeakReference;

    .line 2298
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v9, 0x8

    const/high16 v8, 0x41200000    # 10.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2317
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v5, :cond_0

    .line 2318
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mSpenNotePad:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 2319
    .local v1, "notePad":Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    if-eqz v1, :cond_0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2351
    .end local v1    # "notePad":Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    :cond_0
    :goto_0
    return-void

    .line 2322
    .restart local v1    # "notePad":Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->refresh()V

    .line 2323
    invoke-static {v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;I)V

    .line 2324
    iget-boolean v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isArabic:Z

    if-eqz v2, :cond_3

    .line 2325
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    mul-float/2addr v3, v7

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 2326
    invoke-virtual {v1, v9, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setButtonState(IZ)V

    goto :goto_0

    .line 2329
    :cond_2
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    .line 2330
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    sub-float/2addr v3, v4

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v4, v8

    sub-float/2addr v3, v4

    .line 2331
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v4

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v5

    mul-float/2addr v4, v5

    .line 2330
    mul-float/2addr v3, v4

    .line 2331
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 2330
    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    .line 2329
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V

    .line 2343
    :goto_1
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    .line 2344
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v3

    int-to-float v3, v3

    sub-float v0, v2, v3

    .line 2345
    .local v0, "dis":F
    const-string v2, "ZoomPad"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "move panning"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2346
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 2347
    invoke-virtual {v1, v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 2348
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 2349
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto/16 :goto_0

    .line 2333
    .end local v0    # "dis":F
    :cond_3
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 2334
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mOnePT:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    mul-float/2addr v3, v7

    .line 2333
    add-float/2addr v2, v3

    .line 2334
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenStartX:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_4

    .line 2335
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mMaxDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    .line 2336
    invoke-virtual {v1, v9, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setButtonState(IZ)V

    goto/16 :goto_0

    .line 2339
    :cond_4
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v4, v8

    sub-float/2addr v3, v4

    .line 2340
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v4

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v5

    mul-float/2addr v4, v5

    mul-float/2addr v3, v4

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 2339
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V

    goto/16 :goto_1
.end method

.method public isMovingEnabled()Z
    .locals 1

    .prologue
    .line 2312
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    return v0
.end method

.method public setMovingEnabled(ZF)V
    .locals 1
    .param p1, "enable"    # Z
    .param p2, "x"    # F

    .prologue
    .line 2301
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    .line 2302
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mIsMoving:Z

    if-eqz v0, :cond_1

    .line 2303
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    cmpg-float v0, v0, p2

    if-gez v0, :cond_0

    .line 2304
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    .line 2309
    :cond_0
    :goto_0
    return-void

    .line 2307
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$MoveHandler;->mLastX:F

    goto :goto_0
.end method
