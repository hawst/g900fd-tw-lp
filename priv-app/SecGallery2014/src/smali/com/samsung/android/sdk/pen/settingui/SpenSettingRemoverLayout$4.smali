.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;
.super Ljava/lang/Object;
.source "SpenSettingRemoverLayout.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 425
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 426
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v0

    .line 427
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    if-eqz v0, :cond_1

    .line 428
    if-eqz p2, :cond_2

    .line 429
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    iput v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 430
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V

    .line 431
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->playSoundEffect(I)V

    .line 442
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 445
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    :cond_1
    return-void

    .line 435
    .restart local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v1

    iput v2, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 436
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setEnableSizeSeekbar(Z)V
    invoke-static {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;Z)V

    .line 437
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mIsShowing:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 438
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->playSoundEffect(I)V

    goto :goto_0
.end method
