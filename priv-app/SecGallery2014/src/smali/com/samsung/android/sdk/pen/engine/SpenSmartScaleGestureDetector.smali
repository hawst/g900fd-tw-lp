.class Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
.super Landroid/os/Handler;
.source "SpenSmartScaleGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffect2;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I = null

.field private static final DBG:Z = false

.field private static final HANDLER_TIME:I = 0x0

.field private static final HOLD_FINGER_MAX_DISTANCE:F = 0.5f

.field private static final HOLD_FINGER_MAX_TIME:I = 0x5dc

.field private static final HOLD_HOVER_MAX_TIME:I = 0xc8

.field private static final HOVER_ICON_BOTTOM:I = 0x7

.field private static final HOVER_ICON_BOTTOM_LEFT:I = 0xc

.field private static final HOVER_ICON_LEFT:I = 0x5

.field private static final HOVER_ICON_LEFT_TOP:I = 0x6

.field private static final HOVER_ICON_RIGHT:I = 0x3

.field private static final HOVER_ICON_RIGHT_BOTTOM:I = 0xa

.field private static final HOVER_ICON_TOP:I = 0x1

.field private static final HOVER_ICON_TOP_RIGHT:I = 0x4


# instance fields
.field private final m1CMPixel:F

.field private mBottomScrollRegion:Landroid/graphics/Rect;

.field private mCenterX:F

.field private mCenterY:F

.field private mCurrentMotionEvent:Landroid/view/MotionEvent;

.field private final mDPI:F

.field private mDeltaX:F

.field private mDeltaY:F

.field private final mDensity:F

.field private mDiffX:F

.field private mDiffY:F

.field private mDistanceX:F

.field private mDistanceY:F

.field private mDownTime:J

.field private mDownX:F

.field private mDownY:F

.field private mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

.field private mEdgePaint:Landroid/graphics/Paint;

.field private mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

.field private mEffectFrame:I

.field private mFactor:F

.field private mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

.field private mHoldHoverStartTimeLR:J

.field private mHoldHoverStartTimeTB:J

.field private mHoldStartTime:J

.field private mHorizontalEnterTime:J

.field private mHorizontalResponseTime:I

.field private mHorizontalVelocity:I

.field private mHoverIconType:I

.field private mIsMultiTouch:Z

.field private mIsReleaseEdgeEffectLR:Z

.field private mIsReleaseEdgeEffectTB:Z

.field private mIsReleaseHoverEdgeEffectLR:Z

.field private mIsReleaseHoverEdgeEffectTB:Z

.field private mIsUpdateEEAnimation:Z

.field private mIsUpdateFloating:Z

.field private mIsUpdateFrameBuffer:Z

.field private mLeftScrollRegion:Landroid/graphics/Rect;

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mOrigRatio:F

.field private mParentRect:Landroid/graphics/Rect;

.field private mPreEventX:F

.field private mPreEventY:F

.field private mRatio:F

.field private mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

.field private mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

.field private mRightScrollRegion:Landroid/graphics/Rect;

.field private mScrollX:F

.field private mScrollY:F

.field private mSmartScaleRegion:Landroid/graphics/Rect;

.field private mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

.field private mToolAndActionMap:Landroid/util/SparseIntArray;

.field private mTopScrollRegion:Landroid/graphics/Rect;

.field private mUseHorizontalScroll:Z

.field private mUseSmartScale:Z

.field private mUseVerticalScroll:Z

.field private mVerticalEnterTime:J

.field private mVerticalResponseTime:I

.field private mVerticalVelocity:I

.field private mZoomOutResponseTime:I

.field private mZoomRatio:F


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State()[I
    .locals 3

    .prologue
    .line 17
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->values()[Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;FF)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dpi"    # F
    .param p3, "density"    # F

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 200
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 120
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 134
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 135
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    .line 136
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    .line 141
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    .line 142
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    .line 143
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    .line 144
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    .line 145
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    .line 146
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    .line 147
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    .line 148
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    .line 151
    const/high16 v0, 0x3e000000    # 0.125f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    .line 152
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    .line 153
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    .line 154
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    .line 155
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    .line 156
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    .line 157
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    .line 158
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    .line 160
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    .line 161
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    .line 162
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventX:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventY:F

    .line 165
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    .line 171
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    .line 173
    new-instance v0, Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    .line 181
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    .line 182
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    .line 184
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 185
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    .line 186
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsMultiTouch:Z

    .line 187
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 189
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    .line 190
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    .line 191
    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 192
    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 193
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    .line 194
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    .line 195
    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    .line 201
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDensity:F

    .line 202
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    .line 203
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    const v1, 0x3ec9932d

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    .line 205
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    .line 206
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    .line 208
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    .line 209
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 210
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 212
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    .line 221
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    .line 230
    return-void
.end method

.method private Fling()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x42c80000    # 100.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const v6, 0x3e19999a    # 0.15f

    const/high16 v5, -0x3d380000    # -100.0f

    .line 1355
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    mul-float v0, v4, v6

    .line 1356
    .local v0, "currentDistanceX":F
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    mul-float v1, v4, v6

    .line 1358
    .local v1, "currentDistanceY":F
    cmpg-float v4, v0, v5

    if-gez v4, :cond_3

    .line 1359
    const/high16 v0, -0x3d380000    # -100.0f

    .line 1364
    :cond_0
    :goto_0
    cmpg-float v4, v1, v5

    if-gez v4, :cond_4

    .line 1365
    const/high16 v1, -0x3d380000    # -100.0f

    .line 1370
    :cond_1
    :goto_1
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v4, v0

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    .line 1371
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v4, v1

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    .line 1373
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    sub-float v2, v4, v0

    .line 1374
    .local v2, "deltaX":F
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    sub-float v3, v4, v1

    .line 1376
    .local v3, "deltaY":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v7

    if-gez v4, :cond_6

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v7

    if-gez v4, :cond_6

    .line 1377
    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1378
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1379
    float-to-int v4, v2

    int-to-float v4, v4

    float-to-int v5, v3

    int-to-float v5, v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangePan(FF)V

    .line 1380
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v4, :cond_2

    .line 1381
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    .line 1393
    :cond_2
    :goto_2
    return-void

    .line 1360
    .end local v2    # "deltaX":F
    .end local v3    # "deltaY":F
    :cond_3
    cmpl-float v4, v0, v8

    if-lez v4, :cond_0

    .line 1361
    const/high16 v0, 0x42c80000    # 100.0f

    goto :goto_0

    .line 1366
    :cond_4
    cmpl-float v4, v1, v8

    if-lez v4, :cond_1

    .line 1367
    const/high16 v1, 0x42c80000    # 100.0f

    goto :goto_1

    .line 1384
    .restart local v2    # "deltaX":F
    .restart local v3    # "deltaY":F
    :cond_5
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1385
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v4, :cond_2

    .line 1386
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v4, v9}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto :goto_2

    .line 1390
    :cond_6
    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangePan(FF)V

    .line 1391
    const-wide/16 v4, 0x0

    invoke-virtual {p0, v9, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2
.end method

.method private ReadyForZoomout()V
    .locals 2

    .prologue
    .line 1314
    const-string v0, "SPen_Library"

    const-string v1, "[SMART SCALE] READY FOR ZOOM OUT()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1316
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomOut()V

    .line 1317
    return-void
.end method

.method private Scroll()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1232
    const/4 v2, 0x1

    .line 1233
    .local v2, "sendMessage":Z
    const/4 v3, 0x0

    .line 1235
    .local v3, "setPan":Z
    const/4 v0, 0x0

    .line 1236
    .local v0, "dx":I
    const/4 v1, 0x0

    .line 1238
    .local v1, "dy":I
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_0

    .line 1239
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1243
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    sub-int/2addr v1, v4

    .line 1245
    :cond_0
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_1

    .line 1246
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1250
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    add-int/2addr v1, v4

    .line 1252
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_2

    .line 1253
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1257
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    sub-int/2addr v0, v4

    .line 1259
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 1260
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1264
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    add-int/2addr v0, v4

    .line 1267
    :cond_3
    if-eqz v0, :cond_5

    .line 1271
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    int-to-float v5, v0

    add-float/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    .line 1272
    const/4 v2, 0x1

    .line 1273
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpg-float v4, v4, v7

    if-gez v4, :cond_9

    .line 1274
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    .line 1275
    const/4 v2, 0x0

    .line 1280
    :cond_4
    :goto_0
    const/4 v3, 0x1

    .line 1283
    :cond_5
    if-eqz v1, :cond_7

    .line 1287
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    int-to-float v5, v1

    add-float/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    .line 1288
    const/4 v2, 0x1

    .line 1289
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpg-float v4, v4, v7

    if-gez v4, :cond_a

    .line 1290
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    .line 1291
    const/4 v2, 0x0

    .line 1296
    :cond_6
    :goto_1
    const/4 v3, 0x1

    .line 1299
    :cond_7
    if-eqz v3, :cond_8

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v4, :cond_8

    .line 1303
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangePan(FF)V

    .line 1306
    :cond_8
    if-eqz v2, :cond_b

    .line 1307
    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-virtual {p0, v4, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    .line 1311
    :goto_2
    return-void

    .line 1276
    :cond_9
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 1277
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    .line 1278
    const/4 v2, 0x0

    goto :goto_0

    .line 1292
    :cond_a
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_6

    .line 1293
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    .line 1294
    const/4 v2, 0x0

    goto :goto_1

    .line 1309
    :cond_b
    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    goto :goto_2
.end method

.method private ZoomIn()V
    .locals 4

    .prologue
    .line 1320
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 1321
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1322
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_0

    .line 1323
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 1324
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    .line 1325
    const-string v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SMART SCALE] ZOOM IN(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    :cond_0
    :goto_0
    return-void

    .line 1328
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1329
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 1330
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_2

    .line 1331
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 1333
    :cond_2
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    .line 1334
    const-string v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SMART SCALE] ZOOM IN(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ZoomOut()V
    .locals 4

    .prologue
    .line 1339
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 1340
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1341
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 1342
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1344
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_1

    .line 1345
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 1346
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    .line 1351
    :goto_0
    const-string v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SMART SCALE] ZOOM OUT(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1352
    return-void

    .line 1348
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 1349
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    return-object v0
.end method

.method private calculateEdgeEffect(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 916
    if-nez p1, :cond_1

    .line 1099
    :cond_0
    :goto_0
    return-void

    .line 919
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-lez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-lez v6, :cond_0

    .line 922
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 923
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 924
    .local v1, "eventX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 925
    .local v2, "eventY":F
    const/4 v4, 0x0

    .line 926
    .local v4, "pullLR":F
    const/4 v5, 0x0

    .line 927
    .local v5, "pullTB":F
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 928
    const/16 v6, 0xa

    if-eq v0, v6, :cond_26

    .line 929
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-boolean v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    if-eqz v6, :cond_16

    .line 930
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v6, v7, :cond_11

    .line 931
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_f

    .line 932
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_e

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 933
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 934
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput v1, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    .line 984
    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-boolean v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    if-eqz v6, :cond_1f

    .line 985
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v6, v7, :cond_1a

    .line 986
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_18

    .line 987
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_17

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 988
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 989
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput v2, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    .line 1038
    :cond_3
    :goto_2
    const/4 v6, 0x0

    cmpl-float v6, v4, v6

    if-lez v6, :cond_22

    .line 1039
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_20

    .line 1040
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 1055
    :cond_4
    :goto_3
    const/4 v6, 0x0

    cmpl-float v6, v5, v6

    if-lez v6, :cond_25

    .line 1056
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_23

    .line 1057
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 1071
    :cond_5
    :goto_4
    const/4 v3, 0x0

    .line 1072
    .local v3, "isPull":Z
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    if-nez v6, :cond_6

    const/4 v6, 0x0

    cmpl-float v6, v4, v6

    if-lez v6, :cond_6

    .line 1073
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 1074
    const/4 v3, 0x1

    .line 1076
    :cond_6
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    if-nez v6, :cond_7

    const/4 v6, 0x0

    cmpl-float v6, v5, v6

    if-lez v6, :cond_7

    .line 1077
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, v5}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 1078
    const/4 v3, 0x1

    .line 1080
    :cond_7
    if-nez v3, :cond_d

    .line 1081
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_a

    .line 1082
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_9

    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    .line 1083
    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1084
    :cond_9
    const/16 v6, 0xa

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1086
    :cond_a
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_d

    .line 1087
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_c

    :cond_b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    .line 1088
    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1089
    :cond_c
    const/16 v6, 0xa

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1097
    .end local v3    # "isPull":Z
    :cond_d
    :goto_5
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setHoverIcon(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 935
    :cond_e
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    .line 936
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 937
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_1

    .line 940
    :cond_f
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_2

    .line 941
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_10

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 942
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 943
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput v1, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    goto/16 :goto_1

    .line 944
    :cond_10
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    .line 945
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 946
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_1

    .line 949
    :cond_11
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v6, v7, :cond_13

    .line 950
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_13

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 951
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    if-lez v6, :cond_12

    .line 952
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 953
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 954
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 955
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    goto/16 :goto_1

    .line 957
    :cond_12
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    sub-float v6, v1, v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    div-float v4, v6, v7

    .line 958
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 959
    float-to-double v6, v4

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_2

    .line 960
    const v4, 0x3dcccccd    # 0.1f

    .line 963
    goto/16 :goto_1

    :cond_13
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v6, v7, :cond_15

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_15

    .line 964
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 965
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_14

    .line 966
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 967
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 968
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 969
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    goto/16 :goto_1

    .line 971
    :cond_14
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    sub-float/2addr v6, v1

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    div-float v4, v6, v7

    .line 972
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 973
    float-to-double v6, v4

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_2

    .line 974
    const v4, 0x3dcccccd    # 0.1f

    .line 977
    goto/16 :goto_1

    .line 978
    :cond_15
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    goto/16 :goto_1

    .line 981
    :cond_16
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    goto/16 :goto_1

    .line 990
    :cond_17
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_3

    .line 991
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 992
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_2

    .line 994
    :cond_18
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_3

    .line 995
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_19

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 996
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 997
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput v2, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    goto/16 :goto_2

    .line 998
    :cond_19
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_3

    .line 999
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1000
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_2

    .line 1003
    :cond_1a
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v6, v7, :cond_1c

    .line 1004
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_1c

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 1005
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    if-lez v6, :cond_1b

    .line 1006
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 1007
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1008
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1009
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    goto/16 :goto_2

    .line 1011
    :cond_1b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    sub-float v6, v2, v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    div-float v5, v6, v7

    .line 1012
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 1013
    float-to-double v6, v5

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_3

    .line 1014
    const v5, 0x3dcccccd    # 0.1f

    .line 1017
    goto/16 :goto_2

    :cond_1c
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v6, v7, :cond_1e

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_1e

    .line 1018
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 1019
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1d

    .line 1020
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 1021
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1022
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1023
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    goto/16 :goto_2

    .line 1025
    :cond_1d
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    sub-float/2addr v6, v2

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    div-float v5, v6, v7

    .line 1026
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 1027
    float-to-double v6, v5

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_3

    .line 1028
    const v5, 0x3dcccccd    # 0.1f

    .line 1031
    goto/16 :goto_2

    .line 1032
    :cond_1e
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_2

    .line 1035
    :cond_1f
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_2

    .line 1042
    :cond_20
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0xc8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_4

    .line 1043
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    if-nez v6, :cond_21

    .line 1044
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1045
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1047
    :cond_21
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    goto/16 :goto_3

    .line 1051
    :cond_22
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 1052
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    goto/16 :goto_3

    .line 1059
    :cond_23
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0xc8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_5

    .line 1060
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    if-nez v6, :cond_24

    .line 1061
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1062
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1064
    :cond_24
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    goto/16 :goto_4

    .line 1068
    :cond_25
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 1069
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    goto/16 :goto_4

    .line 1093
    :cond_26
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1094
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1095
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_5
.end method

.method private getHoverIconId(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x0

    .line 836
    const/4 v1, 0x0

    .line 837
    .local v1, "type":I
    const/4 v0, 0x0

    .line 838
    .local v0, "pos":I
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 839
    add-int/lit8 v0, v0, 0x5

    .line 841
    :cond_0
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 842
    add-int/lit8 v0, v0, 0x1

    .line 844
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 845
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 846
    add-int/lit8 v0, v0, 0x3

    .line 848
    :cond_2
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 849
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 850
    add-int/lit8 v0, v0, 0x7

    .line 852
    :cond_3
    if-nez v0, :cond_4

    move v2, v1

    .line 882
    .end local v1    # "type":I
    .local v2, "type":I
    :goto_0
    return v2

    .line 856
    .end local v2    # "type":I
    .restart local v1    # "type":I
    :cond_4
    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v2, v1

    .line 882
    .end local v1    # "type":I
    .restart local v2    # "type":I
    goto :goto_0

    .line 858
    .end local v2    # "type":I
    .restart local v1    # "type":I
    :pswitch_1
    const/16 v1, 0x11

    .line 859
    goto :goto_1

    .line 861
    :pswitch_2
    const/16 v1, 0xb

    .line 862
    goto :goto_1

    .line 864
    :pswitch_3
    const/16 v1, 0xd

    .line 865
    goto :goto_1

    .line 867
    :pswitch_4
    const/16 v1, 0xf

    .line 868
    goto :goto_1

    .line 870
    :pswitch_5
    const/16 v1, 0x12

    .line 871
    goto :goto_1

    .line 873
    :pswitch_6
    const/16 v1, 0xc

    .line 874
    goto :goto_1

    .line 876
    :pswitch_7
    const/16 v1, 0xe

    .line 877
    goto :goto_1

    .line 879
    :pswitch_8
    const/16 v1, 0x10

    goto :goto_1

    .line 856
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method private isLollipopOS()Z
    .locals 2

    .prologue
    .line 1396
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1397
    const/4 v0, 0x1

    .line 1399
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onChangePan(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v1, 0x0

    .line 1211
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_0

    .line 1212
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 1213
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    .line 1214
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1215
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangePan(FF)V

    .line 1217
    :cond_0
    return-void
.end method

.method private onChangeScale(FFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "ratio"    # F

    .prologue
    const/4 v1, 0x0

    .line 1220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_0

    .line 1221
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 1222
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    .line 1223
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1224
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangeScale(FFF)V

    .line 1226
    :cond_0
    return-void
.end method

.method private releaseEdgeEffects()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1102
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1104
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1106
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1107
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1108
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1110
    :cond_1
    return-void
.end method

.method private setHoverIcon(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 886
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-nez v2, :cond_0

    .line 903
    :goto_0
    return-void

    .line 890
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_1

    .line 891
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getHoverIconId(II)I

    move-result v1

    .line 896
    .local v1, "type":I
    :goto_1
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    invoke-static {v2, v1}, Landroid/view/PointerIcon;->setIcon(II)V

    .line 897
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoverIconType:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 898
    :catch_0
    move-exception v0

    .line 899
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 893
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "type":I
    :cond_1
    const/4 v1, 0x1

    .restart local v1    # "type":I
    goto :goto_1

    .line 900
    :catch_1
    move-exception v0

    .line 901
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v2, "SPen_Library"

    const-string v3, "Device OS not support hover icon"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 235
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->close()V

    .line 239
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->close()V

    .line 243
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    .line 245
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 246
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 248
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    if-eqz v0, :cond_4

    .line 249
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 251
    :cond_4
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    .line 252
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 306
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    if-nez v2, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    const/4 v0, 0x0

    .line 311
    .local v0, "isUpdate":Z
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v2, v3, :cond_7

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_7

    .line 312
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 313
    .local v1, "restoreCount":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v4, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 314
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 315
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 316
    const/4 v0, 0x1

    .line 318
    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 328
    .end local v1    # "restoreCount":I
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v2, v3, :cond_9

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_9

    .line 329
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 330
    .restart local v1    # "restoreCount":I
    invoke-virtual {p1, v4, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 331
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 332
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 333
    const/4 v0, 0x1

    .line 335
    :cond_4
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 345
    .end local v1    # "restoreCount":I
    :cond_5
    :goto_2
    if-nez v0, :cond_6

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCurrentMotionEvent:Landroid/view/MotionEvent;

    if-eqz v2, :cond_6

    .line 346
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCurrentMotionEvent:Landroid/view/MotionEvent;

    invoke-virtual {v3, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    if-ne v2, v6, :cond_6

    .line 347
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCurrentMotionEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v6, :cond_6

    .line 348
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCurrentMotionEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    .line 349
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 353
    :cond_6
    if-eqz v0, :cond_b

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    if-eqz v2, :cond_b

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    if-eqz v2, :cond_b

    .line 354
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 355
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 356
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 319
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_3

    .line 320
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 321
    .restart local v1    # "restoreCount":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 322
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 323
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 324
    const/4 v0, 0x1

    .line 326
    :cond_8
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_1

    .line 336
    .end local v1    # "restoreCount":I
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_5

    .line 337
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 338
    .restart local v1    # "restoreCount":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 339
    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 340
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 341
    const/4 v0, 0x1

    .line 343
    :cond_a
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_2

    .line 357
    .end local v1    # "restoreCount":I
    :cond_b
    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    if-eqz v2, :cond_0

    .line 358
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 359
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->post(Ljava/lang/Runnable;)Z

    .line 360
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    goto/16 :goto_0
.end method

.method public enableHorizontalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 0
    .param p1, "use"    # Z
    .param p2, "leftScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "rightScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 289
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    .line 290
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    .line 291
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    .line 292
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    .line 293
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    .line 294
    return-void
.end method

.method public enableSmartScale(ZLandroid/graphics/Rect;IIF)V
    .locals 3
    .param p1, "use"    # Z
    .param p2, "region"    # Landroid/graphics/Rect;
    .param p3, "effectFrame"    # I
    .param p4, "zoomOutResponseTime"    # I
    .param p5, "zoomRatio"    # F

    .prologue
    .line 263
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-nez v0, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    const-string v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SMART SCALE] enableSmartScale use : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    .line 270
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    .line 271
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEffectFrame:I

    .line 272
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    .line 273
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    .line 275
    if-nez p1, :cond_0

    .line 276
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v0, v1, :cond_0

    .line 277
    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 278
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    goto :goto_0
.end method

.method public enableVerticalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 0
    .param p1, "use"    # Z
    .param p2, "topScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "bottomScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 298
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    .line 299
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    .line 300
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    .line 301
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    .line 302
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    .line 303
    return-void
.end method

.method public getCenterX()F
    .locals 1

    .prologue
    .line 1199
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    return v0
.end method

.method public getCenterY()F
    .locals 1

    .prologue
    .line 1203
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    return v0
.end method

.method public getState()Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;
    .locals 1

    .prologue
    .line 1195
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 366
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State()[I

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 408
    :goto_0
    :pswitch_0
    return-void

    .line 376
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomIn()V

    goto :goto_0

    .line 382
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Scroll()V

    goto :goto_0

    .line 394
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ReadyForZoomout()V

    goto :goto_0

    .line 400
    :pswitch_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomOut()V

    goto :goto_0

    .line 403
    :pswitch_5
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Fling()V

    goto :goto_0

    .line 366
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method protected isEdgeEffectWorking()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 906
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 912
    :cond_0
    :goto_0
    return v0

    .line 909
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 912
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isUpdateFloating()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v5, 0x40800000    # 4.0f

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 1113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_6

    .line 1114
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    mul-float/2addr v0, v5

    div-float v0, p3, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    .line 1115
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    mul-float/2addr v0, v5

    div-float v0, p4, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    .line 1117
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_4

    .line 1118
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    .line 1123
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_5

    .line 1124
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    .line 1129
    :cond_1
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    .line 1130
    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1131
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Fling()V

    .line 1142
    :cond_3
    :goto_2
    return-void

    .line 1119
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1120
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    goto :goto_0

    .line 1125
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1126
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    goto :goto_1

    .line 1134
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_3

    .line 1135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_7

    .line 1136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onFlick(I)Z

    goto :goto_2

    .line 1137
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_3

    .line 1138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onFlick(I)Z

    goto :goto_2
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)V
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x2

    const/high16 v12, 0x3f800000    # 1.0f

    const-wide/16 v10, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 411
    if-nez p1, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 415
    .local v0, "action":I
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 416
    const/16 v3, 0x9

    if-ne v0, v3, :cond_2

    .line 417
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    .line 418
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    .line 419
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 420
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 422
    :cond_2
    const/16 v3, 0x9

    if-ne v0, v3, :cond_a

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-eqz v3, :cond_a

    .line 423
    const-string v3, "SPen_Library"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[SMART SCALE] ON HOVER ENTER. STATE : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_3

    .line 426
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeMessages(I)V

    .line 428
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_4

    .line 429
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_0

    .line 430
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v3

    if-ne v3, v6, :cond_5

    .line 431
    const-string v3, "SPen_Library"

    const-string v4, "[SMART SCALE] ON HOVER ENTER. BUTTON_SECONDARY"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 435
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    .line 436
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    .line 438
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_7

    .line 439
    :cond_6
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    .line 442
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 443
    const-string v3, "SPen_Library"

    const-string v4, "[SMART SCALE] ON HOVER ENTER. SMART REGION CONTAINS"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_8

    .line 445
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEffectFrame:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    .line 449
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomIn()V

    goto/16 :goto_0

    .line 447
    :cond_8
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    goto :goto_1

    .line 451
    :cond_9
    const-string v3, "SPen_Library"

    const-string v4, "[SMART SCALE] ON HOVER ENTER. SMART REGION NOT CONTAINS!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 454
    :cond_a
    const/16 v3, 0xa

    if-eq v0, v3, :cond_1f

    .line 455
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_1f

    .line 456
    :cond_b
    const/4 v2, 0x0

    .line 457
    .local v2, "isScroll":Z
    const/4 v1, 0x0

    .line 458
    .local v1, "isEdgeEffect":Z
    const/16 v3, 0x9

    if-eq v0, v3, :cond_0

    .line 460
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    .line 461
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    .line 462
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    .line 463
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    .line 462
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 463
    if-nez v3, :cond_e

    :cond_c
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    .line 464
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    .line 463
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 464
    if-nez v3, :cond_e

    .line 465
    :cond_d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    .line 467
    :cond_e
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_f

    .line 468
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_11

    :cond_f
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    .line 469
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_11

    .line 470
    :cond_10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    .line 472
    :cond_11
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_12

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_12

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_0

    .line 473
    :cond_12
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_17

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    sub-long/2addr v4, v6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_17

    .line 474
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_17

    .line 475
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 476
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v3, v3, v12

    if-lez v3, :cond_16

    .line 477
    const/4 v2, 0x1

    .line 491
    :cond_13
    :goto_2
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_1a

    .line 492
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    sub-long/2addr v4, v6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_1a

    .line 493
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_1a

    .line 494
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 495
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v3, v3, v12

    if-lez v3, :cond_19

    .line 496
    const/4 v2, 0x1

    .line 513
    :cond_14
    :goto_3
    if-nez v2, :cond_15

    .line 514
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 516
    :cond_15
    if-eqz v2, :cond_1c

    .line 517
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setHoverIcon(Landroid/view/MotionEvent;)V

    .line 518
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 519
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Scroll()V

    .line 520
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    .line 521
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    .line 522
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 523
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    goto/16 :goto_0

    .line 479
    :cond_16
    const/4 v1, 0x1

    .line 481
    goto :goto_2

    :cond_17
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_13

    .line 482
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    sub-long/2addr v4, v6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_13

    .line 483
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_13

    .line 484
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 485
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    sub-float/2addr v4, v12

    cmpg-float v3, v3, v4

    if-gez v3, :cond_18

    .line 486
    const/4 v2, 0x1

    .line 487
    goto :goto_2

    .line 488
    :cond_18
    const/4 v1, 0x1

    goto :goto_2

    .line 498
    :cond_19
    const/4 v1, 0x1

    .line 500
    goto :goto_3

    :cond_1a
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_14

    .line 501
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    sub-long/2addr v4, v6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_14

    .line 502
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_14

    .line 503
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 504
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    sub-float/2addr v4, v12

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1b

    .line 505
    const/4 v2, 0x1

    .line 506
    goto :goto_3

    .line 507
    :cond_1b
    const/4 v1, 0x1

    goto :goto_3

    .line 524
    :cond_1c
    if-eqz v1, :cond_1d

    .line 525
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v3, :cond_0

    .line 526
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->calculateEdgeEffect(Landroid/view/MotionEvent;)V

    .line 527
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto/16 :goto_0

    .line 531
    :cond_1d
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    .line 532
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    .line 533
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 534
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 535
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 536
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 537
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 538
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 539
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v3, :cond_1e

    .line 540
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    .line 544
    :cond_1e
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoverIconType:I

    if-eq v3, v9, :cond_0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoverIconType:I

    if-eqz v3, :cond_0

    .line 545
    const/16 v3, 0xa

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 546
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setHoverIcon(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 551
    .end local v1    # "isEdgeEffect":Z
    .end local v2    # "isScroll":Z
    :cond_1f
    const/16 v3, 0xa

    if-ne v0, v3, :cond_0

    .line 552
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoverIconType:I

    if-eq v3, v9, :cond_20

    .line 553
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setHoverIcon(Landroid/view/MotionEvent;)V

    .line 555
    :cond_20
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v3

    if-eq v3, v6, :cond_0

    .line 558
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-eqz v3, :cond_22

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_22

    .line 559
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 560
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    int-to-long v4, v4

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    .line 578
    :cond_21
    :goto_4
    const-string v3, "SPen_Library"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[SMART SCALE] ON HOVER EXIT. STATE : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 561
    :cond_22
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-nez v3, :cond_23

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_21

    .line 562
    :cond_23
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 564
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v3

    if-eqz v3, :cond_24

    .line 565
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 566
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 567
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 568
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v3, :cond_21

    .line 569
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto :goto_4

    .line 572
    :cond_24
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v3, :cond_21

    .line 573
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    .line 574
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3, v9}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto :goto_4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 22
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 584
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    move/from16 v0, v18

    and-int/lit16 v4, v0, 0xff

    .line 585
    .local v4, "action":I
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v15

    .line 587
    .local v15, "toolType":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    .line 588
    .local v11, "eventX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    .line 589
    .local v12, "eventY":F
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCurrentMotionEvent:Landroid/view/MotionEvent;

    .line 591
    if-nez v4, :cond_0

    .line 592
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsMultiTouch:Z

    .line 593
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    .line 594
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    .line 595
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    .line 596
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    .line 598
    :cond_0
    const/16 v18, 0x5

    move/from16 v0, v18

    if-ne v4, v0, :cond_2

    .line 599
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsMultiTouch:Z

    .line 600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_1

    .line 601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 602
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 605
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_2

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 607
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 611
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsMultiTouch:Z

    move/from16 v18, v0

    if-eqz v18, :cond_6

    .line 612
    const/16 v18, 0x1

    move/from16 v0, v18

    if-eq v4, v0, :cond_3

    const/16 v18, 0x3

    move/from16 v0, v18

    if-ne v4, v0, :cond_5

    .line 613
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 614
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 615
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 833
    :cond_4
    :goto_0
    return-void

    .line 618
    :cond_5
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 619
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto :goto_0

    .line 625
    :cond_6
    if-nez v4, :cond_9

    .line 626
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 627
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangePan(FF)V

    .line 628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/util/SparseIntArray;->get(I)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_8

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    .line 633
    :cond_8
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v15, v0, :cond_e

    .line 634
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 641
    :cond_9
    :goto_1
    const/16 v18, 0x6

    move/from16 v0, v18

    if-ne v4, v0, :cond_f

    .line 642
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    .line 676
    :cond_a
    :goto_2
    const/4 v5, 0x0

    .local v5, "diffX":F
    const/4 v8, 0x0

    .line 677
    .local v8, "diffY":F
    const/16 v16, 0x0

    .line 678
    .local v16, "velocityX":F
    const/16 v17, 0x0

    .line 679
    .local v17, "velocityY":F
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v4, v0, :cond_c

    .line 680
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventX:F

    move/from16 v18, v0

    sub-float v5, v11, v18

    .line 681
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventY:F

    move/from16 v18, v0

    sub-float v8, v12, v18

    .line 682
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventX:F

    .line 683
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventY:F

    .line 684
    const/high16 v18, 0x43960000    # 300.0f

    mul-float v18, v18, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    move/from16 v19, v0

    div-float v16, v18, v19

    .line 685
    const/high16 v18, 0x43960000    # 300.0f

    mul-float v18, v18, v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    move/from16 v19, v0

    div-float v17, v18, v19

    .line 686
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    move/from16 v19, v0

    const/high16 v20, 0x43480000    # 200.0f

    div-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    move/from16 v19, v0

    const/high16 v20, 0x43480000    # 200.0f

    div-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_b

    .line 687
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_4

    .line 689
    :cond_b
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_c

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_c

    .line 690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_4

    .line 695
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v18

    if-lez v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->height()I

    move-result v18

    if-lez v18, :cond_4

    .line 698
    const/4 v13, 0x0

    .line 699
    .local v13, "pullLR":F
    const/4 v14, 0x0

    .line 700
    .local v14, "pullTB":F
    const/high16 v9, 0x3f000000    # 0.5f

    .line 701
    .local v9, "displacementLR":F
    const/high16 v10, 0x3f000000    # 0.5f

    .line 703
    .local v10, "displacementTB":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/util/SparseIntArray;->get(I)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 704
    if-nez v4, :cond_16

    .line 705
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-eqz v18, :cond_d

    .line 706
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 708
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 709
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_0

    .line 636
    .end local v5    # "diffX":F
    .end local v8    # "diffY":F
    .end local v9    # "displacementLR":F
    .end local v10    # "displacementTB":F
    .end local v13    # "pullLR":F
    .end local v14    # "pullTB":F
    .end local v16    # "velocityX":F
    .end local v17    # "velocityY":F
    :cond_e
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    goto/16 :goto_1

    .line 643
    :cond_f
    if-nez v4, :cond_10

    .line 644
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    .line 645
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    .line 646
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    .line 647
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventX:F

    .line 648
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventY:F

    .line 649
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 650
    :cond_10
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v4, v0, :cond_a

    .line 651
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    move-wide/from16 v20, v0

    sub-long v6, v18, v20

    .line 652
    .local v6, "diffTime":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    move/from16 v18, v0

    sub-float v18, v11, v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    .line 653
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    move/from16 v18, v0

    sub-float v18, v12, v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    .line 654
    const/high16 v18, 0x43480000    # 200.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    long-to-float v0, v6

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    div-float v16, v18, v19

    .line 655
    .restart local v16    # "velocityX":F
    const/high16 v18, 0x43480000    # 200.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    long-to-float v0, v6

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    div-float v17, v18, v19

    .line 657
    .restart local v17    # "velocityY":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_11

    .line 658
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 659
    :cond_11
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_12

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_12

    .line 660
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 661
    :cond_12
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v19

    cmpl-float v18, v18, v19

    if-lez v18, :cond_14

    .line 662
    const/16 v18, 0x0

    cmpl-float v18, v16, v18

    if-lez v18, :cond_13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmpg-double v18, v18, v20

    if-gtz v18, :cond_13

    .line 663
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 664
    :cond_13
    const/16 v18, 0x0

    cmpg-float v18, v16, v18

    if-gez v18, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_a

    .line 665
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 668
    :cond_14
    const/16 v18, 0x0

    cmpl-float v18, v17, v18

    if-lez v18, :cond_15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_15

    .line 669
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 670
    :cond_15
    const/16 v18, 0x0

    cmpg-float v18, v17, v18

    if-gez v18, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_a

    .line 671
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 711
    .end local v6    # "diffTime":J
    .restart local v5    # "diffX":F
    .restart local v8    # "diffY":F
    .restart local v9    # "displacementLR":F
    .restart local v10    # "displacementTB":F
    .restart local v13    # "pullLR":F
    .restart local v14    # "pullTB":F
    :cond_16
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v4, v0, :cond_2f

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_22

    .line 713
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_1e

    .line 714
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_1d

    const/16 v18, 0x0

    cmpl-float v18, v16, v18

    if-lez v18, :cond_1d

    .line 715
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 716
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    .line 742
    :cond_17
    :goto_3
    const/16 v18, 0x0

    cmpg-float v18, v13, v18

    if-gez v18, :cond_18

    .line 743
    const/4 v13, 0x0

    .line 751
    :cond_18
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_28

    .line 752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_24

    .line 753
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_23

    const/16 v18, 0x0

    cmpl-float v18, v17, v18

    if-lez v18, :cond_23

    .line 754
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    .line 781
    :cond_19
    :goto_5
    const/16 v18, 0x0

    cmpg-float v18, v14, v18

    if-gez v18, :cond_1a

    .line 782
    const/4 v14, 0x0

    .line 790
    :cond_1a
    :goto_6
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDensity:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_2c

    .line 791
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDensity:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_2c

    .line 792
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-nez v18, :cond_29

    .line 793
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    .line 806
    :cond_1b
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    move/from16 v18, v0

    if-nez v18, :cond_1c

    const/16 v18, 0x0

    cmpl-float v18, v13, v18

    if-lez v18, :cond_1c

    .line 807
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isLollipopOS()Z

    move-result v18

    if-eqz v18, :cond_2d

    .line 808
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13, v9}, Landroid/widget/EdgeEffect;->onPull(FF)V

    .line 812
    :goto_8
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 814
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    move/from16 v18, v0

    if-nez v18, :cond_4

    const/16 v18, 0x0

    cmpl-float v18, v14, v18

    if-lez v18, :cond_4

    .line 815
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isLollipopOS()Z

    move-result v18

    if-eqz v18, :cond_2e

    .line 816
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v10}, Landroid/widget/EdgeEffect;->onPull(FF)V

    .line 820
    :goto_9
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    goto/16 :goto_0

    .line 717
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_17

    const/16 v18, 0x0

    cmpg-float v18, v16, v18

    if-gez v18, :cond_17

    .line 718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 719
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    goto/16 :goto_3

    .line 721
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_20

    .line 722
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpl-float v18, v18, v19

    if-lez v18, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_1f

    .line 724
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 725
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 726
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    goto/16 :goto_3

    .line 727
    :cond_1f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_17

    .line 728
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    move/from16 v18, v0

    sub-float v18, v11, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v13, v18, v19

    .line 729
    const/high16 v18, 0x3f800000    # 1.0f

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->height()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    sub-float v9, v18, v19

    .line 731
    goto/16 :goto_3

    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_17

    .line 732
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_21

    .line 734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 735
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 736
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    goto/16 :goto_3

    .line 737
    :cond_21
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_17

    .line 738
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    move/from16 v18, v0

    sub-float v18, v18, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v13, v18, v19

    .line 739
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v9, v18, v19

    goto/16 :goto_3

    .line 746
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-eqz v18, :cond_18

    .line 747
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    goto/16 :goto_4

    .line 756
    :cond_23
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_19

    const/16 v18, 0x0

    cmpg-float v18, v17, v18

    if-gez v18, :cond_19

    .line 757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 758
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    goto/16 :goto_5

    .line 760
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_26

    .line 761
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpl-float v18, v18, v19

    if-lez v18, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_25

    .line 763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 764
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 765
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    goto/16 :goto_5

    .line 766
    :cond_25
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_19

    .line 767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    move/from16 v18, v0

    sub-float v18, v12, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v14, v18, v19

    .line 768
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v10, v18, v19

    .line 770
    goto/16 :goto_5

    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_19

    .line 771
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_27

    .line 773
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 774
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 775
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    goto/16 :goto_5

    .line 776
    :cond_27
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_19

    .line 777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    move/from16 v18, v0

    sub-float v18, v18, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v14, v18, v19

    .line 778
    const/high16 v18, 0x3f800000    # 1.0f

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    sub-float v10, v18, v19

    goto/16 :goto_5

    .line 785
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-eqz v18, :cond_1a

    .line 786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_6

    .line 795
    :cond_29
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    const-wide/16 v20, 0x5dc

    cmp-long v18, v18, v20

    if-ltz v18, :cond_1b

    .line 796
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    move/from16 v18, v0

    if-nez v18, :cond_2b

    .line 797
    :cond_2a
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->releaseEdgeEffects()V

    .line 799
    :cond_2b
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    .line 800
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    goto/16 :goto_7

    .line 804
    :cond_2c
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    goto/16 :goto_7

    .line 810
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto/16 :goto_8

    .line 818
    :cond_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto/16 :goto_9

    .line 823
    :cond_2f
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 824
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 825
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 826
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    .line 828
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface/range {v18 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto/16 :goto_0
.end method

.method public onZoom(FFF)V
    .locals 0
    .param p1, "deltaX"    # F
    .param p2, "deltaY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 1145
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    .line 1146
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    .line 1147
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 1148
    return-void
.end method

.method public setDrawInformation(IIII)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "startX"    # I
    .param p4, "startY"    # I

    .prologue
    .line 1179
    return-void
.end method

.method public setLimitHeight(FF)V
    .locals 4
    .param p1, "maxDeltaX"    # F
    .param p2, "maxDeltaY"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1151
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    .line 1152
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    .line 1154
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    .line 1160
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    .line 1165
    :goto_1
    return-void

    .line 1157
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    goto :goto_0

    .line 1163
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    goto :goto_1
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    .line 256
    return-void
.end method

.method protected setParentRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1182
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1183
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    .line 1192
    :goto_0
    return-void

    .line 1186
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    .line 1187
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    .line 1189
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    .line 1191
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method public setToolTypeAction(II)V
    .locals 1
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 260
    return-void
.end method
