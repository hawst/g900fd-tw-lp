.class Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;
.super Ljava/lang/Object;
.source "Video.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->startVideoPlay()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    .line 413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 418
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->canPause()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 420
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->pause()V

    .line 427
    :cond_0
    :goto_0
    return v3

    .line 422
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->start()V

    .line 423
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    goto :goto_0
.end method
