.class public final Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;
.super Ljava/lang/Object;
.source "SpenSignatureVerification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;
    }
.end annotation


# static fields
.field public static final VERIFICATION_LEVEL_HIGH:I = 0x2

.field private static final VERIFICATION_LEVEL_KEY:Ljava/lang/String; = "VerificationLevel"

.field public static final VERIFICATION_LEVEL_LOW:I = 0x0

.field public static final VERIFICATION_LEVEL_MEDIUM:I = 0x1


# instance fields
.field private mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

.field private mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pluginObject"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    .line 272
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    .line 54
    if-nez p1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'context\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    if-nez p2, :cond_1

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : parameter \'pluginObject\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    .line 63
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;)Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    return-object v0
.end method


# virtual methods
.method public getMinimumRequiredCount()I
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->getMinimumRequiredCount()I

    move-result v0

    return v0
.end method

.method getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    return-object v0
.end method

.method public getRegisteredCount()I
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->getRegisteredCount()I

    move-result v0

    return v0
.end method

.method public getVerificationLevel()I
    .locals 3

    .prologue
    .line 197
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v1, :cond_0

    .line 198
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 201
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 203
    .local v0, "propertyMap":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->getProperty(Landroid/os/Bundle;)V

    .line 205
    const-string v1, "VerificationLevel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 206
    const/4 v1, -0x1

    .line 209
    :goto_0
    return v1

    :cond_1
    const-string v1, "VerificationLevel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public register(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "stroke":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    if-nez p1, :cond_1

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "E_INVALID_ARG : stroke is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->register(Ljava/util/List;)V

    .line 117
    return-void
.end method

.method public request(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 229
    .local p1, "stroke":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->request(Ljava/util/List;)V

    .line 234
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    .line 251
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    .line 256
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;)V

    .line 267
    :cond_1
    return-void
.end method

.method public setVerificationLevel(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 175
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v1, :cond_0

    .line 176
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 179
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 180
    .local v0, "propertyMap":Landroid/os/Bundle;
    const-string v1, "VerificationLevel"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 182
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->setProperty(Landroid/os/Bundle;)V

    .line 183
    return-void
.end method

.method public unregisterAll()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->unregisterAll()V

    .line 135
    return-void
.end method
