.class public Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetImageUriInfo;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GetImageUriInfo"
.end annotation


# static fields
.field public static final INTENT_ARG_CONTENT_ID:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetImageUriInfo.INTENT_ARG_CONTENT_ID"

.field public static final KEY_RESULT_HTTP_PROXY_URI:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetImageUriInfo.KEY_RESULT_HTTP_PROXY_URI"

.field public static final KEY_RESULT_LOCAL_FILE_URI:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetImageUriInfo.KEY_RESULT_LOCAL_FILE_URI"

.field public static final KEY_RESULT_SAME_ACCESS_POINT_URI:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetImageUriInfo.KEY_RESULT_SAME_ACCESS_POINT_URI"

.field public static final KEY_RESULT_SCS_URI:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetImageUriInfo.KEY_RESULT_SCS_URI"

.field public static final NAME:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetImageUriInfo.NAME"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
