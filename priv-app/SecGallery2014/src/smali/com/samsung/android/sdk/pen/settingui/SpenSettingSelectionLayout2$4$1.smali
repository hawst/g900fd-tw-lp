.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;
.super Ljava/lang/Object;
.source "SpenSettingSelectionLayout2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

.field private final synthetic val$fromFinal:I

.field private final synthetic val$toFinal:I


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;II)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$fromFinal:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$toFinal:I

    .line 821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 824
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mCount:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;I)V

    .line 826
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$fromFinal:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$toFinal:I

    if-le v1, v2, :cond_1

    .line 827
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$fromFinal:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mCount:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)I

    move-result v2

    sub-int v0, v1, v2

    .line 828
    .local v0, "pos":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setLayoutHeight(I)V

    .line 830
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$toFinal:I

    if-gt v0, v1, :cond_0

    .line 831
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$toFinal:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setLayoutHeight(I)V

    .line 833
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 834
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 838
    .end local v0    # "pos":I
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$fromFinal:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mCount:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)I

    move-result v2

    add-int v0, v1, v2

    .line 839
    .restart local v0    # "pos":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setLayoutHeight(I)V

    .line 841
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$toFinal:I

    if-lt v0, v1, :cond_0

    .line 842
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->val$toFinal:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setLayoutHeight(I)V

    .line 844
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 845
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    goto :goto_0
.end method
