.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;
.super Ljava/lang/Object;
.source "SpenSettingResourceManager.java"


# static fields
.field private static instance:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

.field public static sameResource:[Ljava/lang/String;


# instance fields
.field public hash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->instance:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    .line 24
    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "snote_popup_close_focus"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "snote_popup_close_press"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 25
    const-string/jumbo v2, "snote_popup_title_left"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "snote_popup_title_center"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "snote_popup_title_right"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 26
    const-string/jumbo v2, "snote_popup_title_bended"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "snote_popup_line"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "snote_popup_bg02_left"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "snote_popup_bg02_right"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 27
    const-string/jumbo v2, "snote_popup_arrow_left_normal"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "snote_popup_arrow_left_press"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "snote_popup_arrow_left_focus"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 28
    const-string/jumbo v2, "snote_popup_arrow_left_dim"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "snote_popup_arrow_right_normal"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "snote_popup_arrow_right_press"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 29
    const-string/jumbo v2, "snote_popup_arrow_right_focus"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "snote_popup_arrow_right_dim"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "snote_popup_bg_expand"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 30
    const-string/jumbo v2, "snote_popup_bg_expand_press"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "snote_popup_handler"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "snote_toolbar_icon_spoid_hover"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 31
    const-string/jumbo v2, "snote_color_spoid_press"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "snote_color_spoid_focus"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "snote_toolbar_bg_center_normal_2"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 32
    const-string/jumbo v2, "snote_toolbar_bg_center2_normal"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "snote_toolbar_handle_2"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "snote_toolbar_handle"

    aput-object v2, v0, v1

    .line 24
    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->sameResource:[Ljava/lang/String;

    .line 32
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->hash:Ljava/util/HashMap;

    .line 10
    return-void
.end method

.method public static close()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->instance:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    .line 22
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->instance:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->instance:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    .line 17
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->instance:Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;

    return-object v0
.end method


# virtual methods
.method public checkResInHash(Ljava/lang/String;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->hash:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public checkString(Ljava/lang/String;)Z
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 49
    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->sameResource:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 53
    :goto_1
    return v1

    .line 49
    :cond_0
    aget-object v0, v3, v2

    .line 50
    .local v0, "string":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 51
    const/4 v1, 0x1

    goto :goto_1

    .line 49
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getDrawableFromKey(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->hash:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public putDrawableToHash(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->hash:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method
