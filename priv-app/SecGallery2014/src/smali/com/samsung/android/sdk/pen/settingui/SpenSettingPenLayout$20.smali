.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 1890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15
    .param p1, "v"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1896
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1897
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v8

    if-nez v8, :cond_0

    .line 1898
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$85(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/List;)V

    .line 1901
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMaximumPresetNumber:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$86()I

    move-result v9

    if-lt v8, v9, :cond_3

    .line 1902
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v8

    const-string/jumbo v9, "string_reached_maximum_preset"

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1903
    .local v4, "strPreset":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 1904
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMaximumPresetNumber:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$86()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v4, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1905
    .local v5, "text":Ljava/lang/CharSequence;
    const/4 v0, 0x0

    .line 1907
    .local v0, "duration":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v5, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    .line 1908
    .local v6, "toast":Landroid/widget/Toast;
    invoke-virtual {v6}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v8

    const v9, 0x102000b

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1909
    .local v7, "v1":Landroid/widget/TextView;
    if-eqz v7, :cond_1

    .line 1910
    const/16 v8, 0x11

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 1912
    :cond_1
    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 2011
    .end local v0    # "duration":I
    .end local v4    # "strPreset":Ljava/lang/String;
    .end local v5    # "text":Ljava/lang/CharSequence;
    .end local v6    # "toast":Landroid/widget/Toast;
    .end local v7    # "v1":Landroid/widget/TextView;
    :cond_2
    :goto_0
    return-void

    .line 1917
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_8

    .line 1933
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v9

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setButtonFocus(Landroid/view/View;)V
    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$88(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Landroid/view/View;)V

    .line 1934
    const/4 v8, 0x1

    sput-boolean v8, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsSwichTab:Z

    .line 1935
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 1936
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v8

    sget-boolean v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsSwichTab:Z

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->setSwitchTabFlag(Z)V

    .line 1939
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$68(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_6

    .line 1940
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$89(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1941
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/TextView;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1943
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$90(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)V

    .line 1944
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-boolean v8, v8, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    if-eqz v8, :cond_b

    .line 1945
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v9

    const v10, 0x44bb8000    # 1500.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 1950
    :goto_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$92(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1951
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$93(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/ViewGroup;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1952
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$94(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1953
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1954
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    .line 1955
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v9, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V
    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$95(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1956
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v9, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyStyleBtnsLayout(Z)V
    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$96(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 1957
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$97(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1958
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$98(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v9

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$100()Ljava/lang/String;

    move-result-object v10

    .line 1959
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v11

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v12

    .line 1958
    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1960
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$102()I

    move-result v8

    const/16 v9, 0x10

    if-ge v8, v9, :cond_c

    .line 1961
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v9

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$100()Ljava/lang/String;

    move-result-object v10

    .line 1962
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v11

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v12

    .line 1961
    invoke-virtual {v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1972
    :goto_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->invalidate()V

    .line 1973
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$104(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/LinearLayout;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1976
    :cond_6
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMontblancStyleBtnsLayout:Landroid/view/View;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$105(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1978
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;-><init>()V

    .line 1980
    .local v3, "presetInfoItem":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v8

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPenName(Ljava/lang/String;)V

    .line 1981
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v8

    const/high16 v9, 0x42820000    # 65.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1982
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v9

    const/high16 v10, 0x42820000    # 65.0f

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 1981
    invoke-virtual {v3, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setBitmapSize(II)V

    .line 1983
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v8

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPenSize(F)V

    .line 1984
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v8

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setColor(I)V

    .line 1985
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v8

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setAdvancedSetting(Ljava/lang/String;)V

    .line 1986
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->getPresetImage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPresetImageName(Ljava/lang/String;)V

    .line 1988
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1989
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v8

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1991
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isRemovePreset:Z
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$2()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1992
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    iput v9, v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    .line 1993
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    .line 1999
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->notifyDataSetChanged()V

    .line 2000
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetDisplay()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    .line 2001
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 2002
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2003
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;->onAdded(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 2004
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V

    .line 2007
    :cond_7
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$SpenPresetGridView;->smoothScrollToPosition(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2008
    .end local v3    # "presetInfoItem":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    :catch_0
    move-exception v1

    .line 2009
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 1917
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_8
    :try_start_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    .line 1918
    .local v2, "info":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v8

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    cmpl-float v8, v8, v10

    if-nez v8, :cond_9

    .line 1919
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v10

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v8

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v8

    invoke-interface {v8}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v8

    if-ne v10, v8, :cond_9

    .line 1920
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 1921
    :cond_9
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v8

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v10, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v8

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    cmpl-float v8, v8, v10

    if-nez v8, :cond_4

    .line 1922
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1923
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v8

    const/4 v10, 0x5

    if-ne v8, v10, :cond_4

    .line 1924
    :cond_a
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-result-object v8

    const-string/jumbo v9, "string_already_exists"

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1925
    .restart local v5    # "text":Ljava/lang/CharSequence;
    const/4 v0, 0x0

    .line 1927
    .restart local v0    # "duration":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v5, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    .line 1928
    .restart local v6    # "toast":Landroid/widget/Toast;
    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1947
    .end local v0    # "duration":I
    .end local v2    # "info":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    .end local v5    # "text":Ljava/lang/CharSequence;
    .end local v6    # "toast":Landroid/widget/Toast;
    :cond_b
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$91(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v9

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    goto/16 :goto_1

    .line 1963
    :cond_c
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$102()I

    move-result v8

    const/16 v9, 0x15

    if-lt v8, v9, :cond_d

    .line 1964
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v8

    new-instance v9, Landroid/graphics/drawable/RippleDrawable;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/16 v11, 0x29

    .line 1965
    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 1964
    invoke-static {v11, v12, v13, v14}, Landroid/graphics/Color;->argb(IIII)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v10

    .line 1965
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$103()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {v9, v10, v11, v12}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1964
    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1968
    :cond_d
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$99(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v9

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$100()Ljava/lang/String;

    move-result-object v10

    .line 1969
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v11

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$101()Ljava/lang/String;

    move-result-object v12

    .line 1968
    invoke-virtual {v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1995
    .restart local v3    # "presetInfoItem":Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
    :cond_e
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    iput v9, v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    .line 1996
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method
