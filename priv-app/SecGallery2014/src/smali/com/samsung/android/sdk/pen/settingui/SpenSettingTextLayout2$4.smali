.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 372
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 373
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x432a0000    # 170.0f

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget v5, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;-><init>(Landroid/view/View;Ljava/util/ArrayList;IIF)V

    .line 372
    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)V

    .line 375
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->setOnItemSelectListner(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;)V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->show(IILjava/lang/String;)V

    .line 397
    return-void
.end method
