.class Lcom/samsung/android/sdk/pen/engine/SpenInView;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$BitmapInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateHandler;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static LASTEST_LIB_PATH:Ljava/lang/String; = null

.field private static final NATIVE_COMMAND_CAPTUREPAGE_TRANSPARENT:I = 0x6

.field private static final NATIVE_COMMAND_CROP:I = 0x2

.field private static final NATIVE_COMMAND_PENBUTTON_SELECTION_ENABLE:I = 0x5

.field private static final NATIVE_COMMAND_REQUEST_PAGEDOC:I = 0x4

.field private static final NATIVE_COMMAND_SET_LOCALE:I = 0x8

.field private static final NATIVE_COMMAND_SET_SCREEN_INFO:I = 0x7

.field private static final NATIVE_COMMAND_STRETCH:I = 0x1

.field private static final NATIVE_COMMAND_TEXT_BOX_SET_DISPLAY_INFO:I = 0x3

.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field public static final REPLAY_STATE_PAUSED:I = 0x2

.field public static final REPLAY_STATE_PLAYING:I = 0x1

.field public static final REPLAY_STATE_STOPPED:I = 0x0

.field private static final STROKE_FRAME_KEY:Ljava/lang/String; = "STROKE_FRAME"

.field private static final STROKE_FRAME_RETAKE:I = 0x2

.field private static final STROKE_FRAME_TAKE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpenInView"

.field private static final TEXT_OBJECT_DEFAULT_SIZE_FONT:F = 36.0f

.field private static mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper; = null

.field private static final penNameBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Brush"

.field private static final penNameChineseBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field private static final penNameFountainPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.FountainPen"

.field private static final penNameInkPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.InkPen"

.field private static final penNameMagicPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field private static final penNameMarker:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Marker"

.field private static final penNameObliquePen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

.field private static final penNamePencil:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Pencil"


# instance fields
.field private final HOVER_INTERVAL:I

.field private MIN_STROKE_LENGTH:F

.field final PEN_HOVERING_LINK_PREVIEW:Ljava/lang/String;

.field final USER_CURRENT_OR_SELF:I

.field private activePen:I

.field private bIsSupport:Z

.field private isEraserCursor:Z

.field private isSkipTouch:Z

.field private isSoundEnabled:Z

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCanvasLayer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mCirclePoint:Landroid/graphics/PointF;

.field private mCircleRadius:F

.field private mClippingPath:Landroid/graphics/Path;

.field private mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

.field private mContext:Landroid/content/Context;

.field private mContextMenu:Landroid/view/ContextMenu;

.field private mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

.field private mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

.field private mCurrentDrawingTooltype:I

.field private mDebugPaint:Landroid/graphics/Paint;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

.field private mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

.field private mDottedLineEnable:Z

.field private mDottedLineHalfWidth:F

.field private mDottedLineIntervalHeight:I

.field private mDottedLinePaint:Landroid/graphics/Paint;

.field private mDoubleTapZoomable:Z

.field private mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

.field private mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

.field private mFloatingLayer:Landroid/graphics/Bitmap;

.field private mFramebufferHeight:I

.field private mFramebufferWidth:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

.field private mHighlightInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHighlightPaint:Landroid/graphics/Paint;

.field private mHoverDrawable:Landroid/graphics/drawable/Drawable;

.field private mHoverEnable:Z

.field private mHoverEnterTime:J

.field private mHoverIconID:I

.field private mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mHoverPoint:Landroid/graphics/Point;

.field private mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

.field private mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

.field private mIndexBrush:I

.field private mIndexEraser:I

.field private mIndexMarker:I

.field private mIndexPencil:I

.field private mIs64:Z

.field private mIsCancelFling:Z

.field private mIsControlSelect:Z

.field private mIsEditableTextBox:Z

.field private mIsHyperText:Z

.field private mIsLongPressEnable:Z

.field private mIsPenButtonSelectionEnabled:Z

.field private mIsSmartHorizontal:Z

.field private mIsSmartScale:Z

.field private mIsSmartVertical:Z

.field private mIsStretch:Z

.field private mIsStrokeDrawing:Z

.field private mIsTemporaryStroke:Z

.field private mIsToolTip:Z

.field private mIsTouchPre:Z

.field private mLayoutExceptSIP:I

.field private mLayoutHeight:I

.field private mLayoutWidth:I

.field private mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

.field private mMagicPenEnabled:Z

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mMeasure:Landroid/graphics/PathMeasure;

.field private mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

.field private mOldX:F

.field private mOldY:F

.field private mOnePT:F

.field private mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

.field private mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

.field private mPageEffectPaint:Landroid/graphics/Paint;

.field private mParentLayout:Landroid/view/ViewGroup;

.field private mPath:Landroid/graphics/Path;

.field private mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

.field private mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

.field private mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

.field private mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

.field private mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mRatio:F

.field private mRatioBitmapHeight:I

.field private mRatioBitmapWidth:I

.field private mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

.field private mRemoverRadius:F

.field private mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mRemoverToastMessage:Landroid/widget/Toast;

.field private mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

.field private mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

.field private mRtoBmpItstScrHeight:I

.field private mRtoBmpItstScrWidth:I

.field private mScreenFB:Landroid/graphics/Bitmap;

.field private mScreenHeight:I

.field private mScreenHeightExceptSIP:I

.field private mScreenInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

.field private mScreenStartX:I

.field private mScreenStartY:I

.field private mScreenWidth:I

.field private mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

.field private mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

.field private mSetPageDocHandler:Landroid/os/Handler;

.field private mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

.field private mSmps:Lcom/samsung/audio/SmpsManager;

.field private final mSpenIconMoreHandler:Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

.field private mSrcPageEffectPaint:Landroid/graphics/Paint;

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mStretchHeight:I

.field private mStretchWidth:I

.field private mStretchXRatio:F

.field private mStretchYRatio:F

.field private mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

.field private mStrokeFrameType:I

.field private mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

.field private mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

.field private mTouchCount:I

.field private mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mTouchProcessingTime:J

.field private mTouchUpHandler:Landroid/os/Handler;

.field private mTransactionClosingControl:Z

.field private mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

.field private mUpdateHandler:Landroid/os/Handler;

.field private mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

.field private mUseC2D:Z

.field private mView:Landroid/view/View;

.field private mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

.field private mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

.field private mZoomable:Z

.field private nativeCanvas:J

.field private removerTouchX:F

.field private removerTouchY:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const-string v0, "/system/vendor/lib/libC2D2.so"

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->LASTEST_LIB_PATH:Ljava/lang/String;

    .line 171
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 378
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;Z)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "updateCanvasListener"    # Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;
    .param p3, "isUseC2D"    # Z

    .prologue
    const/4 v9, -0x1

    const/4 v2, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContextMenu:Landroid/view/ContextMenu;

    .line 112
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutExceptSIP:I

    .line 113
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    .line 114
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    .line 115
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    .line 116
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    .line 117
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    .line 118
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    .line 119
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    .line 120
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    .line 121
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 123
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 124
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    .line 125
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    .line 126
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 128
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .line 129
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 130
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 131
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .line 132
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 133
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .line 134
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .line 135
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 136
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 137
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 138
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    .line 139
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    .line 140
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 141
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    .line 142
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .line 143
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    .line 144
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    .line 145
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 146
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 147
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 148
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 149
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 150
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    .line 151
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    .line 152
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    .line 154
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 155
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 156
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 157
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .line 158
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 159
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    .line 163
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsEditableTextBox:Z

    .line 165
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    .line 166
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 167
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 168
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    .line 169
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 172
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 173
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    .line 174
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverIconID:I

    .line 175
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 176
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    .line 177
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 179
    const/16 v4, 0x12c

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->HOVER_INTERVAL:I

    .line 180
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    .line 181
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnterTime:J

    .line 182
    const/4 v4, -0x3

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->USER_CURRENT_OR_SELF:I

    .line 183
    const-string v4, "pen_hovering_link_preview"

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->PEN_HOVERING_LINK_PREVIEW:Ljava/lang/String;

    .line 185
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    .line 186
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z

    .line 187
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    .line 189
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    .line 190
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    .line 191
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    .line 192
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    .line 193
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    .line 194
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    .line 195
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 197
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPageEffectPaint:Landroid/graphics/Paint;

    .line 198
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectPaint:Landroid/graphics/Paint;

    .line 199
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    .line 200
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    .line 201
    new-instance v4, Landroid/graphics/PointF;

    const/high16 v5, -0x3d380000    # -100.0f

    const/high16 v6, -0x3d380000    # -100.0f

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    .line 202
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 203
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    .line 206
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    .line 207
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    .line 208
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    .line 209
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    .line 211
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomable:Z

    .line 212
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDoubleTapZoomable:Z

    .line 213
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    .line 214
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    .line 215
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    .line 216
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    .line 217
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 218
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    .line 222
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z

    .line 224
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    .line 225
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    .line 233
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    .line 235
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    .line 236
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchX:F

    .line 237
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchY:F

    .line 239
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    .line 241
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 243
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    .line 244
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchHeight:I

    .line 245
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    .line 255
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    .line 256
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

    .line 257
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    .line 258
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsPenButtonSelectionEnabled:Z

    .line 259
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTemporaryStroke:Z

    .line 260
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCurrentDrawingTooltype:I

    .line 264
    const/high16 v4, 0x41700000    # 15.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F

    .line 265
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOnePT:F

    .line 268
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 270
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldX:F

    .line 271
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldY:F

    .line 274
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    .line 277
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    .line 278
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    .line 279
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 280
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    .line 281
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexMarker:I

    .line 282
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexBrush:I

    .line 283
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexEraser:I

    .line 284
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    .line 285
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    .line 286
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    .line 380
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    .line 6271
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    .line 6297
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    .line 6318
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateHandler;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    .line 6401
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenIconMoreHandler:Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

    .line 406
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    .line 407
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v4

    const/16 v5, 0x20

    if-ne v4, v5, :cond_0

    move v2, v3

    :cond_0
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    .line 408
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_init()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    .line 409
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    .line 410
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->construct()V

    .line 411
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v2, :cond_1

    .line 412
    const-string v2, "/system/vendor/lib64/libC2D2.so"

    sput-object v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->LASTEST_LIB_PATH:Ljava/lang/String;

    .line 414
    :cond_1
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->LASTEST_LIB_PATH:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 415
    .local v1, "libFilePath":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 416
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 430
    :cond_2
    :goto_0
    return-void

    .line 417
    :cond_3
    if-eqz p3, :cond_2

    .line 419
    :try_start_0
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_init_c2dJNI()V

    .line 420
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 421
    const-string v2, "c2d"

    const-string v4, "c2d blit init"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 422
    :catch_0
    move-exception v0

    .line 423
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 424
    const-string v2, "c2d"

    const-string v3, "c2d blit init fail, lib loading error"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 425
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 426
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    .line 427
    const-string v2, "c2d"

    const-string v3, "c2d blit init fail, lib loading error"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dst"    # Landroid/graphics/Rect;
    .param p2, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 908
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 909
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 910
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 911
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 912
    return-void
.end method

.method private MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 903
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->right:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 904
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 903
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private Native_cancelStroke(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6888
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6889
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_cancelStroke(J)V

    .line 6893
    :goto_0
    return-void

    .line 6891
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_cancelStroke(I)V

    goto :goto_0
.end method

.method private Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "command"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6928
    .local p4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6929
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 6931
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenInView;
    .param p5, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 6471
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6472
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z

    move-result v0

    .line 6474
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_drawObjectList(JLandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 6904
    .local p4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6905
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_drawObjectList(JLandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z

    move-result v0

    .line 6907
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_drawObjectList(ILandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_enablePenCurve(JZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "curve"    # Z

    .prologue
    .line 6727
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6728
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enablePenCurve(JZ)V

    .line 6732
    :goto_0
    return-void

    .line 6730
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enablePenCurve(IZ)V

    goto :goto_0
.end method

.method private Native_enableZoom(JZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "mode"    # Z

    .prologue
    .line 6567
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6568
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enableZoom(JZ)V

    .line 6572
    :goto_0
    return-void

    .line 6570
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enableZoom(IZ)V

    goto :goto_0
.end method

.method private Native_finalize(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6463
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6464
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_finalize(J)V

    .line 6468
    :goto_0
    return-void

    .line 6466
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_finalize(I)V

    goto :goto_0
.end method

.method private Native_getAdvancedSetting(J)Ljava/lang/String;
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6767
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6768
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getAdvancedSetting(J)Ljava/lang/String;

    move-result-object v0

    .line 6770
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getAdvancedSetting(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getEraserSize(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6751
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6752
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getEraserSize(J)F

    move-result v0

    .line 6754
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getEraserSize(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMaxZoomRatio(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6607
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6608
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMaxZoomRatio(J)F

    move-result v0

    .line 6610
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMaxZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMinZoomRatio(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6623
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6624
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMinZoomRatio(J)F

    move-result v0

    .line 6626
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMinZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPan(JLandroid/graphics/PointF;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 6639
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6640
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPan(JLandroid/graphics/PointF;)V

    .line 6644
    :goto_0
    return-void

    .line 6642
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPan(ILandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method private Native_getPenColor(J)I
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6703
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6704
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenColor(J)I

    move-result v0

    .line 6706
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenSize(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6719
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6720
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenSize(J)F

    move-result v0

    .line 6722
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenSize(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenStyle(J)Ljava/lang/String;
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6687
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6688
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenStyle(J)Ljava/lang/String;

    move-result-object v0

    .line 6690
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPenStyle(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getReplayState(J)I
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6824
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6825
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getReplayState(J)I

    move-result v0

    .line 6827
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getReplayState(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getTemporaryStroke(JLjava/util/ArrayList;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6864
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6865
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getTemporaryStroke(JLjava/util/ArrayList;)V

    .line 6869
    :goto_0
    return-void

    .line 6867
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getTemporaryStroke(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method private Native_getToolTypeAction(JI)I
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "toolType"    # I

    .prologue
    .line 6559
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6560
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getToolTypeAction(JI)I

    move-result v0

    .line 6562
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getToolTypeAction(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getZoomRatio(J)F
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6591
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6592
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getZoomRatio(J)F

    move-result v0

    .line 6594
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_inVisibleUpdate(JIZZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "runtimeHandle"    # I
    .param p4, "isVisible"    # Z
    .param p5, "isClosed"    # Z

    .prologue
    .line 6535
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6536
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_inVisibleUpdate(JIZZ)V

    .line 6540
    :goto_0
    return-void

    .line 6538
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_inVisibleUpdate(IIZZ)V

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 6455
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6456
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_init_64()J

    move-result-wide v0

    .line 6458
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_isPenCurve(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6735
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6736
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isPenCurve(J)Z

    move-result v0

    .line 6738
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isPenCurve(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_isZoomable(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6575
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6576
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isZoomable(J)Z

    move-result v0

    .line 6578
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isZoomable(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onHover(JLandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "toolType"    # I

    .prologue
    .line 6655
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6656
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onHover(JLandroid/view/MotionEvent;I)Z

    move-result v0

    .line 6658
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onHover(ILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onLongPress(JLandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "toolType"    # I

    .prologue
    .line 6671
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6672
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onLongPress(JLandroid/view/MotionEvent;I)Z

    move-result v0

    .line 6674
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onLongPress(ILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "toolType"    # I

    .prologue
    .line 6663
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6664
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z

    move-result v0

    .line 6666
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onSingleTapUp(ILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onTouch(JLandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "event"    # Landroid/view/MotionEvent;
    .param p4, "toolType"    # I

    .prologue
    .line 6647
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6648
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onTouch(JLandroid/view/MotionEvent;I)Z

    move-result v0

    .line 6650
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onTouch(ILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_pauseReplay(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6807
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6808
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_pauseReplay(J)Z

    move-result v0

    .line 6810
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_pauseReplay(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_removeCanvasBitmap(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6511
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6512
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_removeCanvasBitmap(J)V

    .line 6516
    :goto_0
    return-void

    .line 6514
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_removeCanvasBitmap(I)V

    goto :goto_0
.end method

.method private Native_resumeReplay(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6815
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6816
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_resumeReplay(J)Z

    move-result v0

    .line 6818
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_resumeReplay(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setAdvancedSetting(JLjava/lang/String;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 6759
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6760
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setAdvancedSetting(JLjava/lang/String;)V

    .line 6764
    :goto_0
    return-void

    .line 6762
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setAdvancedSetting(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 6495
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6496
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setBitmap(JLandroid/graphics/Bitmap;)V

    .line 6500
    :goto_0
    return-void

    .line 6498
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "layerId"    # I
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 6503
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6504
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 6508
    :goto_0
    return-void

    .line 6506
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setEraserSize(JF)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "width"    # F

    .prologue
    .line 6743
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6744
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserSize(JF)V

    .line 6748
    :goto_0
    return-void

    .line 6746
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserSize(IF)V

    goto :goto_0
.end method

.method private Native_setEraserType(JI)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "type"    # I

    .prologue
    .line 6880
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6881
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserType(JI)V

    .line 6885
    :goto_0
    return-void

    .line 6883
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserType(II)V

    goto :goto_0
.end method

.method private Native_setHyperTextViewEnabled(JZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "enabled"    # Z

    .prologue
    .line 6896
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6897
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setHyperTextViewEnabled(JZ)V

    .line 6901
    :goto_0
    return-void

    .line 6899
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setHyperTextViewEnabled(IZ)V

    goto :goto_0
.end method

.method private Native_setMaxZoomRatio(JF)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "ratio"    # F

    .prologue
    .line 6599
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6600
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMaxZoomRatio(JF)Z

    move-result v0

    .line 6602
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMaxZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setMinZoomRatio(JF)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "ratio"    # F

    .prologue
    .line 6615
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6616
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMinZoomRatio(JF)Z

    move-result v0

    .line 6618
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMinZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 1
    .param p1, "nativeCanvaTouchs"    # J
    .param p3, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p4, "isUpdate"    # Z

    .prologue
    .line 6543
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6544
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    .line 6546
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPan(JFFZ)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "deltaX"    # F
    .param p4, "deltaY"    # F
    .param p5, "isUpdate"    # Z

    .prologue
    .line 6631
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6632
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPan(JFFZ)V

    .line 6636
    :goto_0
    return-void

    .line 6634
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPan(IFFZ)V

    goto :goto_0
.end method

.method private Native_setPenColor(JI)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "color"    # I

    .prologue
    .line 6695
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6696
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenColor(JI)V

    .line 6700
    :goto_0
    return-void

    .line 6698
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenColor(II)V

    goto :goto_0
.end method

.method private Native_setPenSize(JF)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "width"    # F

    .prologue
    .line 6711
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6712
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenSize(JF)V

    .line 6716
    :goto_0
    return-void

    .line 6714
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenSize(IF)V

    goto :goto_0
.end method

.method private Native_setPenStyle(JLjava/lang/String;)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "style"    # Ljava/lang/String;

    .prologue
    .line 6679
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6680
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenStyle(JLjava/lang/String;)Z

    move-result v0

    .line 6682
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenStyle(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setRemoverSize(JF)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "width"    # F

    .prologue
    .line 6920
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6921
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverSize(JF)V

    .line 6925
    :goto_0
    return-void

    .line 6923
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverSize(IF)V

    goto :goto_0
.end method

.method private Native_setRemoverType(JI)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "type"    # I

    .prologue
    .line 6912
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6913
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverType(JI)V

    .line 6917
    :goto_0
    return-void

    .line 6915
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverType(II)V

    goto :goto_0
.end method

.method private Native_setReplayPosition(JI)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "index"    # I

    .prologue
    .line 6840
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6841
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplayPosition(JI)Z

    move-result v0

    .line 6843
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplayPosition(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setReplaySpeed(JI)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "speed"    # I

    .prologue
    .line 6832
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6833
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplaySpeed(JI)Z

    move-result v0

    .line 6835
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplaySpeed(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setScreenFrameBuffer(JLandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 6487
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6488
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenFrameBuffer(JLandroid/graphics/Bitmap;)V

    .line 6492
    :goto_0
    return-void

    .line 6490
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenFrameBuffer(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setScreenSize(JIII)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "heightExceptSIP"    # I

    .prologue
    .line 6479
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6480
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenSize(JIII)V

    .line 6484
    :goto_0
    return-void

    .line 6482
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenSize(IIII)V

    goto :goto_0
.end method

.method private Native_setSelectionType(JI)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "type"    # I

    .prologue
    .line 6872
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6873
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setSelectionType(JI)V

    .line 6877
    :goto_0
    return-void

    .line 6875
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setSelectionType(II)V

    goto :goto_0
.end method

.method private Native_setToolTypeAction(JII)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "toolType"    # I
    .param p4, "action"    # I

    .prologue
    .line 6551
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6552
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setToolTypeAction(JII)V

    .line 6556
    :goto_0
    return-void

    .line 6554
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setToolTypeAction(III)V

    goto :goto_0
.end method

.method private Native_setZoom(JFFF)V
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "centerX"    # F
    .param p4, "centerY"    # F
    .param p5, "ratio"    # F

    .prologue
    .line 6583
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6584
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setZoom(JFFF)V

    .line 6588
    :goto_0
    return-void

    .line 6586
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setZoom(IFFF)V

    goto :goto_0
.end method

.method private Native_startReplay(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6791
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6792
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startReplay(J)Z

    move-result v0

    .line 6794
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startReplay(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_startTemporaryStroke(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6848
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6849
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startTemporaryStroke(J)V

    .line 6853
    :goto_0
    return-void

    .line 6851
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startTemporaryStroke(I)V

    goto :goto_0
.end method

.method private Native_stopReplay(J)Z
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6799
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6800
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopReplay(J)Z

    move-result v0

    .line 6802
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopReplay(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_stopTemporaryStroke(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6856
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6857
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopTemporaryStroke(J)V

    .line 6861
    :goto_0
    return-void

    .line 6859
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopTemporaryStroke(I)V

    goto :goto_0
.end method

.method private Native_update(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6519
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6520
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_update(J)V

    .line 6524
    :goto_0
    return-void

    .line 6522
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_update(I)V

    goto :goto_0
.end method

.method private Native_updateAllScreenFrameBuffer(J)V
    .locals 1
    .param p1, "nativeCanvas"    # J

    .prologue
    .line 6527
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6528
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateAllScreenFrameBuffer(J)V

    .line 6532
    :goto_0
    return-void

    .line 6530
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateAllScreenFrameBuffer(I)V

    goto :goto_0
.end method

.method private Native_updateRedo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .param p4, "length"    # I

    .prologue
    .line 6783
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6784
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateRedo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    .line 6786
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateRedo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_updateUndo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .param p4, "length"    # I

    .prologue
    .line 6775
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 6776
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateUndo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    .line 6778
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateUndo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    goto :goto_0
.end method

.method private absoluteCoordinate(Landroid/graphics/Rect;FFFF)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/Rect;
    .param p2, "l"    # F
    .param p3, "t"    # F
    .param p4, "r"    # F
    .param p5, "b"    # F

    .prologue
    .line 885
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p2, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 886
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p4, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 887
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p3, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 888
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p5, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 889
    return-void
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRectF"    # Landroid/graphics/RectF;
    .param p2, "srcRectF"    # Landroid/graphics/RectF;

    .prologue
    .line 878
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 879
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 880
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 881
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 882
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 6638
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getPan(JLandroid/graphics/PointF;)V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenInView;JFFZ)V
    .locals 1

    .prologue
    .line 6630
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    return-object v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z
    .locals 1

    .prologue
    .line 6662
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z

    return v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    return v0
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PathMeasure;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMeasure:Landroid/graphics/PathMeasure;

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F

    return v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    return-object v0
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenInView;JI)I
    .locals 1

    .prologue
    .line 6558
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getToolTypeAction(JI)I

    move-result v0

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    return v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V
    .locals 0

    .prologue
    .line 202
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    return-void
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    return-object v0
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V
    .locals 0

    .prologue
    .line 203
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    return-void
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    return-void
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V
    .locals 0

    .prologue
    .line 257
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    return-void
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z
    .locals 1

    .prologue
    .line 6670
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onLongPress(JLandroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z

    return v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDoubleTapZoomable:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    return-object v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    return v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    return v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V
    .locals 0

    .prologue
    .line 1319
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 0

    .prologue
    .line 1214
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1196
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer2(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 6297
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    return-object v0
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 2394
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    return-void
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsEditableTextBox:Z

    return v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    return-object v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    return-object v0
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    return-object v0
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    return v0
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    return v0
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    return v0
.end method

.method static synthetic access$56(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    return v0
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/engine/SpenInView;JLandroid/view/MotionEvent;I)Z
    .locals 1

    .prologue
    .line 6646
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onTouch(JLandroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$58(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    return-object v0
.end method

.method static synthetic access$59(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V
    .locals 0

    .prologue
    .line 163
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsEditableTextBox:Z

    return-void
.end method

.method static synthetic access$60(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    return-object v0
.end method

.method static synthetic access$61(Lcom/samsung/android/sdk/pen/engine/SpenInView;JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 6927
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$62()Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    return-object v0
.end method

.method static synthetic access$63(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    return-void
.end method

.method static synthetic access$64(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)V
    .locals 0

    .prologue
    .line 233
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    return-void
.end method

.method static synthetic access$65(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    return-object v0
.end method

.method static synthetic access$66(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 1979
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->raiseOnKeyDown()V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)J
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    return-wide v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenInView;JIZZ)V
    .locals 1

    .prologue
    .line 6534
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_inVisibleUpdate(JIZZ)V

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    return v0
.end method

.method private applyTextSetting(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 14
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 1667
    const/4 v3, 0x0

    .line 1668
    .local v3, "endPos":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 1669
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    .line 1672
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getSpan()Ljava/util/ArrayList;

    move-result-object v10

    .line 1673
    .local v10, "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-nez v10, :cond_2

    .line 1674
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-nez v12, :cond_1

    .line 1675
    new-instance v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v12}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1677
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/high16 v13, 0x42100000    # 36.0f

    iput v13, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 1680
    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    .end local v10    # "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1682
    .restart local v10    # "span":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    new-instance v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    .line 1683
    .local v6, "fsSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;
    const/4 v12, 0x0

    iput v12, v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->startPos:I

    .line 1684
    iput v3, v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->endPos:I

    .line 1685
    const/high16 v12, 0x42100000    # 36.0f

    iput v12, v6, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 1687
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1689
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    .line 1690
    .local v4, "fcSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;
    const/4 v12, 0x0

    iput v12, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->startPos:I

    .line 1691
    iput v3, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->endPos:I

    .line 1693
    const/high16 v12, -0x1000000

    iput v12, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    .line 1695
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1697
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;-><init>()V

    .line 1698
    .local v1, "bsSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;
    const/4 v12, 0x0

    iput v12, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->startPos:I

    .line 1699
    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->endPos:I

    .line 1702
    const/4 v12, 0x0

    iput-boolean v12, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    .line 1704
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1706
    new-instance v7, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    invoke-direct {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;-><init>()V

    .line 1707
    .local v7, "isSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;
    const/4 v12, 0x0

    iput v12, v7, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->startPos:I

    .line 1708
    iput v3, v7, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->endPos:I

    .line 1711
    const/4 v12, 0x0

    iput-boolean v12, v7, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    .line 1713
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1715
    new-instance v11, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    invoke-direct {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;-><init>()V

    .line 1716
    .local v11, "usSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;
    const/4 v12, 0x0

    iput v12, v11, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->startPos:I

    .line 1717
    iput v3, v11, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->endPos:I

    .line 1720
    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    .line 1722
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1724
    new-instance v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    .line 1725
    .local v5, "fnSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;
    const/4 v12, 0x0

    iput v12, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->startPos:I

    .line 1726
    iput v3, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->endPos:I

    .line 1728
    const-string v12, "Roboto-Regular"

    iput-object v12, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    .line 1730
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1732
    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;-><init>()V

    .line 1734
    .local v2, "direction":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;
    const/4 v12, 0x0

    iput v12, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    .line 1735
    const/4 v12, 0x0

    iput v12, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->startPos:I

    .line 1736
    iput v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->endPos:I

    .line 1738
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1740
    invoke-virtual {p1, v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setSpan(Ljava/util/ArrayList;)V

    .line 1743
    .end local v1    # "bsSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;
    .end local v2    # "direction":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;
    .end local v4    # "fcSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;
    .end local v5    # "fnSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;
    .end local v6    # "fsSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;
    .end local v7    # "isSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;
    .end local v11    # "usSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v9

    .line 1744
    .local v9, "paragraph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    if-nez v9, :cond_4

    .line 1745
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-nez v12, :cond_3

    .line 1746
    new-instance v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v12}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1749
    :cond_3
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "paragraph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1751
    .restart local v9    # "paragraph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;>;"
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    .line 1752
    .local v0, "align":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v12, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    int-to-char v12, v12

    iput v12, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    .line 1753
    const/4 v12, 0x0

    iput v12, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->startPos:I

    .line 1754
    iput v3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->endPos:I

    .line 1756
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1766
    new-instance v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    .line 1767
    .local v8, "lineSpacing":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v12, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iput v12, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    .line 1768
    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v12, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v12, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 1769
    const/4 v12, 0x0

    iput v12, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->startPos:I

    .line 1770
    iput v3, v8, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    .line 1772
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1774
    invoke-virtual {p1, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    .line 1776
    .end local v0    # "align":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    .end local v8    # "lineSpacing":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    :cond_4
    return-void
.end method

.method private checkMDMCameraLock()Z
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 4250
    const/4 v2, 0x0

    .line 4251
    .local v2, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v4, 0x0

    .line 4253
    .local v4, "result":Ljava/lang/String;
    :try_start_0
    const-string v6, "android.os.SystemProperties"

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v10

    invoke-static {v6, v9, v10}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    .line 4254
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v5, v6

    .line 4255
    .local v5, "s":[Ljava/lang/Class;
    const/4 v6, 0x1

    new-array v1, v6, [Ljava/lang/Object;

    .line 4256
    .local v1, "args":[Ljava/lang/Object;
    const/4 v6, 0x0

    new-instance v9, Ljava/lang/String;

    const-string v10, "persist.sys.camera_lock"

    invoke-direct {v9, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v9, v1, v6

    .line 4257
    const-string v6, "get"

    invoke-virtual {v2, v6, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    const/4 v9, 0x0

    invoke-virtual {v6, v9, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    .line 4270
    .end local v1    # "args":[Ljava/lang/Object;
    .end local v5    # "s":[Ljava/lang/Class;
    :goto_0
    if-nez v4, :cond_0

    move v6, v7

    .line 4277
    :goto_1
    return v6

    .line 4258
    :catch_0
    move-exception v3

    .line 4259
    .local v3, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v3}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 4260
    .end local v3    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v3

    .line 4261
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 4262
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v3

    .line 4263
    .local v3, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 4264
    .end local v3    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v3

    .line 4265
    .local v3, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 4266
    .end local v3    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v3

    .line 4267
    .local v3, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 4274
    .end local v3    # "e":Ljava/lang/NoSuchMethodException;
    :cond_0
    const-string v6, "camera_lock.enabled"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v6, v8

    .line 4275
    goto :goto_1

    :cond_1
    move v6, v7

    .line 4277
    goto :goto_1
.end method

.method private construct()V
    .locals 30

    .prologue
    .line 433
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "nativeCanvas = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 435
    const/16 v3, 0x8

    const-string v4, " : nativeCanvas must not be null"

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 593
    :goto_0
    return-void

    .line 438
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 439
    const/16 v3, 0x8

    const-string v4, " : context must not be null"

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 444
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v29

    .line 445
    .local v29, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    .end local v29    # "manager":Landroid/content/pm/PackageManager;
    :goto_1
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 451
    .local v8, "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v3, p0

    move-object/from16 v7, p0

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 452
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 455
    :cond_2
    new-instance v3, Landroid/view/GestureDetector;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;)V

    invoke-direct {v3, v4, v5}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 456
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;)V

    invoke-virtual {v3, v4}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 457
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v28

    .line 458
    .local v28, "mDisplayMetrics":Landroid/util/DisplayMetrics;
    if-eqz v28, :cond_5

    .line 459
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    if-nez v3, :cond_3

    .line 460
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    .line 462
    :cond_3
    move-object/from16 v0, v28

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOnePT:F

    .line 463
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOnePT:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->MIN_STROKE_LENGTH:F

    .line 465
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    move-object/from16 v0, v28

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, v28

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v3, v5, :cond_6

    move-object/from16 v0, v28

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_2
    iput v3, v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;->baseRate:I

    .line 468
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 469
    .local v13, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDisplayInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$DisplayInfo;

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v12, 0x3

    const/4 v14, 0x0

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 472
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

    if-nez v3, :cond_4

    .line 473
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

    .line 475
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

    move-object/from16 v0, v28

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    iput v4, v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;->density:F

    .line 476
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

    move-object/from16 v0, v28

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;->width:I

    .line 477
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

    move-object/from16 v0, v28

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v4, v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;->height:I

    .line 478
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 479
    .local v19, "objList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenInfo:Lcom/samsung/android/sdk/pen/engine/SpenInView$ScreenInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v16, v0

    const/16 v18, 0x7

    const/16 v20, 0x0

    move-object/from16 v15, p0

    invoke-direct/range {v15 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 482
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, v28

    iget v5, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v5, v5

    .line 483
    move-object/from16 v0, v28

    iget v6, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;-><init>(Landroid/content/Context;FF)V

    .line 482
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 484
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V

    .line 487
    .end local v13    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v19    # "objList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v27

    .line 488
    .local v27, "locale":Ljava/lang/String;
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 489
    .local v25, "objectList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 490
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v22, v0

    const/16 v24, 0x8

    const/16 v26, 0x0

    move-object/from16 v21, p0

    invoke-direct/range {v21 .. v26}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 492
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;)V

    move-object/from16 v0, v28

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;F)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    .line 493
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;-><init>()V

    sput-object v3, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 495
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    .line 496
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 497
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    const v4, -0xf2c5b1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 499
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    .line 500
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 501
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 503
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    .line 504
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 505
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 506
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x3fc00000    # 1.5f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 507
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    const v4, -0xf2c5b1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 509
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    .line 510
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 511
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 513
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPageEffectPaint:Landroid/graphics/Paint;

    .line 514
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPageEffectPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 515
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPageEffectPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 517
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectPaint:Landroid/graphics/Paint;

    .line 518
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 520
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 521
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 525
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    .line 526
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 528
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    .line 529
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 530
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 531
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 533
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;)V

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 534
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    .line 536
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    .line 537
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 539
    new-instance v3, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 540
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setEraserSize(JF)V

    .line 541
    new-instance v3, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 542
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setRemoverSize(JF)V

    .line 544
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 545
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnNotePadActionListener;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;)V

    .line 546
    new-instance v3, Landroid/widget/Toast;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    .line 548
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->initHapticFeedback()V

    .line 549
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->registerPensoundSolution()V

    .line 551
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 591
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    .line 592
    new-instance v3, Landroid/graphics/PathMeasure;

    invoke-direct {v3}, Landroid/graphics/PathMeasure;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMeasure:Landroid/graphics/PathMeasure;

    goto/16 :goto_0

    .line 446
    .end local v8    # "rect":Landroid/graphics/RectF;
    .end local v25    # "objectList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v27    # "locale":Ljava/lang/String;
    .end local v28    # "mDisplayMetrics":Landroid/util/DisplayMetrics;
    :catch_0
    move-exception v2

    .line 447
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 466
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v8    # "rect":Landroid/graphics/RectF;
    .restart local v28    # "mDisplayMetrics":Landroid/util/DisplayMetrics;
    :cond_6
    move-object/from16 v0, v28

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto/16 :goto_2
.end method

.method private convertPenNameToMaxThicknessValue(Ljava/lang/String;)I
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 2472
    const/4 v0, 0x0

    .line 2474
    .local v0, "maxValue":I
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2475
    :cond_0
    const/16 v0, 0x40

    .line 2486
    :cond_1
    :goto_0
    return v0

    .line 2476
    :cond_2
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2477
    :cond_3
    const/16 v0, 0x20

    .line 2478
    goto :goto_0

    :cond_4
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2479
    :cond_5
    const/16 v0, 0x50

    .line 2480
    goto :goto_0

    :cond_6
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2481
    const/16 v0, 0x6c

    .line 2482
    goto :goto_0

    :cond_7
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Eraser"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2483
    :cond_8
    const/16 v0, 0x64

    goto :goto_0
.end method

.method private createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 12
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v11, 0x6

    const/4 v10, 0x2

    .line 958
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1021
    :cond_0
    :goto_0
    return-void

    .line 962
    :cond_1
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    .line 963
    .local v4, "width":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    .line 964
    .local v2, "height":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    .line 965
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    .line 966
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-nez v5, :cond_2

    .line 967
    const-string v5, "The width of pageDoc is 0"

    invoke-static {v11, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 970
    :cond_2
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-nez v5, :cond_3

    .line 971
    const-string v5, "The height of pageDoc is 0"

    invoke-static {v11, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 975
    :cond_3
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-ne v4, v5, :cond_4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-ne v2, v5, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isLayerChanged()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 978
    :cond_4
    const-string v5, "SpenInView"

    .line 979
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "createBitmap Width="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Height="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " IsLayerChanged="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 980
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isLayerChanged()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 979
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 978
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_5

    .line 983
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 984
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 987
    :cond_5
    :try_start_0
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 992
    :goto_1
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-direct {p0, v6, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setBitmap(JLandroid/graphics/Bitmap;)V

    .line 994
    const-string v5, "SpenInView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Added Layer Count="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Layer Size="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    .line 996
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_removeCanvasBitmap(J)V

    .line 997
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_8

    .line 1002
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 1005
    :cond_7
    const/4 v3, 0x0

    .local v3, "pos":I
    :goto_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerCount()I

    move-result v5

    if-lt v3, v5, :cond_9

    .line 1020
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->deltaZoomSizeChanged()V

    goto/16 :goto_0

    .line 988
    .end local v3    # "pos":I
    :catch_0
    move-exception v1

    .line 989
    .local v1, "e":Ljava/lang/Throwable;
    const-string v5, "SpenInView"

    const-string v6, "Failed to create bitmap"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_1

    .line 997
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_8
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 998
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_6

    .line 999
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2

    .line 1006
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "pos":I
    :cond_9
    const/4 v0, 0x0

    .line 1008
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :try_start_1
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 1013
    :goto_4
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v5, :cond_a

    if-nez v3, :cond_a

    .line 1014
    invoke-static {v0}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_reallocBitmapJNI(Landroid/graphics/Bitmap;)V

    .line 1016
    :cond_a
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerIdByIndex(I)I

    move-result v5

    invoke-direct {p0, v6, v7, v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 1017
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1005
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1009
    :catch_1
    move-exception v1

    .line 1010
    .restart local v1    # "e":Ljava/lang/Throwable;
    const-string v5, "SpenInView"

    const-string v6, "Failed to create bitmap"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1011
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_4
.end method

.method private deltaZoomSizeChanged()V
    .locals 5

    .prologue
    .line 1580
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getVariableForOnUpdateCanvas()V

    .line 1582
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 1583
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onZoom(FFF)V

    .line 1584
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setLimitHeight(FF)V

    .line 1585
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    .line 1586
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    .line 1585
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setDrawInformation(IIII)V

    .line 1589
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_1

    .line 1590
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setRatioBitmapSize(II)V

    .line 1591
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setDeltaValue(FFFF)V

    .line 1594
    :cond_1
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onZoom. dx : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", r : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MaxDx : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1595
    const-string v2, ", MaxDy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1594
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    return-void
.end method

.method private dismissHermes()V
    .locals 5

    .prologue
    .line 2036
    :try_start_0
    new-instance v0, Lcom/samsung/android/hermes/HermesServiceManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/samsung/android/hermes/HermesServiceManager;-><init>(Landroid/content/Context;)V

    .line 2037
    .local v0, "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    if-eqz v0, :cond_0

    .line 2038
    invoke-virtual {v0}, Lcom/samsung/android/hermes/HermesServiceManager;->dismissHermes()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 2045
    .end local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    :cond_0
    :goto_0
    return-void

    .line 2040
    :catch_0
    move-exception v1

    .line 2041
    .local v1, "ie":Ljava/lang/IllegalStateException;
    const-string v3, "SpenInView"

    const-string/jumbo v4, "showHermes() IllegalStateException"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2042
    .end local v1    # "ie":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v2

    .line 2043
    .local v2, "ne":Ljava/lang/NoClassDefFoundError;
    const-string v3, "SpenInView"

    const-string/jumbo v4, "showHermes() NoClassDefFoundError"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "containerObject"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    .line 1122
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v3

    .line 1123
    .local v3, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1125
    .local v2, "objectCount":I
    const/4 v1, 0x0

    .local v1, "cnt":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 1140
    return-void

    .line 1126
    :cond_0
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1127
    .local v0, "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v0, :cond_2

    .line 1125
    .end local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1131
    .restart local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v4

    .line 1132
    .local v4, "type":I
    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 1133
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .end local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    goto :goto_1

    .line 1134
    .restart local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_3
    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 1135
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .end local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V

    goto :goto_1

    .line 1136
    .restart local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_4
    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 1137
    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .end local v0    # "base":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto :goto_1
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "imageObject"    # Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .prologue
    const/4 v7, 0x0

    .line 1098
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 1099
    .local v8, "dstRectF":Landroid/graphics/RectF;
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintText()Ljava/lang/String;

    move-result-object v9

    .line 1100
    .local v9, "hintText":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->isHintTextEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1101
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {p0, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1102
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-virtual {v8, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 1103
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextFontSize()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v6, v1, v2

    .line 1104
    .local v6, "size":F
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextVerticalOffset()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v10, v1, v2

    .line 1106
    .local v10, "verticalOffset":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v2, v7}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1107
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1108
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1109
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v9, v7, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Landroid/text/TextPaint;

    .line 1110
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/4 v5, 0x0

    .line 1109
    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 1112
    .local v0, "textLayout":Landroid/text/StaticLayout;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1113
    iget v1, v8, Landroid/graphics/RectF;->left:F

    .line 1114
    iget v2, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v10

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 1113
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1115
    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1116
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1119
    .end local v0    # "textLayout":Landroid/text/StaticLayout;
    .end local v6    # "size":F
    .end local v10    # "verticalOffset":F
    :cond_0
    return-void
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "textObject"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 1052
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    .line 1053
    .local v9, "dstRectF":Landroid/graphics/RectF;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v12

    .line 1054
    .local v12, "text":Ljava/lang/String;
    if-eqz v12, :cond_0

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1055
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintText()Ljava/lang/String;

    move-result-object v11

    .line 1056
    .local v11, "hintText":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isHintTextEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v11, :cond_4

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1057
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {p0, v9, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1058
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-virtual {v9, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 1059
    iget v1, v9, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v9, Landroid/graphics/RectF;->left:F

    .line 1060
    iget v1, v9, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v9, Landroid/graphics/RectF;->top:F

    .line 1061
    iget v1, v9, Landroid/graphics/RectF;->right:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v9, Landroid/graphics/RectF;->right:F

    .line 1062
    iget v1, v9, Landroid/graphics/RectF;->bottom:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v9, Landroid/graphics/RectF;->bottom:F

    .line 1064
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintTextFontSize()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v6, v1, v2

    .line 1065
    .local v6, "size":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getFont()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1066
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintTextColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1067
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1069
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTextAlignment()I

    move-result v8

    .line 1070
    .local v8, "align":I
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 1071
    .local v4, "alignment":Landroid/text/Layout$Alignment;
    const/4 v1, 0x2

    if-eq v8, v1, :cond_1

    const/4 v1, 0x3

    if-ne v8, v1, :cond_5

    .line 1072
    :cond_1
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    .line 1077
    :cond_2
    :goto_0
    new-instance v0, Landroid/text/StaticLayout;

    const/4 v1, 0x0

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v11, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Landroid/text/TextPaint;

    .line 1078
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 1077
    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 1080
    .local v0, "textLayout":Landroid/text/StaticLayout;
    const v1, 0x3e99999a    # 0.3f

    mul-float v13, v6, v1

    .line 1081
    .local v13, "verticalOffset":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v10

    .line 1082
    .local v10, "gravity":I
    const/4 v1, 0x1

    if-ne v10, v1, :cond_6

    .line 1083
    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v1, v6

    const/high16 v2, 0x40000000    # 2.0f

    div-float v13, v1, v2

    .line 1088
    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1089
    iget v1, v9, Landroid/graphics/RectF;->left:F

    iget v2, v9, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v13

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1090
    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1091
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1095
    .end local v0    # "textLayout":Landroid/text/StaticLayout;
    .end local v4    # "alignment":Landroid/text/Layout$Alignment;
    .end local v6    # "size":F
    .end local v8    # "align":I
    .end local v10    # "gravity":I
    .end local v11    # "hintText":Ljava/lang/String;
    .end local v13    # "verticalOffset":F
    :cond_4
    return-void

    .line 1073
    .restart local v4    # "alignment":Landroid/text/Layout$Alignment;
    .restart local v6    # "size":F
    .restart local v8    # "align":I
    .restart local v11    # "hintText":Ljava/lang/String;
    :cond_5
    const/4 v1, 0x1

    if-ne v8, v1, :cond_2

    .line 1074
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 1084
    .restart local v0    # "textLayout":Landroid/text/StaticLayout;
    .restart local v10    # "gravity":I
    .restart local v13    # "verticalOffset":F
    :cond_6
    const/4 v1, 0x2

    if-ne v10, v1, :cond_3

    .line 1085
    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float v13, v1, v6

    goto :goto_1
.end method

.method private fitControlToObject()V
    .locals 3

    .prologue
    .line 2249
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    .line 2250
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2251
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 2252
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 2257
    :cond_0
    :goto_0
    return-void

    .line 2254
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    goto :goto_0
.end method

.method private getResourceString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    .line 6338
    const/4 v1, 0x0

    .line 6339
    .local v1, "string":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v2, :cond_1

    .line 6340
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v3, "string"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 6341
    .local v0, "resId":I
    if-nez v0, :cond_0

    .line 6342
    const/4 v2, 0x0

    .line 6346
    .end local v0    # "resId":I
    :goto_0
    return-object v2

    .line 6344
    .restart local v0    # "resId":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .end local v0    # "resId":I
    :cond_1
    move-object v2, v1

    .line 6346
    goto :goto_0
.end method

.method private getVariableForOnUpdateCanvas()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1545
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v1, :cond_2

    .line 1546
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    .line 1550
    :goto_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 1551
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    .line 1554
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    if-ge v1, v2, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    .line 1555
    .local v0, "shortHeight":I
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v1, :cond_4

    .line 1556
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchHeight:I

    int-to-float v1, v1

    int-to-float v2, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    .line 1560
    :goto_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_1

    .line 1561
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    .line 1564
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v1, :cond_5

    .line 1565
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    .line 1566
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    .line 1572
    :goto_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    if-ge v1, v2, :cond_6

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    :goto_4
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    .line 1573
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    if-ge v1, v2, :cond_7

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    :goto_5
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    .line 1575
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    .line 1576
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    .line 1577
    return-void

    .line 1548
    .end local v0    # "shortHeight":I
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    goto :goto_0

    .line 1554
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    goto :goto_1

    .line 1558
    .restart local v0    # "shortHeight":I
    :cond_4
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v1, v1

    int-to-float v2, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    goto :goto_2

    .line 1568
    :cond_5
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    .line 1569
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    goto :goto_3

    .line 1572
    :cond_6
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    goto :goto_4

    .line 1573
    :cond_7
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    goto :goto_5
.end method

.method private initHapticFeedback()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2407
    const-string v2, "SpenInView"

    const-string v3, "initHapticFeedback() - Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2408
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-nez v2, :cond_0

    .line 2410
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2411
    .local v0, "dm":Landroid/util/DisplayMetrics;
    new-instance v2, Lcom/samsung/hapticfeedback/HapticEffect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/hapticfeedback/HapticEffect;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 2420
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    const-string v2, "SpenInView"

    const-string v3, "initHapticFeedback() - End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2421
    return-void

    .line 2412
    :catch_0
    move-exception v1

    .line 2413
    .local v1, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 2414
    const-string v2, "TAG"

    const-string v3, "Haptic Effect UnsatisfiedLinkError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2415
    .end local v1    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v1

    .line 2416
    .local v1, "error":Ljava/lang/NoClassDefFoundError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 2417
    const-string v2, "TAG"

    const-string v3, "Haptic Effect NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isIntersect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 2
    .param p1, "lhs"    # Landroid/graphics/RectF;
    .param p2, "rhs"    # Landroid/graphics/RectF;

    .prologue
    .line 899
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLinkPreviewEnabled(I)Z
    .locals 8
    .param p1, "toolType"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1994
    const/4 v1, 0x0

    .line 1995
    .local v1, "ret":Z
    packed-switch p1, :pswitch_data_0

    .line 2016
    :cond_0
    :goto_0
    return v1

    .line 1998
    :pswitch_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "pen_hovering"

    const/4 v6, 0x0

    .line 1999
    const/4 v7, -0x3

    .line 1998
    invoke-static {v4, v5, v6, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    if-ne v4, v2, :cond_1

    move v1, v2

    .line 2000
    :goto_1
    if-eqz v1, :cond_0

    .line 2001
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "pen_hovering_link_preview"

    const/4 v5, 0x0

    .line 2002
    const/4 v6, -0x3

    .line 2001
    invoke-static {v3, v4, v5, v6}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eq v3, v2, :cond_0

    .line 2003
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move v1, v3

    .line 1998
    goto :goto_1

    .line 2006
    :catch_0
    move-exception v0

    .line 2007
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v2, "SpenInView"

    const-string v3, "isLinkPreviewEnabled() Device is not supported"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2008
    const/4 v1, 0x0

    .line 2010
    goto :goto_0

    .line 2013
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :pswitch_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1995
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static native native_cancelStroke(I)V
.end method

.method private static native native_cancelStroke(J)V
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_drawObjectList(ILandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)Z"
        }
    .end annotation
.end method

.method private static native native_drawObjectList(JLandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)Z"
        }
    .end annotation
.end method

.method private static native native_enablePenCurve(IZ)V
.end method

.method private static native native_enablePenCurve(JZ)V
.end method

.method private static native native_enableZoom(IZ)V
.end method

.method private static native native_enableZoom(JZ)V
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getAdvancedSetting(I)Ljava/lang/String;
.end method

.method private static native native_getAdvancedSetting(J)Ljava/lang/String;
.end method

.method private static native native_getEraserSize(I)F
.end method

.method private static native native_getEraserSize(J)F
.end method

.method private static native native_getMaxZoomRatio(I)F
.end method

.method private static native native_getMaxZoomRatio(J)F
.end method

.method private static native native_getMinZoomRatio(I)F
.end method

.method private static native native_getMinZoomRatio(J)F
.end method

.method private static native native_getPan(ILandroid/graphics/PointF;)V
.end method

.method private static native native_getPan(JLandroid/graphics/PointF;)V
.end method

.method private static native native_getPenColor(I)I
.end method

.method private static native native_getPenColor(J)I
.end method

.method private static native native_getPenSize(I)F
.end method

.method private static native native_getPenSize(J)F
.end method

.method private static native native_getPenStyle(I)Ljava/lang/String;
.end method

.method private static native native_getPenStyle(J)Ljava/lang/String;
.end method

.method private static native native_getReplayState(I)I
.end method

.method private static native native_getReplayState(J)I
.end method

.method private static native native_getTemporaryStroke(ILjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation
.end method

.method private static native native_getTemporaryStroke(JLjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation
.end method

.method private static native native_getToolTypeAction(II)I
.end method

.method private static native native_getToolTypeAction(JI)I
.end method

.method private static native native_getZoomRatio(I)F
.end method

.method private static native native_getZoomRatio(J)F
.end method

.method private static native native_inVisibleUpdate(IIZZ)V
.end method

.method private static native native_inVisibleUpdate(JIZZ)V
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_isPenCurve(I)Z
.end method

.method private static native native_isPenCurve(J)Z
.end method

.method private static native native_isZoomable(I)Z
.end method

.method private static native native_isZoomable(J)Z
.end method

.method private static native native_onHover(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onHover(JLandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onLongPress(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onLongPress(JLandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onSingleTapUp(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onSingleTapUp(JLandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(JLandroid/view/MotionEvent;I)Z
.end method

.method private static native native_pauseReplay(I)Z
.end method

.method private static native native_pauseReplay(J)Z
.end method

.method private static native native_removeCanvasBitmap(I)V
.end method

.method private static native native_removeCanvasBitmap(J)V
.end method

.method private static native native_resumeReplay(I)Z
.end method

.method private static native native_resumeReplay(J)Z
.end method

.method private static native native_setAdvancedSetting(ILjava/lang/String;)V
.end method

.method private static native native_setAdvancedSetting(JLjava/lang/String;)V
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)V
.end method

.method private static native native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setEraserSize(IF)V
.end method

.method private static native native_setEraserSize(JF)V
.end method

.method private static native native_setEraserType(II)V
.end method

.method private static native native_setEraserType(JI)V
.end method

.method private static native native_setHyperTextViewEnabled(IZ)V
.end method

.method private static native native_setHyperTextViewEnabled(JZ)V
.end method

.method private static native native_setMaxZoomRatio(IF)Z
.end method

.method private static native native_setMaxZoomRatio(JF)Z
.end method

.method private static native native_setMinZoomRatio(IF)Z
.end method

.method private static native native_setMinZoomRatio(JF)Z
.end method

.method private static native native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPan(IFFZ)V
.end method

.method private static native native_setPan(JFFZ)V
.end method

.method private static native native_setPenColor(II)V
.end method

.method private static native native_setPenColor(JI)V
.end method

.method private static native native_setPenSize(IF)V
.end method

.method private static native native_setPenSize(JF)V
.end method

.method private static native native_setPenStyle(ILjava/lang/String;)Z
.end method

.method private static native native_setPenStyle(JLjava/lang/String;)Z
.end method

.method private static native native_setRemoverSize(IF)V
.end method

.method private static native native_setRemoverSize(JF)V
.end method

.method private static native native_setRemoverType(II)V
.end method

.method private static native native_setRemoverType(JI)V
.end method

.method private static native native_setReplayPosition(II)Z
.end method

.method private static native native_setReplayPosition(JI)Z
.end method

.method private static native native_setReplaySpeed(II)Z
.end method

.method private static native native_setReplaySpeed(JI)Z
.end method

.method private static native native_setScreenFrameBuffer(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setScreenFrameBuffer(JLandroid/graphics/Bitmap;)V
.end method

.method private static native native_setScreenSize(IIII)V
.end method

.method private static native native_setScreenSize(JIII)V
.end method

.method private static native native_setSelectionType(II)V
.end method

.method private static native native_setSelectionType(JI)V
.end method

.method private static native native_setToolTypeAction(III)V
.end method

.method private static native native_setToolTypeAction(JII)V
.end method

.method private static native native_setZoom(IFFF)V
.end method

.method private static native native_setZoom(JFFF)V
.end method

.method private static native native_startReplay(I)Z
.end method

.method private static native native_startReplay(J)Z
.end method

.method private static native native_startTemporaryStroke(I)V
.end method

.method private static native native_startTemporaryStroke(J)V
.end method

.method private static native native_stopReplay(I)Z
.end method

.method private static native native_stopReplay(J)Z
.end method

.method private static native native_stopTemporaryStroke(I)V
.end method

.method private static native native_stopTemporaryStroke(J)V
.end method

.method private static native native_update(I)V
.end method

.method private static native native_update(J)V
.end method

.method private static native native_updateAllScreenFrameBuffer(I)V
.end method

.method private static native native_updateAllScreenFrameBuffer(J)V
.end method

.method private static native native_updateRedo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private static native native_updateRedo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private static native native_updateUndo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private static native native_updateUndo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private onColorPickerChanged(III)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "color"    # I

    .prologue
    .line 1621
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onColorPickerChanged color"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1622
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    if-eqz v3, :cond_1

    .line 1623
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v3, :cond_0

    .line 1624
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v3, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    add-int/2addr v4, p1

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    add-int/2addr v5, p2

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getPixel(II)I

    move-result v1

    .line 1625
    .local v1, "controlColor":I
    if-eqz v1, :cond_0

    .line 1626
    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x437f0000    # 255.0f

    div-float v0, v3, v4

    .line 1627
    .local v0, "alpha":F
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v2, v3, v0

    .line 1628
    .local v2, "ialpha":F
    const/16 v3, 0xff

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    invoke-static {p3}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 1629
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    invoke-static {p3}, Landroid/graphics/Color;->green(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    .line 1630
    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v0

    invoke-static {p3}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v2

    add-float/2addr v6, v7

    float-to-int v6, v6

    .line 1628
    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result p3

    .line 1633
    .end local v0    # "alpha":F
    .end local v1    # "controlColor":I
    .end local v2    # "ialpha":F
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    invoke-interface {v3, p3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;->onChanged(III)V

    .line 1635
    :cond_1
    return-void
.end method

.method private onCompleted()V
    .locals 2

    .prologue
    .line 1645
    const-string v0, "SpenInView"

    const-string v1, "Replay onCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1646
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    if-eqz v0, :cond_0

    .line 1647
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;->onCompleted()V

    .line 1649
    :cond_0
    return-void
.end method

.method private onHoverHyperText(Landroid/view/MotionEvent;)Z
    .locals 48
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2058
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v44, v0

    const-wide/16 v46, 0x0

    cmp-long v44, v44, v46

    if-eqz v44, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v44, v0

    if-eqz v44, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v44

    if-eqz v44, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    move-object/from16 v44, v0

    if-eqz v44, :cond_0

    .line 2059
    sget-object v44, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-nez v44, :cond_1

    .line 2060
    :cond_0
    const/16 v44, 0x0

    .line 2245
    :goto_0
    return v44

    .line 2063
    :cond_1
    move-object/from16 v31, p1

    .line 2064
    .local v31, "tempEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {v31 .. v31}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    .line 2066
    .local v6, "action":I
    const/16 v44, 0xa

    move/from16 v0, v44

    if-ne v6, v0, :cond_2

    .line 2067
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2068
    const/16 v44, 0x0

    goto :goto_0

    .line 2071
    :cond_2
    const/16 v44, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v44

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isLinkPreviewEnabled(I)Z

    move-result v44

    if-nez v44, :cond_3

    .line 2072
    const/16 v44, 0x0

    goto :goto_0

    .line 2075
    :cond_3
    const/16 v44, 0x7

    move/from16 v0, v44

    if-ne v6, v0, :cond_22

    .line 2076
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v44, v0

    move/from16 v0, v44

    neg-int v0, v0

    move/from16 v44, v0

    move/from16 v0, v44

    int-to-float v0, v0

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v45, v0

    move/from16 v0, v45

    neg-int v0, v0

    move/from16 v45, v0

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, v31

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2077
    new-instance v36, Landroid/graphics/RectF;

    const/16 v44, 0x0

    const/16 v45, 0x0

    invoke-virtual/range {v31 .. v31}, Landroid/view/MotionEvent;->getX()F

    move-result v46

    invoke-virtual/range {v31 .. v31}, Landroid/view/MotionEvent;->getY()F

    move-result v47

    move-object/from16 v0, v36

    move/from16 v1, v44

    move/from16 v2, v45

    move/from16 v3, v46

    move/from16 v4, v47

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2078
    .local v36, "tempRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 2079
    new-instance v21, Landroid/graphics/PointF;

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v44, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v45, v0

    move-object/from16 v0, v21

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2081
    .local v21, "point":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v44, v0

    .line 2082
    const/16 v45, 0x2

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v46, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v47, v0

    .line 2081
    invoke-virtual/range {v44 .. v47}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->findTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v19

    check-cast v19, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 2083
    .local v19, "objectText":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    if-eqz v19, :cond_4

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isSelectable()Z

    move-result v44

    if-eqz v44, :cond_4

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isVisible()Z

    move-result v44

    if-nez v44, :cond_5

    .line 2084
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2085
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2088
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v44, v0

    if-eqz v44, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    move/from16 v44, v0

    if-eqz v44, :cond_6

    .line 2089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v44, v0

    check-cast v44, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_6

    .line 2090
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v44, v0

    check-cast v44, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v44

    if-eqz v44, :cond_6

    .line 2091
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2092
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2095
    :cond_6
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v44

    if-nez v44, :cond_7

    .line 2096
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2097
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2100
    :cond_7
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->length()I

    move-result v37

    .line 2102
    .local v37, "textLength":I
    if-gtz v37, :cond_8

    .line 2103
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2104
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2107
    :cond_8
    new-instance v16, Landroid/graphics/PointF;

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v44, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v45, v0

    move-object/from16 v0, v16

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2109
    .local v16, "inPoint":Landroid/graphics/PointF;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v25

    .line 2110
    .local v25, "rotate":F
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getDrawnRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 2111
    .local v10, "drawRect":Landroid/graphics/RectF;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v18

    .line 2113
    .local v18, "objectRect":Landroid/graphics/RectF;
    move/from16 v0, v25

    float-to-double v0, v0

    move-wide/from16 v44, v0

    const-wide/16 v46, 0x0

    cmpl-double v44, v44, v46

    if-eqz v44, :cond_9

    .line 2114
    const/high16 v44, 0x43b40000    # 360.0f

    sub-float v44, v44, v25

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    const-wide v46, 0x4066800000000000L    # 180.0

    div-double v44, v44, v46

    const-wide v46, 0x400921fb54442d18L    # Math.PI

    mul-double v22, v44, v46

    .line 2115
    .local v22, "rad":D
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    .line 2116
    .local v8, "cosq":D
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sin(D)D

    move-result-wide v26

    .line 2117
    .local v26, "sinq":D
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v44, v0

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    move-result v45

    sub-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v32, v0

    .line 2118
    .local v32, "sx":D
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v44, v0

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    move-result v45

    sub-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v34, v0

    .line 2119
    .local v34, "sy":D
    mul-double v44, v32, v8

    mul-double v46, v34, v26

    sub-double v44, v44, v46

    move-wide/from16 v0, v44

    double-to-float v0, v0

    move/from16 v44, v0

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    move-result v45

    add-float v44, v44, v45

    move/from16 v0, v44

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2120
    mul-double v44, v32, v26

    mul-double v46, v34, v8

    add-double v44, v44, v46

    move-wide/from16 v0, v44

    double-to-float v0, v0

    move/from16 v44, v0

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    move-result v45

    add-float v44, v44, v45

    move/from16 v0, v44

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2122
    .end local v8    # "cosq":D
    .end local v22    # "rad":D
    .end local v26    # "sinq":D
    .end local v32    # "sx":D
    .end local v34    # "sy":D
    :cond_9
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v44, v0

    move/from16 v0, v44

    neg-float v0, v0

    move/from16 v44, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v45, v0

    move/from16 v0, v45

    neg-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, v16

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->offset(FF)V

    .line 2124
    const/4 v14, -0x1

    .line 2125
    .local v14, "hoverIndex":I
    new-instance v38, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;

    invoke-direct/range {v38 .. v38}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;-><init>()V

    .line 2126
    .local v38, "textMeasure":Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;
    move-object/from16 v0, v38

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    .line 2129
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move/from16 v0, v37

    if-lt v15, v0, :cond_b

    .line 2137
    :goto_2
    if-ltz v14, :cond_a

    move/from16 v0, v37

    if-lt v14, v0, :cond_d

    .line 2138
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2139
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2130
    :cond_b
    move-object/from16 v0, v38

    invoke-virtual {v0, v15}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->getTextRect(I)Landroid/graphics/RectF;

    move-result-object v39

    .line 2131
    .local v39, "textRect":Landroid/graphics/RectF;
    if-eqz v39, :cond_c

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v44, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v45, v0

    move-object/from16 v0, v39

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v44

    if-eqz v44, :cond_c

    .line 2132
    move v14, v15

    .line 2133
    goto :goto_2

    .line 2129
    :cond_c
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 2142
    .end local v39    # "textRect":Landroid/graphics/RectF;
    :cond_d
    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v14}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v29

    .line 2144
    .local v29, "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    if-nez v29, :cond_e

    .line 2145
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2146
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2149
    :cond_e
    const/16 v28, 0x0

    .line 2151
    .local v28, "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;
    const/4 v15, 0x0

    :goto_3
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v44

    move/from16 v0, v44

    if-lt v15, v0, :cond_f

    .line 2161
    :goto_4
    if-nez v28, :cond_11

    .line 2162
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2163
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2152
    :cond_f
    move-object/from16 v0, v29

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    .line 2153
    .local v40, "tmpSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    if-eqz v40, :cond_10

    .line 2154
    move-object/from16 v0, v40

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    move/from16 v44, v0

    if-eqz v44, :cond_10

    move-object/from16 v44, v40

    .line 2155
    check-cast v44, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    move-object/from16 v0, v44

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->hyperTextType:I

    move/from16 v44, v0

    const/16 v45, 0x3

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_10

    move-object/from16 v28, v40

    .line 2156
    check-cast v28, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    .line 2157
    goto :goto_4

    .line 2151
    :cond_10
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 2166
    .end local v40    # "tmpSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;
    :cond_11
    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->startPos:I

    move/from16 v44, v0

    if-lez v44, :cond_13

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->startPos:I

    move/from16 v30, v0

    .line 2167
    .local v30, "startPos":I
    :goto_5
    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->endPos:I

    move/from16 v44, v0

    move/from16 v0, v44

    move/from16 v1, v37

    if-le v0, v1, :cond_14

    move/from16 v11, v37

    .line 2168
    .local v11, "endPos":I
    :goto_6
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v30

    invoke-virtual {v0, v1, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v42

    .line 2170
    .local v42, "url":Ljava/lang/String;
    if-eqz v42, :cond_12

    invoke-virtual/range {v42 .. v42}, Ljava/lang/String;->length()I

    move-result v44

    if-gtz v44, :cond_15

    .line 2171
    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2172
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2166
    .end local v11    # "endPos":I
    .end local v30    # "startPos":I
    .end local v42    # "url":Ljava/lang/String;
    :cond_13
    const/16 v30, 0x0

    goto :goto_5

    .line 2167
    .restart local v30    # "startPos":I
    :cond_14
    move-object/from16 v0, v28

    iget v11, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->endPos:I

    goto :goto_6

    .line 2175
    .restart local v11    # "endPos":I
    .restart local v42    # "url":Ljava/lang/String;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    move-object/from16 v44, v0

    if-eqz v44, :cond_16

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->startPos:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->startPos:I

    move/from16 v45, v0

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_16

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->endPos:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;->endPos:I

    move/from16 v45, v0

    move/from16 v0, v44

    move/from16 v1, v45

    if-eq v0, v1, :cond_22

    .line 2176
    :cond_16
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnterTime:J

    move-wide/from16 v44, v0

    const-wide/16 v46, 0x0

    cmp-long v44, v44, v46

    if-lez v44, :cond_21

    .line 2177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v44

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnterTime:J

    move-wide/from16 v46, v0

    sub-long v12, v44, v46

    .line 2178
    .local v12, "elipse":J
    const-wide/16 v44, 0x12c

    cmp-long v44, v12, v44

    if-lez v44, :cond_20

    .line 2179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    move-object/from16 v44, v0

    if-eqz v44, :cond_17

    .line 2180
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->dismissHermes()V

    .line 2182
    :cond_17
    const-wide/16 v44, -0x1

    move-wide/from16 v0, v44

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnterTime:J

    .line 2183
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    .line 2185
    const/16 v43, 0x0

    .line 2186
    .local v43, "urlRect":Landroid/graphics/RectF;
    move/from16 v15, v30

    :goto_7
    if-lt v15, v11, :cond_19

    .line 2205
    :cond_18
    if-nez v43, :cond_1d

    .line 2206
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resetVariableForHoverHyperText()V

    .line 2207
    const/16 v44, 0x0

    goto/16 :goto_0

    .line 2187
    :cond_19
    move-object/from16 v0, v38

    invoke-virtual {v0, v15}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->getTextRect(I)Landroid/graphics/RectF;

    move-result-object v39

    .line 2188
    .restart local v39    # "textRect":Landroid/graphics/RectF;
    if-nez v39, :cond_1a

    .line 2186
    :goto_8
    add-int/lit8 v15, v15, 0x1

    goto :goto_7

    .line 2191
    :cond_1a
    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v44, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v45, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v46, v0

    sub-float v45, v45, v46

    cmpl-float v44, v44, v45

    if-gez v44, :cond_18

    .line 2194
    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v44, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v45, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v46, v0

    sub-float v45, v45, v46

    cmpl-float v44, v44, v45

    if-lez v44, :cond_1b

    .line 2195
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v44, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v45, v0

    sub-float v44, v44, v45

    move/from16 v0, v44

    move-object/from16 v1, v39

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 2197
    :cond_1b
    if-nez v43, :cond_1c

    .line 2198
    move-object/from16 v43, v39

    .line 2199
    goto :goto_8

    .line 2200
    :cond_1c
    move-object/from16 v0, v43

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    goto :goto_8

    .line 2210
    .end local v39    # "textRect":Landroid/graphics/RectF;
    :cond_1d
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v44, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v45, v0

    invoke-virtual/range {v43 .. v45}, Landroid/graphics/RectF;->offset(FF)V

    .line 2211
    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move-object/from16 v2, v43

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 2212
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 2213
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v44, v0

    move/from16 v0, v44

    int-to-float v0, v0

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v45, v0

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {v43 .. v45}, Landroid/graphics/RectF;->offset(FF)V

    .line 2214
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v44, v0

    move/from16 v0, v44

    int-to-float v0, v0

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v45, v0

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    move-object/from16 v0, v18

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 2216
    move/from16 v0, v25

    float-to-double v0, v0

    move-wide/from16 v44, v0

    const-wide/16 v46, 0x0

    cmpl-double v44, v44, v46

    if-eqz v44, :cond_1e

    .line 2217
    new-instance v17, Landroid/graphics/Matrix;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Matrix;-><init>()V

    .line 2218
    .local v17, "matrix":Landroid/graphics/Matrix;
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Matrix;->reset()V

    .line 2219
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->centerX()F

    move-result v44

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->centerY()F

    move-result v45

    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v44

    move/from16 v3, v45

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 2220
    move-object/from16 v0, v17

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 2223
    .end local v17    # "matrix":Landroid/graphics/Matrix;
    :cond_1e
    new-instance v20, Landroid/graphics/Rect;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Rect;-><init>()V

    .line 2224
    .local v20, "offsetRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2225
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v44, v0

    move/from16 v0, v44

    int-to-float v0, v0

    move/from16 v44, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v45, v0

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {v43 .. v45}, Landroid/graphics/RectF;->offset(FF)V

    .line 2227
    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v44, v0

    move/from16 v0, v44

    float-to-int v0, v0

    move/from16 v41, v0

    .line 2228
    .local v41, "top":I
    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v44, v0

    move/from16 v0, v44

    float-to-int v7, v0

    .line 2229
    .local v7, "bottom":I
    move/from16 v0, v41

    if-le v0, v7, :cond_1f

    .line 2230
    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v44, v0

    move/from16 v0, v44

    float-to-int v0, v0

    move/from16 v41, v0

    .line 2231
    move-object/from16 v0, v43

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v44, v0

    move/from16 v0, v44

    float-to-int v7, v0

    .line 2234
    :cond_1f
    new-instance v24, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v44

    move/from16 v0, v44

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v45

    move/from16 v0, v45

    float-to-int v0, v0

    move/from16 v45, v0

    add-int/lit8 v45, v45, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v44

    move/from16 v2, v41

    move/from16 v3, v45

    invoke-direct {v0, v1, v2, v3, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2235
    .local v24, "rawRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    move-object/from16 v1, v42

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->showHermes(Ljava/lang/String;Landroid/graphics/Rect;)V

    .line 2242
    .end local v7    # "bottom":I
    .end local v12    # "elipse":J
    .end local v20    # "offsetRect":Landroid/graphics/Rect;
    .end local v24    # "rawRect":Landroid/graphics/Rect;
    .end local v41    # "top":I
    .end local v43    # "urlRect":Landroid/graphics/RectF;
    :cond_20
    :goto_9
    const/16 v44, 0x1

    goto/16 :goto_0

    .line 2238
    :cond_21
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v44

    move-wide/from16 v0, v44

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnterTime:J

    .line 2239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenIconMoreHandler:Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

    move-object/from16 v44, v0

    const/16 v45, 0x1

    invoke-virtual/range {v44 .. v45}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;->setSpenIconMore(Z)V

    .line 2240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenIconMoreHandler:Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenIconMoreHandler:Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

    move-object/from16 v45, v0

    invoke-virtual/range {v44 .. v45}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_9

    .line 2245
    .end local v10    # "drawRect":Landroid/graphics/RectF;
    .end local v11    # "endPos":I
    .end local v14    # "hoverIndex":I
    .end local v15    # "i":I
    .end local v16    # "inPoint":Landroid/graphics/PointF;
    .end local v18    # "objectRect":Landroid/graphics/RectF;
    .end local v19    # "objectText":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .end local v21    # "point":Landroid/graphics/PointF;
    .end local v25    # "rotate":F
    .end local v28    # "span":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;
    .end local v29    # "spans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;>;"
    .end local v30    # "startPos":I
    .end local v36    # "tempRect":Landroid/graphics/RectF;
    .end local v37    # "textLength":I
    .end local v38    # "textMeasure":Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;
    .end local v42    # "url":Ljava/lang/String;
    :cond_22
    const/16 v44, 0x0

    goto/16 :goto_0
.end method

.method private onHyperText(Ljava/lang/String;II)V
    .locals 2
    .param p1, "hyperText"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "handle"    # I

    .prologue
    .line 1987
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    .line 1988
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 1989
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    invoke-interface {v1, p1, p2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;->onSelected(Ljava/lang/String;ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1991
    .end local v0    # "textBox":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :cond_0
    return-void
.end method

.method private onPageDocCanceled(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 1660
    const-string v0, "SpenInView"

    const-string v1, "onPageDocCanceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1661
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    if-eqz v0, :cond_0

    .line 1662
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;->onCanceled(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1664
    :cond_0
    return-void
.end method

.method private onPageDocCompleted(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 1652
    const-string v0, "SpenInView"

    const-string v1, "onPageDocCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1653
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    if-eqz v0, :cond_0

    .line 1654
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 1655
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;->onCompleted(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1657
    :cond_0
    return-void
.end method

.method private onProgressChanged(II)V
    .locals 1
    .param p1, "progress"    # I
    .param p2, "id"    # I

    .prologue
    .line 1639
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    if-eqz v0, :cond_0

    .line 1640
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;->onProgressChanged(II)V

    .line 1642
    :cond_0
    return-void
.end method

.method private onSelectObject(Ljava/util/ArrayList;IIFFI)Z
    .locals 21
    .param p2, "toolType"    # I
    .param p3, "pressType"    # I
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "userdata"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;IIFFI)Z"
        }
    .end annotation

    .prologue
    .line 1780
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v2, :cond_0

    if-nez p3, :cond_0

    .line 1781
    const/4 v2, 0x0

    .line 1976
    :goto_0
    return v2

    .line 1782
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v2, :cond_2

    .line 1783
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isTouchEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1784
    const/4 v2, 0x0

    goto :goto_0

    .line 1787
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 1790
    :cond_2
    if-nez p1, :cond_3

    .line 1791
    const-string v2, "SpenInView"

    const-string v3, "onSelectObject ObjectList is nulll"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1792
    const/4 v2, 0x0

    goto :goto_0

    .line 1795
    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 1796
    const-string v2, "SpenInView"

    const-string v3, "onSelectObject : selected list size is zero."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1797
    const/4 v2, 0x0

    goto :goto_0

    .line 1800
    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1801
    .local v4, "relativeObjectRectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1802
    .local v5, "menuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1803
    .local v6, "styleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v8, Landroid/graphics/PointF;

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v8, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1805
    .local v8, "point":Landroid/graphics/PointF;
    new-instance v19, Landroid/graphics/RectF;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/RectF;-><init>()V

    .line 1806
    .local v19, "tempRelativeRectF":Landroid/graphics/RectF;
    const/16 v18, 0x0

    .line 1807
    .local v18, "tempObject":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    new-instance v10, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-direct {v10, v2, v3, v7, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1809
    .local v10, "boundaryRect":Landroid/graphics/Rect;
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "onSelectObject : objectList.size() ="

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1811
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v14, v2, :cond_5

    .line 1832
    const-string v2, "SpenInView"

    const-string v3, "onSelectObject : boundaryRect finished"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1834
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1837
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_9

    .line 1838
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1839
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    if-eqz v2, :cond_7

    .line 1840
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-object/from16 v3, p1

    move/from16 v7, p3

    invoke-interface/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z

    move-result v11

    .line 1842
    .local v11, "continueProcess":Z
    if-nez v11, :cond_7

    .line 1843
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1812
    .end local v11    # "continueProcess":Z
    :cond_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "tempObject":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1814
    .restart local v18    # "tempObject":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v18, :cond_6

    .line 1815
    const-string v2, "SpenInView"

    const-string v3, "onSelectObject : object is null."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1816
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1824
    :cond_6
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 1825
    .local v17, "relativeRect":Landroid/graphics/Rect;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1826
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1827
    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1829
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 1811
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 1847
    .end local v17    # "relativeRect":Landroid/graphics/Rect;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getSelectedObjectCount()I

    move-result v2

    if-nez v2, :cond_8

    .line 1849
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->selectObject(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1859
    :cond_8
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1860
    .local v12, "control":Lcom/samsung/android/sdk/pen/engine/SpenControlList;
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setStyle(I)V

    .line 1863
    invoke-virtual {v12, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setContextMenu(Ljava/util/ArrayList;)V

    .line 1864
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObject(Ljava/util/ArrayList;)V

    .line 1865
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    .line 1866
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1850
    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlList;
    :catch_0
    move-exception v13

    .line 1851
    .local v13, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "SpenInView"

    const-string v3, "selectObject : object is not in current layer"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1852
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1853
    .end local v13    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v13

    .line 1854
    .local v13, "e":Ljava/lang/IllegalStateException;
    const-string v2, "SpenInView"

    const-string v3, "selectObject : pageDoc is not loaded"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1855
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1870
    .end local v13    # "e":Ljava/lang/IllegalStateException;
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1871
    .local v16, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    const/4 v12, 0x0

    .line 1873
    .local v12, "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getSelectedObjectCount()I

    move-result v2

    if-nez v2, :cond_a

    .line 1875
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->selectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1885
    :cond_a
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1945
    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    if-eqz v2, :cond_10

    .line 1948
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-object/from16 v3, p1

    move/from16 v7, p3

    invoke-interface/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z

    move-result v11

    .line 1950
    .restart local v11    # "continueProcess":Z
    if-nez v11, :cond_10

    .line 1951
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1876
    .end local v11    # "continueProcess":Z
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :catch_2
    move-exception v13

    .line 1877
    .local v13, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "SpenInView"

    const-string v3, "selectObject : object is not in current layer"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1878
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1879
    .end local v13    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v13

    .line 1880
    .local v13, "e":Ljava/lang/IllegalStateException;
    const-string v2, "SpenInView"

    const-string v3, "selectObject : pageDoc is not loaded"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1881
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1887
    .end local v13    # "e":Ljava/lang/IllegalStateException;
    :pswitch_0
    const-string v2, "SpenInView"

    const-string v3, "Text Selection"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1889
    const/4 v2, 0x1

    move/from16 v0, p6

    if-ne v0, v2, :cond_b

    .line 1891
    new-instance v9, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v9, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 1892
    .local v9, "ControlTextBox":Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    .line 1894
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextEraserEnabled(Z)V

    .line 1896
    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1897
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    .line 1898
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setDimEnabled(Z)V

    .line 1899
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    .line 1900
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1903
    .end local v9    # "ControlTextBox":Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_b
    const/4 v15, 0x1

    .line 1904
    .local v15, "isEditable":Z
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_c

    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_c

    .line 1905
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_c

    .line 1906
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_c

    .line 1907
    const/4 v2, 0x2

    move/from16 v0, p3

    if-ne v0, v2, :cond_d

    .line 1908
    :cond_c
    const/4 v15, 0x0

    .line 1911
    :cond_d
    if-eqz v15, :cond_e

    .line 1912
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_e
    move-object/from16 v2, v16

    .line 1915
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->applyTextSetting(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1916
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .restart local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object v2, v12

    .line 1917
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1918
    if-nez p3, :cond_f

    if-eqz v15, :cond_f

    move-object v2, v12

    .line 1919
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setShowSoftInputEnable(Z)V

    :cond_f
    move-object v2, v12

    .line 1921
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v2, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    move-object v2, v12

    .line 1922
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setDimEnabled(Z)V

    goto/16 :goto_2

    .line 1927
    .end local v15    # "isEditable":Z
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :pswitch_1
    const-string v2, "SpenInView"

    const-string v3, "TYPE_IMAGE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1928
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;

    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .restart local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object v2, v12

    .line 1929
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V

    goto/16 :goto_2

    .line 1933
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :pswitch_2
    const-string v2, "SpenInView"

    const-string v3, "TYPE_STROKE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1934
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;

    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .restart local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object v2, v12

    .line 1935
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    goto/16 :goto_2

    .line 1939
    .restart local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :pswitch_3
    const-string v2, "SpenInView"

    const-string v3, "TYPE_CONTAINER"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1940
    new-instance v12, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;

    .end local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v12, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .restart local v12    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    move-object v2, v12

    .line 1941
    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;

    check-cast v16, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .end local v16    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto/16 :goto_2

    .line 1955
    :cond_10
    if-eqz v12, :cond_11

    .line 1956
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setStyle(I)V

    .line 1957
    invoke-virtual {v12, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenu(Ljava/util/ArrayList;)V

    .line 1959
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setFocusable(Z)V

    .line 1960
    invoke-virtual {v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->requestFocus()Z

    .line 1961
    const v2, 0x3ac90

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setNextFocusDownId(I)V

    .line 1962
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    invoke-virtual {v12, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1972
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    .line 1973
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1976
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1885
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private onShowRemoverMessage()V
    .locals 3

    .prologue
    .line 6350
    const-string v0, "SpenInView"

    const-string v1, "onShowRemoverMessage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6352
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 6353
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "string_unable_to_erase_heavy_lines"

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6354
    const/4 v2, 0x0

    .line 6353
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    .line 6366
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 6367
    return-void
.end method

.method private onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 2
    .param p1, "rectf"    # Landroid/graphics/RectF;
    .param p2, "isScreenFramebuffer"    # Z

    .prologue
    .line 1320
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1321
    const/4 p1, 0x0

    .line 1323
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    if-eqz v0, :cond_2

    .line 1324
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1325
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 1326
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 1327
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 1328
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 1337
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1338
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    .line 1344
    :cond_2
    :goto_0
    return-void

    .line 1340
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method private onZoom(FFF)V
    .locals 2
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 1601
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    .line 1602
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    .line 1603
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    .line 1605
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->deltaZoomSizeChanged()V

    .line 1607
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    if-eqz v0, :cond_0

    .line 1608
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;->onZoom(FFF)V

    .line 1611
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    .line 1612
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_1

    .line 1613
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->fit(Z)V

    .line 1615
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onZoom()V

    .line 1617
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    .line 1618
    return-void
.end method

.method private raiseOnKeyDown()V
    .locals 4

    .prologue
    const/16 v3, 0x16

    .line 1980
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    .line 1982
    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 1981
    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 1984
    :cond_0
    return-void
.end method

.method private registerPensoundSolution()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 2433
    const-string v1, "SpenInView"

    const-string v2, "registerPensoundSolution() - Start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2435
    :try_start_0
    sget-boolean v1, Lcom/samsung/audio/SmpsManager;->isSupport:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 2445
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-nez v1, :cond_0

    .line 2446
    new-instance v1, Lcom/samsung/audio/SmpsManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/audio/SmpsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 2447
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v1, :cond_0

    .line 2448
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    .line 2449
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexMarker:I

    .line 2450
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexBrush:I

    .line 2451
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexEraser:I

    .line 2452
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    if-eq v1, v3, :cond_1

    .line 2453
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 2459
    :cond_0
    :goto_0
    const-string v1, "SpenInView"

    const-string v2, "registerPensoundSolution() - End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2460
    :goto_1
    return-void

    .line 2436
    :catch_0
    move-exception v0

    .line 2437
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SpenInView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2438
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    goto :goto_1

    .line 2440
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 2441
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    const-string v1, "SpenInView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2442
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    goto :goto_1

    .line 2454
    .end local v0    # "error":Ljava/lang/NoClassDefFoundError;
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    if-eq v1, v3, :cond_0

    .line 2455
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto :goto_0
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 892
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 893
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 894
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 895
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 896
    return-void
.end method

.method private releaseHapticFeedback()V
    .locals 2

    .prologue
    .line 2424
    const-string v0, "SpenInView"

    const-string v1, "releaseHapticFeedback() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2425
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v0, :cond_0

    .line 2426
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v0}, Lcom/samsung/hapticfeedback/HapticEffect;->closeDevice()V

    .line 2427
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 2429
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "releaseHapticFeedback() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2430
    return-void
.end method

.method private removeHoveringIcon(I)Z
    .locals 6
    .param p1, "nHoverIconID"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2260
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    if-eqz v4, :cond_0

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-nez v4, :cond_1

    .line 2297
    :cond_0
    :goto_0
    return v2

    .line 2263
    :cond_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    .line 2264
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2265
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 2266
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2274
    :try_start_0
    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V

    .line 2275
    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->removeHoveringSpenCustomIcon(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    move v2, v3

    .line 2297
    goto :goto_0

    .line 2276
    :catch_0
    move-exception v0

    .line 2277
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2278
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2280
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2281
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2282
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2284
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v0

    .line 2285
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2286
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 2288
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v0

    .line 2289
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2290
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 2292
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 2293
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "SpenInView"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2294
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private resetVariableForHoverHyperText()V
    .locals 2

    .prologue
    .line 2048
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    if-eqz v0, :cond_0

    .line 2049
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->dismissHermes()V

    .line 2050
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoveredSpan:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$HyperTextStyleSpanInfo;

    .line 2052
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnterTime:J

    .line 2053
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenIconMoreHandler:Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;->setSpenIconMore(Z)V

    .line 2054
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenIconMoreHandler:Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenIconMoreHandler:Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SpenIconMoreHandler;->post(Ljava/lang/Runnable;)Z

    .line 2055
    return-void
.end method

.method private savePngFile(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 3030
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3033
    .local v1, "file":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 3034
    .local v2, "filestream":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 3035
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3041
    .end local v2    # "filestream":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 3036
    :catch_0
    move-exception v0

    .line 3037
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 3038
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 3039
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCustomHoveringIcon()I
    .locals 8

    .prologue
    const/4 v3, -0x1

    .line 2301
    const/4 v1, -0x1

    .line 2302
    .local v1, "nHoverIconID":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_1

    :cond_0
    move v3, v1

    .line 2344
    :goto_0
    return v3

    .line 2305
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    .line 2307
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 2308
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_2

    .line 2309
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    move v3, v1

    .line 2310
    goto :goto_0

    :cond_2
    move v3, v1

    .line 2313
    goto :goto_0

    .line 2317
    :cond_3
    :try_start_0
    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 2318
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 2317
    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(ILandroid/graphics/drawable/Drawable;Landroid/graphics/Point;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move-result v1

    move v3, v1

    .line 2344
    goto :goto_0

    .line 2319
    :catch_0
    move-exception v0

    .line 2320
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2321
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2323
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2324
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() NotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2325
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2327
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_2
    move-exception v0

    .line 2328
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2329
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2331
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_3
    move-exception v0

    .line 2332
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2333
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 2335
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v0

    .line 2336
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() IllegalAccessException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2337
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 2339
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v0

    .line 2340
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "SpenInView"

    const-string v5, "setCustomHoveringIcon() InvocationTargetException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2341
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 2387
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2392
    :goto_0
    return-void

    .line 2390
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "setHoverPointerDrawable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2391
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private setHoveringSpenIcon(I)Z
    .locals 5
    .param p1, "typeIcon"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2348
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_1

    .line 2383
    :cond_0
    :goto_0
    return v2

    .line 2351
    :cond_1
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    .line 2353
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2354
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 2355
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2363
    :try_start_0
    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move v2, v3

    .line 2383
    goto :goto_0

    .line 2364
    :catch_0
    move-exception v0

    .line 2365
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SpenInView"

    const-string v4, "setHoveringSpenIcon() IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2367
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2368
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v3, "SpenInView"

    const-string v4, "setHoveringSpenIcon() NotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2370
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_2
    move-exception v0

    .line 2371
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "SpenInView"

    const-string v4, "setHoveringSpenIcon() ClassNotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2373
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_3
    move-exception v0

    .line 2374
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "SpenInView"

    const-string v4, "setHoveringSpenIcon() NoSuchMethodException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2376
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v0

    .line 2377
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "SpenInView"

    const-string v4, "setHoveringSpenIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2379
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v0

    .line 2380
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "SpenInView"

    const-string v4, "setHoveringSpenIcon() InvocationTargetException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setPenHoverPoint(Ljava/lang/String;)V
    .locals 4
    .param p1, "penname"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x3

    const/16 v2, 0x32

    .line 6370
    if-nez p1, :cond_0

    .line 6399
    :goto_0
    return-void

    .line 6373
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    if-nez v0, :cond_1

    .line 6374
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 6376
    :cond_1
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 6377
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MontblancFountainPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 6378
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 6379
    :cond_3
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    .line 6380
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MontblancCalligraphyPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 6381
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 6382
    :cond_5
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 6383
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 6384
    :cond_6
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    .line 6385
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 6386
    :cond_7
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 6387
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 6388
    :cond_8
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 6389
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 6390
    :cond_9
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    .line 6391
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 6392
    :cond_a
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_b

    .line 6393
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 6394
    :cond_b
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_c

    .line 6395
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 6397
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0
.end method

.method private showHermes(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 2020
    const/4 v0, 0x0

    .line 2022
    .local v0, "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    :try_start_0
    new-instance v1, Lcom/samsung/android/hermes/HermesServiceManager;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Lcom/samsung/android/hermes/HermesServiceManager;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 2023
    .end local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    .local v1, "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    if-eqz v1, :cond_0

    .line 2024
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {v1, p1, p2, v4}, Lcom/samsung/android/hermes/HermesServiceManager;->showHermes(Ljava/lang/String;Landroid/graphics/Rect;I)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_1 .. :try_end_1} :catch_2

    move-object v0, v1

    .line 2032
    .end local v1    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    .restart local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    :goto_0
    return-void

    .line 2026
    :catch_0
    move-exception v2

    .line 2027
    .local v2, "ie":Ljava/lang/IllegalStateException;
    :goto_1
    const-string v4, "SpenInView"

    const-string/jumbo v5, "showHermes() IllegalStateException"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2029
    .end local v2    # "ie":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v3

    .line 2030
    .local v3, "ne":Ljava/lang/NoClassDefFoundError;
    :goto_2
    const-string v4, "SpenInView"

    const-string/jumbo v5, "showHermes() NoClassDefFoundError"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2029
    .end local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    .end local v3    # "ne":Ljava/lang/NoClassDefFoundError;
    .restart local v1    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    :catch_2
    move-exception v3

    move-object v0, v1

    .end local v1    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    .restart local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    goto :goto_2

    .line 2026
    .end local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    .restart local v1    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    :catch_3
    move-exception v2

    move-object v0, v1

    .end local v1    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    .restart local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    goto :goto_1

    .end local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    .restart local v1    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    :cond_0
    move-object v0, v1

    .end local v1    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    .restart local v0    # "hermesManager":Lcom/samsung/android/hermes/HermesServiceManager;
    goto :goto_0
.end method

.method private unregisterPensoundSolution()V
    .locals 2

    .prologue
    .line 2463
    const-string v0, "SpenInView"

    const-string/jumbo v1, "unregisterPensoundSolution() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2464
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_0

    .line 2465
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    invoke-virtual {v0}, Lcom/samsung/audio/SmpsManager;->onDestroy()V

    .line 2466
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 2468
    :cond_0
    const-string v0, "SpenInView"

    const-string/jumbo v1, "unregisterPensoundSolution() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2469
    return-void
.end method

.method private updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "re_rectF"    # Landroid/graphics/RectF;
    .param p3, "isScreenFramebuffer"    # Z

    .prologue
    .line 1215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1317
    :cond_0
    :goto_0
    return-void

    .line 1218
    :cond_1
    if-eqz p2, :cond_2

    .line 1219
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    .line 1220
    const/16 p2, 0x0

    .line 1224
    :cond_2
    if-eqz p1, :cond_0

    .line 1227
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1228
    .local v3, "srcDrawRect":Landroid/graphics/Rect;
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 1229
    .local v12, "dstDrawRect":Landroid/graphics/Rect;
    if-nez p2, :cond_6

    .line 1230
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v7, v2

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 1231
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-virtual {v12, v2, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 1232
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v12, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 1235
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v2, v2

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_3

    .line 1236
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v8, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1237
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    add-int/2addr v2, v4

    int-to-float v5, v2

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    int-to-float v8, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1239
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    .line 1240
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v8, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1241
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    add-int/2addr v2, v4

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    int-to-float v8, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1249
    :cond_4
    :goto_1
    if-eqz p3, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v4

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_8

    .line 1250
    :cond_5
    new-instance v17, Landroid/graphics/Rect;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1251
    .local v17, "srcRectFB":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    neg-int v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    neg-int v4, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 1252
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1254
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v2, :cond_7

    .line 1255
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1, v12, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1244
    .end local v17    # "srcRectFB":Landroid/graphics/Rect;
    :cond_6
    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p2

    iget v6, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 1245
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v12, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    .line 1246
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v12, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_1

    .line 1257
    .restart local v17    # "srcRectFB":Landroid/graphics/Rect;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1, v12, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1260
    .end local v17    # "srcRectFB":Landroid/graphics/Rect;
    :cond_8
    new-instance v16, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1261
    .local v16, "srcRect":Landroid/graphics/Rect;
    new-instance v13, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v6, v6

    invoke-direct {v13, v2, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1263
    .local v13, "dstRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_b

    .line 1264
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iput v2, v13, Landroid/graphics/RectF;->left:F

    .line 1265
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iput v2, v13, Landroid/graphics/RectF;->right:F

    .line 1266
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iput v2, v13, Landroid/graphics/RectF;->top:F

    .line 1267
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iput v2, v13, Landroid/graphics/RectF;->bottom:F

    .line 1275
    :goto_2
    const/4 v15, 0x0

    .local v15, "layer":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v15, v2, :cond_0

    .line 1276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    .line 1277
    .local v11, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v11, :cond_a

    .line 1278
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v11, v5, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-virtual {v11, v2, v4, v5}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v2, :cond_10

    .line 1280
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v2, v4

    .line 1281
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v5

    mul-int/2addr v4, v5

    .line 1280
    add-int v10, v2, v4

    .line 1282
    .local v10, "area":I
    const v2, 0x3f4800

    if-le v10, v2, :cond_9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-nez v2, :cond_d

    .line 1284
    :cond_9
    if-nez v15, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_4
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1, v13, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1275
    .end local v10    # "area":I
    :cond_a
    :goto_5
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 1269
    .end local v11    # "bitmap":Landroid/graphics/Bitmap;
    .end local v15    # "layer":I
    :cond_b
    new-instance v14, Landroid/graphics/RectF;

    invoke-direct {v14}, Landroid/graphics/RectF;-><init>()V

    .line 1270
    .local v14, "dstRelRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1271
    iget v2, v14, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iget v4, v14, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, v14, Landroid/graphics/RectF;->right:F

    .line 1272
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, v14, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    .line 1271
    invoke-virtual {v13, v2, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    .line 1284
    .end local v14    # "dstRelRect":Landroid/graphics/RectF;
    .restart local v10    # "area":I
    .restart local v11    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v15    # "layer":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_4

    .line 1290
    :cond_d
    if-nez v15, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    .line 1289
    :goto_6
    move-object/from16 v0, p1

    invoke-static {v0, v11, v3, v12, v2}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_drawBitmapJNI(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)I

    move-result v2

    if-gez v2, :cond_a

    .line 1291
    if-nez v15, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_7
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1, v13, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_5

    .line 1290
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_6

    .line 1291
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_7

    .line 1297
    .end local v10    # "area":I
    :cond_10
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v2, v4

    .line 1298
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v5

    mul-int/2addr v4, v5

    .line 1297
    add-int v10, v2, v4

    .line 1299
    .restart local v10    # "area":I
    const v2, 0x3f4800

    if-le v10, v2, :cond_11

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-nez v2, :cond_12

    .line 1301
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1, v13, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_5

    .line 1307
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 1306
    move-object/from16 v0, p1

    invoke-static {v0, v11, v3, v12, v2}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_drawBitmapJNI(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)I

    move-result v2

    if-gez v2, :cond_a

    .line 1308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1, v13, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_5
.end method

.method private updateCanvasLayer2(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 1197
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1198
    .local v1, "srcRect":Landroid/graphics/Rect;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v5, v0

    move-object v0, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 1199
    new-instance v7, Landroid/graphics/Rect;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-direct {v7, v9, v9, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1201
    .local v7, "dstRect":Landroid/graphics/Rect;
    const/4 v8, 0x0

    .local v8, "layer":I
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_0

    .line 1212
    return-void

    .line 1202
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    .line 1203
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_1

    .line 1205
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v0, :cond_3

    .line 1206
    if-nez v8, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPageEffectPaint:Landroid/graphics/Paint;

    :goto_1
    invoke-virtual {p1, v6, v1, v7, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1201
    :cond_1
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1206
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectPaint:Landroid/graphics/Paint;

    goto :goto_1

    .line 1208
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v1, v7, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_2
.end method

.method private updateDottedLine(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "re_rectf"    # Landroid/graphics/RectF;

    .prologue
    const/4 v10, 0x0

    .line 1024
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-nez v1, :cond_1

    .line 1049
    :cond_0
    :goto_0
    return-void

    .line 1028
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v1, :cond_0

    .line 1029
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1030
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1031
    .local v0, "canvasbuf":Landroid/graphics/Canvas;
    new-instance v6, Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v2, v2

    invoke-direct {v6, v10, v10, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1032
    .local v6, "ab_rectf":Landroid/graphics/RectF;
    invoke-direct {p0, v6, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1034
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    int-to-float v8, v1

    .line 1035
    .local v8, "currentLine":F
    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    sub-float v1, v8, v1

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    cmpg-float v1, v1, v2

    if-lez v1, :cond_2

    .line 1045
    const/4 v1, 0x0

    invoke-virtual {p1, v7, v10, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1046
    const/4 v0, 0x0

    .line 1047
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 1036
    :cond_2
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    add-float/2addr v2, v8

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_3

    .line 1037
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float v1, v8, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v9, v1, v2

    .line 1038
    .local v9, "re_line":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v1, :cond_3

    .line 1039
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    .line 1040
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v4, v9

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    .line 1039
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1043
    .end local v9    # "re_line":F
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    int-to-float v1, v1

    add-float/2addr v8, v1

    goto :goto_1
.end method

.method private updateFloatingLayer(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1172
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    .line 1194
    :goto_0
    return-void

    .line 1176
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    invoke-virtual {v3, v5, v5, v4}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1178
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1179
    .local v2, "srcRect":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/RectF;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v4, v4

    invoke-direct {v0, v6, v6, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1181
    .local v0, "dstRect":Landroid/graphics/RectF;
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v3, :cond_1

    .line 1182
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float v3, v6, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 1183
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 1184
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float v3, v6, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 1185
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 1193
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v0, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 1187
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1188
    .local v1, "dstRelRect":Landroid/graphics/RectF;
    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1189
    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, v1, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, v1, Landroid/graphics/RectF;->right:F

    .line 1190
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    .line 1189
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1
.end method

.method private updateHighlight(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "re_rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v9, 0x0

    .line 1143
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-nez v6, :cond_0

    .line 1169
    :goto_0
    return-void

    .line 1147
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1148
    .local v2, "count":I
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 1149
    .local v5, "rect":Landroid/graphics/RectF;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1150
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1151
    .local v1, "canvasbuf":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    .local v4, "pos":I
    :goto_1
    if-lt v4, v2, :cond_1

    .line 1166
    const/4 v6, 0x0

    invoke-virtual {p1, v0, v9, v9, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1167
    const/4 v1, 0x0

    .line 1168
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 1152
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;

    .line 1154
    .local v3, "info":Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 1155
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 1156
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 1157
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 1158
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v7, v7

    invoke-virtual {v5, v9, v9, v6, v7}, Landroid/graphics/RectF;->intersect(FFFF)Z

    .line 1159
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 1160
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v7, v7

    invoke-virtual {v5, v9, v9, v6, v7}, Landroid/graphics/RectF;->intersect(FFFF)Z

    .line 1162
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iget v7, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->color:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1163
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iget v7, v3, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->size:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v7, v8

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1164
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1151
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private updateNotepad()V
    .locals 6

    .prologue
    .line 2395
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2396
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPanAndZoom(FFFFF)V

    .line 2397
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setScreenStart(II)V

    .line 2399
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 2400
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 2401
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 2402
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    .line 2404
    :cond_0
    return-void
.end method


# virtual methods
.method public GetPageEffectWorking()Z
    .locals 1

    .prologue
    .line 1378
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-nez v0, :cond_0

    .line 1379
    const/4 v0, 0x0

    .line 1381
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    goto :goto_0
.end method

.method public GetRtoBmpItstScrHeight()I
    .locals 1

    .prologue
    .line 1405
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    return v0
.end method

.method public GetRtoBmpItstScrWidth()I
    .locals 1

    .prologue
    .line 1401
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    return v0
.end method

.method public GetScreenHeight()I
    .locals 1

    .prologue
    .line 1397
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    return v0
.end method

.method public GetScreenStartX()I
    .locals 1

    .prologue
    .line 1385
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    return v0
.end method

.method public GetScreenStartY()I
    .locals 1

    .prologue
    .line 1389
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    return v0
.end method

.method public GetScreenWidth()I
    .locals 1

    .prologue
    .line 1393
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    return v0
.end method

.method public UpdateCanvas(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 22
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rectf"    # Landroid/graphics/RectF;
    .param p3, "isScreenFramebuffer"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 1414
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    .line 1416
    .local v18, "time":J
    move-wide/from16 v14, v18

    .local v14, "postdraw":J
    move-wide/from16 v10, v18

    .local v10, "engine":J
    move-wide/from16 v16, v18

    .line 1417
    .local v16, "predraw":J
    if-nez p2, :cond_3

    .line 1418
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Performance onUpdateCanvas start rect = null isScreenFramebuffer = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1426
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    if-eqz v2, :cond_0

    .line 1427
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 1431
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_14

    .line 1432
    const/4 v2, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->drawAnimation(Landroid/graphics/Canvas;)V

    .line 1535
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    if-eqz v2, :cond_2

    .line 1536
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1539
    :cond_2
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Performance onUpdateCanvas end total = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v18

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms predraw = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1540
    sub-long v4, v16, v18

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms spenview = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, v10, v16

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms postdraw = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, v14, v10

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1541
    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1539
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1542
    return-void

    .line 1420
    :cond_3
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Performance onUpdateCanvas start rect = ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1421
    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") w = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " h = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1422
    const-string v4, " isScreenFramebuffer = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1420
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1436
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-eqz v2, :cond_5

    .line 1437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v7, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v8, v3

    move-object/from16 v3, p1

    move-object/from16 v9, p2

    invoke-interface/range {v2 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;->onDraw(Landroid/graphics/Canvas;FFFFFLandroid/graphics/RectF;)V

    .line 1439
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    .line 1441
    const/4 v12, 0x0

    .line 1442
    .local v12, "isUpdateWholeCanvas":Z
    if-nez p2, :cond_6

    .line 1443
    const/4 v12, 0x1

    .line 1446
    :cond_6
    invoke-direct/range {p0 .. p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    .line 1448
    if-nez p2, :cond_e

    .line 1449
    new-instance p2, Landroid/graphics/RectF;

    .end local p2    # "rectf":Landroid/graphics/RectF;
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v5, v5

    move-object/from16 v0, p2

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1450
    .restart local p2    # "rectf":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTemporaryStroke:Z

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    if-nez v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v2

    if-eqz v2, :cond_d

    .line 1451
    :cond_7
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateFloatingLayer(Landroid/graphics/Canvas;)V

    .line 1459
    :cond_8
    :goto_2
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateHighlight(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 1460
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateDottedLine(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 1462
    if-eqz v12, :cond_9

    .line 1463
    const/high16 v2, -0x3d380000    # -100.0f

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 1464
    const/high16 v2, -0x3d380000    # -100.0f

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    .line 1465
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v3, -0x3d380000    # -100.0f

    iput v3, v2, Landroid/graphics/PointF;->x:F

    .line 1466
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v3, -0x3d380000    # -100.0f

    iput v3, v2, Landroid/graphics/PointF;->y:F

    .line 1468
    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v2

    if-nez v2, :cond_a

    .line 1469
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_f

    .line 1471
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float v20, v2, v3

    .line 1472
    .local v20, "xr":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float v21, v2, v3

    .line 1473
    .local v21, "yr":F
    new-instance v13, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float v2, v2, v20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v3, v3, v21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float v4, v4, v20

    .line 1474
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float v5, v5, v21

    .line 1473
    invoke-direct {v13, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1475
    .local v13, "ovalRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1480
    .end local v13    # "ovalRect":Landroid/graphics/RectF;
    .end local v20    # "xr":F
    .end local v21    # "yr":F
    :cond_a
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v2

    if-nez v2, :cond_b

    .line 1481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    instance-of v2, v2, Landroid/view/SurfaceView;

    if-eqz v2, :cond_12

    .line 1482
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x41f00000    # 30.0f

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_b

    .line 1483
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_10

    .line 1485
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float v20, v2, v3

    .line 1486
    .restart local v20    # "xr":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float v21, v2, v3

    .line 1487
    .restart local v21    # "yr":F
    new-instance v13, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float v2, v2, v20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v3, v3, v21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    .line 1488
    add-float v4, v4, v20

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float v5, v5, v21

    .line 1487
    invoke-direct {v13, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1489
    .restart local v13    # "ovalRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1513
    .end local v13    # "ovalRect":Landroid/graphics/RectF;
    .end local v20    # "xr":F
    .end local v21    # "yr":F
    :cond_b
    :goto_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 1515
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-eqz v2, :cond_c

    .line 1516
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v7, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v8, v3

    move-object/from16 v3, p1

    move-object/from16 v9, p2

    invoke-interface/range {v2 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;->onDraw(Landroid/graphics/Canvas;FFFFFLandroid/graphics/RectF;)V

    .line 1518
    :cond_c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    .line 1520
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->drawScroll(Landroid/graphics/Canvas;)Z

    .line 1522
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 1452
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isUpdateFloating()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1454
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateFloatingLayer(Landroid/graphics/Canvas;)V

    goto/16 :goto_2

    .line 1457
    :cond_e
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateFloatingLayer(Landroid/graphics/Canvas;)V

    goto/16 :goto_2

    .line 1477
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 1491
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOnePT:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_11

    .line 1492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1496
    :goto_5
    const-string v2, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Draw remover circle mCirclePoint.x "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mCirclePoint.y "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mRemoverRadius "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1496
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1494
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_5

    .line 1501
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v2, :cond_13

    .line 1503
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    mul-float v20, v2, v3

    .line 1504
    .restart local v20    # "xr":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    mul-float v21, v2, v3

    .line 1505
    .restart local v21    # "yr":F
    new-instance v13, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float v2, v2, v20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v3, v3, v21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float v4, v4, v20

    .line 1506
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    add-float v5, v5, v21

    .line 1505
    invoke-direct {v13, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1507
    .restart local v13    # "ovalRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1509
    .end local v13    # "ovalRect":Landroid/graphics/RectF;
    .end local v20    # "xr":F
    .end local v21    # "yr":F
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1532
    .end local v12    # "isUpdateWholeCanvas":Z
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1
.end method

.method public cancelStroke()V
    .locals 4

    .prologue
    .line 5234
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5240
    :goto_0
    return-void

    .line 5238
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    .line 5239
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_cancelStroke(J)V

    goto :goto_0
.end method

.method public cancelStrokeFrame()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 4210
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    .line 4211
    :cond_0
    const/16 v1, 0x8

    .line 4212
    const-string v2, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    .line 4211
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 4214
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    if-eqz v1, :cond_3

    .line 4215
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancelStrokeFrame()Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    .line 4216
    .local v0, "o":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    if-eqz v0, :cond_2

    .line 4217
    const-string v1, "STROKE_FRAME"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getExtraDataInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_4

    .line 4218
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    .line 4219
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistoryTag()V

    .line 4225
    :cond_2
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 4227
    .end local v0    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    :cond_3
    return-void

    .line 4220
    .restart local v0    # "o":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    :cond_4
    const-string v1, "STROKE_FRAME"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getExtraDataInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 4221
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    .line 4222
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "includeBackground"    # Z

    .prologue
    const/4 v3, 0x0

    .line 3063
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    move-object v0, v3

    .line 3089
    :goto_0
    return-object v0

    .line 3066
    :cond_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    if-nez v4, :cond_2

    .line 3067
    :cond_1
    const-string v4, "SpenInView"

    const-string v5, "Not yet to create view"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3068
    const/4 v4, 0x4

    const-string v5, " : ScreenWidth or ScreenHeight is zero."

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    move-object v0, v3

    .line 3069
    goto :goto_0

    .line 3071
    :cond_2
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateAllScreenFrameBuffer(J)V

    .line 3072
    const/4 v0, 0x0

    .line 3074
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez p1, :cond_4

    .line 3075
    :try_start_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3083
    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3084
    .local v1, "canvas":Landroid/graphics/Canvas;
    if-nez p1, :cond_3

    .line 3085
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    neg-int v4, v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3087
    :cond_3
    const/4 v4, 0x1

    invoke-direct {p0, v1, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    .line 3088
    const/4 v1, 0x0

    .line 3089
    goto :goto_0

    .line 3077
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_4
    :try_start_1
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 3079
    :catch_0
    move-exception v2

    .line 3080
    .local v2, "e":Ljava/lang/Throwable;
    const-string v4, "SpenInView"

    const-string v5, "Failed to create bitmap"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3081
    const/4 v4, 0x2

    const-string v5, " : fail createBitmap."

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    const/4 v1, 0x0

    .line 3111
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 3112
    :cond_0
    const/4 v0, 0x0

    .line 3115
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    .line 3116
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    const/4 v3, 0x1

    .line 3115
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public capturePage(FZ)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "ratio"    # F
    .param p2, "isTransparent"    # Z

    .prologue
    const/4 v8, 0x0

    .line 3120
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v10, 0x0

    cmp-long v1, v2, v10

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 3142
    :cond_0
    :goto_0
    return-object v8

    .line 3123
    :cond_1
    if-nez p2, :cond_2

    .line 3124
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->capturePage(F)Landroid/graphics/Bitmap;

    move-result-object v8

    goto :goto_0

    .line 3126
    :cond_2
    const/4 v0, 0x0

    .line 3128
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3133
    :goto_1
    if-eqz v0, :cond_0

    .line 3134
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3135
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3136
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v4, 0x6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 3137
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    .line 3138
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    const/4 v3, 0x1

    .line 3137
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 3139
    .local v8, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 3129
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v8    # "result":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 3130
    .local v7, "e":Ljava/lang/Throwable;
    const-string v1, "SpenInView"

    const-string v2, "Failed to create bitmap"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3131
    const/4 v1, 0x2

    const-string v2, " : fail createBitmap."

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public changeStrokeFrame(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 4
    .param p1, "container"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4230
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4231
    :cond_0
    const/16 v0, 0x8

    .line 4232
    const-string v1, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    .line 4231
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 4235
    :cond_1
    if-nez p1, :cond_2

    .line 4236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "container is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4239
    :cond_2
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4240
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 4241
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 4246
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    .line 4247
    return-void

    .line 4243
    :cond_3
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    .line 4244
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    goto :goto_0
.end method

.method public clearHighlight()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4859
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4867
    :cond_0
    :goto_0
    return-void

    .line 4862
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 4863
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    .line 4864
    const/4 v0, 0x1

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public close()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 604
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    if-eqz v1, :cond_0

    .line 605
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 606
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    .line 608
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 610
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v1

    if-eqz v1, :cond_1

    .line 611
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopReplay()V

    .line 614
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 615
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_finalize(J)V

    .line 616
    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    .line 619
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    .line 620
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 621
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 624
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 625
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_12

    .line 630
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 631
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    .line 634
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    .line 635
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 636
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    .line 639
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v1, :cond_7

    .line 640
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->close()V

    .line 641
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 644
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_8

    .line 645
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->close()V

    .line 646
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    .line 648
    :cond_8
    sput-object v4, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 649
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 650
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 651
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v1, :cond_9

    .line 652
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->close()V

    .line 653
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 655
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v1, :cond_a

    .line 656
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, v4}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 657
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 659
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v1, :cond_b

    .line 660
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->close()V

    .line 661
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 663
    :cond_b
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    if-eqz v1, :cond_c

    .line 664
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 665
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    .line 667
    :cond_c
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    .line 668
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    .line 669
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    .line 670
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    .line 671
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 672
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPageEffectPaint:Landroid/graphics/Paint;

    .line 673
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectPaint:Landroid/graphics/Paint;

    .line 674
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    .line 675
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    .line 676
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    .line 677
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    .line 679
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 680
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 681
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 682
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 683
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .line 684
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    .line 686
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .line 687
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 688
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .line 689
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 690
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .line 691
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .line 692
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 693
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 694
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 695
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    .line 696
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    .line 697
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 698
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    .line 699
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .line 700
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    .line 701
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    .line 702
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 703
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 704
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 705
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 706
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 707
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    .line 708
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    .line 709
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 710
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    .line 711
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    if-eqz v1, :cond_d

    .line 712
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 713
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    .line 715
    :cond_d
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    if-eqz v1, :cond_e

    .line 716
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 717
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    .line 719
    :cond_e
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    if-eqz v1, :cond_f

    .line 720
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 721
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateHandler:Landroid/os/Handler;

    .line 724
    :cond_f
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v1, :cond_10

    .line 725
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->stop()V

    .line 726
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->close()V

    .line 727
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 729
    :cond_10
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    .line 730
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    .line 731
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .line 732
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContextMenu:Landroid/view/ContextMenu;

    .line 733
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 734
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    .line 735
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    .line 736
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    .line 737
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v1, :cond_11

    .line 738
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_deinit_c2dJNI()V

    .line 742
    :cond_11
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->releaseHapticFeedback()V

    .line 743
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->unregisterPensoundSolution()V

    .line 744
    return-void

    .line 625
    :cond_12
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 626
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 627
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0
.end method

.method public closeControl()V
    .locals 6

    .prologue
    .line 4618
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    if-eqz v1, :cond_1

    .line 4633
    :cond_0
    :goto_0
    return-void

    .line 4622
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    .line 4624
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v1, :cond_2

    .line 4625
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEditableTextBox()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsEditableTextBox:Z

    .line 4627
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4632
    :cond_2
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    goto :goto_0

    .line 4628
    :catch_0
    move-exception v0

    .line 4629
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v10, 0x0

    .line 5210
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v4, v10

    .line 5230
    :cond_1
    :goto_0
    return-object v4

    .line 5214
    :cond_2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 5215
    .local v0, "dstRect":Landroid/graphics/RectF;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 5219
    const/4 v4, 0x0

    .line 5221
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 5222
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_drawObjectList(JLandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    :goto_2
    move-object v4, v10

    .line 5230
    goto :goto_0

    .line 5215
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 5216
    .local v8, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getDrawnRect()Landroid/graphics/RectF;

    move-result-object v9

    .line 5217
    .local v9, "srcRect":Landroid/graphics/RectF;
    invoke-virtual {p0, v0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->joinRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 5225
    .end local v8    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v9    # "srcRect":Landroid/graphics/RectF;
    .restart local v4    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 5226
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "SpenInView"

    const-string v2, "Failed to create bitmap"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5227
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_2
.end method

.method protected findObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;
    .locals 3
    .param p1, "typeFilter"    # I
    .param p2, "rectf"    # Landroid/graphics/RectF;
    .param p3, "allAreas"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/RectF;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5646
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_0

    .line 5647
    const/4 v1, 0x0

    .line 5649
    .local v1, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->findObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 5656
    .end local v1    # "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :goto_0
    return-object v1

    .line 5650
    .restart local v1    # "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :catch_0
    move-exception v0

    .line 5652
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 5656
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBlankColor()I
    .locals 4

    .prologue
    .line 3520
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3521
    const/4 v0, 0x0

    .line 3523
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3551
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3554
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3538
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3541
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 4

    .prologue
    .line 4604
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4605
    const/4 v0, 0x0

    .line 4607
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 6

    .prologue
    .line 3874
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3875
    const/4 v0, 0x0

    .line 3883
    :cond_0
    :goto_0
    return-object v0

    .line 3877
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    .line 3878
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v1, :cond_0

    .line 3879
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    .line 3880
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    goto :goto_0
.end method

.method public getFrameStartPosition()Landroid/graphics/PointF;
    .locals 4

    .prologue
    .line 3364
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3365
    const/4 v0, 0x0

    .line 3367
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 4

    .prologue
    .line 3399
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3400
    const/4 v0, 0x0

    .line 3402
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getMaxZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 4

    .prologue
    .line 3434
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3435
    const/4 v0, 0x0

    .line 3437
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getMinZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 6

    .prologue
    .line 3479
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 3480
    const/4 v0, 0x0

    .line 3484
    :goto_0
    return-object v0

    .line 3482
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 3483
    .local v0, "point":Landroid/graphics/PointF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getPan(JLandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 6

    .prologue
    .line 3786
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3787
    const/4 v0, 0x0

    .line 3798
    :cond_0
    :goto_0
    return-object v0

    .line 3789
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 3790
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v1, :cond_0

    .line 3791
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 3792
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 3793
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 3794
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 3795
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 6

    .prologue
    .line 3959
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3960
    const/4 v0, 0x0

    .line 3968
    :cond_0
    :goto_0
    return-object v0

    .line 3962
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    .line 3963
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v1, :cond_0

    .line 3964
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 3965
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    goto :goto_0
.end method

.method public getReplayState()I
    .locals 4

    .prologue
    .line 4704
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4705
    const/4 v0, 0x0

    .line 4707
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getReplayState(J)I

    move-result v0

    goto :goto_0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 6

    .prologue
    .line 4021
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 4022
    const/4 v0, 0x0

    .line 4029
    :cond_0
    :goto_0
    return-object v0

    .line 4024
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;-><init>()V

    .line 4025
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    if-eqz v1, :cond_0

    .line 4026
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    goto :goto_0
.end method

.method public getTemporaryStroke()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4907
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 4908
    const/4 v0, 0x0

    .line 4913
    :goto_0
    return-object v0

    .line 4910
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4911
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getTemporaryStroke(JLjava/util/ArrayList;)V

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 6

    .prologue
    .line 3650
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3651
    const/4 v0, 0x0

    .line 3671
    :cond_0
    :goto_0
    return-object v0

    .line 3653
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v1, :cond_2

    .line 3654
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v1, :cond_2

    .line 3655
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    goto :goto_0

    .line 3659
    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    .line 3660
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3661
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-eqz v1, :cond_0

    .line 3662
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3663
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3664
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3665
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 3666
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 3667
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 3668
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    goto :goto_0
.end method

.method public getToolTypeAction(I)I
    .locals 4
    .param p1, "toolType"    # I

    .prologue
    .line 3262
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3263
    const/4 v0, 0x0

    .line 3265
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getToolTypeAction(JI)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadBoxHeight()F
    .locals 4

    .prologue
    .line 5058
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5059
    :cond_0
    const/4 v0, 0x0

    .line 5062
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getBoxHeight()F

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadBoxPosition()Landroid/graphics/PointF;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 5075
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v1, :cond_1

    .line 5076
    :cond_0
    const/4 v0, 0x0

    .line 5084
    :goto_0
    return-object v0

    .line 5079
    :cond_1
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 5080
    .local v0, "point":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 5081
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 5083
    const-string v1, "ZoomPad"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getZoomPadBoxPosition ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getZoomPadBoxRect()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 5023
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5024
    :cond_0
    const/4 v0, 0x0

    .line 5027
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getStrokeBoxRectF()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomPadPosition()Landroid/graphics/PointF;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 5097
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v1, :cond_1

    .line 5098
    :cond_0
    const/4 v0, 0x0

    .line 5106
    :goto_0
    return-object v0

    .line 5101
    :cond_1
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 5102
    .local v0, "point":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 5103
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 5105
    const-string v1, "ZoomPad"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getZoomPadPosition ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getZoomPadRect()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 5031
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5032
    :cond_0
    const/4 v0, 0x0

    .line 5037
    :goto_0
    return-object v0

    .line 5035
    :cond_1
    const-string v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getZoomPadRect ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 5036
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5035
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5037
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->getPadRectF()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 4

    .prologue
    .line 3347
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3348
    const/4 v0, 0x0

    .line 3350
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public isDottedLineEnabled()Z
    .locals 4

    .prologue
    .line 4823
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4824
    const/4 v0, 0x0

    .line 4826
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    goto :goto_0
.end method

.method public isEditableTextBox()Z
    .locals 1

    .prologue
    .line 5016
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5017
    const/4 v0, 0x1

    .line 5019
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHorizontalScrollBarEnabled()Z
    .locals 6

    .prologue
    .line 4975
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 4976
    const/4 v0, 0x0

    .line 4982
    :cond_0
    :goto_0
    return v0

    .line 4978
    :cond_1
    const/4 v0, 0x0

    .line 4979
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_0

    .line 4980
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->isHorizontalScroll()Z

    move-result v0

    goto :goto_0
.end method

.method public isHorizontalSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 4465
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    return v0
.end method

.method public isHyperTextViewEnabled()Z
    .locals 1

    .prologue
    .line 4529
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    return v0
.end method

.method public isLongPressEnabled()Z
    .locals 4

    .prologue
    .line 3000
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3001
    const/4 v0, 0x0

    .line 3003
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z

    goto :goto_0
.end method

.method public isPenButtonSelectionEnabled()Z
    .locals 1

    .prologue
    .line 5252
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsPenButtonSelectionEnabled:Z

    return v0
.end method

.method public isScrollBarEnabled()Z
    .locals 6

    .prologue
    .line 4945
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 4946
    const/4 v0, 0x0

    .line 4952
    :cond_0
    :goto_0
    return v0

    .line 4948
    :cond_1
    const/4 v0, 0x0

    .line 4949
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_0

    .line 4950
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->isScroll()Z

    move-result v0

    goto :goto_0
.end method

.method public isSmartScaleEnabled()Z
    .locals 1

    .prologue
    .line 4413
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    return v0
.end method

.method public isTextCursorEnabled()Z
    .locals 4

    .prologue
    .line 3015
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3016
    const/4 v0, 0x0

    .line 3018
    :goto_0
    return v0

    :cond_0
    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverCursorEnable:Z

    goto :goto_0
.end method

.method public isToolTipEnabled()Z
    .locals 1

    .prologue
    .line 4541
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    return v0
.end method

.method public isVerticalScrollBarEnabled()Z
    .locals 6

    .prologue
    .line 5005
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 5006
    const/4 v0, 0x0

    .line 5012
    :cond_0
    :goto_0
    return v0

    .line 5008
    :cond_1
    const/4 v0, 0x0

    .line 5009
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_0

    .line 5010
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->isVerticalScroll()Z

    move-result v0

    goto :goto_0
.end method

.method public isVerticalSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 4516
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    return v0
.end method

.method public isZoomPadEnabled()Z
    .locals 4

    .prologue
    .line 5144
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5145
    :cond_0
    const/4 v0, 0x0

    .line 5148
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomPadStroking()Z
    .locals 4

    .prologue
    .line 5041
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5042
    :cond_0
    const/4 v0, 0x0

    .line 5045
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isStroking()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomable()Z
    .locals 4

    .prologue
    .line 3299
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3300
    const/4 v0, 0x0

    .line 3302
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_isZoomable(J)Z

    move-result v0

    goto :goto_0
.end method

.method joinRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dst"    # Landroid/graphics/RectF;
    .param p2, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 929
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 955
    :cond_0
    :goto_0
    return-void

    .line 932
    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    .line 936
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    .line 937
    :cond_2
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 938
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 939
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 940
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 942
    :cond_3
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 943
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 945
    :cond_4
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 946
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 948
    :cond_5
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    .line 949
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 951
    :cond_6
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 952
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2947
    if-eqz p1, :cond_0

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-nez v10, :cond_1

    .line 2948
    :cond_0
    const/4 v10, 0x1

    .line 2996
    :goto_0
    return v10

    .line 2951
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onHoverHyperText(Landroid/view/MotionEvent;)Z

    .line 2953
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v12, 0x0

    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v12

    invoke-direct {p0, v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getToolTypeAction(JI)I

    move-result v1

    .line 2954
    .local v1, "toolTypeAction":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v10

    if-nez v10, :cond_3

    .line 2955
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    const/16 v11, 0x9

    if-ne v10, v11, :cond_5

    const/4 v10, 0x7

    if-eq v1, v10, :cond_5

    .line 2956
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setCustomHoveringIcon()I

    move-result v10

    iput v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverIconID:I

    .line 2961
    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    const/16 v11, 0x9

    if-ne v10, v11, :cond_3

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v10, :cond_3

    .line 2962
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v10, :cond_3

    .line 2963
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v11, v10}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2964
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onCanvasHoverEnter()V

    .line 2968
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2969
    .local v0, "action":I
    const/16 v10, 0x9

    if-ne v0, v10, :cond_4

    .line 2970
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 2971
    .local v2, "diffTime":J
    const-wide/16 v10, 0x64

    iget-wide v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    add-long/2addr v10, v12

    cmp-long v10, v2, v10

    if-lez v10, :cond_6

    const/4 v10, 0x1

    :goto_2
    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    .line 2972
    iget-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    if-eqz v10, :cond_4

    .line 2973
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 2974
    .local v6, "eventTime":J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 2975
    .local v4, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 2976
    .local v8, "systemTime":J
    const-string v10, "SpenInView"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "skiptouch hover action = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " eventTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " downTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2977
    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " systemTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " diffTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2976
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2981
    .end local v2    # "diffTime":J
    .end local v4    # "downTime":J
    .end local v6    # "eventTime":J
    .end local v8    # "systemTime":J
    :cond_4
    iget-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    if-eqz v10, :cond_7

    .line 2983
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 2957
    .end local v0    # "action":I
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    const/16 v11, 0xa

    if-ne v10, v11, :cond_2

    .line 2958
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverIconID:I

    invoke-direct {p0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removeHoveringIcon(I)Z

    goto/16 :goto_1

    .line 2971
    .restart local v0    # "action":I
    .restart local v2    # "diffTime":J
    :cond_6
    const/4 v10, 0x0

    goto :goto_2

    .line 2986
    .end local v2    # "diffTime":J
    :cond_7
    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v12, 0x0

    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v12

    invoke-direct {p0, v10, v11, p1, v12}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onHover(JLandroid/view/MotionEvent;I)Z

    .line 2988
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-nez v10, :cond_8

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v10, :cond_8

    .line 2989
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v10, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onHoverEvent(Landroid/view/MotionEvent;)V

    .line 2992
    :cond_8
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    invoke-interface {v10, v11, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2993
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 2996
    :cond_9
    const/4 v10, 0x1

    goto/16 :goto_0
.end method

.method protected declared-synchronized onLayout(ZIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 16
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "parentLayoutRect"    # Landroid/graphics/Rect;
    .param p7, "windowVisibleRect"    # Landroid/graphics/Rect;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .prologue
    .line 784
    monitor-enter p0

    sub-int v6, p4, p2

    .line 785
    .local v6, "w":I
    sub-int v7, p5, p3

    .line 786
    .local v7, "h":I
    if-nez p7, :cond_1

    move v10, v7

    .line 789
    .local v10, "hExceptSIP":I
    :goto_0
    if-eqz v6, :cond_0

    if-nez v7, :cond_2

    .line 875
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 786
    .end local v10    # "hExceptSIP":I
    :cond_1
    :try_start_0
    move-object/from16 v0, p7

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 787
    move-object/from16 v0, p6

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    add-int/2addr v4, v5

    sub-int v10, v3, v4

    goto :goto_0

    .line 791
    .restart local v10    # "hExceptSIP":I
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    if-ne v6, v3, :cond_3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    if-ne v7, v3, :cond_3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutExceptSIP:I

    if-eq v10, v3, :cond_0

    .line 795
    :cond_3
    const/16 v2, 0xc8

    .line 796
    .local v2, "delayTime":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    if-nez v3, :cond_5

    .line 797
    :cond_4
    const/4 v2, 0x0

    .line 799
    :cond_5
    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutWidth:I

    .line 800
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutHeight:I

    .line 801
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLayoutExceptSIP:I

    .line 807
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 808
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;->onCompleted()V

    .line 809
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopReplay()V

    .line 812
    :cond_6
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLayout("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    const-string v3, "onLayout. parentLayoutRect : "

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->printRect(Ljava/lang/String;Landroid/graphics/Rect;)V

    .line 814
    if-eqz p7, :cond_7

    .line 815
    const-string v3, "onLayout. windowVisibleRect : "

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->printRect(Ljava/lang/String;Landroid/graphics/Rect;)V

    .line 816
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLayout. hExceptSIP : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    :cond_7
    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    .line 820
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    .line 821
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    .line 823
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v3, :cond_8

    .line 824
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v3, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setScreenSize(II)V

    .line 826
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v3, :cond_9

    .line 827
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v3, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setScreenResolution(II)V

    .line 829
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v3, :cond_a

    .line 830
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setParentRect(Landroid/graphics/Rect;)V

    .line 833
    :cond_a
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setScreenSize(JIII)V

    .line 834
    const/4 v11, 0x0

    .line 835
    .local v11, "isScreenFBCreate":Z
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-le v3, v4, :cond_e

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    .line 836
    .local v13, "newFramebufferWidth":I
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-le v3, v4, :cond_f

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    .line 837
    .local v12, "newFramebufferHeight":I
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    if-gt v13, v3, :cond_b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    if-le v12, v3, :cond_d

    .line 838
    :cond_b
    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    .line 839
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    .line 841
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_c

    .line 842
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 845
    :cond_c
    :try_start_1
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLayout. ScreenFB W : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", H : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    .line 847
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setScreenFrameBuffer(JLandroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 848
    const/4 v11, 0x1

    .line 849
    const/4 v2, 0x0

    .line 855
    :cond_d
    :goto_4
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEditableTextBox()Z

    move-result v3

    if-eqz v3, :cond_10

    if-nez v11, :cond_10

    .line 856
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 784
    .end local v2    # "delayTime":I
    .end local v10    # "hExceptSIP":I
    .end local v11    # "isScreenFBCreate":Z
    .end local v12    # "newFramebufferHeight":I
    .end local v13    # "newFramebufferWidth":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 835
    .restart local v2    # "delayTime":I
    .restart local v10    # "hExceptSIP":I
    .restart local v11    # "isScreenFBCreate":Z
    :cond_e
    :try_start_3
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    goto/16 :goto_2

    .line 836
    .restart local v13    # "newFramebufferWidth":I
    :cond_f
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    goto/16 :goto_3

    .line 850
    .restart local v12    # "newFramebufferHeight":I
    :catch_0
    move-exception v9

    .line 851
    .local v9, "e":Ljava/lang/Throwable;
    const-string v3, "SpenInView"

    const-string v4, "Failed to create bitmap"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_4

    .line 858
    .end local v9    # "e":Ljava/lang/Throwable;
    :cond_10
    if-nez v2, :cond_11

    .line 859
    const-string v3, "SpenInView"

    const-string v4, "onLayout. delayTime = 0"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    .line 861
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto/16 :goto_1

    .line 863
    :cond_11
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLayout. delayTime = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    .line 865
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    .line 871
    int-to-long v14, v2

    .line 865
    invoke-virtual {v3, v4, v14, v15}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 36
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2648
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v32, v0

    const-wide/16 v34, 0x0

    cmp-long v31, v32, v34

    if-nez v31, :cond_1

    .line 2649
    :cond_0
    const/16 v31, 0x1

    .line 2926
    :goto_0
    return v31

    .line 2652
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v31

    move/from16 v0, v31

    and-int/lit16 v6, v0, 0xff

    .line 2653
    .local v6, "action":I
    if-nez v6, :cond_2

    .line 2654
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z

    .line 2656
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v31

    const/16 v32, 0x2

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_3

    .line 2657
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsCancelFling:Z

    .line 2659
    :cond_3
    const/16 v31, 0x1

    move/from16 v0, v31

    if-eq v6, v0, :cond_4

    const/16 v31, 0x3

    move/from16 v0, v31

    if-eq v6, v0, :cond_4

    .line 2660
    const/16 v31, 0x5

    move/from16 v0, v31

    if-ne v6, v0, :cond_5

    .line 2661
    :cond_4
    const/high16 v31, -0x3d380000    # -100.0f

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 2662
    const/high16 v31, -0x3d380000    # -100.0f

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    .line 2663
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    const/high16 v32, -0x3d380000    # -100.0f

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2664
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    const/high16 v32, -0x3d380000    # -100.0f

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2666
    :cond_5
    if-nez v6, :cond_8

    .line 2667
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v34

    sub-long v8, v32, v34

    .line 2668
    .local v8, "diffTime":J
    const-wide/16 v32, 0x258

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    move-wide/from16 v34, v0

    add-long v32, v32, v34

    cmp-long v31, v8, v32

    if-lez v31, :cond_f

    const/16 v31, 0x1

    :goto_1
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    .line 2669
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v31

    if-eqz v31, :cond_6

    .line 2670
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    .line 2672
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    move/from16 v31, v0

    if-eqz v31, :cond_7

    .line 2673
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v14

    .line 2674
    .local v14, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v10

    .line 2675
    .local v10, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v24

    .line 2676
    .local v24, "systemTime":J
    const-string v31, "SpenInView"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string/jumbo v33, "skiptouch action = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " eventTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " downTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2677
    const-string v33, " systemTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " diffTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " mTouchProcessingTime = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2678
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 2676
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2680
    .end local v10    # "downTime":J
    .end local v14    # "eventTime":J
    .end local v24    # "systemTime":J
    :cond_7
    const-wide/16 v32, 0x0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    .line 2682
    .end local v8    # "diffTime":J
    :cond_8
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    .line 2683
    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v31

    const/16 v32, 0x7

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_9

    .line 2684
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    .line 2686
    :cond_9
    if-eqz v6, :cond_a

    const/16 v31, 0xd3

    move/from16 v0, v31

    if-ne v6, v0, :cond_b

    .line 2687
    :cond_a
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 2689
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v31, v0

    if-eqz v31, :cond_12

    .line 2690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isTouchEnabled()Z

    move-result v31

    if-eqz v31, :cond_12

    .line 2691
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getStyle()I

    move-result v31

    const/16 v32, 0x3

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_12

    .line 2692
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    move/from16 v31, v0

    if-eqz v31, :cond_10

    .line 2693
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_c

    .line 2694
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 2696
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2698
    const/16 v31, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2705
    :cond_d
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v31, v0

    if-eqz v31, :cond_e

    .line 2706
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move/from16 v0, v31

    neg-int v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v32, v0

    move/from16 v0, v32

    neg-int v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2707
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2709
    :cond_e
    const/16 v31, 0x1

    goto/16 :goto_0

    .line 2668
    .restart local v8    # "diffTime":J
    :cond_f
    const/16 v31, 0x0

    goto/16 :goto_1

    .line 2700
    .end local v8    # "diffTime":J
    :cond_10
    const/16 v31, 0x1

    move/from16 v0, v31

    if-eq v6, v0, :cond_11

    const/16 v31, 0xd4

    move/from16 v0, v31

    if-ne v6, v0, :cond_d

    .line 2701
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 2702
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTouchPre:Z

    goto :goto_2

    .line 2713
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    move/from16 v31, v0

    if-nez v31, :cond_42

    .line 2714
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v32, v0

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move-object/from16 v0, p0

    move-wide/from16 v1, v32

    move/from16 v3, v31

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_getToolTypeAction(JI)I

    move-result v30

    .line 2716
    .local v30, "toolTypeAction":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v17

    .line 2717
    .local v17, "originAction":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    .line 2718
    .local v7, "newX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v16

    .line 2720
    .local v16, "newY":F
    const/16 v31, 0x5

    move/from16 v0, v31

    if-ne v6, v0, :cond_14

    .line 2721
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    .line 2722
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableUpdate(Z)V

    .line 2723
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_14

    .line 2724
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2725
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    move/from16 v31, v0

    if-eqz v31, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_13

    .line 2726
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 2728
    :cond_13
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2729
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    .line 2733
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    if-nez v31, :cond_1c

    .line 2734
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v31

    if-nez v31, :cond_1c

    .line 2735
    const/16 v31, 0x2

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_15

    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_15

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1c

    .line 2736
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    move/from16 v31, v0

    if-eqz v31, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_17

    .line 2737
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_16

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_17

    .line 2738
    :cond_16
    if-nez v6, :cond_27

    .line 2739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexEraser:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 2740
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_26

    .line 2741
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    move/from16 v32, v0

    const-string v33, "Eraser"

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v33

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    div-float v32, v32, v33

    move/from16 v0, v32

    float-to-double v0, v0

    move-wide/from16 v32, v0

    invoke-virtual/range {v31 .. v33}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 2752
    :cond_17
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v26

    .line 2753
    .local v26, "tempX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v27

    .line 2754
    .local v27, "tempY":F
    move/from16 v18, v26

    .line 2755
    .local v18, "originX":F
    move/from16 v19, v27

    .line 2757
    .local v19, "originY":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpg-float v31, v26, v31

    if-gez v31, :cond_28

    .line 2758
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v26, v0

    .line 2759
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    .line 2765
    :cond_18
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpg-float v31, v27, v31

    if-gez v31, :cond_29

    .line 2766
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v27, v0

    .line 2767
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    .line 2773
    :cond_19
    :goto_5
    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 2775
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    move/from16 v31, v0

    if-eqz v31, :cond_2d

    .line 2776
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    move/from16 v31, v0

    if-eqz v31, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_1a

    .line 2777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 2780
    :cond_1a
    if-nez v6, :cond_2a

    .line 2781
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldX:F

    .line 2782
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldY:F

    .line 2801
    :cond_1b
    :goto_6
    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 2802
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    .line 2805
    .end local v18    # "originX":F
    .end local v19    # "originY":F
    .end local v26    # "tempX":F
    .end local v27    # "tempY":F
    :cond_1c
    const/16 v31, 0x6

    move/from16 v0, v31

    if-eq v6, v0, :cond_1d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_1e

    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_1e

    .line 2806
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, -0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchCount:I

    .line 2809
    :cond_1e
    if-nez v6, :cond_31

    .line 2810
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 2811
    const/16 v31, 0x2

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2e

    .line 2812
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    .line 2813
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Path;->reset()V

    .line 2814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v33

    invoke-virtual/range {v31 .. v33}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2815
    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCurrentDrawingTooltype:I

    .line 2825
    :cond_1f
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    move/from16 v31, v0

    if-nez v31, :cond_20

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v31

    const/16 v32, 0x3

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_22

    .line 2826
    :cond_20
    const/16 v31, 0x1

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    const/16 v31, 0x3

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    .line 2827
    const/16 v31, 0x5

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    const/16 v31, 0xd3

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    .line 2828
    const/16 v31, 0xd4

    move/from16 v0, v31

    if-eq v6, v0, :cond_21

    const/16 v31, 0xd5

    move/from16 v0, v31

    if-ne v6, v0, :cond_32

    .line 2829
    :cond_21
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    .line 2835
    :cond_22
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move/from16 v0, v31

    neg-int v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v32, v0

    move/from16 v0, v32

    neg-int v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2837
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v31, v0

    if-eqz v31, :cond_23

    .line 2838
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 2840
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v31, v0

    if-eqz v31, :cond_24

    .line 2841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2844
    :cond_24
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_25

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_25

    .line 2845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getState()Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-result-object v31

    sget-object v32, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_25

    .line 2846
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual/range {v31 .. v32}, Landroid/os/Handler;->removeMessages(I)V

    .line 2847
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual/range {v31 .. v32}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2850
    :cond_25
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v28

    .line 2852
    .local v28, "time":J
    move-wide/from16 v20, v28

    .local v20, "posttouch":J
    move-wide/from16 v12, v28

    .local v12, "engine":J
    move-wide/from16 v22, v28

    .line 2853
    .local v22, "pretouch":J
    const-string v31, "SpenInView"

    const-string v32, "Performance touch process start"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2854
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v31, v0

    if-eqz v31, :cond_33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, p1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v31

    if-eqz v31, :cond_33

    .line 2855
    const-string v31, "SpenInView"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "Performance pretouch listener has consumed action = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2856
    const/16 v31, 0x1

    goto/16 :goto_0

    .line 2743
    .end local v12    # "engine":J
    .end local v20    # "posttouch":J
    .end local v22    # "pretouch":J
    .end local v28    # "time":J
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    move/from16 v32, v0

    const-string v33, "Eraser"

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v33

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    div-float v32, v32, v33

    move/from16 v0, v32

    float-to-double v0, v0

    move-wide/from16 v32, v0

    invoke-virtual/range {v31 .. v33}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    goto/16 :goto_3

    .line 2745
    :cond_27
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_17

    .line 2746
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 2747
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    move/from16 v32, v0

    .line 2748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v33

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    div-float v32, v32, v33

    move/from16 v0, v32

    float-to-double v0, v0

    move-wide/from16 v32, v0

    .line 2747
    invoke-virtual/range {v31 .. v33}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    goto/16 :goto_3

    .line 2760
    .restart local v18    # "originX":F
    .restart local v19    # "originY":F
    .restart local v26    # "tempX":F
    .restart local v27    # "tempY":F
    :cond_28
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpl-float v31, v26, v31

    if-lez v31, :cond_18

    .line 2761
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v26, v0

    .line 2762
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    goto/16 :goto_4

    .line 2768
    :cond_29
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpl-float v31, v27, v31

    if-lez v31, :cond_19

    .line 2769
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    move/from16 v32, v0

    add-int v31, v31, v32

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v27, v0

    .line 2770
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSoundEnabled:Z

    goto/16 :goto_5

    .line 2783
    :cond_2a
    const/16 v31, 0x2

    move/from16 v0, v31

    if-ne v6, v0, :cond_2c

    .line 2785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v31, v0

    if-eqz v31, :cond_2b

    .line 2786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldX:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldY:F

    move/from16 v33, v0

    move-object/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v16

    invoke-virtual {v0, v1, v2, v7, v3}, Lcom/samsung/hapticfeedback/HapticEffect;->playEffectByDistance(FFFF)V

    .line 2788
    :cond_2b
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldX:F

    .line 2789
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mOldY:F

    goto/16 :goto_6

    .line 2790
    :cond_2c
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_1b

    .line 2792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v31, v0

    if-eqz v31, :cond_1b

    .line 2793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/hapticfeedback/HapticEffect;->stopAllEffect()V

    goto/16 :goto_6

    .line 2796
    :cond_2d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    move/from16 v31, v0

    if-eqz v31, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    if-eqz v31, :cond_1b

    .line 2797
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2798
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 2799
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    goto/16 :goto_6

    .line 2816
    .end local v18    # "originX":F
    .end local v19    # "originY":F
    .end local v26    # "tempX":F
    .end local v27    # "tempY":F
    :cond_2e
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2f

    .line 2817
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Path;->reset()V

    .line 2818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v33

    invoke-virtual/range {v31 .. v33}, Landroid/graphics/Path;->moveTo(FF)V

    goto/16 :goto_7

    .line 2819
    :cond_2f
    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_30

    if-nez v30, :cond_1f

    .line 2820
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableUpdate(Z)V

    goto/16 :goto_7

    .line 2822
    :cond_31
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v6, v0, :cond_1f

    .line 2823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    move-object/from16 v31, v0

    const/16 v32, 0x1

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableUpdate(Z)V

    goto/16 :goto_7

    .line 2830
    :cond_32
    const/16 v31, 0x2

    move/from16 v0, v31

    if-ne v6, v0, :cond_22

    .line 2831
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPath:Landroid/graphics/Path;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v33

    invoke-virtual/range {v31 .. v33}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_8

    .line 2858
    .restart local v12    # "engine":J
    .restart local v20    # "posttouch":J
    .restart local v22    # "pretouch":J
    .restart local v28    # "time":J
    :cond_33
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v22

    .line 2860
    if-nez v6, :cond_3c

    .line 2861
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    move/from16 v31, v0

    if-eqz v31, :cond_34

    .line 2862
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_34

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_34

    .line 2863
    const/16 v31, 0x5

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_34

    .line 2864
    const/16 v31, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 2867
    :cond_34
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_35

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_37

    .line 2868
    :cond_35
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    .line 2869
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_3a

    .line 2870
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 2878
    :cond_36
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    add-float v32, v32, v33

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    add-float v32, v32, v33

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    invoke-virtual/range {v31 .. v32}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2904
    :cond_37
    :goto_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchX:F

    .line 2905
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchY:F

    .line 2906
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-object/from16 v31, v0

    if-eqz v31, :cond_38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v31

    if-nez v31, :cond_39

    .line 2907
    :cond_38
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-wide/from16 v32, v0

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    move-object/from16 v0, p0

    move-wide/from16 v1, v32

    move-object/from16 v3, p1

    move/from16 v4, v31

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_onTouch(JLandroid/view/MotionEvent;I)Z

    .line 2909
    :cond_39
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    .line 2911
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v31, v0

    if-eqz v31, :cond_41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, p1

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v31

    if-eqz v31, :cond_41

    .line 2912
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v20

    .line 2913
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v32

    sub-long v32, v32, v28

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    .line 2914
    const-string v31, "SpenInView"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "Performance touch process end total = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v34

    sub-long v34, v34, v28

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2915
    const-string v33, " ms pretouch = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v22, v28

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " ms spenview = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v12, v22

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2916
    const-string v33, " ms posttouch = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v20, v12

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " ms action = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 2914
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2917
    const/16 v31, 0x1

    goto/16 :goto_0

    .line 2871
    :cond_3a
    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_36

    .line 2872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    move/from16 v31, v0

    if-nez v31, :cond_3b

    .line 2873
    const/high16 v31, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto/16 :goto_9

    .line 2874
    :cond_3b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_36

    .line 2875
    const/high16 v31, 0x42200000    # 40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto/16 :goto_9

    .line 2882
    :cond_3c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    move/from16 v31, v0

    if-eqz v31, :cond_37

    .line 2883
    const/16 v31, 0x2

    move/from16 v0, v31

    if-ne v6, v0, :cond_40

    .line 2884
    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_3e

    .line 2885
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 2893
    :cond_3d
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    add-float v32, v32, v33

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2894
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    add-float v32, v32, v33

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2895
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v33, v0

    mul-float v32, v32, v33

    invoke-virtual/range {v31 .. v32}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_a

    .line 2886
    :cond_3e
    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_3d

    .line 2887
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    move/from16 v31, v0

    if-nez v31, :cond_3f

    .line 2888
    const/high16 v31, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto :goto_b

    .line 2889
    :cond_3f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_3d

    .line 2890
    const/high16 v31, 0x42200000    # 40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    move/from16 v32, v0

    mul-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto/16 :goto_b

    .line 2897
    :cond_40
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    .line 2898
    const/high16 v31, -0x3d380000    # -100.0f

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    .line 2899
    const/high16 v31, -0x3d380000    # -100.0f

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    .line 2900
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    const/high16 v32, -0x3d380000    # -100.0f

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    const/high16 v32, -0x3d380000    # -100.0f

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/graphics/PointF;->y:F

    goto/16 :goto_a

    .line 2919
    :cond_41
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v20

    .line 2920
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v32

    sub-long v32, v32, v28

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    .line 2921
    const-string v31, "SpenInView"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "Performance touch process end total = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v34

    sub-long v34, v34, v28

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2922
    const-string v33, " ms pretouch = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v22, v28

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " ms spenview = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v12, v22

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    .line 2923
    const-string v33, " ms posttouch = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sub-long v34, v20, v12

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " ms action = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 2921
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2926
    .end local v7    # "newX":F
    .end local v12    # "engine":J
    .end local v16    # "newY":F
    .end local v17    # "originAction":I
    .end local v20    # "posttouch":J
    .end local v22    # "pretouch":J
    .end local v28    # "time":J
    .end local v30    # "toolTypeAction":I
    :cond_42
    const/16 v31, 0x1

    goto/16 :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 749
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_0

    .line 750
    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    .line 751
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->hideSoftInput()V

    .line 757
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    .line 758
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_deinit_c2dJNI()V

    .line 759
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_init_c2dJNI()V

    .line 763
    :cond_1
    return-void

    .line 752
    :cond_2
    if-nez p2, :cond_0

    .line 753
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->showSoftInput()V

    goto :goto_0
.end method

.method protected onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 766
    const-string v0, "SpenInView"

    const-string v1, "onWindowFocusChanged() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    if-eqz p1, :cond_1

    .line 768
    const-string v0, "SpenInView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->registerPensoundSolution()V

    .line 770
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->showSoftInput()V

    .line 777
    :cond_0
    :goto_0
    const-string v0, "SpenInView"

    const-string v1, "onWindowFocusChanged() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    return-void

    .line 774
    :cond_1
    const-string v0, "SpenInView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->unregisterPensoundSolution()V

    goto :goto_0
.end method

.method public pauseReplay()V
    .locals 4

    .prologue
    .line 4674
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4680
    :cond_0
    :goto_0
    return-void

    .line 4677
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_pauseReplay(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4678
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method printRect(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "srcRect"    # Landroid/graphics/Rect;

    .prologue
    .line 915
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 916
    const-string v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 915
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    return-void
.end method

.method printRect(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 920
    if-nez p2, :cond_0

    .line 921
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    :goto_0
    return-void

    .line 923
    :cond_0
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 924
    const-string v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 923
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public requestPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 7
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z

    .prologue
    const/4 v6, 0x0

    .line 4193
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4206
    :goto_0
    return v6

    .line 4196
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4197
    const-string v0, "SpenInView"

    const-string v1, "requestPageDoc is closed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4201
    :cond_1
    const-string v0, "SpenInView"

    const-string/jumbo v1, "start requestPageDoc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4202
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 4203
    .local v5, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4204
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4205
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v4, 0x4

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 4206
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public resumeReplay()V
    .locals 4

    .prologue
    .line 4689
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4695
    :cond_0
    :goto_0
    return-void

    .line 4692
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_resumeReplay(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4693
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public retakeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 16
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "canvasLayout"    # Landroid/view/ViewGroup;
    .param p3, "strokeFrameContainer"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .param p4, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .prologue
    .line 4328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 4329
    :cond_0
    const/16 v2, 0x8

    .line 4330
    const-string v3, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    .line 4329
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 4333
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    if-nez p4, :cond_3

    .line 4334
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Arguments is null. activity = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewgroup = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 4335
    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stroke = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " listener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4334
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4338
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->checkMDMCameraLock()Z

    move-result v2

    if-nez v2, :cond_4

    .line 4339
    const/16 v2, 0x22

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-interface {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 4367
    :goto_0
    return-void

    .line 4344
    :cond_4
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    .line 4345
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    .line 4346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v2, :cond_5

    .line 4347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    .line 4350
    :cond_5
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 4351
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 4352
    .local v5, "bgBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/16 v4, -0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 4353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v6

    .line 4354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v11

    .line 4355
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v13

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    move-object/from16 v14, p2

    .line 4353
    invoke-virtual/range {v2 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->retakeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V

    .line 4357
    new-instance v15, Landroid/os/Handler;

    invoke-direct {v15}, Landroid/os/Handler;-><init>()V

    .line 4358
    .local v15, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    .line 4366
    const-wide/16 v6, 0x1e

    .line 4358
    invoke-virtual {v15, v2, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .prologue
    .line 3565
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v1, :cond_0

    .line 3566
    const-string v1, "SpenInView"

    const-string v2, "setBackgroundColorListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3567
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 3569
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_0

    .line 3570
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 3571
    .local v0, "enable":Z
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_0

    .line 3572
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 3573
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 3577
    .end local v0    # "enable":Z
    :cond_0
    return-void

    .line 3570
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBlankColor(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 3500
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3507
    :cond_0
    :goto_0
    return-void

    .line 3503
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 3504
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_0

    .line 3505
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setClippingPath(Landroid/graphics/Path;)V
    .locals 0
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    .line 1409
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mClippingPath:Landroid/graphics/Path;

    .line 1410
    return-void
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .prologue
    .line 5363
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5367
    :goto_0
    return-void

    .line 5366
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    goto :goto_0
.end method

.method public setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 5
    .param p1, "control"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .prologue
    const/4 v4, 0x0

    .line 4565
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4590
    .end local p1    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    :cond_0
    :goto_0
    return-void

    .line 4568
    .restart local p1    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    :cond_1
    if-nez p1, :cond_2

    .line 4569
    const/4 v0, 0x7

    const-string v1, " control instance must not be null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 4571
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_3

    .line 4572
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    .line 4573
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .line 4576
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    .line 4578
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    .line 4579
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;)V

    .line 4581
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateRectList()V

    .line 4583
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_4

    .line 4584
    check-cast p1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    .end local p1    # "control":Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;

    invoke-direct {v0, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;)V

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;)V

    .line 4587
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 4588
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .prologue
    .line 5540
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5544
    :goto_0
    return-void

    .line 5543
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    goto :goto_0
.end method

.method public setDottedLineEnabled(ZIII[FF)V
    .locals 5
    .param p1, "enable"    # Z
    .param p2, "intervalHeight"    # I
    .param p3, "color"    # I
    .param p4, "thickness"    # I
    .param p5, "pathIntervals"    # [F
    .param p6, "phase"    # F

    .prologue
    const/4 v4, 0x0

    .line 4789
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4810
    :cond_0
    :goto_0
    return-void

    .line 4792
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-ne v0, p1, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    if-ne v0, p2, :cond_3

    .line 4793
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-ne v0, p3, :cond_3

    .line 4794
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    int-to-float v1, p4

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 4795
    :cond_3
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    .line 4796
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-eqz v0, :cond_4

    .line 4797
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    .line 4798
    int-to-float v0, p4

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    .line 4800
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    .line 4801
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 4802
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    int-to-float v1, p4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4803
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    invoke-direct {v1, p5, p6}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 4804
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4808
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0

    .line 4806
    :cond_4
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    goto :goto_1
.end method

.method public setDoubleTapZoomable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 5630
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5635
    :goto_0
    return-void

    .line 5634
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDoubleTapZoomable:Z

    goto :goto_0
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .prologue
    .line 5464
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5468
    :goto_0
    return-void

    .line 5467
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    goto :goto_0
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v3, 0x5

    .line 3817
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 3858
    :cond_0
    :goto_0
    return-void

    .line 3820
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 3822
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v0, :cond_5

    .line 3823
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    .line 3824
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    .line 3825
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    .line 3826
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    if-ne v1, v2, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextEraserEnabled(Z)V

    .line 3830
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_3

    .line 3831
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput v6, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 3835
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 3836
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 3837
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 3838
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 3839
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_7

    .line 3840
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    .line 3841
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    .line 3840
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3847
    :cond_4
    :goto_2
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setEraserType(JI)V

    .line 3848
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setEraserSize(JF)V

    .line 3850
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_5

    .line 3851
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    .line 3855
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    if-eqz v0, :cond_0

    .line 3856
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto/16 :goto_0

    .line 3827
    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    .line 3843
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .prologue
    .line 5559
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5563
    :goto_0
    return-void

    .line 5562
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    goto :goto_0
.end method

.method public setForceStretchView(ZII)Z
    .locals 7
    .param p1, "enable"    # Z
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v4, 0x1

    .line 1353
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;-><init>()V

    .line 1354
    .local v0, "stretchInfo":Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    iput-boolean p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;->isStretch:Z

    .line 1355
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchWidth:I

    iput p2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;->stretchWidth:I

    .line 1356
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchHeight:I

    iput p3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$StretchInfo;->stretchHeight:I

    .line 1358
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    .line 1359
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v1, :cond_1

    .line 1360
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-lez v1, :cond_0

    .line 1361
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    .line 1363
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-lez v1, :cond_1

    .line 1364
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    .line 1367
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1368
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1370
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 1371
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->deltaZoomSizeChanged()V

    .line 1372
    const/4 v0, 0x0

    .line 1374
    return v4
.end method

.method public setHighlight(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4842
    .local p1, "highlightInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;>;"
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4850
    :cond_0
    :goto_0
    return-void

    .line 4845
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    .line 4847
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 4848
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public setHorizontalScrollBarEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4963
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4969
    :cond_0
    :goto_0
    return-void

    .line 4966
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_0

    .line 4967
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableHorizontalScroll(Z)V

    goto :goto_0
.end method

.method public setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "leftScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "rightScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 4444
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4452
    :cond_0
    :goto_0
    return-void

    .line 4447
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 4448
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    .line 4449
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableHorizontalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .prologue
    .line 5306
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5310
    :goto_0
    return-void

    .line 5309
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    goto :goto_0
.end method

.method public setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    .prologue
    .line 5606
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5611
    :goto_0
    return-void

    .line 5610
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    goto :goto_0
.end method

.method public setHyperTextViewEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4520
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4526
    :goto_0
    return-void

    .line 4524
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    .line 4525
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setHyperTextViewEnabled(JZ)V

    goto :goto_0
.end method

.method public setLongPressEnabled(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 3007
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3008
    const/4 v0, 0x0

    .line 3011
    :goto_0
    return v0

    .line 3010
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsLongPressEnable:Z

    .line 3011
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    .prologue
    .line 5325
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5329
    :goto_0
    return-void

    .line 5328
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 3382
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3383
    const/4 v0, 0x0

    .line 3385
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setMaxZoomRatio(JF)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 3417
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3418
    const/4 v0, 0x0

    .line 3420
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setMinZoomRatio(JF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 9
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "direction"    # I
    .param p3, "type"    # I
    .param p4, "centerY"    # F

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 4131
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 4189
    :cond_0
    :goto_0
    return v6

    .line 4134
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-ne p1, v1, :cond_2

    .line 4135
    const-string v1, "SpenInView"

    const-string v2, "setPageDoc is same"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v8

    .line 4136
    goto :goto_0

    .line 4138
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_3

    .line 4139
    const-string v1, "SpenInView"

    const-string v2, "setPageDoc is closed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4142
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4146
    const-string v1, "SpenInView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SetPageDoc, direction="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4148
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setType(I)V

    .line 4149
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 4150
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setCanvasInformation(IIII)V

    .line 4152
    const/4 v7, 0x1

    .line 4153
    .local v7, "isMemoryEnough":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->saveScreenshot()Z

    move-result v1

    if-nez v1, :cond_4

    .line 4154
    const-string v1, "SpenInView"

    const-string v2, "setPageDoc. No enough memory1. Change to PAGE_TRANSITION_EFFECT_NONE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4155
    const/4 v7, 0x0

    .line 4158
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 4159
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 4161
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    .line 4162
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    .line 4161
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setCanvasInformation1(IIII)V

    .line 4164
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 4165
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v4, 0x0

    move-object v1, p0

    move v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V

    .line 4167
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_5

    .line 4168
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_7

    move v0, v8

    .line 4169
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_5

    .line 4170
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 4171
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 4175
    .end local v0    # "enable":Z
    :cond_5
    if-eqz v7, :cond_8

    .line 4176
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v2, v3, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    .line 4177
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->startAnimation(I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 4178
    const-string v1, "SpenInView"

    const-string v2, "setPageDoc. No enough memory2. Change to PAGE_TRANSITION_EFFECT_NONE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4179
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    :goto_2
    move v6, v8

    .line 4189
    goto/16 :goto_0

    :cond_7
    move v0, v6

    .line 4168
    goto :goto_1

    .line 4182
    :cond_8
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v2, v3, v1, v8}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 4183
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 4185
    :cond_9
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_2
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 8
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4057
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 4093
    :cond_0
    :goto_0
    return v2

    .line 4060
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-ne p1, v3, :cond_2

    .line 4061
    const-string v2, "SpenInView"

    const-string v3, "setPageDoc is same"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 4062
    goto :goto_0

    .line 4064
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v3

    if-nez v3, :cond_3

    .line 4065
    const-string v1, "SpenInView"

    const-string v3, "setPageDoc is closed"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4068
    :cond_3
    const-string v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SetPageDoc, direction : NONE, isUpdate = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4069
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v3

    if-nez v3, :cond_0

    .line 4073
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 4074
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 4075
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 4077
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v3, :cond_5

    .line 4078
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v3

    shr-int/lit8 v3, v3, 0x18

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0xff

    if-ne v3, v4, :cond_7

    move v0, v1

    .line 4079
    .local v0, "enable":Z
    :goto_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v3, :cond_5

    .line 4080
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v3, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 4081
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 4085
    .end local v0    # "enable":Z
    :cond_5
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v4, v5, v3, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 4086
    if-eqz p2, :cond_6

    .line 4087
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    :goto_2
    move v2, v1

    .line 4093
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 4078
    goto :goto_1

    .line 4090
    :cond_8
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_2
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .prologue
    .line 5401
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 5402
    return-void
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 7
    .param p1, "position"    # Landroid/graphics/PointF;

    .prologue
    const/4 v4, 0x0

    .line 3453
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3466
    :goto_0
    return-void

    .line 3457
    :cond_0
    iget v0, p1, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_1

    iget v0, p1, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 3458
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    .line 3459
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setCheckCursorOnScroll(Z)V

    .line 3463
    :cond_2
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget v4, p1, Landroid/graphics/PointF;->x:F

    iget v5, p1, Landroid/graphics/PointF;->y:F

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPan(JFFZ)V

    .line 3465
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    goto :goto_0
.end method

.method public setParent(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 4549
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    .line 4550
    return-void
.end method

.method public setPenButtonSelectionEnabled(Z)V
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    .line 5243
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5249
    :goto_0
    return-void

    .line 5247
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsPenButtonSelectionEnabled:Z

    .line 5248
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/4 v4, 0x5

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    const/4 v6, 0x1

    :goto_1
    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .prologue
    .line 5445
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5449
    :goto_0
    return-void

    .line 5448
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    goto :goto_0
.end method

.method public setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .prologue
    .line 5417
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 5430
    :cond_0
    :goto_0
    return-void

    .line 5420
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .line 5422
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    if-eqz v1, :cond_0

    .line 5423
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    if-nez v1, :cond_0

    .line 5424
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.samsung.pen.INSERT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 5425
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.pen.INSERT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 5426
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    .line 5427
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 8
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x5

    .line 3690
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 3770
    :cond_0
    :goto_0
    return-void

    .line 3693
    :cond_1
    const/4 v0, 0x0

    .line 3694
    .local v0, "valueChanged":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v1, :cond_7

    if-eqz p1, :cond_7

    .line 3695
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3696
    or-int/lit8 v0, v0, 0x1

    .line 3698
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    if-eq v1, v2, :cond_3

    .line 3699
    or-int/lit8 v0, v0, 0x2

    .line 3701
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_4

    .line 3702
    or-int/lit8 v0, v0, 0x4

    .line 3704
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iget-boolean v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    if-eq v1, v2, :cond_5

    .line 3705
    or-int/lit8 v0, v0, 0x8

    .line 3707
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 3708
    or-int/lit8 v0, v0, 0x10

    .line 3710
    :cond_6
    if-eqz v0, :cond_0

    .line 3712
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v1, :cond_8

    const/16 v0, 0x1f

    .line 3713
    :cond_8
    if-eqz p1, :cond_f

    .line 3714
    new-instance v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 3715
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 3716
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 3717
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 3718
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 3719
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 3724
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v1, :cond_0

    .line 3725
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_9

    .line 3726
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    const/high16 v2, 0x41200000    # 10.0f

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 3728
    :cond_9
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->bIsSupport:Z

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v1, :cond_c

    .line 3729
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 3730
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 3731
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 3732
    :cond_a
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexPencil:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    .line 3739
    :cond_b
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 3740
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 3743
    :cond_c
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v1, :cond_d

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_d

    .line 3744
    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_d

    .line 3745
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_d

    .line 3746
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_d

    .line 3747
    and-int/lit8 v1, v0, 0x7

    if-eqz v1, :cond_d

    .line 3748
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 3749
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 3748
    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3750
    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v1

    if-ne v1, v7, :cond_d

    .line 3751
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenHoverPoint(Ljava/lang/String;)V

    .line 3755
    :cond_d
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPenStyle(JLjava/lang/String;)Z

    .line 3756
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPenColor(JI)V

    .line 3757
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setPenSize(JF)V

    .line 3758
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_enablePenCurve(JZ)V

    .line 3759
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setAdvancedSetting(JLjava/lang/String;)V

    .line 3761
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v1, :cond_e

    .line 3762
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 3765
    :cond_e
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    if-eqz v1, :cond_0

    .line 3766
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto/16 :goto_0

    .line 3721
    :cond_f
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    goto/16 :goto_1

    .line 3733
    :cond_10
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 3734
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 3735
    :cond_11
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexBrush:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    goto/16 :goto_2

    .line 3736
    :cond_12
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 3737
    :cond_13
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIndexMarker:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->activePen:I

    goto/16 :goto_2
.end method

.method public setPenTooltipImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "penName"    # Ljava/lang/String;
    .param p2, "image"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 5638
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-nez v0, :cond_0

    .line 5642
    :goto_0
    return-void

    .line 5641
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setPenTooltipImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .prologue
    .line 5598
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5603
    :goto_0
    return-void

    .line 5601
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "Register setPostDrawListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5602
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    goto :goto_0
.end method

.method public setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    .prologue
    .line 5578
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5583
    :goto_0
    return-void

    .line 5581
    :cond_0
    const-string v0, "SpenInView"

    const-string v1, "Register setPreDrawListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5582
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    goto :goto_0
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 5268
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5272
    :goto_0
    return-void

    .line 5271
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .prologue
    .line 5483
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5487
    :goto_0
    return-void

    .line 5486
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x5

    .line 3902
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3943
    :cond_0
    :goto_0
    return-void

    .line 3905
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 3907
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v0, :cond_4

    .line 3908
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 3909
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 3912
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 3913
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 3914
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 3915
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 3916
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v0, :cond_6

    .line 3917
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_5

    .line 3918
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/high16 v1, 0x41a00000    # 20.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3932
    :cond_3
    :goto_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setRemoverType(JI)V

    .line 3933
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setRemoverSize(JF)V

    .line 3935
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_4

    .line 3936
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 3940
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    if-eqz v0, :cond_0

    .line 3941
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0

    .line 3920
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 3923
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v5, :cond_3

    .line 3924
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_7

    .line 3925
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/high16 v1, 0x42200000    # 40.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 3927
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    .prologue
    .line 5344
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5348
    :goto_0
    return-void

    .line 5347
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    goto :goto_0
.end method

.method public setReplayPosition(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 4746
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4752
    :cond_0
    :goto_0
    return-void

    .line 4749
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setReplayPosition(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4750
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setReplaySpeed(I)V
    .locals 4
    .param p1, "speed"    # I

    .prologue
    .line 4721
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4727
    :cond_0
    :goto_0
    return-void

    .line 4724
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setReplaySpeed(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4725
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setRequestPageDocListener(Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    .prologue
    .line 5622
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5627
    :goto_0
    return-void

    .line 5626
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRequestPageDocListener:Lcom/samsung/android/sdk/pen/engine/SpenRequestPageDocListener;

    goto :goto_0
.end method

.method public setScrollBarEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4929
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4936
    :cond_0
    :goto_0
    return-void

    .line 4932
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_0

    .line 4933
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableScroll(Z)V

    .line 4934
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreen()V

    goto :goto_0
.end method

.method public setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    .prologue
    .line 5521
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5525
    :goto_0
    return-void

    .line 5524
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    goto :goto_0
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 5
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    const/4 v4, 0x5

    .line 3987
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4005
    :cond_0
    :goto_0
    return-void

    .line 3990
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .line 3991
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 3992
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 3993
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 3994
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 3995
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3998
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    if-eqz v0, :cond_3

    .line 3999
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setSelectionType(JI)V

    .line 4002
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    if-eqz v0, :cond_0

    .line 4003
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "region"    # Landroid/graphics/Rect;
    .param p3, "effectFrame"    # I
    .param p4, "zoomOutResponseTime"    # I
    .param p5, "zoomRatio"    # F

    .prologue
    .line 4393
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4400
    :cond_0
    :goto_0
    return-void

    .line 4396
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 4397
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    .line 4398
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableSmartScale(ZLandroid/graphics/Rect;IIF)V

    goto :goto_0
.end method

.method public setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    .prologue
    .line 5502
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5506
    :goto_0
    return-void

    .line 5505
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    goto :goto_0
.end method

.method public setTextCursorEnabled(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 3022
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3023
    const/4 v0, 0x0

    .line 3026
    :goto_0
    return v0

    .line 3025
    :cond_0
    sput-boolean p1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHoverCursorEnable:Z

    .line 3026
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x5

    .line 3596
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 3634
    :cond_0
    :goto_0
    return-void

    .line 3599
    :cond_1
    if-eqz p1, :cond_0

    .line 3602
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_3

    .line 3603
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    .line 3604
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 3607
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    if-eqz v0, :cond_0

    .line 3608
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    invoke-interface {v0, p1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;I)V

    goto :goto_0

    .line 3614
    :cond_3
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 3615
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 3616
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 3617
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 3618
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 3619
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3622
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-eqz v0, :cond_6

    .line 3623
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 3624
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3626
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-gez v0, :cond_6

    .line 3627
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3631
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    if-eqz v0, :cond_0

    .line 3632
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-interface {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;I)V

    goto :goto_0
.end method

.method public setToolTipEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 4533
    const-string v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setToolTipEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4534
    if-nez p1, :cond_0

    .line 4535
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 4537
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    .line 4538
    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 8
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x5

    const/4 v4, 0x2

    .line 3167
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3240
    :goto_0
    return-void

    .line 3170
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStrokeDrawing:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCurrentDrawingTooltype:I

    if-ne p1, v0, :cond_1

    if-eq p2, v4, :cond_1

    .line 3171
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->cancelStroke()V

    .line 3173
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    .line 3174
    if-eq p1, v5, :cond_2

    .line 3175
    if-ne p2, v5, :cond_8

    .line 3176
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    .line 3183
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_3

    .line 3184
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setToolTypeAction(II)V

    .line 3187
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_4

    .line 3188
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setToolTypeAction(II)V

    .line 3190
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_7

    .line 3191
    if-ne p2, v4, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_9

    .line 3192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    if-nez v0, :cond_5

    .line 3193
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    .line 3198
    :cond_5
    :goto_2
    if-ne p2, v4, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_a

    .line 3199
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_6

    .line 3200
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 3202
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 3203
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 3202
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3204
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenHoverPoint(Ljava/lang/String;)V

    .line 3239
    :cond_7
    :goto_3
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setToolTypeAction(JII)V

    goto :goto_0

    .line 3178
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    goto :goto_1

    .line 3196
    :cond_9
    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPoint:Landroid/graphics/Point;

    goto :goto_2

    .line 3205
    :cond_a
    const/4 v0, 0x3

    if-ne p2, v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_d

    .line 3206
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v0, :cond_b

    .line 3207
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 3209
    :cond_b
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_c

    .line 3210
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    .line 3211
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    .line 3210
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 3213
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 3215
    :cond_d
    const/4 v0, 0x4

    if-ne p2, v0, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_12

    .line 3216
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-nez v0, :cond_e

    .line 3217
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 3219
    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v0, :cond_10

    .line 3220
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_f

    .line 3221
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/high16 v1, 0x41a00000    # 20.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 3223
    :cond_f
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 3225
    :cond_10
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v6, :cond_7

    .line 3226
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsStretch:Z

    if-eqz v0, :cond_11

    .line 3227
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/high16 v1, 0x42200000    # 40.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchXRatio:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStretchYRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(FFF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 3229
    :cond_11
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 3232
    :cond_12
    if-ne p2, v5, :cond_13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_13

    .line 3233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableHoverImage()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 3235
    :cond_13
    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 5287
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5291
    :goto_0
    return-void

    .line 5290
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setVerticalScrollBarEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 4993
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4999
    :cond_0
    :goto_0
    return-void

    .line 4996
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_0

    .line 4997
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableVerticalScroll(Z)V

    goto :goto_0
.end method

.method public setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "topScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "bottomScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 4495
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4503
    :cond_0
    :goto_0
    return-void

    .line 4498
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 4499
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    .line 4500
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableVerticalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 4545
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    .line 4546
    return-void
.end method

.method public setZoom(FFF)V
    .locals 7
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 3328
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3334
    :goto_0
    return-void

    .line 3331
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setZoom(JFFF)V

    .line 3333
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .prologue
    .line 5382
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5386
    :goto_0
    return-void

    .line 5385
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    goto :goto_0
.end method

.method public setZoomPadBoxHeight(F)V
    .locals 4
    .param p1, "height"    # F

    .prologue
    .line 5049
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5055
    :cond_0
    :goto_0
    return-void

    .line 5053
    :cond_1
    const-string v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setZoomPadBoxHeight ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5054
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setBoxHeight(F)V

    goto :goto_0
.end method

.method public setZoomPadBoxHeightEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 5159
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5163
    :cond_0
    :goto_0
    return-void

    .line 5162
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setBoxHeightEnabled(Z)V

    goto :goto_0
.end method

.method public setZoomPadBoxPosition(Landroid/graphics/PointF;)V
    .locals 4
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 5066
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5072
    :cond_0
    :goto_0
    return-void

    .line 5070
    :cond_1
    const-string v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setZoomPadBoxPosition ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5071
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setBoxPosition(FF)V

    goto :goto_0
.end method

.method public setZoomPadButtonEnabled(ZZZZZ)V
    .locals 6
    .param p1, "left"    # Z
    .param p2, "right"    # Z
    .param p3, "enter"    # Z
    .param p4, "up"    # Z
    .param p5, "down"    # Z

    .prologue
    .line 5185
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5189
    :cond_0
    :goto_0
    return-void

    .line 5188
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setButtonEnabled(ZZZZZ)V

    goto :goto_0
.end method

.method public setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    .prologue
    .line 5614
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5619
    :goto_0
    return-void

    .line 5618
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    goto :goto_0
.end method

.method public setZoomPadPosition(Landroid/graphics/PointF;)V
    .locals 4
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 5088
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5094
    :cond_0
    :goto_0
    return-void

    .line 5092
    :cond_1
    const-string v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setZoomPadPosition ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5093
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPadPosition(FF)V

    goto :goto_0
.end method

.method public setZoomable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 3281
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3286
    :goto_0
    return-void

    .line 3284
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomable:Z

    .line 3285
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_enableZoom(JZ)V

    goto :goto_0
.end method

.method public startReplay()V
    .locals 4

    .prologue
    .line 4643
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    .line 4644
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4650
    :cond_0
    :goto_0
    return-void

    .line 4647
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_startReplay(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4648
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public startTemporaryStroke()V
    .locals 4

    .prologue
    .line 4876
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4881
    :goto_0
    return-void

    .line 4879
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTemporaryStroke:Z

    .line 4880
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_startTemporaryStroke(J)V

    goto :goto_0
.end method

.method public startZoomPad()V
    .locals 7

    .prologue
    .line 5110
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 5133
    :cond_0
    :goto_0
    return-void

    .line 5114
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 5119
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 5120
    .local v6, "bgBitmap":Landroid/graphics/Bitmap;
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/16 v2, -0x64

    invoke-direct {p0, v0, v1, v2, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 5122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setScreenStart(II)V

    .line 5123
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPanAndZoom(FFFFF)V

    .line 5124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setLayer(Ljava/util/ArrayList;)V

    .line 5125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentLayout:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->start(Landroid/view/ViewGroup;II)V

    .line 5127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setReference(Landroid/graphics/Bitmap;)V

    .line 5129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_2

    .line 5130
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 5132
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public stopReplay()V
    .locals 4

    .prologue
    .line 4659
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4665
    :cond_0
    :goto_0
    return-void

    .line 4662
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_stopReplay(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4663
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public stopTemporaryStroke()V
    .locals 4

    .prologue
    .line 4890
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4895
    :goto_0
    return-void

    .line 4893
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsTemporaryStroke:Z

    .line 4894
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_stopTemporaryStroke(J)V

    goto :goto_0
.end method

.method public stopZoomPad()V
    .locals 4

    .prologue
    .line 5136
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-nez v0, :cond_1

    .line 5141
    :cond_0
    :goto_0
    return-void

    .line 5140
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->stop()V

    goto :goto_0
.end method

.method public takeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Ljava/util/List;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 18
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "canvasLayout"    # Landroid/view/ViewGroup;
    .param p4, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;",
            "Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4283
    .local p3, "strokeList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 4284
    :cond_0
    const/16 v2, 0x8

    .line 4285
    const-string v3, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    .line 4284
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 4287
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    if-nez p4, :cond_3

    .line 4288
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Arguments is null. activity = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewgroup = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 4289
    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stroke = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " listener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4288
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4292
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->checkMDMCameraLock()Z

    move-result v2

    if-nez v2, :cond_4

    .line 4293
    const/16 v2, 0x21

    .line 4294
    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 4293
    move-object/from16 v0, p4

    invoke-interface {v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 4324
    :goto_0
    return-void

    .line 4298
    :cond_4
    new-instance v16, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 4299
    .local v16, "oc":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4300
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 4304
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    .line 4306
    new-instance v15, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v15}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 4307
    .local v15, "bc":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    invoke-virtual {v15, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4308
    new-instance v9, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 4309
    .local v9, "c":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setOutOfViewEnabled(Z)V

    .line 4310
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setRotatable(Z)V

    .line 4311
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    .line 4312
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4313
    invoke-virtual {v9, v15}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setHistoryTag()V

    .line 4315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4316
    const-string v2, "STROKE_FRAME"

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setExtraDataInt(Ljava/lang/String;I)V

    .line 4317
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    .line 4318
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 4319
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 4320
    .local v5, "bgBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const/16 v4, -0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 4321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v6

    .line 4322
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v12

    .line 4323
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v13

    move-object/from16 v3, p1

    move-object/from16 v14, p2

    .line 4321
    invoke-virtual/range {v2 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->takeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V

    goto/16 :goto_0

    .line 4300
    .end local v5    # "bgBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "c":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .end local v15    # "bc":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 4301
    .local v17, "s":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4302
    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto/16 :goto_1
.end method

.method public declared-synchronized update()V
    .locals 6

    .prologue
    .line 2500
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2519
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2504
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2505
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_update(J)V

    .line 2507
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_2

    .line 2508
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_3

    const/4 v0, 0x1

    .line 2509
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_2

    .line 2510
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 2511
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    .line 2515
    .end local v0    # "enable":Z
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2516
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->refresh()V

    .line 2517
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mNotePad:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2500
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 2508
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 4
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 2614
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2628
    :goto_0
    return-void

    .line 2617
    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    .line 2618
    :cond_1
    const-string v0, "SpenInView"

    const-string v1, "The parameter \'userDataList\' cannot be null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2622
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2623
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    array-length v2, p1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateRedo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2624
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2626
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->fitControlToObject()V

    .line 2627
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    goto :goto_0
.end method

.method public updateScreen()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2551
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2562
    :cond_0
    :goto_0
    return-void

    .line 2555
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v1

    if-nez v1, :cond_0

    .line 2559
    const-string v1, "SpenInView"

    const-string/jumbo v2, "updateScreen"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2560
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v2, v2

    invoke-direct {v0, v6, v6, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2561
    .local v0, "rectf":Landroid/graphics/RectF;
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public updateScreenFrameBuffer()V
    .locals 4

    .prologue
    .line 2528
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2542
    :goto_0
    return-void

    .line 2532
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2534
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->pauseReplay()V

    .line 2536
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateAllScreenFrameBuffer(J)V

    .line 2538
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resumeReplay()V

    goto :goto_0

    .line 2540
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateAllScreenFrameBuffer(J)V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 4
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 2581
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2595
    :goto_0
    return-void

    .line 2584
    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    .line 2585
    :cond_1
    const-string v0, "SpenInView"

    const-string v1, "The parameter \'userDataList\' cannot be null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2589
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2590
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:J

    array-length v2, p1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->Native_updateUndo(J[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2591
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2593
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->fitControlToObject()V

    .line 2594
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateNotepad()V

    goto :goto_0
.end method
