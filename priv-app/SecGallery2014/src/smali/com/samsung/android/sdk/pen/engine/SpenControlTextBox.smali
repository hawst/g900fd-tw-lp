.class public Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
.super Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.source "SpenControlTextBox.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_BORDER_MARGIN:I = 0x3

.field private static final TEXT_INPUT_UNLIMITED:I = -0x1


# instance fields
.field private BOTTOM_MARGIN:I

.field private LEFT_MARGIN:I

.field private RIGHT_MARGIN:I

.field private TOP_MARGIN:I

.field private mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

.field private mEditable:Z

.field private mFirstDraw:Z

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mIsShowSoftInputEnable:Z

.field private mReceiveActionDown:Z

.field private final mRequestObjectChange:Ljava/lang/Runnable;

.field private mSizeChanged:Z

.field private mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

.field private final mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

.field private mTextLimitCount:I

.field private mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private mTransactionFitTextBox:Z

.field private final mUpdateHandler:Landroid/os/Handler;

.field private mUseTextEraser:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 117
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 34
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 36
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    .line 37
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    .line 38
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    .line 39
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    .line 41
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    .line 46
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    .line 47
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    .line 48
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    .line 49
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextLimitCount:I

    .line 54
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    .line 59
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    .line 1258
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .line 119
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 120
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUpdateHandler:Landroid/os/Handler;

    .line 124
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    .line 130
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Z)V
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    return-object v0
.end method

.method private checkInvalidStateEditable()V
    .locals 1

    .prologue
    .line 1397
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v0, :cond_0

    .line 1398
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 1400
    :cond_0
    return-void
.end method

.method private checkSettingTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x7

    .line 701
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    .line 702
    :cond_0
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 705
    :cond_1
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    if-ltz v0, :cond_2

    .line 706
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    .line 707
    :cond_2
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 710
    :cond_3
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    cmpg-float v0, v0, v3

    if-ltz v0, :cond_4

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_5

    .line 711
    :cond_4
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 714
    :cond_5
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-ltz v0, :cond_6

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-le v0, v2, :cond_7

    .line 715
    :cond_6
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 717
    :cond_7
    return-void
.end method

.method private checkTextBoxValidation()Z
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-nez v0, :cond_0

    .line 695
    const/4 v0, 0x0

    .line 697
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private verifyRect(Landroid/graphics/RectF;)Z
    .locals 3
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1197
    if-nez p1, :cond_1

    .line 1204
    :cond_0
    :goto_0
    return v0

    .line 1201
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1202
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public appendText(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 728
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 738
    :goto_0
    return-void

    .line 732
    :cond_0
    if-nez p1, :cond_1

    .line 733
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 737
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1105
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    .line 1106
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1107
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_0

    .line 1108
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    .line 1109
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 1140
    :cond_0
    :goto_0
    return-void

    .line 1115
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_2

    .line 1116
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getObjectText()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    .line 1117
    .local v1, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    .line 1118
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1120
    if-eqz v1, :cond_2

    .line 1121
    :try_start_1
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->parseHyperText()V

    .line 1123
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v2, :cond_3

    .line 1124
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    .line 1125
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onVisibleUpdated(Z)V
    :try_end_1
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1139
    .end local v1    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :cond_2
    :goto_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    goto :goto_0

    .line 1127
    .restart local v1    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :cond_3
    const/4 v2, 0x0

    :try_start_2
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    .line 1128
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V
    :try_end_2
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1131
    :catch_0
    move-exception v0

    .line 1132
    .local v0, "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    :try_start_3
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V
    :try_end_3
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 1135
    .end local v0    # "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    .end local v1    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :catch_1
    move-exception v0

    .line 1136
    .restart local v0    # "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V

    goto :goto_1
.end method

.method public fit(Z)V
    .locals 1
    .param p1, "checkCursorPosition"    # Z

    .prologue
    .line 442
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 444
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    if-eqz v0, :cond_0

    .line 455
    :goto_0
    return-void

    .line 448
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    .line 450
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 454
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    goto :goto_0
.end method

.method public getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 2

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 484
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 485
    :cond_0
    const/4 v1, 0x0

    .line 488
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    goto :goto_0
.end method

.method protected getPanKey(II)Ljava/lang/String;
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v0, :cond_1

    .line 139
    if-le p1, p2, :cond_0

    .line 140
    new-instance v0, Ljava/lang/String;

    const-string v1, "WidthHeight"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 145
    :goto_0
    return-object v0

    .line 142
    :cond_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "HeightWidth"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getPanKey(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPixel(II)I
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1187
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    .line 1188
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getX()F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    sub-int/2addr p1, v0

    .line 1189
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getY()F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    sub-int/2addr p2, v0

    .line 1191
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getPixel(II)I

    move-result v0

    .line 1193
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 1
    .param p1, "onlySelection"    # Z

    .prologue
    .line 816
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 817
    const/4 v0, 0x0

    .line 820
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextCursorPosition()I
    .locals 1

    .prologue
    .line 865
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 866
    const/4 v0, 0x0

    .line 871
    :goto_0
    return v0

    .line 869
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkInvalidStateEditable()V

    .line 871
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v0

    goto :goto_0
.end method

.method public getTextLimit()I
    .locals 1

    .prologue
    .line 909
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 910
    const/4 v0, -0x1

    .line 913
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLimit()I

    move-result v0

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 2

    .prologue
    .line 1084
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    .line 1085
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 1086
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 1087
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 1088
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 1089
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 1090
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 1091
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 1092
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 1094
    return-object v0
.end method

.method protected handleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    if-nez v11, :cond_0

    .line 498
    const/4 v11, 0x0

    .line 605
    :goto_0
    return v11

    .line 501
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v11

    and-int/lit16 v11, v11, 0xff

    packed-switch v11, :pswitch_data_0

    .line 605
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v11

    goto :goto_0

    .line 503
    :pswitch_0
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v11

    if-nez v11, :cond_1

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v11

    if-nez v11, :cond_1

    .line 504
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 506
    :cond_1
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 507
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    .line 510
    :cond_2
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isHorizontalResizeZonePressed()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 511
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v9

    .line 512
    .local v9, "option":I
    and-int/lit8 v9, v9, -0x2

    .line 513
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    .line 516
    .end local v9    # "option":I
    :cond_3
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isVerticalResizeZonePressed()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 517
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v9

    .line 518
    .restart local v9    # "option":I
    and-int/lit8 v9, v9, -0x3

    .line 519
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    .line 522
    .end local v9    # "option":I
    :cond_4
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultWidth()I

    move-result v7

    .line 523
    .local v7, "minTextWidth":I
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v6

    .line 525
    .local v6, "minTextHeight":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lt v5, v11, :cond_8

    .line 585
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v11

    if-eqz v11, :cond_5

    iget-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTrivialMovingEn:Z

    if-eqz v11, :cond_1a

    .line 586
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    .line 592
    :cond_6
    :goto_2
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTrivialMovingEn:Z

    .line 593
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 596
    .end local v5    # "i":I
    .end local v6    # "minTextHeight":I
    .end local v7    # "minTextWidth":I
    :cond_7
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    .line 598
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 600
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->invalidate()V

    .line 602
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 526
    .restart local v5    # "i":I
    .restart local v6    # "minTextHeight":I
    .restart local v7    # "minTextWidth":I
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRectList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/RectF;

    .line 528
    .local v10, "rectf":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 529
    .local v8, "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxWidth()F

    move-result v2

    .line 530
    .local v2, "currMaxWidth":F
    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxHeight()F

    move-result v1

    .line 532
    .local v1, "currMaxHeight":F
    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v11

    int-to-float v12, v7

    cmpg-float v11, v11, v12

    if-gez v11, :cond_a

    .line 533
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v12, 0x8

    if-eq v11, v12, :cond_9

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x3

    if-eq v11, v12, :cond_9

    .line 534
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x5

    if-ne v11, v12, :cond_11

    .line 535
    :cond_9
    iget v11, v10, Landroid/graphics/RectF;->left:F

    int-to-float v12, v7

    add-float/2addr v11, v12

    iput v11, v10, Landroid/graphics/RectF;->right:F

    .line 542
    :cond_a
    :goto_3
    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v11

    cmpl-float v11, v11, v2

    if-lez v11, :cond_c

    .line 543
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v12, 0x8

    if-eq v11, v12, :cond_b

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x3

    if-eq v11, v12, :cond_b

    .line 544
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x5

    if-ne v11, v12, :cond_13

    .line 545
    :cond_b
    iget v11, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v11, v2

    iput v11, v10, Landroid/graphics/RectF;->right:F

    .line 552
    :cond_c
    :goto_4
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v11

    int-to-float v12, v6

    cmpg-float v11, v11, v12

    if-gez v11, :cond_e

    .line 553
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v12, 0x8

    if-eq v11, v12, :cond_d

    .line 554
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x6

    if-eq v11, v12, :cond_d

    .line 555
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x7

    if-ne v11, v12, :cond_15

    .line 556
    :cond_d
    iget v11, v10, Landroid/graphics/RectF;->top:F

    int-to-float v12, v6

    add-float/2addr v11, v12

    iput v11, v10, Landroid/graphics/RectF;->bottom:F

    .line 563
    :cond_e
    :goto_5
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v11

    cmpl-float v11, v11, v1

    if-lez v11, :cond_10

    .line 564
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v12, 0x8

    if-eq v11, v12, :cond_f

    .line 565
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x6

    if-eq v11, v12, :cond_f

    .line 566
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x7

    if-ne v11, v12, :cond_17

    .line 567
    :cond_f
    iget v11, v10, Landroid/graphics/RectF;->top:F

    add-float/2addr v11, v1

    iput v11, v10, Landroid/graphics/RectF;->bottom:F

    .line 574
    :cond_10
    :goto_6
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v0

    .line 575
    .local v0, "boundBox":Landroid/graphics/RectF;
    if-nez v0, :cond_19

    .line 576
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 536
    .end local v0    # "boundBox":Landroid/graphics/RectF;
    :cond_11
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x6

    if-eq v11, v12, :cond_12

    .line 537
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x1

    if-eq v11, v12, :cond_12

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x4

    if-ne v11, v12, :cond_a

    .line 538
    :cond_12
    iget v11, v10, Landroid/graphics/RectF;->right:F

    int-to-float v12, v7

    sub-float/2addr v11, v12

    iput v11, v10, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 546
    :cond_13
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x6

    if-eq v11, v12, :cond_14

    .line 547
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x1

    if-eq v11, v12, :cond_14

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x4

    if-ne v11, v12, :cond_c

    .line 548
    :cond_14
    iget v11, v10, Landroid/graphics/RectF;->right:F

    sub-float/2addr v11, v2

    iput v11, v10, Landroid/graphics/RectF;->left:F

    goto/16 :goto_4

    .line 557
    :cond_15
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x1

    if-eq v11, v12, :cond_16

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x2

    if-eq v11, v12, :cond_16

    .line 558
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_e

    .line 559
    :cond_16
    iget v11, v10, Landroid/graphics/RectF;->bottom:F

    int-to-float v12, v6

    sub-float/2addr v11, v12

    iput v11, v10, Landroid/graphics/RectF;->top:F

    goto :goto_5

    .line 568
    :cond_17
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x1

    if-eq v11, v12, :cond_18

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x2

    if-eq v11, v12, :cond_18

    .line 569
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_10

    .line 570
    :cond_18
    iget v11, v10, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v11, v1

    iput v11, v10, Landroid/graphics/RectF;->top:F

    goto :goto_6

    .line 579
    .restart local v0    # "boundBox":Landroid/graphics/RectF;
    :cond_19
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v11

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    move-result v12

    sub-float/2addr v11, v12

    float-to-int v3, v11

    .line 580
    .local v3, "deltaX":I
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v11

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    move-result v12

    sub-float/2addr v11, v12

    float-to-int v4, v11

    .line 581
    .local v4, "deltaY":I
    int-to-float v11, v3

    int-to-float v12, v4

    invoke-virtual {v10, v11, v12}, Landroid/graphics/RectF;->offset(FF)V

    .line 525
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 588
    .end local v0    # "boundBox":Landroid/graphics/RectF;
    .end local v1    # "currMaxHeight":F
    .end local v2    # "currMaxWidth":F
    .end local v3    # "deltaX":I
    .end local v4    # "deltaY":I
    .end local v8    # "obj":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v10    # "rectf":Landroid/graphics/RectF;
    :cond_1a
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v11, :cond_6

    .line 589
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto/16 :goto_2

    .line 501
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public hideSoftInput()V
    .locals 1

    .prologue
    .line 342
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    :goto_0
    return-void

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    .prologue
    .line 1241
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1242
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuVisible()Z

    move-result v0

    .line 1245
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isContextMenuVisible()Z

    move-result v0

    goto :goto_0
.end method

.method public isEditable()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    return v0
.end method

.method public isSelectByKey()Z
    .locals 1

    .prologue
    .line 1255
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isSelectByKey()Z

    move-result v0

    return v0
.end method

.method public isTextEraserEnabled()Z
    .locals 1

    .prologue
    .line 1168
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 206
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    .line 208
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 209
    .local v2, "rect":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 210
    .local v1, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->verifyRect(Landroid/graphics/RectF;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 211
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v3

    if-nez v3, :cond_0

    .line 244
    :goto_0
    return-void

    .line 215
    :cond_0
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v7

    invoke-direct {v4, v5, v3, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 216
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 217
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setEraserMode(Z)V

    .line 218
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBoxListener(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;)V

    .line 219
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v4

    invoke-virtual {v3, v4, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 220
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setMargin(IIII)V

    .line 221
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setShowSoftInputEnable(Z)V

    .line 222
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    .line 223
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    .line 225
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextLimitCount:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 226
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextLimitCount:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextLimit(I)V

    .line 229
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "desctription":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 231
    const-string v0, ""

    .line 233
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TextBox "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 234
    const/16 v3, 0x80

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->sendAccessibilityEvent(I)V

    .line 236
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v3, :cond_4

    .line 237
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 243
    .end local v0    # "desctription":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onAttachedToWindow()V

    goto/16 :goto_0

    .line 239
    .restart local v0    # "desctription":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->bringToFront()V

    goto :goto_1
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 396
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    .line 398
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUpdateHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 401
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 412
    :goto_0
    return-void

    .line 405
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 406
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    .line 407
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->seFitOnSizeChanged()V

    .line 408
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 411
    :cond_2
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/RectF;
    .param p3, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    const/high16 v4, 0x40400000    # 3.0f

    .line 355
    iget v1, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v4

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    iput v1, p2, Landroid/graphics/RectF;->left:F

    .line 356
    iget v1, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v4

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    iput v1, p2, Landroid/graphics/RectF;->top:F

    .line 357
    iget v1, p2, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v4

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    iput v1, p2, Landroid/graphics/RectF;->right:F

    .line 358
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v4

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    iput v1, p2, Landroid/graphics/RectF;->bottom:F

    .line 360
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getStyle()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 361
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v0

    .line 363
    .local v0, "tmpPaint":Landroid/graphics/Paint;
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SM-N750"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 364
    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 367
    :cond_0
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 371
    .end local v0    # "tmpPaint":Landroid/graphics/Paint;
    :goto_0
    return-void

    .line 369
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method protected onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 379
    iget v0, p2, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v0, -0x3

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 380
    iget v0, p2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v0, v0, -0x3

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p2, Landroid/graphics/Rect;->top:I

    .line 381
    iget v0, p2, Landroid/graphics/Rect;->right:I

    add-int/lit8 v0, v0, 0x3

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 382
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, 0x3

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 384
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 385
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 420
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onSizeChanged(IIII)V

    .line 421
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    .line 422
    .local v0, "coordinateInfo":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 424
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    .line 426
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 427
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextMenuLocation()V

    .line 429
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 618
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-nez v2, :cond_0

    move v0, v1

    .line 680
    :goto_0
    return v0

    .line 620
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isTouchEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 621
    const/4 v0, 0x0

    goto :goto_0

    .line 624
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v1, :cond_3

    .line 625
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_2

    .line 626
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    .line 628
    :cond_2
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    move v0, v1

    .line 629
    goto :goto_0

    .line 632
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_4

    .line 633
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    .line 636
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->check(Landroid/view/MotionEvent;)V

    .line 638
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    if-nez v2, :cond_5

    .line 639
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 642
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v2, :cond_7

    .line 643
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 644
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_6

    .line 645
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->close()V

    :cond_6
    move v0, v1

    .line 647
    goto :goto_0

    .line 650
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 665
    :goto_1
    const/4 v0, 0x0

    .line 666
    .local v0, "ret":Z
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 667
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v2, :cond_8

    .line 668
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 670
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    if-ne v2, v1, :cond_9

    .line 671
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    .line 675
    :cond_9
    const/4 v0, 0x1

    .line 678
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 652
    .end local v0    # "ret":Z
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_b

    .line 653
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 655
    :cond_b
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v2, :cond_c

    .line 656
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideTextBox()V

    .line 658
    :cond_c
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->initHandleView()V

    goto :goto_1

    .line 661
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->hideHandleView()V

    goto :goto_1

    .line 650
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onVisibleUpdated(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onVisibleUpdated(Ljava/util/ArrayList;Z)V

    .line 158
    :cond_0
    return-void
.end method

.method protected onZoom()V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    .line 179
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onZoom()V

    .line 180
    return-void
.end method

.method public removeText()V
    .locals 1

    .prologue
    .line 747
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 752
    :goto_0
    return-void

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    goto :goto_0
.end method

.method public selectAllText()V
    .locals 1

    .prologue
    .line 965
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 972
    :goto_0
    return-void

    .line 969
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkInvalidStateEditable()V

    .line 971
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelectionAll()V

    goto :goto_0
.end method

.method protected setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;)V
    .locals 0
    .param p1, "actionListener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    .line 170
    return-void
.end method

.method protected setCheckCursorOnScroll(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 688
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 691
    :cond_0
    return-void
.end method

.method public setContextMenuVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1221
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setContextMenuVisible(Z)V

    .line 1222
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextMenuLocation()V

    .line 1226
    :goto_0
    return-void

    .line 1224
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    goto :goto_0
.end method

.method public setEditable(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 260
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    if-eqz v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eq v0, p1, :cond_0

    .line 268
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    .line 269
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v0, :cond_3

    .line 270
    invoke-super {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_2

    .line 273
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setShowSoftInputEnable(Z)V

    .line 275
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    .line 276
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    .line 278
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    .line 296
    :goto_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v0, :cond_0

    .line 297
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->bringToFront()V

    goto :goto_0

    .line 280
    :cond_3
    invoke-super {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    .line 282
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onVisibleUpdated(Z)V

    .line 284
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_4

    .line 285
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    .line 286
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    .line 287
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    .line 289
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    .line 290
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideTextBox()V

    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    .line 293
    :cond_4
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    goto :goto_1
.end method

.method public setMargin(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 769
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    .line 770
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    .line 771
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    .line 772
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    .line 774
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 779
    :goto_0
    return-void

    .line 778
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setMargin(IIII)V

    goto :goto_0
.end method

.method public setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 1
    .param p1, "ObjectTextBox"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 466
    if-nez p1, :cond_0

    .line 473
    :goto_0
    return-void

    .line 469
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 470
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObjectList(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public setSelection(II)V
    .locals 5
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x1

    .line 932
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 954
    :goto_0
    return-void

    .line 936
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkInvalidStateEditable()V

    .line 938
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    .line 939
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_1

    if-le p1, p2, :cond_2

    .line 940
    :cond_1
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 944
    :cond_2
    if-ltz p1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le p2, v1, :cond_4

    .line 945
    :cond_3
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 949
    :cond_4
    if-ne p1, p2, :cond_5

    .line 950
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, p1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_0

    .line 952
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, p1, p2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_0
.end method

.method protected setShowSoftInputEnable(Z)V
    .locals 0
    .param p1, "isShowSoftInputEnable"    # Z

    .prologue
    .line 306
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsShowSoftInputEnable:Z

    .line 307
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 793
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 803
    :goto_0
    return-void

    .line 797
    :cond_0
    if-nez p1, :cond_1

    .line 798
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 802
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTextCursorPosition(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x7

    .line 835
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 853
    :goto_0
    return-void

    .line 839
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkInvalidStateEditable()V

    .line 841
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    .line 842
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 843
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 847
    :cond_1
    if-ltz p1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le p1, v1, :cond_3

    .line 848
    :cond_2
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 852
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto :goto_0
.end method

.method public setTextEraserEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1152
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    .line 1153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    .line 1154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setEraserMode(Z)V

    .line 1156
    :cond_0
    return-void
.end method

.method public setTextLimit(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 887
    const/4 v0, -0x1

    if-ge p1, v0, :cond_0

    .line 888
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 893
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 898
    :goto_1
    return-void

    .line 890
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextLimitCount:I

    goto :goto_0

    .line 897
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextLimit(I)V

    goto :goto_1
.end method

.method public setTextSelectionContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 1
    .param p1, "contextMenu"    # Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .prologue
    .line 983
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    .line 984
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    .line 986
    :cond_0
    return-void
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 11
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 997
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1074
    :cond_0
    :goto_0
    return-void

    .line 1001
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkSettingTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 1003
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-eq v6, v7, :cond_4

    .line 1004
    const/4 v5, 0x0

    .line 1005
    .local v5, "type":I
    const/4 v2, 0x0

    .line 1007
    .local v2, "option":Z
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-le v6, v7, :cond_c

    .line 1008
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    sub-int v5, v6, v7

    .line 1009
    const/4 v2, 0x0

    .line 1015
    :goto_1
    and-int/lit8 v6, v5, 0x1

    if-ne v6, v8, :cond_2

    .line 1016
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    .line 1018
    :cond_2
    and-int/lit8 v6, v5, 0x2

    if-ne v6, v9, :cond_3

    .line 1019
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v9, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    .line 1021
    :cond_3
    and-int/lit8 v6, v5, 0x4

    if-ne v6, v10, :cond_4

    .line 1022
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v10, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    .line 1025
    .end local v2    # "option":Z
    .end local v5    # "type":I
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    if-eq v6, v7, :cond_5

    .line 1026
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    .line 1027
    .local v4, "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v6, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    .line 1029
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 1031
    .end local v4    # "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_6

    .line 1032
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    .line 1033
    .local v4, "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v6, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    .line 1035
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 1037
    .end local v4    # "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_7

    .line 1038
    new-instance v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    .line 1039
    .local v4, "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;
    iget-object v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v6, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    .line 1041
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 1043
    .end local v4    # "txtSpan":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;
    :cond_7
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    if-eq v6, v7, :cond_8

    .line 1044
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;-><init>()V

    .line 1045
    .local v1, "directionInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    int-to-char v6, v6

    iput v6, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    .line 1047
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    .line 1049
    .end local v1    # "directionInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;
    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-eq v6, v7, :cond_9

    .line 1050
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    .line 1051
    .local v0, "alignInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    int-to-char v6, v6

    iput v6, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    .line 1052
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V

    .line 1054
    .end local v0    # "alignInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_a

    .line 1055
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    if-eq v6, v7, :cond_b

    .line 1056
    :cond_a
    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    .line 1057
    .local v3, "spacingInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iput v6, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    .line 1058
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v6, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    .line 1059
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V

    .line 1062
    .end local v3    # "spacingInfo":Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 1063
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 1064
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 1065
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 1066
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 1067
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 1068
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v7, v6, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 1070
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v6, :cond_0

    .line 1071
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    .line 1072
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    goto/16 :goto_0

    .line 1011
    .restart local v2    # "option":Z
    .restart local v5    # "type":I
    :cond_c
    iget v6, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    sub-int v5, v6, v7

    .line 1012
    const/4 v2, 0x1

    goto/16 :goto_1
.end method

.method public setTouchEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTouch(Z)V

    .line 198
    :cond_0
    return-void
.end method

.method public showSoftInput()V
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    goto :goto_0
.end method
