.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;
.super Ljava/lang/Object;
.source "SpenTextBox.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mCursorPosChanged:Z

.field private final mDownPoint:Landroid/graphics/PointF;

.field private mLastCursorPos:I

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 1603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1604
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mDownPoint:Landroid/graphics/PointF;

    .line 1605
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mLastCursorPos:I

    .line 1606
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mCursorPosChanged:Z

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1610
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v9

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getAbsolutePoint(FF)[F
    invoke-static {v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;FF)[F

    move-result-object v6

    .line 1611
    .local v6, "pts":[F
    if-nez v6, :cond_0

    .line 1613
    const/4 v8, 0x1

    .line 1693
    :goto_0
    return v8

    .line 1616
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 1618
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 1693
    :cond_1
    :goto_1
    const/4 v8, 0x1

    goto :goto_0

    .line 1620
    :pswitch_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 1623
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v5

    .line 1624
    .local v5, "pos":I
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mLastCursorPos:I

    .line 1625
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;
    invoke-static {v8, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)Landroid/graphics/Rect;

    move-result-object v7

    .line 1626
    .local v7, "temp":Landroid/graphics/Rect;
    if-eqz v7, :cond_1

    .line 1631
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v7}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 1632
    .local v0, "cursorRect":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mDownPoint:Landroid/graphics/PointF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    const/4 v10, 0x0

    aget v10, v6, v10

    sub-float/2addr v9, v10

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v10

    const/4 v11, 0x1

    aget v11, v6, v11

    sub-float/2addr v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/graphics/PointF;->set(FF)V

    .line 1633
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    .line 1634
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 1636
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 1638
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mCursorPosChanged:Z

    goto :goto_1

    .line 1643
    .end local v0    # "cursorRect":Landroid/graphics/RectF;
    .end local v5    # "pos":I
    .end local v7    # "temp":Landroid/graphics/Rect;
    :pswitch_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mDownPoint:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    const/4 v9, 0x0

    aget v9, v6, v9

    add-float v3, v8, v9

    .line 1644
    .local v3, "newX":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mDownPoint:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    const/4 v9, 0x1

    aget v9, v6, v9

    add-float v4, v8, v9

    .line 1646
    .local v4, "newY":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I
    invoke-static {v8, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I

    move-result v2

    .line 1647
    .local v2, "line":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I
    invoke-static {v8, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I

    move-result v1

    .line 1649
    .local v1, "index":I
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mLastCursorPos:I

    if-eq v8, v1, :cond_1

    .line 1653
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->length()I

    move-result v8

    if-ge v8, v1, :cond_3

    .line 1654
    :cond_2
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1657
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v8

    invoke-static {v8, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 1659
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 1662
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 1663
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 1665
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mDownPoint:Landroid/graphics/PointF;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mDownPoint:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mDownPoint:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F
    invoke-static {v11, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)F

    move-result v11

    add-float/2addr v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/graphics/PointF;->set(FF)V

    .line 1667
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mLastCursorPos:I

    .line 1668
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mCursorPosChanged:Z

    .line 1670
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawHandle()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 1671
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto/16 :goto_1

    .line 1678
    .end local v1    # "index":I
    .end local v2    # "line":I
    .end local v3    # "newX":F
    .end local v4    # "newY":F
    :pswitch_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 1682
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->mCursorPosChanged:Z

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 1683
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    .line 1685
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 1687
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    .line 1689
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    goto/16 :goto_1

    .line 1618
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
