.class public Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
.super Ljava/lang/Object;
.source "SpenSettingTextInfo.java"


# instance fields
.field public align:I

.field public bgColor:I

.field public color:I

.field public direction:I

.field public font:Ljava/lang/String;

.field public lineIndent:I

.field public lineSpacing:F

.field public lineSpacingType:I

.field public size:F

.field public style:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 22
    iput v1, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    .line 28
    iput v1, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 34
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 40
    const-string v0, "Roboto-Regular"

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 46
    iput v1, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 52
    iput v1, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    .line 64
    const v0, 0x3fa66666    # 1.3f

    iput v0, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 70
    iput v1, p0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 10
    return-void
.end method
