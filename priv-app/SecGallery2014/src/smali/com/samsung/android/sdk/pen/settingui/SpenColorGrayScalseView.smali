.class Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;
.super Landroid/widget/ImageView;
.source "SpenColorGrayScalseView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;
    }
.end annotation


# instance fields
.field private mBorderPaint:Landroid/graphics/Paint;

.field private mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

.field private mCurY:I

.field private mCurrentColor:I

.field private mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mCursorRect:Landroid/graphics/Rect;

.field private mCustom_imagepath:Ljava/lang/String;

.field mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mHeight:I

.field private mIsCursorVisible:Z

.field private mRatio:F

.field private mSpectrum:Landroid/graphics/Bitmap;

.field private mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 2
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "custom_imagepath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCustom_imagepath:Ljava/lang/String;

    .line 31
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mIsCursorVisible:Z

    .line 32
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurrentColor:I

    .line 34
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mRatio:F

    .line 46
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCustom_imagepath:Ljava/lang/String;

    .line 47
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCustom_imagepath:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 48
    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mRatio:F

    .line 49
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->initView()V

    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40d00000    # 6.5f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    .line 51
    return-void
.end method

.method private initView()V
    .locals 7

    .prologue
    const/16 v2, 0xa

    const/4 v6, 0x1

    const/high16 v5, 0x40d00000    # 6.5f

    .line 231
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_select_kit"

    invoke-virtual {v0, v1, v2, v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_box_grayscale"

    const/16 v2, 0xc2

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    .line 238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mWidth:I

    .line 239
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mHeight:I

    .line 240
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mWidth:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mHeight:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mWidth:I

    div-int/lit8 v3, v3, 0x2

    .line 241
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mHeight:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 240
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorRect:Landroid/graphics/Rect;

    .line 242
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 243
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mBorderPaint:Landroid/graphics/Paint;

    .line 244
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mBorderPaint:Landroid/graphics/Paint;

    const v1, -0x737374

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 245
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mBorderPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 246
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 247
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 57
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 63
    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_4

    .line 68
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 71
    :cond_3
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 74
    :cond_4
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorRect:Landroid/graphics/Rect;

    .line 79
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mBorderPaint:Landroid/graphics/Paint;

    .line 81
    return-void
.end method

.method protected getGrayCursoVisibility()Z
    .locals 1

    .prologue
    .line 255
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mIsCursorVisible:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 155
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 156
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mIsCursorVisible:Z

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mBorderPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 161
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 189
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 191
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_2

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_box_grayscale"

    const/16 v2, 0xc2

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 198
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    .line 200
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 202
    :cond_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/high16 v12, -0x2000000

    const/high16 v8, 0x40000000    # 2.0f

    const v11, 0xffffff

    const/4 v10, 0x1

    .line 92
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 93
    .local v1, "i":I
    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    .line 94
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->playSoundEffect(I)V

    .line 95
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    if-eqz v4, :cond_0

    .line 96
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurrentColor:I

    invoke-interface {v4, v10, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;->onActionUp(ZI)V

    .line 142
    :goto_0
    return v10

    .line 100
    :cond_0
    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4, v10}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 103
    :cond_1
    if-ne v1, v10, :cond_2

    .line 104
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->playSoundEffect(I)V

    .line 105
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    if-eqz v4, :cond_2

    .line 106
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurrentColor:I

    invoke-interface {v4, v10, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;->onActionUp(ZI)V

    .line 110
    :cond_2
    const/4 v0, -0x1

    .line 112
    .local v0, "color":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 114
    .local v2, "mCurX":I
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mRatio:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 116
    .local v3, "minValue":I
    if-gt v2, v3, :cond_3

    .line 117
    add-int/lit8 v2, v3, 0x1

    .line 119
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_6

    .line 120
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int v5, v2, v3

    if-gt v4, v5, :cond_4

    .line 121
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mRatio:F

    mul-float/2addr v5, v8

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    sub-int v2, v4, v5

    .line 123
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    add-int/2addr v5, v3

    if-gt v4, v5, :cond_5

    .line 124
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mRatio:F

    mul-float/2addr v5, v8

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    .line 126
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    invoke-virtual {v4, v2, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    .line 129
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorRect:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mWidth:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v2, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mWidth:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v2

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mHeight:I

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 131
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 133
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    if-eqz v4, :cond_8

    .line 134
    and-int v4, v0, v11

    if-ne v4, v11, :cond_7

    .line 135
    const v0, 0xfefefe

    .line 137
    :cond_7
    and-int v4, v11, v0

    or-int/2addr v4, v12

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurrentColor:I

    .line 138
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    and-int v5, v11, v0

    or-int/2addr v5, v12

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    invoke-interface {v4, v5, v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;->onColorChanged(III)V

    .line 139
    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mIsCursorVisible:Z

    .line 141
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->invalidate()V

    goto/16 :goto_0
.end method

.method public selectColorForGrayScale(ID)V
    .locals 9
    .param p1, "color"    # I
    .param p2, "deltaColor"    # D

    .prologue
    const/4 v8, 0x1

    .line 205
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 206
    :cond_0
    const-string v2, "SpenColorGrayScale"

    const-string v3, "NullPointerExeption! Check resouce again"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :goto_0
    return-void

    .line 210
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_2

    .line 211
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 214
    :cond_2
    const/4 v2, 0x3

    new-array v0, v2, [F

    .line 215
    .local v0, "hsv":[F
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 218
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x2

    aget v3, v0, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 219
    .local v1, "mPosY":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mWidth:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mWidth:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v1

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCurY:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 220
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 221
    aget v2, v0, v8

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    .line 222
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mIsCursorVisible:Z

    .line 226
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->invalidate()V

    goto :goto_0

    .line 224
    :cond_3
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mIsCursorVisible:Z

    goto :goto_1
.end method

.method public setColorGrayScaleColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;)V
    .locals 0
    .param p1, "parama"    # Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    .line 172
    return-void
.end method

.method protected setGrayCursorVisibility(Z)V
    .locals 0
    .param p1, "isVisible"    # Z

    .prologue
    .line 250
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->mIsCursorVisible:Z

    .line 251
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->invalidate()V

    .line 252
    return-void
.end method
