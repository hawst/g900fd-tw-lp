.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;
.super Ljava/lang/Object;
.source "SpenTextBox.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mDownPoint:Landroid/graphics/PointF;

.field private mLastStartIndex:I

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 1518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mDownPoint:Landroid/graphics/PointF;

    .line 1520
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mLastStartIndex:I

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1524
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v12

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getAbsolutePoint(FF)[F
    invoke-static {v10, v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;FF)[F

    move-result-object v6

    .line 1525
    .local v6, "pts":[F
    if-nez v6, :cond_0

    .line 1527
    const/4 v10, 0x1

    .line 1599
    :goto_0
    return v10

    .line 1530
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v10

    invoke-static {v10}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v9

    .line 1531
    .local v9, "start":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v10

    invoke-static {v10}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1533
    .local v1, "end":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 1599
    :cond_1
    :goto_1
    const/4 v10, 0x1

    goto :goto_0

    .line 1535
    :pswitch_0
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mLastStartIndex:I

    .line 1536
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;
    invoke-static {v10, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)Landroid/graphics/Rect;

    move-result-object v0

    .line 1537
    .local v0, "cursorRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_1

    .line 1541
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1542
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 1544
    :cond_2
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mDownPoint:Landroid/graphics/PointF;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v11

    int-to-float v11, v11

    const/4 v12, 0x0

    aget v12, v6, v12

    sub-float/2addr v11, v12

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v12

    int-to-float v12, v12

    const/4 v13, 0x1

    aget v13, v6, v13

    sub-float/2addr v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/graphics/PointF;->set(FF)V

    .line 1545
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    .line 1546
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 1547
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    goto :goto_1

    .line 1551
    .end local v0    # "cursorRect":Landroid/graphics/Rect;
    :pswitch_1
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mDownPoint:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    const/4 v11, 0x0

    aget v11, v6, v11

    add-float v4, v10, v11

    .line 1552
    .local v4, "newX":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mDownPoint:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    const/4 v11, 0x1

    aget v11, v6, v11

    add-float v5, v10, v11

    .line 1554
    .local v5, "newY":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I
    invoke-static {v10, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I

    move-result v3

    .line 1555
    .local v3, "line":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I
    invoke-static {v10, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I

    move-result v2

    .line 1557
    .local v2, "index":I
    if-eq v1, v2, :cond_1

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mLastStartIndex:I

    if-eq v10, v2, :cond_1

    .line 1559
    :try_start_0
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v10

    const/16 v11, 0x16

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->performHapticFeedback(I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1566
    :goto_2
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10}, Landroid/text/Editable;->length()I

    move-result v11

    if-ge v2, v1, :cond_4

    move v10, v1

    :goto_3
    if-ge v11, v10, :cond_5

    .line 1567
    :cond_3
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 1560
    :catch_0
    move-exception v7

    .line 1561
    .local v7, "se":Ljava/lang/SecurityException;
    const-string v10, "SpenTextBox"

    const-string v11, "haptic is disabled in this model"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .end local v7    # "se":Ljava/lang/SecurityException;
    :cond_4
    move v10, v2

    .line 1566
    goto :goto_3

    .line 1570
    :cond_5
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v10

    invoke-static {v10, v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 1571
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 1575
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v10

    invoke-virtual {v10, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 1576
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 1578
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mLastStartIndex:I

    .line 1579
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mDownPoint:Landroid/graphics/PointF;

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mDownPoint:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->mDownPoint:Landroid/graphics/PointF;

    iget v12, v12, Landroid/graphics/PointF;->y:F

    iget-object v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F
    invoke-static {v13, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)F

    move-result v13

    add-float/2addr v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/graphics/PointF;->set(FF)V

    .line 1580
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawHandle()V
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    goto/16 :goto_1

    .line 1585
    .end local v2    # "index":I
    .end local v3    # "line":I
    .end local v4    # "newX":F
    .end local v5    # "newY":F
    :pswitch_2
    if-le v9, v1, :cond_6

    .line 1586
    move v8, v9

    .line 1587
    .local v8, "st":I
    move v9, v1

    .line 1588
    move v1, v8

    .line 1590
    .end local v8    # "st":I
    :cond_6
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v11, 0x0

    invoke-virtual {v10, v9, v1, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 1591
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 1592
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    .line 1594
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    goto/16 :goto_1

    .line 1533
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
