.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    .line 745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 749
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 800
    :cond_2
    :goto_0
    return-void

    .line 755
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->calculatePopupPosition()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    .line 756
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 757
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const/high16 v3, 0x432a0000    # 170.0f

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)F

    move-result v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;-><init>(Landroid/view/View;Ljava/util/ArrayList;IIF)V

    .line 756
    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$53(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)V

    .line 759
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->setOnItemSelectListner(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;)V

    .line 799
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$52(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->show(IILjava/lang/String;)V

    goto :goto_0
.end method
