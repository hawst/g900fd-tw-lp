.class public Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;
.super Ljava/lang/Object;
.source "SpenScreenCodecDecoder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decode_file(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 118
    .local v0, "resultBitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public static decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 130
    const/4 v4, 0x0

    .line 131
    .local v4, "resultBitmap":Landroid/graphics/Bitmap;
    if-nez p0, :cond_0

    .line 132
    const-string v6, "SpenScreenCodecDecoder"

    const-string/jumbo v7, "stream is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v4

    .line 165
    .end local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    .local v5, "resultBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v5

    .line 136
    .end local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    :cond_0
    const/4 v0, 0x0

    .line 138
    .local v0, "buffer":[B
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v3

    .line 139
    .local v3, "length":I
    new-array v0, v3, [B

    .line 140
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-eq v6, v3, :cond_1

    .line 141
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 142
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Failed to read stream"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .end local v3    # "length":I
    :catch_0
    move-exception v1

    .line 146
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v5, v4

    .line 150
    .end local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 147
    .end local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v2

    .local v2, "e1":Ljava/io/IOException;
    move-object v5, v4

    .line 148
    .end local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 153
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "length":I
    .restart local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 159
    const/4 v6, 0x4

    aget-byte v6, v0, v6

    const/16 v7, -0x56

    if-ne v6, v7, :cond_2

    const/4 v6, 0x5

    aget-byte v6, v0, v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 160
    array-length v6, v0

    invoke-static {v0, v6}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decode_stream([BI)Landroid/graphics/Bitmap;

    move-result-object v4

    :goto_1
    move-object v5, v4

    .line 165
    .end local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 154
    .end local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    :catch_2
    move-exception v1

    .restart local v1    # "e":Ljava/io/IOException;
    move-object v5, v4

    .line 155
    .end local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 162
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "resultBitmap":Landroid/graphics/Bitmap;
    :cond_2
    const/4 v6, 0x0

    array-length v7, v0

    invoke-static {v0, v6, v7}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_1
.end method

.method public static native decode_file(Ljava/lang/String;)Landroid/graphics/Bitmap;
.end method

.method public static native decode_stream([BI)Landroid/graphics/Bitmap;
.end method

.method public static getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 13
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "id"    # I

    .prologue
    const/4 v11, 0x6

    const/4 v9, 0x0

    .line 30
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v6

    .line 31
    .local v6, "stm":Ljava/io/InputStream;
    if-nez v6, :cond_0

    .line 32
    const-string v10, "SpenScreenCodecDecoder"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Can\'t open raw resource. id = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v9

    .line 104
    :goto_0
    return-object v2

    .line 35
    :cond_0
    const/4 v8, 0x0

    .line 37
    .local v8, "temp":[B
    const/4 v10, 0x6

    :try_start_0
    new-array v8, v10, [B

    .line 38
    invoke-virtual {v6, v8}, Ljava/io/InputStream;->read([B)I

    move-result v10

    if-eq v10, v11, :cond_1

    .line 39
    const-string v10, "SpenScreenCodecDecoder"

    const-string v11, "Failed to read stream 1."

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    move-object v2, v9

    .line 41
    goto :goto_0

    .line 43
    :cond_1
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v7

    .line 55
    .local v7, "stream":Ljava/io/InputStream;
    const/4 v10, 0x4

    aget-byte v10, v8, v10

    const/16 v11, -0x56

    if-ne v10, v11, :cond_4

    const/4 v10, 0x5

    aget-byte v10, v8, v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    .line 56
    const/4 v1, 0x0

    .line 59
    .local v1, "buffer":[B
    :try_start_1
    invoke-virtual {v7}, Ljava/io/InputStream;->available()I

    move-result v5

    .line 60
    .local v5, "length":I
    new-array v1, v5, [B

    .line 61
    invoke-virtual {v7, v1}, Ljava/io/InputStream;->read([B)I

    move-result v10

    if-eq v10, v5, :cond_2

    .line 62
    const-string v10, "SpenScreenCodecDecoder"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Failed to read stream. length = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v2, v9

    .line 64
    goto :goto_0

    .line 44
    .end local v1    # "buffer":[B
    .end local v5    # "length":I
    .end local v7    # "stream":Ljava/io/InputStream;
    :catch_0
    move-exception v3

    .line 46
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    move-object v2, v9

    .line 50
    goto :goto_0

    .line 47
    :catch_1
    move-exception v4

    .line 48
    .local v4, "e1":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 67
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "e1":Ljava/io/IOException;
    .restart local v1    # "buffer":[B
    .restart local v5    # "length":I
    .restart local v7    # "stream":Ljava/io/InputStream;
    :cond_2
    :try_start_3
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 77
    array-length v10, v1

    invoke-static {v1, v10}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decode_stream([BI)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 78
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_3

    .line 79
    const-string v10, "SpenScreenCodecDecoder"

    const-string v11, "Failed to create the bitmap"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v9

    .line 80
    goto :goto_0

    .line 68
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "length":I
    :catch_2
    move-exception v3

    .line 70
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_2
    move-object v2, v9

    .line 74
    goto :goto_0

    .line 71
    :catch_3
    move-exception v4

    .line 72
    .restart local v4    # "e1":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 83
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "e1":Ljava/io/IOException;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "length":I
    :cond_3
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, p0, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 86
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "buffer":[B
    .end local v5    # "length":I
    :cond_4
    :try_start_5
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 87
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_5

    .line 88
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 98
    :try_start_6
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_0

    .line 99
    :catch_4
    move-exception v3

    .line 100
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 91
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 93
    .local v3, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_7
    invoke-virtual {v3}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 98
    :try_start_8
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 103
    .end local v3    # "e":Landroid/content/res/Resources$NotFoundException;
    :goto_3
    const-string v10, "SpenScreenCodecDecoder"

    const-string v11, "fail to getDrawable."

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v9

    .line 104
    goto/16 :goto_0

    .line 99
    .restart local v3    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_6
    move-exception v3

    .line 100
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 94
    .end local v3    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v3

    .line 95
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 98
    :try_start_a
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    goto :goto_3

    .line 99
    :catch_8
    move-exception v3

    .line 100
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 96
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 98
    :try_start_b
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 102
    :goto_4
    throw v9

    .line 99
    :catch_9
    move-exception v3

    .line 100
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 98
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    :try_start_c
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a

    goto :goto_3

    .line 99
    :catch_a
    move-exception v3

    .line 100
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method
