.class Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;
.super Ljava/lang/Object;
.source "SpenInView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SPenHoverPointerIconWrapper"
.end annotation


# static fields
.field private static mIconType:I


# instance fields
.field private mKlass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private mRemoveHoveringSpenCustomIcon:Ljava/lang/reflect/Method;

.field private mSetHoveringSpenIcon:Ljava/lang/reflect/Method;

.field private mSetHoveringSpenIcon2:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6220
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mIconType:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 6222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6224
    :try_start_0
    const-string v1, "android.view.PointerIcon"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mKlass:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6228
    :goto_0
    return-void

    .line 6225
    :catch_0
    move-exception v0

    .line 6226
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getIconType()I
    .locals 1

    .prologue
    .line 6267
    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mIconType:I

    return v0
.end method


# virtual methods
.method public removeHoveringSpenCustomIcon(I)V
    .locals 6
    .param p1, "customIconId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/NoSuchMethodException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6257
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mRemoveHoveringSpenCustomIcon:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 6259
    new-array v0, v5, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v4

    .line 6260
    .local v0, "paramTypes":[Ljava/lang/Class;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mKlass:Ljava/lang/Class;

    const-string v3, "removeHoveringSpenCustomIcon"

    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mRemoveHoveringSpenCustomIcon:Ljava/lang/reflect/Method;

    .line 6262
    .end local v0    # "paramTypes":[Ljava/lang/Class;
    :cond_0
    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 6263
    .local v1, "parameters":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mRemoveHoveringSpenCustomIcon:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 6264
    return-void
.end method

.method public setHoveringSpenIcon(ILandroid/graphics/drawable/Drawable;Landroid/graphics/Point;)I
    .locals 9
    .param p1, "iconType"    # I
    .param p2, "d"    # Landroid/graphics/drawable/Drawable;
    .param p3, "p"    # Landroid/graphics/Point;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/NoSuchMethodException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 6232
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mSetHoveringSpenIcon:Ljava/lang/reflect/Method;

    if-nez v3, :cond_0

    .line 6234
    new-array v0, v8, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v5

    const-class v3, Landroid/graphics/drawable/Drawable;

    aput-object v3, v0, v6

    const-class v3, Landroid/graphics/Point;

    aput-object v3, v0, v7

    .line 6235
    .local v0, "paramTypes":[Ljava/lang/Class;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mKlass:Ljava/lang/Class;

    const-string v4, "setHoveringSpenIcon"

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mSetHoveringSpenIcon:Ljava/lang/reflect/Method;

    .line 6237
    .end local v0    # "paramTypes":[Ljava/lang/Class;
    :cond_0
    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v5

    aput-object p2, v1, v6

    aput-object p3, v1, v7

    .line 6238
    .local v1, "parameters":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mSetHoveringSpenIcon:Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 6239
    .local v2, "rt":Ljava/lang/Integer;
    sput p1, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mIconType:I

    .line 6240
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    return v3
.end method

.method public setHoveringSpenIcon(I)V
    .locals 7
    .param p1, "iconType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/NoSuchMethodException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6245
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mSetHoveringSpenIcon2:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 6247
    new-array v0, v6, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v4

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v5

    .line 6248
    .local v0, "paramTypes":[Ljava/lang/Class;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mKlass:Ljava/lang/Class;

    const-string v3, "setHoveringSpenIcon"

    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mSetHoveringSpenIcon2:Ljava/lang/reflect/Method;

    .line 6250
    .end local v0    # "paramTypes":[Ljava/lang/Class;
    :cond_0
    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    .line 6251
    .local v1, "parameters":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mSetHoveringSpenIcon2:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 6252
    sput p1, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->mIconType:I

    .line 6253
    return-void
.end method
