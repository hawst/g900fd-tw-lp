.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;
.super Ljava/lang/Object;
.source "SpenStrokeFrame.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete([B)V
    .locals 14
    .param p1, "arg0"    # [B

    .prologue
    const/high16 v13, 0x41200000    # 10.0f

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/16 v11, 0x20

    .line 838
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 839
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 840
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    .line 842
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 843
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 844
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$47(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    .line 846
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 847
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 848
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    .line 852
    :cond_2
    if-nez p1, :cond_3

    .line 853
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Camera onComplete arg0 is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    .line 934
    :goto_0
    return-void

    .line 858
    :cond_3
    array-length v8, p1

    invoke-static {p1, v12, v8, v9}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 859
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_4

    .line 860
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 861
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Camera onComplete Bitmap is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto :goto_0

    .line 865
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    .line 867
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createRotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 870
    .local v7, "rotateBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 871
    .local v0, "absoluteRect":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 872
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {v8, v0, v12}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 873
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 874
    .local v4, "relativePath":Landroid/graphics/Path;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v5

    .line 875
    .local v5, "relativeRect":Landroid/graphics/RectF;
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 876
    .local v3, "m":Landroid/graphics/Matrix;
    sget-object v8, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v0, v5, v8}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 877
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 879
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    invoke-static {v8, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 880
    .local v2, "fitBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_5

    .line 881
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 882
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Camera onComplete Bitmap is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 886
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    invoke-static {v8, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 887
    .local v6, "resultBitmap":Landroid/graphics/Bitmap;
    if-nez v6, :cond_6

    .line 888
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    .line 889
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 892
    :cond_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 893
    const/4 v2, 0x0

    .line 894
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v8

    invoke-virtual {v8, v0, v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setRect(Landroid/graphics/RectF;Z)V

    .line 895
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Landroid/graphics/Bitmap;)V

    .line 896
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 897
    const/4 v6, 0x0

    .line 900
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    .line 901
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {v8, v0, v12}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 902
    new-instance v4, Landroid/graphics/Path;

    .end local v4    # "relativePath":Landroid/graphics/Path;
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 903
    .restart local v4    # "relativePath":Landroid/graphics/Path;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v5

    .line 904
    new-instance v3, Landroid/graphics/Matrix;

    .end local v3    # "m":Landroid/graphics/Matrix;
    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 905
    .restart local v3    # "m":Landroid/graphics/Matrix;
    sget-object v8, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v0, v5, v8}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 906
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 907
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    invoke-static {v8, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 908
    if-nez v2, :cond_7

    .line 909
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Camera onComplete Bitmap is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 913
    :cond_7
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 914
    const/4 v1, 0x0

    .line 915
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 916
    const/4 v7, 0x0

    .line 917
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    invoke-static {v8, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 918
    if-nez v6, :cond_8

    .line 919
    const-string v8, "SpenStrokeFrame"

    const-string v9, "Beautify Stroke Frame ResultBitmap is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v8, v11}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    .line 923
    :cond_8
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 924
    const/4 v2, 0x0

    .line 925
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v8

    invoke-virtual {v8, v0, v12}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setRect(Landroid/graphics/RectF;Z)V

    .line 926
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Landroid/graphics/Bitmap;)V

    .line 927
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 928
    const/4 v6, 0x0

    .line 930
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v8

    invoke-virtual {v8, v13, v13}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setMinSize(FF)V

    .line 931
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v10

    invoke-interface {v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCompleted(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    .line 932
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/app/Activity;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOrientation:I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 933
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    goto/16 :goto_0
.end method
