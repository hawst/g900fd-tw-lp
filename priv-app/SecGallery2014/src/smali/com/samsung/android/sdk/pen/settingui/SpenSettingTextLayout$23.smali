.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    .line 3989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    .locals 1

    .prologue
    .line 3989
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    return-object v0
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 3995
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 3996
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    .line 3997
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$79(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3998
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$80(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 3999
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$81(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 4055
    :cond_0
    :goto_0
    return-void

    .line 4004
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$79(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$82(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 4005
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$81(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 4008
    :cond_2
    const/4 v5, 0x2

    new-array v3, v5, [I

    .line 4009
    .local v3, "location":[I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v5, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLocationOnScreen([I)V

    .line 4011
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I

    move-result-object v5

    const/4 v6, 0x0

    aget v5, v5, v6

    const/4 v6, 0x0

    aget v6, v3, v6

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I

    move-result-object v5

    const/4 v6, 0x1

    aget v5, v5, v6

    const/4 v6, 0x1

    aget v6, v3, v6

    if-eq v5, v6, :cond_4

    .line 4012
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 4013
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    aget v7, v3, v7

    aput v7, v5, v6

    .line 4014
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x1

    aget v7, v3, v7

    aput v7, v5, v6

    .line 4016
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 4018
    .local v4, "mMoveableRect":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int v5, v4, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    const/16 v6, 0x96

    if-le v5, v6, :cond_7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$85(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 4019
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 4020
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->updatePosition()V

    .line 4022
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 4023
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->updatePosition()V

    .line 4025
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 4028
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$79(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 4029
    const-string/jumbo v5, "settingui-settingText"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mNeedRotateWhenSetPosition: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$86(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4030
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$86(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 4031
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->rotatePosition()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    .line 4033
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$80(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 4034
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$81(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 4035
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 4044
    :goto_1
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 4045
    .local v2, "handler":Landroid/os/Handler;
    new-instance v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;)V

    invoke-virtual {v2, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 4052
    .end local v2    # "handler":Landroid/os/Handler;
    .end local v3    # "location":[I
    .end local v4    # "mMoveableRect":I
    :catch_0
    move-exception v1

    .line 4053
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 4038
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .restart local v3    # "location":[I
    .restart local v4    # "mMoveableRect":I
    :cond_9
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->checkPosition()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$88(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    .line 4039
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 4040
    .local v0, "Params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
