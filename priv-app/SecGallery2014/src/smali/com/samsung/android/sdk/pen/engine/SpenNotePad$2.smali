.class Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;
.super Ljava/lang/Object;
.source "SpenNotePad.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->onLayout(ZIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    .line 928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 932
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mEnable:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 961
    :goto_0
    return-void

    .line 935
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 936
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V

    .line 937
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V

    .line 938
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onSizeChanged()V

    .line 939
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 940
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mScreenHeight:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$ActionListener;->onChangePan(FF)V

    .line 942
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaX:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V

    .line 943
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxStartY:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDeltaY:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;F)V

    .line 946
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->checkBox()V

    .line 947
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateBox(ZF)V

    .line 948
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updateLayout()V

    .line 949
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 950
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v1

    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setRect(Landroid/graphics/RectF;)V

    .line 951
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mAutoMovingRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 952
    .local v0, "tmpRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 953
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->updateRect(Landroid/graphics/RectF;)V

    .line 954
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mPadBarRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->createBitmap(II)V

    .line 955
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mDrawing:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxHeight:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->mBoxRate:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenNotePad;)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->setHeightAndRate(FF)V

    .line 957
    .end local v0    # "tmpRect":Landroid/graphics/RectF;
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->refresh()V

    .line 958
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->updatePad()V

    .line 960
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePad$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenNotePad;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePad;->invalidate()V

    goto/16 :goto_0
.end method
