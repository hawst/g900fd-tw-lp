.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;
.super Ljava/lang/Object;
.source "SpenSettingRemoverLayout2.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 328
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_0

    .line 329
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v0

    .line 330
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    if-eqz v0, :cond_0

    .line 331
    if-eqz p2, :cond_1

    .line 332
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 333
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEnableSizeSeekbar(Z)V
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;Z)V

    .line 334
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->playSoundEffect(I)V

    .line 341
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 344
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    :cond_0
    return-void

    .line 336
    .restart local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput v2, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 337
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEnableSizeSeekbar(Z)V
    invoke-static {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;Z)V

    .line 338
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->playSoundEffect(I)V

    goto :goto_0
.end method
