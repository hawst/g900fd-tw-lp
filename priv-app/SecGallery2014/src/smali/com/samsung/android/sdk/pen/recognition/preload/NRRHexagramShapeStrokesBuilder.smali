.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;
.super Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;
.source "NRRHexagramShapeStrokesBuilder.java"


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V
    .locals 0
    .param p1, "penSettings"    # Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;
    .param p2, "shapeInfo"    # Lcom/samsung/recognitionengine/ShapeInfo;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V

    .line 21
    return-void
.end method


# virtual methods
.method public buildLayoutObject()Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 24
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    invoke-virtual {v2}, Lcom/samsung/recognitionengine/ShapeInfo;->getRecognizedPoints()Lcom/samsung/recognitionengine/PointFVector;

    move-result-object v1

    .line 26
    .local v1, "points":Lcom/samsung/recognitionengine/PointFVector;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    invoke-virtual {v2}, Lcom/samsung/recognitionengine/ShapeInfo;->getShapeType()Lcom/samsung/recognitionengine/ShapeType;

    move-result-object v2

    sget-object v3, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagram:Lcom/samsung/recognitionengine/ShapeType;

    if-ne v2, v3, :cond_0

    .line 27
    invoke-virtual {v1}, Lcom/samsung/recognitionengine/PointFVector;->size()J

    move-result-wide v2

    const-wide/16 v4, 0x6

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 28
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 30
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    .line 32
    .local v0, "container":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-virtual {v1, v6}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v2

    invoke-virtual {v1, v7}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;->createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 33
    invoke-virtual {v1, v7}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v2

    invoke-virtual {v1, v8}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;->createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 34
    invoke-virtual {v1, v8}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v2

    invoke-virtual {v1, v6}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;->createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 36
    invoke-virtual {v1, v9}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v2

    invoke-virtual {v1, v10}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;->createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 37
    invoke-virtual {v1, v10}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;->createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 38
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v2

    invoke-virtual {v1, v9}, Lcom/samsung/recognitionengine/PointFVector;->get(I)Lcom/samsung/recognitionengine/PointF;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRHexagramShapeStrokesBuilder;->createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 40
    return-object v0
.end method
