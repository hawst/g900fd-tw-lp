.class public Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
.super Landroid/widget/LinearLayout;
.source "SpenSettingTextLayout.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;,
        Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;
    }
.end annotation


# static fields
.field private static final BODY_LAYOUT_HEIGHT:I = 0xfe

.field private static final BOTTOM_LAYOUT_HEIGHT:I = 0x1a

.field private static final EXIT_BUTTON_HEIGHT:I = 0x24

.field private static EXIT_BUTTON_WIDTH:I = 0x0

.field private static final GRADIENT_LAYOUT_HEIGHT:I = 0x34

.field private static final GRAY_PICKER_HEIGHT:I = 0x44

.field private static final GRAY_SCALE_HEIGHT:I = 0xd

.field private static final LL_VERSION_CODE:I = 0x15

.field private static final PICKER_LAYOUT_HEIGHT:I = 0x8c

.field private static final TAB_A_CANVAS_WIDTH:I = 0x300

.field private static final TAG:Ljava/lang/String; = "settingui-settingText"

.field private static final TEXT_SETTING_BOLD:I = 0x2

.field private static final TEXT_SETTING_ITALIC:I = 0x3

.field private static final TEXT_SETTING_UNDERLINE:I = 0x4

.field private static final TITLE_LAYOUT_HEIGHT:I = 0x29

.field private static final TOTAL_LAYOUT_WIDTH:I = 0xf7

.field private static final VIENNA_SCREEN_WIDTH:I = 0x640

.field public static final VIEW_MODE_COLOR:I = 0x4

.field public static final VIEW_MODE_MINIMUM:I = 0x1

.field public static final VIEW_MODE_MINIMUM_WITHOUT_PREVIEW:I = 0x2

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_PARAGRAPH:I = 0x5

.field public static final VIEW_MODE_STYLE:I = 0x3

.field public static final VIEW_MODE_TITLE:I = 0x6

.field private static final bottomExpandPath:Ljava/lang/String; = "snote_popup_bg_expand"

.field private static final bottomExpandPressPath:Ljava/lang/String; = "snote_popup_bg_expand_press"

.field private static final bottomHandlePath:Ljava/lang/String; = "snote_popup_handler"

.field private static final exitPath:Ljava/lang/String; = "snote_popup_close"

.field private static final exitPressPath:Ljava/lang/String; = "snote_popup_close_press"

.field private static final exitfocusPath:Ljava/lang/String; = "snote_popup_close_focus"

.field private static final grayBodyLeftPath:Ljava/lang/String; = "snote_popup_bg02_left"

.field private static final grayBodyRightPath:Ljava/lang/String; = "snote_popup_bg02_right"

.field private static final lefBgDimPath:Ljava/lang/String; = "snote_popup_arrow_left_dim"

.field private static final lefBgFocusPath:Ljava/lang/String; = "snote_popup_arrow_left_focus"

.field private static final lefBgPressPath:Ljava/lang/String; = "snote_popup_arrow_left_press"

.field private static final leftBgPath:Ljava/lang/String; = "snote_popup_arrow_left_normal"

.field private static final lightBodyLeftPath:Ljava/lang/String; = "snote_popup_bg_left"

.field private static final lightBodyRightPath:Ljava/lang/String; = "snote_popup_bg_right"

.field private static final mBoldIconPath:Ljava/lang/String; = "snote_popup_textoption_bold"

.field private static final mBoldLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_left_focus"

.field private static final mBoldLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_left_normal"

.field private static final mBoldLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_left_press_1"

.field private static final mButtonBgPath:Ljava/lang/String; = "snote_popup_btn_normal"

.field private static final mButtonBgPressPath:Ljava/lang/String; = "snote_popup_btn_press"

.field private static final mCenterIconPressPath:Ljava/lang/String;

.field private static final mCenterIocnPath:Ljava/lang/String;

.field private static final mDefaultPath:Ljava/lang/String; = ""

.field private static final mDropdownFocusPath:Ljava/lang/String; = "snote_dropdown_focused"

.field private static final mDropdownNormalPath:Ljava/lang/String; = "snote_dropdown_normal"

.field private static final mDropdownPressPath:Ljava/lang/String; = "snote_dropdown_pressed"

.field private static final mItalicIconPath:Ljava/lang/String; = "snote_popup_textoption_italic"

.field private static final mItalicLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_center_focus"

.field private static final mItalicLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_center_normal"

.field private static final mItalicLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_center_press"

.field private static final mLeftIconPressPath:Ljava/lang/String;

.field private static final mLeftIndentIconPath:Ljava/lang/String;

.field private static final mOptionBgPath:Ljava/lang/String;

.field private static final mPreviewBgPath:Ljava/lang/String; = "snote_popup_preview_bg"

.field private static final mRightIconPath:Ljava/lang/String;

.field private static final mRightIconPressPath:Ljava/lang/String;

.field private static final mRightIndentIconPath:Ljava/lang/String;

.field private static final mScrollHandelNormal:Ljava/lang/String;

.field private static mSdkVersion:I = 0x0

.field private static final mUnderLeftFocusPath:Ljava/lang/String; = "snote_popup_option_btn_right_focus"

.field private static final mUnderLeftNomarPath:Ljava/lang/String; = "snote_popup_option_btn_right_normal"

.field private static final mUnderLeftPressPath:Ljava/lang/String; = "snote_popup_option_btn_right_press_1"

.field private static final mUnderLineIconPath:Ljava/lang/String; = "snote_popup_textoption_underline"

.field private static minHeight:I = 0x0

.field private static minHeightNormal:I = 0x0

.field private static final rightBgDimPath:Ljava/lang/String; = "snote_popup_arrow_right_dim"

.field private static final rightBgFocusPath:Ljava/lang/String; = "snote_popup_arrow_right_focus"

.field private static final rightBgPath:Ljava/lang/String; = "snote_popup_arrow_right_normal"

.field private static final rightBgPressPath:Ljava/lang/String; = "snote_popup_arrow_right_press"

.field private static final titleCenterPath:Ljava/lang/String; = "snote_popup_title_center"

.field private static final titleLeftPath:Ljava/lang/String; = "snote_popup_title_left"

.field private static final titleRightIndicatorPath:Ljava/lang/String; = "snote_popup_title_bended"

.field private static final titleRightPath:Ljava/lang/String; = "snote_popup_title_right"


# instance fields
.field private final EXIT_BUTTON_RIGHT_MARGIN:I

.field private EXIT_BUTTON_TOP_MARGIN:I

.field private RIGHT_INDICATOR_WIDTH:I

.field private final arrImageView:[Landroid/view/View;

.field private deltaOfFirstTime:I

.field private deltaPreview:I

.field private final handlerRotate:Landroid/os/Handler;

.field private isChangePosition:Z

.field private isFirstTime:Z

.field private final localDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

.field private mAlignCenterBtn:Landroid/widget/ImageButton;

.field private mAlignLeftBtn:Landroid/widget/ImageButton;

.field private mAlignRightBtn:Landroid/widget/ImageButton;

.field private mBodyBg:Landroid/view/View;

.field private mBodyLayout:Landroid/view/View;

.field private mBodyLayoutHeight:I

.field private mBoldBtn:Landroid/widget/ImageButton;

.field private mBottomExtendBg:Landroid/widget/ImageView;

.field private mBottomLayout:Landroid/view/View;

.field private mCanvasLayout:Landroid/widget/RelativeLayout;

.field private mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field private mCanvasWidth:I

.field private mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

.field private mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

.field private mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

.field private mColorPickerColor:Landroid/view/View;

.field private final mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

.field private final mColorPickerColorListener:Landroid/view/View$OnClickListener;

.field private mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

.field private mColorPickerSettingExitButton:Landroid/view/View;

.field private mColorSelectedAndPicker:Landroid/view/View;

.field private mCurrentFontName:Ljava/lang/String;

.field private mCurrentFontSize:Ljava/lang/String;

.field private final mCurrentLocation:[I

.field private mCurrentTopMargin:I

.field private final mDismissPopupRunnable:Ljava/lang/Runnable;

.field private mDismissPopupWhenScroll:Z

.field private mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mExitButton:Landroid/view/View;

.field private final mExitButtonListener:Landroid/view/View$OnClickListener;

.field private mExpandFlag:Z

.field mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

.field mExpendBarListener:Landroid/view/View$OnTouchListener;

.field private mFirstLongPress:Z

.field private mFontLineSpaceSpinner:Landroid/widget/Spinner;

.field private mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

.field private mFontSizeButton:Landroid/widget/TextView;

.field private mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

.field private final mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

.field private mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

.field private mFontSizeSpinnerView:Landroid/widget/TextView;

.field private mFontTypeButton:Landroid/widget/TextView;

.field private final mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

.field private mFontTypeSpinnerView:Landroid/view/ViewGroup;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mGestureDetectorListener:Landroid/view/GestureDetector$OnGestureListener;

.field private final mGrayColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

.field private mGrayScaleView:Landroid/view/View;

.field private final mHoverListener:Landroid/view/View$OnHoverListener;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field private mIndentLeftBtn:Landroid/widget/ImageButton;

.field private mIndentRightBtn:Landroid/widget/ImageButton;

.field private mIndicator:Landroid/widget/ImageView;

.field private mIsColorPickerEnabled:Z

.field private mIsFirstShown:Z

.field private mIsRotated:Z

.field private mIsRotated2:Z

.field private mItalicBtn:Landroid/widget/ImageButton;

.field mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mLeftMargin:I

.field private final mMovableRect:Landroid/graphics/Rect;

.field private mMoveSettingLayout:Z

.field private mNeedCalculateMargin:Z

.field private mNeedRecalculateRotate:Z

.field private mNeedRotateWhenSetPosition:Z

.field private final mOldLocation:[I

.field private final mOldMovableRect:Landroid/graphics/Rect;

.field private final mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private final mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

.field private mPaletteBg:Landroid/view/View;

.field private mPaletteLeftButton:Landroid/widget/ImageView;

.field private final mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

.field private mPaletteRightButton:Landroid/widget/ImageView;

.field private mPaletteView:Landroid/view/View;

.field private mParaLineSpinner1stSelect:Z

.field private mParagraphSetting:Landroid/view/View;

.field private mPickerView:Landroid/view/View;

.field private mPopupHeight:I

.field private mPosY:I

.field private mPreCanvasFingerAction:I

.field private mPreCanvasPenAction:I

.field private mRightIndicator:Landroid/widget/ImageView;

.field private mScale:F

.field private mScrollHandle:Landroid/widget/ImageView;

.field private mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

.field private final mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

.field private mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private final mTempMovableRect:Landroid/graphics/Rect;

.field private final mTextAlignSettingListener:Landroid/view/View$OnClickListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field private mTextContext:Landroid/content/Context;

.field private mTextFontSizeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTextIndentSettingListener:Landroid/view/View$OnClickListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field private final mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation
.end field

.field private final mTextOptButtonListener:Landroid/view/View$OnClickListener;

.field private mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

.field private mTextSettingPreview:Landroid/view/View;

.field private mTextSizeButtonView:[Landroid/view/View;

.field private final mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mTitleLayout:Landroid/view/View;

.field private mTopMargin:I

.field private mUnderlineBtn:Landroid/widget/ImageButton;

.field private mViewMode:I

.field private mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

.field private mXDelta:I

.field private mYDelta:I

.field private palateLayoutTranparent:Landroid/widget/RelativeLayout;

.field private paragraphSettingLayout:Landroid/widget/LinearLayout;

.field private final runnableRotate:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 124
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    .line 220
    const/16 v0, 0x26

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    .line 228
    sput v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    .line 229
    sput v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    .line 3935
    const-string/jumbo v0, "snote_text_center"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIocnPath:Ljava/lang/String;

    .line 3936
    const-string/jumbo v0, "snote_text_right"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPath:Ljava/lang/String;

    .line 3937
    const-string/jumbo v0, "snote_text_all_left"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIndentIconPath:Ljava/lang/String;

    .line 3938
    const-string/jumbo v0, "snote_text_all_right"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndentIconPath:Ljava/lang/String;

    .line 3939
    const-string/jumbo v0, "snote_text_left_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIconPressPath:Ljava/lang/String;

    .line 3940
    const-string/jumbo v0, "snote_text_center_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIconPressPath:Ljava/lang/String;

    .line 3941
    const-string/jumbo v0, "snote_text_right_press"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPressPath:Ljava/lang/String;

    .line 3942
    const-string/jumbo v0, "snote_option_in_bg"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOptionBgPath:Ljava/lang/String;

    .line 3943
    const-string/jumbo v0, "snote_popup_scroll_handle_n"

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandelNormal:Ljava/lang/String;

    .line 3944
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p4, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/RelativeLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v6, 0x15

    const/4 v5, 0x2

    const/high16 v4, 0x3fc00000    # 1.5f

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1141
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 54
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    .line 55
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFirstShown:Z

    .line 134
    const-string v1, "10"

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    .line 146
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    .line 147
    const/4 v1, -0x2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    .line 180
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .line 181
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .line 191
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z

    .line 192
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 193
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 194
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    .line 198
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    .line 199
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    .line 205
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    .line 206
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    .line 209
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z

    .line 210
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    .line 212
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 213
    const/16 v1, 0x5a0

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 214
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    .line 230
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 231
    const/4 v1, 0x5

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 235
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    .line 236
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    .line 237
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    .line 244
    const/16 v1, 0x13

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    .line 402
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 486
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 493
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetectorListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 558
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    .line 606
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    .line 648
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 695
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    .line 745
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    .line 803
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    .line 810
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 834
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$11;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    .line 856
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$12;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    .line 891
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$13;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    .line 910
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    .line 940
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    .line 972
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 1001
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$17;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 1010
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$18;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 1019
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    .line 1064
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    .line 1065
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupRunnable:Ljava/lang/Runnable;

    .line 1073
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$21;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 1862
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    .line 1863
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    .line 1864
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$22;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->runnableRotate:Ljava/lang/Runnable;

    .line 2539
    const/16 v1, 0xe

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    .line 3989
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 4071
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    .line 4072
    const/16 v1, 0x1f4

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 4100
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    .line 1142
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    .line 1143
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    invoke-direct {v1, p1, p2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1144
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 1145
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1146
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1147
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1149
    new-instance v1, Lcom/samsung/android/sdk/pen/util/SpenFont;

    invoke-direct {v1, p1, p3}, Lcom/samsung/android/sdk/pen/util/SpenFont;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 1150
    const/16 v0, 0x8

    .local v0, "mTextSize":I
    :goto_0
    if-lt v0, v6, :cond_3

    .line 1153
    const/16 v0, 0x16

    :goto_1
    const/16 v1, 0x21

    if-lt v0, v1, :cond_4

    .line 1156
    const/16 v0, 0x24

    :goto_2
    const/16 v1, 0x41

    if-lt v0, v1, :cond_5

    .line 1159
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 1160
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_6

    .line 1161
    const/4 v1, 0x6

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 1165
    :cond_0
    :goto_3
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-lt v1, v6, :cond_1

    .line 1166
    const/16 v1, 0x24

    sput v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    .line 1168
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_2

    .line 1169
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    .line 1171
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->initView()V

    .line 1172
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setListener()V

    .line 1173
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    .line 1174
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    .line 1175
    new-array v1, v5, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    .line 1176
    new-array v1, v5, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I

    .line 1177
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    .line 1178
    return-void

    .line 1151
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1154
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 1157
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1156
    add-int/lit8 v0, v0, 0x4

    goto :goto_2

    .line 1162
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v1, v1, v4

    if-nez v1, :cond_0

    .line 1163
    const/16 v1, 0x10

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    goto :goto_3
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;F)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p4, "relativeLayout"    # Landroid/widget/RelativeLayout;
    .param p5, "ratio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/RelativeLayout;",
            "F)V"
        }
    .end annotation

    .prologue
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v7, 0x15

    const/4 v6, 0x2

    const/high16 v5, 0x3fc00000    # 1.5f

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1245
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 54
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    .line 55
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFirstShown:Z

    .line 134
    const-string v1, "10"

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    .line 146
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    .line 147
    const/4 v1, -0x2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    .line 180
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .line 181
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .line 191
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z

    .line 192
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 193
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 194
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    .line 198
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    .line 199
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    .line 205
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    .line 206
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    .line 209
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z

    .line 210
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    .line 212
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 213
    const/16 v1, 0x5a0

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 214
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    .line 230
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 231
    const/4 v1, 0x5

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_RIGHT_MARGIN:I

    .line 235
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    .line 236
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    .line 237
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    .line 244
    const/16 v1, 0x13

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    .line 402
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 486
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    .line 493
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetectorListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 558
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    .line 606
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    .line 648
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    .line 695
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    .line 745
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    .line 803
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    .line 810
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    .line 834
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$11;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    .line 856
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$12;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    .line 891
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$13;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    .line 910
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    .line 940
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    .line 972
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 1001
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$17;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    .line 1010
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$18;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 1019
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    .line 1064
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    .line 1065
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupRunnable:Ljava/lang/Runnable;

    .line 1073
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$21;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    .line 1862
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    .line 1863
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    .line 1864
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$22;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->runnableRotate:Ljava/lang/Runnable;

    .line 2539
    const/16 v1, 0xe

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    .line 3989
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$23;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 4071
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    .line 4072
    const/16 v1, 0x1f4

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 4100
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    .line 1246
    iput p5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 1247
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    :goto_0
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 1248
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    const v2, 0x3f59999a    # 0.85f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    :goto_1
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 1249
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    .line 1250
    const-string v1, "Hoa"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mSdkVersion1: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1251
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    invoke-direct {v1, p1, p2, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1252
    const-string v1, "Hoa"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mSdkVersion2: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    .line 1254
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1255
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1256
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1258
    new-instance v1, Lcom/samsung/android/sdk/pen/util/SpenFont;

    invoke-direct {v1, p1, p3}, Lcom/samsung/android/sdk/pen/util/SpenFont;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 1259
    const/16 v0, 0x8

    .local v0, "mTextSize":I
    :goto_2
    if-lt v0, v7, :cond_5

    .line 1262
    const/16 v0, 0x16

    :goto_3
    const/16 v1, 0x21

    if-lt v0, v1, :cond_6

    .line 1265
    const/16 v0, 0x24

    :goto_4
    const/16 v1, 0x41

    if-lt v0, v1, :cond_7

    .line 1268
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 1269
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    cmpg-float v1, v1, v5

    if-gez v1, :cond_8

    .line 1270
    const/4 v1, 0x6

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    .line 1274
    :cond_0
    :goto_5
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-lt v1, v7, :cond_1

    .line 1275
    const/16 v1, 0x24

    sput v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    .line 1277
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    cmpg-float v1, v1, v5

    if-gez v1, :cond_2

    .line 1278
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    .line 1281
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->initView()V

    .line 1283
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setListener()V

    .line 1284
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    .line 1285
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    .line 1286
    new-array v1, v6, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    .line 1287
    new-array v1, v6, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I

    .line 1288
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    .line 1289
    return-void

    .line 1247
    .end local v0    # "mTextSize":I
    :cond_3
    const/high16 v1, 0x40000000    # 2.0f

    goto/16 :goto_0

    .line 1248
    :cond_4
    const v1, 0x3f59999a    # 0.85f

    goto/16 :goto_1

    .line 1260
    .restart local v0    # "mTextSize":I
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1259
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1263
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1262
    add-int/lit8 v0, v0, 0x2

    goto :goto_3

    .line 1266
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1265
    add-int/lit8 v0, v0, 0x4

    goto :goto_4

    .line 1271
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v1, v1, v5

    if-nez v1, :cond_0

    .line 1272
    const/16 v1, 0x10

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    goto :goto_5
.end method

.method private ColorPickerSettinginit()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1794
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;FII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    .line 1795
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    .line 1796
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1797
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColor:Landroid/view/View;

    .line 1798
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColor:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1799
    return-void
.end method

.method private GrayScaleView()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2524
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2525
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2526
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2525
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2527
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d00000    # 26.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2528
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2529
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42f00000    # 120.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2530
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2532
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->colorGrayScaleView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    .line 2533
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2534
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 2536
    return-object v1
.end method

.method private PaletteView()Landroid/view/ViewGroup;
    .locals 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 2544
    new-instance v11, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v11, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2545
    .local v11, "sizeDisplayLayout":Landroid/widget/RelativeLayout;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    .line 2546
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x425c0000    # 55.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 2545
    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2547
    .local v12, "sizeDisplayLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    .line 2548
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41100000    # 9.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, 0x0

    .line 2547
    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2549
    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2550
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 2551
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    .line 2552
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2553
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x425c0000    # 55.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 2552
    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2554
    .local v10, "rightImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2555
    const/16 v0, 0xb

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2556
    const/16 v0, 0xf

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2558
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 2559
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2562
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2563
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_next"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2565
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 2566
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_arrow_right_normal"

    const-string/jumbo v2, "snote_popup_arrow_right_press"

    .line 2567
    const-string/jumbo v3, "snote_popup_arrow_right_focus"

    const-string/jumbo v4, "snote_popup_arrow_right_dim"

    const/16 v5, 0x12

    const/16 v6, 0x37

    .line 2566
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2579
    :goto_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    .line 2580
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2581
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x425c0000    # 55.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 2580
    invoke-direct {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2582
    .local v9, "leftImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x1

    iput-boolean v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2583
    const/16 v0, 0x9

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2584
    const/16 v0, 0xf

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2586
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 2587
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2591
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2592
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_back"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2594
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_4

    .line 2595
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_arrow_left_normal"

    const-string/jumbo v2, "snote_popup_arrow_left_press"

    .line 2596
    const-string/jumbo v3, "snote_popup_arrow_left_focus"

    const-string/jumbo v4, "snote_popup_arrow_left_dim"

    const/16 v5, 0x12

    const/16 v6, 0x37

    .line 2595
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2608
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    .line 2610
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paletteTransparent()V

    .line 2611
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2612
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2613
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2614
    return-object v11

    .line 2568
    .end local v9    # "leftImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 2569
    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    const/4 v0, -0x1

    invoke-direct {v8, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2570
    .local v8, "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    .line 2571
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x2d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v8}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2570
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2572
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_arrow_right_normal"

    const/4 v2, 0x0

    const-string/jumbo v3, "snote_popup_arrow_right_focus"

    .line 2573
    const/4 v4, 0x0

    const-string/jumbo v5, "snote_popup_arrow_right_dim"

    const/16 v6, 0x12

    const/16 v7, 0x37

    .line 2572
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2575
    .end local v8    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_arrow_right_normal"

    const-string/jumbo v2, "snote_popup_arrow_right_press"

    .line 2576
    const-string/jumbo v3, "snote_popup_arrow_right_focus"

    const-string/jumbo v4, "snote_popup_arrow_right_dim"

    const/16 v5, 0x12

    const/16 v6, 0x37

    .line 2575
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2597
    .restart local v9    # "leftImageParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_5

    .line 2598
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 2599
    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    const/4 v0, -0x1

    invoke-direct {v8, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2600
    .restart local v8    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/RippleDrawable;

    .line 2601
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v3, 0x2d

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v8}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2600
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2602
    iget-object v13, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_arrow_left_normal"

    const/4 v2, 0x0

    const-string/jumbo v3, "snote_popup_arrow_left_focus"

    .line 2603
    const/4 v4, 0x0

    const-string/jumbo v5, "snote_popup_arrow_left_dim"

    const/16 v6, 0x12

    const/16 v7, 0x37

    .line 2602
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 2605
    .end local v8    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_popup_arrow_left_normal"

    const-string/jumbo v2, "snote_popup_arrow_left_press"

    const-string/jumbo v3, "snote_popup_arrow_left_focus"

    .line 2606
    const-string/jumbo v4, "snote_popup_arrow_left_dim"

    const/16 v5, 0x12

    const/16 v6, 0x37

    .line 2605
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method private PickerView()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2509
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2510
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2511
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42400000    # 48.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2510
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2512
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d00000    # 26.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2513
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2514
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x428a0000    # 69.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2515
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2517
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->colorPickerView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    .line 2518
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2519
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 2520
    return-object v1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/GestureDetector;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 210
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 4058
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 200
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftMargin:I

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 201
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTopMargin:I

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 198
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 199
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 194
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 195
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mXDelta:I

    return-void
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 196
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mYDelta:I

    return-void
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    return-object v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    return v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    return-object v0
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    return-object v0
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    return-object v0
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    return-object v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 4100
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    return v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 188
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    return-void
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 189
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    return-void
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    return v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 236
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    return-void
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mXDelta:I

    return v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    return v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 4074
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->calculatePopupPosition()V

    return-void
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 4072
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    return v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)F
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    return v0
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    return-void
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/util/DisplayMetrics;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    return-object v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V
    .locals 0

    .prologue
    .line 213
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    return v0
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mYDelta:I

    return v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 4071
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    return v0
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    return-void
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$56()I
    .locals 1

    .prologue
    .line 124
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    return v0
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$58(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 3363
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setTextStyle(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$59(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$60(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    return v0
.end method

.method static synthetic access$61(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$62(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$63(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$64(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$65(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$66(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$67(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$68(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[Landroid/view/View;
    .locals 1

    .prologue
    .line 2539
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$69(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z

    return v0
.end method

.method static synthetic access$7()I
    .locals 1

    .prologue
    .line 228
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    return v0
.end method

.method static synthetic access$70(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParaLineSpinner1stSelect:Z

    return-void
.end method

.method static synthetic access$71(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$73(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    return-object v0
.end method

.method static synthetic access$74(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 146
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    return-void
.end method

.method static synthetic access$75(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    return v0
.end method

.method static synthetic access$76(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 1064
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    return-void
.end method

.method static synthetic access$77(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 1064
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    return v0
.end method

.method static synthetic access$78(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 1862
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    return-void
.end method

.method static synthetic access$79(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$80(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    return-void
.end method

.method static synthetic access$81(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    return-void
.end method

.method static synthetic access$82(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    return v0
.end method

.method static synthetic access$83(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentLocation:[I

    return-object v0
.end method

.method static synthetic access$84(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V
    .locals 0

    .prologue
    .line 209
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z

    return-void
.end method

.method static synthetic access$85(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isChangePosition:Z

    return v0
.end method

.method static synthetic access$86(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z
    .locals 1

    .prologue
    .line 1862
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    return v0
.end method

.method static synthetic access$87(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 2809
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->rotatePosition()V

    return-void
.end method

.method static synthetic access$88(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 2871
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->checkPosition()V

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method

.method private addParagraphSettingLayout()V
    .locals 27

    .prologue
    .line 2185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    if-nez v22, :cond_0

    .line 2189
    new-instance v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v13, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2190
    .local v13, "paragraphSettingAlignLayout":Landroid/widget/LinearLayout;
    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    .line 2191
    const/16 v22, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2190
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v14, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2192
    .local v14, "paragraphSettingAlignLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40c00000    # 6.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40e00000    # 7.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2194
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2195
    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2196
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2197
    new-instance v10, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v10, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2198
    .local v10, "mAlignTextView":Landroid/widget/TextView;
    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x1

    .line 2199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2198
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v11, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2200
    .local v11, "mAlignTextViewParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    iput v0, v11, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2201
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v22, v0

    const-string/jumbo v23, "string_align"

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2203
    const/high16 v22, -0x1000000

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2204
    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41700000    # 15.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2205
    const/16 v22, 0x11

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2206
    invoke-virtual {v13, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2207
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    .line 2208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x42300000    # 44.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41d80000    # 27.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2207
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v9, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2209
    .local v9, "mAlignLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x41300000    # 11.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    iput v0, v9, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2210
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v9, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2211
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    .line 2212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const-string/jumbo v24, "snote_text_left"

    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIconPressPath:Ljava/lang/String;

    .line 2215
    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIconPressPath:Ljava/lang/String;

    .line 2214
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2217
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    .line 2218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x42300000    # 44.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41d80000    # 27.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2217
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v8, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2219
    .local v8, "mAlignCenterBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v8, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2220
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    .line 2221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIocnPath:Ljava/lang/String;

    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIconPressPath:Ljava/lang/String;

    .line 2224
    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCenterIconPressPath:Ljava/lang/String;

    .line 2223
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2226
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    .line 2227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string/jumbo v24, "snote_popup_btn_normal"

    const-string/jumbo v25, "snote_popup_btn_press"

    .line 2231
    const-string/jumbo v26, "snote_popup_btn_press"

    .line 2230
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string/jumbo v24, "snote_popup_btn_normal"

    const-string/jumbo v25, "snote_popup_btn_press"

    .line 2233
    const-string/jumbo v26, "snote_popup_btn_press"

    .line 2232
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string/jumbo v24, "snote_popup_btn_normal"

    const-string/jumbo v25, "snote_popup_btn_press"

    .line 2235
    const-string/jumbo v26, "snote_popup_btn_press"

    .line 2234
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPath:Ljava/lang/String;

    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPressPath:Ljava/lang/String;

    .line 2238
    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIconPressPath:Ljava/lang/String;

    .line 2237
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2243
    new-instance v15, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v15, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2244
    .local v15, "paragraphSettingIndentLayout":Landroid/widget/LinearLayout;
    new-instance v16, Landroid/widget/LinearLayout$LayoutParams;

    .line 2245
    const/16 v22, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2244
    move-object/from16 v0, v16

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2246
    .local v16, "paragraphSettingIndentLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40c00000    # 6.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40e00000    # 7.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2248
    const/16 v22, 0x10

    move/from16 v0, v22

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2249
    invoke-virtual/range {v15 .. v16}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2250
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2252
    new-instance v7, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2253
    .local v7, "indentTextView":Landroid/widget/TextView;
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x1

    .line 2254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2253
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v6, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2255
    .local v6, "indentTextParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    iput v0, v6, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2256
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v22, v0

    const-string/jumbo v23, "string_indent"

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2258
    const/high16 v22, -0x1000000

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2259
    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41700000    # 15.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2260
    const/16 v22, 0x11

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2261
    invoke-virtual {v15, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2263
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    .line 2264
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 2265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x42300000    # 44.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41d80000    # 27.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2264
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v4, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2266
    .local v4, "indentLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x41300000    # 11.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    iput v0, v4, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2267
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIndentIconPath:Ljava/lang/String;

    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIndentIconPath:Ljava/lang/String;

    .line 2270
    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftIndentIconPath:Ljava/lang/String;

    .line 2269
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2273
    new-instance v22, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    .line 2274
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 2275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x42300000    # 44.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41d80000    # 27.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2274
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v5, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2276
    .local v5, "indentRightBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v22, 0x10

    move/from16 v0, v22

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string/jumbo v24, "snote_popup_btn_normal"

    const-string/jumbo v25, "snote_popup_btn_press"

    .line 2281
    const-string/jumbo v26, "snote_popup_btn_press"

    .line 2280
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v23, v0

    const-string/jumbo v24, "snote_popup_btn_normal"

    const-string/jumbo v25, "snote_popup_btn_press"

    .line 2283
    const-string/jumbo v26, "snote_popup_btn_press"

    .line 2282
    invoke-virtual/range {v22 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndentIconPath:Ljava/lang/String;

    .line 2286
    sget-object v25, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndentIconPath:Ljava/lang/String;

    sget-object v26, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndentIconPath:Ljava/lang/String;

    .line 2285
    invoke-virtual/range {v23 .. v26}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2291
    new-instance v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2292
    .local v17, "paragraphSettingSpacingLayout":Landroid/widget/LinearLayout;
    new-instance v18, Landroid/widget/LinearLayout$LayoutParams;

    .line 2293
    const/16 v22, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2292
    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2294
    .local v18, "paragraphSettingSpacingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40c00000    # 6.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x40e00000    # 7.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2296
    const/16 v22, 0x10

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2297
    invoke-virtual/range {v17 .. v18}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2298
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2300
    new-instance v20, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2301
    .local v20, "spacingTextView":Landroid/widget/TextView;
    new-instance v19, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x1

    .line 2302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x42580000    # 54.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    .line 2301
    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2303
    .local v19, "spacingTextParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    move-object/from16 v1, v19

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2304
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    move-object/from16 v22, v0

    const-string/jumbo v23, "string_line_spacing"

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2306
    const/high16 v22, -0x1000000

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2307
    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v23, v0

    const/high16 v24, 0x41700000    # 15.0f

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2308
    const/16 v22, 0x11

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2309
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2311
    new-instance v22, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    .line 2312
    new-instance v21, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v22, -0x2

    .line 2313
    const/16 v23, -0x2

    .line 2312
    invoke-direct/range {v21 .. v23}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2314
    .local v21, "spinnerBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v22, v0

    const/high16 v23, 0x41300000    # 11.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2315
    const/16 v22, 0x10

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2317
    const/16 v22, 0x1e

    move/from16 v0, v22

    new-array v12, v0, [Ljava/lang/String;

    const/16 v22, 0x0

    const-string v23, "10"

    aput-object v23, v12, v22

    const/16 v22, 0x1

    const-string v23, "11"

    aput-object v23, v12, v22

    const/16 v22, 0x2

    const-string v23, "12"

    aput-object v23, v12, v22

    const/16 v22, 0x3

    const-string v23, "13"

    aput-object v23, v12, v22

    const/16 v22, 0x4

    const-string v23, "14"

    aput-object v23, v12, v22

    const/16 v22, 0x5

    const-string v23, "15"

    aput-object v23, v12, v22

    const/16 v22, 0x6

    const-string v23, "16"

    aput-object v23, v12, v22

    const/16 v22, 0x7

    const-string v23, "17"

    aput-object v23, v12, v22

    const/16 v22, 0x8

    const-string v23, "18"

    aput-object v23, v12, v22

    const/16 v22, 0x9

    const-string v23, "19"

    aput-object v23, v12, v22

    const/16 v22, 0xa

    const-string v23, "20"

    aput-object v23, v12, v22

    const/16 v22, 0xb

    const-string v23, "22"

    aput-object v23, v12, v22

    const/16 v22, 0xc

    const-string v23, "24"

    aput-object v23, v12, v22

    const/16 v22, 0xd

    const-string v23, "26"

    aput-object v23, v12, v22

    const/16 v22, 0xe

    .line 2318
    const-string v23, "28"

    aput-object v23, v12, v22

    const/16 v22, 0xf

    const-string v23, "30"

    aput-object v23, v12, v22

    const/16 v22, 0x10

    const-string v23, "32"

    aput-object v23, v12, v22

    const/16 v22, 0x11

    const-string v23, "36"

    aput-object v23, v12, v22

    const/16 v22, 0x12

    const-string v23, "40"

    aput-object v23, v12, v22

    const/16 v22, 0x13

    const-string v23, "43"

    aput-object v23, v12, v22

    const/16 v22, 0x14

    const-string v23, "44"

    aput-object v23, v12, v22

    const/16 v22, 0x15

    const-string v23, "48"

    aput-object v23, v12, v22

    const/16 v22, 0x16

    const-string v23, "52"

    aput-object v23, v12, v22

    const/16 v22, 0x17

    const-string v23, "56"

    aput-object v23, v12, v22

    const/16 v22, 0x18

    const-string v23, "60"

    aput-object v23, v12, v22

    const/16 v22, 0x19

    const-string v23, "64"

    aput-object v23, v12, v22

    const/16 v22, 0x1a

    const-string v23, "68"

    aput-object v23, v12, v22

    const/16 v22, 0x1b

    const-string v23, "72"

    aput-object v23, v12, v22

    const/16 v22, 0x1c

    const-string v23, "80"

    aput-object v23, v12, v22

    const/16 v22, 0x1d

    const-string v23, "88"

    aput-object v23, v12, v22

    .line 2319
    .local v12, "numbers":[Ljava/lang/String;
    new-instance v3, Landroid/widget/ArrayAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x1090008

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v3, v0, v1, v12}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 2321
    .local v3, "adpater":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2322
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    const/16 v23, 0x13

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextLineSpacingSettingListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    move-object/from16 v22, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2332
    .end local v3    # "adpater":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .end local v4    # "indentLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v5    # "indentRightBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "indentTextParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v7    # "indentTextView":Landroid/widget/TextView;
    .end local v8    # "mAlignCenterBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v9    # "mAlignLeftBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v10    # "mAlignTextView":Landroid/widget/TextView;
    .end local v11    # "mAlignTextViewParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v12    # "numbers":[Ljava/lang/String;
    .end local v13    # "paragraphSettingAlignLayout":Landroid/widget/LinearLayout;
    .end local v14    # "paragraphSettingAlignLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v15    # "paragraphSettingIndentLayout":Landroid/widget/LinearLayout;
    .end local v16    # "paragraphSettingIndentLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v17    # "paragraphSettingSpacingLayout":Landroid/widget/LinearLayout;
    .end local v18    # "paragraphSettingSpacingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v19    # "spacingTextParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v20    # "spacingTextView":Landroid/widget/TextView;
    .end local v21    # "spinnerBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    .line 2333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2340
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    if-eqz v22, :cond_2

    .line 2341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2344
    :cond_2
    return-void
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 12

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x1

    const/high16 v7, 0x43770000    # 247.0f

    const/high16 v9, 0x42f80000    # 124.0f

    const/4 v8, -0x1

    .line 1534
    new-instance v5, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1535
    .local v5, "layout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1536
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1535
    invoke-direct {v0, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1538
    .local v0, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1540
    new-instance v1, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1541
    .local v1, "bodyLeft":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1542
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1541
    invoke-direct {v2, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1543
    .local v2, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v10, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1544
    const/16 v6, 0x9

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1545
    invoke-virtual {v2, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1546
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1547
    new-instance v3, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1548
    .local v3, "bodyRight":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1549
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    sub-int/2addr v6, v7

    .line 1548
    invoke-direct {v4, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1551
    .local v4, "bodyRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v10, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1552
    const/16 v6, 0xb

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1553
    invoke-virtual {v4, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1554
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1556
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v7, "snote_popup_bg_left"

    invoke-virtual {v6, v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1557
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v7, "snote_popup_bg_right"

    invoke-virtual {v6, v3, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1558
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1559
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1560
    invoke-virtual {v5, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1561
    invoke-virtual {v5, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1562
    return-object v5
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 15

    .prologue
    const/high16 v14, 0x43770000    # 247.0f

    const/4 v10, 0x1

    const/4 v13, -0x1

    const/4 v12, -0x2

    const/4 v11, 0x0

    .line 1472
    new-instance v8, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    .line 1473
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1474
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1473
    invoke-direct {v3, v8, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1475
    .local v3, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x41d00000    # 26.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-virtual {v3, v11, v11, v11, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1476
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1477
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalFadingEdgeEnabled(Z)V

    .line 1478
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setFadingEdgeLength(I)V

    .line 1479
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 1480
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOverScrollMode(I)V

    .line 1482
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;-><init>(Landroid/content/Context;)V

    .line 1483
    .local v4, "palletViewLayout":Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v13, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1485
    .local v5, "palletViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1486
    invoke-virtual {v4, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setOrientation(I)V

    .line 1487
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->textPreview()Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    .line 1488
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->fontSizeSpinnerOptButton()Landroid/view/ViewGroup;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    .line 1489
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->colorSelectedAndPicker()Landroid/view/ViewGroup;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    .line 1490
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 1491
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 1492
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 1494
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSetting()Landroid/view/ViewGroup;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    .line 1496
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->bottomLayout()Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    .line 1497
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v10, "string_drag_to_resize"

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1499
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    .line 1500
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1501
    const/4 v8, -0x5

    const/4 v9, -0x5

    invoke-virtual {v4, v8, v11, v9, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setPadding(IIII)V

    .line 1502
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->addView(Landroid/view/View;)V

    .line 1504
    new-instance v6, Landroid/widget/RelativeLayout;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v8}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1505
    .local v6, "scrollParent":Landroid/widget/RelativeLayout;
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v7, v13, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1507
    .local v7, "scrollParentParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x40c00000    # 6.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    neg-int v8, v8

    invoke-virtual {v7, v11, v11, v11, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1508
    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1509
    const/4 v8, 0x5

    const/4 v9, 0x5

    invoke-virtual {v6, v8, v11, v9, v11}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1511
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1513
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v8}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1514
    .local v1, "layout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1515
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1514
    invoke-direct {v2, v8, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1516
    .local v2, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1517
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    .line 1518
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1520
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1521
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1522
    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    if-nez v8, :cond_0

    .line 1523
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1524
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42900000    # 72.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1523
    invoke-direct {v0, v13, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1525
    .local v0, "colorPickerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1530
    .end local v0    # "colorPickerParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-object v1

    .line 1527
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 1528
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    goto :goto_0
.end method

.method private bottomLayout()Landroid/view/View;
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 2729
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2730
    .local v2, "bottomLayout":Landroid/widget/RelativeLayout;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    .line 2731
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41d00000    # 26.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2730
    invoke-direct {v3, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2732
    .local v3, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v6, 0x0

    .line 2733
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v8, v8, 0xfe

    add-int/lit8 v8, v8, -0x1a

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2732
    invoke-virtual {v3, v6, v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2734
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2735
    new-instance v6, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    .line 2736
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v8, -0x1

    .line 2737
    const/4 v9, -0x1

    invoke-direct {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2736
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2739
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v7, 0x10

    if-ge v6, v7, :cond_0

    .line 2740
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    const-string/jumbo v8, "snote_popup_bg_expand"

    const-string/jumbo v9, "snote_popup_bg_expand_press"

    .line 2741
    const-string/jumbo v10, "snote_popup_bg_expand_press"

    .line 2740
    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2742
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v8, "snote_popup_bg_expand"

    .line 2743
    const-string/jumbo v9, "snote_popup_bg_expand_press"

    const-string/jumbo v10, "snote_popup_bg_expand_press"

    .line 2742
    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2755
    :goto_0
    new-instance v0, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2757
    .local v0, "bottomHandle":Landroid/widget/ImageView;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-lez v6, :cond_2

    .line 2758
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2759
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41a00000    # 20.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/lit8 v7, v7, -0x13

    .line 2758
    invoke-direct {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2764
    .local v1, "bottomHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_1
    const/4 v6, 0x1

    iput-boolean v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2765
    const/16 v6, 0xe

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2766
    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2768
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-lez v6, :cond_3

    .line 2769
    const/16 v6, 0x10

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 2775
    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2776
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v7, "snote_popup_handler"

    const/16 v8, 0x16

    const/16 v9, 0x10

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2777
    new-instance v5, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 2779
    .local v5, "mButtonHandle":Landroid/widget/ImageButton;
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-lez v6, :cond_5

    .line 2780
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2781
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41a00000    # 20.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/lit8 v7, v7, -0x13

    .line 2780
    invoke-direct {v4, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2786
    .local v4, "buttonHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_3
    const/4 v6, 0x1

    iput-boolean v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2787
    const/16 v6, 0xe

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2788
    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2789
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-lez v6, :cond_6

    .line 2790
    const/16 v6, 0x10

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 2796
    :goto_4
    invoke-virtual {v5, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2797
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 2798
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 2799
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v7, "string_drag_to_resize"

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2801
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2802
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2803
    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2804
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2806
    return-object v2

    .line 2744
    .end local v0    # "bottomHandle":Landroid/widget/ImageView;
    .end local v1    # "bottomHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "buttonHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "mButtonHandle":Landroid/widget/ImageButton;
    :cond_0
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_1

    .line 2745
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    new-instance v7, Landroid/graphics/drawable/RippleDrawable;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v9, 0x29

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v8

    .line 2746
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v10, "snote_popup_bg_expand"

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2745
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2749
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    const-string/jumbo v8, "snote_popup_bg_expand"

    const-string/jumbo v9, "snote_popup_bg_expand_press"

    .line 2750
    const-string/jumbo v10, "snote_popup_bg_expand_press"

    .line 2749
    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2751
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v8, "snote_popup_bg_expand"

    const-string/jumbo v9, "snote_popup_bg_expand_press"

    .line 2752
    const-string/jumbo v10, "snote_popup_bg_expand_press"

    .line 2751
    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2761
    .restart local v0    # "bottomHandle":Landroid/widget/ImageView;
    :cond_2
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2762
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41a00000    # 20.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/lit8 v7, v7, -0x8

    .line 2761
    invoke-direct {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .restart local v1    # "bottomHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    goto/16 :goto_1

    .line 2770
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-nez v6, :cond_4

    .line 2771
    const/16 v6, 0x9

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_2

    .line 2773
    :cond_4
    const/4 v6, 0x5

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_2

    .line 2783
    .restart local v5    # "mButtonHandle":Landroid/widget/ImageButton;
    :cond_5
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2784
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41a00000    # 20.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/lit8 v7, v7, -0x8

    .line 2783
    invoke-direct {v4, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .restart local v4    # "buttonHandleParam":Landroid/widget/RelativeLayout$LayoutParams;
    goto/16 :goto_3

    .line 2791
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-nez v6, :cond_7

    .line 2792
    const/16 v6, 0x9

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_4

    .line 2794
    :cond_7
    const/4 v6, 0x5

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_4
.end method

.method private calculatePopupPosition()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x41d80000    # 27.0f

    const/4 v6, 0x2

    .line 4075
    new-array v0, v6, [I

    .line 4076
    .local v0, "location":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->getLocationInWindow([I)V

    .line 4077
    new-array v1, v6, [I

    .line 4078
    .local v1, "location2":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 4079
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43200000    # 160.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 4081
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    aget v5, v0, v8

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 4082
    .local v2, "mTopAvailable":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    aget v5, v0, v8

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 4083
    .local v3, "mbottomAvailable":I
    add-int/lit8 v4, v3, -0x2

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    if-le v4, v5, :cond_0

    .line 4084
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    .line 4098
    :goto_0
    return-void

    .line 4086
    :cond_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    if-le v2, v4, :cond_1

    .line 4087
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x433b0000    # 187.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    neg-int v4, v4

    add-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    goto :goto_0

    .line 4089
    :cond_1
    if-le v2, v3, :cond_2

    .line 4090
    add-int/lit8 v4, v2, -0x5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    .line 4091
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    neg-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    goto :goto_0

    .line 4093
    :cond_2
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPosY:I

    .line 4094
    add-int/lit8 v4, v3, -0x5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPopupHeight:I

    goto :goto_0
.end method

.method private checkPosition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2872
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 2873
    .local v0, "location":[I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43770000    # 247.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int v1, v3, v4

    .line 2874
    .local v1, "minWidth":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    aget v3, v3, v6

    aput v3, v0, v6

    .line 2875
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    aget v3, v3, v7

    aput v3, v0, v7

    .line 2876
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2878
    .local v2, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    aget v3, v0, v6

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-ge v3, v4, :cond_0

    .line 2879
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2881
    :cond_0
    aget v3, v0, v7

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-ge v3, v4, :cond_1

    .line 2882
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2885
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    aget v4, v0, v6

    sub-int/2addr v3, v4

    if-ge v3, v1, :cond_2

    .line 2886
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x2

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2888
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v3, :cond_2

    .line 2889
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2892
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    aget v4, v0, v7

    sub-int/2addr v3, v4

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    if-ge v3, v4, :cond_3

    .line 2893
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2895
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v3, :cond_3

    .line 2896
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2899
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    aput v4, v3, v6

    .line 2900
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v5

    aput v4, v3, v7

    .line 2901
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2902
    return-void
.end method

.method private colorGrayScaleView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2706
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 2707
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 2706
    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 2708
    .local v1, "localf":Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2709
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2708
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2711
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2712
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setClickable(Z)V

    .line 2713
    invoke-virtual {v1, v5, v5, v5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setPadding(IIII)V

    .line 2714
    const v2, -0xff0100

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setBackgroundColor(I)V

    .line 2715
    return-object v1
.end method

.method private colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 2658
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 2659
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    .line 2658
    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 2660
    .local v0, "colorPaletteView":Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColorPickerEnable(Z)V

    .line 2662
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2663
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43410000    # 193.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, -0x2

    .line 2662
    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2665
    .local v1, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2666
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2667
    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2669
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2670
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setClickable(Z)V

    .line 2671
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setFocusable(Z)V

    .line 2672
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 2673
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setImportantForAccessibility(I)V

    .line 2675
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_palette"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2677
    return-object v0
.end method

.method private colorPickerView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2689
    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScale:F

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    .line 2690
    .local v1, "localf":Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2691
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x423c0000    # 47.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2690
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2693
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2694
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setClickable(Z)V

    .line 2695
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_gradation"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2696
    invoke-virtual {v1, v5, v5, v5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setPadding(IIII)V

    .line 2697
    return-object v1
.end method

.method private colorSelectedAndPicker()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2461
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2462
    .local v1, "localLinearLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    .line 2463
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x430c0000    # 140.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2462
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2465
    .local v0, "localLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2466
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->PickerView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPickerView:Landroid/view/View;

    .line 2467
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->PaletteView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteView:Landroid/view/View;

    .line 2468
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paletteBg()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    .line 2469
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->GrayScaleView()Landroid/view/ViewGroup;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayScaleView:Landroid/view/View;

    .line 2470
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2471
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2472
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPickerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2473
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayScaleView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2474
    return-object v1
.end method

.method private exitButton()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x29

    const/16 v6, 0x24

    const/4 v4, -0x1

    const/high16 v3, 0x42240000    # 41.0f

    const/16 v7, 0xff

    .line 1974
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1976
    .local v1, "exitButton":Landroid/widget/ImageButton;
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_0

    .line 1977
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1978
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    sub-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x26

    div-int/lit8 v0, v0, 0x24

    .line 1977
    invoke-direct {v8, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1980
    .local v8, "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_close"

    const-string/jumbo v3, "snote_popup_close_press"

    const-string/jumbo v4, "snote_popup_close_focus"

    .line 1981
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    .line 1980
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1991
    :goto_0
    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1992
    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1993
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1994
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1996
    return-object v1

    .line 1983
    .end local v8    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1984
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    sub-int/2addr v0, v2

    .line 1983
    invoke-direct {v8, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1986
    .restart local v8    # "exitButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1987
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1986
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1988
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_close"

    const-string/jumbo v3, "snote_popup_close_press"

    const-string/jumbo v4, "snote_popup_close_focus"

    .line 1989
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_WIDTH:I

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    .line 1988
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto :goto_0
.end method

.method private fontSizeSpinner()Landroid/widget/TextView;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2357
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    .line 2358
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2359
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2358
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2360
    .local v0, "fontSizeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40e00000    # 7.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2361
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2362
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 2363
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v3, "snote_dropdown_normal"

    .line 2364
    const-string/jumbo v4, "snote_dropdown_pressed"

    const-string/jumbo v5, "snote_dropdown_focused"

    .line 2363
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2374
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 2375
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v8, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2377
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2378
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    invoke-static {v8, v8, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2379
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2380
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2381
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2383
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2384
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    return-object v1

    .line 2365
    :cond_0
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 2366
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x3d

    invoke-static {v4, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 2367
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v5, "snote_dropdown_normal"

    const-string/jumbo v6, "snote_dropdown_normal"

    const-string/jumbo v7, "snote_dropdown_focused"

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    .line 2368
    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2366
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2370
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v3, "snote_dropdown_normal"

    const-string/jumbo v4, "snote_dropdown_pressed"

    .line 2371
    const-string/jumbo v5, "snote_dropdown_focused"

    .line 2370
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method private fontSizeSpinnerOptButton()Landroid/view/ViewGroup;
    .locals 14

    .prologue
    .line 2055
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2056
    .local v1, "fontSizeSpinnerOptButtonLayout":Landroid/widget/LinearLayout;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 2057
    const/4 v6, -0x1

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x421c0000    # 39.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2056
    invoke-direct {v2, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2058
    .local v2, "fontSizeSpinnerOptButtonLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2059
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 2060
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2061
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41500000    # 13.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2062
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x40a00000    # 5.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2061
    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2063
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->fontTypeSpinnerView()Landroid/view/ViewGroup;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    .line 2064
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->fontSizeSpinner()Landroid/widget/TextView;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    .line 2065
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2066
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2068
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_0

    .line 2069
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e00000    # 28.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2070
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41f80000    # 31.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2069
    invoke-direct {v3, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2076
    .local v3, "mBoldBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    new-instance v6, Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    .line 2077
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v6, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2078
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2079
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v8, "string_bold"

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2080
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v7, 0x15

    if-ge v6, v7, :cond_1

    .line 2081
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    const-string/jumbo v8, "snote_popup_option_btn_left_normal"

    const-string/jumbo v9, "snote_popup_option_btn_left_press_1"

    .line 2082
    const-string/jumbo v10, "snote_popup_option_btn_left_focus"

    .line 2081
    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2088
    :goto_1
    const/4 v0, 0x0

    .line 2089
    .local v0, "checkCanvasWidth":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v6, v7, :cond_2

    .line 2090
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2094
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    if-nez v6, :cond_3

    const/16 v6, 0x300

    if-ne v0, v6, :cond_3

    .line 2095
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v8, "snote_popup_textoption_bold"

    const/16 v9, 0x1c

    const/16 v10, 0x1c

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2099
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2100
    new-instance v6, Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    .line 2102
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_4

    .line 2103
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e00000    # 28.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2104
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41f80000    # 31.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2103
    invoke-direct {v4, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2109
    .local v4, "mItalicBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v6, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2110
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2111
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v8, "string_italic"

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2113
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v7, 0x15

    if-ge v6, v7, :cond_5

    .line 2114
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    const-string/jumbo v8, "snote_popup_option_btn_center_normal"

    const-string/jumbo v9, "snote_popup_option_btn_center_press"

    .line 2115
    const-string/jumbo v10, "snote_popup_option_btn_center_focus"

    .line 2114
    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2121
    :goto_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    if-nez v6, :cond_6

    const/16 v6, 0x300

    if-ne v0, v6, :cond_6

    .line 2122
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v8, "snote_popup_textoption_italic"

    const/16 v9, 0x1c

    const/16 v10, 0x1c

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2126
    :goto_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2128
    new-instance v6, Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    .line 2130
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_7

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_7

    .line 2131
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e00000    # 28.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2132
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41f80000    # 31.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2131
    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2137
    .local v5, "mUnderlineBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_7
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v6, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2138
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 2139
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v8, "string_underline"

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2143
    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v7, 0x15

    if-ge v6, v7, :cond_8

    .line 2144
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    const-string/jumbo v8, "snote_popup_option_btn_right_normal"

    const-string/jumbo v9, "snote_popup_option_btn_right_press_1"

    .line 2145
    const-string/jumbo v10, "snote_popup_option_btn_right_focus"

    .line 2144
    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2151
    :goto_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    if-nez v6, :cond_9

    const/16 v6, 0x300

    if-ne v0, v6, :cond_9

    .line 2152
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v8, "snote_popup_textoption_underline"

    const/16 v9, 0x1c

    const/16 v10, 0x1c

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2156
    :goto_9
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2158
    return-object v1

    .line 2072
    .end local v0    # "checkCanvasWidth":I
    .end local v3    # "mBoldBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "mItalicBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v5    # "mUnderlineBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e00000    # 28.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2073
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41e80000    # 29.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2072
    invoke-direct {v3, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .restart local v3    # "mBoldBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    goto/16 :goto_0

    .line 2084
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    new-instance v7, Landroid/graphics/drawable/RippleDrawable;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v9, 0x3d

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v8

    .line 2085
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v10, "snote_popup_option_btn_left_normal"

    const-string/jumbo v11, "snote_popup_option_btn_left_normal"

    const-string/jumbo v12, "snote_popup_option_btn_left_focus"

    .line 2086
    const-string/jumbo v13, "snote_popup_option_btn_left_press_1"

    .line 2085
    invoke-virtual {v9, v10, v11, v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v9

    .line 2086
    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2084
    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 2092
    .restart local v0    # "checkCanvasWidth":I
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    goto/16 :goto_2

    .line 2097
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v8, "snote_popup_textoption_bold"

    const/16 v9, 0x14

    const/16 v10, 0x14

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 2106
    :cond_4
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e00000    # 28.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2107
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41e80000    # 29.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2106
    invoke-direct {v4, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .restart local v4    # "mItalicBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    goto/16 :goto_4

    .line 2117
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    new-instance v7, Landroid/graphics/drawable/RippleDrawable;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v9, 0x3d

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v8

    .line 2118
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v10, "snote_popup_option_btn_center_normal"

    const-string/jumbo v11, "snote_popup_option_btn_center_normal"

    const-string/jumbo v12, "snote_popup_option_btn_center_focus"

    .line 2119
    const-string/jumbo v13, "snote_popup_option_btn_center_press"

    .line 2118
    invoke-virtual {v9, v10, v11, v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v9

    .line 2119
    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2117
    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 2124
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v8, "snote_popup_textoption_italic"

    const/16 v9, 0x14

    const/16 v10, 0x14

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    .line 2134
    :cond_7
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41e00000    # 28.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2135
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41e80000    # 29.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 2134
    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .restart local v5    # "mUnderlineBtnParams":Landroid/widget/LinearLayout$LayoutParams;
    goto/16 :goto_7

    .line 2147
    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    new-instance v7, Landroid/graphics/drawable/RippleDrawable;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v9, 0x3d

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v8

    .line 2148
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v10, "snote_popup_option_btn_right_normal"

    const-string/jumbo v11, "snote_popup_option_btn_right_normal"

    const-string/jumbo v12, "snote_popup_option_btn_right_focus"

    .line 2149
    const-string/jumbo v13, "snote_popup_option_btn_right_press_1"

    .line 2148
    invoke-virtual {v9, v10, v11, v12, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v9

    .line 2149
    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2147
    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_8

    .line 2154
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v8, "snote_popup_textoption_underline"

    const/16 v9, 0x14

    const/16 v10, 0x14

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_9
.end method

.method private fontTypeSpinner()Landroid/widget/TextView;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x10

    const/4 v8, 0x0

    .line 2418
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    .line 2419
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2420
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42b00000    # 88.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2419
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2421
    .local v0, "fontTypeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2423
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-ge v1, v9, :cond_0

    .line 2424
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v3, "snote_dropdown_normal"

    .line 2425
    const-string/jumbo v4, "snote_dropdown_pressed"

    const-string/jumbo v5, "snote_dropdown_focused"

    .line 2424
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2435
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 2436
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2437
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41200000    # 10.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2436
    invoke-virtual {v1, v2, v8, v3, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2439
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2440
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    invoke-static {v8, v8, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2441
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2442
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    if-lt v1, v9, :cond_2

    .line 2444
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2448
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2449
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    return-object v1

    .line 2426
    :cond_0
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 2427
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x3d

    invoke-static {v4, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 2428
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v5, "snote_dropdown_normal"

    const-string/jumbo v6, "snote_dropdown_normal"

    const-string/jumbo v7, "snote_dropdown_focused"

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    .line 2429
    invoke-direct {v2, v3, v4, v10}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2427
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2431
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v3, "snote_dropdown_normal"

    const-string/jumbo v4, "snote_dropdown_pressed"

    .line 2432
    const-string/jumbo v5, "snote_dropdown_focused"

    .line 2431
    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2446
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_1
.end method

.method private fontTypeSpinnerView()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 2397
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2398
    .local v0, "fontTypeSpinnerLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2399
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42b00000    # 88.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41d80000    # 27.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 2398
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2400
    .local v1, "fontTypeSpinnerLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2401
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2402
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->fontTypeSpinner()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2403
    return-object v0
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4059
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 4060
    .local v0, "location":[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 4062
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 4064
    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 4065
    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 4066
    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 4067
    aget v2, v0, v5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 4068
    return-object v1
.end method

.method private initColorSelecteView()V
    .locals 3

    .prologue
    .line 1787
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    if-eqz v0, :cond_0

    .line 1788
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnColorChangedListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getPreviewTextColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;I)V

    .line 1791
    :cond_0
    return-void
.end method

.method private initView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1699
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43190000    # 153.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sput v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    .line 1700
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    sput v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    .line 1701
    const/4 v0, 0x0

    .line 1702
    .local v0, "checkCanvasWidth":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_3

    .line 1703
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1708
    :goto_0
    const/16 v2, 0x640

    if-eq v0, v2, :cond_0

    .line 1709
    const/16 v2, 0x300

    if-ne v0, v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    .line 1710
    :cond_0
    const/16 v2, 0x21

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    .line 1715
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->totalLayout()V

    .line 1716
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    new-array v2, v2, [Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    .line 1717
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lt v1, v2, :cond_5

    .line 1721
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    .line 1722
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1723
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1724
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextAlignSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1725
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1726
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1727
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1729
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    .line 1730
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1731
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextIndentSettingListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1734
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->initColorSelecteView()V

    .line 1735
    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1736
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->ColorPickerSettinginit()V

    .line 1737
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 1738
    return-void

    .line 1705
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    goto/16 :goto_0

    .line 1712
    :cond_4
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    goto :goto_1

    .line 1718
    .restart local v1    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1717
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private paletteBg()Landroid/view/ViewGroup;
    .locals 11

    .prologue
    const/16 v10, 0xa

    const/4 v9, 0x1

    const v8, 0x42fa999a    # 125.3f

    const/4 v7, -0x1

    .line 2479
    new-instance v3, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2480
    .local v3, "paletteBgLayout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2483
    .local v0, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2485
    new-instance v4, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2486
    .local v4, "paletteLeft":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2487
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2486
    invoke-direct {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2488
    .local v1, "bodyLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v9, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2489
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2490
    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2491
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2492
    new-instance v5, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2493
    .local v5, "paletteRight":Landroid/widget/ImageView;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2494
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 2493
    invoke-direct {v2, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2495
    .local v2, "bodyRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v9, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2496
    const/16 v6, 0xb

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2497
    invoke-virtual {v2, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2498
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2500
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v7, "snote_popup_bg02_left"

    invoke-virtual {v6, v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2501
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v7, "snote_popup_bg02_right"

    invoke-virtual {v6, v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2503
    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2504
    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2505
    return-object v3
.end method

.method private paletteTransparent()V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v6, -0x2

    const/high16 v8, 0x41c80000    # 25.0f

    .line 2620
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    .line 2621
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2623
    .local v2, "palateParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v9, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 2624
    const/16 v4, 0xe

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2625
    const/16 v4, 0xf

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2626
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2627
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 2628
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2629
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v10, :cond_1

    .line 2643
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_0

    .line 2644
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v10}, Landroid/widget/RelativeLayout;->setImportantForAccessibility(I)V

    .line 2646
    :cond_0
    return-void

    .line 2630
    :cond_1
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    const/4 v4, 0x7

    if-lt v1, v4, :cond_2

    .line 2629
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2631
    :cond_2
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2632
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 2631
    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2633
    .local v3, "transparentLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v1

    new-instance v6, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    aput-object v6, v4, v5

    .line 2634
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41e00000    # 28.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    mul-int/2addr v4, v1

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2635
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41f00000    # 30.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    mul-int/2addr v4, v0

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2636
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2637
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    .line 2638
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->mColorContentDescritionTable:[Ljava/lang/String;

    mul-int/lit8 v6, v0, 0x7

    add-int/2addr v6, v1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2639
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v5, v0, 0x7

    add-int/2addr v5, v1

    aget-object v4, v4, v5

    invoke-virtual {v4, v9}, Landroid/view/View;->setFocusable(Z)V

    .line 2640
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->palateLayoutTranparent:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->arrImageView:[Landroid/view/View;

    mul-int/lit8 v6, v0, 0x7

    add-int/2addr v6, v1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2630
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private paragraphSetting()Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 2175
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    .line 2176
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2177
    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43300000    # 176.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 2176
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2178
    .local v0, "paragraphSettingLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2179
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2181
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->paragraphSettingLayout:Landroid/widget/LinearLayout;

    return-object v1
.end method

.method private rotatePosition()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const v13, 0x3f7d70a4    # 0.99f

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 2811
    const-string/jumbo v8, "settingui-settingText"

    const-string v9, "==== SettingText ===="

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2812
    const-string/jumbo v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "old  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2813
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2812
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2814
    const-string/jumbo v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "new  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2815
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2814
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2817
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2819
    .local v4, "r":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    aget v8, v8, v11

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 2820
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    aget v8, v8, v14

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 2821
    iget v8, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 2822
    iget v8, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 2824
    const-string/jumbo v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "view = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2826
    iget v8, v4, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    int-to-float v2, v8

    .line 2827
    .local v2, "left":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    iget v9, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v9

    int-to-float v5, v8

    .line 2828
    .local v5, "right":F
    iget v8, v4, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    int-to-float v6, v8

    .line 2829
    .local v6, "top":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, v9

    int-to-float v0, v8

    .line 2831
    .local v0, "bottom":F
    add-float v8, v2, v5

    div-float v1, v2, v8

    .line 2832
    .local v1, "hRatio":F
    add-float v8, v6, v0

    div-float v7, v6, v8

    .line 2834
    .local v7, "vRatio":F
    const-string/jumbo v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "left :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", right :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2835
    const-string/jumbo v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "top :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", bottom :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2836
    const-string/jumbo v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "hRatio = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", vRatio = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2838
    cmpl-float v8, v1, v13

    if-lez v8, :cond_2

    .line 2839
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2844
    :cond_0
    :goto_0
    cmpl-float v8, v7, v13

    if-lez v8, :cond_3

    .line 2845
    const/high16 v7, 0x3f800000    # 1.0f

    .line 2850
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2852
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    if-ge v8, v9, :cond_4

    .line 2853
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v1

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2858
    :goto_2
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    if-ge v8, v9, :cond_5

    .line 2859
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2863
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v10

    aput v9, v8, v11

    .line 2864
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v10

    aput v9, v8, v14

    .line 2866
    const-string/jumbo v8, "settingui-settingText"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "lMargin = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", tMargin = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2867
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    .line 2868
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2869
    return-void

    .line 2840
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    cmpg-float v8, v1, v12

    if-gez v8, :cond_0

    .line 2841
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2846
    :cond_3
    cmpg-float v8, v7, v12

    if-gez v8, :cond_1

    .line 2847
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 2855
    .restart local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_4
    iput v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_2

    .line 2861
    :cond_5
    iput v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_3
.end method

.method private setListener()V
    .locals 4

    .prologue
    .line 1741
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1742
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1745
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    if-eqz v1, :cond_1

    .line 1746
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColorChangeListenerText:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setColorPickerColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;)V

    .line 1748
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    if-eqz v1, :cond_2

    .line 1749
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setColorGrayScaleColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView$onColorGrayScaleChangedListener;)V

    .line 1751
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1752
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1754
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    if-eqz v1, :cond_4

    .line 1756
    const/4 v0, 0x0

    .local v0, "mTextOptButton":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_9

    .line 1765
    .end local v0    # "mTextOptButton":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 1766
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1767
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1771
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_6

    .line 1772
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1775
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_7

    .line 1776
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1779
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    if-eqz v1, :cond_8

    .line 1780
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOnScrollChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;)V

    .line 1783
    :cond_8
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetectorListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1784
    return-void

    .line 1757
    .restart local v0    # "mTextOptButton":I
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_a

    .line 1758
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    instance-of v1, v1, Landroid/widget/ImageButton;

    if-eqz v1, :cond_a

    .line 1759
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextOptButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1756
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setTextStyle(Landroid/view/View;)V
    .locals 9
    .param p1, "paramView"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3364
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    if-nez v3, :cond_0

    .line 3400
    :goto_0
    return-void

    .line 3367
    :cond_0
    const/4 v1, 0x2

    .local v1, "mStyleButton":I
    :goto_1
    if-le v1, v6, :cond_3

    .line 3393
    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->getTextStyle()C

    move-result v2

    .line 3394
    .local v2, "textType":C
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v3, :cond_2

    .line 3395
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 3396
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iput v2, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3397
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 3399
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    goto :goto_0

    .line 3368
    .end local v2    # "textType":C
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 3369
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 3370
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 3371
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 3372
    if-ne v1, v7, :cond_4

    .line 3373
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewBold(Z)V

    goto :goto_2

    .line 3374
    :cond_4
    if-ne v1, v8, :cond_5

    .line 3375
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSkewX(Z)V

    goto :goto_2

    .line 3376
    :cond_5
    if-ne v1, v6, :cond_1

    .line 3377
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewUnderLine(Z)V

    goto :goto_2

    .line 3380
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 3381
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 3382
    if-ne v1, v7, :cond_7

    .line 3383
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewBold(Z)V

    goto :goto_2

    .line 3384
    :cond_7
    if-ne v1, v8, :cond_8

    .line 3385
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSkewX(Z)V

    goto :goto_2

    .line 3386
    :cond_8
    if-ne v1, v6, :cond_1

    .line 3387
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewUnderLine(Z)V

    goto :goto_2

    .line 3367
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private textPreview()Landroid/view/View;
    .locals 9

    .prologue
    const/high16 v8, 0x41500000    # 13.0f

    const/4 v7, -0x1

    .line 2008
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2009
    .local v1, "mPreviewLayout":Landroid/widget/LinearLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 2010
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x428e0000    # 71.0f

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 2009
    invoke-direct {v3, v7, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2011
    .local v3, "previewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2012
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2013
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2014
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2016
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v5, "snote_popup_preview_bg"

    invoke-virtual {v4, v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 2017
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v4, :cond_3

    .line 2018
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v4, v5, :cond_2

    .line 2019
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 2024
    :goto_0
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v5, 0x5f0

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v5, 0x5fc

    if-ne v4, v5, :cond_1

    .line 2025
    :cond_0
    const/16 v4, 0x5a0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 2036
    :cond_1
    :goto_1
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;-><init>(Landroid/content/Context;I)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    .line 2038
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 2039
    const/16 v4, 0x11

    .line 2038
    invoke-direct {v2, v7, v7, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 2040
    .local v2, "mPreviewParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, -0x3f600000    # -5.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2041
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v4, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2042
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2043
    return-object v1

    .line 2021
    .end local v2    # "mPreviewParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto :goto_0

    .line 2028
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2030
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v4, v5, :cond_4

    .line 2031
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto :goto_1

    .line 2033
    :cond_4
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto :goto_1
.end method

.method private titleBg()Landroid/view/View;
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x1

    const/4 v11, -0x2

    const/high16 v10, 0x42f60000    # 123.0f

    const/4 v9, -0x1

    .line 1382
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1383
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1385
    new-instance v3, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1386
    .local v3, "titleLeft":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1387
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1386
    invoke-direct {v4, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1388
    .local v4, "titleLeftParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v4, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1389
    const/16 v7, 0x9

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1390
    const/16 v7, 0xa

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1392
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1394
    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    .line 1395
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1397
    .local v2, "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1399
    new-instance v5, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v5, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1400
    .local v5, "titleRight":Landroid/widget/ImageView;
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1401
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x43770000    # 247.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 1402
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v8, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 1401
    sub-int/2addr v7, v8

    .line 1400
    invoke-direct {v6, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1403
    .local v6, "titleRightParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput-boolean v12, v6, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 1404
    const/16 v7, 0xb

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1405
    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1406
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1407
    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1409
    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    .line 1410
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1412
    .local v1, "rightIndicatorParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0xb

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1413
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1415
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v8, "snote_popup_title_left"

    invoke-virtual {v7, v3, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1416
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    const-string/jumbo v9, "snote_popup_title_center"

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1417
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v8, "snote_popup_title_right"

    invoke-virtual {v7, v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1418
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    const-string/jumbo v9, "snote_popup_title_bended"

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 1420
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1421
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1422
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1423
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1424
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1425
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1426
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1427
    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/high16 v6, 0x42240000    # 41.0f

    const/4 v5, 0x0

    .line 1351
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1352
    .local v0, "layout":Landroid/widget/RelativeLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1353
    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v3, v7, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1352
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1354
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->exitButton()Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    .line 1355
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->titleBg()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1356
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->titleText()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1358
    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1359
    .local v1, "mButtonLayout":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1360
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 1359
    invoke-direct {v2, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1361
    .local v2, "mButtonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1362
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1363
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1364
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/lit8 v3, v3, 0x5

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1365
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 1366
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v1, v5, v5, v3, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1368
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1369
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1370
    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 1431
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1432
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42240000    # 41.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 1431
    invoke-direct {v1, v7, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1433
    .local v1, "titleLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->EXIT_BUTTON_TOP_MARGIN:I

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1434
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/lit8 v4, v4, 0x5

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1435
    new-instance v2, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1436
    .local v2, "titleView":Landroid/widget/TextView;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1437
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    const/high16 v5, 0x40000000    # 2.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 1438
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v4, v5, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1439
    :goto_0
    const/16 v5, 0x640

    .line 1438
    if-ne v4, v5, :cond_1

    .line 1441
    :try_start_0
    const-string v4, "/system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v4}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 1442
    .local v3, "type":Landroid/graphics/Typeface;
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1446
    .end local v3    # "type":Landroid/graphics/Typeface;
    :goto_1
    const v4, -0xa0a0b

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1447
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x41633333    # 14.2f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1453
    :goto_2
    const/16 v4, 0x13

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1454
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1455
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1456
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1457
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_text_settings"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1458
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_text_settings"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1459
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41200000    # 10.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x9

    invoke-virtual {v2, v4, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1460
    return-object v2

    .line 1439
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    .line 1443
    :catch_0
    move-exception v0

    .line 1444
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 1449
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_1
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1450
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_2
.end method

.method private totalLayout()V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v12, -0x1

    const/4 v11, -0x2

    const/high16 v10, 0x41d00000    # 26.0f

    const/high16 v9, 0x43770000    # 247.0f

    const/4 v8, 0x0

    .line 1297
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1298
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int/2addr v5, v6

    .line 1297
    invoke-direct {v2, v5, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1300
    .local v2, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1302
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setOrientation(I)V

    .line 1303
    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_0

    .line 1304
    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutDirection(I)V

    .line 1306
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    .line 1307
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    .line 1308
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->addView(Landroid/view/View;)V

    .line 1309
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->addView(Landroid/view/View;)V

    .line 1311
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1312
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    int-to-float v7, v7

    .line 1311
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 1313
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    if-eq v5, v11, :cond_1

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    if-nez v5, :cond_2

    .line 1314
    :cond_1
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 1315
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1316
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    add-int/lit8 v7, v7, 0x1a

    .line 1317
    add-int/lit8 v7, v7, -0x34

    int-to-float v7, v7

    .line 1316
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1314
    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1318
    .local v3, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1321
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v12, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1323
    .local v0, "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1326
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1327
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1328
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1326
    invoke-direct {v1, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1330
    .local v1, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v6, v6, 0xfe

    add-int/lit8 v6, v6, -0x34

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1329
    invoke-virtual {v1, v8, v5, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1332
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1333
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v6, v6, 0xfe

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    .line 1334
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1335
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 1336
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v7, v7, 0xfe

    add-int/lit8 v7, v7, 0x1a

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 1334
    invoke-direct {v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1337
    .local v4, "scrollLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v4, v8, v8, v8, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1338
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v5, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1340
    .end local v0    # "bodyBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "scrollLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1574
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v1, :cond_0

    .line 1696
    :goto_0
    return-void

    .line 1578
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    if-eqz v1, :cond_1

    .line 1579
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 1580
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    .line 1582
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    if-eqz v1, :cond_2

    .line 1583
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->dismiss()V

    .line 1584
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    .line 1587
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1588
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1589
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    .line 1590
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1591
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    .line 1592
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1593
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    .line 1594
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    .line 1595
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_7

    .line 1600
    .end local v0    # "i":I
    :cond_3
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    .line 1601
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1602
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    .line 1603
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1604
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    .line 1605
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1606
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    .line 1608
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1609
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    .line 1610
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1611
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    .line 1612
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    if-eqz v1, :cond_4

    .line 1613
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->close()V

    .line 1614
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1615
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    .line 1617
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    if-eqz v1, :cond_5

    .line 1618
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->close()V

    .line 1619
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1620
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    .line 1622
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1623
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    .line 1624
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1625
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    .line 1626
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1627
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    .line 1628
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1629
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentLeftBtn:Landroid/widget/ImageButton;

    .line 1630
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1631
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndentRightBtn:Landroid/widget/ImageButton;

    .line 1632
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1633
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    .line 1634
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1635
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    .line 1637
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1638
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    .line 1639
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1640
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    .line 1642
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1643
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteRightButton:Landroid/widget/ImageView;

    .line 1644
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1645
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteLeftButton:Landroid/widget/ImageView;

    .line 1646
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteView:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1647
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteView:Landroid/view/View;

    .line 1648
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPickerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1649
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPickerView:Landroid/view/View;

    .line 1650
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1651
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    .line 1653
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1654
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    .line 1655
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1656
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    .line 1657
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1658
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    .line 1659
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1660
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    .line 1661
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColor:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1662
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerColor:Landroid/view/View;

    .line 1663
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1664
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    .line 1666
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1667
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    .line 1668
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1669
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyBg:Landroid/view/View;

    .line 1670
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1671
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    .line 1672
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1673
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    .line 1675
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->close()V

    .line 1676
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1677
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    .line 1678
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 1679
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    .line 1680
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .line 1681
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    .line 1682
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1683
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    .line 1686
    :cond_6
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .line 1687
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .line 1688
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1689
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 1690
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1691
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 1693
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    .line 1694
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    .line 1695
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingResourceManager;->close()V

    goto/16 :goto_0

    .line 1596
    .restart local v0    # "i":I
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 1597
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aput-object v3, v1, v0

    .line 1595
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 3428
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    return-object v0
.end method

.method protected getSizeOption()I
    .locals 1

    .prologue
    .line 3697
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    return v0
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 3054
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 63
    const-string/jumbo v3, "settingui-settingText"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onConfig text "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    :try_start_0
    const-string/jumbo v3, "settingui-settingText"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "old  = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 67
    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 66
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 70
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupWhenScroll:Z

    .line 71
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 72
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDismissPopupRunnable:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 74
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_5

    .line 75
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 77
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 78
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 79
    const-string/jumbo v1, "settingui-settingText"

    const-string v2, "Resote old moveable rect "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    .line 99
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-eqz v1, :cond_1

    .line 100
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->setRotation()V

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    if-eqz v1, :cond_2

    .line 104
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->isAutoScroll:Z

    if-eqz v1, :cond_2

    .line 105
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 108
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    if-eqz v1, :cond_3

    .line 109
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-boolean v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->isAutoScroll:Z

    if-eqz v1, :cond_3

    .line 110
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->dismiss()V

    .line 114
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 119
    :goto_1
    return-void

    .line 81
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLocationOnScreen([I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 85
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_5
    :try_start_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFirstShown:Z

    if-nez v3, :cond_0

    .line 86
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    if-eqz v3, :cond_6

    :goto_2
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    .line 87
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 88
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 89
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTempMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 90
    const-string/jumbo v1, "settingui-settingText"

    const-string v2, "Resote old moveable rect "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    move v1, v2

    .line 86
    goto :goto_2

    .line 92
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLocationOnScreen([I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected onScroll(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 3771
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 3953
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    .line 3987
    :goto_0
    return-void

    .line 3957
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    if-eqz v2, :cond_1

    .line 3958
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    .line 3959
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {p0, p1, v2, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->onSizeChanged(IIII)V

    .line 3960
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->requestLayout()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3983
    :catch_0
    move-exception v0

    .line 3984
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 3965
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z

    if-nez v2, :cond_2

    .line 3966
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41d00000    # 26.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    add-int/2addr v2, v3

    .line 3967
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42240000    # 41.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 3966
    add-int/2addr v2, v3

    .line 3967
    if-le v2, p2, :cond_2

    .line 3968
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    .line 3969
    const-string/jumbo v2, "settingui-settingText"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onSizeChanged height "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3971
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42860000    # 67.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sub-int v2, p2, v2

    .line 3970
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3973
    :cond_2
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 3974
    .local v1, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$24;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$24;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3981
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 11
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 2911
    if-ne p1, p0, :cond_0

    .line 2912
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    if-eqz v4, :cond_0

    .line 2914
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    invoke-interface {v4, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;->onVisibilityChanged(I)V

    .line 2917
    :cond_0
    if-ne p1, p0, :cond_3

    .line 2918
    if-nez p2, :cond_7

    .line 2920
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2921
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 2922
    .local v1, "location":[I
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLocationInWindow([I)V

    .line 2924
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFirstShown:Z

    if-eqz v4, :cond_1

    .line 2925
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFirstShown:Z

    .line 2927
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    if-eqz v4, :cond_4

    .line 2928
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2929
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    if-eqz v4, :cond_2

    .line 2930
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->rotatePosition()V

    .line 2931
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2933
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2979
    .end local v1    # "location":[I
    :cond_3
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 2980
    return-void

    .line 2937
    .restart local v1    # "location":[I
    :cond_4
    :try_start_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    if-eqz v4, :cond_5

    .line 2938
    const/4 v4, 0x2

    new-array v2, v4, [I

    .line 2939
    .local v2, "parentLocation":[I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->getLocationInWindow([I)V

    .line 2941
    const/4 v4, 0x0

    aget v4, v1, v4

    const/4 v5, 0x0

    aget v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mLeftMargin:I

    .line 2942
    const/4 v4, 0x1

    aget v4, v1, v4

    const/4 v5, 0x1

    aget v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTopMargin:I

    .line 2944
    const/4 v4, 0x2

    new-array v3, v4, [I

    .line 2945
    .local v3, "rootLocation":[I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2946
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedCalculateMargin:Z

    .line 2949
    .end local v2    # "parentLocation":[I
    .end local v3    # "rootLocation":[I
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2951
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    aget v6, v1, v6

    const/4 v7, 0x1

    aget v7, v1, v7

    const/4 v8, 0x0

    aget v8, v1, v8

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    .line 2952
    const/4 v9, 0x1

    aget v9, v1, v9

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2951
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v4

    .line 2952
    if-nez v4, :cond_6

    .line 2953
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->checkPosition()V

    .line 2955
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_3

    .line 2956
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setClickable(Z)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2976
    .end local v1    # "location":[I
    :catch_0
    move-exception v0

    .line 2977
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 2961
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_7
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExitButton:Landroid/view/View;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2962
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    .line 2963
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRecalculateRotate:Z

    .line 2965
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    if-eqz v4, :cond_8

    .line 2966
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 2968
    :cond_8
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    if-eqz v4, :cond_9

    .line 2969
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->dismiss()V

    .line 2971
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    if-eqz v4, :cond_3

    .line 2972
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setClickable(Z)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 3858
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-nez v1, :cond_1

    .line 3875
    :cond_0
    :goto_0
    return-void

    .line 3861
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 3864
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3865
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    .line 3866
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_2

    .line 3867
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 3868
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3874
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowVisibilityChanged(I)V

    goto :goto_0

    .line 3871
    :catch_0
    move-exception v0

    .line 3872
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method protected scroll(F)V
    .locals 1
    .param p1, "scrollYPosition"    # F

    .prologue
    .line 3745
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    .line 3747
    const/4 p1, 0x0

    .line 3749
    :cond_0
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .prologue
    .line 3655
    if-eqz p1, :cond_0

    .line 3656
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    .line 3658
    :cond_0
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 0
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 3726
    if-eqz p1, :cond_0

    .line 3727
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .line 3729
    :cond_0
    return-void
.end method

.method public setColorPickerEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 4109
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    .line 4110
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    if-eqz v0, :cond_0

    .line 4111
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsColorPickerEnabled:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColorPickerEnable(Z)V

    .line 4113
    :cond_0
    return-void
.end method

.method public setColorPickerPosition(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1958
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-eqz v0, :cond_0

    .line 1959
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->movePosition(II)V

    .line 1962
    :cond_0
    return-void
.end method

.method setExpandBarPosition(I)V
    .locals 12
    .param p1, "position"    # I

    .prologue
    const/4 v11, -0x1

    const/high16 v10, 0x42aa0000    # 85.0f

    const/high16 v7, 0x42860000    # 67.0f

    const/high16 v9, 0x41d00000    # 26.0f

    const/4 v8, 0x0

    .line 3275
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    if-nez v4, :cond_a

    .line 3276
    const/16 v4, 0x44

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    .line 3280
    :goto_0
    const-string/jumbo v4, "settingui-settingText"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "setExpandBarPosition position "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3281
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-lez v4, :cond_0

    .line 3282
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 3283
    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    if-lt p1, v4, :cond_0

    .line 3284
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    sub-int/2addr v4, v5

    .line 3285
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3284
    sub-int p1, v4, v5

    .line 3287
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3289
    .local v3, "expandParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput p1, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3291
    iget v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v6, v6, 0xfe

    .line 3292
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    .line 3291
    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    if-le v4, v5, :cond_1

    .line 3293
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v5, v5, 0xfe

    .line 3294
    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    .line 3293
    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3295
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3296
    const/4 v4, -0x2

    .line 3295
    invoke-direct {v2, v11, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3298
    .local v2, "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v5, v5, 0xfe

    add-int/lit8 v5, v5, -0x1a

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3297
    invoke-virtual {v2, v8, v4, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 3300
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3303
    .end local v2    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    if-lez v4, :cond_2

    .line 3304
    iget v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 3305
    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    if-le v4, v5, :cond_2

    .line 3306
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    .line 3307
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    .line 3308
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x42240000    # 41.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    .line 3306
    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3310
    iget v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v4, :cond_2

    .line 3311
    iput v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3315
    :cond_2
    iget v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 3316
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3318
    :cond_3
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 3319
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42f00000    # 120.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3321
    :cond_4
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_5

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_6

    .line 3322
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x421c0000    # 39.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3324
    :cond_6
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_7

    .line 3325
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43060000    # 134.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3327
    :cond_7
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_8

    .line 3328
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43300000    # 176.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3331
    :cond_8
    iget v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    .line 3333
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 3334
    .local v1, "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 3335
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3339
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v5, 0x4

    if-eq v4, v5, :cond_9

    .line 3340
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3341
    .local v0, "PaletteBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iput v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 3343
    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-ltz v4, :cond_9

    .line 3344
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3347
    .end local v0    # "PaletteBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3349
    .restart local v2    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    invoke-virtual {v2, v8, v4, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 3350
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3351
    return-void

    .line 3278
    .end local v1    # "bodyParam":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "bottomLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "expandParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_a
    iput v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaOfFirstTime:I

    goto/16 :goto_0
.end method

.method public setIndicatorPosition(I)V
    .locals 11
    .param p1, "position"    # I

    .prologue
    const/4 v10, -0x1

    const/high16 v9, 0x43770000    # 247.0f

    const/high16 v8, 0x41f00000    # 30.0f

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1820
    if-gez p1, :cond_1

    .line 1821
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1822
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1823
    const/16 v1, -0x63

    if-ne p1, v1, :cond_0

    .line 1824
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    .line 1825
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFirstLongPress:Z

    .line 1860
    :goto_0
    return-void

    .line 1827
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    goto :goto_0

    .line 1830
    :cond_1
    const/16 v1, 0x9

    if-ge p1, v1, :cond_2

    .line 1831
    const/16 p1, 0x9

    .line 1833
    :cond_2
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z

    .line 1834
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1835
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v1

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_4

    .line 1836
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1837
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x9

    if-le p1, v1, :cond_3

    .line 1838
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1839
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1841
    :cond_3
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1842
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    .line 1841
    invoke-direct {v0, v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1843
    .local v0, "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1844
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1845
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1848
    .end local v0    # "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, -0x2d

    add-int/lit8 v1, v1, -0x9

    if-le p1, v1, :cond_5

    .line 1849
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1850
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1852
    :cond_5
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1853
    const/4 v1, -0x2

    .line 1852
    invoke-direct {v0, v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1854
    .restart local v0    # "titleCenterParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p1, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1855
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1856
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mRightIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 12
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 3463
    if-nez p1, :cond_0

    .line 3633
    :goto_0
    return-void

    .line 3467
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 3468
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 3469
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3470
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3471
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3472
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 3473
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 3474
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 3475
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v7, :cond_1

    .line 3476
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v7}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 3477
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 3478
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    .line 3479
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3480
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3481
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    .line 3482
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    .line 3483
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    .line 3484
    iget v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v7, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    .line 3485
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v7, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 3488
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextColor(I)V

    .line 3489
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSize(F)V

    .line 3490
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v8}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    .line 3491
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->setColorPickerColor(I)V

    .line 3493
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v7, :cond_9

    .line 3494
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v8, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v7, v8, :cond_8

    .line 3495
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 3500
    :goto_1
    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    if-eqz v7, :cond_2

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v8, 0x5f0

    if-eq v7, v8, :cond_2

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v8, 0x5fc

    if-ne v7, v8, :cond_3

    .line 3501
    :cond_2
    const/16 v7, 0x5a0

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 3507
    :cond_3
    :goto_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v7}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    .line 3508
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    if-eqz v7, :cond_d

    .line 3509
    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v8, 0x10

    if-ge v7, v8, :cond_c

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getWidth()I

    move-result v7

    if-lez v7, :cond_c

    .line 3510
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    .line 3511
    .local v5, "mTempFontName":Ljava/lang/String;
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 3513
    .local v3, "mPaint":Landroid/graphics/Paint;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 3514
    .local v6, "mTextWidth":I
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getWidth()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42040000    # 33.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    sub-int/2addr v7, v8

    if-lt v6, v7, :cond_b

    .line 3515
    :goto_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getWidth()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x42040000    # 33.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    sub-int/2addr v7, v8

    if-gt v6, v7, :cond_a

    .line 3520
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3535
    .end local v3    # "mPaint":Landroid/graphics/Paint;
    .end local v5    # "mTempFontName":Ljava/lang/String;
    .end local v6    # "mTextWidth":I
    :goto_4
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v8}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3537
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v8, v8

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    double-to-float v8, v8

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    if-gtz v7, :cond_f

    .line 3538
    const-string v7, ""

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    .line 3543
    :goto_5
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3545
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    if-eqz v7, :cond_4

    .line 3546
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-nez v7, :cond_10

    .line 3547
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3548
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3549
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3561
    :cond_4
    :goto_6
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    .line 3563
    sget-boolean v7, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    if-eqz v7, :cond_6

    .line 3564
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    if-nez v7, :cond_5

    .line 3565
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    .line 3566
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3567
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGrayScaleView:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3568
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    .line 3569
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v9, 0x430c0000    # 140.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    .line 3568
    invoke-direct {v2, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3570
    .local v2, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3571
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v8, v8, 0xfe

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3573
    .end local v2    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3574
    const/high16 v9, 0x41a00000    # 20.0f

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-double v10, v9

    .line 3573
    invoke-virtual {v7, v8, v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->selectColorForGrayScale(ID)V

    .line 3575
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->getGrayCursoVisibility()Z

    move-result v7

    if-nez v7, :cond_12

    .line 3576
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 3577
    const/high16 v9, 0x41a00000    # 20.0f

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-double v10, v9

    .line 3576
    invoke-virtual {v7, v8, v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->selectColorForGradiation(ID)V

    .line 3583
    :cond_6
    :goto_7
    const/4 v4, 0x2

    .local v4, "mStyleButton":I
    :goto_8
    const/4 v7, 0x4

    if-le v4, v7, :cond_13

    .line 3624
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    if-eqz v7, :cond_7

    .line 3625
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_9
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7}, Landroid/widget/Spinner;->getCount()I

    move-result v7

    if-lt v0, v7, :cond_17

    .line 3632
    .end local v0    # "i":I
    :cond_7
    :goto_a
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    goto/16 :goto_0

    .line 3497
    .end local v4    # "mStyleButton":I
    :cond_8
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto/16 :goto_1

    .line 3504
    :cond_9
    const/16 v7, 0x5a0

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto/16 :goto_2

    .line 3516
    .restart local v3    # "mPaint":Landroid/graphics/Paint;
    .restart local v5    # "mTempFontName":Ljava/lang/String;
    .restart local v6    # "mTextWidth":I
    :cond_a
    const/4 v7, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 3517
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    .line 3518
    const-string v9, "..."

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v9

    add-float/2addr v8, v9

    .line 3517
    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    goto/16 :goto_3

    .line 3522
    :cond_b
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 3525
    .end local v3    # "mPaint":Landroid/graphics/Paint;
    .end local v5    # "mTempFontName":Ljava/lang/String;
    .end local v6    # "mTextWidth":I
    :cond_c
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 3529
    :cond_d
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    if-nez v7, :cond_e

    .line 3530
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 3532
    :cond_e
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 3540
    :cond_f
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v8, v8

    const-wide v10, 0x4076800000000000L    # 360.0

    div-double/2addr v8, v10

    double-to-float v8, v8

    div-float/2addr v7, v8

    const/high16 v8, 0x3f000000    # 0.5f

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    goto/16 :goto_5

    .line 3550
    :cond_10
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_11

    .line 3551
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3552
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3553
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_6

    .line 3554
    :cond_11
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    .line 3555
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignLeftBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3556
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignCenterBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3557
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mAlignRightBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_6

    .line 3579
    :cond_12
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setGradientCursorRectVisibility(Z)V

    goto/16 :goto_7

    .line 3584
    .restart local v4    # "mStyleButton":I
    :cond_13
    packed-switch v4, :pswitch_data_0

    .line 3583
    :goto_b
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_8

    .line 3586
    :pswitch_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v7, v7, 0x1

    const/4 v8, 0x1

    if-ne v7, v8, :cond_14

    .line 3587
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3588
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3589
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewBold(Z)V

    goto :goto_b

    .line 3591
    :cond_14
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3592
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3593
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewBold(Z)V

    goto :goto_b

    .line 3598
    :pswitch_1
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v7, v7, 0x2

    const/4 v8, 0x2

    if-ne v7, v8, :cond_15

    .line 3599
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3600
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3601
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSkewX(Z)V

    goto :goto_b

    .line 3603
    :cond_15
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3604
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3605
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSkewX(Z)V

    goto :goto_b

    .line 3610
    :pswitch_2
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v7, v7, 0x4

    const/4 v8, 0x4

    if-ne v7, v8, :cond_16

    .line 3611
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3612
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3613
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewUnderLine(Z)V

    goto/16 :goto_b

    .line 3615
    :cond_16
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSizeButtonView:[Landroid/view/View;

    aget-object v7, v7, v4

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSelected(Z)V

    .line 3616
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 3617
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewUnderLine(Z)V

    goto/16 :goto_b

    .line 3626
    .restart local v0    # "i":I
    :cond_17
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v8, v7, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7, v0}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    int-to-float v7, v7

    cmpl-float v7, v8, v7

    if-nez v7, :cond_18

    .line 3627
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_a

    .line 3625
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_9

    .line 3584
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setPosition(II)V
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v8, 0x0

    .line 1895
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mNeedRotateWhenSetPosition:Z

    .line 1896
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->runnableRotate:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1897
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->handlerRotate:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->runnableRotate:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1899
    const-string/jumbo v3, "settingui-settingText"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "set Position x,y : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1901
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1903
    .local v2, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43770000    # 247.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->RIGHT_INDICATOR_WIDTH:I

    add-int v1, v3, v4

    .line 1905
    .local v1, "minWidth":I
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v1

    if-le p1, v3, :cond_0

    .line 1906
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v1

    add-int/lit8 p1, v3, -0x2

    .line 1908
    :cond_0
    if-gez p1, :cond_1

    .line 1909
    const/4 p1, 0x0

    .line 1912
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    sub-int/2addr v3, v4

    if-le p2, v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    if-le v3, v4, :cond_2

    .line 1913
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    sub-int p2, v3, v4

    .line 1915
    :cond_2
    if-ltz p2, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    if-gt v3, v4, :cond_4

    .line 1916
    :cond_3
    const/4 p2, 0x0

    .line 1919
    :cond_4
    iput p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1920
    iput p2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1921
    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentTopMargin:I

    .line 1922
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1923
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1924
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, p1

    aput v4, v3, v8

    .line 1925
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, p2

    aput v5, v3, v4

    .line 1927
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    if-eqz v3, :cond_5

    .line 1928
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->updatePosition()V

    .line 1930
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    if-eqz v3, :cond_6

    .line 1931
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->updatePosition()V

    .line 1933
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1934
    .local v0, "bottomParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 1935
    return-void
.end method

.method public setTextPreview(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 4124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    if-eqz v0, :cond_0

    .line 4125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setTextPreview(Ljava/lang/String;)V

    .line 4126
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    .line 4128
    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 12
    .param p1, "viewMode"    # I

    .prologue
    const/high16 v11, 0x41d00000    # 26.0f

    const/4 v10, 0x1

    const/high16 v9, 0x42240000    # 41.0f

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 3084
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsFirstShown:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    .line 3085
    const-string/jumbo v4, "settingui-settingText"

    const-string v5, "SetViewMode failed, please setVisibility before calling this API!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3272
    :goto_0
    return-void

    .line 3088
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    .line 3089
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->isShown()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3090
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    .line 3091
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v4, :cond_1

    .line 3092
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v5, 0x2

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 3093
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    invoke-interface {v4, v10, v5}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 3096
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v4, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setScrollingEnabled(Z)V

    .line 3097
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    .line 3098
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    packed-switch v4, :pswitch_data_0

    .line 3247
    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    .line 3248
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3249
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3250
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3251
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3252
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3253
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->invalidate()V

    .line 3254
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3255
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3256
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3258
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3259
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3260
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3261
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    .line 3264
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    if-eqz v4, :cond_3

    .line 3265
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mViewMode:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_7

    .line 3266
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v10}, Landroid/widget/Spinner;->setClickable(Z)V

    .line 3271
    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->checkPosition()V

    goto/16 :goto_0

    .line 3100
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3101
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3102
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3103
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3104
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3105
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->invalidate()V

    .line 3106
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3107
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3108
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3110
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3112
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3113
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3114
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3115
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x437e0000    # 254.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3114
    add-int v2, v4, v5

    .line 3117
    .local v2, "mModeNormalHeigh":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    if-ne v4, v5, :cond_4

    .line 3118
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->deltaPreview:I

    add-int/lit16 v5, v5, 0xfe

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    .line 3120
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    if-lez v4, :cond_5

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    if-le v4, v5, :cond_5

    .line 3121
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    .line 3122
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3121
    sub-int/2addr v4, v5

    .line 3123
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3121
    sub-int v2, v4, v5

    .line 3125
    :cond_5
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3126
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isFirstTime:Z

    if-nez v4, :cond_6

    .line 3127
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    .line 3128
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x42900000    # 72.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3127
    invoke-direct {v0, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3129
    .local v0, "colorPickerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3134
    .end local v0    # "colorPickerParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_3
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeightNormal:I

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_1

    .line 3131
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3132
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    goto :goto_3

    .line 3138
    .end local v2    # "mModeNormalHeigh":I
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3139
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3140
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3141
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3142
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3143
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3144
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3145
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3147
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3148
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3149
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3150
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3151
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3152
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42f00000    # 120.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3153
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x433b0000    # 187.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_1

    .line 3156
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3157
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3158
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3159
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3160
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3161
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3162
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3163
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3165
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3166
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3167
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3168
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3169
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3170
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x421c0000    # 39.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3171
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42d40000    # 106.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_1

    .line 3174
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3175
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3176
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3177
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3178
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3179
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3180
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3181
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3183
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3184
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3185
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3186
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3187
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3188
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x421c0000    # 39.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3189
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42d40000    # 106.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_1

    .line 3192
    :pswitch_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3193
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3194
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3195
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3196
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3197
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3198
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3200
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3201
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3202
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3203
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3204
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3205
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3206
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43020000    # 130.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3207
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    .line 3208
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x430c0000    # 140.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    .line 3207
    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3209
    .local v1, "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3210
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3211
    .local v3, "palletBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x430e0000    # 142.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 3212
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3213
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43490000    # 201.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_1

    .line 3216
    .end local v1    # "localLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "palletBgParam":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_5
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->addParagraphSettingLayout()V

    .line 3217
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3218
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextSettingPreview:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3219
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerOptButton:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3220
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeSpinnerView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3221
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeSpinnerView:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3222
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBoldBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3223
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mItalicBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3224
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mUnderlineBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3226
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorSelectedAndPicker:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3227
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    .line 3228
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorGrayScalseView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGrayScalseView;->setVisibility(I)V

    .line 3229
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3230
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mParagraphSetting:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3231
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setScrollingEnabled(Z)V

    .line 3232
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43300000    # 176.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 3233
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43730000    # 243.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    sput v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I

    goto/16 :goto_1

    .line 3237
    :pswitch_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3238
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 3239
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    if-lez v4, :cond_2

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBodyLayoutHeight:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    if-le v4, v5, :cond_2

    .line 3240
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    .line 3241
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    .line 3242
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    sub-int/2addr v4, v5

    .line 3240
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    .line 3268
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontLineSpaceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setClickable(Z)V

    goto/16 :goto_2

    .line 3098
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setVisibility(I)V
    .locals 8
    .param p1, "visibility"    # I

    .prologue
    const-wide v6, 0x4076800000000000L    # 360.0

    .line 3788
    const/4 v1, 0x1

    .line 3789
    .local v1, "mIsInit":Z
    if-nez p1, :cond_2

    .line 3790
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v2, :cond_2

    .line 3791
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    .line 3793
    if-eqz v1, :cond_2

    .line 3794
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    if-eqz v2, :cond_4

    .line 3795
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_3

    .line 3796
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 3800
    :goto_0
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v3, 0x5f0

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    const/16 v3, 0x5fc

    if-ne v2, v3, :cond_1

    .line 3801
    :cond_0
    const-string/jumbo v2, "settingui-settingText"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "canvas size: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3802
    const/16 v2, 0x5a0

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    .line 3807
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    .line 3808
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 3809
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3818
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3820
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v4, v3

    div-double/2addr v4, v6

    double-to-float v3, v4

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    if-gtz v2, :cond_7

    .line 3821
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    .line 3827
    :goto_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3829
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSize(F)V

    .line 3830
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    .line 3831
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    .line 3835
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    if-nez v2, :cond_8

    .line 3849
    :goto_4
    return-void

    .line 3798
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3846
    :catch_0
    move-exception v0

    .line 3847
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_4

    .line 3805
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_4
    const/16 v2, 0x5a0

    :try_start_1
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto :goto_1

    .line 3811
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 3812
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 3814
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 3823
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 3824
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v4, v3

    div-double/2addr v4, v6

    double-to-float v3, v4

    div-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v2, v3

    .line 3823
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;

    goto :goto_3

    .line 3838
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 3839
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    .line 3840
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_9

    .line 3841
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v3, 0x2

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 3842
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v3, 0x1

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    .line 3845
    :cond_9
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method public setVisibilityChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .prologue
    .line 3679
    if-eqz p1, :cond_0

    .line 3680
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ViewListener;

    .line 3682
    :cond_0
    return-void
.end method

.method protected textSettingScroll()Landroid/view/View;
    .locals 12

    .prologue
    const/high16 v11, 0x40f00000    # 7.5f

    const/4 v10, 0x0

    const/4 v9, -0x1

    const/high16 v8, 0x40400000    # 3.0f

    .line 2994
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;-><init>(Landroid/content/Context;)V

    .line 2995
    .local v3, "localThumbControlBackGround":Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40800000    # 4.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setTrackWidth(I)V

    .line 2996
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setTopPadding(I)V

    .line 2997
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2998
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41c80000    # 25.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 2997
    invoke-direct {v0, v4, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2999
    .local v0, "localLayoutParams1":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 3000
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41000000    # 8.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 3001
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3002
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 3003
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    .line 3004
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    .line 3002
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->setPadding(IIII)V

    .line 3006
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOptionBgPath:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    .line 3007
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 3008
    .local v2, "localRelativeLayout":Landroid/widget/RelativeLayout;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3009
    invoke-direct {v4, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3008
    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3010
    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    .line 3011
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 3012
    const/4 v4, -0x2

    .line 3011
    invoke-direct {v1, v9, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3013
    .local v1, "localLayoutParams2":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3014
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 3015
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v4, v10, v5, v10, v10}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 3016
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandelNormal:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3017
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollHandle:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 3018
    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->addView(Landroid/view/View;)V

    .line 3019
    return-object v3
.end method
