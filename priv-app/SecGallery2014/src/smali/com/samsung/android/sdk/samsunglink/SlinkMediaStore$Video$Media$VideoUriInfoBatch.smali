.class public Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VideoUriInfoBatch"
.end annotation


# instance fields
.field private final mHttpProxyUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

.field private final mLocalUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

.field private final mSameAccessPointUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

.field private final mScsUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;)V
    .locals 0
    .param p1, "httpProxyUris"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .param p2, "sameAccessPointUris"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .param p3, "scsUris"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .param p4, "localUris"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    .prologue
    .line 1919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1921
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->mHttpProxyUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    .line 1922
    iput-object p2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->mSameAccessPointUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    .line 1923
    iput-object p3, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->mScsUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    .line 1924
    iput-object p4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->mLocalUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    .line 1925
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .param p2, "x1"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .param p3, "x2"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .param p4, "x3"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .param p5, "x4"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$1;

    .prologue
    .line 1908
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;-><init>(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;)V

    return-void
.end method


# virtual methods
.method public getHttpProxyUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .locals 1

    .prologue
    .line 1932
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->mHttpProxyUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    return-object v0
.end method

.method public getLocalUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .locals 1

    .prologue
    .line 1951
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->mLocalUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    return-object v0
.end method

.method public getSameAccessPointUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .locals 1

    .prologue
    .line 1939
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->mSameAccessPointUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    return-object v0
.end method

.method public getScsUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;
    .locals 1

    .prologue
    .line 1947
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->mScsUris:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    return-object v0
.end method
