.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;
.super Ljava/lang/Object;
.source "SpenTextBox.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 6535
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;)V
    .locals 0

    .prologue
    .line 6535
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 6538
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 6545
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 14
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 6550
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 6626
    :cond_0
    :goto_0
    return-void

    .line 6554
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 6555
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 6558
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6560
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v9

    if-lez v9, :cond_9

    .line 6561
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 6564
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v7

    .line 6565
    .local v7, "str":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 6569
    const/4 v9, 0x2

    new-array v4, v9, [F

    const/4 v9, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    aput v10, v4, v9

    const/4 v9, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    aput v10, v4, v9

    .line 6570
    .local v4, "point":[F
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6571
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6573
    const/4 v9, 0x1

    aget v10, v4, v9

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J
    invoke-static {v12}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)J

    move-result-wide v12

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F
    invoke-static {v11, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;J)F

    move-result v11

    add-float/2addr v10, v11

    aput v10, v4, v9

    .line 6575
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v10, 0x1

    aget v10, v4, v10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I

    move-result v1

    .line 6576
    .local v1, "line":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v10, 0x0

    aget v10, v4, v10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getOffsetForHorizontal(IF)I
    invoke-static {v9, v1, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I

    move-result v2

    .line 6578
    .local v2, "offset":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I

    move-result-object v9

    if-eqz v9, :cond_0

    .line 6582
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I

    move-result-object v9

    aget v9, v9, v1

    if-ne v2, v9, :cond_3

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;

    move-result-object v9

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/RectF;->right:F

    const/4 v10, 0x0

    aget v10, v4, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_3

    .line 6583
    add-int/lit8 v2, v2, 0x1

    .line 6586
    :cond_3
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-lt v2, v9, :cond_6

    add-int/lit8 v9, v2, -0x1

    invoke-virtual {v7, v9}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 6588
    .local v3, "oneChar":C
    :goto_1
    const/16 v9, 0x20

    if-eq v3, v9, :cond_4

    const/16 v9, 0x9

    if-eq v3, v9, :cond_4

    const/16 v9, 0xa

    if-eq v3, v9, :cond_4

    const/16 v9, 0xd

    if-ne v3, v9, :cond_7

    .line 6589
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v10, 0x0

    invoke-virtual {v9, v2, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6591
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v9, v2, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    .line 6593
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6618
    :cond_5
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    goto/16 :goto_0

    .line 6586
    .end local v3    # "oneChar":C
    :cond_6
    invoke-virtual {v7, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    goto :goto_1

    .line 6595
    .restart local v3    # "oneChar":C
    :cond_7
    new-instance v8, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    .line 6596
    .local v8, "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    const/4 v6, 0x0

    .local v6, "start":I
    const/4 v0, 0x0

    .line 6597
    .local v0, "end":I
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    invoke-static {v9, v2, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 6598
    iget v6, v8, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    .line 6599
    iget v0, v8, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    .line 6601
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v10, 0x0

    invoke-virtual {v9, v6, v0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    .line 6602
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    .line 6605
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v9

    const/16 v10, 0x16

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->performHapticFeedback(I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 6606
    :catch_0
    move-exception v5

    .line 6607
    .local v5, "se":Ljava/lang/SecurityException;
    const-string v9, "SpenTextBox"

    const-string v10, "Vibrator is disabled in this model"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 6610
    .end local v5    # "se":Ljava/lang/SecurityException;
    :cond_8
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v10, 0x0

    invoke-virtual {v9, v2, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6611
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6612
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v9

    if-ne v2, v9, :cond_5

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 6613
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v9, v2, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    goto :goto_2

    .line 6620
    .end local v0    # "end":I
    .end local v1    # "line":I
    .end local v2    # "offset":I
    .end local v3    # "oneChar":C
    .end local v4    # "point":[F
    .end local v6    # "start":I
    .end local v7    # "str":Ljava/lang/String;
    .end local v8    # "word":Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;
    :cond_9
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6621
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 6622
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    .line 6624
    :cond_a
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 6632
    if-eqz p1, :cond_0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 6642
    :goto_0
    return v2

    .line 6636
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)F

    move-result v0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    .line 6637
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCanvasScroll:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)F

    move-result v1

    add-float/2addr v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->scrollTextbox(F)V

    .line 6638
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$47(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)V

    goto :goto_0

    .line 6640
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->scrollTextbox(F)V

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 6647
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6651
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 6685
    :cond_0
    :goto_0
    return v3

    .line 6654
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 6658
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v5

    if-lez v5, :cond_4

    .line 6659
    const/4 v5, 0x2

    new-array v2, v5, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    aput v5, v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    aput v5, v2, v4

    .line 6660
    .local v2, "point":[F
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6661
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 6663
    aget v5, v2, v4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:J
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)J

    move-result-wide v8

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->Native_getPan(J)F
    invoke-static {v6, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;J)F

    move-result v6

    add-float/2addr v5, v6

    aput v5, v2, v4

    .line 6665
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueTopButtonDown(Landroid/view/MotionEvent;)Z
    invoke-static {v5, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueBottomButtonDown(Landroid/view/MotionEvent;)Z
    invoke-static {v5, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 6666
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 6667
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    move v3, v4

    .line 6668
    goto :goto_0

    .line 6672
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v6, v2, v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I

    move-result v1

    .line 6673
    .local v1, "line":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v6, v2, v3

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I
    invoke-static {v5, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I

    move-result v0

    .line 6675
    .local v0, "index":I
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v5, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    .line 6677
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6679
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    .line 6682
    .end local v0    # "index":I
    .end local v1    # "line":I
    .end local v2    # "point":[F
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6683
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    move v3, v4

    .line 6685
    goto/16 :goto_0
.end method
