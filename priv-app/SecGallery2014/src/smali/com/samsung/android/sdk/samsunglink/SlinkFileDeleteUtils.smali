.class public Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;
.super Ljava/lang/Object;
.source "SlinkFileDeleteUtils.java"


# static fields
.field public static final ACTION_DELETE_MODAL:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filedelete.DeleteModal"

.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;->context:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 35
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 37
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;

    if-nez v0, :cond_1

    .line 38
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;

    .line 40
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileDeleteUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public createModalDeleteActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)Landroid/content/Intent;
    .locals 2
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filedelete.DeleteModal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->writeToIntent(Landroid/content/Intent;)V

    .line 68
    return-object v0
.end method
