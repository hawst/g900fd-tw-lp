.class public Lcom/samsung/android/sdk/pen/engine/SpenMultiView;
.super Landroid/view/View;
.source "SpenMultiView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;
    }
.end annotation


# static fields
.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpenMultiView"

.field private static cancelStroke:I = 0x0

.field private static final penNameBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Brush"

.field private static final penNameChineseBrush:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field private static final penNameFountainPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.FountainPen"

.field private static final penNameInkPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.InkPen"

.field private static final penNameMagicPen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field private static final penNameMarker:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Marker"

.field private static final penNameObliquePen:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

.field private static final penNamePencil:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Pencil"

.field private static requestAllocateLayer:I

.field private static requestReleaseLayer:I

.field private static setLayerBitmap:I

.field private static setLayerCount:I


# instance fields
.field private activePen:I

.field private bIsSupport:Z

.field private isEraserCursor:Z

.field private isSkipTouch:Z

.field private localUserId:I

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCanvasHeight:I

.field private mCanvasWidth:I

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mCirclePoint:Landroid/graphics/PointF;

.field private mCircleRadius:F

.field private mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

.field private mContext:Landroid/content/Context;

.field private mDebugPaint:Landroid/graphics/Paint;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

.field private mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mFBCanvas:Landroid/graphics/Canvas;

.field private mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

.field private mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

.field private mFloatingLayerUsed:[Z

.field private mFloatingLayerUsedId:[I

.field private mFrameBuffer:Landroid/graphics/Bitmap;

.field private mFrameHeight:I

.field private mFrameStartX:I

.field private mFrameStartY:I

.field private mFrameWidth:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

.field private mHoverDrawable:Landroid/graphics/drawable/Drawable;

.field private mHoverEnable:Z

.field private mHoverIconID:I

.field private mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mHoverPoint:Landroid/graphics/Point;

.field private mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

.field private mIndexBrush:I

.field private mIndexEraser:I

.field private mIndexMarker:I

.field private mIndexPencil:I

.field private mIs64:Z

.field private mIsCancelFling:Z

.field private mIsDoubleTap:Z

.field private mIsToolTip:Z

.field private mMagicPenEnabled:Z

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mNativeMulti:J

.field private mNumberFloatingLayer:I

.field private mOldX:F

.field private mOldY:F

.field private mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

.field private mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

.field private mParentRect:Landroid/graphics/Rect;

.field private mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

.field private mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mRatio:F

.field private mRatioCanvasWidth:I

.field private mRtoCvsItstFrmHeight:I

.field private mRtoCvsItstFrmWidth:I

.field private mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

.field private mSmps:Lcom/samsung/audio/SmpsManager;

.field private mThreadId:J

.field private mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

.field private mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mTouchProcessingTime:J

.field private mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setLayerCount:I

    .line 56
    const/4 v0, 0x3

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setLayerBitmap:I

    .line 57
    const/4 v0, 0x4

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->requestAllocateLayer:I

    .line 58
    const/4 v0, 0x5

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->requestReleaseLayer:I

    .line 59
    const/4 v0, 0x6

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->cancelStroke:I

    .line 202
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 226
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 65
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 66
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    .line 68
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    .line 70
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 71
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 72
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    .line 74
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 75
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    .line 77
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    .line 79
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    .line 80
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 82
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 83
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 84
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 86
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 87
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 88
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 90
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 91
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 92
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 93
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 94
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 95
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 96
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 97
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 98
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 99
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 100
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 102
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 103
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 104
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 105
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 107
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 108
    new-instance v2, Landroid/graphics/PointF;

    const/high16 v3, -0x3d380000    # -100.0f

    const/high16 v4, -0x3d380000    # -100.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    .line 109
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 110
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 112
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 113
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 114
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 115
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 116
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 117
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    .line 118
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 119
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    .line 120
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 121
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    .line 123
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 126
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 128
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 129
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    .line 132
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    .line 133
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 134
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    .line 135
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    .line 136
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    .line 137
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    .line 138
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    .line 148
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 227
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    .line 228
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 229
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    .line 230
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    .line 231
    return-void

    :cond_0
    move v0, v1

    .line 228
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 259
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 66
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    .line 68
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    .line 70
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 71
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 72
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    .line 74
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 75
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    .line 77
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    .line 79
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    .line 80
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 82
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 83
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 84
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 86
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 87
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 88
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 90
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 91
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 92
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 93
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 94
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 95
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 96
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 97
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 98
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 99
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 100
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 102
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 103
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 104
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 105
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 107
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 108
    new-instance v2, Landroid/graphics/PointF;

    const/high16 v3, -0x3d380000    # -100.0f

    const/high16 v4, -0x3d380000    # -100.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    .line 109
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 110
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 112
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 113
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 114
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 115
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 116
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 117
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    .line 118
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 119
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    .line 120
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 121
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    .line 123
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 126
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 128
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 129
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    .line 132
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    .line 133
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 134
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    .line 135
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    .line 136
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    .line 137
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    .line 138
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    .line 148
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 260
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    .line 261
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 262
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    .line 263
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    .line 264
    return-void

    :cond_0
    move v0, v1

    .line 261
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 303
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 66
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    .line 68
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    .line 70
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 71
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 72
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    .line 74
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 75
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    .line 77
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    .line 79
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    .line 80
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 82
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 83
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 84
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 86
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 87
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 88
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 90
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 91
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 92
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 93
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 94
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 95
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 96
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 97
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 98
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 99
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 100
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 102
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 103
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 104
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 105
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 107
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 108
    new-instance v2, Landroid/graphics/PointF;

    const/high16 v3, -0x3d380000    # -100.0f

    const/high16 v4, -0x3d380000    # -100.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    .line 109
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 110
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 112
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 113
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 114
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 115
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 116
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 117
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    .line 118
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 119
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    .line 120
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 121
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    .line 123
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 126
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 128
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 129
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    .line 132
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    .line 133
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 134
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    .line 135
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    .line 136
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    .line 137
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    .line 138
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    .line 148
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 304
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    .line 305
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    .line 306
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    .line 307
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    .line 308
    return-void

    :cond_0
    move v0, v1

    .line 305
    goto :goto_0
.end method

.method private Native_addUser(JI)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3364
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3365
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_addUser(JI)Z

    move-result v0

    .line 3367
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_addUser(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "command"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3380
    .local p4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3381
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 3383
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenMultiView;
    .param p5, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 3100
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3101
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z

    move-result v0

    .line 3103
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_enablePenCurve(JIZ)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "curve"    # Z

    .prologue
    .line 3284
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3285
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enablePenCurve(JIZ)V

    .line 3289
    :goto_0
    return-void

    .line 3287
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enablePenCurve(IIZ)V

    goto :goto_0
.end method

.method private Native_enableZoom(JZ)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "mode"    # Z

    .prologue
    .line 3156
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3157
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enableZoom(JZ)V

    .line 3161
    :goto_0
    return-void

    .line 3159
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enableZoom(IZ)V

    goto :goto_0
.end method

.method private Native_finalize(J)V
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3084
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3085
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_finalize(J)V

    .line 3089
    :goto_0
    return-void

    .line 3087
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_finalize(I)V

    goto :goto_0
.end method

.method private Native_getAdvancedSetting(JI)Ljava/lang/String;
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3308
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3309
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getAdvancedSetting(JI)Ljava/lang/String;

    move-result-object v0

    .line 3311
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getAdvancedSetting(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getEraserSize(JI)F
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3324
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3325
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getEraserSize(JI)F

    move-result v0

    .line 3327
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getEraserSize(II)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getLocalUserId(J)I
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3356
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3357
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getLocalUserId(J)I

    move-result v0

    .line 3359
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getLocalUserId(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getMaxZoomRatio(J)F
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3196
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3197
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMaxZoomRatio(J)F

    move-result v0

    .line 3199
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMaxZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getMinZoomRatio(J)F
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3212
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3213
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMinZoomRatio(J)F

    move-result v0

    .line 3215
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMinZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPan(JLandroid/graphics/PointF;)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 3228
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3229
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPan(JLandroid/graphics/PointF;)V

    .line 3233
    :goto_0
    return-void

    .line 3231
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPan(ILandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method private Native_getPenColor(JI)I
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3260
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3261
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenColor(JI)I

    move-result v0

    .line 3263
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenColor(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenSize(JI)F
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3276
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3277
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenSize(JI)F

    move-result v0

    .line 3279
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenSize(II)F

    move-result v0

    goto :goto_0
.end method

.method private Native_getPenStyle(JI)Ljava/lang/String;
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3244
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3245
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenStyle(JI)Ljava/lang/String;

    move-result-object v0

    .line 3247
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenStyle(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_getToolTypeAction(JII)I
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "toolType"    # I

    .prologue
    .line 3148
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3149
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getToolTypeAction(JII)I

    move-result v0

    .line 3151
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getToolTypeAction(III)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getZoomRatio(J)F
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3180
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3181
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getZoomRatio(J)F

    move-result v0

    .line 3183
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 3076
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3077
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_init_64()J

    move-result-wide v0

    .line 3079
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_isPenCurve(JI)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3292
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3293
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isPenCurve(JI)Z

    move-result v0

    .line 3295
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isPenCurve(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_isZoomable(J)Z
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3164
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3165
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isZoomable(J)Z

    move-result v0

    .line 3167
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isZoomable(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onHover(JILandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "event"    # Landroid/view/MotionEvent;
    .param p5, "toolType"    # I

    .prologue
    .line 3116
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3117
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onHover(JILandroid/view/MotionEvent;I)Z

    move-result v0

    .line 3119
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onHover(IILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_onTouch(JILandroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "event"    # Landroid/view/MotionEvent;
    .param p5, "toolType"    # I

    .prologue
    .line 3108
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3109
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onTouch(JILandroid/view/MotionEvent;I)Z

    move-result v0

    .line 3111
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onTouch(IILandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_removeUser(JI)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3372
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3373
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_removeUser(JI)Z

    move-result v0

    .line 3375
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_removeUser(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setAdvancedSetting(JILjava/lang/String;)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "advancedSetting"    # Ljava/lang/String;

    .prologue
    .line 3300
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3301
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setAdvancedSetting(JILjava/lang/String;)V

    .line 3305
    :goto_0
    return-void

    .line 3303
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setAdvancedSetting(IILjava/lang/String;)V

    goto :goto_0
.end method

.method private Native_setBitmap(JLandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 3124
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3125
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setBitmap(JLandroid/graphics/Bitmap;)V

    .line 3129
    :goto_0
    return-void

    .line 3127
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setEraserSize(JIF)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "width"    # F

    .prologue
    .line 3316
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3317
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setEraserSize(JIF)Z

    move-result v0

    .line 3319
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setEraserSize(IIF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setLocalUserId(JI)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I

    .prologue
    .line 3348
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3349
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setLocalUserId(JI)Z

    move-result v0

    .line 3351
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setLocalUserId(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setMaxZoomRatio(JF)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "ratio"    # F

    .prologue
    .line 3188
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3189
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMaxZoomRatio(JF)Z

    move-result v0

    .line 3191
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMaxZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setMinZoomRatio(JF)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "ratio"    # F

    .prologue
    .line 3204
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3205
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMinZoomRatio(JF)Z

    move-result v0

    .line 3207
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMinZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p4, "isUpdate"    # Z

    .prologue
    .line 3132
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3133
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    .line 3135
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setPan(JFFZ)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "deltaX"    # F
    .param p4, "deltaY"    # F
    .param p5, "isUpdate"    # Z

    .prologue
    .line 3220
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3221
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPan(JFFZ)V

    .line 3225
    :goto_0
    return-void

    .line 3223
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPan(IFFZ)V

    goto :goto_0
.end method

.method private Native_setPenColor(JII)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "color"    # I

    .prologue
    .line 3252
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3253
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenColor(JII)V

    .line 3257
    :goto_0
    return-void

    .line 3255
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenColor(III)V

    goto :goto_0
.end method

.method private Native_setPenSize(JIF)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "width"    # F

    .prologue
    .line 3268
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3269
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenSize(JIF)V

    .line 3273
    :goto_0
    return-void

    .line 3271
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenSize(IIF)V

    goto :goto_0
.end method

.method private Native_setPenStyle(JILjava/lang/String;)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "style"    # Ljava/lang/String;

    .prologue
    .line 3236
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3237
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenStyle(JILjava/lang/String;)Z

    move-result v0

    .line 3239
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenStyle(IILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setScreenSize(JII)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 3092
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3093
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setScreenSize(JII)V

    .line 3097
    :goto_0
    return-void

    .line 3095
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setScreenSize(III)V

    goto :goto_0
.end method

.method private Native_setToolTypeAction(JIII)Z
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "userId"    # I
    .param p4, "toolType"    # I
    .param p5, "action"    # I

    .prologue
    .line 3140
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3141
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setToolTypeAction(JIII)Z

    move-result v0

    .line 3143
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setToolTypeAction(IIII)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setZoom(JFFF)V
    .locals 1
    .param p1, "nativeMulti"    # J
    .param p3, "centerX"    # F
    .param p4, "centerY"    # F
    .param p5, "ratio"    # F

    .prologue
    .line 3172
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3173
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setZoom(JFFF)V

    .line 3177
    :goto_0
    return-void

    .line 3175
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setZoom(IFFF)V

    goto :goto_0
.end method

.method private Native_update(J)Z
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3332
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3333
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_update(J)Z

    move-result v0

    .line 3335
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_update(I)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_updateHistory(J)Z
    .locals 1
    .param p1, "nativeMulti"    # J

    .prologue
    .line 3340
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIs64:Z

    if-eqz v0, :cond_0

    .line 3341
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_updateHistory(J)Z

    move-result v0

    .line 3343
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_updateHistory(I)Z

    move-result v0

    goto :goto_0
.end method

.method private absoluteCoordinate(Landroid/graphics/Rect;FFFF)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/Rect;
    .param p2, "l"    # F
    .param p3, "t"    # F
    .param p4, "r"    # F
    .param p5, "b"    # F

    .prologue
    .line 630
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p2, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 631
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p4, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 632
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p3, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 633
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p5, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 634
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;Z)V
    .locals 0

    .prologue
    .line 500
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 489
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas2(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    return-wide v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;JFFZ)V
    .locals 1

    .prologue
    .line 3219
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPan(JFFZ)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;Z)V
    .locals 0

    .prologue
    .line 548
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    return-object v0
.end method

.method private construct()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 311
    const-string v1, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nativeMulti = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 313
    const-string v1, " : nativeMulti must not be null"

    invoke-static {v9, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 359
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 317
    const-string v1, " : context must not be null"

    invoke-static {v9, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 321
    :cond_1
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 322
    .local v6, "rect":Landroid/graphics/RectF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 323
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 326
    :cond_2
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 327
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 328
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    const v2, -0xf2c5b1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 330
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 331
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 332
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 334
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 335
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 337
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 338
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 339
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 340
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 342
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;

    invoke-direct {v3, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 343
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;

    invoke-direct {v2, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;)V

    invoke-virtual {v1, v2}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 344
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 345
    .local v0, "mDisplayMetrics":Landroid/util/DisplayMetrics;
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    iget v3, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    .line 346
    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;-><init>(Landroid/content/Context;FF)V

    .line 345
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 347
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;

    invoke-direct {v2, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V

    .line 349
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;

    invoke-direct {v2, p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;)V

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 350
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    .line 352
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 353
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 355
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    .line 357
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->initHapticFeedback()V

    .line 358
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->registerPensoundSolution()V

    goto/16 :goto_0
.end method

.method private convertPenNameToMaxThicknessValue(Ljava/lang/String;)I
    .locals 2
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    .line 879
    const/4 v0, 0x0

    .line 881
    .local v0, "maxValue":I
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 882
    :cond_0
    const/16 v0, 0x40

    .line 893
    :cond_1
    :goto_0
    return v0

    .line 883
    :cond_2
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 884
    :cond_3
    const/16 v0, 0x20

    .line 885
    goto :goto_0

    :cond_4
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 886
    :cond_5
    const/16 v0, 0x50

    .line 887
    goto :goto_0

    :cond_6
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 888
    const/16 v0, 0x6c

    .line 889
    goto :goto_0

    :cond_7
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Eraser"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 890
    :cond_8
    const/16 v0, 0x64

    goto :goto_0
.end method

.method private createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 10
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x2

    .line 436
    if-eqz p1, :cond_0

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 487
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    .line 441
    .local v3, "width":I
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 442
    .local v2, "height":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    .line 443
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 444
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-nez v4, :cond_2

    .line 445
    const-string v4, "The width of pageDoc is 0"

    invoke-static {v9, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 448
    :cond_2
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-nez v4, :cond_3

    .line 449
    const-string v4, "The height of pageDoc is 0"

    invoke-static {v9, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 452
    :cond_3
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-ne v4, v3, :cond_4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-eq v4, v2, :cond_0

    .line 455
    :cond_4
    const-string v4, "SpenMultiView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "createBitmap Width="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Height="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_5

    .line 459
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 461
    :cond_5
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 462
    new-instance v4, Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 463
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 464
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setBitmap(JLandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    :goto_1
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_2
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lt v0, v4, :cond_6

    .line 486
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->deltaZoomSizeChanged()V

    goto/16 :goto_0

    .line 465
    .end local v0    # "cnt":I
    :catch_0
    move-exception v1

    .line 466
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "Failed to create bitmap of frame buffer"

    invoke-static {v8, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1

    .line 470
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "cnt":I
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_7

    .line 471
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 473
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    aput-object v5, v4, v0

    .line 474
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-lez v4, :cond_8

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-lez v4, :cond_8

    .line 476
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 477
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 476
    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v4, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 482
    :goto_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    invoke-direct {p0, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setLayerBitmap(ILandroid/graphics/Bitmap;)Z

    .line 469
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 478
    :catch_1
    move-exception v1

    .line 480
    .restart local v1    # "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to create bitmap of frame buffer mFloatingLayerBitmap["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 479
    invoke-static {v8, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_3
.end method

.method private deltaZoomSizeChanged()V
    .locals 5

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getVariableForOnUpdateCanvas()V

    .line 580
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onZoom(FFF)V

    .line 582
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setLimitHeight(FF)V

    .line 583
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    .line 584
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    .line 583
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setDrawInformation(IIII)V

    .line 587
    :cond_0
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onZoom. dx : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", r : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MaxDx : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 588
    const-string v2, ", MaxDy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 587
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    return-void
.end method

.method private getVariableForOnUpdateCanvas()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 557
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    .line 558
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 559
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    .line 562
    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 563
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_1

    .line 564
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    .line 567
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    .line 568
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 570
    .local v0, "mRatioCanvasHeight":I
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    if-ge v1, v2, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatioCanvasWidth:I

    :goto_0
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    .line 571
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    if-ge v0, v1, :cond_3

    .end local v0    # "mRatioCanvasHeight":I
    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    .line 573
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    .line 574
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    .line 575
    return-void

    .line 570
    .restart local v0    # "mRatioCanvasHeight":I
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    goto :goto_0

    .line 571
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    goto :goto_1
.end method

.method private initHapticFeedback()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 814
    const-string v2, "SpenMultiView"

    const-string v3, "initHapticFeedback() - Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-nez v2, :cond_0

    .line 817
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 818
    .local v0, "dm":Landroid/util/DisplayMetrics;
    new-instance v2, Lcom/samsung/hapticfeedback/HapticEffect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/hapticfeedback/HapticEffect;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 827
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    const-string v2, "SpenMultiView"

    const-string v3, "initHapticFeedback() - End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    return-void

    .line 819
    :catch_0
    move-exception v1

    .line 820
    .local v1, "error":Ljava/lang/UnsatisfiedLinkError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 821
    const-string v2, "TAG"

    const-string v3, "Haptic Effect UnsatisfiedLinkError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 822
    .end local v1    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v1

    .line 823
    .local v1, "error":Ljava/lang/NoClassDefFoundError;
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 824
    const-string v2, "TAG"

    const-string v3, "Haptic Effect NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static native native_addUser(II)Z
.end method

.method private static native native_addUser(JI)Z
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_construct(JLandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_enablePenCurve(IIZ)V
.end method

.method private static native native_enablePenCurve(JIZ)V
.end method

.method private static native native_enableZoom(IZ)V
.end method

.method private static native native_enableZoom(JZ)V
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getAdvancedSetting(II)Ljava/lang/String;
.end method

.method private static native native_getAdvancedSetting(JI)Ljava/lang/String;
.end method

.method private static native native_getEraserSize(II)F
.end method

.method private static native native_getEraserSize(JI)F
.end method

.method private static native native_getLocalUserId(I)I
.end method

.method private static native native_getLocalUserId(J)I
.end method

.method private static native native_getMaxZoomRatio(I)F
.end method

.method private static native native_getMaxZoomRatio(J)F
.end method

.method private static native native_getMinZoomRatio(I)F
.end method

.method private static native native_getMinZoomRatio(J)F
.end method

.method private static native native_getPan(ILandroid/graphics/PointF;)V
.end method

.method private static native native_getPan(JLandroid/graphics/PointF;)V
.end method

.method private static native native_getPenColor(II)I
.end method

.method private static native native_getPenColor(JI)I
.end method

.method private static native native_getPenSize(II)F
.end method

.method private static native native_getPenSize(JI)F
.end method

.method private static native native_getPenStyle(II)Ljava/lang/String;
.end method

.method private static native native_getPenStyle(JI)Ljava/lang/String;
.end method

.method private static native native_getToolTypeAction(III)I
.end method

.method private static native native_getToolTypeAction(JII)I
.end method

.method private static native native_getZoomRatio(I)F
.end method

.method private static native native_getZoomRatio(J)F
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_isPenCurve(II)Z
.end method

.method private static native native_isPenCurve(JI)Z
.end method

.method private static native native_isZoomable(I)Z
.end method

.method private static native native_isZoomable(J)Z
.end method

.method private static native native_onHover(IILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onHover(JILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(IILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(JILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_removeUser(II)Z
.end method

.method private static native native_removeUser(JI)Z
.end method

.method private static native native_setAdvancedSetting(IILjava/lang/String;)V
.end method

.method private static native native_setAdvancedSetting(JILjava/lang/String;)V
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setBitmap(JLandroid/graphics/Bitmap;)V
.end method

.method private static native native_setEraserSize(IIF)Z
.end method

.method private static native native_setEraserSize(JIF)Z
.end method

.method private static native native_setLocalUserId(II)Z
.end method

.method private static native native_setLocalUserId(JI)Z
.end method

.method private static native native_setMaxZoomRatio(IF)Z
.end method

.method private static native native_setMaxZoomRatio(JF)Z
.end method

.method private static native native_setMinZoomRatio(IF)Z
.end method

.method private static native native_setMinZoomRatio(JF)Z
.end method

.method private static native native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPan(IFFZ)V
.end method

.method private static native native_setPan(JFFZ)V
.end method

.method private static native native_setPenColor(III)V
.end method

.method private static native native_setPenColor(JII)V
.end method

.method private static native native_setPenSize(IIF)V
.end method

.method private static native native_setPenSize(JIF)V
.end method

.method private static native native_setPenStyle(IILjava/lang/String;)Z
.end method

.method private static native native_setPenStyle(JILjava/lang/String;)Z
.end method

.method private static native native_setScreenSize(III)V
.end method

.method private static native native_setScreenSize(JII)V
.end method

.method private static native native_setToolTypeAction(IIII)Z
.end method

.method private static native native_setToolTypeAction(JIII)Z
.end method

.method private static native native_setZoom(IFFF)V
.end method

.method private static native native_setZoom(JFFF)V
.end method

.method private static native native_update(I)Z
.end method

.method private static native native_update(J)Z
.end method

.method private static native native_updateHistory(I)Z
.end method

.method private static native native_updateHistory(J)Z
.end method

.method private onColorPickerChanged(III)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "color"    # I

    .prologue
    .line 606
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onColorPickerChanged color"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    invoke-interface {v0, p3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;->onChanged(III)V

    .line 610
    :cond_0
    return-void
.end method

.method private onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 4
    .param p1, "abRect"    # Landroid/graphics/RectF;
    .param p2, "isScreenFramebuffer"    # Z

    .prologue
    .line 549
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 550
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->invalidate()V

    .line 554
    :goto_0
    return-void

    .line 552
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->postInvalidate()V

    goto :goto_0
.end method

.method private onZoom(FFF)V
    .locals 1
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 594
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    .line 595
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    .line 596
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    .line 598
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->deltaZoomSizeChanged()V

    .line 600
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;->onZoom(FFF)V

    .line 603
    :cond_0
    return-void
.end method

.method private printRect(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "srcRect"    # Landroid/graphics/Rect;

    .prologue
    .line 613
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 614
    const-string v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 613
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    return-void
.end method

.method private printRect(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 618
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 619
    const-string v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 618
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    return-void
.end method

.method private registerPensoundSolution()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 840
    const-string v1, "SpenMultiView"

    const-string v2, "registerPensoundSolution() - Start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    :try_start_0
    sget-boolean v1, Lcom/samsung/audio/SmpsManager;->isSupport:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    .line 852
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-nez v1, :cond_0

    .line 853
    new-instance v1, Lcom/samsung/audio/SmpsManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/audio/SmpsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 854
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v1, :cond_0

    .line 855
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    .line 856
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    .line 857
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    .line 858
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->getPenIndex(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    .line 859
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    if-eq v1, v3, :cond_1

    .line 860
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 866
    :cond_0
    :goto_0
    const-string v1, "SpenMultiView"

    const-string v2, "registerPensoundSolution() - End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    :goto_1
    return-void

    .line 843
    :catch_0
    move-exception v0

    .line 844
    .local v0, "error":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SpenMultiView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    goto :goto_1

    .line 847
    .end local v0    # "error":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 848
    .local v0, "error":Ljava/lang/NoClassDefFoundError;
    const-string v1, "SpenMultiView"

    const-string v2, "Smps is disabled in this model"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    goto :goto_1

    .line 861
    .end local v0    # "error":Ljava/lang/NoClassDefFoundError;
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    if-eq v1, v3, :cond_0

    .line 862
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    invoke-virtual {v1, v2}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    goto :goto_0
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/Rect;

    .prologue
    .line 644
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 645
    iget v0, p2, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 646
    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 647
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 648
    return-void
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 637
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 638
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 639
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 640
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 641
    return-void
.end method

.method private releaseHapticFeedback()V
    .locals 2

    .prologue
    .line 831
    const-string v0, "SpenMultiView"

    const-string v1, "releaseHapticFeedback() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v0, :cond_0

    .line 833
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v0}, Lcom/samsung/hapticfeedback/HapticEffect;->closeDevice()V

    .line 834
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    .line 836
    :cond_0
    const-string v0, "SpenMultiView"

    const-string v1, "releaseHapticFeedback() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    return-void
.end method

.method private removeHoveringIcon(I)Z
    .locals 6
    .param p1, "nHoverIconID"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 651
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    if-nez v4, :cond_1

    .line 694
    :cond_0
    :goto_0
    return v2

    .line 655
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    if-eqz v4, :cond_0

    .line 658
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 659
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 660
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_0

    .line 661
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 669
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v4, :cond_2

    .line 670
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V

    .line 671
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->removeHoveringSpenCustomIcon(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    :cond_2
    move v2, v3

    .line 694
    goto :goto_0

    .line 673
    :catch_0
    move-exception v0

    .line 674
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 677
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 678
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 681
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v0

    .line 682
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 685
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v0

    .line 686
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 689
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 690
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "SpenMultiView"

    const-string v4, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCustomHoveringIcon(Landroid/graphics/drawable/Drawable;)I
    .locals 7
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v3, -0x1

    .line 698
    const/4 v1, -0x1

    .line 699
    .local v1, "nHoverIconID":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    if-nez v4, :cond_0

    move v3, v1

    .line 742
    :goto_0
    return v3

    .line 702
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverEnable:Z

    .line 704
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 705
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_1

    .line 706
    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    move v3, v1

    .line 707
    goto :goto_0

    :cond_1
    move v3, v1

    .line 710
    goto :goto_0

    .line 714
    :cond_2
    if-eqz p1, :cond_3

    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v4, :cond_3

    .line 715
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v4, v5, p1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(ILandroid/graphics/drawable/Drawable;Landroid/graphics/Point;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move-result v1

    :cond_3
    move v3, v1

    .line 742
    goto :goto_0

    .line 717
    :catch_0
    move-exception v0

    .line 718
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 721
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 722
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() NotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    .line 725
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_2
    move-exception v0

    .line 726
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 729
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_3
    move-exception v0

    .line 730
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 733
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v0

    .line 734
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() IllegalAccessException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 737
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v0

    .line 738
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "SpenMultiView"

    const-string v5, "setCustomHoveringIcon() InvocationTargetException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 746
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 751
    :goto_0
    return-void

    .line 749
    :cond_0
    const-string v0, "SpenMultiView"

    const-string v1, "setHoverPointerDrawable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private setLayerBitmap(ILandroid/graphics/Bitmap;)Z
    .locals 8
    .param p1, "index"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x0

    .line 2695
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    .line 2704
    :cond_0
    :goto_0
    return v0

    .line 2698
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2699
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2700
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x3

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2701
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setPenHoverPoint(Ljava/lang/String;)V
    .locals 4
    .param p1, "penname"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x3

    const/16 v2, 0x32

    .line 3046
    if-nez p1, :cond_0

    .line 3073
    :goto_0
    return-void

    .line 3049
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    if-nez v0, :cond_1

    .line 3050
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 3052
    :cond_1
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 3053
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3054
    :cond_2
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 3055
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3056
    :cond_3
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 3057
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3058
    :cond_4
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 3059
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3060
    :cond_5
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 3061
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3062
    :cond_6
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    .line 3063
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3064
    :cond_7
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 3065
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3066
    :cond_8
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 3067
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3068
    :cond_9
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    .line 3069
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 3071
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0
.end method

.method private unregisterPensoundSolution()V
    .locals 2

    .prologue
    .line 870
    const-string v0, "SpenMultiView"

    const-string/jumbo v1, "unregisterPensoundSolution() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_0

    .line 872
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    invoke-virtual {v0}, Lcom/samsung/audio/SmpsManager;->onDestroy()V

    .line 873
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    .line 875
    :cond_0
    const-string v0, "SpenMultiView"

    const-string/jumbo v1, "unregisterPensoundSolution() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    return-void
.end method

.method private updateCanvas(Landroid/graphics/Canvas;Z)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "framebufferUpdate"    # Z

    .prologue
    const/4 v12, 0x0

    const/4 v1, 0x0

    .line 502
    if-nez p1, :cond_0

    .line 546
    :goto_0
    return-void

    .line 505
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 506
    .local v10, "time":J
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_7

    .line 507
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v12, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 509
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 510
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 511
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    add-int/2addr v0, v2

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v5, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v6, v0

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 513
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 515
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 517
    :cond_2
    if-eqz p2, :cond_3

    .line 518
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v12, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v0, v12, v12, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 521
    :cond_3
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 522
    .local v3, "srcRect":Landroid/graphics/Rect;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    int-to-float v6, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    int-to-float v7, v0

    move-object v2, p0

    move v4, v1

    move v5, v1

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 523
    new-instance v9, Landroid/graphics/Rect;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-direct {v9, v12, v12, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 524
    .local v9, "dstRect":Landroid/graphics/Rect;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    invoke-virtual {v9, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 526
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 528
    const/4 v8, 0x0

    .local v8, "cnt":I
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lt v8, v0, :cond_5

    .line 539
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 540
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 545
    .end local v3    # "srcRect":Landroid/graphics/Rect;
    .end local v8    # "cnt":I
    .end local v9    # "dstRect":Landroid/graphics/Rect;
    :cond_4
    :goto_2
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Performance updateCanvas end "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v10

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 529
    .restart local v3    # "srcRect":Landroid/graphics/Rect;
    .restart local v8    # "cnt":I
    .restart local v9    # "dstRect":Landroid/graphics/Rect;
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    aget-boolean v0, v0, v8

    if-eqz v0, :cond_6

    .line 530
    const-string v0, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Multi start"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " layer width = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 531
    const-string v4, " height = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 530
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v8

    invoke-virtual {v2, v12, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v0, v12, v12, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 533
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 534
    const-string v0, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Multi end"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " layer width = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 535
    const-string v4, " height = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 534
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 543
    .end local v3    # "srcRect":Landroid/graphics/Rect;
    .end local v8    # "cnt":I
    .end local v9    # "dstRect":Landroid/graphics/Rect;
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_2
.end method

.method private updateCanvas2(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 490
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 491
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 492
    .local v1, "srcRect":Landroid/graphics/Rect;
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    int-to-float v5, v0

    move-object v0, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    .line 493
    new-instance v6, Landroid/graphics/Rect;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-direct {v6, v7, v7, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 494
    .local v6, "dstRect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 498
    .end local v1    # "srcRect":Landroid/graphics/Rect;
    .end local v6    # "dstRect":Landroid/graphics/Rect;
    :goto_0
    return-void

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public addUser(I)Z
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 1935
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1936
    const/4 v0, 0x0

    .line 1938
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_addUser(JI)Z

    move-result v0

    goto :goto_0
.end method

.method public cancelStroke(I)V
    .locals 7
    .param p1, "userId"    # I

    .prologue
    .line 2769
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2774
    :goto_0
    return-void

    .line 2773
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "includeBlank"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2566
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    move-object v0, v3

    .line 2592
    :goto_0
    return-object v0

    .line 2570
    :cond_0
    const/4 v0, 0x0

    .line 2572
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    const-string v4, "SpenMultiView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RtoCvs["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] Frame["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 2573
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2572
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2574
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    if-nez v4, :cond_2

    :cond_1
    move-object v0, v3

    .line 2575
    goto :goto_0

    .line 2577
    :cond_2
    if-nez p1, :cond_4

    .line 2578
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2586
    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2587
    .local v1, "canvas":Landroid/graphics/Canvas;
    if-nez p1, :cond_3

    .line 2588
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    neg-int v3, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2590
    :cond_3
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    .line 2591
    const/4 v1, 0x0

    .line 2592
    goto :goto_0

    .line 2580
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_4
    :try_start_1
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 2582
    :catch_0
    move-exception v2

    .line 2583
    .local v2, "e":Ljava/lang/Throwable;
    const-string v3, "SpenMultiView"

    const-string v4, "Failed to create bitmap"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2584
    const/4 v3, 0x2

    const-string v4, " : fail createBitmap."

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 2623
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 2624
    const/4 v0, 0x0

    .line 2627
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    .line 2628
    const/4 v3, 0x1

    .line 2627
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public close()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 371
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_0

    .line 372
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_finalize(J)V

    .line 373
    iput-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    .line 376
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 377
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 378
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    .line 380
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 381
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-lt v1, v4, :cond_6

    .line 389
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v1, :cond_3

    .line 390
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->close()V

    .line 391
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    .line 393
    :cond_3
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 394
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 395
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 396
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 398
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    .line 399
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    .line 400
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    .line 401
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 402
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    .line 403
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    .line 404
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    .line 405
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 406
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 408
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 409
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v1, :cond_4

    .line 410
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->close()V

    .line 411
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    .line 413
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v1, :cond_5

    .line 414
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->close()V

    .line 415
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    .line 418
    :cond_5
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 419
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .line 420
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 421
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .line 422
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 423
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 424
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 425
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 426
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 427
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .line 428
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    .line 429
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 431
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->releaseHapticFeedback()V

    .line 432
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->unregisterPensoundSolution()V

    .line 433
    return-void

    .line 381
    :cond_6
    aget-object v0, v3, v1

    .line 382
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_7

    .line 383
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 381
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public closeControl()V
    .locals 0

    .prologue
    .line 3556
    return-void
.end method

.method public getBlankColor()I
    .locals 4

    .prologue
    .line 2161
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2162
    const/4 v0, 0x0

    .line 2164
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1848
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1851
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1834
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1837
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 6

    .prologue
    .line 1496
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1497
    const/4 v0, 0x0

    .line 1505
    :cond_0
    :goto_0
    return-object v0

    .line 1499
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    .line 1500
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v0, :cond_0

    .line 1501
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getEraserSize(JI)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1502
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    goto :goto_0
.end method

.method public getEraserSettingInfo(I)Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 6
    .param p1, "userId"    # I

    .prologue
    .line 1526
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1527
    const/4 v0, 0x0

    .line 1534
    :cond_0
    :goto_0
    return-object v0

    .line 1529
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    .line 1530
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    if-eqz v0, :cond_0

    .line 1531
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getEraserSize(JI)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    goto :goto_0
.end method

.method public getLayerCount()I
    .locals 1

    .prologue
    .line 2691
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    return v0
.end method

.method public getLocalUserId()I
    .locals 4

    .prologue
    .line 1912
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1913
    const/4 v0, -0x1

    .line 1916
    :goto_0
    return v0

    .line 1915
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getLocalUserId(J)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    .line 1916
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 4

    .prologue
    .line 2039
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2040
    const/4 v0, 0x0

    .line 2042
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getMaxZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 4

    .prologue
    .line 2078
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2079
    const/4 v0, 0x0

    .line 2081
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getMinZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 6

    .prologue
    .line 2117
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2118
    const/4 v0, 0x0

    .line 2122
    :goto_0
    return-object v0

    .line 2120
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 2121
    .local v0, "point":Landroid/graphics/PointF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPan(JLandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 6

    .prologue
    .line 1346
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1347
    const/4 v0, 0x0

    .line 1359
    :cond_0
    :goto_0
    return-object v0

    .line 1349
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 1350
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v0, :cond_0

    .line 1351
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenStyle(JI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 1352
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenSize(JI)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1353
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenColor(JI)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1354
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_isPenCurve(JI)Z

    move-result v1

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 1355
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getAdvancedSetting(JI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 1356
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    goto :goto_0
.end method

.method public getPenSettingInfo(I)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 6
    .param p1, "userId"    # I

    .prologue
    .line 1380
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1381
    const/4 v0, 0x0

    .line 1392
    :cond_0
    :goto_0
    return-object v0

    .line 1383
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 1384
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v0, :cond_0

    .line 1385
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenStyle(JI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 1386
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenSize(JI)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1387
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getPenColor(JI)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1388
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_isPenCurve(JI)Z

    move-result v1

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 1389
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getAdvancedSetting(JI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 1577
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    .prologue
    .line 1620
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 1226
    const/4 v0, 0x0

    return-object v0
.end method

.method public getToolTypeAction(I)I
    .locals 6
    .param p1, "toolType"    # I

    .prologue
    .line 1777
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1778
    const/4 v0, 0x0

    .line 1784
    :cond_0
    :goto_0
    return v0

    .line 1780
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-direct {p0, v2, v3, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getToolTypeAction(JII)I

    move-result v0

    .line 1781
    .local v0, "action":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1782
    const/4 v1, 0x1

    const-string v2, "not set LocalUserId"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getToolTypeAction(II)I
    .locals 4
    .param p1, "userId"    # I
    .param p2, "toolType"    # I

    .prologue
    .line 1815
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1816
    const/4 v0, 0x0

    .line 1818
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getToolTypeAction(JII)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 4

    .prologue
    .line 2000
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2001
    const/4 v0, 0x0

    .line 2003
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getZoomRatio(J)F

    move-result v0

    goto :goto_0
.end method

.method public isDoubleTapEnabled()Z
    .locals 1

    .prologue
    .line 2870
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    return v0
.end method

.method public isToolTipEnabled()Z
    .locals 1

    .prologue
    .line 2844
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    return v0
.end method

.method public isZoomable()Z
    .locals 4

    .prologue
    .line 2812
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2813
    const/4 v0, 0x0

    .line 2816
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_isZoomable(J)Z

    move-result v0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 797
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 811
    :goto_0
    return-void

    .line 801
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 802
    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 803
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->drawAnimation(Landroid/graphics/Canvas;)V

    .line 810
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 805
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    .line 806
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_1

    .line 807
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1143
    if-eqz p1, :cond_0

    iget-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v12, 0x0

    cmp-long v1, v10, v12

    if-nez v1, :cond_1

    .line 1144
    :cond_0
    const/4 v1, 0x1

    .line 1183
    :goto_0
    return v1

    .line 1147
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v10, 0x9

    if-ne v1, v10, :cond_4

    .line 1148
    const-string v1, "SpenMultiView"

    const-string v10, "[HOVER_CONTROL] Hover Enter"

    invoke-static {v1, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 1150
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setCustomHoveringIcon(Landroid/graphics/drawable/Drawable;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    .line 1157
    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1158
    .local v0, "action":I
    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 1159
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 1160
    .local v2, "diffTime":J
    const-wide/16 v10, 0x64

    iget-wide v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    add-long/2addr v10, v12

    cmp-long v1, v2, v10

    if-lez v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 1161
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v1, :cond_3

    .line 1162
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 1163
    .local v6, "eventTime":J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 1164
    .local v4, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 1165
    .local v8, "systemTime":J
    const-string v1, "SpenMultiView"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "skiptouch hover action = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " eventTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " downTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 1166
    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " systemTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " diffTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1165
    invoke-static {v1, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1170
    .end local v2    # "diffTime":J
    .end local v4    # "downTime":J
    .end local v6    # "eventTime":J
    .end local v8    # "systemTime":J
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v1, :cond_6

    .line 1171
    const-string v1, "SpenMultiView"

    const-string/jumbo v10, "skiptouch hover"

    invoke-static {v1, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 1152
    .end local v0    # "action":I
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v10, 0xa

    if-ne v1, v10, :cond_2

    .line 1153
    const-string v1, "SpenMultiView"

    const-string v10, "[HOVER_CONTROL] Hover Exit"

    invoke-static {v1, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1154
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverIconID:I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->removeHoveringIcon(I)Z

    goto/16 :goto_1

    .line 1160
    .restart local v0    # "action":I
    .restart local v2    # "diffTime":J
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 1179
    .end local v2    # "diffTime":J
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    invoke-interface {v1, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1180
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 1183
    :cond_7
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 760
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 762
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mParentRect:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 763
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mParentRect:Landroid/graphics/Rect;

    .line 765
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 766
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_1

    .line 767
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setParentRect(Landroid/graphics/Rect;)V

    .line 769
    :cond_1
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 778
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 789
    :goto_0
    return-void

    .line 782
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    .line 783
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    .line 785
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setScreenResolution(II)V

    .line 786
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setScreenSize(JII)V

    .line 788
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    goto :goto_0
.end method

.method public onTouchEvent(ILandroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "userId"    # I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    .line 1115
    if-eqz p2, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1121
    :cond_0
    :goto_0
    return v7

    .line 1119
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_onTouch(JILandroid/view/MotionEvent;I)Z

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 28
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 942
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 943
    :cond_0
    const/4 v5, 0x1

    .line 1099
    :goto_0
    return v5

    .line 946
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    and-int/lit16 v4, v5, 0xff

    .line 947
    .local v4, "action":I
    if-nez v4, :cond_2

    .line 948
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 950
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 951
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsCancelFling:Z

    .line 953
    :cond_3
    if-nez v4, :cond_4

    .line 954
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v8

    sub-long v12, v6, v8

    .line 955
    .local v12, "diffTime":J
    const-wide/16 v6, 0x64

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    add-long/2addr v6, v8

    cmp-long v5, v12, v6

    if-lez v5, :cond_c

    const/4 v5, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    .line 956
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v5, :cond_4

    .line 957
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v16

    .line 958
    .local v16, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v14

    .line 959
    .local v14, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v22

    .line 960
    .local v22, "systemTime":J
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "skiptouch action = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " eventTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v16

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " downTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 961
    const-string v7, " systemTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v22

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " diffTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 960
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    .end local v12    # "diffTime":J
    .end local v14    # "downTime":J
    .end local v16    # "eventTime":J
    .end local v22    # "systemTime":J
    :cond_4
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    neg-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    neg-int v6, v6

    int-to-float v6, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 967
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 968
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 970
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-nez v5, :cond_1d

    .line 971
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v26

    .line 972
    .local v26, "time":J
    const-string v5, "SpenMultiView"

    const-string v6, "Performance mPreTouchListener start"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v5, v8}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getToolTypeAction(JII)I

    move-result v25

    .line 976
    .local v25, "toolTypeAction":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    .line 977
    .local v11, "newX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    .line 979
    .local v18, "newY":F
    const/4 v5, 0x2

    move/from16 v0, v25

    if-eq v0, v5, :cond_5

    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_5

    .line 980
    const/4 v5, 0x4

    move/from16 v0, v25

    if-ne v0, v5, :cond_b

    .line 981
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    if-eqz v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v5, :cond_a

    .line 982
    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_6

    const/4 v5, 0x4

    move/from16 v0, v25

    if-ne v0, v5, :cond_7

    .line 983
    :cond_6
    if-nez v4, :cond_d

    .line 984
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexEraser:I

    invoke-virtual {v5, v6}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 985
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const-string v7, "Eraser"

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    float-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 993
    :cond_7
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v21

    .line 994
    .local v21, "tempX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v24

    .line 995
    .local v24, "tempY":F
    move/from16 v19, v21

    .line 996
    .local v19, "originX":F
    move/from16 v20, v24

    .line 997
    .local v20, "originY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_8

    .line 998
    const/16 v24, 0x0

    .line 1000
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_9

    .line 1001
    const/16 v21, 0x0

    .line 1003
    :cond_9
    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1004
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/audio/SmpsManager;->generateSound(Landroid/view/MotionEvent;)V

    .line 1005
    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1008
    .end local v19    # "originX":F
    .end local v20    # "originY":F
    .end local v21    # "tempX":F
    .end local v24    # "tempY":F
    :cond_a
    if-nez v4, :cond_e

    .line 1009
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 1010
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    .line 1026
    :cond_b
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v5, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-interface {v5, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1027
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 955
    .end local v11    # "newX":F
    .end local v18    # "newY":F
    .end local v25    # "toolTypeAction":I
    .end local v26    # "time":J
    .restart local v12    # "diffTime":J
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 986
    .end local v12    # "diffTime":J
    .restart local v11    # "newX":F
    .restart local v18    # "newY":F
    .restart local v25    # "toolTypeAction":I
    .restart local v26    # "time":J
    :cond_d
    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 987
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    invoke-virtual {v5, v6}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 988
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 989
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    float-to-double v6, v6

    .line 988
    invoke-virtual {v5, v6, v7}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    goto/16 :goto_2

    .line 1011
    :cond_e
    const/4 v5, 0x2

    if-ne v4, v5, :cond_10

    .line 1013
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v5, :cond_f

    .line 1014
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    move/from16 v0, v18

    invoke-virtual {v5, v6, v7, v11, v0}, Lcom/samsung/hapticfeedback/HapticEffect;->playEffectByDistance(FFFF)V

    .line 1016
    :cond_f
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldX:F

    .line 1017
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mOldY:F

    goto :goto_3

    .line 1018
    :cond_10
    const/4 v5, 0x1

    if-ne v4, v5, :cond_b

    .line 1020
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    if-eqz v5, :cond_b

    .line 1021
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHapticEffect:Lcom/samsung/hapticfeedback/HapticEffect;

    invoke-virtual {v5}, Lcom/samsung/hapticfeedback/HapticEffect;->stopAllEffect()V

    goto :goto_3

    .line 1029
    :cond_11
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Performance mPreTouchListener.onTouch end "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    if-nez v4, :cond_16

    .line 1032
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v5, :cond_12

    .line 1033
    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_12

    const/4 v5, 0x4

    move/from16 v0, v25

    if-eq v0, v5, :cond_12

    .line 1034
    const/4 v5, 0x5

    move/from16 v0, v25

    if-eq v0, v5, :cond_12

    .line 1035
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    .line 1038
    :cond_12
    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_13

    const/4 v5, 0x4

    move/from16 v0, v25

    if-ne v0, v5, :cond_15

    .line 1039
    :cond_13
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 1040
    const/4 v5, 0x3

    move/from16 v0, v25

    if-ne v0, v5, :cond_14

    .line 1041
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getEraserSize(JI)F

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 1049
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 1050
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 1051
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1086
    :cond_15
    :goto_4
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v10

    move-object/from16 v5, p0

    move-object/from16 v9, p1

    invoke-direct/range {v5 .. v10}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_onTouch(JILandroid/view/MotionEvent;I)Z

    .line 1087
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Performance native_onTouch end "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-interface {v5, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 1090
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Performance mTouchListener.onTouch end "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1091
    const-string v7, " ms action = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1090
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 1053
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    if-eqz v5, :cond_15

    .line 1054
    const/4 v5, 0x2

    if-ne v4, v5, :cond_1a

    .line 1055
    const/4 v5, 0x3

    move/from16 v0, v25

    if-eq v0, v5, :cond_17

    const/4 v5, 0x4

    move/from16 v0, v25

    if-ne v0, v5, :cond_19

    .line 1056
    :cond_17
    const/4 v5, 0x3

    move/from16 v0, v25

    if-ne v0, v5, :cond_18

    .line 1057
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_getEraserSize(JI)F

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 1065
    :cond_18
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 1066
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 1067
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_4

    .line 1069
    :cond_19
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 1070
    const/high16 v5, -0x3d380000    # -100.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 1072
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v6, -0x3d380000    # -100.0f

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 1073
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v6, -0x3d380000    # -100.0f

    iput v6, v5, Landroid/graphics/PointF;->y:F

    goto/16 :goto_4

    .line 1076
    :cond_1a
    const/4 v5, 0x1

    if-eq v4, v5, :cond_1b

    const/4 v5, 0x3

    if-ne v4, v5, :cond_15

    .line 1077
    :cond_1b
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isEraserCursor:Z

    .line 1078
    const/high16 v5, -0x3d380000    # -100.0f

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCircleRadius:F

    .line 1080
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v6, -0x3d380000    # -100.0f

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 1081
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v6, -0x3d380000    # -100.0f

    iput v6, v5, Landroid/graphics/PointF;->y:F

    goto/16 :goto_4

    .line 1094
    :cond_1c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v26

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    .line 1095
    const-string v5, "SpenMultiView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Performance mTouchListener.onTouch end "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1096
    const-string v7, " ms action = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1095
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    .end local v11    # "newX":F
    .end local v18    # "newY":F
    .end local v25    # "toolTypeAction":I
    .end local v26    # "time":J
    :cond_1d
    const/4 v5, 0x1

    goto/16 :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 912
    const-string v0, "SpenMultiView"

    const-string v1, "onWindowFocusChanged() - Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    if-eqz p1, :cond_0

    .line 914
    const-string v0, "SpenMultiView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->registerPensoundSolution()V

    .line 920
    :goto_0
    const-string v0, "SpenMultiView"

    const-string v1, "onWindowFocusChanged() - End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    return-void

    .line 917
    :cond_0
    const-string v0, "SpenMultiView"

    const-string v1, "onWindowFocusChanged() - hasWindowFocus : false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->unregisterPensoundSolution()V

    goto :goto_0
.end method

.method public removeUser(I)Z
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 1951
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1952
    const/4 v0, 0x0

    .line 1954
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_removeUser(JI)Z

    move-result v0

    goto :goto_0
.end method

.method public requestAllocateLayer(I)Z
    .locals 10
    .param p1, "userId"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2717
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    move v1, v8

    .line 2732
    :goto_0
    return v1

    .line 2721
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2723
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v0, :cond_3

    .line 2724
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;

    .line 2725
    .local v7, "tmp":Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;
    iget v1, v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;->a:I

    if-ltz v1, :cond_1

    .line 2726
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    iget v2, v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;->a:I

    aput-boolean v9, v1, v2

    .line 2727
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    iget v2, v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;->a:I

    aput p1, v1, v2

    .line 2730
    :cond_1
    iget v1, v7, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;->a:I

    if-ltz v1, :cond_2

    move v1, v9

    goto :goto_0

    :cond_2
    move v1, v8

    goto :goto_0

    .end local v7    # "tmp":Lcom/samsung/android/sdk/pen/engine/SpenMultiView$retClass;
    :cond_3
    move v1, v8

    .line 2732
    goto :goto_0
.end method

.method public requestReleaseLayer(I)V
    .locals 8
    .param p1, "userId"    # I

    .prologue
    const/4 v7, 0x0

    .line 2744
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2757
    :cond_0
    :goto_0
    return-void

    .line 2748
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 2750
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-ge v0, v1, :cond_0

    .line 2751
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_2

    .line 2752
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    aput-boolean v7, v1, v0

    .line 2753
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    aput v7, v1, v0

    goto :goto_0

    .line 2750
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "listener"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .prologue
    .line 1861
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1876
    :cond_0
    :goto_0
    return-void

    .line 1864
    :cond_1
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v1, :cond_0

    .line 1865
    const-string v1, "SpenMultiView"

    const-string v2, "setBackgroundColorListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1866
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    .line 1868
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_0

    .line 1869
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_2

    const/4 v0, 0x1

    .line 1870
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_0

    .line 1871
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 1872
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    goto :goto_0

    .line 1869
    .end local v0    # "enable":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setBlankColor(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 2139
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2147
    :cond_0
    :goto_0
    return-void

    .line 2142
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 2143
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2144
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    .line 2145
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .prologue
    .line 2457
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2461
    :goto_0
    return-void

    .line 2460
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    goto :goto_0
.end method

.method public setDoubleTapEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2857
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setDoubleTapEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2858
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsDoubleTap:Z

    .line 2859
    return-void
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    .prologue
    .line 2537
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2541
    :goto_0
    return-void

    .line 2540
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    goto :goto_0
.end method

.method public setEraserSettingInfo(ILcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 7
    .param p1, "userId"    # I
    .param p2, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1465
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1479
    :cond_0
    :goto_0
    return-void

    .line 1469
    :cond_1
    if-eqz p2, :cond_0

    .line 1470
    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_2

    .line 1471
    iput v6, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1473
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-ge v1, v2, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 1474
    .local v0, "maxSize":I
    :goto_1
    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    int-to-float v2, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 1475
    int-to-float v1, v0

    iput v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1477
    :cond_3
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-direct {p0, v2, v3, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setEraserSize(JIF)Z

    goto :goto_0

    .line 1473
    .end local v0    # "maxSize":I
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    goto :goto_1
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 8
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x5

    .line 1415
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1441
    :cond_0
    :goto_0
    return-void

    .line 1419
    :cond_1
    if-eqz p1, :cond_4

    .line 1420
    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v1, v1, v7

    if-gez v1, :cond_2

    .line 1421
    iput v7, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1423
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-ge v1, v2, :cond_5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 1424
    .local v0, "maxSize":I
    :goto_1
    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    int-to-float v2, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 1425
    int-to-float v1, v0

    iput v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    .line 1427
    :cond_3
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setEraserSize(JIF)Z

    .line 1429
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 1430
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 1431
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 1432
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 1433
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 1434
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1438
    .end local v0    # "maxSize":I
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    if-eqz v1, :cond_0

    .line 1439
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    invoke-interface {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_0

    .line 1423
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    goto :goto_1
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    .prologue
    .line 2417
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2421
    :goto_0
    return-void

    .line 2420
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .prologue
    .line 2437
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2441
    :goto_0
    return-void

    .line 2440
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    goto :goto_0
.end method

.method public setLayerCount(I)V
    .locals 11
    .param p1, "count"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 2640
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-nez v1, :cond_1

    .line 2682
    :cond_0
    return-void

    .line 2644
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x1

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    .line 2646
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lez v1, :cond_2

    .line 2647
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lt v0, v1, :cond_5

    .line 2654
    .end local v0    # "cnt":I
    :cond_2
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 2655
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 2656
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 2658
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 2659
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-gez v1, :cond_3

    .line 2660
    iput v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    .line 2662
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-lez v1, :cond_0

    .line 2663
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    new-array v1, v1, [Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    .line 2664
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    .line 2665
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    .line 2666
    const/4 v0, 0x0

    .restart local v0    # "cnt":I
    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNumberFloatingLayer:I

    if-ge v0, v1, :cond_0

    .line 2667
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aput-object v5, v1, v0

    .line 2668
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsed:[Z

    aput-boolean v10, v1, v0

    .line 2669
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerUsedId:[I

    aput v10, v1, v0

    .line 2670
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-lez v1, :cond_4

    .line 2672
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    .line 2673
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2672
    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2678
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setLayerBitmap(ILandroid/graphics/Bitmap;)Z

    .line 2666
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2648
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2649
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2650
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFloatingLayerBitmap:[Landroid/graphics/Bitmap;

    aput-object v5, v1, v0

    .line 2647
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2674
    :catch_0
    move-exception v7

    .line 2675
    .local v7, "e":Ljava/lang/Exception;
    const/4 v1, 0x2

    .line 2676
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create bitmap of frame buffer mFloatingLayerBitmap["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2675
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public setLocalUserId(I)V
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 1892
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1898
    :cond_0
    :goto_0
    return-void

    .line 1895
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setLocalUserId(JI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1896
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 2021
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2022
    const/4 v0, 0x0

    .line 2024
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setMaxZoomRatio(JF)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 2060
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2061
    const/4 v0, 0x0

    .line 2063
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setMinZoomRatio(JF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 8
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "direction"    # I
    .param p3, "type"    # I
    .param p4, "centerY"    # F

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2257
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2290
    :cond_0
    :goto_0
    return v6

    .line 2260
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2261
    const-string v1, "SpenMultiView"

    const-string v2, "setPageDoc is closed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2264
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2268
    const-string v1, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setPageDoc, direction="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2270
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setType(I)V

    .line 2271
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setCanvasInformation(IIII)V

    .line 2272
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->saveScreenshot()Z

    .line 2274
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2276
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 2278
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_3

    .line 2279
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_4

    move v0, v7

    .line 2280
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_3

    .line 2281
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 2282
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 2285
    .end local v0    # "enable":Z
    :cond_3
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const/4 v4, 0x0

    move-object v1, p0

    move v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPan(JFFZ)V

    .line 2286
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v2, v3, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    .line 2288
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->startAnimation(I)Z

    move v6, v7

    .line 2290
    goto/16 :goto_0

    :cond_4
    move v0, v6

    .line 2279
    goto :goto_1
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 6
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "isUpdate"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2193
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 2216
    :goto_0
    return v0

    .line 2196
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2197
    const-string v1, "SpenMultiView"

    const-string v2, "setPageDoc is closed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2201
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 2203
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 2205
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v2, :cond_3

    .line 2206
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v2

    shr-int/lit8 v2, v2, 0x18

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0xff

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 2207
    .local v0, "enable":Z
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    if-eq v0, v2, :cond_3

    .line 2208
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v2, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 2209
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    .line 2212
    .end local v0    # "enable":Z
    :cond_3
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2213
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    :cond_4
    move v0, v1

    .line 2216
    goto :goto_0
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    .prologue
    .line 2497
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2501
    :goto_0
    return-void

    .line 2500
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    goto :goto_0
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 7
    .param p1, "position"    # Landroid/graphics/PointF;

    .prologue
    .line 2098
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2103
    :goto_0
    return-void

    .line 2102
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v4, p1, Landroid/graphics/PointF;->x:F

    iget v5, p1, Landroid/graphics/PointF;->y:F

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPan(JFFZ)V

    goto :goto_0
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .prologue
    .line 2517
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2521
    :goto_0
    return-void

    .line 2520
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    goto :goto_0
.end method

.method public setPenSettingInfo(ILcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 4
    .param p1, "userId"    # I
    .param p2, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 1315
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1329
    :cond_0
    :goto_0
    return-void

    .line 1319
    :cond_1
    if-eqz p2, :cond_0

    .line 1320
    iget v0, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1321
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1323
    :cond_2
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenStyle(JILjava/lang/String;)Z

    .line 1324
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenColor(JII)V

    .line 1325
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenSize(JIF)V

    .line 1326
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-boolean v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_enablePenCurve(JIZ)V

    .line 1327
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget-object v2, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setAdvancedSetting(JILjava/lang/String;)V

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x5

    .line 1246
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1291
    :cond_0
    :goto_0
    return-void

    .line 1250
    :cond_1
    if-eqz p1, :cond_0

    .line 1251
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1252
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1254
    :cond_2
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenStyle(JILjava/lang/String;)Z

    .line 1255
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenColor(JII)V

    .line 1256
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setPenSize(JIF)V

    .line 1257
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-boolean v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_enablePenCurve(JIZ)V

    .line 1258
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setAdvancedSetting(JILjava/lang/String;)V

    .line 1260
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 1261
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->bIsSupport:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    if-eqz v0, :cond_5

    .line 1262
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1263
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1264
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1265
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexPencil:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    .line 1272
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    invoke-virtual {v0, v1}, Lcom/samsung/audio/SmpsManager;->setActivePen(I)Z

    .line 1273
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmps:Lcom/samsung/audio/SmpsManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->convertPenNameToMaxThicknessValue(Ljava/lang/String;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/samsung/audio/SmpsManager;->setThickness(D)Z

    .line 1276
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 1277
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 1278
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 1279
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    .line 1280
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1281
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1280
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1282
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-ne v0, v5, :cond_6

    .line 1283
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setPenHoverPoint(Ljava/lang/String;)V

    .line 1287
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    if-eqz v0, :cond_0

    .line 1288
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto/16 :goto_0

    .line 1266
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1267
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1268
    :cond_8
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexBrush:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    goto/16 :goto_1

    .line 1269
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1270
    :cond_a
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIndexMarker:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->activePen:I

    goto/16 :goto_1
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 2377
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2381
    :goto_0
    return-void

    .line 2380
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    const/4 v1, 0x5

    .line 1554
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1555
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1556
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1557
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1558
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1560
    :cond_0
    return-void
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    const/4 v1, 0x5

    .line 1597
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1598
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1599
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1600
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1601
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1603
    :cond_0
    return-void
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    const/4 v1, 0x5

    .line 1203
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1204
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1205
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1206
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1207
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1209
    :cond_0
    return-void
.end method

.method public setToolTipEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2828
    const-string v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setToolTipEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2829
    if-nez p1, :cond_0

    .line 2830
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2832
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    .line 2833
    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 7
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 1659
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1703
    :cond_0
    :goto_0
    return-void

    .line 1663
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setToolTypeAction(II)V

    .line 1665
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    .line 1666
    if-ne p2, v4, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_5

    .line 1667
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    if-nez v0, :cond_2

    .line 1668
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    .line 1673
    :cond_2
    :goto_1
    if-ne p2, v4, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_6

    .line 1674
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_3

    .line 1675
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 1677
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1678
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1677
    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1679
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setPenHoverPoint(Ljava/lang/String;)V

    .line 1700
    :cond_4
    :goto_2
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    move-object v1, p0

    move v5, p1

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setToolTypeAction(JIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1701
    const/4 v0, 0x1

    const-string v1, "not set LocalUserId"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 1671
    :cond_5
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverPoint:Landroid/graphics/Point;

    goto :goto_1

    .line 1680
    :cond_6
    const/4 v0, 0x3

    if-ne p2, v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_8

    .line 1681
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v0, :cond_7

    .line 1682
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 1684
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1685
    :cond_8
    const/4 v0, 0x4

    if-ne p2, v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-nez v0, :cond_4

    .line 1694
    :cond_9
    const/4 v0, 0x5

    if-ne p2, v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_a

    .line 1695
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableHoverImage()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1697
    :cond_a
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public setToolTypeAction(III)V
    .locals 7
    .param p1, "userId"    # I
    .param p2, "toolType"    # I
    .param p3, "action"    # I

    .prologue
    .line 1738
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1743
    :goto_0
    return-void

    .line 1742
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setToolTypeAction(JIII)Z

    goto :goto_0
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .prologue
    .line 2397
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2401
    :goto_0
    return-void

    .line 2400
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setZoom(FFF)V
    .locals 7
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 1982
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1986
    :goto_0
    return-void

    .line 1985
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_setZoom(JFFF)V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .prologue
    .line 2477
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2481
    :goto_0
    return-void

    .line 2480
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    goto :goto_0
.end method

.method public setZoomable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 2791
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2796
    :goto_0
    return-void

    .line 2795
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_enableZoom(JZ)V

    goto :goto_0
.end method

.method public update()V
    .locals 6

    .prologue
    .line 2306
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2321
    :cond_0
    :goto_0
    return-void

    .line 2309
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_update(J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2310
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    .line 2311
    const-string v2, "We can\'t update this state, we can update just for append object"

    .line 2310
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 2314
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v1, :cond_0

    .line 2315
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_3

    const/4 v0, 0x1

    .line 2316
    .local v0, "enable":Z
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_0

    .line 2317
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    .line 2318
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMagicPenEnabled:Z

    goto :goto_0

    .line 2315
    .end local v0    # "enable":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)V
    .locals 4
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .param p2, "userId"    # I

    .prologue
    .line 2355
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2361
    :cond_0
    :goto_0
    return-void

    .line 2358
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_updateHistory(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2359
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    const-string v1, "We can\'t redo"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)V
    .locals 4
    .param p1, "userDataList"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .param p2, "userId"    # I

    .prologue
    .line 2335
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2341
    :cond_0
    :goto_0
    return-void

    .line 2338
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->Native_updateHistory(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2339
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    const-string v1, "We can\'t undo"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method
