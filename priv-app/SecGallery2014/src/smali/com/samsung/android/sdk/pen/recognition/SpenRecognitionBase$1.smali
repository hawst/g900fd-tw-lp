.class Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$1;
.super Ljava/lang/Object;
.source "SpenRecognitionBase.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .local p2, "output":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->access$0(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;)Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->access$0(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;)Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;->onResult(Ljava/util/List;Ljava/util/List;)V

    .line 137
    :cond_0
    return-void
.end method
