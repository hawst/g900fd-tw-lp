.class Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;
.super Ljava/lang/Object;
.source "SPenFontSizeDropdown.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 247
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$0(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 248
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowHeight:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I

    move-result v5

    .line 249
    .local v5, "tempHeight":I
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v0

    if-le v5, v0, :cond_3

    .line 250
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v5

    .line 254
    :goto_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->isAutoScroll:Z

    .line 255
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v0

    const/16 v1, 0x640

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getScreenWidth()I

    move-result v0

    const/16 v1, 0x300

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->getDensity()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->anchor:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetX:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$2(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mOffsetY:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I

    move-result v4

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 261
    :goto_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->isAutoScroll:Z

    .line 266
    .end local v5    # "tempHeight":I
    :cond_2
    :goto_2
    return-void

    .line 252
    .restart local v5    # "tempHeight":I
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowHeight:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I

    move-result v5

    goto :goto_0

    .line 259
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->anchor:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->windowWidth:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)I

    move-result v2

    invoke-virtual {v0, v1, v2, v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 263
    .end local v5    # "tempHeight":I
    :catch_0
    move-exception v6

    .line 264
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2
.end method
