.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
.super Landroid/widget/VideoView;
.source "WrapperVideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

.field private mStart:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    .line 76
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/widget/VideoView;->onDraw(Landroid/graphics/Canvas;)V

    .line 48
    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->mStart:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;->onStart()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    .line 54
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->mStart:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->mStart:I

    .line 55
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-static {v2, p1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->getDefaultSize(II)I

    move-result v1

    .line 64
    .local v1, "width":I
    invoke-static {v2, p2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->getDefaultSize(II)I

    move-result v0

    .line 66
    .local v0, "height":I
    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setMeasuredDimension(II)V

    .line 67
    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->mListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    .line 37
    return-void
.end method
