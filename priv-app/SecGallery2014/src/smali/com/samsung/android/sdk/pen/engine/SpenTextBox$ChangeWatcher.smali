.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;
.super Ljava/lang/Object;
.source "SpenTextBox.java"

# interfaces
.implements Landroid/text/SpanWatcher;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangeWatcher"
.end annotation


# instance fields
.field private mBeforeText:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    .prologue
    .line 6779
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V
    .locals 0

    .prologue
    .line 6779
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1, "buffer"    # Landroid/text/Editable;

    .prologue
    .line 6911
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    .line 6912
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "buffer"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 6786
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6787
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    .line 6789
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->startInput()V

    .line 6790
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6792
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6794
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6795
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->mBeforeText:Ljava/lang/CharSequence;

    .line 6797
    :cond_0
    return-void
.end method

.method public onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .locals 0
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I

    .prologue
    .line 6920
    return-void
.end method

.method public onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V
    .locals 0
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I
    .param p5, "st"    # I
    .param p6, "en"    # I

    .prologue
    .line 6916
    return-void
.end method

.method public onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .locals 0
    .param p1, "buf"    # Landroid/text/Spannable;
    .param p2, "what"    # Ljava/lang/Object;
    .param p3, "s"    # I
    .param p4, "e"    # I

    .prologue
    .line 6924
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 9
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    const/4 v8, 0x1

    .line 6802
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isSelected()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isShown()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 6803
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->mBeforeText:Ljava/lang/CharSequence;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->sendAccessibilityEventTypeViewTextChanged(Ljava/lang/CharSequence;III)V
    invoke-static {v6, v7, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Ljava/lang/CharSequence;III)V

    .line 6804
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->mBeforeText:Ljava/lang/CharSequence;

    .line 6807
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    if-nez v6, :cond_3

    .line 6907
    :cond_2
    :goto_0
    return-void

    .line 6811
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsUndoOrRedo:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsChangedByKeyShortCut:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$58(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 6812
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$59(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Ljava/lang/String;)V

    .line 6813
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$60(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    .line 6814
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$61(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6818
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->recordedDelete:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$62(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-eqz v6, :cond_5

    if-le p4, p3, :cond_5

    .line 6819
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$59(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Ljava/lang/String;)V

    .line 6820
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$63(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6823
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCheckCursorOnScroll(Z)V

    .line 6825
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$64(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-nez v6, :cond_c

    .line 6826
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 6827
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    .line 6830
    :cond_6
    add-int v6, p2, p4

    invoke-interface {p1, p2, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 6834
    .local v5, "str":Ljava/lang/String;
    if-eqz p3, :cond_11

    .line 6835
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v4

    .line 6836
    .local v4, "objectString":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 6840
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, v8, :cond_f

    .line 6841
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$65(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$66(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$67(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsSetEmptyText:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$68(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 6843
    :cond_7
    const/4 v0, 0x0

    .line 6844
    .local v0, "correctDelUTF16":Z
    if-lez p2, :cond_8

    if-le p3, v8, :cond_8

    .line 6845
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v7, p2, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 6846
    add-int/lit8 p2, p2, -0x1

    .line 6847
    add-int/lit8 p3, p3, 0x1

    .line 6848
    const/4 v0, 0x1

    .line 6851
    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->removeText(II)V

    .line 6852
    if-eqz v0, :cond_9

    .line 6853
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateEditable()V
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$69(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6855
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$70(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 6856
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 6878
    .end local v0    # "correctDelUTF16":Z
    .end local v4    # "objectString":Ljava/lang/String;
    :cond_a
    :goto_1
    if-eqz p4, :cond_b

    .line 6879
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 6882
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsUndoOrRedo:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v6

    if-nez v6, :cond_c

    .line 6883
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v7

    invoke-static {v7}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$73(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    .line 6884
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6891
    .end local v5    # "str":Ljava/lang/String;
    :cond_c
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    .line 6892
    .local v1, "desctription":Ljava/lang/String;
    if-nez v1, :cond_d

    .line 6893
    const-string v1, ""

    .line 6895
    :cond_d
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TextBox "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 6897
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$74(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 6898
    const-string v7, "default_input_method"

    .line 6897
    invoke-static {v6, v7}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 6900
    .local v3, "inputMethodId":Ljava/lang/String;
    const-string v6, "com.google.android.googlequicksearchbox/com.google.android.voicesearch.ime.VoiceInputMethodService"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 6901
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v6, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$75(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    .line 6902
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    add-int v7, p2, p4

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$76(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    .line 6903
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showDeletePopupWindow()V
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$77(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    goto/16 :goto_0

    .line 6859
    .end local v1    # "desctription":Ljava/lang/String;
    .end local v3    # "inputMethodId":Ljava/lang/String;
    .restart local v4    # "objectString":Ljava/lang/String;
    .restart local v5    # "str":Ljava/lang/String;
    :cond_e
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$71(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v7

    add-int/2addr v7, p3

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$72(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    :try_end_1
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 6887
    .end local v4    # "objectString":Ljava/lang/String;
    .end local v5    # "str":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 6888
    .local v2, "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V

    goto :goto_2

    .line 6862
    .end local v2    # "e":Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;
    .restart local v4    # "objectString":Ljava/lang/String;
    .restart local v5    # "str":Ljava/lang/String;
    :cond_f
    :try_start_2
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$71(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v6

    if-nez v6, :cond_10

    .line 6863
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    invoke-virtual {v6, v5, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    goto/16 :goto_1

    .line 6865
    :cond_10
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$71(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v7

    invoke-virtual {v6, v5, p2, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    .line 6866
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$72(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    goto/16 :goto_1

    .line 6870
    .end local v4    # "objectString":Ljava/lang/String;
    :cond_11
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$71(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v6

    if-nez v6, :cond_12

    .line 6871
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->insertTextAtCursor(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6873
    :cond_12
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$71(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v7

    invoke-virtual {v6, v5, p2, v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    .line 6874
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$72(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    :try_end_2
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 6905
    .end local v5    # "str":Ljava/lang/String;
    .restart local v1    # "desctription":Ljava/lang/String;
    .restart local v3    # "inputMethodId":Ljava/lang/String;
    :cond_13
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideDeleteTextPopup()V
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    goto/16 :goto_0
.end method
