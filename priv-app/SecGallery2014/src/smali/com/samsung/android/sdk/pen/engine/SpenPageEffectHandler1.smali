.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;
.super Landroid/os/Handler;
.source "SpenPageEffectHandler.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;
    }
.end annotation


# static fields
.field private static final FRAME_COUNT:I = 0x8

.field private static final MIN_RATIO:F = 0.98f

.field private static final PAGE_EFFECT_TIMER_INTERVAL:I = 0x0

.field private static final SIZE_FACTOR:F = 0.0024999976f


# instance fields
.field private mBackPaint:Landroid/graphics/Paint;

.field private mBackRect:[Landroid/graphics/Rect;

.field private mBmpGradient:Landroid/graphics/Bitmap;

.field private mCanvasHeight0:I

.field private mCanvasHeight1:I

.field private mCanvasHeightL:I

.field private mCanvasHeightR:I

.field private mCanvasWidth0:I

.field private mCanvasWidth1:I

.field private mCanvasWidthL:I

.field private mCanvasWidthR:I

.field private mCount:I

.field private mDirection:I

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

.field private mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

.field private mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

.field private mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

.field private mStartX0:I

.field private mStartX1:I

.field private mStartXL:I

.field private mStartXR:I

.field private mStartY0:I

.field private mStartY1:I

.field private mStartYL:I

.field private mStartYR:I

.field private mWorking:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .prologue
    .line 213
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 214
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    .line 215
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    .line 217
    const/4 v1, 0x7

    new-array v1, v1, [Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    .line 218
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 222
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .line 224
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    .line 225
    return-void

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v1, v0

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth0:I

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight0:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth1:I

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight1:I

    return v0
.end method

.method private endAnimation()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 420
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->removeMessages(I)V

    .line 421
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    .line 422
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onFinish()V

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    if-eqz v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->clean()V

    .line 428
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    if-eqz v0, :cond_2

    .line 429
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->clean()V

    .line 431
    :cond_2
    return-void
.end method

.method private makeShadowBitmap()Z
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/16 v6, 0xff

    const/4 v1, 0x0

    const/4 v11, 0x0

    .line 435
    :try_start_0
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    div-int/lit8 v2, v2, 0xa

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightL:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 444
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    int-to-float v2, v2

    const/high16 v3, 0x41200000    # 10.0f

    div-float v3, v2, v3

    const/16 v2, 0x7d

    invoke-static {v2, v11, v11, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    .line 445
    invoke-static {v11, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    .line 444
    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 446
    .local v0, "gradient":Landroid/graphics/LinearGradient;
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 447
    .local v10, "gradientPaint":Landroid/graphics/Paint;
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 449
    new-instance v8, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    invoke-direct {v8, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 450
    .local v8, "canvas":Landroid/graphics/Canvas;
    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    div-int/lit8 v2, v2, 0xa

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightL:I

    invoke-direct {v1, v11, v11, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v8, v1, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 451
    const/4 v8, 0x0

    .line 453
    const/4 v1, 0x1

    .end local v0    # "gradient":Landroid/graphics/LinearGradient;
    .end local v8    # "canvas":Landroid/graphics/Canvas;
    .end local v10    # "gradientPaint":Landroid/graphics/Paint;
    :goto_0
    return v1

    .line 436
    :catch_0
    move-exception v9

    .line 437
    .local v9, "e":Ljava/lang/Exception;
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    move v1, v11

    .line 438
    goto :goto_0

    .line 439
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v9

    .line 440
    .local v9, "e":Ljava/lang/OutOfMemoryError;
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    move v1, v11

    .line 441
    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 229
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->removeMessages(I)V

    .line 230
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    .line 231
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->clean()V

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    .line 234
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    .line 235
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->clean()V

    .line 239
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    .line 240
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    .line 241
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 244
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 245
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    .line 247
    :cond_2
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    .line 248
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    .line 249
    return-void
.end method

.method public drawAnimation(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 349
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_2

    .line 354
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 359
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    invoke-virtual {p1, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 361
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 366
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    invoke-virtual {p1, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 367
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYL:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 370
    :cond_1
    return-void

    .line 349
    :cond_2
    aget-object v0, v2, v1

    .line 350
    .local v0, "r":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 351
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 349
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 22
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 374
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    const/16 v17, 0x8

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_0

    .line 375
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->endAnimation()V

    .line 417
    :goto_0
    return-void

    .line 380
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    const/high16 v17, 0x41000000    # 8.0f

    div-float v15, v16, v17

    .line 381
    .local v15, "tomak":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mDirection:I

    move/from16 v16, v0

    if-nez v16, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    move/from16 v16, v0

    rsub-int/lit8 v16, v16, 0x8

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v9, v15, v16

    .line 382
    .local v9, "moveDistance":F
    :goto_1
    neg-float v0, v9

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartXL:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    add-float v7, v16, v17

    .line 383
    .local v7, "dstStartX":F
    const/16 v16, 0x0

    cmpl-float v16, v7, v16

    if-lez v16, :cond_2

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightL:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    float-to-int v0, v7

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYL:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    add-float v19, v19, v7

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYL:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightL:I

    move/from16 v21, v0

    add-int v20, v20, v21

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 392
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mDirection:I

    move/from16 v16, v0

    if-nez v16, :cond_3

    const/high16 v16, 0x3f800000    # 1.0f

    const v17, 0x3b23d700    # 0.0024999976f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v17, v17, v18

    sub-float v14, v16, v17

    .line 394
    .local v14, "ratio":F
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthR:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v11, v16, v14

    .line 395
    .local v11, "newW":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightR:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v10, v16, v14

    .line 396
    .local v10, "newH":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartXR:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthR:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    sub-float v17, v17, v11

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    add-float v12, v16, v17

    .line 397
    .local v12, "newX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYR:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightR:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    sub-float v17, v17, v10

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    add-float v13, v16, v17

    .line 398
    .local v13, "newY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v12, v0}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 399
    .local v5, "dstL":F
    move v8, v13

    .line 400
    .local v8, "dstT":F
    add-float v6, v12, v11

    .line 401
    .local v6, "dstR":F
    add-float v4, v13, v10

    .line 402
    .local v4, "dstB":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    sub-float v17, v5, v12

    div-float v17, v17, v14

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthR:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightR:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    float-to-int v0, v5

    move/from16 v17, v0

    float-to-int v0, v8

    move/from16 v18, v0

    float-to-int v0, v6

    move/from16 v19, v0

    float-to-int v0, v4

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget-object v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    aget-object v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenHeight:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x2

    aget-object v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x3

    aget-object v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x4

    aget-object v16, v16, v17

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x5

    aget-object v16, v16, v17

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x6

    aget-object v16, v16, v17

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenHeight:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdate()V

    .line 416
    const/16 v16, 0x0

    const-wide/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 381
    .end local v4    # "dstB":F
    .end local v5    # "dstL":F
    .end local v6    # "dstR":F
    .end local v7    # "dstStartX":F
    .end local v8    # "dstT":F
    .end local v9    # "moveDistance":F
    .end local v10    # "newH":F
    .end local v11    # "newW":F
    .end local v12    # "newX":F
    .end local v13    # "newY":F
    .end local v14    # "ratio":F
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v9, v15, v16

    goto/16 :goto_1

    .line 387
    .restart local v7    # "dstStartX":F
    .restart local v9    # "moveDistance":F
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    neg-float v0, v7

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightL:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYL:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    add-float v19, v19, v7

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYL:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightL:I

    move/from16 v21, v0

    add-int v20, v20, v21

    invoke-virtual/range {v16 .. v20}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    .line 392
    :cond_3
    const v16, 0x3f7ae148    # 0.98f

    const v17, 0x3b23d700    # 0.0024999976f

    .line 393
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v17, v17, v18

    add-float v14, v16, v17

    goto/16 :goto_3
.end method

.method public isWorking()Z
    .locals 1

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    return v0
.end method

.method public saveScreenshot()Z
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->saveScreenshot()Z

    move-result v0

    return v0
.end method

.method public setCanvasInformation(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 272
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX0:I

    .line 273
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY0:I

    .line 274
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth0:I

    .line 275
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight0:I

    .line 276
    return-void
.end method

.method public setCanvasInformation1(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 280
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX1:I

    .line 281
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY1:I

    .line 282
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth1:I

    .line 283
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight1:I

    .line 284
    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackPaint:Landroid/graphics/Paint;

    .line 254
    return-void
.end method

.method public setScreenResolution(II)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 258
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenHeight:I

    if-ne p2, v0, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    .line 263
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenHeight:I

    .line 265
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    goto :goto_0
.end method

.method public startAnimation(I)Z
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 296
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->saveScreenshot1()Z

    move-result v2

    if-nez v2, :cond_1

    .line 338
    :cond_0
    :goto_0
    return v0

    .line 299
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth0:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight0:I

    invoke-virtual {v2, v0, v0, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 300
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX0:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY0:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX0:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth0:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY0:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight0:I

    add-int/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 302
    if-nez p1, :cond_2

    .line 303
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    .line 304
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    .line 306
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX1:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartXL:I

    .line 307
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY1:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYL:I

    .line 308
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth1:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    .line 309
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight1:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightL:I

    .line 311
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX0:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartXR:I

    .line 312
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY0:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYR:I

    .line 313
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth0:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthR:I

    .line 314
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight0:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightR:I

    .line 329
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->makeShadowBitmap()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 332
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    .line 333
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mDirection:I

    .line 334
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    .line 336
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 338
    goto :goto_0

    .line 316
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    .line 317
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    .line 319
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX0:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartXL:I

    .line 320
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY0:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYL:I

    .line 321
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth0:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthL:I

    .line 322
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight0:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightL:I

    .line 324
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX1:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartXR:I

    .line 325
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY1:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartYR:I

    .line 326
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth1:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidthR:I

    .line 327
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight1:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeightR:I

    goto :goto_1
.end method
