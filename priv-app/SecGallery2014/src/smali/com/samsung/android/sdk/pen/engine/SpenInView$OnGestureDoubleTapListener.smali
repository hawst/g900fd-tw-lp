.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnGestureDoubleTapListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 5904
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;)V
    .locals 0

    .prologue
    .line 5904
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 5908
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDoubleTapZoomable:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 5909
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 5910
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 5911
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    if-nez v0, :cond_0

    .line 5912
    const-string v0, "SpenInView"

    const-string v1, "one finger double tab"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5913
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 5914
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    .line 5918
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    .line 5920
    :cond_0
    return v5

    .line 5916
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 5925
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 5930
    const/4 v0, 0x0

    return v0
.end method
