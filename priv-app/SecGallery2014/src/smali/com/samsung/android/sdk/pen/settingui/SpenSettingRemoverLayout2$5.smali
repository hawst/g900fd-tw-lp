.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;
.super Ljava/lang/Object;
.source "SpenSettingRemoverLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 415
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 416
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 417
    .local v0, "progress":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 418
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 423
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    const/high16 v2, 0x41200000    # 10.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 424
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    int-to-float v4, v4

    const/high16 v5, 0x44870000    # 1080.0f

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 425
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    aget-object v2, v2, v3

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 427
    :cond_0
    return-void

    .line 420
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method
