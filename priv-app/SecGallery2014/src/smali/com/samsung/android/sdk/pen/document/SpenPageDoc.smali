.class public final Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
.super Ljava/lang/Object;
.source "SpenPageDoc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;,
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;,
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;,
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;,
        Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;
    }
.end annotation


# static fields
.field public static final BACKGROUND_IMAGE_MODE_CENTER:I = 0x0

.field public static final BACKGROUND_IMAGE_MODE_FIT:I = 0x2

.field public static final BACKGROUND_IMAGE_MODE_STRETCH:I = 0x1

.field public static final BACKGROUND_IMAGE_MODE_TILE:I = 0x3

.field public static final FIND_TYPE_ALL:I = 0x1f

.field public static final FIND_TYPE_CONTAINER:I = 0x8

.field public static final FIND_TYPE_IMAGE:I = 0x4

.field public static final FIND_TYPE_STROKE:I = 0x1

.field public static final FIND_TYPE_TEXT_BOX:I = 0x2

.field public static final GEO_TAG_STATE_DEFAULT:I = 0x0

.field public static final GEO_TAG_STATE_REMOVED:I = 0x2

.field public static final GEO_TAG_STATE_SET:I = 0x1

.field public static final HISTORY_MANAGER_MODE_MULTIPLE_VIEW:I = 0x1

.field public static final HISTORY_MANAGER_MODE_SINGLE_VIEW:I = 0x0

.field private static final NATIVE_COMMAND_APPEND_OBJECTLIST:I = 0x2

.field private static final NATIVE_COMMAND_DUMMY:I = 0x0

.field private static final NATIVE_COMMAND_SET_MULTI_VIEW_USERID:I = 0x1

.field public static final ORIENTATION_LANDSCAPE:I = 0x1

.field public static final ORIENTATION_PORTRAIT:I


# instance fields
.field private mHandle:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    .line 404
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 1
    .param p1, "templateUri"    # Ljava/lang/String;
    .param p2, "templatePage"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    .line 407
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_Construct2(Ljava/lang/String;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 410
    :cond_0
    return-void
.end method

.method private native Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_AddTag(Ljava/lang/String;)Z
.end method

.method private native PageDoc_AppendLayer(I)Z
.end method

.method private native PageDoc_AppendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native PageDoc_ClearChangedFlagOfLayer()V
.end method

.method private native PageDoc_ClearRecordedObject()Z
.end method

.method private native PageDoc_Construct2(Ljava/lang/String;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z
.end method

.method private native PageDoc_Copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z
.end method

.method private native PageDoc_EnableLayerEventForward(IZ)Z
.end method

.method private native PageDoc_FindObjectAtPosition(IFF)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IFF)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_FindObjectInClosedCurve(I[Landroid/graphics/PointF;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[",
            "Landroid/graphics/PointF;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_FindObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/RectF;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_FindTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.end method

.method private native PageDoc_GetBackgroundColor()I
.end method

.method private native PageDoc_GetBackgroundImage()Landroid/graphics/Bitmap;
.end method

.method private native PageDoc_GetBackgroundImageMode()I
.end method

.method private native PageDoc_GetBackgroundImagePath()Ljava/lang/String;
.end method

.method private native PageDoc_GetCurrentLayerId()I
.end method

.method private native PageDoc_GetExtraDataByteArray(Ljava/lang/String;)[B
.end method

.method private native PageDoc_GetExtraDataInt(Ljava/lang/String;)I
.end method

.method private native PageDoc_GetExtraDataString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native PageDoc_GetExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method private native PageDoc_GetGeoTagLatitude()D
.end method

.method private native PageDoc_GetGeoTagLongitude()D
.end method

.method private native PageDoc_GetGeoTagState()I
.end method

.method private native PageDoc_GetHeight()I
.end method

.method private native PageDoc_GetHistoryUpdateRect()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetId()Ljava/lang/String;
.end method

.method private native PageDoc_GetLastEditedTime()J
.end method

.method private native PageDoc_GetLayerCount()I
.end method

.method private native PageDoc_GetLayerHistoryId(I)I
.end method

.method private native PageDoc_GetLayerIdByIndex(I)I
.end method

.method private native PageDoc_GetLayerIndex(I)I
.end method

.method private native PageDoc_GetLayerName(I)Ljava/lang/String;
.end method

.method private native PageDoc_GetObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.end method

.method private native PageDoc_GetObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.end method

.method private native PageDoc_GetObjectCount(Z)I
.end method

.method private native PageDoc_GetObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)I
.end method

.method private native PageDoc_GetObjectList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetObjectList2(I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetObjectList3(ILjava/lang/String;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetObjectList4(ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetOrientation()I
.end method

.method private native PageDoc_GetRectOfAllObject()Landroid/graphics/RectF;
.end method

.method private native PageDoc_GetSelectedObject()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetSelectedObjectCount()I
.end method

.method private native PageDoc_GetTag()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetTemplateObjectList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native PageDoc_GetTemplateUri()Ljava/lang/String;
.end method

.method private native PageDoc_GetThumbnail()Landroid/graphics/Bitmap;
.end method

.method private native PageDoc_GetVoiceData()Ljava/lang/String;
.end method

.method private native PageDoc_GetWidth()I
.end method

.method private native PageDoc_GroupObject(Ljava/util/ArrayList;Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;Z)",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;"
        }
    .end annotation
.end method

.method private native PageDoc_GroupSelectedObject(Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
.end method

.method private native PageDoc_HasExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native PageDoc_HasExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native PageDoc_HasExtraDataString(Ljava/lang/String;)Z
.end method

.method private native PageDoc_HasExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native PageDoc_HasRecordedObject()Z
.end method

.method private native PageDoc_InsertLayer(II)Z
.end method

.method private native PageDoc_InsertObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;I)Z
.end method

.method private native PageDoc_IsChanged()Z
.end method

.method private native PageDoc_IsHistoryManagerUsed()Z
.end method

.method private native PageDoc_IsLayerChanged()Z
.end method

.method private native PageDoc_IsLayerEventForwardable(I)Z
.end method

.method private native PageDoc_IsObjectLoaded()Z
.end method

.method private native PageDoc_IsRecording()Z
.end method

.method private native PageDoc_IsTextOnly()Z
.end method

.method private native PageDoc_IsValid()Z
.end method

.method private native PageDoc_LoadHeader(Ljava/lang/String;)Z
.end method

.method private native PageDoc_LoadObject()Z
.end method

.method private native PageDoc_MoveLayerIndex(II)Z
.end method

.method private native PageDoc_MoveObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;IZ)Z
.end method

.method private native PageDoc_RemoveAllObject()Z
.end method

.method private native PageDoc_RemoveExtraDataByteArray(Ljava/lang/String;)Z
.end method

.method private native PageDoc_RemoveExtraDataInt(Ljava/lang/String;)Z
.end method

.method private native PageDoc_RemoveExtraDataString(Ljava/lang/String;)Z
.end method

.method private native PageDoc_RemoveExtraDataStringArray(Ljava/lang/String;)Z
.end method

.method private native PageDoc_RemoveGeoTag()Z
.end method

.method private native PageDoc_RemoveLayer(I)Z
.end method

.method private native PageDoc_RemoveObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native PageDoc_RemoveSelectedObject()Z
.end method

.method private native PageDoc_RemoveTag(Ljava/lang/String;)Z
.end method

.method private native PageDoc_Save()Z
.end method

.method private native PageDoc_SelectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native PageDoc_SelectObject(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native PageDoc_SetBackgroundColor(I)Z
.end method

.method private native PageDoc_SetBackgroundImage(Ljava/lang/String;)Z
.end method

.method private native PageDoc_SetBackgroundImageMode(I)Z
.end method

.method private native PageDoc_SetCurrentLayer(I)Z
.end method

.method private native PageDoc_SetExtraDataByteArray(Ljava/lang/String;[BI)Z
.end method

.method private native PageDoc_SetExtraDataInt(Ljava/lang/String;I)Z
.end method

.method private native PageDoc_SetExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native PageDoc_SetExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z
.end method

.method private native PageDoc_SetGeoTag(DD)Z
.end method

.method private native PageDoc_SetLayerName(ILjava/lang/String;)Z
.end method

.method private native PageDoc_SetObjectIndexMovedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;)Z
.end method

.method private native PageDoc_SetObjectListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;)Z
.end method

.method private native PageDoc_SetObjectSelectedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;)Z
.end method

.method private native PageDoc_SetTemplateUri(Ljava/lang/String;)Z
.end method

.method private native PageDoc_SetThumbnail(Landroid/graphics/Bitmap;)Z
.end method

.method private native PageDoc_SetVoiceData(Ljava/lang/String;)V
.end method

.method private native PageDoc_SetVolatileBackgroundImage(Landroid/graphics/Bitmap;)Z
.end method

.method private native PageDoc_StartRecord()Z
.end method

.method private native PageDoc_StopRecord()Z
.end method

.method private native PageDoc_UngroupObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Z)Z
.end method

.method private native PageDoc_UngroupSelectedObject(Z)Z
.end method

.method private native PageDoc_UnloadObject()Z
.end method

.method private native PageDoc_UseHistoryManager(Z)V
.end method

.method private native PageDoc_clearHistory()V
.end method

.method private native PageDoc_clearHistory2(I)V
.end method

.method private native PageDoc_clearHistoryTag()Z
.end method

.method private native PageDoc_clearRedoHistory()V
.end method

.method private native PageDoc_commitHistory(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)Z
.end method

.method private native PageDoc_finalize()V
.end method

.method private native PageDoc_getHistoryManagerMode()I
.end method

.method private native PageDoc_getLastHistoryId()I
.end method

.method private native PageDoc_getUndoLimit()I
.end method

.method private native PageDoc_isRedoable()Z
.end method

.method private native PageDoc_isRedoable2(I)Z
.end method

.method private native PageDoc_isUndoable()Z
.end method

.method private native PageDoc_isUndoable2(I)Z
.end method

.method private native PageDoc_redo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_redo2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_redoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_redoAll2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_setHistoryId(I)Z
.end method

.method private native PageDoc_setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)Z
.end method

.method private native PageDoc_setHistoryManagerMode(I)Z
.end method

.method private native PageDoc_setHistoryTag()Z
.end method

.method private native PageDoc_setUndoLimit(I)V
.end method

.method private native PageDoc_startHistoryGroup()Z
.end method

.method private native PageDoc_stopHistoryGroup()Z
.end method

.method private native PageDoc_undo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_undo2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_undoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_undoAll2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private native PageDoc_undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
.end method

.method private static isBuildTypeEngMode()Z
    .locals 2

    .prologue
    .line 432
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 413
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 414
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenPageDoc("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 416
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 418
    return-void
.end method


# virtual methods
.method public addTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 1142
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_AddTag(Ljava/lang/String;)Z

    move-result v0

    .line 1143
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1144
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1146
    :cond_0
    return-void
.end method

.method public appendLayer(I)V
    .locals 2
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 1869
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_AppendLayer(I)Z

    move-result v0

    .line 1870
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1871
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1873
    :cond_0
    return-void
.end method

.method public appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 486
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_AppendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 487
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 489
    :cond_0
    return-void
.end method

.method public appendObjectList(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2776
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2777
    .local v2, "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-nez p1, :cond_1

    .line 2797
    :cond_0
    :goto_0
    return-void

    .line 2780
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 2782
    .local v0, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2789
    const/4 v4, 0x2

    invoke-direct {p0, v4, v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2790
    .local v1, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-nez v1, :cond_4

    .line 2791
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    goto :goto_0

    .line 2783
    .end local v1    # "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 2784
    .local v3, "tmpObject":Ljava/lang/Object;
    if-nez v3, :cond_3

    .line 2785
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "E_INVALID_ARG"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2787
    :cond_3
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2794
    .end local v3    # "tmpObject":Ljava/lang/Object;
    .restart local v1    # "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_4
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2795
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    goto :goto_0
.end method

.method public clearChangedFlagOfLayer()V
    .locals 0

    .prologue
    .line 2098
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_ClearChangedFlagOfLayer()V

    .line 2099
    return-void
.end method

.method public clearHistory()V
    .locals 0

    .prologue
    .line 2507
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_clearHistory()V

    .line 2508
    return-void
.end method

.method public clearHistoryTag()V
    .locals 1

    .prologue
    .line 2586
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_clearHistoryTag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2587
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2589
    :cond_0
    return-void
.end method

.method public clearRecordedObject()V
    .locals 2

    .prologue
    .line 1206
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_ClearRecordedObject()Z

    move-result v0

    .line 1207
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1208
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1210
    :cond_0
    return-void
.end method

.method public clearRedoHistory()V
    .locals 0

    .prologue
    .line 2516
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_clearRedoHistory()V

    .line 2517
    return-void
.end method

.method public commitHistory(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 2
    .param p1, "userData"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    .prologue
    .line 2532
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_commitHistory(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)Z

    move-result v0

    .line 2533
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2534
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2536
    :cond_0
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "sourcePage"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 2165
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_Copy(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z

    move-result v0

    .line 2166
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2167
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2169
    :cond_0
    return-void
.end method

.method public createObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 2702
    packed-switch p1, :pswitch_data_0

    move-object v0, v1

    .line 2717
    :goto_0
    return-object v0

    .line 2704
    :pswitch_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2707
    :pswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2710
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    .line 2711
    .local v0, "i":Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Ljava/lang/String;)V

    goto :goto_0

    .line 2715
    .end local v0    # "i":Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 2702
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public createObject(IZ)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 1
    .param p1, "type"    # I
    .param p2, "isTemplateObject"    # Z

    .prologue
    .line 2735
    packed-switch p1, :pswitch_data_0

    .line 2748
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2737
    :pswitch_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>(Z)V

    goto :goto_0

    .line 2740
    :pswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;-><init>(Z)V

    goto :goto_0

    .line 2743
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>(Z)V

    goto :goto_0

    .line 2746
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>(Z)V

    goto :goto_0

    .line 2735
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 2806
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 2807
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .end local p1    # "o":Ljava/lang/Object;
    iget v1, p1, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    if-ne v0, v1, :cond_0

    .line 2808
    const/4 v0, 0x1

    .line 2811
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 426
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    .line 427
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_finalize()V

    .line 428
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    .line 429
    return-void
.end method

.method public findObjectAtPosition(IFF)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IFF)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 808
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_FindObjectAtPosition(IFF)Ljava/util/ArrayList;

    move-result-object v0

    .line 809
    .local v0, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 810
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 812
    :cond_0
    return-object v0
.end method

.method public findObjectInClosedCurve(I[Landroid/graphics/PointF;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "points"    # [Landroid/graphics/PointF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 836
    array-length v1, p2

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_FindObjectInClosedCurve(I[Landroid/graphics/PointF;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 837
    .local v0, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 838
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 840
    :cond_0
    return-object v0
.end method

.method public findObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "rectf"    # Landroid/graphics/RectF;
    .param p3, "allAreas"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/RectF;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 866
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_FindObjectInRect(ILandroid/graphics/RectF;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 867
    .local v0, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 868
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 870
    :cond_0
    return-object v0
.end method

.method public findTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 1
    .param p1, "typeFilter"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 785
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_FindTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 1345
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetBackgroundColor()I

    move-result v0

    return v0
.end method

.method public getBackgroundImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1276
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetBackgroundImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundImageMode()I
    .locals 1

    .prologue
    .line 1320
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetBackgroundImageMode()I

    move-result v0

    return v0
.end method

.method public getBackgroundImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1286
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetBackgroundImagePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentLayerId()I
    .locals 1

    .prologue
    .line 1952
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetCurrentLayerId()I

    move-result v0

    return v0
.end method

.method public getDrawnRectOfAllObject()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1393
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetRectOfAllObject()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataByteArray(Ljava/lang/String;)[B
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1650
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetExtraDataByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1626
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetExtraDataInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1614
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetExtraDataString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1638
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGeoTagLatitude()D
    .locals 2

    .prologue
    .line 1472
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetGeoTagLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getGeoTagLongitude()D
    .locals 2

    .prologue
    .line 1483
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetGeoTagLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getGeoTagState()I
    .locals 3

    .prologue
    .line 1496
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetGeoTagState()I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1500
    :goto_0
    return v1

    .line 1497
    :catch_0
    move-exception v0

    .line 1498
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 1499
    const-string v1, "SpenPageDoc"

    const-string v2, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1500
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 452
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetHeight()I

    move-result v0

    return v0
.end method

.method public getHistoryManagerMode()I
    .locals 1

    .prologue
    .line 2669
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_getHistoryManagerMode()I

    move-result v0

    return v0
.end method

.method public getHistoryUpdateRect()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2562
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetHistoryUpdateRect()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1380
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastEditedTime()J
    .locals 2

    .prologue
    .line 463
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLastEditedTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLayerCount()I
    .locals 1

    .prologue
    .line 1998
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerCount()I

    move-result v0

    return v0
.end method

.method public getLayerIdByIndex(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1984
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerIdByIndex(I)I

    move-result v0

    .line 1985
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1986
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1988
    :cond_0
    return v0
.end method

.method public getLayerIndex(I)I
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1966
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerIndex(I)I

    move-result v0

    .line 1967
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1968
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1970
    :cond_0
    return v0
.end method

.method public getLayerName(I)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 2030
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerIndex(I)I

    move-result v0

    .line 2031
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2032
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2035
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerName(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 584
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    .line 585
    .local v0, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v0, :cond_0

    .line 586
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 588
    :cond_0
    return-object v0
.end method

.method public getObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2
    .param p1, "runtimeHandle"    # I

    .prologue
    .line 719
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    .line 720
    .local v0, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v0, :cond_0

    .line 721
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 723
    :cond_0
    return-object v0
.end method

.method public getObjectCount(Z)I
    .locals 1
    .param p1, "includeInvisible"    # Z

    .prologue
    .line 705
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectCount(Z)I

    move-result v0

    return v0
.end method

.method public getObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)I
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 739
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)I

    move-result v0

    .line 740
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 741
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 743
    :cond_0
    return v0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 599
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 600
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 601
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 603
    :cond_0
    return-object v0
.end method

.method public getObjectList(I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 618
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectList2(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 619
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 620
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 622
    :cond_0
    return-object v0
.end method

.method public getObjectList(ILjava/lang/String;I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "extraKey"    # Ljava/lang/String;
    .param p3, "extraValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 646
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectList3(ILjava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 647
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 648
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 650
    :cond_0
    return-object v0
.end method

.method public getObjectList(ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "typeFilter"    # I
    .param p2, "extraKey"    # Ljava/lang/String;
    .param p3, "extraValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 674
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectList4(ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 675
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 676
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 678
    :cond_0
    return-object v0
.end method

.method public getOrientation()I
    .locals 2

    .prologue
    .line 1221
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetOrientation()I

    move-result v0

    .line 1222
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1223
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1225
    :cond_0
    return v0
.end method

.method public getSelectedObject()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 952
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetSelectedObject()Ljava/util/ArrayList;

    move-result-object v0

    .line 953
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 954
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 956
    :cond_0
    return-object v0
.end method

.method public getSelectedObjectCount()I
    .locals 1

    .prologue
    .line 967
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetSelectedObjectCount()I

    move-result v0

    return v0
.end method

.method public getTag()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1171
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 1172
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 1173
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1175
    :cond_0
    return-object v0
.end method

.method public getTemplateObjectList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 688
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetTemplateObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 689
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 690
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 692
    :cond_0
    return-object v0
.end method

.method public getTemplateUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1370
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetTemplateUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUndoLimit()I
    .locals 1

    .prologue
    .line 2496
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_getUndoLimit()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetWidth()I

    move-result v0

    return v0
.end method

.method public groupObject(Ljava/util/ArrayList;Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 2
    .param p2, "selectAfterGroup"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;Z)",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;"
        }
    .end annotation

    .prologue
    .line 1030
    .local p1, "groupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GroupObject(Ljava/util/ArrayList;Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    .line 1031
    .local v0, "conatiner":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    if-nez v0, :cond_0

    .line 1032
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1034
    :cond_0
    return-object v0
.end method

.method public groupSelectedObject(Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 2
    .param p1, "selectAfterGroup"    # Z

    .prologue
    .line 1056
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GroupSelectedObject(Z)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    .line 1057
    .local v0, "conatiner":Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    if-nez v0, :cond_0

    .line 1058
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1060
    :cond_0
    return-object v0
.end method

.method public hasExtraDataByteArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1702
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataInt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1676
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1663
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasExtraDataString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasExtraDataStringArray(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1689
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasRecordedObject()Z
    .locals 1

    .prologue
    .line 1197
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_HasRecordedObject()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 2821
    iget v0, p0, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->mHandle:I

    return v0
.end method

.method public insertLayer(II)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "index"    # I

    .prologue
    .line 1898
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_InsertLayer(II)Z

    move-result v0

    .line 1899
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1900
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1902
    :cond_0
    return-void
.end method

.method public insertObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;I)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p2, "index"    # I

    .prologue
    .line 515
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_InsertObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 518
    :cond_0
    return-void
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 1404
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsChanged()Z

    move-result v0

    return v0
.end method

.method public isLayerChanged()Z
    .locals 1

    .prologue
    .line 2089
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsLayerChanged()Z

    move-result v0

    return v0
.end method

.method public isLayerEventForwardEnabled(I)Z
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 2072
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetLayerIndex(I)I

    move-result v0

    .line 2073
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2074
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2077
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsLayerEventForwardable(I)Z

    move-result v1

    return v1
.end method

.method public isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 756
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_GetObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)I

    move-result v0

    .line 757
    .local v0, "rnt":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 758
    const/4 v1, 0x0

    .line 760
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isObjectLoaded()Z
    .locals 1

    .prologue
    .line 1844
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsObjectLoaded()Z

    move-result v0

    return v0
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 1440
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsRecording()Z

    move-result v0

    return v0
.end method

.method public isRedoable()Z
    .locals 1

    .prologue
    .line 2212
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_isRedoable()Z

    move-result v0

    return v0
.end method

.method public isRedoable(I)Z
    .locals 1
    .param p1, "userId"    # I

    .prologue
    .line 2260
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_isRedoable2(I)Z

    move-result v0

    return v0
.end method

.method public isTextOnly()Z
    .locals 1

    .prologue
    .line 1186
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsTextOnly()Z

    move-result v0

    return v0
.end method

.method public isUndoable()Z
    .locals 1

    .prologue
    .line 2190
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_isUndoable()Z

    move-result v0

    return v0
.end method

.method public isUndoable(I)Z
    .locals 1
    .param p1, "userId"    # I

    .prologue
    .line 2236
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_isUndoable2(I)Z

    move-result v0

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 2761
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_IsValid()Z

    move-result v0

    return v0
.end method

.method public loadObject()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1798
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_LoadObject()Z

    move-result v0

    .line 1799
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1800
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    .line 1806
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1809
    :cond_0
    return-void
.end method

.method public moveObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;IZ)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p2, "step"    # I
    .param p3, "ignoreInvisible"    # Z

    .prologue
    .line 892
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_MoveObjectIndex(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;IZ)Z

    move-result v0

    .line 893
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 894
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 896
    :cond_0
    return-void
.end method

.method public redo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2300
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_redo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2301
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2302
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2304
    :cond_0
    return-object v0
.end method

.method public redo(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 2348
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_redo2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2349
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2350
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2352
    :cond_0
    return-object v0
.end method

.method public redoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2398
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_redoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2399
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2400
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2402
    :cond_0
    return-object v0
.end method

.method public redoAll(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 2456
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_redoAll2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2457
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2458
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2460
    :cond_0
    return-object v0
.end method

.method public removeAllObject()V
    .locals 1

    .prologue
    .line 553
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveAllObject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 554
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 556
    :cond_0
    return-void
.end method

.method public removeExtraDataByteArray(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1755
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveExtraDataByteArray(Ljava/lang/String;)Z

    move-result v0

    .line 1756
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1757
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1759
    :cond_0
    return-void
.end method

.method public removeExtraDataInt(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1727
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveExtraDataInt(Ljava/lang/String;)Z

    move-result v0

    .line 1728
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1729
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1731
    :cond_0
    return-void
.end method

.method public removeExtraDataString(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1713
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveExtraDataString(Ljava/lang/String;)Z

    move-result v0

    .line 1714
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1715
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1717
    :cond_0
    return-void
.end method

.method public removeExtraDataStringArray(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1741
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveExtraDataStringArray(Ljava/lang/String;)Z

    move-result v0

    .line 1742
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1743
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1745
    :cond_0
    return-void
.end method

.method public removeGeoTag()V
    .locals 4

    .prologue
    .line 1513
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveGeoTag()Z

    move-result v1

    .line 1514
    .local v1, "rnt":Z
    if-nez v1, :cond_0

    .line 1515
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1521
    .end local v1    # "rnt":Z
    :cond_0
    :goto_0
    return-void

    .line 1517
    :catch_0
    move-exception v0

    .line 1518
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 1519
    const-string v2, "SpenPageDoc"

    const-string v3, "Native method is not found. Please update S Pen SDK libraries."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeLayer(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1921
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveLayer(I)Z

    move-result v0

    .line 1922
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1923
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1925
    :cond_0
    return-void
.end method

.method public removeObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 539
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 540
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 542
    :cond_0
    return-void
.end method

.method public removeSelectedObject()V
    .locals 1

    .prologue
    .line 568
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveSelectedObject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 569
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 571
    :cond_0
    return-void
.end method

.method public removeTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 1158
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_RemoveTag(Ljava/lang/String;)Z

    move-result v0

    .line 1159
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1160
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1162
    :cond_0
    return-void
.end method

.method public save()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1772
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_Save()Z

    move-result v0

    .line 1773
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1774
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    .line 1778
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1781
    :cond_0
    return-void
.end method

.method public selectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 914
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SelectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 915
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 916
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 918
    :cond_0
    return-void
.end method

.method public selectObject(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 937
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SelectObject(Ljava/util/ArrayList;)Z

    move-result v0

    .line 938
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 939
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 941
    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 1332
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetBackgroundColor(I)Z

    move-result v0

    .line 1333
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1334
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1336
    :cond_0
    return-void
.end method

.method public setBackgroundImage(Ljava/lang/String;)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1263
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetBackgroundImage(Ljava/lang/String;)Z

    move-result v0

    .line 1264
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1265
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1267
    :cond_0
    return-void
.end method

.method public setBackgroundImageMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1303
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetBackgroundImageMode(I)Z

    move-result v0

    .line 1304
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1305
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1307
    :cond_0
    return-void
.end method

.method public setCurrentLayer(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1939
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetCurrentLayer(I)Z

    move-result v0

    .line 1940
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1941
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1943
    :cond_0
    return-void
.end method

.method public setExtraDataByteArray(Ljava/lang/String;[B)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 1577
    if-nez p2, :cond_1

    .line 1578
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    .line 1582
    .local v0, "rnt":Z
    :goto_0
    if-nez v0, :cond_0

    .line 1583
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1585
    :cond_0
    return-void

    .line 1580
    .end local v0    # "rnt":Z
    :cond_1
    array-length v1, p2

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataByteArray(Ljava/lang/String;[BI)Z

    move-result v0

    .restart local v0    # "rnt":Z
    goto :goto_0
.end method

.method public setExtraDataInt(Ljava/lang/String;I)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1599
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataInt(Ljava/lang/String;I)Z

    move-result v0

    .line 1600
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1601
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1603
    :cond_0
    return-void
.end method

.method public setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1535
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1536
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1537
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1539
    :cond_0
    return-void
.end method

.method public setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    .line 1554
    if-nez p2, :cond_1

    .line 1555
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    .line 1559
    .local v0, "rnt":Z
    :goto_0
    if-nez v0, :cond_0

    .line 1560
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1562
    :cond_0
    return-void

    .line 1557
    .end local v0    # "rnt":Z
    :cond_1
    array-length v1, p2

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;I)Z

    move-result v0

    .restart local v0    # "rnt":Z
    goto :goto_0
.end method

.method public setGeoTag(DD)V
    .locals 3
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 1458
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetGeoTag(DD)Z

    move-result v0

    .line 1459
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1460
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1462
    :cond_0
    return-void
.end method

.method public setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

    .prologue
    .line 2548
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)Z

    move-result v0

    .line 2549
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2550
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2552
    :cond_0
    return-void
.end method

.method public setHistoryManagerMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 2655
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_setHistoryManagerMode(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2656
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2658
    :cond_0
    return-void
.end method

.method public setHistoryTag()V
    .locals 1

    .prologue
    .line 2574
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_setHistoryTag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2575
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2577
    :cond_0
    return-void
.end method

.method public setLayerEventForwardEnabled(IZ)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 2053
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_EnableLayerEventForward(IZ)Z

    move-result v0

    .line 2054
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2055
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2057
    :cond_0
    return-void
.end method

.method public setLayerName(ILjava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 2013
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetLayerName(ILjava/lang/String;)Z

    move-result v0

    .line 2014
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2015
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2017
    :cond_0
    return-void
.end method

.method public setObjectIndexMovedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;

    .prologue
    .line 2143
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetObjectIndexMovedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectIndexMovedListener;)Z

    move-result v0

    .line 2144
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2145
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2147
    :cond_0
    return-void
.end method

.method public setObjectListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;

    .prologue
    .line 2111
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetObjectListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectListener;)Z

    move-result v0

    .line 2112
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2113
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2115
    :cond_0
    return-void
.end method

.method public setObjectSelectedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;

    .prologue
    .line 2127
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetObjectSelectedListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$ObjectSelectedListener;)Z

    move-result v0

    .line 2128
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 2129
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2131
    :cond_0
    return-void
.end method

.method public setTemplateUri(Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 1357
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetTemplateUri(Ljava/lang/String;)Z

    move-result v0

    .line 1358
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1359
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1361
    :cond_0
    return-void
.end method

.method public setUndoLimit(I)V
    .locals 0
    .param p1, "undoLimit"    # I

    .prologue
    .line 2479
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_setUndoLimit(I)V

    .line 2480
    return-void
.end method

.method public setUserIdForHistoryListener(I)V
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 2682
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2683
    .local v1, "tempInteger":Ljava/lang/Integer;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2685
    .local v0, "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2686
    const/4 v2, 0x1

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->Native_command(ILjava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2687
    return-void
.end method

.method public setVolatileBackgroundImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1240
    if-eqz p1, :cond_0

    .line 1241
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1242
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "bitmap is recyled."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1245
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_SetVolatileBackgroundImage(Landroid/graphics/Bitmap;)Z

    move-result v0

    .line 1246
    .local v0, "rnt":Z
    if-nez v0, :cond_1

    .line 1247
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1249
    :cond_1
    return-void
.end method

.method public startHistoryGroup()V
    .locals 1

    .prologue
    .line 2620
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_startHistoryGroup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2621
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2623
    :cond_0
    return-void
.end method

.method public startRecord()V
    .locals 2

    .prologue
    .line 1414
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_StartRecord()Z

    move-result v0

    .line 1415
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1416
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1418
    :cond_0
    return-void
.end method

.method public stopHistoryGroup()V
    .locals 1

    .prologue
    .line 2637
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_stopHistoryGroup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2638
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2640
    :cond_0
    return-void
.end method

.method public stopRecord()V
    .locals 2

    .prologue
    .line 1426
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_StopRecord()Z

    move-result v0

    .line 1427
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1428
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1430
    :cond_0
    return-void
.end method

.method public undo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2278
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2279
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2280
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2282
    :cond_0
    return-object v0
.end method

.method public undo(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 2324
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undo2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2325
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2326
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2328
    :cond_0
    return-object v0
.end method

.method public undoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2373
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2374
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2375
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2377
    :cond_0
    return-object v0
.end method

.method public undoAll(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 2426
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undoAll2(I)[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2427
    .local v0, "rnt":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2428
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 2430
    :cond_0
    return-object v0
.end method

.method public undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    .locals 2

    .prologue
    .line 2602
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 2603
    .local v0, "ret":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    if-nez v0, :cond_0

    .line 2604
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 2607
    :cond_0
    return-object v0
.end method

.method public ungroupObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Z)V
    .locals 2
    .param p1, "group"    # Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .param p2, "selectAfterUngroup"    # Z

    .prologue
    .line 1105
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_UngroupObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Z)Z

    move-result v0

    .line 1106
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1107
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1109
    :cond_0
    return-void
.end method

.method public ungroupSelectedObject(Z)V
    .locals 2
    .param p1, "selectAfterUngroup"    # Z

    .prologue
    .line 1126
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_UngroupSelectedObject(Z)Z

    move-result v0

    .line 1127
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1128
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1130
    :cond_0
    return-void
.end method

.method public unloadObject()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1830
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->PageDoc_UnloadObject()Z

    move-result v0

    .line 1831
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 1832
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->throwUncheckedException(I)V

    .line 1834
    :cond_0
    return-void
.end method
