.class public Lcom/samsung/android/sdk/pen/recognition/preload/Signature;
.super Ljava/lang/Object;
.source "Signature.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;,
        Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;
    }
.end annotation


# static fields
.field public static final SIGNATURE_DEFAULT_MIN_SIZE:I = 0xc8

.field public static final SIGNATURE_DEFAULT_USER_ID:I = 0xa

.field public static final SIGNATURE_MAX_POINT_COUNT:I = 0x400

.field public static final SIGNATURE_MAX_REGISTRATION_NUM:I = 0x3

.field public static final SIGNATURE_VERIFICATION_LEVEL_HIGH:I = 0x2

.field public static final SIGNATURE_VERIFICATION_LEVEL_LOW:I = 0x0

.field public static final SIGNATURE_VERIFICATION_LEVEL_MEDIUM:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Signature"

.field public static final VERIFICATION_LEVEL_KEY:Ljava/lang/String; = "VerificationLevel"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

.field private final mHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

.field private mVerificationLevel:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    .line 90
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    .line 91
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    .line 92
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I

    .line 288
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHandler:Landroid/os/Handler;

    .line 25
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;I)Z
    .locals 1

    .prologue
    .line 220
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->verifySignature(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private closeSignatureEngine()Z
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v0, :cond_0

    .line 164
    const-string v0, "Signature"

    const-string v1, "Signature Engine is not Opened!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v0, 0x0

    .line 170
    :goto_0
    return v0

    .line 168
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    .line 170
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getSignatureTempPath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 95
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "PARENT_DIR":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/_tmp/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "TEMP_DIR":Ljava/lang/String;
    return-object v1
.end method

.method private openSignatureEngine()Z
    .locals 6

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->getSignatureTempPath()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "saveDirPath":Ljava/lang/String;
    const-string v0, "Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "openSignatureEngine() : saveDirPath = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    .line 144
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v2, 0x400

    const/4 v3, 0x3

    .line 145
    const/16 v4, 0xa

    .line 146
    const/16 v5, 0xc8

    .line 144
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->initHSV(Ljava/lang/String;IIII)Z

    move-result v0

    return v0
.end method

.method private registerSignature()I
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v0, :cond_0

    .line 193
    const-string v0, "Signature"

    const-string v1, "Signature Engine is not Opened!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const/4 v0, -0x1

    .line 197
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->signatureTraining(I)I

    move-result v0

    goto :goto_0
.end method

.method private unregisterSignature()Z
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->deleteRegistration(I)Z

    move-result v0

    return v0
.end method

.method private verifySignature(I)Z
    .locals 4
    .param p1, "verificationLevel"    # I

    .prologue
    const/4 v1, 0x0

    .line 221
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v2, :cond_0

    .line 222
    const-string v2, "Signature"

    const-string v3, "Signature Engine is not Opened!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :goto_0
    return v1

    .line 226
    :cond_0
    const/4 v0, 0x0

    .line 227
    .local v0, "nLevel":I
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 229
    :pswitch_0
    const/4 v0, 0x0

    .line 241
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->verification(II)Z

    move-result v1

    goto :goto_0

    .line 232
    :pswitch_1
    const/4 v0, 0x1

    .line 233
    goto :goto_1

    .line 235
    :pswitch_2
    const/4 v0, 0x2

    .line 236
    goto :goto_1

    .line 227
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getMinimumRequiredCount()I
    .locals 1

    .prologue
    .line 494
    const/4 v0, 0x3

    return v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 378
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 512
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 369
    const-string v0, "VerificationLevel"

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 370
    return-void
.end method

.method public getRegisteredCount()I
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v0, :cond_0

    .line 432
    const-string v0, "Signature"

    const-string v1, "Signature Engine is not Opened!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    const/4 v0, -0x1

    .line 436
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->getRegisteredDataCount(I)I

    move-result v0

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 304
    const-string v4, "Signature Recognition"

    const-string v5, "Load libSPenHSV.so."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    sget-boolean v4, Lcom/samsung/android/sdk/pen/Spen;->IS_SPEN_PRELOAD_MODE:Z

    if-eqz v4, :cond_1

    .line 308
    :try_start_0
    const-string v4, "SPenHSV"

    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    .line 327
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->openSignatureEngine()Z

    move-result v4

    if-nez v4, :cond_2

    .line 328
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    .line 329
    iput-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    .line 330
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Fail to load signature engine"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 309
    :catch_0
    move-exception v1

    .line 310
    .local v1, "error":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 311
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "ibSPenHSV.so is not loaded."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 314
    .end local v1    # "error":Ljava/lang/Exception;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/data/data/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/lib/libSPenHSV.so"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 315
    .local v3, "libFullName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 316
    .local v2, "libFilePath":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 318
    :try_start_1
    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 319
    :catch_1
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 321
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "ibSPenHSV.so is not loaded."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 332
    .end local v0    # "e":Ljava/lang/Throwable;
    .end local v2    # "libFilePath":Ljava/io/File;
    .end local v3    # "libFullName":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public onUnload()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 340
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->closeSignatureEngine()Z

    .line 342
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    .line 343
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    .line 344
    return-void
.end method

.method public register(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 387
    .local p1, "stroke":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v6, :cond_0

    .line 388
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Signature Engine is not Opened!"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 391
    :cond_0
    if-nez p1, :cond_1

    .line 392
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "stroke is null"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 395
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 398
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 410
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->registerSignature()I

    move-result v4

    .line 411
    .local v4, "ret":I
    if-gez v4, :cond_3

    .line 412
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Fail to register the signature!"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 401
    .end local v4    # "ret":I
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 403
    .local v0, "__stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v2

    .line 404
    .local v2, "points":[Landroid/graphics/PointF;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v5

    .line 405
    .local v5, "stamp":[I
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v3

    .line 407
    .local v3, "pressure":[F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    invoke-virtual {v6, v2, v5, v3}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->setDrawData([Landroid/graphics/PointF;[I[F)V

    goto :goto_0

    .line 414
    .end local v0    # "__stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    .end local v2    # "points":[Landroid/graphics/PointF;
    .end local v3    # "pressure":[F
    .end local v5    # "stamp":[I
    .restart local v4    # "ret":I
    :cond_3
    return-void
.end method

.method public request(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 445
    .local p1, "stroke":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    if-nez v1, :cond_0

    .line 446
    const-string v1, "Signature"

    const-string v2, "The result listener isn\'t set yet!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 451
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v1, :cond_1

    .line 452
    const-string v1, "Signature"

    const-string v2, "Signature Engine is not Opened!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 457
    :cond_1
    if-nez p1, :cond_2

    .line 458
    const-string v1, "Signature"

    const-string v2, "Input parameter \'stroke\' is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 463
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->checkRegistration(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 464
    const-string v1, "Signature"

    const-string v2, "Registered signature is not enough"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 469
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-eqz v1, :cond_4

    .line 470
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 471
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 477
    .end local v0    # "thread":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 474
    :cond_4
    const-string v1, "Signature"

    const-string v2, "The recognition engine is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "propertyMap"    # Landroid/os/Bundle;

    .prologue
    .line 352
    const-string v1, "VerificationLevel"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 353
    const-string v1, "VerificationLevel"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 354
    .local v0, "level":I
    if-ltz v0, :cond_0

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    .line 356
    :cond_0
    const/4 v0, 0x1

    .line 359
    :cond_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I

    .line 361
    .end local v0    # "level":I
    :cond_2
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    .prologue
    .line 485
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    .line 486
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 503
    const/4 v0, 0x1

    return v0
.end method

.method public unregisterAll()V
    .locals 0

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->unregisterSignature()Z

    .line 423
    return-void
.end method
