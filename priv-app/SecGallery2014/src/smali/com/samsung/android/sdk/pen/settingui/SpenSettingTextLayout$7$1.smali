.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    .line 709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectItem(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/16 v4, 0x5a0

    .line 712
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/util/DisplayMetrics;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 713
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_3

    .line 714
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V

    .line 719
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$48(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$48(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v2

    const/16 v3, 0x5f0

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$48(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v2

    const/16 v3, 0x5fc

    if-ne v2, v3, :cond_1

    .line 720
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V

    .line 726
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$48(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v2

    int-to-double v2, v2

    const-wide v4, 0x4076800000000000L    # 360.0

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 727
    .local v0, "fontPointPixel":F
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$42(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iput v2, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 728
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v3

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSize(F)V

    .line 729
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$42(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$49(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V

    .line 730
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$50(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$51(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 731
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    .line 733
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 734
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 735
    .local v1, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v2

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    .line 736
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 739
    .end local v1    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_2
    return-void

    .line 716
    .end local v0    # "fontPointPixel":F
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->localDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V

    goto/16 :goto_0

    .line 723
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V

    goto/16 :goto_1
.end method
