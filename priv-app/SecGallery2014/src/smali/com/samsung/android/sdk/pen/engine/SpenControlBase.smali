.class public abstract Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.super Landroid/view/View;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I = null

.field protected static final ALL:I = -0x1

.field private static final ANGLE_RECT_HEIGHT:I = 0x1e

.field private static final ANGLE_RECT_WIDTH:I = 0x3c

.field private static final ANGLE_TEXT_SIZE:I = 0x12

.field protected static final DEFAULT_BORDER_POINT:Ljava/lang/String; = "handler_icon"

.field protected static final DEFAULT_DEGREE_STRING:Ljava/lang/String; = "\u00b0"

.field private static final DEFAULT_DENSITY_DPI:I = 0xa0

.field protected static final DEFAULT_HANDLE_RESIZE_ICON_SIZE:I = 0x16

.field protected static final DEFAULT_HANDLE_RESIZE_ICON_SIZE_N1:I = 0x21

.field protected static final DEFAULT_HANDLE_ROTATE_ICON_SIZE:I = 0x1e

.field protected static final DEFAULT_HANDLE_ROTATE_ICON_SIZE_N1:I = 0x2d

.field protected static final DEFAULT_RESIZE_ZONE_SIZE:I = 0x7

.field protected static final DEFAULT_RESIZE_ZONE_SIZE_N1:I = 0xc

.field protected static final DEFAULT_ROTATE_POINT_BORDER:Ljava/lang/String; = "handler_icon_rotate"

.field protected static final DEFAULT_ROTATION_ZONE_SIZE:I = 0x19

.field protected static final DEFAULT_ROTATION_ZONE_SIZE_N1:I = 0x26

.field protected static final DIMMING_BG_COLOR:I = 0x40000000

.field protected static final FLIP_DIRECTION_HORIZONTAL:I = 0x2

.field protected static final FLIP_DIRECTION_NONE:I = 0x0

.field protected static final FLIP_DIRECTION_VERTICAL:I = 0x1

.field protected static final MIN_RESIZE_ZONE_SIZE:I = 0x5

.field protected static final NO_OBJECT:I = -0x1

.field protected static final SIGMA:F = 1.0E-4f

.field public static final STYLE_BORDER_NONE:I = 0x1

.field public static final STYLE_BORDER_NONE_ACTION_NONE:I = 0x3

.field public static final STYLE_BORDER_OBJECT:I = 0x0

.field public static final STYLE_BORDER_STATIC:I = 0x2

.field protected static final TOUCH_ZONE_BOTTOM:I = 0x7

.field protected static final TOUCH_ZONE_BOTTOM_LEFT:I = 0x6

.field protected static final TOUCH_ZONE_BOTTOM_RIGHT:I = 0x8

.field protected static final TOUCH_ZONE_CENTER:I = 0x9

.field protected static final TOUCH_ZONE_LEFT:I = 0x4

.field protected static final TOUCH_ZONE_MAX:I = 0xa

.field protected static final TOUCH_ZONE_NONE:I = -0x1

.field protected static final TOUCH_ZONE_RIGHT:I = 0x5

.field protected static final TOUCH_ZONE_ROTATE:I = 0x0

.field protected static final TOUCH_ZONE_TOP:I = 0x2

.field protected static final TOUCH_ZONE_TOP_LEFT:I = 0x1

.field protected static final TOUCH_ZONE_TOP_RIGHT:I = 0x3

.field private static final TRIVIAL_MOVING_CRITERIA:I = 0x14

.field private static arabicChars:[C = null

.field private static final mDeltaEdge:I = 0x1

.field private static mObjectOutlineEnable:Z


# instance fields
.field protected mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

.field private final mControlBaseContext:Landroid/content/Context;

.field private mDensityDpi:I

.field private mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

.field protected mIsClosed:Z

.field private mIsDim:Z

.field private mIsFirstTouch:Z

.field private mIsFlipDirectionHorizontal:Z

.field private mIsFlipDirectionVertical:Z

.field protected mIsObjectChange:Z

.field protected mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

.field protected mMaximumResizeRect:Landroid/graphics/RectF;

.field protected mMinimumResizeRect:Landroid/graphics/RectF;

.field private mObjectBaseList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field private mOnePT:F

.field private mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

.field private mOrgRectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field protected mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPageDocHeight:I

.field private mPageDocWidth:I

.field private mPanBackup:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field private mPointDown:Landroid/graphics/PointF;

.field private mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

.field private mRectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private mResourceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field protected mRotateAngle:F

.field private final mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

.field private mStyle:I

.field private mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

.field protected mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

.field private mTempRectF:Landroid/graphics/RectF;

.field private mTmpMatrix:Landroid/graphics/Matrix;

.field private mTouchEnable:Z

.field protected mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

.field private mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

.field protected mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

.field protected mTrivialMovingEn:Z


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT()[I
    .locals 3

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->values()[Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    .line 352
    const/16 v0, 0xa

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->arabicChars:[C

    return-void

    nop

    :array_0
    .array-data 2
        0x660s
        0x661s
        0x662s
        0x663s
        0x664s
        0x665s
        0x666s
        0x667s
        0x668s
        0x669s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3781
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 292
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    .line 298
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 304
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    .line 311
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    .line 312
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    .line 313
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    .line 315
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    .line 318
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    .line 325
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    .line 326
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    .line 331
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    .line 343
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    .line 347
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 348
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 757
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    .line 3782
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mControlBaseContext:Landroid/content/Context;

    .line 3783
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->initialize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 3784
    return-void
.end method

.method private Increase2MinimumRect(Landroid/graphics/RectF;)Z
    .locals 3
    .param p1, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 2125
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v1, :cond_0

    .line 2126
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 2127
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2130
    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 2131
    .local v0, "rect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2133
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 2134
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 2135
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 2138
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 2139
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 2141
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2143
    const/4 v1, 0x1

    .line 2145
    :goto_0
    return v1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mControlBaseContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    return v0
.end method

.method static synthetic access$2()[C
    .locals 1

    .prologue
    .line 352
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->arabicChars:[C

    return-object v0
.end method

.method private adjustObjectRect(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Landroid/graphics/RectF;)V
    .locals 13
    .param p1, "obj"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p2, "objectRect"    # Landroid/graphics/RectF;

    .prologue
    .line 3839
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v11

    if-nez v11, :cond_1

    .line 3898
    :cond_0
    :goto_0
    return-void

    .line 3843
    :cond_1
    const/high16 v7, 0x41200000    # 10.0f

    .line 3844
    .local v7, "defaultMinWidth":F
    const/high16 v6, 0x41200000    # 10.0f

    .line 3845
    .local v6, "defaultMinHeight":F
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDocWidth:I

    mul-int/lit8 v11, v11, 0x2

    int-to-float v5, v11

    .line 3846
    .local v5, "defaultMaxWidht":F
    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDocHeight:I

    mul-int/lit8 v11, v11, 0x2

    int-to-float v4, v11

    .line 3848
    .local v4, "defaultMaxHeight":F
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinWidth()F

    move-result v3

    .line 3849
    .local v3, "currMinWidth":F
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxWidth()F

    move-result v1

    .line 3850
    .local v1, "currMaxWidth":F
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinHeight()F

    move-result v2

    .line 3851
    .local v2, "currMinHeight":F
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxHeight()F

    move-result v0

    .line 3853
    .local v0, "currMaxHeight":F
    const/4 v11, 0x0

    cmpg-float v11, v3, v11

    if-lez v11, :cond_0

    const/4 v11, 0x0

    cmpg-float v11, v2, v11

    if-lez v11, :cond_0

    const/4 v11, 0x0

    cmpg-float v11, v1, v11

    if-lez v11, :cond_0

    const/4 v11, 0x0

    cmpg-float v11, v0, v11

    if-lez v11, :cond_0

    .line 3859
    sub-float v11, v7, v3

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_2

    sub-float v11, v5, v1

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_2

    .line 3860
    sub-float v11, v6, v2

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_2

    .line 3861
    sub-float v11, v4, v0

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v11, v11, v12

    if-ltz v11, :cond_0

    .line 3865
    :cond_2
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v10

    .line 3866
    .local v10, "width":F
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v8

    .line 3867
    .local v8, "height":F
    const/4 v9, 0x0

    .line 3869
    .local v9, "isRectChange":Z
    cmpg-float v11, v10, v3

    if-gez v11, :cond_3

    .line 3870
    move v10, v3

    .line 3871
    const/4 v9, 0x1

    .line 3874
    :cond_3
    cmpg-float v11, v8, v2

    if-gez v11, :cond_4

    .line 3875
    move v8, v2

    .line 3876
    const/4 v9, 0x1

    .line 3878
    :cond_4
    const/4 v11, 0x0

    cmpl-float v11, v1, v11

    if-lez v11, :cond_5

    cmpl-float v11, v1, v3

    if-lez v11, :cond_5

    cmpl-float v11, v10, v1

    if-lez v11, :cond_5

    .line 3879
    move v10, v1

    .line 3880
    const/4 v9, 0x1

    .line 3882
    :cond_5
    const/4 v11, 0x0

    cmpl-float v11, v0, v11

    if-lez v11, :cond_6

    cmpl-float v11, v0, v2

    if-lez v11, :cond_6

    cmpl-float v11, v8, v0

    if-lez v11, :cond_6

    .line 3883
    move v8, v0

    .line 3884
    const/4 v9, 0x1

    .line 3887
    :cond_6
    if-eqz v9, :cond_0

    .line 3891
    iget v11, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v11, v10

    iput v11, p2, Landroid/graphics/RectF;->right:F

    .line 3892
    iget v11, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v11, v8

    iput v11, p2, Landroid/graphics/RectF;->bottom:F

    .line 3894
    const/4 v11, 0x0

    invoke-virtual {p1, p2, v11}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 3895
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v11, :cond_0

    .line 3896
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-interface {v11, v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onObjectChanged(Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method private calculateContextMenuPosition(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 23
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1134
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 1135
    .local v13, "retRect":Landroid/graphics/Rect;
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v12

    .line 1136
    .local v12, "rectBorder":Landroid/graphics/RectF;
    if-nez v12, :cond_0

    .line 1137
    const/4 v13, 0x0

    .line 1199
    .end local v13    # "retRect":Landroid/graphics/Rect;
    :goto_0
    return-object v13

    .line 1140
    .restart local v13    # "retRect":Landroid/graphics/Rect;
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v20, v20, v21

    iget v0, v12, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    add-float v20, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    move-object/from16 v21, v0

    .line 1141
    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v1, v12}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    .line 1140
    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v8, v0

    .line 1143
    .local v8, "knopHeight":I
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    move-result v18

    int-to-float v0, v8

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v9, v0

    .line 1145
    .local v9, "knopY":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 1147
    .local v11, "parentLayout":Landroid/view/ViewGroup;
    if-nez v11, :cond_1

    .line 1148
    const/4 v13, 0x0

    goto :goto_0

    .line 1151
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v18, v0

    if-nez v18, :cond_2

    .line 1152
    const/4 v13, 0x0

    goto :goto_0

    .line 1155
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getRect()Landroid/graphics/Rect;

    move-result-object v10

    .line 1156
    .local v10, "menuRect":Landroid/graphics/Rect;
    if-nez v10, :cond_3

    .line 1157
    const/4 v13, 0x0

    goto :goto_0

    .line 1160
    :cond_3
    const/16 v16, 0x0

    .local v16, "y1":I
    const/16 v17, 0x0

    .local v17, "y2":I
    const/4 v15, 0x0

    .local v15, "y":I
    const/4 v14, 0x0

    .line 1161
    .local v14, "x":I
    const/high16 v18, 0x40a00000    # 5.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v6, v0

    .line 1163
    .local v6, "delta_padding":I
    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    sub-int v18, v18, v6

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->left:I

    .line 1164
    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    sub-int v18, v18, v6

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->top:I

    .line 1165
    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    add-int v18, v18, v6

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->right:I

    .line 1166
    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    add-int v18, v18, v6

    move/from16 v0, v18

    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    .line 1168
    const/high16 v18, 0x40e00000    # 7.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v7, v0

    .line 1169
    .local v7, "gap":I
    const/high16 v18, 0x40a00000    # 5.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v5, v0

    .line 1170
    .local v5, "delta":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v3, v0

    .line 1172
    .local v3, "centerTopX":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v0, v9, :cond_6

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 1173
    .local v4, "centerTopY":I
    :goto_1
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v0, v9, :cond_7

    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1175
    .local v2, "centerBottomY":I
    :goto_2
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    sub-int v14, v3, v18

    .line 1176
    if-gez v14, :cond_8

    .line 1177
    const/4 v14, 0x0

    .line 1182
    :cond_4
    :goto_3
    sub-int v18, v4, v7

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    sub-int v18, v18, v19

    add-int v16, v18, v5

    .line 1183
    add-int v18, v2, v7

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    add-int v18, v18, v19

    sub-int v17, v18, v5

    .line 1185
    move/from16 v15, v16

    .line 1186
    if-gez v16, :cond_a

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_a

    .line 1187
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    move-result v18

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-lez v18, :cond_9

    .line 1188
    const/4 v15, 0x0

    .line 1198
    :cond_5
    :goto_4
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    add-int v18, v18, v14

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    add-int v19, v19, v15

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v14, v15, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .end local v2    # "centerBottomY":I
    .end local v4    # "centerTopY":I
    :cond_6
    move v4, v9

    .line 1172
    goto :goto_1

    .restart local v4    # "centerTopY":I
    :cond_7
    move v2, v9

    .line 1173
    goto :goto_2

    .line 1178
    .restart local v2    # "centerBottomY":I
    :cond_8
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v18

    add-int v18, v18, v14

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_4

    .line 1179
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getWidth()I

    move-result v18

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v19

    sub-int v14, v18, v19

    goto :goto_3

    .line 1190
    :cond_9
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v19

    sub-int v18, v18, v19

    sub-int v15, v18, v5

    .line 1192
    goto :goto_4

    :cond_a
    if-gez v16, :cond_b

    .line 1193
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v18

    sub-int v15, v17, v18

    .line 1194
    goto :goto_4

    :cond_b
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getHeight()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_5

    .line 1195
    sub-int v15, v16, v5

    goto :goto_4
.end method

.method private checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z
    .locals 22
    .param p1, "rectF"    # Landroid/graphics/RectF;
    .param p2, "index"    # I
    .param p3, "angle"    # F

    .prologue
    .line 1277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 1278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 1279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1282
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v8

    .line 1283
    .local v8, "pivotX":F
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v9

    .line 1286
    .local v9, "pivotY":F
    new-instance v15, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v15, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1287
    .local v15, "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v8, v9, v1, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v15

    .line 1289
    new-instance v16, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1290
    .local v16, "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p3

    move-object/from16 v2, v16

    invoke-virtual {v0, v8, v9, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v16

    .line 1292
    new-instance v5, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1293
    .local v5, "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v8, v9, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 1295
    new-instance v6, Landroid/graphics/PointF;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1296
    .local v6, "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v8, v9, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 1298
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v10

    .line 1299
    .local v10, "points":[F
    const/16 v17, 0x0

    aget v7, v10, v17

    .line 1300
    .local v7, "left":F
    const/16 v17, 0x1

    aget v14, v10, v17

    .line 1301
    .local v14, "top":F
    const/16 v17, 0x2

    aget v11, v10, v17

    .line 1302
    .local v11, "right":F
    const/16 v17, 0x3

    aget v4, v10, v17

    .line 1304
    .local v4, "bottom":F
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12, v7, v14, v11, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1305
    .local v12, "tmpBoundBoxRect":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v17, v0

    const/high16 v18, 0x3f800000    # 1.0f

    add-float v17, v17, v18

    .line 1306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    .line 1307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    sub-float v20, v20, v21

    .line 1305
    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1309
    .local v13, "tmpFrameRect":Landroid/graphics/RectF;
    invoke-virtual {v13, v12}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v17

    if-nez v17, :cond_1

    invoke-virtual {v12, v13}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1310
    :cond_1
    const/16 v17, 0x0

    .line 1313
    :goto_0
    return v17

    :cond_2
    const/16 v17, 0x1

    goto :goto_0
.end method

.method private drawDimmingWindow(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 850
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    if-eqz v0, :cond_0

    .line 851
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 853
    :cond_0
    return-void
.end method

.method private drawHighlightRect(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 4351
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 4371
    :goto_0
    return-void

    .line 4354
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    .line 4356
    .local v1, "paint":Landroid/graphics/Paint;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4357
    .local v0, "dstRect":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 4359
    .local v2, "srcRect":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 4361
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v3, :cond_1

    .line 4362
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 4364
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 4366
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 4367
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4369
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 4370
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method private drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V
    .locals 31
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "objectStroke"    # Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .prologue
    .line 4434
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 4674
    :cond_0
    :goto_0
    return-void

    .line 4437
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getType()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 4442
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v23

    .line 4443
    .local v23, "penName":Ljava/lang/String;
    if-eqz v23, :cond_0

    .line 4447
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 4451
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 4453
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v3, :cond_2

    .line 4454
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 4457
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getXPoints()[F

    move-result-object v26

    .line 4458
    .local v26, "pointXs":[F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getYPoints()[F

    move-result-object v27

    .line 4459
    .local v27, "pointYs":[F
    if-eqz v26, :cond_0

    if-eqz v27, :cond_0

    .line 4462
    move-object/from16 v0, v26

    array-length v13, v0

    .line 4463
    .local v13, "lenX":I
    move-object/from16 v0, v27

    array-length v14, v0

    .line 4464
    .local v14, "lenY":I
    if-le v13, v14, :cond_6

    move v12, v14

    .line 4465
    .local v12, "len":I
    :goto_1
    new-array v0, v12, [Landroid/graphics/PointF;

    move-object/from16 v16, v0

    .line 4466
    .local v16, "myPointList":[Landroid/graphics/PointF;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    move-object/from16 v0, v16

    array-length v3, v0

    if-lt v11, v3, :cond_7

    .line 4471
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v22

    .line 4472
    .local v22, "paint":Landroid/graphics/Paint;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4473
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 4475
    .local v2, "myPath":Landroid/graphics/Path;
    const/4 v11, 0x0

    :goto_3
    if-lt v11, v12, :cond_8

    .line 4482
    const/16 v3, 0x64

    if-gt v12, v3, :cond_1e

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->isCurveEnabled()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 4483
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 4484
    .local v20, "orgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    const/4 v11, 0x0

    :goto_4
    if-lt v11, v12, :cond_9

    .line 4490
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v21

    .line 4492
    .local v21, "orgSize":I
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 4493
    .local v17, "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 4494
    const/4 v11, 0x0

    :goto_5
    add-int/lit8 v3, v21, -0x1

    if-lt v11, v3, :cond_b

    .line 4509
    :cond_3
    :goto_6
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 4511
    .local v18, "newSize":I
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    aget-object v4, v16, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 4513
    new-instance v30, Landroid/graphics/PointF;

    invoke-direct/range {v30 .. v30}, Landroid/graphics/PointF;-><init>()V

    .line 4514
    .local v30, "startPoint":Landroid/graphics/PointF;
    new-instance v10, Landroid/graphics/PointF;

    invoke-direct {v10}, Landroid/graphics/PointF;-><init>()V

    .line 4515
    .local v10, "controlPoint":Landroid/graphics/PointF;
    new-instance v19, Landroid/graphics/PointF;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/PointF;-><init>()V

    .line 4517
    .local v19, "nextPoint":Landroid/graphics/PointF;
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4519
    const/4 v3, 0x3

    move/from16 v0, v21

    if-lt v0, v3, :cond_1c

    .line 4520
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Marker"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 4521
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Brush"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 4522
    :cond_4
    new-instance v9, Landroid/graphics/PointF;

    invoke-direct {v9}, Landroid/graphics/PointF;-><init>()V

    .line 4523
    .local v9, "calcPoint":Landroid/graphics/PointF;
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v9, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getStartEndBitmapCalcPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;F)V

    .line 4524
    new-instance v28, Landroid/graphics/PointF;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/PointF;-><init>()V

    .line 4526
    .local v28, "prevMiddlePoint":Landroid/graphics/PointF;
    iget v4, v9, Landroid/graphics/PointF;->x:F

    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v28

    iput v3, v0, Landroid/graphics/PointF;->x:F

    .line 4527
    iget v4, v9, Landroid/graphics/PointF;->y:F

    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v28

    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 4529
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v4, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 4531
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v4, v9, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4532
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v4, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4533
    move-object/from16 v0, v28

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v28

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4535
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_e

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_e

    .line 4536
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4537
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4542
    :goto_7
    const/16 v29, 0x0

    .line 4543
    .local v29, "prevPoint":Landroid/graphics/PointF;
    const/4 v11, 0x1

    :goto_8
    add-int/lit8 v3, v21, -0x1

    if-lt v11, v3, :cond_f

    .line 4565
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4566
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v29

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4567
    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4569
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_11

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_11

    .line 4570
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4571
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4644
    .end local v9    # "calcPoint":Landroid/graphics/PointF;
    .end local v28    # "prevMiddlePoint":Landroid/graphics/PointF;
    .end local v29    # "prevPoint":Landroid/graphics/PointF;
    :cond_5
    :goto_9
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 4645
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4646
    const/4 v3, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4647
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 4648
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .end local v2    # "myPath":Landroid/graphics/Path;
    .end local v10    # "controlPoint":Landroid/graphics/PointF;
    .end local v11    # "i":I
    .end local v12    # "len":I
    .end local v16    # "myPointList":[Landroid/graphics/PointF;
    .end local v17    # "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v18    # "newSize":I
    .end local v19    # "nextPoint":Landroid/graphics/PointF;
    .end local v20    # "orgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v21    # "orgSize":I
    .end local v22    # "paint":Landroid/graphics/Paint;
    .end local v30    # "startPoint":Landroid/graphics/PointF;
    :cond_6
    move v12, v13

    .line 4464
    goto/16 :goto_1

    .line 4467
    .restart local v11    # "i":I
    .restart local v12    # "len":I
    .restart local v16    # "myPointList":[Landroid/graphics/PointF;
    :cond_7
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    aput-object v3, v16, v11

    .line 4468
    aget-object v3, v16, v11

    aget v4, v26, v11

    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 4469
    aget-object v3, v16, v11

    aget v4, v27, v11

    iput v4, v3, Landroid/graphics/PointF;->y:F

    .line 4466
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 4476
    .restart local v2    # "myPath":Landroid/graphics/Path;
    .restart local v22    # "paint":Landroid/graphics/Paint;
    :cond_8
    aget-object v3, v16, v11

    aget-object v4, v16, v11

    iget v4, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v5

    .line 4477
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v5

    .line 4476
    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 4478
    aget-object v3, v16, v11

    aget-object v4, v16, v11

    iget v4, v4, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v5

    .line 4479
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v5

    .line 4478
    iput v4, v3, Landroid/graphics/PointF;->y:F

    .line 4475
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 4485
    .restart local v20    # "orgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    :cond_9
    aget-object v3, v16, v11

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isContained(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 4486
    aget-object v3, v16, v11

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4484
    :cond_a
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 4495
    .restart local v17    # "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .restart local v21    # "orgSize":I
    :cond_b
    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v3, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v6, v3, Landroid/graphics/PointF;->y:F

    .line 4496
    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 4495
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4494
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_5

    .line 4499
    :cond_c
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4501
    const/4 v11, 0x1

    :goto_a
    add-int/lit8 v3, v21, -0x2

    if-lt v11, v3, :cond_d

    .line 4505
    const/4 v3, 0x1

    move/from16 v0, v21

    if-le v0, v3, :cond_3

    .line 4506
    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 4502
    :cond_d
    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x2

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v3, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v6, v3, Landroid/graphics/PointF;->y:F

    .line 4503
    add-int/lit8 v3, v11, 0x2

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 4502
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4501
    add-int/lit8 v11, v11, 0x2

    goto :goto_a

    .line 4539
    .restart local v9    # "calcPoint":Landroid/graphics/PointF;
    .restart local v10    # "controlPoint":Landroid/graphics/PointF;
    .restart local v18    # "newSize":I
    .restart local v19    # "nextPoint":Landroid/graphics/PointF;
    .restart local v28    # "prevMiddlePoint":Landroid/graphics/PointF;
    .restart local v30    # "startPoint":Landroid/graphics/PointF;
    :cond_e
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto/16 :goto_7

    .line 4544
    .restart local v29    # "prevPoint":Landroid/graphics/PointF;
    :cond_f
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    .end local v29    # "prevPoint":Landroid/graphics/PointF;
    check-cast v29, Landroid/graphics/PointF;

    .line 4546
    .restart local v29    # "prevPoint":Landroid/graphics/PointF;
    new-instance v15, Landroid/graphics/PointF;

    invoke-direct {v15}, Landroid/graphics/PointF;-><init>()V

    .line 4547
    .local v15, "middlePoint":Landroid/graphics/PointF;
    move-object/from16 v0, v29

    iget v4, v0, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, v15, Landroid/graphics/PointF;->x:F

    .line 4548
    move-object/from16 v0, v29

    iget v4, v0, Landroid/graphics/PointF;->y:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v4

    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, v15, Landroid/graphics/PointF;->y:F

    .line 4550
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4551
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v29

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4552
    iget v3, v15, Landroid/graphics/PointF;->x:F

    iget v4, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4554
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_10

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_10

    .line 4555
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4556
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4561
    :goto_b
    move-object/from16 v29, v15

    .line 4543
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_8

    .line 4558
    :cond_10
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_b

    .line 4573
    .end local v15    # "middlePoint":Landroid/graphics/PointF;
    :cond_11
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto/16 :goto_9

    .line 4576
    .end local v9    # "calcPoint":Landroid/graphics/PointF;
    .end local v28    # "prevMiddlePoint":Landroid/graphics/PointF;
    .end local v29    # "prevPoint":Landroid/graphics/PointF;
    :cond_12
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 4577
    const/4 v11, 0x0

    :goto_c
    move/from16 v0, v18

    if-lt v11, v0, :cond_13

    .line 4591
    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_9

    .line 4578
    :cond_13
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4579
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4581
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14

    .line 4582
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4583
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4588
    :goto_d
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4577
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_c

    .line 4585
    :cond_14
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_d

    .line 4592
    :cond_15
    const-string v3, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 4593
    rem-int/lit8 v3, v21, 0x2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_17

    .line 4594
    const/4 v11, 0x0

    :goto_e
    add-int/lit8 v3, v18, -0x1

    if-ge v11, v3, :cond_5

    .line 4595
    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4596
    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4598
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_16

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_16

    .line 4599
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4600
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4605
    :goto_f
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4594
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_e

    .line 4602
    :cond_16
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_f

    .line 4608
    :cond_17
    const/4 v11, 0x0

    :goto_10
    add-int/lit8 v3, v18, -0x2

    if-lt v11, v3, :cond_18

    .line 4621
    new-instance v25, Landroid/graphics/PointF;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/PointF;-><init>()V

    .line 4622
    .local v25, "point3":Landroid/graphics/PointF;
    add-int/lit8 v3, v18, -0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/graphics/PointF;

    .line 4623
    .local v24, "point2":Landroid/graphics/PointF;
    add-int/lit8 v3, v21, -0x3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    .end local v25    # "point3":Landroid/graphics/PointF;
    check-cast v25, Landroid/graphics/PointF;

    .line 4625
    .restart local v25    # "point3":Landroid/graphics/PointF;
    move-object/from16 v0, v24

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/PointF;->y:F

    add-int/lit8 v7, v18, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    .line 4626
    add-int/lit8 v8, v18, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    .line 4625
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_9

    .line 4609
    .end local v24    # "point2":Landroid/graphics/PointF;
    .end local v25    # "point3":Landroid/graphics/PointF;
    :cond_18
    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4610
    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4612
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_19

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_19

    .line 4613
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4614
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4619
    :goto_11
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4608
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_10

    .line 4616
    :cond_19
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_11

    .line 4629
    :cond_1a
    const/4 v11, 0x0

    :goto_12
    add-int/lit8 v3, v18, -0x1

    if-ge v11, v3, :cond_5

    .line 4630
    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v10, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4631
    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 4633
    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1b

    move-object/from16 v0, v30

    iget v3, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41700000    # 15.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1b

    .line 4634
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4635
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4640
    :goto_13
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 4629
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_12

    .line 4637
    :cond_1b
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v10, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_13

    .line 4649
    :cond_1c
    const/4 v3, 0x2

    move/from16 v0, v21

    if-ne v0, v3, :cond_1d

    .line 4650
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4651
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 4653
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4654
    const/4 v3, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4655
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 4656
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 4657
    :cond_1d
    const/4 v3, 0x1

    move/from16 v0, v21

    if-ne v0, v3, :cond_0

    .line 4658
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4659
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    aget-object v4, v16, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v5

    .line 4660
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v5, v5

    .line 4659
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 4663
    .end local v10    # "controlPoint":Landroid/graphics/PointF;
    .end local v17    # "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v18    # "newSize":I
    .end local v19    # "nextPoint":Landroid/graphics/PointF;
    .end local v20    # "orgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v21    # "orgSize":I
    .end local v30    # "startPoint":Landroid/graphics/PointF;
    :cond_1e
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    aget-object v4, v16, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->setLastPoint(FF)V

    .line 4664
    const/4 v11, 0x0

    :goto_14
    if-lt v11, v12, :cond_1f

    .line 4667
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 4669
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4670
    const/4 v3, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4671
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 4672
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 4665
    :cond_1f
    aget-object v3, v16, v11

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aget-object v4, v16, v11

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 4664
    add-int/lit8 v11, v11, 0x1

    goto :goto_14
.end method

.method private findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F
    .locals 7
    .param p1, "p1"    # Landroid/graphics/PointF;
    .param p2, "oppo_p1"    # Landroid/graphics/PointF;

    .prologue
    .line 2257
    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_0

    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 2258
    .local v1, "left":F
    :goto_0
    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    iget v3, p1, Landroid/graphics/PointF;->x:F

    .line 2259
    .local v3, "right":F
    :goto_1
    iget v5, p1, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_2

    iget v4, p1, Landroid/graphics/PointF;->y:F

    .line 2260
    .local v4, "top":F
    :goto_2
    iget v5, p1, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    iget v0, p1, Landroid/graphics/PointF;->y:F

    .line 2262
    .local v0, "bottom":F
    :goto_3
    const/4 v5, 0x4

    new-array v2, v5, [F

    const/4 v5, 0x0

    aput v1, v2, v5

    const/4 v5, 0x1

    aput v4, v2, v5

    const/4 v5, 0x2

    aput v3, v2, v5

    const/4 v5, 0x3

    aput v0, v2, v5

    .line 2264
    .local v2, "ret":[F
    return-object v2

    .line 2257
    .end local v0    # "bottom":F
    .end local v1    # "left":F
    .end local v2    # "ret":[F
    .end local v3    # "right":F
    .end local v4    # "top":F
    :cond_0
    iget v1, p2, Landroid/graphics/PointF;->x:F

    goto :goto_0

    .line 2258
    .restart local v1    # "left":F
    :cond_1
    iget v3, p2, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 2259
    .restart local v3    # "right":F
    :cond_2
    iget v4, p2, Landroid/graphics/PointF;->y:F

    goto :goto_2

    .line 2260
    .restart local v4    # "top":F
    :cond_3
    iget v0, p2, Landroid/graphics/PointF;->y:F

    goto :goto_3
.end method

.method private findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F
    .locals 7
    .param p1, "topleft"    # Landroid/graphics/PointF;
    .param p2, "topright"    # Landroid/graphics/PointF;
    .param p3, "bottomleft"    # Landroid/graphics/PointF;
    .param p4, "bottomright"    # Landroid/graphics/PointF;

    .prologue
    .line 2233
    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_8

    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 2234
    .local v1, "left":F
    :goto_0
    iget v5, p3, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v1

    if-gtz v5, :cond_0

    iget v1, p3, Landroid/graphics/PointF;->x:F

    .line 2235
    :cond_0
    iget v5, p4, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v1

    if-gtz v5, :cond_1

    iget v1, p4, Landroid/graphics/PointF;->x:F

    .line 2237
    :cond_1
    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_9

    iget v3, p1, Landroid/graphics/PointF;->x:F

    .line 2238
    .local v3, "right":F
    :goto_1
    iget v5, p3, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v3

    if-ltz v5, :cond_2

    iget v3, p3, Landroid/graphics/PointF;->x:F

    .line 2239
    :cond_2
    iget v5, p4, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v3

    if-ltz v5, :cond_3

    iget v3, p4, Landroid/graphics/PointF;->x:F

    .line 2241
    :cond_3
    iget v5, p1, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_a

    iget v4, p1, Landroid/graphics/PointF;->y:F

    .line 2242
    .local v4, "top":F
    :goto_2
    iget v5, p3, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v4

    if-gtz v5, :cond_4

    iget v4, p3, Landroid/graphics/PointF;->y:F

    .line 2243
    :cond_4
    iget v5, p4, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v4

    if-gtz v5, :cond_5

    iget v4, p4, Landroid/graphics/PointF;->y:F

    .line 2245
    :cond_5
    iget v5, p1, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_b

    iget v0, p1, Landroid/graphics/PointF;->y:F

    .line 2246
    .local v0, "bottom":F
    :goto_3
    iget v5, p3, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v0

    if-ltz v5, :cond_6

    iget v0, p3, Landroid/graphics/PointF;->y:F

    .line 2247
    :cond_6
    iget v5, p4, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v0

    if-ltz v5, :cond_7

    iget v0, p4, Landroid/graphics/PointF;->y:F

    .line 2249
    :cond_7
    const/4 v5, 0x4

    new-array v2, v5, [F

    const/4 v5, 0x0

    aput v1, v2, v5

    const/4 v5, 0x1

    aput v4, v2, v5

    const/4 v5, 0x2

    aput v3, v2, v5

    const/4 v5, 0x3

    aput v0, v2, v5

    .line 2251
    .local v2, "ret":[F
    return-object v2

    .line 2233
    .end local v0    # "bottom":F
    .end local v1    # "left":F
    .end local v2    # "ret":[F
    .end local v3    # "right":F
    .end local v4    # "top":F
    :cond_8
    iget v1, p2, Landroid/graphics/PointF;->x:F

    goto :goto_0

    .line 2237
    .restart local v1    # "left":F
    :cond_9
    iget v3, p2, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 2241
    .restart local v3    # "right":F
    :cond_a
    iget v4, p2, Landroid/graphics/PointF;->y:F

    goto :goto_2

    .line 2245
    .restart local v4    # "top":F
    :cond_b
    iget v0, p2, Landroid/graphics/PointF;->y:F

    goto :goto_3
.end method

.method private findResizeRate(Landroid/graphics/PointF;Landroid/graphics/RectF;)Landroid/graphics/PointF;
    .locals 5
    .param p1, "diff_rotated"    # Landroid/graphics/PointF;
    .param p2, "rectStart"    # Landroid/graphics/RectF;

    .prologue
    const v4, 0x38d1b717    # 1.0E-4f

    .line 1020
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 1021
    .local v1, "resizeRate":Landroid/graphics/PointF;
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 1022
    .local v2, "width":F
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 1023
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1025
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 1026
    .local v0, "height":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 1027
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1030
    :cond_1
    iget v3, p1, Landroid/graphics/PointF;->x:F

    div-float/2addr v3, v2

    iput v3, v1, Landroid/graphics/PointF;->x:F

    .line 1031
    iget v3, p1, Landroid/graphics/PointF;->y:F

    div-float/2addr v3, v0

    iput v3, v1, Landroid/graphics/PointF;->y:F

    .line 1032
    return-object v1
.end method

.method private getBorderAngle(I)F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 834
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 835
    :cond_0
    const/4 v0, 0x0

    .line 838
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v0

    goto :goto_0
.end method

.method private getBorderRect(I)Landroid/graphics/RectF;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 818
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 819
    const/4 v0, 0x0

    .line 830
    :goto_0
    return-object v0

    .line 822
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 824
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v1, :cond_1

    .line 825
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 828
    :cond_1
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 829
    .local v0, "relativeRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    goto :goto_0
.end method

.method private getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;
    .locals 2
    .param p1, "touchZone"    # I
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 1824
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1825
    .local v0, "result":Landroid/graphics/PointF;
    packed-switch p1, :pswitch_data_0

    .line 1864
    :goto_0
    :pswitch_0
    return-object v0

    .line 1827
    :pswitch_1
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1828
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1831
    :pswitch_2
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1832
    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1835
    :pswitch_3
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1836
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1839
    :pswitch_4
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1840
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1843
    :pswitch_5
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1844
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1847
    :pswitch_6
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1848
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1851
    :pswitch_7
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1852
    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1855
    :pswitch_8
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1856
    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1859
    :pswitch_9
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1860
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 1825
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_9
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_2
        :pswitch_7
    .end packed-switch
.end method

.method private getOffsetWhenRotation(ILandroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/PointF;
    .locals 14
    .param p1, "touchZone"    # I
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "dscRect"    # Landroid/graphics/RectF;
    .param p4, "degree"    # F

    .prologue
    .line 1807
    new-instance v11, Landroid/graphics/PointF;

    invoke-direct {v11}, Landroid/graphics/PointF;-><init>()V

    .line 1809
    .local v11, "offset":Landroid/graphics/PointF;
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v13

    .line 1810
    .local v13, "srcTouchPoint":Landroid/graphics/PointF;
    iget v3, v13, Landroid/graphics/PointF;->x:F

    float-to-int v4, v3

    iget v3, v13, Landroid/graphics/PointF;->y:F

    float-to-int v5, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    .line 1811
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    move/from16 v0, p4

    float-to-double v8, v0

    move-object v3, p0

    .line 1810
    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v10

    .line 1813
    .local v10, "newTouchPoint":Landroid/graphics/PointF;
    move-object/from16 v0, p3

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v12

    .line 1814
    .local v12, "resizeTouchPoint":Landroid/graphics/PointF;
    iget v3, v12, Landroid/graphics/PointF;->x:F

    float-to-int v4, v3

    iget v3, v12, Landroid/graphics/PointF;->y:F

    float-to-int v5, v3

    .line 1815
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    move/from16 v0, p4

    float-to-double v8, v0

    move-object v3, p0

    .line 1814
    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v2

    .line 1817
    .local v2, "newResizeTouchPoint":Landroid/graphics/PointF;
    iget v3, v10, Landroid/graphics/PointF;->x:F

    iget v4, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    iput v3, v11, Landroid/graphics/PointF;->x:F

    .line 1818
    iget v3, v10, Landroid/graphics/PointF;->y:F

    iget v4, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    iput v3, v11, Landroid/graphics/PointF;->y:F

    .line 1820
    return-object v11
.end method

.method private getRotateable(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 842
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 843
    :cond_0
    const/4 v0, 0x0

    .line 846
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v0

    goto :goto_0
.end method

.method private initialize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 10
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/16 v9, 0xa0

    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 856
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 857
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v4

    if-nez v4, :cond_1

    .line 900
    :cond_0
    :goto_0
    return-void

    .line 861
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDocWidth:I

    .line 862
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDocHeight:I

    .line 864
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 865
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    .line 866
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    .line 868
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    .line 869
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    .line 870
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    .line 871
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    .line 872
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    .line 873
    new-instance v4, Ljava/util/Hashtable;

    invoke-direct {v4}, Ljava/util/Hashtable;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    .line 874
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 875
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    .line 876
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    .line 877
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    .line 878
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    .line 879
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 880
    .local v0, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    .line 882
    const/high16 v4, 0x41200000    # 10.0f

    invoke-static {v6, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    .line 883
    .local v3, "minResizeEdge":F
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    invoke-static {v6, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    mul-float v2, v4, v7

    .line 886
    .local v2, "maxResizeWidth":F
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    .line 885
    invoke-static {v6, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    mul-float v1, v4, v7

    .line 887
    .local v1, "maxResizeHeight":F
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v5, v5, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    .line 888
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v5, v5, v2, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    .line 890
    iget v4, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v4, v9, :cond_2

    .line 891
    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    .line 896
    :goto_1
    const/4 v4, 0x0

    invoke-virtual {p0, v6, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setLayerType(ILandroid/graphics/Paint;)V

    .line 898
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;-><init>(F)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    .line 899
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsObjectChange:Z

    goto/16 :goto_0

    .line 893
    :cond_2
    const/16 v4, 0x140

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    goto :goto_1
.end method

.method private isClippedObjectMovingOutsideFrameRect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 5
    .param p1, "topleft"    # Landroid/graphics/PointF;
    .param p2, "topright"    # Landroid/graphics/PointF;
    .param p3, "bottomleft"    # Landroid/graphics/PointF;
    .param p4, "bottomright"    # Landroid/graphics/PointF;

    .prologue
    const v4, 0x3951b717    # 2.0E-4f

    const v3, 0x38d1b717    # 1.0E-4f

    const/4 v1, 0x1

    .line 2270
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 2271
    .local v0, "outsideFrameRect":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 2272
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 2273
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v4

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 2274
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v4

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 2276
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v2, :pswitch_data_0

    .line 2330
    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    .line 2278
    :pswitch_0
    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2279
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2284
    :pswitch_1
    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2285
    iget v2, p4, Landroid/graphics/PointF;->x:F

    iget v3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2290
    :pswitch_2
    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2295
    :pswitch_3
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2296
    iget v2, p4, Landroid/graphics/PointF;->x:F

    iget v3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2301
    :pswitch_4
    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2302
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 2307
    :pswitch_5
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2308
    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2309
    iget v2, p4, Landroid/graphics/PointF;->x:F

    iget v3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_0

    .line 2314
    :pswitch_6
    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2315
    iget v2, p4, Landroid/graphics/PointF;->x:F

    iget v3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_0

    .line 2320
    :pswitch_7
    iget v2, p4, Landroid/graphics/PointF;->x:F

    iget v3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2321
    iget v2, p3, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2322
    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_0

    .line 2276
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method private isContained(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z
    .locals 5
    .param p2, "point"    # Landroid/graphics/PointF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;",
            "Landroid/graphics/PointF;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    const v4, 0x38d1b717    # 1.0E-4f

    .line 4677
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 4682
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 4677
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 4678
    .local v0, "pointF":Landroid/graphics/PointF;
    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_0

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_0

    .line 4679
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isMovable()Z
    .locals 3

    .prologue
    .line 923
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 928
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 923
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 924
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isMovable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 925
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isObjectAvailable()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 774
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 780
    :cond_0
    :goto_0
    return v0

    .line 777
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 780
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isOutOfViewEnabled()Z
    .locals 3

    .prologue
    .line 914
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 919
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 914
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 915
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 916
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isRotatable()Z
    .locals 3

    .prologue
    .line 903
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    if-nez v1, :cond_1

    .line 904
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 910
    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 904
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 905
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 906
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private normalizeDegree(F)F
    .locals 4
    .param p1, "degrees"    # F

    .prologue
    const/high16 v3, -0x3c4c0000    # -360.0f

    const/high16 v2, 0x43b40000    # 360.0f

    .line 986
    move v0, p1

    .line 987
    .local v0, "result":F
    cmpl-float v1, v3, p1

    if-ltz v1, :cond_0

    .line 988
    add-float v0, p1, v2

    .line 990
    :cond_0
    cmpg-float v1, v2, p1

    if-gtz v1, :cond_1

    .line 991
    sub-float v0, p1, v2

    .line 993
    :cond_1
    cmpl-float v1, v3, v0

    if-gez v1, :cond_2

    cmpg-float v1, v2, v0

    if-gtz v1, :cond_3

    .line 994
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v0

    .line 996
    :cond_3
    return v0
.end method

.method private resize(ILandroid/graphics/PointF;)V
    .locals 13
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 1036
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    .line 1037
    .local v4, "tmp":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 1038
    .local v2, "preRectF":Landroid/graphics/RectF;
    iget v6, v4, Landroid/graphics/RectF;->left:F

    iget v7, v4, Landroid/graphics/RectF;->top:F

    iget v8, v4, Landroid/graphics/RectF;->right:F

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1045
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v6, :pswitch_data_0

    .line 1127
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1128
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v6

    invoke-direct {p0, v4, p1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1129
    iget v6, v2, Landroid/graphics/RectF;->left:F

    iget v7, v2, Landroid/graphics/RectF;->top:F

    iget v8, v2, Landroid/graphics/RectF;->right:F

    iget v9, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1131
    :cond_1
    return-void

    .line 1047
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    .line 1048
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    goto :goto_0

    .line 1052
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 1053
    .local v3, "rectf":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    .line 1054
    .local v0, "center":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1055
    .local v1, "height":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1056
    .local v5, "width":F
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    .line 1057
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 1058
    cmpl-float v6, v1, v11

    if-eqz v6, :cond_0

    .line 1061
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v5

    mul-float v7, v10, v1

    div-float/2addr v6, v7

    sub-float v6, v0, v6

    iput v6, v3, Landroid/graphics/RectF;->left:F

    .line 1062
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v5

    mul-float v7, v10, v1

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    iput v6, v3, Landroid/graphics/RectF;->right:F

    goto :goto_0

    .line 1067
    .end local v0    # "center":F
    .end local v1    # "height":F
    .end local v3    # "rectf":Landroid/graphics/RectF;
    .end local v5    # "width":F
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    .line 1068
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    goto :goto_0

    .line 1072
    :pswitch_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 1073
    .restart local v3    # "rectf":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    .line 1074
    .restart local v0    # "center":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1075
    .restart local v1    # "height":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1076
    .restart local v5    # "width":F
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    .line 1077
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 1078
    cmpl-float v6, v5, v11

    if-eqz v6, :cond_0

    .line 1081
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v1

    mul-float v7, v10, v5

    div-float/2addr v6, v7

    sub-float v6, v0, v6

    iput v6, v3, Landroid/graphics/RectF;->top:F

    .line 1082
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v1

    mul-float v7, v10, v5

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    iput v6, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1086
    .end local v0    # "center":F
    .end local v1    # "height":F
    .end local v3    # "rectf":Landroid/graphics/RectF;
    .end local v5    # "width":F
    :pswitch_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 1087
    .restart local v3    # "rectf":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    .line 1088
    .restart local v0    # "center":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1089
    .restart local v1    # "height":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1090
    .restart local v5    # "width":F
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    .line 1091
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 1092
    cmpl-float v6, v5, v11

    if-eqz v6, :cond_0

    .line 1095
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v1

    mul-float v7, v10, v5

    div-float/2addr v6, v7

    sub-float v6, v0, v6

    iput v6, v3, Landroid/graphics/RectF;->top:F

    .line 1096
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v1

    mul-float v7, v10, v5

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    iput v6, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1100
    .end local v0    # "center":F
    .end local v1    # "height":F
    .end local v3    # "rectf":Landroid/graphics/RectF;
    .end local v5    # "width":F
    :pswitch_5
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    .line 1101
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    goto/16 :goto_0

    .line 1105
    :pswitch_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 1106
    .restart local v3    # "rectf":Landroid/graphics/RectF;
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    .line 1107
    .restart local v0    # "center":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1108
    .restart local v1    # "height":F
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1109
    .restart local v5    # "width":F
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    .line 1110
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 1111
    cmpl-float v6, v1, v11

    if-eqz v6, :cond_0

    .line 1114
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v5

    mul-float v7, v10, v1

    div-float/2addr v6, v7

    sub-float v6, v0, v6

    iput v6, v3, Landroid/graphics/RectF;->left:F

    .line 1115
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v5

    mul-float v7, v10, v1

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    iput v6, v3, Landroid/graphics/RectF;->right:F

    goto/16 :goto_0

    .line 1119
    .end local v0    # "center":F
    .end local v1    # "height":F
    .end local v3    # "rectf":Landroid/graphics/RectF;
    .end local v5    # "width":F
    :pswitch_7
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    .line 1120
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    goto/16 :goto_0

    .line 1045
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private resize2Threshold(I)Z
    .locals 21
    .param p1, "objID"    # I

    .prologue
    .line 1657
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/RectF;

    .line 1659
    .local v11, "rectf":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v13

    .line 1661
    .local v13, "srcRect":Landroid/graphics/RectF;
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const v19, 0x38d1b717    # 1.0E-4f

    cmpg-float v18, v18, v19

    if-ltz v18, :cond_0

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const v19, 0x38d1b717    # 1.0E-4f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_1

    .line 1662
    :cond_0
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    .line 1663
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 1664
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1665
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    .line 1666
    const/16 v18, 0x0

    .line 1803
    :goto_0
    return v18

    .line 1669
    :cond_1
    const/4 v4, 0x0

    .line 1670
    .local v4, "isNotKeepRatio":Z
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v18

    const v19, 0x3f800347    # 1.0001f

    cmpg-float v18, v18, v19

    if-lez v18, :cond_2

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v18

    const v19, 0x3f800347    # 1.0001f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_3

    .line 1671
    :cond_2
    const/4 v4, 0x1

    .line 1674
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 1676
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v19

    div-float v10, v18, v19

    .line 1677
    .local v10, "ratio":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v19, v0

    mul-float v6, v18, v19

    .line 1678
    .local v6, "maxWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v19, v0

    mul-float v5, v18, v19

    .line 1680
    .local v5, "maxHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v19, v0

    mul-float v8, v18, v19

    .line 1681
    .local v8, "minWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v19, v0

    mul-float v7, v18, v19

    .line 1683
    .local v7, "minHeight":F
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v6

    if-gez v18, :cond_4

    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v8

    if-lez v18, :cond_4

    .line 1684
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v7

    if-lez v18, :cond_4

    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v5

    if-gez v18, :cond_4

    .line 1685
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 1688
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_5

    .line 1689
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v18

    if-eqz v18, :cond_6

    if-nez v4, :cond_6

    .line 1690
    :cond_5
    mul-float v18, v10, v5

    cmpl-float v18, v6, v18

    if-lez v18, :cond_c

    mul-float v16, v10, v5

    .line 1691
    .local v16, "tmpMaxWidth":F
    :goto_1
    cmpg-float v18, v16, v6

    if-gez v18, :cond_d

    .line 1692
    move/from16 v6, v16

    .line 1697
    :goto_2
    mul-float v18, v10, v7

    cmpg-float v18, v8, v18

    if-gez v18, :cond_e

    mul-float v17, v10, v7

    .line 1698
    .local v17, "tmpMinWidth":F
    :goto_3
    cmpl-float v18, v17, v8

    if-lez v18, :cond_f

    .line 1699
    move/from16 v8, v17

    .line 1705
    .end local v16    # "tmpMaxWidth":F
    .end local v17    # "tmpMinWidth":F
    :cond_6
    :goto_4
    const/4 v12, 0x1

    .line 1706
    .local v12, "sign":I
    const/4 v3, 0x0

    .line 1708
    .local v3, "isEdgeThreshold":Z
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v6

    if-lez v18, :cond_10

    move v15, v6

    .line 1709
    .local v15, "thresholdW":F
    :goto_5
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v5

    if-lez v18, :cond_11

    move v14, v5

    .line 1711
    .local v14, "thresholdH":F
    :goto_6
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v6

    if-gtz v18, :cond_7

    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v8

    if-gez v18, :cond_8

    .line 1712
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_0

    .line 1740
    :goto_7
    :pswitch_0
    const/4 v3, 0x1

    .line 1742
    :cond_8
    const/4 v12, 0x1

    .line 1743
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v5

    if-gtz v18, :cond_9

    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v7

    if-gez v18, :cond_a

    .line 1744
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_1

    .line 1772
    :goto_8
    :pswitch_1
    const/4 v3, 0x1

    .line 1775
    :cond_a
    if-eqz v3, :cond_b

    .line 1776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 1777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_2

    .line 1799
    :cond_b
    :goto_9
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1800
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v18

    .line 1799
    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v13, v11, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getOffsetWhenRotation(ILandroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/PointF;

    move-result-object v9

    .line 1801
    .local v9, "offset":Landroid/graphics/PointF;
    iget v0, v9, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    iget v0, v9, Landroid/graphics/PointF;->y:F

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 1803
    const/16 v18, 0x1

    goto/16 :goto_0

    .end local v3    # "isEdgeThreshold":Z
    .end local v9    # "offset":Landroid/graphics/PointF;
    .end local v12    # "sign":I
    .end local v14    # "thresholdH":F
    .end local v15    # "thresholdW":F
    :cond_c
    move/from16 v16, v6

    .line 1690
    goto/16 :goto_1

    .line 1694
    .restart local v16    # "tmpMaxWidth":F
    :cond_d
    div-float v5, v6, v10

    goto/16 :goto_2

    :cond_e
    move/from16 v17, v8

    .line 1697
    goto/16 :goto_3

    .line 1701
    .restart local v17    # "tmpMinWidth":F
    :cond_f
    div-float v7, v8, v10

    goto/16 :goto_4

    .end local v16    # "tmpMaxWidth":F
    .end local v17    # "tmpMinWidth":F
    .restart local v3    # "isEdgeThreshold":Z
    .restart local v12    # "sign":I
    :cond_10
    move v15, v8

    .line 1708
    goto/16 :goto_5

    .restart local v15    # "thresholdW":F
    :cond_11
    move v14, v7

    .line 1709
    goto/16 :goto_6

    .line 1716
    .restart local v14    # "thresholdH":F
    :pswitch_3
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_12

    .line 1717
    const/4 v12, -0x1

    .line 1719
    :cond_12
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v15

    sub-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    .line 1720
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    goto/16 :goto_7

    .line 1725
    :pswitch_4
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_13

    .line 1726
    const/4 v12, -0x1

    .line 1728
    :cond_13
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v15

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 1729
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    goto/16 :goto_7

    .line 1732
    :pswitch_5
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_14

    .line 1733
    const/4 v12, -0x1

    .line 1735
    :cond_14
    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v15

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    goto/16 :goto_7

    .line 1746
    :pswitch_6
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_15

    .line 1747
    const/4 v12, -0x1

    .line 1749
    :cond_15
    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v14

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_8

    .line 1754
    :pswitch_7
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_16

    .line 1755
    const/4 v12, -0x1

    .line 1757
    :cond_16
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1758
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v14

    sub-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    goto/16 :goto_8

    .line 1763
    :pswitch_8
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v18

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_17

    .line 1764
    const/4 v12, -0x1

    .line 1766
    :cond_17
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v14

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1767
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    goto/16 :goto_8

    .line 1779
    :pswitch_9
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v19, v15, v19

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 1780
    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    sub-float v18, v18, v15

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 1783
    :pswitch_a
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v19, v14, v19

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1784
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    sub-float v18, v18, v14

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 1787
    :pswitch_b
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v19, v14, v19

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 1788
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    sub-float v18, v18, v14

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 1791
    :pswitch_c
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v19, v15, v19

    add-float v18, v18, v19

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 1792
    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    sub-float v18, v18, v15

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 1712
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 1744
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_1
        :pswitch_1
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch

    .line 1777
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_9
        :pswitch_2
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_c
    .end packed-switch
.end method

.method private resizeB2T(ILandroid/graphics/PointF;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 1015
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 1016
    .local v0, "rectF":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1017
    return-void
.end method

.method private resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 19
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "iconWidth"    # I
    .param p3, "iconHeight"    # I

    .prologue
    .line 3399
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 3400
    .local v13, "manager":Landroid/content/pm/PackageManager;
    const/4 v11, 0x0

    .line 3402
    .local v11, "mApk1Resources":Landroid/content/res/Resources;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v11

    .line 3403
    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 3405
    .local v12, "mDrawableResID":I
    if-eqz v12, :cond_1

    .line 3407
    invoke-static {v11, v12}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 3408
    .local v2, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 3409
    const/4 v3, 0x0

    .line 3439
    .end local v2    # "BitmapOrg":Landroid/graphics/Bitmap;
    .end local v12    # "mDrawableResID":I
    :goto_0
    return-object v3

    .line 3412
    .restart local v2    # "BitmapOrg":Landroid/graphics/Bitmap;
    .restart local v12    # "mDrawableResID":I
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 3413
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 3415
    .local v6, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    .line 3416
    .local v9, "dm":Landroid/util/DisplayMetrics;
    const/4 v3, 0x1

    move/from16 v0, p2

    int-to-float v4, v0

    invoke-static {v3, v4, v9}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v15, v3

    .line 3417
    .local v15, "newWidth":I
    const/4 v3, 0x1

    move/from16 v0, p3

    int-to-float v4, v0

    invoke-static {v3, v4, v9}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v14, v3

    .line 3420
    .local v14, "newHeight":I
    int-to-float v3, v15

    int-to-float v4, v5

    div-float v18, v3, v4

    .line 3421
    .local v18, "scaleWidth":F
    int-to-float v3, v14

    int-to-float v4, v6

    div-float v17, v3, v4

    .line 3424
    .local v17, "scaleHeight":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 3426
    .local v7, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 3429
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 3433
    .local v16, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, v16

    invoke-direct {v3, v11, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3435
    .end local v2    # "BitmapOrg":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v9    # "dm":Landroid/util/DisplayMetrics;
    .end local v12    # "mDrawableResID":I
    .end local v14    # "newHeight":I
    .end local v15    # "newWidth":I
    .end local v16    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "scaleHeight":F
    .end local v18    # "scaleWidth":F
    :catch_0
    move-exception v10

    .line 3436
    .local v10, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 3439
    .end local v10    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private resizeL2R(ILandroid/graphics/PointF;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 1000
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 1001
    .local v0, "rectF":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1002
    return-void
.end method

.method private resizeR2L(ILandroid/graphics/PointF;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 1005
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 1006
    .local v0, "rectF":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1007
    return-void
.end method

.method private resizeT2B(ILandroid/graphics/PointF;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "diff_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 1010
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 1011
    .local v0, "rectF":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1012
    return-void
.end method

.method private showHandleView(Z)V
    .locals 3
    .param p1, "isDrawRotation"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1515
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    if-eqz v0, :cond_0

    .line 1516
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setVisibility(I)V

    .line 1517
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setRect(Landroid/graphics/RectF;)V

    .line 1518
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setAngle(F)V

    .line 1519
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setCenter(FF)V

    .line 1520
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setDrawRotation(Z)V

    .line 1521
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->invalidate()V

    .line 1523
    :cond_0
    return-void
.end method


# virtual methods
.method protected absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 788
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 798
    :cond_0
    :goto_0
    return-void

    .line 792
    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 793
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    .line 794
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    .line 793
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 795
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 796
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    .line 797
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    .line 796
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method protected applyChange()V
    .locals 8

    .prologue
    .line 3581
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-nez v6, :cond_1

    .line 3610
    :cond_0
    return-void

    .line 3585
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3586
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3587
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 3589
    .local v4, "objectRect":Landroid/graphics/RectF;
    const/4 v5, 0x0

    .line 3590
    .local v5, "rotateDiff":F
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 3591
    .local v1, "index":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 3592
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v7

    sub-float v5, v6, v7

    .line 3595
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 3596
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v4, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3597
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 3598
    .local v3, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 3600
    const/4 v2, 0x0

    .line 3601
    .local v2, "objAng":F
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 3603
    if-eq v0, v1, :cond_3

    .line 3604
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v6

    add-float/2addr v6, v5

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v2

    .line 3606
    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 3607
    float-to-int v6, v2

    int-to-float v6, v6

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotation(F)V

    .line 3595
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 3965
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 3966
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    .line 3967
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 3968
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->close()Z

    .line 3971
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->closeHandleView()V

    .line 3973
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_1

    .line 3974
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onClosed(Ljava/util/ArrayList;)V

    .line 3977
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    .line 3978
    return-void
.end method

.method protected closeHandleView()V
    .locals 2

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1563
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 1564
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 1566
    :cond_0
    return-void
.end method

.method protected draw8Points(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 12
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/RectF;
    .param p4, "isKeepRatio"    # Z

    .prologue
    .line 3267
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 3268
    .local v7, "w":I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 3270
    .local v3, "h":I
    int-to-float v8, v7

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 3271
    .local v4, "halfX":I
    int-to-float v8, v3

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 3272
    .local v5, "halfY":I
    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 3273
    .local v0, "CenterX":I
    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 3275
    .local v1, "CenterY":I
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 3276
    .local v6, "tmpRect":Landroid/graphics/Rect;
    invoke-virtual {p3, v6}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 3278
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 3280
    .local v2, "bounds":Landroid/graphics/Rect;
    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v4

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v5

    iget v10, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v4

    iget v11, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3281
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3282
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3285
    iget v8, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v4

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v5

    iget v10, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v4

    iget v11, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3286
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3287
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3290
    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v4

    iget v9, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    iget v10, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v4

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3291
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3292
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3295
    iget v8, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v4

    iget v9, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    iget v10, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v4

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3296
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3297
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3299
    instance-of v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-nez v8, :cond_1

    .line 3300
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;)F

    move-result v9

    const/high16 v10, 0x40400000    # 3.0f

    mul-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-lez v8, :cond_0

    .line 3302
    sub-int v8, v0, v4

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v5

    add-int v10, v0, v4

    iget v11, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3303
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3304
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3307
    sub-int v8, v0, v4

    iget v9, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    add-int v10, v0, v4

    iget v11, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v11, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3308
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3309
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3312
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;)F

    move-result v9

    const/high16 v10, 0x40400000    # 3.0f

    mul-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-lez v8, :cond_1

    .line 3314
    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v4

    sub-int v9, v1, v5

    iget v10, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v4

    add-int v11, v1, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3315
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3316
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3319
    iget v8, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v4

    sub-int v9, v1, v5

    iget v10, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v4

    add-int v11, v1, v5

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 3320
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3321
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3324
    :cond_1
    return-void
.end method

.method protected drawHighlightObject(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 4339
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4348
    .end local p2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :goto_0
    return-void

    .line 4343
    .restart local p2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_0
    instance-of v0, p2, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v0, :cond_1

    .line 4344
    check-cast p2, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .end local p2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    goto :goto_0

    .line 4346
    .restart local p2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightRect(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method public fit()V
    .locals 9

    .prologue
    .line 3810
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v4

    if-nez v4, :cond_1

    .line 3836
    :cond_0
    :goto_0
    return-void

    .line 3814
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3815
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v4, :cond_2

    .line 3816
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3819
    :cond_2
    const/4 v1, 0x0

    .line 3820
    .local v1, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    const/4 v2, 0x0

    .line 3821
    .local v2, "objectRect":Landroid/graphics/RectF;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_4

    .line 3832
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    .line 3833
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateContextMenu()V

    .line 3834
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto :goto_0

    .line 3822
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 3823
    .restart local v1    # "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 3825
    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->adjustObjectRect(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Landroid/graphics/RectF;)V

    .line 3827
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 3828
    .local v3, "rect":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v3, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3829
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v5, v3, Landroid/graphics/RectF;->left:F

    iget v6, v3, Landroid/graphics/RectF;->top:F

    iget v7, v3, Landroid/graphics/RectF;->right:F

    iget v8, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3821
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected fitRotateAngle2BorderAngle()V
    .locals 2

    .prologue
    .line 3689
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3690
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 3694
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 3691
    .restart local v0    # "i":I
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 3690
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getBoundBox(I)Landroid/graphics/RectF;
    .locals 20
    .param p1, "index"    # I

    .prologue
    .line 1207
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1208
    const/16 v17, 0x0

    .line 1255
    :cond_0
    :goto_0
    return-object v17

    .line 1211
    :cond_1
    const/4 v3, -0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_2

    .line 1212
    new-instance v17, Landroid/graphics/RectF;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1215
    .local v17, "sumRect":Landroid/graphics/RectF;
    const/4 v10, 0x1

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v10, v3, :cond_0

    .line 1216
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v18

    .line 1217
    .local v18, "tempRect":Landroid/graphics/RectF;
    invoke-virtual/range {v17 .. v18}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 1215
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 1222
    .end local v10    # "i":I
    .end local v17    # "sumRect":Landroid/graphics/RectF;
    .end local v18    # "tempRect":Landroid/graphics/RectF;
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v6

    .line 1223
    .local v6, "centerX":F
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v7

    .line 1224
    .local v7, "centerY":F
    const/4 v3, 0x4

    new-array v0, v3, [Landroid/graphics/PointF;

    move-object/from16 v16, v0

    .line 1225
    .local v16, "rotatePoint":[Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/graphics/RectF;

    .line 1226
    .local v15, "rectF":Landroid/graphics/RectF;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v2

    .line 1227
    .local v2, "degree":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move/from16 v0, p1

    if-ne v0, v3, :cond_3

    .line 1228
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 1230
    :cond_3
    const/16 v19, 0x0

    iget v3, v15, Landroid/graphics/RectF;->left:F

    float-to-int v4, v3

    iget v3, v15, Landroid/graphics/RectF;->top:F

    float-to-int v5, v3

    float-to-double v8, v2

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v3

    aput-object v3, v16, v19

    .line 1231
    const/16 v19, 0x1

    iget v3, v15, Landroid/graphics/RectF;->right:F

    float-to-int v4, v3

    iget v3, v15, Landroid/graphics/RectF;->top:F

    float-to-int v5, v3

    float-to-double v8, v2

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v3

    aput-object v3, v16, v19

    .line 1232
    const/16 v19, 0x2

    iget v3, v15, Landroid/graphics/RectF;->left:F

    float-to-int v4, v3

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v3

    float-to-double v8, v2

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v3

    aput-object v3, v16, v19

    .line 1233
    const/16 v19, 0x3

    iget v3, v15, Landroid/graphics/RectF;->right:F

    float-to-int v4, v3

    iget v3, v15, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v3

    float-to-double v8, v2

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v3

    aput-object v3, v16, v19

    .line 1236
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v12, v3, Landroid/graphics/PointF;->x:F

    .line 1237
    .local v12, "pointX_min":F
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v14, v3, Landroid/graphics/PointF;->y:F

    .line 1238
    .local v14, "pointY_min":F
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v11, v3, Landroid/graphics/PointF;->x:F

    .line 1239
    .local v11, "pointX_max":F
    const/4 v3, 0x0

    aget-object v3, v16, v3

    iget v13, v3, Landroid/graphics/PointF;->y:F

    .line 1240
    .local v13, "pointY_max":F
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_2
    move-object/from16 v0, v16

    array-length v3, v0

    if-lt v10, v3, :cond_4

    .line 1255
    new-instance v17, Landroid/graphics/RectF;

    move-object/from16 v0, v17

    invoke-direct {v0, v12, v14, v11, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_0

    .line 1241
    :cond_4
    aget-object v3, v16, v10

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpl-float v3, v12, v3

    if-ltz v3, :cond_5

    .line 1242
    aget-object v3, v16, v10

    iget v12, v3, Landroid/graphics/PointF;->x:F

    .line 1244
    :cond_5
    aget-object v3, v16, v10

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpg-float v3, v11, v3

    if-gtz v3, :cond_6

    .line 1245
    aget-object v3, v16, v10

    iget v11, v3, Landroid/graphics/PointF;->x:F

    .line 1247
    :cond_6
    aget-object v3, v16, v10

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpl-float v3, v14, v3

    if-ltz v3, :cond_7

    .line 1248
    aget-object v3, v16, v10

    iget v14, v3, Landroid/graphics/PointF;->y:F

    .line 1250
    :cond_7
    aget-object v3, v16, v10

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpg-float v3, v13, v3

    if-gtz v3, :cond_8

    .line 1251
    aget-object v3, v16, v10

    iget v13, v3, Landroid/graphics/PointF;->y:F

    .line 1240
    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method

.method public getContextMenu()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4106
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getControlPivotX(I)F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2191
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2192
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    .line 2195
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    goto :goto_0
.end method

.method protected getControlPivotY(I)F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2203
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2204
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    .line 2206
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    goto :goto_0
.end method

.method protected getDrawableImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 18
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 2020
    const/4 v8, 0x0

    .line 2021
    .local v8, "drawable":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    check-cast v8, Landroid/graphics/drawable/Drawable;

    .line 2022
    .restart local v8    # "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v8, :cond_0

    move-object v1, v8

    .line 2084
    :goto_0
    return-object v1

    .line 2026
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 2027
    .local v17, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v15

    .line 2029
    .local v15, "mApk1Resources":Landroid/content/res/Resources;
    const-string v2, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v15, v0, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 2031
    .local v16, "mDrawableResID":I
    if-eqz v16, :cond_2

    .line 2032
    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 2033
    if-eqz v8, :cond_1

    .line 2034
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .end local v15    # "mApk1Resources":Landroid/content/res/Resources;
    .end local v16    # "mDrawableResID":I
    .end local v17    # "manager":Landroid/content/pm/PackageManager;
    :cond_1
    :goto_1
    move-object v1, v8

    .line 2084
    goto :goto_0

    .line 2037
    .restart local v15    # "mApk1Resources":Landroid/content/res/Resources;
    .restart local v16    # "mDrawableResID":I
    .restart local v17    # "manager":Landroid/content/pm/PackageManager;
    :cond_2
    const/4 v12, 0x0

    .line 2038
    .local v12, "localInputStream":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 2039
    .local v1, "localObject":Ljava/lang/Object;
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 2041
    .local v5, "localRect":Landroid/graphics/Rect;
    const-class v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v14

    .line 2043
    .local v14, "localURL":Ljava/net/URL;
    if-eqz v14, :cond_3

    .line 2044
    :try_start_1
    invoke-virtual {v14}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v12

    .line 2050
    :cond_3
    :try_start_2
    new-instance v13, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v13}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2051
    .local v13, "localOptions":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x0

    .line 2052
    .local v3, "localBitmap":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_4

    .line 2053
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    iput v2, v13, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 2054
    invoke-static {v12, v5, v13}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    .line 2056
    :try_start_3
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2064
    if-eqz v3, :cond_1

    .line 2065
    :try_start_4
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v4

    .line 2066
    .local v4, "arrayOfByte":[B
    invoke-static {v4}, Landroid/graphics/NinePatch;->isNinePatchChunk([B)Z

    move-result v7

    .line 2067
    .local v7, "bool":Z
    if-eqz v7, :cond_5

    .line 2068
    new-instance v1, Landroid/graphics/drawable/NinePatchDrawable;

    .end local v1    # "localObject":Ljava/lang/Object;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2069
    const/4 v6, 0x0

    .line 2068
    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    .line 2074
    :goto_2
    if-eqz v1, :cond_1

    .line 2075
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    move-object v0, v1

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v2, v0

    move-object/from16 v0, p1

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2076
    check-cast v1, Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 2046
    .end local v3    # "localBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "arrayOfByte":[B
    .end local v7    # "bool":Z
    .end local v13    # "localOptions":Landroid/graphics/BitmapFactory$Options;
    .restart local v1    # "localObject":Ljava/lang/Object;
    :catch_0
    move-exception v10

    .line 2047
    .local v10, "localIOException2":Ljava/io/IOException;
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2057
    .end local v10    # "localIOException2":Ljava/io/IOException;
    .restart local v3    # "localBitmap":Landroid/graphics/Bitmap;
    .restart local v13    # "localOptions":Landroid/graphics/BitmapFactory$Options;
    :catch_1
    move-exception v11

    .line 2058
    .local v11, "localIOException3":Ljava/io/IOException;
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2061
    .end local v11    # "localIOException3":Ljava/io/IOException;
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2071
    .restart local v4    # "arrayOfByte":[B
    .restart local v7    # "bool":Z
    :cond_5
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "localObject":Ljava/lang/Object;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    .local v1, "localObject":Landroid/graphics/drawable/BitmapDrawable;
    goto :goto_2

    .line 2081
    .end local v1    # "localObject":Landroid/graphics/drawable/BitmapDrawable;
    .end local v3    # "localBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "arrayOfByte":[B
    .end local v5    # "localRect":Landroid/graphics/Rect;
    .end local v7    # "bool":Z
    .end local v12    # "localInputStream":Ljava/io/InputStream;
    .end local v13    # "localOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v14    # "localURL":Ljava/net/URL;
    .end local v15    # "mApk1Resources":Landroid/content/res/Resources;
    .end local v16    # "mDrawableResID":I
    .end local v17    # "manager":Landroid/content/pm/PackageManager;
    :catch_2
    move-exception v9

    .line 2082
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v9}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public getMinResizeRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 4205
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    .line 4206
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    .line 4208
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4706
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getPanKey(II)Ljava/lang/String;
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 3681
    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public getPixel(II)I
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 4727
    const/4 v0, 0x0

    return v0
.end method

.method public getPopupMenu()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 4790
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 4791
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->getPopupMenu()Landroid/widget/PopupWindow;

    move-result-object v0

    .line 4794
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4000
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 4001
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 4008
    :goto_0
    return-object v2

    .line 4003
    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v4, v4, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 4004
    .local v1, "rect":Landroid/graphics/RectF;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    move-object v2, v1

    .line 4008
    goto :goto_0

    .line 4005
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 4004
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected getRectList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getRotatePoint(IIFFD)Landroid/graphics/PointF;
    .locals 21
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "pivotX"    # F
    .param p4, "pivotY"    # F
    .param p5, "degrees"    # D

    .prologue
    .line 1264
    invoke-static/range {p5 .. p6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    .line 1265
    .local v4, "dSetDegree":D
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 1266
    .local v2, "cosq":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 1267
    .local v10, "sinq":D
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, p3

    move/from16 v0, v16

    float-to-double v12, v0

    .line 1268
    .local v12, "sx":D
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, p4

    move/from16 v0, v16

    float-to-double v14, v0

    .line 1269
    .local v14, "sy":D
    mul-double v16, v12, v2

    mul-double v18, v14, v10

    sub-double v16, v16, v18

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v6, v16, v18

    .line 1270
    .local v6, "rx":D
    mul-double v16, v12, v10

    mul-double v18, v14, v2

    add-double v16, v16, v18

    move/from16 v0, p4

    float-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v8, v16, v18

    .line 1272
    .local v8, "ry":D
    new-instance v16, Landroid/graphics/PointF;

    double-to-float v0, v6

    move/from16 v17, v0

    double-to-float v0, v8

    move/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v16
.end method

.method getStartEndBitmapCalcPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;F)V
    .locals 16
    .param p1, "curPoint"    # Landroid/graphics/PointF;
    .param p2, "prevPoint"    # Landroid/graphics/PointF;
    .param p3, "calcPoint"    # Landroid/graphics/PointF;
    .param p4, "distance"    # F

    .prologue
    .line 4378
    move-object/from16 v0, p1

    iget v9, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/PointF;->x:F

    sub-float v5, v9, v10

    .line 4379
    .local v5, "originalDx":F
    move-object/from16 v0, p1

    iget v9, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/PointF;->y:F

    sub-float v6, v9, v10

    .line 4381
    .local v6, "originalDy":F
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 4382
    .local v7, "quadrant":Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;
    const/4 v9, 0x0

    cmpl-float v9, v5, v9

    if-ltz v9, :cond_0

    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-ltz v9, :cond_0

    .line 4383
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 4392
    :goto_0
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 4393
    .local v3, "dx":F
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 4395
    .local v4, "dy":F
    float-to-double v10, v4

    float-to-double v12, v3

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->atan(D)D

    move-result-wide v10

    const-wide v12, 0x4066800000000000L    # 180.0

    mul-double/2addr v10, v12

    const-wide v12, 0x400921fb60000000L    # 3.1415927410125732

    div-double/2addr v10, v12

    double-to-float v2, v10

    .line 4396
    .local v2, "degree":F
    const/4 v8, 0x0

    .line 4398
    .local v8, "radian":F
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT()[I

    move-result-object v9

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 4431
    :goto_1
    return-void

    .line 4384
    .end local v2    # "degree":F
    .end local v3    # "dx":F
    .end local v4    # "dy":F
    .end local v8    # "radian":F
    :cond_0
    const/4 v9, 0x0

    cmpg-float v9, v5, v9

    if-gez v9, :cond_1

    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-ltz v9, :cond_1

    .line 4385
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 4386
    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    cmpl-float v9, v5, v9

    if-ltz v9, :cond_2

    const/4 v9, 0x0

    cmpg-float v9, v6, v9

    if-gez v9, :cond_2

    .line 4387
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    .line 4388
    goto :goto_0

    .line 4389
    :cond_2
    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    goto :goto_0

    .line 4400
    .restart local v2    # "degree":F
    .restart local v3    # "dx":F
    .restart local v4    # "dy":F
    .restart local v8    # "radian":F
    :pswitch_0
    const/high16 v9, 0x42b40000    # 90.0f

    sub-float/2addr v9, v2

    const v10, 0x3c8efa35

    mul-float v8, v9, v10

    .line 4402
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->x:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->x:F

    .line 4403
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 4407
    :pswitch_1
    const v9, 0x3c8efa35

    mul-float v8, v2, v9

    .line 4409
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->x:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->x:F

    .line 4410
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 4414
    :pswitch_2
    const v9, 0x3c8efa35

    mul-float v8, v2, v9

    .line 4416
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->x:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->x:F

    .line 4417
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    .line 4421
    :pswitch_3
    const/high16 v9, 0x42b40000    # 90.0f

    sub-float/2addr v9, v2

    const v10, 0x3c8efa35

    mul-float v8, v9, v10

    .line 4423
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->x:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->x:F

    .line 4424
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    move/from16 v0, p4

    float-to-double v12, v0

    float-to-double v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p3

    iput v9, v0, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    .line 4398
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 4044
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    return v0
.end method

.method protected handleMoveControl(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 9
    .param p1, "pos"    # Landroid/graphics/Point;
    .param p2, "pos_rotated"    # Landroid/graphics/Point;

    .prologue
    const/high16 v7, 0x41a00000    # 20.0f

    .line 1458
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1502
    :cond_0
    :goto_0
    return-void

    .line 1462
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isMovable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1466
    iget v5, p1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    sub-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v0, v5

    .line 1467
    .local v0, "changeX":F
    iget v5, p1, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    sub-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v1, v5

    .line 1469
    .local v1, "changeY":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v7

    if-gez v5, :cond_2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v7

    if-gez v5, :cond_2

    .line 1470
    iget-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    if-eqz v5, :cond_0

    .line 1474
    :cond_2
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    .line 1477
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 1479
    .local v3, "preRectF":Landroid/graphics/RectF;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_3

    .line 1496
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->showHandleViewNoTextRotation()V

    .line 1497
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, p1, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    .line 1498
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, p1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    .line 1500
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, p2, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    .line 1501
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v6, p2, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    goto :goto_0

    .line 1480
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    .line 1481
    .local v4, "rectF":Landroid/graphics/RectF;
    iget v5, v4, Landroid/graphics/RectF;->left:F

    iget v6, v4, Landroid/graphics/RectF;->top:F

    iget v7, v4, Landroid/graphics/RectF;->right:F

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1482
    iget v5, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, v0

    iget v6, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v1

    iget v7, v4, Landroid/graphics/RectF;->right:F

    add-float/2addr v7, v0

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v8, v1

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1484
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1485
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v5

    invoke-direct {p0, v4, v2, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1486
    iget v5, v3, Landroid/graphics/RectF;->left:F

    iget v6, v3, Landroid/graphics/RectF;->top:F

    iget v7, v3, Landroid/graphics/RectF;->right:F

    iget v8, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1489
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1491
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1479
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1
.end method

.method protected handleResizeControl(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 37
    .param p1, "pos"    # Landroid/graphics/PointF;
    .param p2, "pos_rotated"    # Landroid/graphics/PointF;

    .prologue
    .line 1321
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1322
    const/4 v5, 0x0

    .line 1450
    :goto_0
    return v5

    .line 1325
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 1327
    .local v6, "index":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v5

    const/4 v7, 0x2

    if-ne v5, v7, :cond_1

    .line 1328
    const/4 v5, 0x0

    goto :goto_0

    .line 1331
    :cond_1
    new-instance v19, Landroid/graphics/PointF;

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v5, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1333
    .local v19, "diff_rotated":Landroid/graphics/PointF;
    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    sub-float/2addr v5, v7

    move-object/from16 v0, v19

    iput v5, v0, Landroid/graphics/PointF;->x:F

    .line 1334
    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    sub-float/2addr v5, v7

    move-object/from16 v0, v19

    iput v5, v0, Landroid/graphics/PointF;->y:F

    .line 1336
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findResizeRate(Landroid/graphics/PointF;Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v31

    .line 1338
    .local v31, "resizeRate":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v23

    .line 1339
    .local v23, "objRect":Landroid/graphics/RectF;
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/RectF;->width()F

    move-result v5

    const v7, 0x3f800347    # 1.0001f

    cmpl-float v5, v5, v7

    if-lez v5, :cond_2

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/RectF;->height()F

    move-result v5

    const v7, 0x3f800347    # 1.0001f

    cmpl-float v5, v5, v7

    if-lez v5, :cond_2

    .line 1340
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v5, :pswitch_data_0

    .line 1376
    :cond_2
    :goto_1
    :pswitch_0
    new-instance v29, Landroid/graphics/RectF;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/RectF;-><init>()V

    .line 1377
    .local v29, "prevRect":Landroid/graphics/RectF;
    new-instance v28, Landroid/graphics/RectF;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/RectF;-><init>()V

    .line 1378
    .local v28, "postRect":Landroid/graphics/RectF;
    new-instance v20, Landroid/graphics/PointF;

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v5, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1381
    .local v20, "diff_rotated_rated":Landroid/graphics/PointF;
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    move/from16 v0, v21

    if-lt v0, v5, :cond_7

    .line 1449
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->showHandleViewNoTextRotation()V

    .line 1450
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 1342
    .end local v20    # "diff_rotated_rated":Landroid/graphics/PointF;
    .end local v21    # "i":I
    .end local v28    # "postRect":Landroid/graphics/RectF;
    .end local v29    # "prevRect":Landroid/graphics/RectF;
    :pswitch_1
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v31

    iget v7, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_3

    .line 1343
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v31

    iput v5, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 1345
    :cond_3
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v31

    iput v5, v0, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 1350
    :pswitch_2
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->x:F

    neg-float v5, v5

    move-object/from16 v0, v31

    iget v7, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_4

    .line 1351
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->x:F

    neg-float v5, v5

    move-object/from16 v0, v31

    iput v5, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 1353
    :cond_4
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->y:F

    neg-float v5, v5

    move-object/from16 v0, v31

    iput v5, v0, Landroid/graphics/PointF;->x:F

    goto :goto_1

    .line 1358
    :pswitch_3
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->x:F

    neg-float v5, v5

    move-object/from16 v0, v31

    iget v7, v0, Landroid/graphics/PointF;->y:F

    neg-float v7, v7

    cmpl-float v5, v5, v7

    if-lez v5, :cond_5

    .line 1359
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v31

    iput v5, v0, Landroid/graphics/PointF;->y:F

    goto :goto_1

    .line 1361
    :cond_5
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v31

    iput v5, v0, Landroid/graphics/PointF;->x:F

    goto/16 :goto_1

    .line 1366
    :pswitch_4
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v31

    iget v7, v0, Landroid/graphics/PointF;->y:F

    neg-float v7, v7

    cmpl-float v5, v5, v7

    if-lez v5, :cond_6

    .line 1367
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->x:F

    neg-float v5, v5

    move-object/from16 v0, v31

    iput v5, v0, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    .line 1369
    :cond_6
    move-object/from16 v0, v31

    iget v5, v0, Landroid/graphics/PointF;->y:F

    neg-float v5, v5

    move-object/from16 v0, v31

    iput v5, v0, Landroid/graphics/PointF;->x:F

    goto/16 :goto_1

    .line 1382
    .restart local v20    # "diff_rotated_rated":Landroid/graphics/PointF;
    .restart local v21    # "i":I
    .restart local v28    # "postRect":Landroid/graphics/RectF;
    .restart local v29    # "prevRect":Landroid/graphics/RectF;
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1384
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v30

    .line 1385
    .local v30, "rect":Landroid/graphics/RectF;
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/RectF;->width()F

    move-result v5

    move-object/from16 v0, v31

    iget v7, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v5, v7

    move-object/from16 v0, v20

    iput v5, v0, Landroid/graphics/PointF;->x:F

    .line 1386
    invoke-virtual/range {v30 .. v30}, Landroid/graphics/RectF;->height()F

    move-result v5

    move-object/from16 v0, v31

    iget v7, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v5, v7

    move-object/from16 v0, v20

    iput v5, v0, Landroid/graphics/PointF;->y:F

    .line 1388
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resize(ILandroid/graphics/PointF;)V

    .line 1391
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v5

    if-nez v5, :cond_8

    .line 1393
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v25

    .line 1394
    .local v25, "pivotX":F
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v26

    .line 1397
    .local v26, "pivotY":F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v15

    .line 1398
    .local v15, "angle":F
    new-instance v35, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, v35

    invoke-direct {v0, v5, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1399
    .local v35, "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v35

    invoke-virtual {v0, v1, v2, v15, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v35

    .line 1401
    new-instance v36, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, v36

    invoke-direct {v0, v5, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1402
    .local v36, "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v36

    invoke-virtual {v0, v1, v2, v15, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v36

    .line 1404
    new-instance v17, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v17

    invoke-direct {v0, v5, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1405
    .local v17, "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v17

    invoke-virtual {v0, v1, v2, v15, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v17

    .line 1407
    new-instance v18, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v18

    invoke-direct {v0, v5, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1408
    .local v18, "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v15, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 1410
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v17

    move-object/from16 v4, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v27

    .line 1411
    .local v27, "points":[F
    const/4 v5, 0x0

    aget v22, v27, v5

    .line 1412
    .local v22, "left":F
    const/4 v5, 0x1

    aget v34, v27, v5

    .line 1413
    .local v34, "top":F
    const/4 v5, 0x2

    aget v32, v27, v5

    .line 1414
    .local v32, "right":F
    const/4 v5, 0x3

    aget v16, v27, v5

    .line 1416
    .local v16, "bottom":F
    new-instance v33, Landroid/graphics/RectF;

    move-object/from16 v0, v33

    move/from16 v1, v22

    move/from16 v2, v34

    move/from16 v3, v32

    move/from16 v4, v16

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1418
    .local v33, "tmpBoundBoxRect":Landroid/graphics/RectF;
    new-instance v24, Landroid/graphics/RectF;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/RectF;-><init>()V

    .line 1419
    .local v24, "outsideFrameRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    const v7, 0x38d1b717    # 1.0E-4f

    sub-float/2addr v5, v7

    move-object/from16 v0, v24

    iput v5, v0, Landroid/graphics/RectF;->left:F

    .line 1420
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    const v7, 0x38d1b717    # 1.0E-4f

    sub-float/2addr v5, v7

    move-object/from16 v0, v24

    iput v5, v0, Landroid/graphics/RectF;->top:F

    .line 1421
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    const v7, 0x3951b717    # 2.0E-4f

    add-float/2addr v5, v7

    move-object/from16 v0, v24

    iput v5, v0, Landroid/graphics/RectF;->right:F

    .line 1422
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    const v7, 0x3951b717    # 2.0E-4f

    add-float/2addr v5, v7

    move-object/from16 v0, v24

    iput v5, v0, Landroid/graphics/RectF;->bottom:F

    .line 1423
    move-object/from16 v0, v24

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1424
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v12

    .line 1425
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v5

    const/4 v13, 0x1

    if-ne v5, v13, :cond_9

    const/4 v13, 0x1

    .line 1426
    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v14

    invoke-virtual {v14}, Landroid/graphics/RectF;->height()F

    move-result v14

    div-float v14, v5, v14

    move-object/from16 v5, p0

    .line 1424
    invoke-virtual/range {v5 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v5

    .line 1426
    if-eqz v5, :cond_8

    .line 1427
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1432
    .end local v15    # "angle":F
    .end local v16    # "bottom":F
    .end local v17    # "bottomleft":Landroid/graphics/PointF;
    .end local v18    # "bottomright":Landroid/graphics/PointF;
    .end local v22    # "left":F
    .end local v24    # "outsideFrameRect":Landroid/graphics/RectF;
    .end local v25    # "pivotX":F
    .end local v26    # "pivotY":F
    .end local v27    # "points":[F
    .end local v32    # "right":F
    .end local v33    # "tmpBoundBoxRect":Landroid/graphics/RectF;
    .end local v34    # "top":F
    .end local v35    # "topleft":Landroid/graphics/PointF;
    .end local v36    # "topright":Landroid/graphics/PointF;
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1434
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v5

    if-nez v5, :cond_a

    .line 1435
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 1425
    .restart local v15    # "angle":F
    .restart local v16    # "bottom":F
    .restart local v17    # "bottomleft":Landroid/graphics/PointF;
    .restart local v18    # "bottomright":Landroid/graphics/PointF;
    .restart local v22    # "left":F
    .restart local v24    # "outsideFrameRect":Landroid/graphics/RectF;
    .restart local v25    # "pivotX":F
    .restart local v26    # "pivotY":F
    .restart local v27    # "points":[F
    .restart local v32    # "right":F
    .restart local v33    # "tmpBoundBoxRect":Landroid/graphics/RectF;
    .restart local v34    # "top":F
    .restart local v35    # "topleft":Landroid/graphics/PointF;
    .restart local v36    # "topright":Landroid/graphics/PointF;
    :cond_9
    const/4 v13, 0x0

    goto :goto_3

    .line 1438
    .end local v15    # "angle":F
    .end local v16    # "bottom":F
    .end local v17    # "bottomleft":Landroid/graphics/PointF;
    .end local v18    # "bottomright":Landroid/graphics/PointF;
    .end local v22    # "left":F
    .end local v24    # "outsideFrameRect":Landroid/graphics/RectF;
    .end local v25    # "pivotX":F
    .end local v26    # "pivotY":F
    .end local v27    # "points":[F
    .end local v32    # "right":F
    .end local v33    # "tmpBoundBoxRect":Landroid/graphics/RectF;
    .end local v34    # "top":F
    .end local v35    # "topleft":Landroid/graphics/PointF;
    .end local v36    # "topright":Landroid/graphics/PointF;
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1440
    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, v29

    iget v7, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->signum(F)F

    move-result v5

    move-object/from16 v0, v28

    iget v7, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, v28

    iget v8, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->signum(F)F

    move-result v7

    cmpl-float v5, v5, v7

    if-eqz v5, :cond_b

    .line 1441
    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1443
    :cond_b
    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v29

    iget v7, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->signum(F)F

    move-result v5

    move-object/from16 v0, v28

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v28

    iget v8, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->signum(F)F

    move-result v7

    cmpl-float v5, v5, v7

    if-eqz v5, :cond_c

    .line 1444
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1381
    :cond_c
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_2

    .line 1340
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected handleRotationControl(Landroid/graphics/Point;)V
    .locals 33
    .param p1, "pos"    # Landroid/graphics/Point;

    .prologue
    .line 1573
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1653
    :cond_0
    :goto_0
    return-void

    .line 1577
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v22, v0

    .line 1578
    .local v22, "preDegree":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move/from16 v17, v0

    .line 1579
    .local v17, "index":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v2

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    sub-float v15, v2, v4

    .line 1580
    .local v15, "dx":F
    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v4

    sub-float v16, v2, v4

    .line 1582
    .local v16, "dy":F
    move/from16 v0, v16

    float-to-double v4, v0

    float-to-double v6, v15

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v24

    .line 1583
    .local v24, "radian":D
    const-wide v4, 0x4066800000000000L    # 180.0

    mul-double v4, v4, v24

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v4, v6

    double-to-float v14, v4

    .line 1585
    .local v14, "degree":F
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v14

    .line 1587
    const/16 v18, 0x1

    .line 1588
    .local v18, "isClippable":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_9

    .line 1594
    :goto_2
    if-nez v18, :cond_2

    .line 1595
    const/4 v3, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_b

    .line 1602
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v28

    .line 1603
    .local v28, "rotateRect":Landroid/graphics/RectF;
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/RectF;->centerX()F

    move-result v29

    .line 1604
    .local v29, "startX":F
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/RectF;->centerY()F

    move-result v32

    .line 1606
    .local v32, "startY":F
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v12

    .line 1607
    .local v12, "centerPointX":F
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v13

    .line 1609
    .local v13, "centerPointY":F
    sub-float v2, v12, v29

    sub-float v4, v12, v29

    mul-float/2addr v2, v4

    sub-float v4, v13, v32

    .line 1610
    sub-float v5, v13, v32

    mul-float/2addr v4, v5

    .line 1609
    add-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v30

    .line 1611
    .local v30, "rotationRad":D
    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float v2, v12, v2

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    sub-float v4, v12, v4

    mul-float/2addr v2, v4

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    sub-float v4, v13, v4

    .line 1612
    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    sub-float v5, v13, v5

    mul-float/2addr v4, v5

    .line 1611
    add-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v20

    .line 1614
    .local v20, "pos2Center":D
    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v2, v14

    float-to-int v2, v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 1616
    const-wide v4, 0x4051800000000000L    # 70.0

    add-double v4, v4, v30

    cmpg-double v2, v20, v4

    if-gez v2, :cond_5

    .line 1617
    const/high16 v2, 0x43320000    # 178.0f

    cmpl-float v2, v14, v2

    if-lez v2, :cond_3

    const/high16 v2, 0x43380000    # 184.0f

    cmpg-float v2, v14, v2

    if-gez v2, :cond_3

    .line 1618
    const/high16 v14, 0x43340000    # 180.0f

    .line 1620
    :cond_3
    const/high16 v2, -0x3cc80000    # -184.0f

    cmpl-float v2, v14, v2

    if-lez v2, :cond_4

    const/high16 v2, -0x3cce0000    # -178.0f

    cmpg-float v2, v14, v2

    if-gez v2, :cond_4

    .line 1621
    const/high16 v14, -0x3ccc0000    # -180.0f

    .line 1624
    :cond_4
    float-to-int v2, v14

    div-int/lit8 v2, v2, 0x5

    mul-int/lit8 v2, v2, 0x5

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 1627
    :cond_5
    if-eqz v18, :cond_6

    .line 1629
    new-instance v23, Landroid/graphics/RectF;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/RectF;-><init>()V

    .line 1631
    .local v23, "preRectF":Landroid/graphics/RectF;
    const/4 v3, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_c

    .line 1642
    .end local v23    # "preRectF":Landroid/graphics/RectF;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->showHandleViewWithTextRotation()V

    .line 1643
    const/4 v3, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 1644
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1645
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v19, v0

    .line 1646
    .local v19, "objAng":F
    move/from16 v0, v17

    if-eq v3, v0, :cond_7

    const/4 v2, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_7

    .line 1647
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    sub-float v27, v2, v4

    .line 1648
    .local v27, "rotateDiff":F
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v2

    add-float v2, v2, v27

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v19

    .line 1650
    .end local v27    # "rotateDiff":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 1643
    .end local v19    # "objAng":F
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1589
    .end local v12    # "centerPointX":F
    .end local v13    # "centerPointY":F
    .end local v20    # "pos2Center":D
    .end local v28    # "rotateRect":Landroid/graphics/RectF;
    .end local v29    # "startX":F
    .end local v30    # "rotationRad":D
    .end local v32    # "startY":F
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1590
    const/16 v18, 0x0

    .line 1591
    goto/16 :goto_2

    .line 1588
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 1596
    :cond_b
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    float-to-int v2, v14

    int-to-float v9, v2

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1595
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 1632
    .restart local v12    # "centerPointX":F
    .restart local v13    # "centerPointY":F
    .restart local v20    # "pos2Center":D
    .restart local v23    # "preRectF":Landroid/graphics/RectF;
    .restart local v28    # "rotateRect":Landroid/graphics/RectF;
    .restart local v29    # "startX":F
    .restart local v30    # "rotationRad":D
    .restart local v32    # "startY":F
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/graphics/RectF;

    .line 1633
    .local v26, "rectF":Landroid/graphics/RectF;
    move-object/from16 v0, v26

    iget v2, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v26

    iget v4, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, v26

    iget v5, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, v26

    iget v6, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1635
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1636
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    goto/16 :goto_0

    .line 1631
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4
.end method

.method protected handleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 27
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1872
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1873
    const/4 v4, 0x0

    .line 2012
    :goto_0
    return v4

    .line 1876
    :cond_0
    const/4 v5, 0x0

    .line 1877
    .local v5, "activeRect":Landroid/graphics/RectF;
    new-instance v22, Landroid/graphics/Point;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, v22

    invoke-direct {v0, v4, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 1879
    .local v22, "pos":Landroid/graphics/Point;
    new-instance v23, Landroid/graphics/Point;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Point;-><init>()V

    .line 1881
    .local v23, "pos_rotated":Landroid/graphics/Point;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    packed-switch v4, :pswitch_data_0

    .line 2012
    :goto_1
    :pswitch_0
    const/4 v4, 0x0

    goto :goto_0

    .line 1883
    :pswitch_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 1884
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 1886
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iput v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    iput v7, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    .line 1887
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    iput v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    iput v7, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    .line 1888
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 1890
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-lt v0, v4, :cond_2

    .line 1925
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iput v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    iput v7, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    .line 1926
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgPosition:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    iput v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    iput v7, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    .line 1928
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isPressed()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1929
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1891
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1892
    .local v21, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    move-object/from16 v0, v22

    iget v6, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v8

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v9

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    neg-float v4, v4

    float-to-double v10, v4

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    .end local v5    # "activeRect":Landroid/graphics/RectF;
    move-result-object v25

    .line 1893
    .local v25, "tempPoint":Landroid/graphics/PointF;
    new-instance v23, Landroid/graphics/Point;

    .end local v23    # "pos_rotated":Landroid/graphics/Point;
    move-object/from16 v0, v25

    iget v4, v0, Landroid/graphics/PointF;->x:F

    float-to-int v4, v4

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 1895
    .restart local v23    # "pos_rotated":Landroid/graphics/Point;
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v5

    .line 1896
    .restart local v5    # "activeRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 1898
    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v23

    iget v7, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v4, p0

    move-object/from16 v8, v21

    invoke-virtual/range {v4 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onCheckTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V

    .line 1899
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isPressed()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1900
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move/from16 v0, v17

    iput v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 1901
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 1903
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1904
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resize2Threshold(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1905
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v4, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1907
    move-object/from16 v0, p0

    instance-of v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-eqz v4, :cond_4

    .line 1908
    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 1914
    :cond_3
    :goto_3
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_1

    .line 1915
    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    .line 1917
    const/16 v19, 0x0

    .local v19, "j":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v19

    if-ge v0, v4, :cond_1

    .line 1918
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOrgRectList:Ljava/util/ArrayList;

    new-instance v7, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    invoke-direct {v7, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1917
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    .line 1910
    .end local v19    # "j":I
    :cond_4
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    goto :goto_3

    .line 1890
    :cond_5
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_2

    .line 1931
    .end local v21    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v25    # "tempPoint":Landroid/graphics/PointF;
    :cond_6
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1934
    .end local v17    # "i":I
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    goto/16 :goto_1

    .line 1938
    :pswitch_3
    const/16 v18, 0x0

    .line 1939
    .local v18, "index":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    if-ltz v4, :cond_7

    .line 1940
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move/from16 v18, v0

    .line 1942
    :cond_7
    move-object/from16 v0, v22

    iget v8, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v22

    iget v9, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v10

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v11

    .line 1943
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    neg-float v4, v4

    float-to-double v12, v4

    move-object/from16 v7, p0

    .line 1942
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v25

    .line 1944
    .restart local v25    # "tempPoint":Landroid/graphics/PointF;
    new-instance v23, Landroid/graphics/Point;

    .end local v23    # "pos_rotated":Landroid/graphics/Point;
    move-object/from16 v0, v25

    iget v4, v0, Landroid/graphics/PointF;->x:F

    float-to-int v4, v4

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 1946
    .restart local v23    # "pos_rotated":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v4

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1947
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleRotationControl(Landroid/graphics/Point;)V

    goto/16 :goto_1

    .line 1951
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1952
    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-direct {v4, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v6, Landroid/graphics/PointF;

    move-object/from16 v0, v25

    iget v7, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v25

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleResizeControl(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v4

    goto/16 :goto_0

    .line 1955
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1956
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleMoveControl(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 1957
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1959
    :cond_a
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1962
    .end local v18    # "index":I
    .end local v25    # "tempPoint":Landroid/graphics/PointF;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v4

    if-nez v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v4

    if-nez v4, :cond_b

    .line 1963
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1964
    :cond_b
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-lt v0, v4, :cond_10

    .line 1976
    .end local v17    # "i":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1977
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v17

    if-lt v0, v4, :cond_11

    .line 1997
    .end local v17    # "i":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v4

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    if-eqz v4, :cond_14

    .line 1998
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    .line 2005
    :cond_f
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 2006
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    .line 2007
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1965
    .restart local v17    # "i":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/graphics/RectF;

    .line 1966
    .local v24, "rectf":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v14

    .line 1967
    .local v14, "boundBox":Landroid/graphics/RectF;
    if-eqz v14, :cond_c

    if-eqz v24, :cond_c

    .line 1970
    invoke-virtual {v14}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    sub-float/2addr v4, v6

    float-to-int v15, v4

    .line 1971
    .local v15, "deltaX":I
    invoke-virtual {v14}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    sub-float/2addr v4, v6

    float-to-int v0, v4

    move/from16 v16, v0

    .line 1972
    .local v16, "deltaY":I
    int-to-float v4, v15

    move/from16 v0, v16

    int-to-float v6, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v6}, Landroid/graphics/RectF;->offset(FF)V

    .line 1964
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 1978
    .end local v14    # "boundBox":Landroid/graphics/RectF;
    .end local v15    # "deltaX":I
    .end local v16    # "deltaY":I
    .end local v24    # "rectf":Landroid/graphics/RectF;
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/graphics/RectF;

    .line 1981
    .local v20, "modifiedRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    if-eqz v4, :cond_12

    .line 1982
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    .line 1983
    .local v26, "tmpEdge":F
    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v20

    iput v4, v0, Landroid/graphics/RectF;->right:F

    .line 1984
    move/from16 v0, v26

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 1986
    .end local v26    # "tmpEdge":F
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    if-eqz v4, :cond_13

    .line 1987
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    .line 1988
    .restart local v26    # "tmpEdge":F
    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v20

    iput v4, v0, Landroid/graphics/RectF;->top:F

    .line 1989
    move/from16 v0, v26

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 1977
    .end local v26    # "tmpEdge":F
    :cond_13
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_6

    .line 2000
    .end local v17    # "i":I
    .end local v20    # "modifiedRect":Landroid/graphics/RectF;
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    if-eqz v4, :cond_15

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v6, 0x1

    if-eq v4, v6, :cond_f

    .line 2001
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto/16 :goto_7

    .line 1881
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected hideHandleView()V
    .locals 2

    .prologue
    .line 1509
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    if-eqz v0, :cond_0

    .line 1510
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setVisibility(I)V

    .line 1512
    :cond_0
    return-void
.end method

.method protected initHandleView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1546
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1547
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mControlBaseContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    .line 1548
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    .line 1549
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setRect(Landroid/graphics/RectF;)V

    .line 1550
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setAngle(F)V

    .line 1551
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setCenter(FF)V

    .line 1552
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;->setPaint(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;)V

    .line 1553
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandleView:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$HandleView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1555
    :cond_0
    return-void
.end method

.method protected isClippedObject(IZZZFFFZF)Z
    .locals 24
    .param p1, "index"    # I
    .param p2, "isRotating"    # Z
    .param p3, "isMoving"    # Z
    .param p4, "isResizing"    # Z
    .param p5, "changeX"    # F
    .param p6, "changeY"    # F
    .param p7, "angle"    # F
    .param p8, "isKeepRatio"    # Z
    .param p9, "ratio"    # F

    .prologue
    .line 2342
    const/4 v14, 0x0

    .line 2344
    .local v14, "rect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 2345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 2346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2349
    :cond_0
    if-nez p3, :cond_1

    if-eqz p4, :cond_3

    .line 2350
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "rect":Landroid/graphics/RectF;
    check-cast v14, Landroid/graphics/RectF;

    .line 2354
    .restart local v14    # "rect":Landroid/graphics/RectF;
    :cond_2
    :goto_0
    if-nez v14, :cond_4

    .line 2355
    const/16 v20, 0x0

    .line 3250
    :goto_1
    return v20

    .line 2351
    :cond_3
    if-eqz p2, :cond_2

    .line 2352
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v14

    goto :goto_0

    .line 2358
    :cond_4
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v12

    .line 2359
    .local v12, "px":F
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v13

    .line 2361
    .local v13, "py":F
    if-eqz p3, :cond_b

    .line 2362
    const/4 v8, 0x0

    .local v8, "deltaY":F
    const/4 v7, 0x0

    .line 2363
    .local v7, "deltaX":F
    new-instance v18, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2364
    .local v18, "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2366
    new-instance v19, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2367
    .local v19, "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2369
    new-instance v5, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2370
    .local v5, "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2372
    new-instance v6, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2373
    .local v6, "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2375
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2376
    .local v11, "points":[F
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 2377
    .local v9, "left":F
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 2378
    .local v17, "top":F
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 2379
    .local v15, "right":F
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 2381
    .local v4, "bottom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    cmpg-float v20, v9, v20

    if-gez v20, :cond_5

    .line 2382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v7, v20, v9

    .line 2384
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    cmpl-float v20, v15, v20

    if-lez v20, :cond_6

    .line 2385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    sub-float v7, v20, v15

    .line 2387
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpg-float v20, v17, v20

    if-gez v20, :cond_7

    .line 2388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v8, v20, v17

    .line 2390
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    cmpl-float v20, v4, v20

    if-lez v20, :cond_8

    .line 2391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v8, v20, v4

    .line 2394
    :cond_8
    const/16 v20, 0x0

    cmpl-float v20, v7, v20

    if-nez v20, :cond_9

    const/16 v20, 0x0

    cmpl-float v20, v8, v20

    if-eqz v20, :cond_a

    .line 2395
    :cond_9
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2396
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2397
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->x:F

    .line 2398
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 2399
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2400
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2401
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->y:F

    .line 2402
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->y:F

    .line 2404
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2405
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 2406
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 2407
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 2408
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 2410
    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-float v22, v9, v15

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v12, v0

    .line 2411
    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-float v22, v17, v4

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v13, v0

    .line 2412
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2413
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2414
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2415
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2417
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2418
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 2419
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 2420
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 2421
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 2422
    move/from16 v0, v17

    invoke-virtual {v14, v9, v0, v15, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2424
    :cond_a
    const/16 v20, 0x1

    goto/16 :goto_1

    .line 2425
    .end local v4    # "bottom":F
    .end local v5    # "bottomleft":Landroid/graphics/PointF;
    .end local v6    # "bottomright":Landroid/graphics/PointF;
    .end local v7    # "deltaX":F
    .end local v8    # "deltaY":F
    .end local v9    # "left":F
    .end local v11    # "points":[F
    .end local v15    # "right":F
    .end local v17    # "top":F
    .end local v18    # "topleft":Landroid/graphics/PointF;
    .end local v19    # "topright":Landroid/graphics/PointF;
    :cond_b
    if-eqz p4, :cond_c7

    .line 2426
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v10

    .line 2428
    .local v10, "originalRect":Landroid/graphics/RectF;
    const/4 v8, 0x0

    .restart local v8    # "deltaY":F
    const/4 v7, 0x0

    .line 2429
    .restart local v7    # "deltaX":F
    new-instance v18, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2430
    .restart local v18    # "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2432
    new-instance v19, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2433
    .restart local v19    # "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2435
    new-instance v5, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2436
    .restart local v5    # "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2438
    new-instance v6, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2439
    .restart local v6    # "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2441
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObjectMovingOutsideFrameRect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 2442
    const/16 v20, 0x1

    goto/16 :goto_1

    .line 2445
    :cond_c
    const/4 v11, 0x0

    .line 2447
    .restart local v11    # "points":[F
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2448
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 2449
    .restart local v9    # "left":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    cmpg-float v20, v9, v20

    if-gez v20, :cond_12

    .line 2451
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_d

    .line 2452
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 2454
    :cond_d
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_e

    .line 2455
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 2458
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v7, v20, v9

    .line 2460
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v9, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_24

    .line 2461
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2463
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2464
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2466
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2490
    :cond_f
    :goto_2
    const/16 v20, 0x0

    aget v20, v11, v20

    const/16 v21, 0x1

    aget v21, v11, v21

    const/16 v22, 0x2

    aget v22, v11, v22

    const/16 v23, 0x3

    aget v23, v11, v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2493
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2494
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2496
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2498
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2500
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2502
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2504
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2505
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-eqz v20, :cond_36

    .line 2508
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_10

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_2e

    .line 2509
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_28

    .line 2510
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_27

    .line 2511
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2563
    :cond_11
    :goto_3
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2564
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2566
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2567
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2569
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2570
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2572
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2573
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2649
    :cond_12
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2650
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 2651
    .restart local v17    # "top":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpg-float v20, v17, v20

    if-gez v20, :cond_18

    .line 2652
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_13

    .line 2653
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 2655
    :cond_13
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_14

    .line 2656
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 2659
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v8, v20, v17

    .line 2660
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v17, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_4d

    .line 2661
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2663
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2664
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2666
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2691
    :cond_15
    :goto_5
    const/16 v20, 0x0

    aget v20, v11, v20

    const/16 v21, 0x1

    aget v21, v11, v21

    const/16 v22, 0x2

    aget v22, v11, v22

    const/16 v23, 0x3

    aget v23, v11, v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2694
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2695
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2697
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2699
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2701
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2703
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2705
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2706
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-eqz v20, :cond_5f

    .line 2709
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_16

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_57

    .line 2710
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_51

    .line 2711
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_50

    .line 2712
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2764
    :cond_17
    :goto_6
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2765
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2767
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2768
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2770
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2771
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2773
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2774
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2850
    :cond_18
    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2851
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 2853
    .restart local v15    # "right":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    cmpl-float v20, v15, v20

    if-lez v20, :cond_1e

    .line 2854
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_19

    .line 2855
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 2857
    :cond_19
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_1a

    .line 2858
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 2861
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    sub-float v7, v20, v15

    .line 2862
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v15, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_76

    .line 2863
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2865
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2866
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2868
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2892
    :cond_1b
    :goto_8
    const/16 v20, 0x0

    aget v20, v11, v20

    const/16 v21, 0x1

    aget v21, v11, v21

    const/16 v22, 0x2

    aget v22, v11, v22

    const/16 v23, 0x3

    aget v23, v11, v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2895
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2896
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2898
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2899
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2901
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2903
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2905
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2907
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-eqz v20, :cond_88

    .line 2910
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_1c

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_80

    .line 2911
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_7a

    .line 2912
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_79

    .line 2913
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2965
    :cond_1d
    :goto_9
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2966
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2968
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2969
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2971
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2972
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2974
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2975
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 3050
    :cond_1e
    :goto_a
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 3051
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 3053
    .restart local v4    # "bottom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    cmpl-float v20, v4, v20

    if-lez v20, :cond_23

    .line 3054
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_1f

    .line 3055
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    .line 3057
    :cond_1f
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    move-result v20

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_20

    .line 3058
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    .line 3061
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v8, v20, v4

    .line 3062
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v4, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_9f

    .line 3064
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 3066
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 3067
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 3069
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 3095
    :cond_21
    :goto_b
    const/16 v20, 0x0

    aget v20, v11, v20

    const/16 v21, 0x1

    aget v21, v11, v21

    const/16 v22, 0x2

    aget v22, v11, v22

    const/16 v23, 0x3

    aget v23, v11, v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3098
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 3099
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 3101
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 3102
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 3104
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 3105
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 3107
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 3108
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 3110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-eqz v20, :cond_b1

    .line 3111
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_22

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a9

    .line 3112
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_a3

    .line 3113
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a2

    .line 3114
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 3250
    .end local v4    # "bottom":F
    .end local v5    # "bottomleft":Landroid/graphics/PointF;
    .end local v6    # "bottomright":Landroid/graphics/PointF;
    .end local v7    # "deltaX":F
    .end local v8    # "deltaY":F
    .end local v9    # "left":F
    .end local v10    # "originalRect":Landroid/graphics/RectF;
    .end local v11    # "points":[F
    .end local v15    # "right":F
    .end local v17    # "top":F
    .end local v18    # "topleft":Landroid/graphics/PointF;
    .end local v19    # "topright":Landroid/graphics/PointF;
    :cond_23
    :goto_c
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 2467
    .restart local v5    # "bottomleft":Landroid/graphics/PointF;
    .restart local v6    # "bottomright":Landroid/graphics/PointF;
    .restart local v7    # "deltaX":F
    .restart local v8    # "deltaY":F
    .restart local v9    # "left":F
    .restart local v10    # "originalRect":Landroid/graphics/RectF;
    .restart local v11    # "points":[F
    .restart local v18    # "topleft":Landroid/graphics/PointF;
    .restart local v19    # "topright":Landroid/graphics/PointF;
    :cond_24
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v9, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_25

    .line 2468
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2470
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2471
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2473
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2474
    goto/16 :goto_2

    :cond_25
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v9, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_26

    .line 2475
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->x:F

    .line 2477
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2478
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2480
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2481
    goto/16 :goto_2

    :cond_26
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v9, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_f

    .line 2482
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 2484
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2485
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2487
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    goto/16 :goto_2

    .line 2513
    :cond_27
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    .line 2515
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2a

    .line 2516
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_29

    .line 2517
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    .line 2519
    :cond_29
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    .line 2521
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2c

    .line 2522
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_2b

    .line 2523
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    .line 2525
    :cond_2b
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 2527
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    .line 2528
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_2d

    .line 2529
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 2531
    :cond_2d
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    .line 2535
    :cond_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_30

    .line 2536
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_2f

    .line 2537
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    .line 2539
    :cond_2f
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    .line 2541
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_32

    .line 2542
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_31

    .line 2543
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    .line 2545
    :cond_31
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    .line 2547
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_34

    .line 2548
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_33

    .line 2549
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 2551
    :cond_33
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    .line 2553
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    .line 2554
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_35

    .line 2555
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    .line 2557
    :cond_35
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    .line 2574
    :cond_36
    if-eqz p8, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-nez v20, :cond_12

    .line 2575
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_37

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_3c

    .line 2576
    :cond_37
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_39

    .line 2577
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2634
    :cond_38
    :goto_d
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2635
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2637
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2638
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2640
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2641
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2643
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2644
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    goto/16 :goto_4

    .line 2578
    :cond_39
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3a

    .line 2579
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2580
    :cond_3a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3b

    .line 2581
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2582
    :cond_3b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_38

    .line 2583
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2586
    :cond_3c
    move/from16 v16, p7

    .line 2587
    .local v16, "tmp_angle":F
    const/16 v20, 0x0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_3d

    .line 2588
    const/high16 v20, 0x43b40000    # 360.0f

    add-float v16, v16, v20

    .line 2591
    :cond_3d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_41

    .line 2592
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_3e

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_3e

    .line 2593
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2594
    :cond_3e
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_3f

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_3f

    .line 2595
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2596
    :cond_3f
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_40

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_40

    .line 2597
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    .line 2598
    :cond_40
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_38

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_38

    .line 2599
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    .line 2601
    :cond_41
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_45

    .line 2602
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_42

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_42

    .line 2603
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2604
    :cond_42
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_43

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_43

    .line 2605
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2606
    :cond_43
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_44

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_44

    .line 2607
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2608
    :cond_44
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_38

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_38

    .line 2609
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    .line 2611
    :cond_45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_49

    .line 2612
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_46

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_46

    .line 2613
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2614
    :cond_46
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_47

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_47

    .line 2615
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2616
    :cond_47
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_48

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_48

    .line 2617
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    .line 2618
    :cond_48
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_38

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_38

    .line 2619
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2621
    :cond_49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_38

    .line 2622
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_4a

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_4a

    .line 2623
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2624
    :cond_4a
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_4b

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_4b

    .line 2625
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    .line 2626
    :cond_4b
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_4c

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_4c

    .line 2627
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    .line 2628
    :cond_4c
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_38

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_38

    .line 2629
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    .line 2667
    .end local v16    # "tmp_angle":F
    .restart local v17    # "top":F
    :cond_4d
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v17, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_4e

    .line 2668
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 2670
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2671
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2673
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2674
    goto/16 :goto_5

    :cond_4e
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v17, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_4f

    .line 2676
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->y:F

    .line 2678
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2679
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2681
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2682
    goto/16 :goto_5

    :cond_4f
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v17, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_15

    .line 2683
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->y:F

    .line 2685
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2686
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2688
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    goto/16 :goto_5

    .line 2714
    :cond_50
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    .line 2716
    :cond_51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_53

    .line 2717
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_52

    .line 2718
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    .line 2720
    :cond_52
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    .line 2722
    :cond_53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_55

    .line 2723
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_54

    .line 2724
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    .line 2726
    :cond_54
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    .line 2728
    :cond_55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    .line 2729
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_56

    .line 2730
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    .line 2732
    :cond_56
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    .line 2736
    :cond_57
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_59

    .line 2737
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_58

    .line 2738
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    .line 2740
    :cond_58
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    .line 2742
    :cond_59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5b

    .line 2743
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_5a

    .line 2744
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    .line 2746
    :cond_5a
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    .line 2748
    :cond_5b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5d

    .line 2749
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_5c

    .line 2750
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    .line 2752
    :cond_5c
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    .line 2754
    :cond_5d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    .line 2755
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_5e

    .line 2756
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    .line 2758
    :cond_5e
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    .line 2775
    :cond_5f
    if-eqz p8, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-nez v20, :cond_18

    .line 2776
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_60

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_65

    .line 2777
    :cond_60
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_62

    .line 2778
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 2835
    :cond_61
    :goto_e
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 2836
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2838
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 2839
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2841
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2842
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2844
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 2845
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    goto/16 :goto_7

    .line 2779
    :cond_62
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_63

    .line 2780
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2781
    :cond_63
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_64

    .line 2782
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2783
    :cond_64
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_61

    .line 2784
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2787
    :cond_65
    move/from16 v16, p7

    .line 2788
    .restart local v16    # "tmp_angle":F
    const/16 v20, 0x0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_66

    .line 2789
    const/high16 v20, 0x43b40000    # 360.0f

    add-float v16, v16, v20

    .line 2792
    :cond_66
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_6a

    .line 2793
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_67

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_67

    .line 2794
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_e

    .line 2795
    :cond_67
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_68

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_68

    .line 2796
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2797
    :cond_68
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_69

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_69

    .line 2798
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2799
    :cond_69
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_61

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_61

    .line 2800
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_e

    .line 2802
    :cond_6a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_6e

    .line 2803
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_6b

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_6b

    .line 2804
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_e

    .line 2805
    :cond_6b
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_6c

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_6c

    .line 2806
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2807
    :cond_6c
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_6d

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_6d

    .line 2808
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2809
    :cond_6d
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_61

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_61

    .line 2810
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2812
    :cond_6e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_72

    .line 2813
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_6f

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_6f

    .line 2814
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2815
    :cond_6f
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_70

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_70

    .line 2816
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2817
    :cond_70
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_71

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_71

    .line 2818
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2819
    :cond_71
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_61

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_61

    .line 2820
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_e

    .line 2822
    :cond_72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_61

    .line 2823
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_73

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_73

    .line 2824
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2825
    :cond_73
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_74

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_74

    .line 2826
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_e

    .line 2827
    :cond_74
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_75

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_75

    .line 2828
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_e

    .line 2829
    :cond_75
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_61

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_61

    .line 2830
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_e

    .line 2869
    .end local v16    # "tmp_angle":F
    .restart local v15    # "right":F
    :cond_76
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v15, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_77

    .line 2870
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 2872
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2873
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2875
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2876
    goto/16 :goto_8

    :cond_77
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v15, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_78

    .line 2877
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->x:F

    .line 2879
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 2880
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 2882
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 2883
    goto/16 :goto_8

    :cond_78
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v20, v15, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_1b

    .line 2884
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    add-float v20, v20, v7

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->x:F

    .line 2886
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 2887
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 2889
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    goto/16 :goto_8

    .line 2915
    :cond_79
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    .line 2917
    :cond_7a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_7c

    .line 2918
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7b

    .line 2919
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    .line 2921
    :cond_7b
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 2923
    :cond_7c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_7e

    .line 2924
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7d

    .line 2925
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 2927
    :cond_7d
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 2929
    :cond_7e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1d

    .line 2930
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7f

    .line 2931
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 2933
    :cond_7f
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    .line 2937
    :cond_80
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_82

    .line 2938
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_81

    .line 2939
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    .line 2941
    :cond_81
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    .line 2943
    :cond_82
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_84

    .line 2944
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_83

    .line 2945
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 2947
    :cond_83
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    .line 2949
    :cond_84
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_86

    .line 2950
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_85

    .line 2951
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 2953
    :cond_85
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    .line 2955
    :cond_86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1d

    .line 2956
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_87

    .line 2957
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    .line 2959
    :cond_87
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    .line 2976
    :cond_88
    if-eqz p8, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-nez v20, :cond_1e

    .line 2977
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_89

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_8e

    .line 2978
    :cond_89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8b

    .line 2979
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    .line 3036
    :cond_8a
    :goto_f
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 3037
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 3039
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/graphics/PointF;->set(FF)V

    .line 3040
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 3042
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 3043
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 3045
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 3046
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    goto/16 :goto_a

    .line 2980
    :cond_8b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8c

    .line 2981
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 2982
    :cond_8c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8d

    .line 2983
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 2984
    :cond_8d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8a

    .line 2985
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 2988
    :cond_8e
    move/from16 v16, p7

    .line 2989
    .restart local v16    # "tmp_angle":F
    const/16 v20, 0x0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8f

    .line 2990
    const/high16 v20, 0x43b40000    # 360.0f

    add-float v16, v16, v20

    .line 2993
    :cond_8f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_93

    .line 2994
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_90

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_90

    .line 2995
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    .line 2996
    :cond_90
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_91

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_91

    .line 2997
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    .line 2998
    :cond_91
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_92

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_92

    .line 2999
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 3000
    :cond_92
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_8a

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8a

    .line 3001
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 3003
    :cond_93
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_97

    .line 3004
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_94

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_94

    .line 3005
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 3006
    :cond_94
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_95

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_95

    .line 3007
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    .line 3008
    :cond_95
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_96

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_96

    .line 3009
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 3010
    :cond_96
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_8a

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8a

    .line 3011
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 3013
    :cond_97
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_9b

    .line 3014
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_98

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_98

    .line 3015
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 3016
    :cond_98
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_99

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_99

    .line 3017
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 3018
    :cond_99
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_9a

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9a

    .line 3019
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 3020
    :cond_9a
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_8a

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8a

    .line 3021
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 3023
    :cond_9b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8a

    .line 3024
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_9c

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9c

    .line 3025
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    .line 3026
    :cond_9c
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_9d

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9d

    .line 3027
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 3028
    :cond_9d
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_9e

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9e

    .line 3029
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    .line 3030
    :cond_9e
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_8a

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_8a

    .line 3031
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    .line 3070
    .end local v16    # "tmp_angle":F
    .restart local v4    # "bottom":F
    :cond_9f
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v4, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a0

    .line 3071
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 3073
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 3074
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 3076
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 3077
    goto/16 :goto_b

    :cond_a0
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v4, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a1

    .line 3079
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v5, Landroid/graphics/PointF;->y:F

    .line 3081
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 3082
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 3084
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 3086
    goto/16 :goto_b

    :cond_a1
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v20, v4, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_21

    .line 3087
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    add-float v20, v20, v8

    move/from16 v0, v20

    iput v0, v6, Landroid/graphics/PointF;->y:F

    .line 3089
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 3090
    move/from16 v0, p7

    neg-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 3092
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    goto/16 :goto_b

    .line 3116
    :cond_a2
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3118
    :cond_a3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_a5

    .line 3119
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a4

    .line 3120
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3122
    :cond_a4
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3124
    :cond_a5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_a7

    .line 3125
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a6

    .line 3126
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3128
    :cond_a6
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3130
    :cond_a7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 3131
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_a8

    .line 3132
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3134
    :cond_a8
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3138
    :cond_a9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_ab

    .line 3139
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_aa

    .line 3140
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3142
    :cond_aa
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3144
    :cond_ab
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_ad

    .line 3145
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_ac

    .line 3146
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3148
    :cond_ac
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3150
    :cond_ad
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_af

    .line 3151
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_ae

    .line 3152
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3154
    :cond_ae
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3156
    :cond_af
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 3157
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    cmpg-float v20, v20, v21

    if-gez v20, :cond_b0

    .line 3158
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3160
    :cond_b0
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3164
    :cond_b1
    if-eqz p8, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v20

    if-nez v20, :cond_23

    .line 3165
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-ltz v20, :cond_b2

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v21, 0x38d1b717    # 1.0E-4f

    cmpg-float v20, v20, v21

    if-gez v20, :cond_b6

    .line 3166
    :cond_b2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b3

    .line 3167
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3168
    :cond_b3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b4

    .line 3169
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3170
    :cond_b4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b5

    .line 3171
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3172
    :cond_b5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 3173
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3176
    :cond_b6
    move/from16 v16, p7

    .line 3177
    .restart local v16    # "tmp_angle":F
    const/16 v20, 0x0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_b7

    .line 3178
    const/high16 v20, 0x43b40000    # 360.0f

    add-float v16, v16, v20

    .line 3181
    :cond_b7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_bb

    .line 3182
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_b8

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_b8

    .line 3183
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3184
    :cond_b8
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_b9

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_b9

    .line 3185
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3186
    :cond_b9
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_ba

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_ba

    .line 3187
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3188
    :cond_ba
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_23

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_23

    .line 3189
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3191
    :cond_bb
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_bf

    .line 3192
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_bc

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_bc

    .line 3193
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3194
    :cond_bc
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_bd

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_bd

    .line 3195
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3196
    :cond_bd
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_be

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_be

    .line 3197
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3198
    :cond_be
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_23

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_23

    .line 3199
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3201
    :cond_bf
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x7

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c3

    .line 3202
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c0

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c0

    .line 3203
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3204
    :cond_c0
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c1

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c1

    .line 3205
    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    .line 3206
    :cond_c1
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c2

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c2

    .line 3207
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3208
    :cond_c2
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_23

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_23

    .line 3209
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3211
    :cond_c3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 3212
    const/16 v20, 0x0

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c4

    const/high16 v20, 0x42b40000    # 90.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c4

    .line 3213
    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    sub-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    .line 3214
    :cond_c4
    const/high16 v20, 0x42b40000    # 90.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c5

    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c5

    .line 3215
    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    div-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    .line 3216
    :cond_c5
    const/high16 v20, 0x43340000    # 180.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_c6

    const/high16 v20, 0x43870000    # 270.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_c6

    .line 3217
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3218
    :cond_c6
    const/high16 v20, 0x43870000    # 270.0f

    cmpl-float v20, v16, v20

    if-lez v20, :cond_23

    const/high16 v20, 0x43b40000    # 360.0f

    cmpg-float v20, v16, v20

    if-gez v20, :cond_23

    .line 3219
    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    mul-float v21, v21, p9

    add-float v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    .line 3225
    .end local v4    # "bottom":F
    .end local v5    # "bottomleft":Landroid/graphics/PointF;
    .end local v6    # "bottomright":Landroid/graphics/PointF;
    .end local v7    # "deltaX":F
    .end local v8    # "deltaY":F
    .end local v9    # "left":F
    .end local v10    # "originalRect":Landroid/graphics/RectF;
    .end local v11    # "points":[F
    .end local v15    # "right":F
    .end local v16    # "tmp_angle":F
    .end local v17    # "top":F
    .end local v18    # "topleft":Landroid/graphics/PointF;
    .end local v19    # "topright":Landroid/graphics/PointF;
    :cond_c7
    if-eqz p2, :cond_23

    .line 3226
    new-instance v18, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3227
    .restart local v18    # "topleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 3229
    new-instance v19, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3230
    .restart local v19    # "topright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, v19

    invoke-virtual {v0, v12, v13, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v19

    .line 3232
    new-instance v5, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3233
    .restart local v5    # "bottomleft":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    .line 3235
    new-instance v6, Landroid/graphics/PointF;

    iget v0, v14, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3236
    .restart local v6    # "bottomright":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-virtual {v0, v12, v13, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    .line 3238
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v11

    .line 3239
    .restart local v11    # "points":[F
    const/16 v20, 0x0

    aget v9, v11, v20

    .line 3240
    .restart local v9    # "left":F
    const/16 v20, 0x1

    aget v17, v11, v20

    .line 3241
    .restart local v17    # "top":F
    const/16 v20, 0x2

    aget v15, v11, v20

    .line 3242
    .restart local v15    # "right":F
    const/16 v20, 0x3

    aget v4, v11, v20

    .line 3244
    .restart local v4    # "bottom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    cmpg-float v20, v9, v20

    if-ltz v20, :cond_c8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    cmpl-float v20, v15, v20

    if-gtz v20, :cond_c8

    .line 3245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpg-float v20, v17, v20

    if-ltz v20, :cond_c8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    cmpl-float v20, v4, v20

    if-lez v20, :cond_23

    .line 3246
    :cond_c8
    const/16 v20, 0x1

    goto/16 :goto_1
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 3989
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    return v0
.end method

.method public isContextMenuItemEnabled(I)Z
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 4091
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-nez v0, :cond_0

    .line 4092
    const/4 v0, 0x0

    .line 4095
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getItemEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    .prologue
    .line 4142
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    return v0
.end method

.method public isDimEnabled()Z
    .locals 1

    .prologue
    .line 4237
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    return v0
.end method

.method public isObjectOutlineEnabled()Z
    .locals 1

    .prologue
    .line 4752
    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    return v0
.end method

.method public isTouchEnabled()Z
    .locals 1

    .prologue
    .line 4171
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    return v0
.end method

.method protected onCanvasHoverEnter()V
    .locals 1

    .prologue
    .line 4760
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4761
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    .line 4763
    :cond_1
    return-void
.end method

.method protected onCheckTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V
    .locals 6
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .param p5, "touchState"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    .prologue
    .line 3258
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->checkTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V

    .line 3260
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x0

    .line 4282
    if-eqz p1, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 4332
    :cond_0
    :goto_0
    return-void

    .line 4286
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    if-eqz v4, :cond_2

    .line 4287
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-boolean v6, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    .line 4289
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onPrepareDraw(Landroid/graphics/Canvas;)V

    .line 4292
    :cond_2
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    .line 4294
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4301
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    if-eqz v4, :cond_3

    instance-of v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-nez v4, :cond_3

    .line 4302
    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 4305
    :cond_3
    const/4 v0, 0x0

    .line 4306
    .local v0, "activeRect":Landroid/graphics/RectF;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_4

    .line 4328
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsObjectChange:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v4, :cond_0

    .line 4329
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-interface {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onObjectChanged(Ljava/util/ArrayList;)V

    .line 4330
    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsObjectChange:Z

    goto :goto_0

    .line 4307
    :cond_4
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    .line 4308
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    .line 4309
    .local v1, "centerX":F
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    .line 4310
    .local v2, "centerY":F
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 4312
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    if-eqz v4, :cond_5

    instance-of v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-nez v4, :cond_5

    .line 4313
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 4314
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    invoke-virtual {p1, v4, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4315
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawDimmingWindow(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 4316
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 4318
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v4, :cond_6

    .line 4319
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    .line 4322
    :cond_6
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 4323
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    invoke-virtual {p1, v4, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 4324
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {p0, p1, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 4325
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 4306
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method protected onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 25
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/RectF;
    .param p3, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3331
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v8, 0x1

    if-ne v3, v8, :cond_1

    .line 3395
    :cond_0
    :goto_0
    return-void

    .line 3335
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v3

    const/4 v8, 0x2

    if-eq v3, v8, :cond_5

    const/4 v13, 0x1

    .line 3336
    .local v13, "isEightPoint":Z
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v15

    .line 3338
    .local v15, "isRotatable":Z
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_6

    const/4 v14, 0x1

    .line 3340
    .local v14, "isKeepRatio":Z
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v8, 0x2

    if-ne v3, v8, :cond_2

    .line 3341
    const/4 v13, 0x0

    .line 3342
    const/4 v15, 0x0

    .line 3345
    :cond_2
    if-eqz v15, :cond_4

    .line 3346
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v3, v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v20

    .line 3347
    .local v20, "rotateRect":Landroid/graphics/RectF;
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    .line 3348
    .local v4, "startX":F
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    .line 3349
    .local v5, "startY":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    .line 3350
    .local v6, "stopX":F
    move-object/from16 v0, p2

    iget v7, v0, Landroid/graphics/RectF;->top:F

    .line 3352
    .local v7, "stopY":F
    const-string v3, "handler_icon_rotate"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 3353
    .local v11, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v11, :cond_4

    .line 3354
    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v8, 0x40000000    # 2.0f

    div-float v12, v3, v8

    .line 3355
    .local v12, "hx":F
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_3

    .line 3356
    const/high16 v3, 0x42340000    # 45.0f

    mul-float/2addr v3, v12

    const/high16 v8, 0x41f00000    # 30.0f

    div-float v12, v3, v8

    .line 3359
    :cond_3
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    .line 3360
    .local v9, "CenterX":F
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerY()F

    move-result v10

    .line 3361
    .local v10, "CenterY":F
    const/high16 v3, 0x40800000    # 4.0f

    sub-float v19, v12, v3

    .line 3362
    .local v19, "radius":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v21, v0

    .line 3363
    new-instance v22, Landroid/graphics/PointF;

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerX()F

    move-result v23

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerY()F

    move-result v24

    invoke-direct/range {v22 .. v24}, Landroid/graphics/PointF;-><init>(FF)V

    .line 3362
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v3, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v16

    .line 3364
    .local v16, "knobPos":Landroid/graphics/PointF;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getTemplateProperty()Z

    .line 3365
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup;

    .line 3366
    .local v18, "parentLayout":Landroid/view/ViewGroup;
    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/PointF;->x:F

    add-float v3, v3, v19

    const/4 v8, 0x0

    cmpl-float v3, v3, v8

    if-lez v3, :cond_4

    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/PointF;->x:F

    sub-float v3, v3, v19

    invoke-virtual/range {v18 .. v18}, Landroid/view/ViewGroup;->getWidth()I

    move-result v8

    int-to-float v8, v8

    cmpg-float v3, v3, v8

    if-gez v3, :cond_4

    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/PointF;->y:F

    add-float v3, v3, v19

    const/4 v8, 0x0

    cmpl-float v3, v3, v8

    if-lez v3, :cond_4

    .line 3367
    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/PointF;->y:F

    sub-float v3, v3, v19

    invoke-virtual/range {v18 .. v18}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    int-to-float v8, v8

    cmpg-float v3, v3, v8

    if-gez v3, :cond_4

    .line 3368
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v8

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 3369
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/16 v8, 0x9

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v9, v10, v1, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 3374
    .end local v4    # "startX":F
    .end local v5    # "startY":F
    .end local v6    # "stopX":F
    .end local v7    # "stopY":F
    .end local v9    # "CenterX":F
    .end local v10    # "CenterY":F
    .end local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v12    # "hx":F
    .end local v16    # "knobPos":Landroid/graphics/PointF;
    .end local v18    # "parentLayout":Landroid/view/ViewGroup;
    .end local v19    # "radius":F
    .end local v20    # "rotateRect":Landroid/graphics/RectF;
    :cond_4
    if-eqz v13, :cond_8

    .line 3375
    const/4 v11, 0x0

    .line 3376
    .restart local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_7

    .line 3377
    const-string v3, "handler_icon"

    const/16 v8, 0x21

    .line 3378
    const/16 v21, 0x21

    .line 3377
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v3, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 3384
    :goto_3
    if-eqz v11, :cond_0

    .line 3385
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 3386
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v11, v1, v2, v14}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->draw8Points(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    goto/16 :goto_0

    .line 3335
    .end local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v13    # "isEightPoint":Z
    .end local v14    # "isKeepRatio":Z
    .end local v15    # "isRotatable":Z
    :cond_5
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 3338
    .restart local v13    # "isEightPoint":Z
    .restart local v15    # "isRotatable":Z
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 3380
    .restart local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v14    # "isKeepRatio":Z
    :cond_7
    const-string v3, "handler_icon"

    const/16 v8, 0x16

    .line 3381
    const/16 v21, 0x16

    .line 3380
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v3, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    goto :goto_3

    .line 3389
    .end local v11    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v17

    .line 3390
    .local v17, "paint":Landroid/graphics/Paint;
    const/high16 v3, 0x40800000    # 4.0f

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 3391
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/RectF;->left:F

    const/high16 v8, 0x40800000    # 4.0f

    sub-float/2addr v3, v8

    const/high16 v8, 0x3f800000    # 1.0f

    add-float/2addr v3, v8

    move-object/from16 v0, p2

    iget v8, v0, Landroid/graphics/RectF;->top:F

    const/high16 v21, 0x40800000    # 4.0f

    sub-float v8, v8, v21

    const/high16 v21, 0x3f800000    # 1.0f

    add-float v8, v8, v21

    .line 3392
    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    const/high16 v22, 0x40800000    # 4.0f

    add-float v21, v21, v22

    const/high16 v22, 0x3f800000    # 1.0f

    sub-float v21, v21, v22

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v22, v0

    const/high16 v23, 0x40800000    # 4.0f

    add-float v22, v22, v23

    const/high16 v23, 0x3f800000    # 1.0f

    sub-float v22, v22, v23

    .line 3391
    move-object/from16 v0, p2

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v3, v8, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3393
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method protected onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3647
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 3648
    .local v0, "tmpRect":Landroid/graphics/RectF;
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 3649
    iget v1, p2, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 3650
    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 3652
    :cond_0
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 3653
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 3654
    iget v1, p2, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 3657
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3658
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 3661
    :cond_2
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SM-P90"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3662
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 3667
    :goto_0
    return-void

    .line 3666
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 0
    .param p1, "flipDirection"    # I
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3674
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 3800
    const/4 v0, 0x0

    return v0
.end method

.method protected onMenuSelected(I)V
    .locals 2
    .param p1, "itemId"    # I

    .prologue
    .line 3617
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 3618
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onMenuSelected(Ljava/util/ArrayList;I)V

    .line 3620
    :cond_0
    return-void
.end method

.method protected onObjectChanged()V
    .locals 24

    .prologue
    .line 3457
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    if-eqz v2, :cond_1

    .line 3574
    :cond_0
    :goto_0
    return-void

    .line 3461
    :cond_1
    const/16 v17, 0x1

    .line 3462
    .local v17, "isTransaction":Z
    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    .line 3463
    .local v20, "objectRect":Landroid/graphics/RectF;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_2

    .line 3497
    if-nez v17, :cond_9

    .line 3498
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 3499
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_0

    .line 3500
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateContextMenu()V

    .line 3501
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto :goto_0

    .line 3464
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 3465
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/graphics/RectF;

    .line 3467
    .local v21, "rectF":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3468
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3469
    const/16 v17, 0x0

    .line 3470
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 3463
    .end local v21    # "rectF":Landroid/graphics/RectF;
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3472
    .restart local v21    # "rectF":Landroid/graphics/RectF;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 3473
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    .line 3475
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3476
    const/4 v13, 0x0

    .line 3477
    .local v13, "deltaX":F
    const/4 v14, 0x0

    .line 3479
    .local v14, "deltaY":F
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->left:F

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_7

    .line 3480
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->left:F

    neg-float v13, v2

    .line 3485
    :cond_5
    :goto_3
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->top:F

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_8

    .line 3486
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->top:F

    neg-float v14, v2

    .line 3491
    :cond_6
    :goto_4
    move-object/from16 v0, v20

    invoke-virtual {v0, v13, v14}, Landroid/graphics/RectF;->offset(FF)V

    .line 3492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    goto :goto_2

    .line 3481
    :cond_7
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDocWidth:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_5

    .line 3482
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDocWidth:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->right:F

    sub-float v13, v2, v4

    goto :goto_3

    .line 3487
    :cond_8
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDocHeight:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_6

    .line 3488
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDocHeight:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v14, v2, v4

    goto :goto_4

    .line 3506
    .end local v13    # "deltaX":F
    .end local v14    # "deltaY":F
    .end local v21    # "rectF":Landroid/graphics/RectF;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v2, :cond_b

    .line 3507
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3508
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3510
    const/16 v22, 0x0

    .line 3511
    .local v22, "rotateDiff":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v15, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 3512
    .local v15, "index":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_a

    .line 3513
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v4

    sub-float v22, v2, v4

    .line 3516
    :cond_a
    const/16 v16, 0x0

    .line 3517
    .local v16, "isResize2Threshold":Z
    const/4 v3, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v3, v2, :cond_c

    .line 3568
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsObjectChange:Z

    .line 3571
    .end local v15    # "index":I
    .end local v16    # "isResize2Threshold":Z
    .end local v22    # "rotateDiff":F
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 3573
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    goto/16 :goto_0

    .line 3518
    .restart local v15    # "index":I
    .restart local v16    # "isResize2Threshold":Z
    .restart local v22    # "rotateDiff":F
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v2

    if-nez v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_e

    .line 3519
    :cond_d
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resize2Threshold(I)Z

    move-result v16

    .line 3522
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 3524
    .local v19, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isFlipEnabled()Z

    move-result v2

    if-nez v2, :cond_10

    .line 3526
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_f

    .line 3527
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v23, v0

    .line 3528
    .local v23, "temp":F
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v20

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 3529
    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 3531
    .end local v23    # "temp":F
    :cond_f
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_10

    .line 3532
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    .line 3533
    .restart local v23    # "temp":F
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, v20

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 3534
    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 3538
    .end local v23    # "temp":F
    :cond_10
    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;

    if-nez v2, :cond_11

    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    if-eqz v2, :cond_16

    .line 3539
    :cond_11
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v12

    .line 3540
    .local v12, "currObjectRect":Landroid/graphics/RectF;
    if-eqz v16, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_15

    .line 3541
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 3542
    const/4 v2, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 3550
    .end local v12    # "currObjectRect":Landroid/graphics/RectF;
    :goto_6
    const/16 v18, 0x0

    .line 3551
    .local v18, "objAng":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move/from16 v18, v0

    .line 3553
    if-eq v3, v15, :cond_12

    .line 3554
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v2

    add-float v2, v2, v22

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v18

    .line 3556
    :cond_12
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 3557
    move/from16 v0, v18

    float-to-int v2, v0

    int-to-float v2, v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotation(F)V

    .line 3560
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v2

    if-nez v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isPressed()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 3561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3562
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    .line 3563
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3564
    const/4 v2, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 3517
    :cond_14
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    .line 3544
    .end local v18    # "objAng":F
    .restart local v12    # "currObjectRect":Landroid/graphics/RectF;
    :cond_15
    const/4 v2, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    goto/16 :goto_6

    .line 3547
    .end local v12    # "currObjectRect":Landroid/graphics/RectF;
    :cond_16
    const/4 v2, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    goto/16 :goto_6
.end method

.method protected onPrepareDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 4245
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    if-eqz v0, :cond_0

    .line 4246
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    .line 4248
    :cond_0
    return-void
.end method

.method protected onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3627
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 3628
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3630
    :cond_0
    return-void
.end method

.method protected onRequestScroll(FF)V
    .locals 1
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 3447
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 3448
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestScroll(FF)V

    .line 3450
    :cond_0
    return-void
.end method

.method protected onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1
    .param p1, "angle"    # F
    .param p2, "objectBase"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 3637
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 3638
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    .line 3640
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 20
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 3702
    invoke-super/range {p0 .. p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 3704
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-nez v15, :cond_1

    .line 3767
    :cond_0
    :goto_0
    return-void

    .line 3708
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 3712
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 3713
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    invoke-interface/range {v15 .. v16}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3715
    const/4 v14, 0x0

    .line 3717
    .local v14, "scrollDirty":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->setDirty()V

    .line 3718
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 3720
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v15, v15, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v6, v15, Landroid/graphics/PointF;->x:F

    .line 3721
    .local v6, "curPanX":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v15, v15, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v7, v15, Landroid/graphics/PointF;->y:F

    .line 3723
    .local v7, "curPanY":F
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getPanKey(II)Ljava/lang/String;

    move-result-object v9

    .line 3724
    .local v9, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    invoke-virtual {v15, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Point;

    .line 3725
    .local v11, "pan":Landroid/graphics/Point;
    if-eqz v11, :cond_2

    .line 3726
    iget v15, v11, Landroid/graphics/Point;->x:I

    int-to-float v15, v15

    sub-float/2addr v15, v6

    iget v0, v11, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, v7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRequestScroll(FF)V

    .line 3727
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    invoke-virtual {v15, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3728
    const/4 v14, 0x1

    .line 3731
    :cond_2
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 3732
    .local v4, "absoluteScreenRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v15, v15, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v15, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3734
    const/4 v15, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v10

    .line 3735
    .local v10, "objectBoundBox":Landroid/graphics/RectF;
    if-eqz v10, :cond_0

    .line 3738
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 3739
    .local v3, "absoluteObjectBoundBox":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 3741
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getPanKey(II)Ljava/lang/String;

    move-result-object v16

    new-instance v17, Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    .line 3742
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-direct/range {v17 .. v19}, Landroid/graphics/Point;-><init>(II)V

    .line 3741
    invoke-virtual/range {v15 .. v17}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3744
    invoke-virtual {v4, v3}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v5

    .line 3745
    .local v5, "containFlag":Z
    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v8

    .line 3746
    .local v8, "intersectFlag":Z
    if-eqz v8, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isOutOfViewEnabled()Z

    move-result v15

    if-nez v15, :cond_6

    if-nez v5, :cond_6

    .line 3747
    :cond_3
    const/4 v12, 0x0

    .local v12, "panX":F
    const/4 v13, 0x0

    .line 3748
    .local v13, "panY":F
    iget v15, v10, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_7

    .line 3749
    iget v15, v10, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v16, v0

    div-float v13, v15, v16

    .line 3754
    :cond_4
    :goto_1
    iget v15, v10, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v16, v0

    cmpg-float v15, v15, v16

    if-gez v15, :cond_8

    .line 3755
    iget v15, v10, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v16, v0

    div-float v12, v15, v16

    .line 3760
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRequestScroll(FF)V

    .line 3761
    const/4 v14, 0x1

    .line 3764
    .end local v12    # "panX":F
    .end local v13    # "panY":F
    :cond_6
    if-eqz v14, :cond_0

    .line 3765
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    goto/16 :goto_0

    .line 3750
    .restart local v12    # "panX":F
    .restart local v13    # "panY":F
    :cond_7
    iget v15, v10, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    cmpg-float v15, v15, v16

    if-gez v15, :cond_4

    .line 3751
    iget v15, v10, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v16, v0

    div-float v13, v15, v16

    goto/16 :goto_1

    .line 3756
    :cond_8
    iget v15, v10, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_5

    .line 3757
    iget v15, v10, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    move/from16 v16, v0

    div-float v12, v15, v16

    goto/16 :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3910
    if-eqz p1, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    if-ne v2, v3, :cond_1

    .line 3955
    :cond_0
    :goto_0
    return v0

    .line 3913
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    if-eqz v2, :cond_0

    .line 3917
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 3918
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    move v0, v1

    .line 3919
    goto :goto_0

    .line 3921
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->check(Landroid/view/MotionEvent;)V

    .line 3923
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_3

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    if-nez v0, :cond_3

    .line 3924
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 3927
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 3951
    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 3952
    goto :goto_0

    .line 3929
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    if-nez v0, :cond_4

    .line 3930
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    .line 3932
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_1

    .line 3936
    :pswitch_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    if-eqz v0, :cond_5

    .line 3937
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    .line 3938
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    .line 3937
    add-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 3938
    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    if-eq v0, v1, :cond_6

    .line 3939
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->hide()V

    .line 3941
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->initHandleView()V

    goto :goto_1

    .line 3944
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->hideHandleView()V

    goto :goto_1

    .line 3955
    :cond_7
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 3927
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 4771
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 4772
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4773
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->hide()V

    .line 4774
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->setDirty()V

    .line 4775
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    .line 4777
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 4261
    if-eqz p1, :cond_0

    .line 4262
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fitRotateAngle2BorderAngle()V

    .line 4263
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 4264
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 4265
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 4267
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    .line 4268
    return-void
.end method

.method protected onZoom()V
    .locals 0

    .prologue
    .line 769
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 770
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    .line 771
    return-void
.end method

.method protected relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2
    .param p1, "dstRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/RectF;
    .param p3, "coordinateInfo"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .prologue
    .line 805
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 809
    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 810
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    .line 811
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    .line 810
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 812
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 813
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    .line 814
    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    .line 813
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method protected rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 4
    .param p1, "px"    # F
    .param p2, "py"    # F
    .param p3, "angle"    # F
    .param p4, "input"    # Landroid/graphics/PointF;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2214
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    if-nez v1, :cond_0

    .line 2215
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    .line 2218
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, p3, p1, p2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 2220
    const/4 v1, 0x2

    new-array v0, v1, [F

    .line 2221
    .local v0, "pts":[F
    iget v1, p4, Landroid/graphics/PointF;->x:F

    aput v1, v0, v2

    .line 2222
    iget v1, p4, Landroid/graphics/PointF;->y:F

    aput v1, v0, v3

    .line 2224
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 2226
    new-instance v1, Landroid/graphics/PointF;

    aget v2, v0, v2

    aget v3, v0, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v1
.end method

.method public setContextMenu(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4057
    .local p1, "menuItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-object p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    .line 4058
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->setDirty()V

    .line 4059
    return-void
.end method

.method public setContextMenuItemEnabled(IZ)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 4072
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-nez v0, :cond_0

    .line 4077
    :goto_0
    return-void

    .line 4076
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->setItemEnabled(IZ)V

    goto :goto_0
.end method

.method public setContextMenuVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 4121
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    if-eq v0, p1, :cond_0

    .line 4122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-boolean p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    .line 4123
    if-eqz p1, :cond_1

    .line 4124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    .line 4129
    :cond_0
    :goto_0
    return-void

    .line 4126
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->hide()V

    goto :goto_0
.end method

.method public setDimEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 4224
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    .line 4225
    return-void
.end method

.method protected setListener(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    .prologue
    .line 4695
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    .line 4696
    return-void
.end method

.method public setMinResizeRect(Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v2, 0x0

    .line 4185
    if-nez p1, :cond_0

    .line 4186
    const/4 v0, 0x7

    const-string v1, " rect must be not null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 4189
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    .line 4190
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    .line 4191
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    .line 4192
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 4195
    :cond_1
    return-void
.end method

.method protected setObjectList(Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2092
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    .line 2094
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v0, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2095
    .local v0, "defaultMaxHeight":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v1, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2097
    .local v1, "defaultMaxWidth":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v8, :cond_0

    .line 2098
    new-instance v7, Landroid/graphics/RectF;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2099
    .local v7, "unionRectF":Landroid/graphics/RectF;
    new-instance v6, Landroid/graphics/RectF;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v6, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2101
    .local v6, "unionMaxRectF":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 2119
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v8, v7}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 2120
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v8, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 2122
    .end local v6    # "unionMaxRectF":Landroid/graphics/RectF;
    .end local v7    # "unionRectF":Landroid/graphics/RectF;
    :cond_0
    return-void

    .line 2101
    .restart local v6    # "unionMaxRectF":Landroid/graphics/RectF;
    .restart local v7    # "unionRectF":Landroid/graphics/RectF;
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 2103
    .local v5, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    new-instance v9, Landroid/graphics/RectF;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinWidth()F

    move-result v12

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinHeight()F

    move-result v13

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v7, v9}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 2105
    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxWidth()F

    move-result v4

    .line 2106
    .local v4, "maxWidth":F
    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxHeight()F

    move-result v2

    .line 2107
    .local v2, "maxHeight":F
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v9, v9, v10

    if-ltz v9, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v9

    int-to-float v10, v1

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 2108
    :cond_2
    int-to-float v9, v1

    const/high16 v10, 0x40000000    # 2.0f

    mul-float v4, v9, v10

    .line 2110
    :cond_3
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v9, v9, v10

    if-ltz v9, :cond_4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v9

    int-to-float v10, v0

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    cmpl-float v9, v9, v10

    if-lez v9, :cond_5

    .line 2111
    :cond_4
    int-to-float v9, v0

    const/high16 v10, 0x40000000    # 2.0f

    mul-float v2, v9, v10

    .line 2113
    :cond_5
    new-instance v3, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {v3, v9, v10, v4, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2114
    .local v3, "maxRect":Landroid/graphics/RectF;
    invoke-virtual {v6, v3}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 2116
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setObjectOutlineEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 4739
    sput-boolean p1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    .line 4740
    return-void
.end method

.method public setStyle(I)V
    .locals 1
    .param p1, "style"    # I

    .prologue
    .line 4025
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    .line 4026
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    .line 4030
    :goto_0
    return-void

    .line 4028
    :cond_0
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setTouchEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 4157
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    .line 4158
    return-void
.end method

.method protected showHandleViewNoTextRotation()V
    .locals 1

    .prologue
    .line 1538
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->showHandleView(Z)V

    .line 1539
    return-void
.end method

.method protected showHandleViewWithTextRotation()V
    .locals 1

    .prologue
    .line 1530
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->showHandleView(Z)V

    .line 1531
    return-void
.end method

.method protected updateContextMenu()V
    .locals 10

    .prologue
    .line 936
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    if-nez v5, :cond_1

    .line 983
    :cond_0
    :goto_0
    return-void

    .line 940
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 941
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mDirtyFlag:Z

    if-eqz v5, :cond_3

    .line 942
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v5, :cond_2

    .line 943
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->close()Z

    .line 944
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 947
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 948
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    .line 949
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    invoke-direct {v6, v7, p0, v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;-><init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;)V

    .line 948
    iput-object v6, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 953
    :cond_3
    const/4 v5, -0x1

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v3

    .line 954
    .local v3, "rectF":Landroid/graphics/RectF;
    if-eqz v3, :cond_0

    .line 957
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 958
    .local v2, "rect":Landroid/graphics/Rect;
    const/16 v4, 0x19

    .line 959
    .local v4, "top_diff":I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 960
    const/16 v4, 0x26

    .line 963
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isRotatable()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 964
    int-to-float v5, v4

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mOnePT:F

    mul-float/2addr v5, v6

    float-to-int v4, v5

    .line 967
    :cond_5
    iget v5, v3, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iget v6, v3, Landroid/graphics/RectF;->top:F

    int-to-float v7, v4

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iget v7, v3, Landroid/graphics/RectF;->right:F

    float-to-int v7, v7

    iget v8, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    .line 968
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mZoneSize:F
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;)F

    move-result v9

    float-to-int v9, v9

    add-int/2addr v8, v9

    .line 967
    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 970
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v5, :cond_0

    .line 973
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->calculateContextMenuPosition(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 974
    .local v0, "contextMenuPositionRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_6

    .line 975
    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 978
    :cond_6
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 979
    .local v1, "offsetRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 980
    iget v5, v1, Landroid/graphics/Rect;->left:I

    iget v6, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    .line 981
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->setRect(Landroid/graphics/Rect;)V

    goto/16 :goto_0
.end method

.method protected updateRectList()V
    .locals 9

    .prologue
    .line 2153
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 2154
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2156
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 2158
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v3, :cond_0

    .line 2159
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2162
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 2163
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2176
    :cond_1
    return-void

    .line 2163
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 2164
    .local v1, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 2165
    .local v0, "dstRect":Landroid/graphics/RectF;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 2167
    .local v2, "srcRect":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 2169
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->Increase2MinimumRect(Landroid/graphics/RectF;)Z

    .line 2171
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2172
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget v5, v0, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->top:F

    iget v7, v0, Landroid/graphics/RectF;->right:F

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method
