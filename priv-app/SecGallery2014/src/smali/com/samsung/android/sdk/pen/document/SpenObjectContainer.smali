.class public final Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "SpenObjectContainer.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 54
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_init2(Ljava/util/ArrayList;)Z

    move-result v0

    .line 55
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 56
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 58
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Z)V
    .locals 2
    .param p2, "isTemplateObject"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_init3(Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 74
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 75
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 77
    :cond_0
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "isTemplateObject"    # Z

    .prologue
    .line 33
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    .line 35
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_init3(Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 36
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 37
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 39
    :cond_0
    return-void
.end method

.method private native ObjectContainer_IsInvisibleChildResizingEnabled()Z
.end method

.method private native ObjectContainer_appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectContainer_appendObjectList(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectContainer_clearChangedFlag()V
.end method

.method private native ObjectContainer_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectContainer_enableUngrouping(Z)Z
.end method

.method private native ObjectContainer_getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.end method

.method private native ObjectContainer_getObjectList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation
.end method

.method private native ObjectContainer_getRect()Landroid/graphics/RectF;
.end method

.method private native ObjectContainer_init1()Z
.end method

.method private native ObjectContainer_init2(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectContainer_init3(Ljava/util/ArrayList;Z)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;Z)Z"
        }
    .end annotation
.end method

.method private native ObjectContainer_isChanged()Z
.end method

.method private native ObjectContainer_isUngroupable()Z
.end method

.method private native ObjectContainer_move(FF)Z
.end method

.method private native ObjectContainer_removeObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectContainer_removeObjectList(Ljava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation
.end method

.method private native ObjectContainer_resize(FF)Z
.end method

.method private native ObjectContainer_setInvisibleChildResizingEnabled(Z)Z
.end method

.method private native ObjectContainer_setRotation(F)Z
.end method

.method private throwUncheckedException(I)V
    .locals 3
    .param p1, "errno"    # I

    .prologue
    .line 339
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 340
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SpenObjectContainer("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 344
    return-void
.end method


# virtual methods
.method public appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 123
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 124
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 126
    :cond_0
    return-void
.end method

.method public appendObject(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_appendObjectList(Ljava/util/ArrayList;)Z

    move-result v0

    .line 144
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 145
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 147
    :cond_0
    return-void
.end method

.method public clearChangedFlag()V
    .locals 0

    .prologue
    .line 290
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_clearChangedFlag()V

    .line 291
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "source"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 324
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 325
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 326
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 328
    :cond_0
    return-void
.end method

.method public createObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 90
    packed-switch p1, :pswitch_data_0

    move-object v0, v1

    .line 105
    :goto_0
    return-object v0

    .line 92
    :pswitch_0
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :pswitch_1
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    .line 99
    .local v0, "i":Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    .end local v0    # "i":Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    .line 212
    .local v0, "object":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-nez v0, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 215
    :cond_0
    return-object v0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 227
    .local v0, "rnt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    if-nez v0, :cond_0

    .line 228
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 230
    :cond_0
    return-object v0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_getRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_isChanged()Z

    move-result v0

    return v0
.end method

.method public isInvisibleChildResizeEnabled()Z
    .locals 1

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_IsInvisibleChildResizingEnabled()Z

    move-result v0

    return v0
.end method

.method public isUngroupEnabled()Z
    .locals 1

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_isUngroupable()Z

    move-result v0

    return v0
.end method

.method public removeObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_removeObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    .line 179
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 180
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 182
    :cond_0
    return-void
.end method

.method public removeObject(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_removeObjectList(Ljava/util/ArrayList;)Z

    move-result v0

    .line 195
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 196
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 198
    :cond_0
    return-void
.end method

.method public setInvisibleChildResizeEnabled(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 271
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_setInvisibleChildResizingEnabled(Z)Z

    move-result v0

    return v0
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "regionOnly"    # Z

    .prologue
    .line 165
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    .line 166
    return-void
.end method

.method public setRotation(F)V
    .locals 2
    .param p1, "degree"    # F

    .prologue
    .line 306
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_setRotation(F)Z

    move-result v0

    .line 307
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 308
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 310
    :cond_0
    return-void
.end method

.method public setUngroupEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 244
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->ObjectContainer_enableUngrouping(Z)Z

    move-result v0

    .line 245
    .local v0, "rnt":Z
    if-nez v0, :cond_0

    .line 246
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->throwUncheckedException(I)V

    .line 248
    :cond_0
    return-void
.end method
