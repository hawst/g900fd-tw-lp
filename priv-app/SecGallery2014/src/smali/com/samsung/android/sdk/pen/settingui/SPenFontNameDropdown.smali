.class Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
.super Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;
.source "SPenFontNameDropdown.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;,
        Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field protected static isAutoScroll:Z = false

.field protected static final mDefaultPath:Ljava/lang/String; = ""

.field private static final mDropdownFocusPath:Ljava/lang/String; = "tw_spinner_list_focused_holo_light"

.field private static final mDropdownNormalPath:Ljava/lang/String; = ""

.field private static final mDropdownPressPath:Ljava/lang/String; = "tw_spinner_list_pressed_holo_light"

.field protected static mSdkVersion:I


# instance fields
.field private final context:Landroid/content/Context;

.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mHandler:Landroid/os/Handler;

.field private final mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mListView:Landroid/widget/ListView;

.field private mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;

.field private mOffsetX:I

.field private mOffsetY:I

.field private mSelectedIndex:I

.field private final root:Landroid/view/View;

.field private final runnable:Ljava/lang/Runnable;

.field private windowHeight:I

.field private windowWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->isAutoScroll:Z

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ljava/util/ArrayList;IIF)V
    .locals 8
    .param p1, "anchor"    # Landroid/view/View;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "ratio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;IIF)V"
        }
    .end annotation

    .prologue
    .line 196
    .local p2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;-><init>(Landroid/view/View;)V

    .line 34
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;

    .line 39
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I

    .line 40
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I

    .line 41
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mOffsetY:I

    .line 42
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mOffsetX:I

    .line 43
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSelectedIndex:I

    .line 251
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->runnable:Ljava/lang/Runnable;

    .line 197
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mHandler:Landroid/os/Handler;

    .line 198
    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I

    .line 199
    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I

    .line 200
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;

    .line 201
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mItemList:Ljava/util/ArrayList;

    .line 202
    new-instance v4, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;

    const-string v6, ""

    invoke-direct {v4, v5, v6, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 203
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;

    const-string v5, "accessibility"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 205
    new-instance v2, Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 206
    .local v2, "dropdownLayout":Landroid/widget/FrameLayout;
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x2

    .line 207
    const/4 v5, -0x2

    .line 206
    invoke-direct {v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 208
    .local v3, "dropdownLayoutParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 210
    new-instance v4, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    .line 214
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 219
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->root:Landroid/view/View;

    .line 220
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->root:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->setContentView(Landroid/view/View;)V

    .line 224
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;

    const v5, 0x1090003

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mItemList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;Landroid/content/Context;ILjava/util/List;)V

    .line 225
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 226
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 227
    sget v4, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    const/16 v5, 0x15

    if-ge v4, v5, :cond_3

    .line 228
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v6, "tw_list_divider_holo_light"

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 229
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 233
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v5, "tw_menu_dropdown_panel_holo_light"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 234
    .local v1, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 235
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v4

    const/16 v5, 0x640

    if-ne v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_1

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v4

    const/16 v5, 0x300

    if-ne v4, v5, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_2

    .line 237
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v4

    const/16 v5, 0x640

    if-ne v4, v5, :cond_4

    .line 238
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x42fd570a    # 126.67f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I

    .line 242
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    neg-int v4, v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mOffsetY:I

    .line 243
    const/4 v4, -0x2

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mOffsetX:I

    .line 245
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 246
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v4, p4}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 247
    return-void

    .line 231
    .end local v1    # "background":Landroid/graphics/drawable/Drawable;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 240
    .restart local v1    # "background":Landroid/graphics/drawable/Drawable;
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x43204ccd    # 160.3f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mOffsetX:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mOffsetY:I

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSelectedIndex:I

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method


# virtual methods
.method public changeOrientation(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->dismiss()V

    .line 333
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 337
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;->onSelectItem(I)V

    .line 340
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->dismiss()V

    .line 341
    return-void
.end method

.method public setOnItemSelectListner(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;)V
    .locals 0
    .param p1, "listner"    # Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;

    .line 345
    return-void
.end method

.method public show(IILjava/lang/String;)V
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "setItem"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 291
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->show()V

    .line 292
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 293
    .local v0, "index":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 294
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSelectedIndex:I

    .line 295
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 298
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->root:Landroid/view/View;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->root:Landroid/view/View;

    invoke-virtual {v2, v4, v4}, Landroid/view/View;->measure(II)V

    .line 301
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I

    .line 302
    .local v1, "mtempHeight":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v2

    if-le v1, v2, :cond_5

    .line 303
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v1

    .line 307
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 308
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 310
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v7, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 311
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_1
    sget v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_6

    .line 312
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 316
    :goto_1
    sput-boolean v5, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->isAutoScroll:Z

    .line 317
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v2

    const/16 v3, 0x640

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    .line 318
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v2

    const/16 v3, 0x300

    if-ne v2, v3, :cond_7

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    .line 319
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mOffsetX:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mOffsetY:I

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 324
    :goto_2
    sput-boolean v6, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->isAutoScroll:Z

    .line 325
    if-ne p2, v7, :cond_4

    .line 326
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->updatePosition()V

    .line 329
    :cond_4
    return-void

    .line 305
    :cond_5
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I

    goto :goto_0

    .line 314
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v6}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    goto :goto_1

    .line 321
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v2, v3, v5, p2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_2
.end method

.method public updatePosition()V
    .locals 4

    .prologue
    .line 284
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 286
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->runnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 288
    :cond_0
    return-void
.end method
