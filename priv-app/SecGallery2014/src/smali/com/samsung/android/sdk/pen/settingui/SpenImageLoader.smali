.class Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
.super Ljava/lang/Object;
.source "SpenImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;,
        Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;
    }
.end annotation


# instance fields
.field private mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field public mLoaded:Z

.field private mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

.field private final mSdkVersion:I

.field private mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V
    .locals 1
    .param p1, "drawableImg"    # Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mSdkVersion:I

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    .line 53
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 54
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    .line 55
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    .line 56
    return-void
.end method


# virtual methods
.method public addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "normalImagePath"    # Ljava/lang/String;
    .param p3, "pressPath"    # Ljava/lang/String;
    .param p4, "focusPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mRippleColorList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    return-void
.end method

.method public addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "normalImagePath"    # Ljava/lang/String;
    .param p3, "pressPath"    # Ljava/lang/String;
    .param p4, "focusPath"    # Ljava/lang/String;
    .param p5, "rippleColor"    # I

    .prologue
    const/4 v2, -0x1

    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mRippleColorList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    return-void
.end method

.method public addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "normalImagePath"    # Ljava/lang/String;
    .param p3, "pressPath"    # Ljava/lang/String;
    .param p4, "focusPath"    # Ljava/lang/String;
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mRippleColorList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "normalImagePath"    # Ljava/lang/String;
    .param p3, "pressPath"    # Ljava/lang/String;
    .param p4, "focusPath"    # Ljava/lang/String;
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "rippleColor"    # I

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mRippleColorList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    return-void
.end method

.method public addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "imagePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;II)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "imagePath"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V

    .line 67
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 76
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    .line 77
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 78
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 80
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    .line 81
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    .line 83
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 84
    return-void
.end method

.method public loadImage()V
    .locals 18
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 146
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-eqz v1, :cond_1

    .line 227
    :cond_0
    return-void

    .line 150
    :cond_1
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    .line 155
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v15, v1, :cond_2

    .line 176
    const/4 v15, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v15, v1, :cond_0

    .line 177
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/View;

    .line 178
    .local v17, "view":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 179
    .local v5, "width":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 180
    .local v6, "height":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mRippleColorList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 181
    .local v16, "rippleColor":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_7

    .line 182
    if-gez v5, :cond_6

    if-gez v6, :cond_6

    .line 183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 183
    invoke-virtual {v4, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 176
    .end local v17    # "view":Landroid/view/View;
    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1

    .line 156
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v16    # "rippleColor":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/View;

    .line 157
    .restart local v17    # "view":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 158
    .restart local v5    # "width":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 159
    .restart local v6    # "height":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_4

    .line 160
    if-gez v5, :cond_3

    if-gez v6, :cond_3

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    :goto_3
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    .line 163
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 163
    invoke-virtual {v3, v1, v4, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 167
    :cond_4
    if-gez v5, :cond_5

    if-gez v6, :cond_5

    .line 168
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 170
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 170
    invoke-virtual {v3, v1, v4, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 186
    .restart local v16    # "rippleColor":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 186
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 190
    :cond_7
    if-eqz v16, :cond_b

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mSdkVersion:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_b

    .line 191
    move-object/from16 v0, v17

    instance-of v1, v0, Landroid/widget/ImageButton;

    if-eqz v1, :cond_9

    .line 192
    if-gez v5, :cond_8

    if-gez v6, :cond_8

    .line 193
    check-cast v17, Landroid/widget/ImageButton;

    .end local v17    # "view":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 194
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v7, 0x0

    .line 193
    invoke-virtual {v3, v1, v4, v2, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 197
    .restart local v17    # "view":Landroid/view/View;
    :cond_8
    check-cast v17, Landroid/widget/ImageButton;

    .end local v17    # "view":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    .line 198
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const/4 v11, 0x0

    .line 199
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 197
    invoke-virtual/range {v7 .. v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 202
    .restart local v17    # "view":Landroid/view/View;
    :cond_9
    if-gez v5, :cond_a

    if-gez v6, :cond_a

    .line 203
    new-instance v4, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    .line 204
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v9, 0x0

    .line 205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 204
    invoke-virtual {v8, v1, v9, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    .line 205
    const/4 v2, 0x0

    invoke-direct {v4, v7, v1, v2}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 203
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 207
    :cond_a
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 208
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const/4 v9, 0x0

    .line 209
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const/4 v12, 0x0

    .line 210
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 208
    invoke-virtual/range {v7 .. v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    .line 210
    const/4 v4, 0x0

    invoke-direct {v2, v3, v1, v4}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 207
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 214
    :cond_b
    if-gez v5, :cond_c

    if-gez v6, :cond_c

    .line 215
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 215
    invoke-virtual {v4, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 219
    :cond_c
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 220
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 221
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 219
    invoke-virtual/range {v7 .. v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2
.end method
