.class public Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;
.super Ljava/lang/Object;
.source "ShapeRecognition.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VIShapeRecognition"


# instance fields
.field private final mResultPointsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

.field private mShapeTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShapeTypeList:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    .line 53
    return-void
.end method

.method private makeResults([Lcom/samsung/vip/engine/shape/GraphPrimitive;)V
    .locals 8
    .param p1, "graphPrimitives"    # [Lcom/samsung/vip/engine/shape/GraphPrimitive;

    .prologue
    .line 120
    new-instance v3, Lcom/samsung/vip/engine/VIShapeGenerator;

    invoke-direct {v3}, Lcom/samsung/vip/engine/VIShapeGenerator;-><init>()V

    .line 121
    .local v3, "shapeGenerator":Lcom/samsung/vip/engine/VIShapeGenerator;
    array-length v1, p1

    .line 122
    .local v1, "segmentCount":I
    if-lez v1, :cond_0

    .line 123
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShapeTypeList:Ljava/util/ArrayList;

    .line 125
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 127
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 138
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Lcom/samsung/vip/engine/VIShapeGenerator;->generate(Ljava/util/ArrayList;)V

    .line 139
    return-void

    .line 128
    :cond_1
    aget-object v2, p1, v0

    .line 129
    .local v2, "shape":Lcom/samsung/vip/engine/shape/GraphPrimitive;
    if-nez v2, :cond_2

    .line 127
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    iget-short v6, v2, Lcom/samsung/vip/engine/shape/GraphPrimitive;->nType:S

    invoke-virtual {v5, v6}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->getPrimitiveName(I)Ljava/lang/String;

    move-result-object v4

    .line 133
    .local v4, "typeName":Ljava/lang/String;
    const-string v5, "VIShapeRecognition"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Id: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v7, v2, Lcom/samsung/vip/engine/shape/GraphPrimitive;->nId:S

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShapeTypeList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    invoke-virtual {v3, v2}, Lcom/samsung/vip/engine/VIShapeGenerator;->addShape(Lcom/samsung/vip/engine/shape/GraphPrimitive;)V

    goto :goto_1
.end method


# virtual methods
.method public addStroke([F[F)V
    .locals 1
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->addStroke([F[F)V

    .line 74
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->close()V

    .line 109
    :cond_0
    return-void
.end method

.method public getResult()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShapeTypeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShapeTypeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public init(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    .line 61
    new-instance v0, Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    invoke-direct {v0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->init()V

    .line 65
    const/4 v0, 0x1

    return v0
.end method

.method public process()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    invoke-virtual {v1}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->clearScene()V

    .line 91
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/VIShapeRecognitionLib;

    invoke-virtual {v1}, Lcom/samsung/vip/engine/VIShapeRecognitionLib;->recog()[Lcom/samsung/vip/engine/shape/GraphPrimitive;

    move-result-object v0

    .line 92
    .local v0, "graphPrimitives":[Lcom/samsung/vip/engine/shape/GraphPrimitive;
    if-nez v0, :cond_0

    .line 93
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    .line 98
    :goto_0
    return-object v1

    .line 96
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->makeResults([Lcom/samsung/vip/engine/shape/GraphPrimitive;)V

    .line 98
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    goto :goto_0
.end method
