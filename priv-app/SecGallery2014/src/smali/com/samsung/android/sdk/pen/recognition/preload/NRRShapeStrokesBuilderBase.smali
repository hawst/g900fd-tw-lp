.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;
.super Ljava/lang/Object;
.source "NRRShapeStrokesBuilderBase.java"


# instance fields
.field private mPenSettings:Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;

.field protected mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;Lcom/samsung/recognitionengine/ShapeInfo;)V
    .locals 0
    .param p1, "penSettings"    # Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;
    .param p2, "shapeInfo"    # Lcom/samsung/recognitionengine/ShapeInfo;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->mPenSettings:Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;

    .line 22
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->mShapeInfo:Lcom/samsung/recognitionengine/ShapeInfo;

    .line 23
    return-void
.end method


# virtual methods
.method protected addPointToStroke(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;Lcom/samsung/recognitionengine/PointF;)V
    .locals 4
    .param p1, "stroke"    # Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    .param p2, "p"    # Lcom/samsung/recognitionengine/PointF;

    .prologue
    .line 33
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/high16 v1, 0x3f800000    # 1.0f

    .line 34
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    .line 33
    invoke-virtual {p1, v0, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FI)V

    .line 35
    return-void
.end method

.method protected createLineStroke(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    .locals 7
    .param p1, "start"    # Lcom/samsung/recognitionengine/PointF;
    .param p2, "end"    # Lcom/samsung/recognitionengine/PointF;

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 40
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>()V

    .line 41
    .local v0, "stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setToolType(I)V

    .line 42
    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->addPointToStroke(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;Lcom/samsung/recognitionengine/PointF;)V

    .line 43
    new-instance v1, Lcom/samsung/recognitionengine/PointF;

    invoke-virtual {p1}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v2

    invoke-virtual {p2}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v3

    add-float/2addr v2, v3

    div-float/2addr v2, v5

    invoke-virtual {p1}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v3

    invoke-virtual {p2}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v4

    add-float/2addr v3, v4

    div-float/2addr v3, v5

    invoke-direct {v1, v2, v3}, Lcom/samsung/recognitionengine/PointF;-><init>(FF)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->addPointToStroke(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;Lcom/samsung/recognitionengine/PointF;)V

    .line 44
    invoke-virtual {p0, v0, p2}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->addPointToStroke(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;Lcom/samsung/recognitionengine/PointF;)V

    .line 46
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->setStrokeStyle(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    .line 47
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setCurveEnabled(Z)V

    .line 49
    return-object v0
.end method

.method protected setStrokeStyle(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V
    .locals 1
    .param p1, "stroke"    # Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->mPenSettings:Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->getPenName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenName(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->mPenSettings:Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->getPenSize()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenSize(F)V

    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRShapeStrokesBuilderBase;->mPenSettings:Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRPenSettings;->getColor()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setColor(I)V

    .line 29
    return-void
.end method
