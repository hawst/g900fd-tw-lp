.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;
.super Ljava/lang/Object;
.source "SpenSettingSelectionLayout.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    .line 1330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;
    .locals 1

    .prologue
    .line 1330
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    return-object v0
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 1336
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1337
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 1338
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mIsRotated:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$23(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1339
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$24(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;Z)V

    .line 1367
    :cond_0
    :goto_0
    return-void

    .line 1344
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mIsRotated:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$23(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1345
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->rotatePosition()V
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)V

    .line 1346
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$24(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;Z)V

    .line 1347
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mOldMovableRect:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1354
    :cond_2
    :goto_1
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 1355
    .local v1, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1364
    .end local v1    # "handler":Landroid/os/Handler;
    :catch_0
    move-exception v0

    .line 1365
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 1349
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mNeedCalculateMargin:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1350
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->checkPosition()V
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$26(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
