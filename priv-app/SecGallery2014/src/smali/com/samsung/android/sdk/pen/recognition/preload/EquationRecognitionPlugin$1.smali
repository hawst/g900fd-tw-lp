.class Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;
.super Landroid/os/Handler;
.source "EquationRecognitionPlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    .line 149
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 152
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;

    .line 153
    .local v0, "info":Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;->mInput:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;->access$0(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;)Ljava/util/List;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;->mOutput:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;->onResult(Ljava/util/List;Ljava/util/List;)V

    .line 156
    :cond_0
    return-void
.end method
