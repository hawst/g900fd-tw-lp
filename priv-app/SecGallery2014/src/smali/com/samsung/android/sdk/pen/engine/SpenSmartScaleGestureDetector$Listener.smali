.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;
.super Ljava/lang/Object;
.source "SpenSmartScaleGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onChangePan(FF)V
.end method

.method public abstract onChangeScale(FFF)V
.end method

.method public abstract onFlick(I)Z
.end method

.method public abstract onUpdate(Z)V
.end method

.method public abstract onUpdateScreenFrameBuffer()V
.end method
