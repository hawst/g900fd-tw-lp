.class Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;
.super Landroid/view/View;
.source "SpenNotePadDrawing.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;
    }
.end annotation


# static fields
.field private static final PAD_BAR_COLOR:I = -0x699d9b95

.field private static final PAD_MOVING_RECT_COLOR:I = 0x4779caf2

.field private static final STROKE_PAD_WIDTH:I = 0x4

.field private static final TRANSPARENCY_DEGREE:I = 0x96


# instance fields
.field private PAD_HANDLE_HEIGHT:I

.field private PAD_HANDLE_WIDTH:I

.field dstRect:Landroid/graphics/RectF;

.field private isArabic:Z

.field private isEraserCursor:Z

.field private isRefreshed:Z

.field private mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mAutoMovingRect:Landroid/graphics/RectF;

.field private mBitmap:[Landroid/graphics/Bitmap;

.field private mBoxHeight:F

.field private mCanvasLayer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mCirclePoint:Landroid/graphics/PointF;

.field private mCircleRadius:F

.field private mContext:Landroid/content/Context;

.field private mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mFloatingLayer:Landroid/graphics/Bitmap;

.field private mHeight:I

.field public mIsBeatify:Z

.field public mIsMagic:Z

.field public mIsMarker:Z

.field private mIsMultiTouch:Z

.field private mIsOOV:Z

.field private mIsPenUp:Z

.field private final mOnePT:F

.field private mPadBarRect:Landroid/graphics/RectF;

.field private mPadLayer:Landroid/graphics/Bitmap;

.field private mPadLinePaint:Landroid/graphics/Paint;

.field private mPadPaint:Landroid/graphics/Paint;

.field private mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

.field private mPenName:Ljava/lang/String;

.field private mRatio:F

.field private mReferenceBitmap:Landroid/graphics/Bitmap;

.field private mRemoverRadius:F

.field private mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mScreenStartX:I

.field private mScreenStartY:I

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mStroke:Z

.field private mWidth:I

.field private preEvent:Landroid/view/MotionEvent;

.field private prePoint:Landroid/graphics/PointF;

.field srcRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;F)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "PT"    # F

    .prologue
    const/high16 v5, -0x3d380000    # -100.0f

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 91
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 37
    const/16 v0, 0x1f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    .line 38
    const/16 v0, 0x22

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_HEIGHT:I

    .line 39
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsBeatify:Z

    .line 40
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMagic:Z

    .line 41
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMarker:Z

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 53
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 55
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    .line 64
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 66
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    .line 67
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v5, v5}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    .line 68
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 69
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 70
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 71
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 72
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMultiTouch:Z

    .line 73
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    .line 74
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isArabic:Z

    .line 76
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartX:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartY:I

    .line 77
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    .line 80
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed:Z

    .line 81
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->prePoint:Landroid/graphics/PointF;

    .line 957
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    .line 958
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    .line 92
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mContext:Landroid/content/Context;

    .line 94
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    .line 95
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    const v1, -0x699d9b95

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 100
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 102
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 104
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 106
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 111
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    .line 112
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isArabic:Z

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isArabic:Z

    goto :goto_0
.end method

.method private ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dst"    # Landroid/graphics/Rect;
    .param p2, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 1048
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 1049
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 1050
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 1051
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 1052
    return-void
.end method

.method private MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "src"    # Landroid/graphics/RectF;

    .prologue
    .line 1055
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->right:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 1056
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 1055
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "dstRectF"    # Landroid/graphics/RectF;
    .param p2, "srcRectF"    # Landroid/graphics/RectF;

    .prologue
    .line 1041
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartX:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 1042
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartX:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 1043
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 1044
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 1045
    return-void
.end method

.method private createEvent(Landroid/view/MotionEvent;Landroid/graphics/PointF;I)Landroid/view/MotionEvent;
    .locals 21
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "point"    # Landroid/graphics/PointF;
    .param p3, "action"    # I

    .prologue
    .line 341
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    .line 342
    .local v2, "downTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    .line 343
    .local v4, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v10

    .line 344
    .local v10, "metaState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v11

    .line 345
    .local v11, "buttonState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v12

    .line 346
    .local v12, "xPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v13

    .line 347
    .local v13, "yPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v14

    .line 348
    .local v14, "deviceId":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v15

    .line 349
    .local v15, "edgeFlags":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v16

    .line 350
    .local v16, "source":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v17

    .line 351
    .local v17, "flags":I
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v19

    .line 353
    .local v19, "toolType":I
    const/4 v6, 0x1

    new-array v8, v6, [Landroid/view/MotionEvent$PointerProperties;

    .line 354
    .local v8, "pp":[Landroid/view/MotionEvent$PointerProperties;
    const/4 v6, 0x0

    new-instance v7, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v7, v8, v6

    .line 355
    const/4 v6, 0x0

    aget-object v6, v8, v6

    move/from16 v0, v19

    iput v0, v6, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 357
    const/4 v6, 0x1

    new-array v9, v6, [Landroid/view/MotionEvent$PointerCoords;

    .line 358
    .local v9, "pc":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v6, 0x0

    new-instance v7, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v7, v9, v6

    .line 359
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 360
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/16 v7, 0x19

    const v20, 0x3f4ccccd    # 0.8f

    move/from16 v0, v20

    invoke-virtual {v6, v7, v0}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 361
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/high16 v7, 0x3f000000    # 0.5f

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 363
    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-object v7, v9, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 365
    const/16 v18, 0x0

    .line 366
    .local v18, "newEvent":Landroid/view/MotionEvent;
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->prePoint:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 367
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->prePoint:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 368
    const/4 v6, 0x1

    move/from16 v0, v19

    if-eq v0, v6, :cond_1

    .line 369
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v7}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 371
    :cond_1
    const/4 v7, 0x1

    move/from16 v6, p3

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v18

    .line 373
    return-object v18
.end method

.method private getFirst(Landroid/view/MotionEvent;Landroid/graphics/RectF;)Ljava/util/List;
    .locals 28
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "bitmapRect"    # Landroid/graphics/RectF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MotionEvent;",
            "Landroid/graphics/RectF;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/MotionEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 452
    .local v20, "eventList":Ljava/util/List;
    const/16 v22, 0x0

    .line 453
    .local v22, "firstEv":Landroid/view/MotionEvent;
    const/16 v23, 0x0

    .line 454
    .local v23, "firstEvent":Landroid/view/MotionEvent;
    const/16 v19, 0x0

    .line 455
    .local v19, "ev":Landroid/view/MotionEvent;
    const/16 v25, 0x0

    .line 458
    .local v25, "tempEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    .line 459
    .local v2, "downTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    .line 460
    .local v4, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v10

    .line 461
    .local v10, "metaState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v11

    .line 462
    .local v11, "buttonState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v12

    .line 463
    .local v12, "xPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v13

    .line 464
    .local v13, "yPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v14

    .line 465
    .local v14, "deviceId":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v15

    .line 466
    .local v15, "edgeFlags":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v16

    .line 467
    .local v16, "source":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v17

    .line 468
    .local v17, "flags":I
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v26

    .line 470
    .local v26, "toolType":I
    const/4 v6, 0x1

    new-array v8, v6, [Landroid/view/MotionEvent$PointerProperties;

    .line 471
    .local v8, "pp":[Landroid/view/MotionEvent$PointerProperties;
    const/4 v6, 0x0

    new-instance v7, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v7, v8, v6

    .line 472
    const/4 v6, 0x0

    aget-object v6, v8, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 474
    const/4 v6, 0x1

    new-array v9, v6, [Landroid/view/MotionEvent$PointerCoords;

    .line 475
    .local v9, "pc":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v6, 0x0

    new-instance v7, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v7, v9, v6

    .line 476
    const/4 v6, 0x1

    move/from16 v0, v26

    if-ne v0, v6, :cond_0

    .line 477
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/16 v7, 0x19

    const v27, 0x3f4ccccd    # 0.8f

    move/from16 v0, v27

    invoke-virtual {v6, v7, v0}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 478
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/high16 v7, 0x3f000000    # 0.5f

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 480
    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-object v7, v9, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 482
    const/16 v21, -0x1

    .line 483
    .local v21, "first":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    add-int/lit8 v18, v6, -0x1

    .local v18, "a":I
    :goto_0
    if-gez v18, :cond_4

    .line 491
    :goto_1
    const/4 v6, -0x1

    move/from16 v0, v21

    if-eq v0, v6, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    move/from16 v0, v21

    if-ge v0, v6, :cond_8

    .line 492
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 493
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 494
    const/4 v6, 0x1

    move/from16 v0, v26

    if-eq v0, v6, :cond_1

    .line 495
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 497
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v25

    .line 500
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 501
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 503
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v19

    .line 506
    move/from16 v24, v21

    .local v24, "i":I
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    move/from16 v0, v24

    if-lt v0, v6, :cond_6

    .line 518
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 519
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 520
    const/4 v6, 0x1

    move/from16 v0, v26

    if-eq v0, v6, :cond_2

    .line 521
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 524
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    move-object/from16 v0, v25

    invoke-virtual {v0, v6, v7, v9, v10}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 525
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 526
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 528
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v7, v9, v10}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 529
    move-object/from16 p1, v25

    .line 549
    .end local v24    # "i":I
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    if-nez v6, :cond_a

    .line 550
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 551
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 552
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 558
    :goto_4
    const/4 v6, 0x1

    move/from16 v0, v26

    if-ne v0, v6, :cond_3

    .line 559
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/16 v7, 0x19

    const v27, 0x3f4ccccd    # 0.8f

    move/from16 v0, v27

    invoke-virtual {v6, v7, v0}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 560
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/high16 v7, 0x3f000000    # 0.5f

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 562
    :cond_3
    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v23

    .line 564
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 565
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 567
    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v22

    .line 570
    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 571
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 572
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 573
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 575
    return-object v20

    .line 484
    :cond_4
    move/from16 v21, v18

    .line 485
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-nez v6, :cond_5

    .line 486
    add-int/lit8 v21, v18, 0x1

    .line 487
    goto/16 :goto_1

    .line 483
    :cond_5
    add-int/lit8 v18, v18, -0x1

    goto/16 :goto_0

    .line 507
    .restart local v24    # "i":I
    :cond_6
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 508
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 509
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_7

    .line 510
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 512
    :cond_7
    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    move-object/from16 v0, v25

    invoke-virtual {v0, v6, v7, v9, v10}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 514
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 515
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 516
    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v7, v9, v10}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 506
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_2

    .line 532
    .end local v24    # "i":I
    :cond_8
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 533
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 534
    const/4 v6, 0x1

    move/from16 v0, v26

    if-eq v0, v6, :cond_9

    .line 535
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 537
    :cond_9
    const/16 p1, 0x0

    .line 538
    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object p1

    .line 541
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 542
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v27, v0

    mul-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v7, v7, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v27, v0

    div-float v7, v7, v27

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 544
    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v19

    goto/16 :goto_3

    .line 554
    :cond_a
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 555
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 556
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    goto/16 :goto_4
.end method

.method private getLast(Landroid/view/MotionEvent;Landroid/graphics/RectF;)Ljava/util/List;
    .locals 43
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "bitmapRect"    # Landroid/graphics/RectF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MotionEvent;",
            "Landroid/graphics/RectF;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/MotionEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 579
    new-instance v36, Ljava/util/ArrayList;

    invoke-direct/range {v36 .. v36}, Ljava/util/ArrayList;-><init>()V

    .line 580
    .local v36, "eventList":Ljava/util/List;
    const/16 v39, 0x0

    .line 581
    .local v39, "lastEv":Landroid/view/MotionEvent;
    const/16 v40, 0x0

    .line 582
    .local v40, "lastEvent":Landroid/view/MotionEvent;
    const/16 v35, 0x0

    .line 583
    .local v35, "ev":Landroid/view/MotionEvent;
    const/16 v41, 0x0

    .line 586
    .local v41, "tempEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    .line 587
    .local v2, "downTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    .line 588
    .local v4, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v10

    .line 589
    .local v10, "metaState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v11

    .line 590
    .local v11, "buttonState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v12

    .line 591
    .local v12, "xPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v13

    .line 592
    .local v13, "yPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v14

    .line 593
    .local v14, "deviceId":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v15

    .line 594
    .local v15, "edgeFlags":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v16

    .line 595
    .local v16, "source":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v17

    .line 596
    .local v17, "flags":I
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v42

    .line 598
    .local v42, "toolType":I
    const/4 v6, 0x1

    new-array v8, v6, [Landroid/view/MotionEvent$PointerProperties;

    .line 599
    .local v8, "pp":[Landroid/view/MotionEvent$PointerProperties;
    const/4 v6, 0x0

    new-instance v7, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v7, v8, v6

    .line 600
    const/4 v6, 0x0

    aget-object v6, v8, v6

    move/from16 v0, v42

    iput v0, v6, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 602
    const/4 v6, 0x1

    new-array v9, v6, [Landroid/view/MotionEvent$PointerCoords;

    .line 603
    .local v9, "pc":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v6, 0x0

    new-instance v7, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v7, v9, v6

    .line 604
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 605
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/16 v7, 0x19

    const v18, 0x3f4ccccd    # 0.8f

    move/from16 v0, v18

    invoke-virtual {v6, v7, v0}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 606
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/high16 v7, 0x3f000000    # 0.5f

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 608
    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-object v7, v9, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 610
    const/16 v38, -0x1

    .line 611
    .local v38, "last":I
    const/16 v34, 0x0

    .local v34, "a":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    move/from16 v0, v34

    if-lt v0, v6, :cond_3

    .line 619
    :goto_1
    if-ltz v38, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    if-lez v6, :cond_7

    .line 620
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 621
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 622
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 623
    const/4 v6, 0x1

    move/from16 v0, v42

    if-eq v0, v6, :cond_1

    .line 624
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 626
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v41

    .line 629
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v18, v0

    mul-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v18, v0

    div-float v7, v7, v18

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 630
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v18, v0

    mul-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v18, v0

    div-float v7, v7, v18

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 632
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v35

    .line 635
    const/16 v37, 0x1

    .local v37, "i":I
    :goto_2
    move/from16 v0, v37

    move/from16 v1, v38

    if-le v0, v1, :cond_5

    .line 647
    move-object/from16 p1, v41

    .line 649
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 650
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 651
    const/4 v6, 0x1

    move/from16 v0, v42

    if-eq v0, v6, :cond_2

    .line 652
    const/4 v6, 0x0

    aget-object v6, v9, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 654
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    const-wide/16 v18, 0x1

    add-long v20, v6, v18

    const/16 v22, 0x1

    const/16 v23, 0x1

    move-wide/from16 v18, v2

    move-object/from16 v24, v8

    move-object/from16 v25, v9

    move/from16 v26, v10

    move/from16 v27, v11

    move/from16 v28, v12

    move/from16 v29, v13

    move/from16 v30, v14

    move/from16 v31, v15

    move/from16 v32, v16

    move/from16 v33, v17

    invoke-static/range {v18 .. v33}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v40

    .line 657
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v18, v0

    mul-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v18, v0

    div-float v7, v7, v18

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 658
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v18, v0

    mul-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v18, v0

    div-float v7, v7, v18

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 659
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    const-wide/16 v18, 0x1

    add-long v20, v6, v18

    const/16 v22, 0x1

    const/16 v23, 0x1

    move-wide/from16 v18, v2

    move-object/from16 v24, v8

    move-object/from16 v25, v9

    move/from16 v26, v10

    move/from16 v27, v11

    move/from16 v28, v12

    move/from16 v29, v13

    move/from16 v30, v14

    move/from16 v31, v15

    move/from16 v32, v16

    move/from16 v33, v17

    invoke-static/range {v18 .. v33}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v39

    .line 678
    .end local v37    # "i":I
    :goto_3
    move-object/from16 v0, v36

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 679
    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 681
    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 682
    return-object v36

    .line 612
    :cond_3
    move/from16 v38, v34

    .line 613
    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v6

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-nez v6, :cond_4

    .line 614
    add-int/lit8 v38, v34, -0x1

    .line 615
    goto/16 :goto_1

    .line 611
    :cond_4
    add-int/lit8 v34, v34, 0x1

    goto/16 :goto_0

    .line 636
    .restart local v37    # "i":I
    :cond_5
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 637
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 638
    const/4 v6, 0x1

    move/from16 v0, v42

    if-eq v0, v6, :cond_6

    .line 639
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 641
    :cond_6
    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    move-object/from16 v0, v41

    invoke-virtual {v0, v6, v7, v9, v10}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 643
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v18, v0

    mul-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v18, v0

    div-float v7, v7, v18

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 644
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v18, v0

    mul-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v18, v0

    div-float v7, v7, v18

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 645
    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v6, v7, v9, v10}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 635
    add-int/lit8 v37, v37, 0x1

    goto/16 :goto_2

    .line 663
    .end local v37    # "i":I
    :cond_7
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->prePoint:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 664
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->prePoint:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 665
    const/4 v6, 0x1

    move/from16 v0, v42

    if-eq v0, v6, :cond_8

    .line 666
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v7}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 668
    :cond_8
    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object p1

    .line 671
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v18, v0

    mul-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v18, v0

    div-float v7, v7, v18

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 672
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/4 v7, 0x0

    aget-object v7, v9, v7

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v18, v0

    mul-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v7, v7, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v18, v0

    div-float v7, v7, v18

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 674
    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v35

    goto/16 :goto_3
.end method

.method private relativeCoordinate(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 24
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 377
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v8, v9, 0xff

    .line 378
    .local v8, "action":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    .line 379
    .local v4, "downTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 380
    .local v6, "eventTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v12

    .line 381
    .local v12, "metaState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v13

    .line 382
    .local v13, "buttonState":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v14

    .line 383
    .local v14, "xPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v15

    .line 384
    .local v15, "yPrecision":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v16

    .line 385
    .local v16, "deviceId":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v17

    .line 386
    .local v17, "edgeFlags":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v18

    .line 387
    .local v18, "source":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v19

    .line 389
    .local v19, "flags":I
    const/4 v9, 0x1

    new-array v10, v9, [Landroid/view/MotionEvent$PointerProperties;

    .line 390
    .local v10, "pp":[Landroid/view/MotionEvent$PointerProperties;
    const/4 v9, 0x0

    new-instance v22, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct/range {v22 .. v22}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v22, v10, v9

    .line 391
    const/4 v9, 0x0

    aget-object v9, v10, v9

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v22

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 393
    const/4 v9, 0x1

    new-array v11, v9, [Landroid/view/MotionEvent$PointerCoords;

    .line 394
    .local v11, "pc":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v9, 0x0

    new-instance v22, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v22 .. v22}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v22, v11, v9

    .line 395
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v9, v0, :cond_0

    .line 396
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x19

    const v23, 0x3f4ccccd    # 0.8f

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 397
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v22, 0x3f000000    # 0.5f

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 399
    :cond_0
    const/4 v9, 0x0

    const/16 v22, 0x0

    aget-object v22, v11, v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v9, v1}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 401
    const/16 v21, 0x0

    .line 402
    .local v21, "ev":Landroid/view/MotionEvent;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    if-lez v9, :cond_5

    .line 403
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 404
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 405
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v22

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 406
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v9, v0, :cond_1

    .line 407
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x19

    const v23, 0x3f4ccccd    # 0.8f

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 408
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v22, 0x3f000000    # 0.5f

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 411
    :cond_1
    const/4 v9, 0x1

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v21

    .line 414
    const/16 v20, 0x1

    .local v20, "a":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    move/from16 v0, v20

    if-lt v0, v9, :cond_3

    .line 425
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 426
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 427
    const/4 v9, 0x0

    aget-object v9, v11, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v22

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 428
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v9, v0, :cond_2

    .line 429
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x19

    const v23, 0x3f4ccccd    # 0.8f

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 430
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v22, 0x3f000000    # 0.5f

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 433
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v22

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 446
    .end local v20    # "a":I
    :goto_1
    return-object v21

    .line 415
    .restart local v20    # "a":I
    :cond_3
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v22

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 416
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 417
    const/4 v9, 0x0

    aget-object v9, v11, v9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 418
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v9, v0, :cond_4

    .line 419
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x19

    const v23, 0x3f4ccccd    # 0.8f

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 420
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v22, 0x3f000000    # 0.5f

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 423
    :cond_4
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v22

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2, v11, v12}, Landroid/view/MotionEvent;->addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V

    .line 414
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .line 435
    .end local v20    # "a":I
    :cond_5
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 436
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    move/from16 v23, v0

    div-float v22, v22, v23

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 437
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v22

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 438
    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v9

    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v9, v0, :cond_6

    .line 439
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/16 v22, 0x19

    const v23, 0x3f4ccccd    # 0.8f

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    .line 440
    const/4 v9, 0x0

    aget-object v9, v11, v9

    const/high16 v22, 0x3f000000    # 0.5f

    move/from16 v0, v22

    iput v0, v9, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 443
    :cond_6
    const/4 v9, 0x1

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v21

    goto/16 :goto_1
.end method


# virtual methods
.method public checkPen(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 829
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-nez v0, :cond_1

    .line 842
    :cond_0
    :goto_0
    return-void

    .line 832
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 833
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsBeatify:Z

    .line 834
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 835
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 836
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMagic:Z

    .line 837
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 838
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 839
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMarker:Z

    .line 840
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 121
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    .line 122
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 126
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v0, :cond_2

    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    .line 131
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->close()V

    .line 134
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 138
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 140
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 141
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 142
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    .line 144
    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    .line 145
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 146
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    .line 147
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    .line 148
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    .line 149
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    .line 150
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    .line 151
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    .line 152
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 153
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 155
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 156
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    .line 157
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mContext:Landroid/content/Context;

    .line 158
    return-void
.end method

.method public createBitmap(II)V
    .locals 5
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v4, 0x0

    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 164
    :cond_0
    if-lez p1, :cond_1

    if-gtz p2, :cond_2

    .line 187
    :cond_1
    :goto_0
    return-void

    .line 167
    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 168
    int-to-float v0, p1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-float v1, p2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 169
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 168
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    .line 174
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 175
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 177
    :cond_3
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v0, :cond_4

    .line 179
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 181
    :cond_4
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    .line 182
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    .line 184
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 185
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    goto :goto_0

    .line 171
    :cond_5
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method public getBeautifyPen()Z
    .locals 1

    .prologue
    .line 845
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsBeatify:Z

    return v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getMagicPen()Z
    .locals 1

    .prologue
    .line 853
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMagic:Z

    return v0
.end method

.method public getMarkerPen()Z
    .locals 1

    .prologue
    .line 861
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMarker:Z

    return v0
.end method

.method public isRefreshed()Z
    .locals 1

    .prologue
    .line 328
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/high16 v7, 0x40800000    # 4.0f

    .line 964
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 965
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v8, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v1, v8, v8, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 966
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v2, v7

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v7

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 967
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v7

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 966
    invoke-virtual {v1, v2, v8, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 968
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v2, v7

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    .line 969
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v7

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    .line 968
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 970
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 973
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 974
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v8, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v1, v8, v8, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 975
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    mul-float v0, v1, v2

    .line 976
    .local v0, "rate":F
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v2, v7

    div-float/2addr v2, v0

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    .line 977
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v7

    sub-float/2addr v3, v4

    div-float/2addr v3, v0

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v7

    sub-float/2addr v4, v5

    div-float/2addr v4, v0

    float-to-int v4, v4

    .line 976
    invoke-virtual {v1, v2, v8, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 978
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v2, v7

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    .line 979
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v7

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    .line 978
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 980
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 982
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    cmpl-float v1, v1, v11

    if-lez v1, :cond_1

    .line 983
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 985
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    cmpl-float v1, v1, v11

    if-lez v1, :cond_2

    .line 986
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 990
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    const v2, -0x699d9b95

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 991
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    invoke-virtual {v1, v8, v8, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 995
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    const/16 v2, 0x96

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 996
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v3, v7

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v7

    add-float/2addr v3, v4

    .line 997
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v7

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    .line 996
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 998
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v8

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 999
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1002
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    const v2, 0x4779caf2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1003
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isArabic:Z

    if-eqz v1, :cond_4

    .line 1004
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v3, v7

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    .line 1005
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    .line 1004
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1011
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1015
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v3, v9

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    int-to-float v4, v4

    .line 1016
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v9

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v9

    sub-float/2addr v5, v6

    .line 1015
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1017
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1020
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v10

    if-eqz v1, :cond_3

    .line 1021
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v10

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v8, v8, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1022
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isArabic:Z

    if-eqz v1, :cond_5

    .line 1023
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    int-to-float v3, v3

    div-float/2addr v3, v9

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 1024
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_HEIGHT:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v9

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    .line 1025
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    int-to-float v5, v5

    div-float/2addr v5, v9

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    .line 1026
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v9

    sub-float/2addr v5, v6

    .line 1023
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1033
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v10

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->srcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1037
    .end local v0    # "rate":F
    :cond_3
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1038
    return-void

    .line 1007
    .restart local v0    # "rate":F
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    .line 1008
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v7

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    .line 1007
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_0

    .line 1028
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->dstRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    int-to-float v3, v3

    div-float/2addr v3, v9

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 1029
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_HEIGHT:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v4, v9

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    .line 1030
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    int-to-float v5, v5

    div-float/2addr v5, v9

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    .line 1031
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mOnePT:F

    mul-float/2addr v6, v9

    sub-float/2addr v5, v6

    .line 1028
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1
.end method

.method onTouchEraser(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, -0x3d380000    # -100.0f

    .line 869
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v0, v4, 0xff

    .line 870
    .local v0, "action":I
    const/4 v4, 0x5

    if-eq v0, v4, :cond_0

    const/4 v4, 0x6

    if-ne v0, v4, :cond_1

    .line 871
    :cond_0
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMultiTouch:Z

    .line 873
    :cond_1
    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-ne v4, v8, :cond_2

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMultiTouch:Z

    if-eqz v4, :cond_2

    .line 874
    const/4 v0, 0x0

    .line 875
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMultiTouch:Z

    .line 877
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 878
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 879
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 880
    .local v3, "y":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    neg-float v4, v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 882
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    div-float v1, v4, v5

    .line 883
    .local v1, "rate":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v4, :cond_3

    .line 884
    new-instance v4, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 886
    :cond_3
    if-nez v0, :cond_5

    .line 887
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 888
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    mul-float/2addr v4, v1

    div-float/2addr v4, v7

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 889
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 890
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v3, v4, Landroid/graphics/PointF;->y:F

    .line 891
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    mul-float v5, v7, v1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 908
    :cond_4
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    .line 909
    return v8

    .line 892
    :cond_5
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    if-eqz v4, :cond_4

    .line 893
    const/4 v4, 0x2

    if-ne v0, v4, :cond_6

    .line 894
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    mul-float/2addr v4, v1

    div-float/2addr v4, v7

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 895
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 896
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v3, v4, Landroid/graphics/PointF;->y:F

    .line 897
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    mul-float v5, v7, v1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0

    .line 899
    :cond_6
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 900
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 901
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCircleRadius:F

    .line 902
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 903
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 904
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v6, v4, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method onTouchInput(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 686
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v7, :cond_0

    .line 687
    const/4 v7, 0x0

    .line 825
    :goto_0
    return v7

    .line 689
    :cond_0
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 690
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    and-int/lit16 v1, v7, 0xff

    .line 691
    .local v1, "action":I
    const/4 v3, 0x0

    .line 692
    .local v3, "ev":Landroid/view/MotionEvent;
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 693
    .local v6, "tmpRect":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v10, v10

    invoke-direct {v2, v7, v8, v9, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 694
    .local v2, "bitmapRect":Landroid/graphics/RectF;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    invoke-virtual {v2, v7, v8}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 696
    packed-switch v1, :pswitch_data_0

    .line 786
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 816
    :cond_2
    :goto_2
    const/4 v7, 0x3

    if-ne v1, v7, :cond_4

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    if-nez v7, :cond_4

    .line 817
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed:Z

    .line 818
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v7, :cond_3

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v7, :cond_3

    .line 819
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    invoke-interface {v7, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 821
    :cond_3
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    .line 822
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 823
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 825
    :cond_4
    const/4 v7, 0x1

    goto :goto_0

    .line 698
    :pswitch_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    const-string v8, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_5

    .line 699
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    .line 701
    :cond_5
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    .line 702
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    .line 704
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v7, :cond_6

    .line 705
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    invoke-interface {v7, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 707
    :cond_6
    new-instance v7, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    invoke-direct {v7, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->prePoint:Landroid/graphics/PointF;

    .line 708
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->relativeCoordinate(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 709
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v7, v3, v6}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 713
    :pswitch_1
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    if-eqz v7, :cond_7

    .line 714
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    const/4 v8, 0x1

    if-gt v7, v8, :cond_1

    .line 717
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    .line 718
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 722
    :cond_7
    const/4 v5, 0x0

    .line 723
    .local v5, "isOut":Z
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    if-nez v7, :cond_b

    .line 727
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    .line 729
    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->getFirst(Landroid/view/MotionEvent;Landroid/graphics/RectF;)Ljava/util/List;

    move-result-object v4

    .line 730
    .local v4, "eventList":Ljava/util/List;, "Ljava/util/List<Landroid/view/MotionEvent;>;"
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v7, :cond_8

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v7, :cond_8

    .line 731
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    const/4 v7, 0x0

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/MotionEvent;

    invoke-interface {v8, v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 733
    :cond_8
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v7, 0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/MotionEvent;

    invoke-virtual {v8, v7, v6}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 734
    invoke-virtual {v6}, Landroid/graphics/RectF;->setEmpty()V

    .line 735
    const/4 v7, 0x2

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "event":Landroid/view/MotionEvent;
    check-cast p1, Landroid/view/MotionEvent;

    .line 736
    .restart local p1    # "event":Landroid/view/MotionEvent;
    const/4 v7, 0x3

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "ev":Landroid/view/MotionEvent;
    check-cast v3, Landroid/view/MotionEvent;

    .line 751
    .end local v4    # "eventList":Ljava/util/List;, "Ljava/util/List<Landroid/view/MotionEvent;>;"
    .restart local v3    # "ev":Landroid/view/MotionEvent;
    :goto_3
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v7, :cond_9

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v7, :cond_9

    .line 752
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    invoke-interface {v7, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 754
    :cond_9
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v7, :cond_a

    .line 755
    new-instance v7, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    invoke-direct {v7, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->prePoint:Landroid/graphics/PointF;

    .line 756
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v7, v3, v6}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 757
    if-eqz v5, :cond_a

    .line 758
    const/4 p1, 0x0

    .line 761
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    goto/16 :goto_1

    .line 739
    :cond_b
    const/4 v0, 0x0

    .local v0, "a":I
    :goto_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v7

    if-lt v0, v7, :cond_d

    .line 745
    :goto_5
    if-eqz v5, :cond_c

    .line 746
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->prePoint:Landroid/graphics/PointF;

    const/4 v8, 0x1

    invoke-direct {p0, p1, v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->createEvent(Landroid/view/MotionEvent;Landroid/graphics/PointF;I)Landroid/view/MotionEvent;

    move-result-object p1

    .line 748
    :cond_c
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->relativeCoordinate(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    goto :goto_3

    .line 740
    :cond_d
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v7

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v8

    invoke-virtual {v2, v7, v8}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v7

    if-nez v7, :cond_e

    .line 741
    const/4 v5, 0x1

    .line 742
    goto :goto_5

    .line 739
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 766
    .end local v0    # "a":I
    .end local v5    # "isOut":Z
    :pswitch_2
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 767
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed:Z

    .line 768
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    if-nez v7, :cond_f

    .line 770
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 772
    :cond_f
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->relativeCoordinate(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 774
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v7, :cond_10

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v7, :cond_10

    .line 775
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    invoke-interface {v7, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 777
    :cond_10
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v7, :cond_11

    .line 778
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    .line 780
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v7, v3, v6}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 781
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->checkPen(Landroid/view/MotionEvent;)V

    .line 783
    :cond_11
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    goto/16 :goto_1

    .line 788
    :cond_12
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    invoke-virtual {v7}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_2

    .line 789
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsOOV:Z

    .line 792
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsPenUp:Z

    .line 793
    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->getLast(Landroid/view/MotionEvent;Landroid/graphics/RectF;)Ljava/util/List;

    move-result-object v4

    .line 794
    .restart local v4    # "eventList":Ljava/util/List;, "Ljava/util/List<Landroid/view/MotionEvent;>;"
    const/4 v7, 0x0

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "event":Landroid/view/MotionEvent;
    check-cast p1, Landroid/view/MotionEvent;

    .line 795
    .restart local p1    # "event":Landroid/view/MotionEvent;
    const/4 v7, 0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "ev":Landroid/view/MotionEvent;
    check-cast v3, Landroid/view/MotionEvent;

    .line 796
    .restart local v3    # "ev":Landroid/view/MotionEvent;
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed:Z

    .line 797
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    if-eqz v7, :cond_13

    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v7, :cond_13

    .line 798
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    invoke-interface {v7, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 799
    const/4 v7, 0x2

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_13

    .line 800
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    const/4 v7, 0x2

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/MotionEvent;

    invoke-interface {v8, v7}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;->onUpdate(Landroid/view/MotionEvent;)V

    .line 803
    :cond_13
    iget-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v7, :cond_15

    .line 804
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v7, v3, v6}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 805
    invoke-virtual {v6}, Landroid/graphics/RectF;->setEmpty()V

    .line 806
    const/4 v7, 0x3

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_14

    .line 807
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v7, 0x3

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/MotionEvent;

    invoke-virtual {v8, v7, v6}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 809
    :cond_14
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->checkPen(Landroid/view/MotionEvent;)V

    .line 810
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    .line 812
    :cond_15
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    .line 813
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    goto/16 :goto_2

    .line 696
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method onTouchRemover(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v9, 0x41a00000    # 20.0f

    const/4 v5, 0x0

    const/high16 v8, -0x3d380000    # -100.0f

    const/4 v7, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    .line 913
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v0, v4, 0xff

    .line 914
    .local v0, "action":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 915
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 916
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 917
    .local v3, "y":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    neg-float v4, v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 919
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    div-float v1, v4, v5

    .line 920
    .local v1, "rate":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-nez v4, :cond_0

    .line 921
    new-instance v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 923
    :cond_0
    if-nez v0, :cond_4

    .line 924
    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 925
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v4, :cond_3

    .line 926
    mul-float v4, v9, v1

    div-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 930
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 931
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v3, v4, Landroid/graphics/PointF;->y:F

    .line 932
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    mul-float v5, v6, v1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 953
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    .line 954
    return v7

    .line 927
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v4, v7, :cond_1

    .line 928
    const/high16 v4, 0x42200000    # 40.0f

    mul-float/2addr v4, v1

    div-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    goto :goto_0

    .line 933
    :cond_4
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    if-eqz v4, :cond_2

    .line 934
    const/4 v4, 0x2

    if-eq v0, v4, :cond_5

    const/4 v4, 0x5

    if-eq v0, v4, :cond_5

    .line 935
    const/4 v4, 0x6

    if-ne v0, v4, :cond_8

    .line 936
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v4, :cond_7

    .line 937
    mul-float v4, v9, v1

    div-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 941
    :cond_6
    :goto_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 942
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v3, v4, Landroid/graphics/PointF;->y:F

    .line 943
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePaint:Landroid/graphics/Paint;

    mul-float v5, v6, v1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_1

    .line 938
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v4, v7, :cond_6

    .line 939
    const/high16 v4, 0x42200000    # 40.0f

    mul-float/2addr v4, v1

    div-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    goto :goto_2

    .line 945
    :cond_8
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->preEvent:Landroid/view/MotionEvent;

    .line 946
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isEraserCursor:Z

    .line 947
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverRadius:F

    .line 948
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v8, v4, Landroid/graphics/PointF;->x:F

    .line 949
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCirclePoint:Landroid/graphics/PointF;

    iput v8, v4, Landroid/graphics/PointF;->y:F

    goto :goto_1
.end method

.method public refresh()V
    .locals 2

    .prologue
    .line 332
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isRefreshed:Z

    .line 333
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 334
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    .prologue
    .line 337
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing$ActionListener;

    .line 338
    return-void
.end method

.method public setArabic(Z)V
    .locals 0
    .param p1, "arabic"    # Z

    .prologue
    .line 1061
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->isArabic:Z

    .line 1062
    return-void
.end method

.method public setBeautifyPen(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 849
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsBeatify:Z

    .line 850
    return-void
.end method

.method public setBitmap([Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # [Landroid/graphics/Bitmap;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBitmap:[Landroid/graphics/Bitmap;

    .line 191
    return-void
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    .line 268
    return-void
.end method

.method setHandleSize(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 209
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_WIDTH:I

    .line 210
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->PAD_HANDLE_HEIGHT:I

    .line 211
    return-void
.end method

.method public setHeightAndRate(FF)V
    .locals 0
    .param p1, "height"    # F
    .param p2, "rate"    # F

    .prologue
    .line 223
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mBoxHeight:F

    .line 224
    return-void
.end method

.method public setLayer(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "layer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    .line 195
    return-void
.end method

.method public setMagicPen(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 857
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMagic:Z

    .line 858
    return-void
.end method

.method public setMarkerPen(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 865
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mIsMarker:Z

    .line 866
    return-void
.end method

.method public setPadLayer(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 289
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    .line 290
    return-void
.end method

.method public setPanAndZoom(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 198
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaX:F

    .line 199
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mDeltaY:F

    .line 200
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRatio:F

    .line 201
    return-void
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 227
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-nez v1, :cond_0

    .line 228
    new-instance v1, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    .line 232
    :cond_0
    :try_start_0
    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    .line 233
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v1, :cond_1

    .line 234
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    .line 235
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    if-eqz v1, :cond_1

    .line 236
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mStroke:Z

    .line 237
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 238
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    .line 241
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 251
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 252
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    .line 253
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    .line 254
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 256
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-boolean v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setCurveEnabled(Z)V

    .line 258
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 259
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setAdvancedSetting(Ljava/lang/String;)V

    .line 261
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPenName:Ljava/lang/String;

    const-string v2, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 262
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    .line 264
    :cond_4
    return-void

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 244
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 245
    .local v0, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 246
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 248
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 249
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setRect(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadBarRect:Landroid/graphics/RectF;

    .line 215
    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "mRef"    # Landroid/graphics/Bitmap;

    .prologue
    .line 276
    if-eqz p1, :cond_1

    .line 277
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 280
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mReferenceBitmap:Landroid/graphics/Bitmap;

    .line 282
    :cond_1
    return-void
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 272
    return-void
.end method

.method public setScreenStart(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 204
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartX:I

    .line 205
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mScreenStartY:I

    .line 206
    return-void
.end method

.method public update()V
    .locals 0

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->postInvalidate()V

    .line 294
    return-void
.end method

.method public updateFrameBuffer(ZLandroid/graphics/RectF;)V
    .locals 10
    .param p1, "isUpdate"    # Z
    .param p2, "boxRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 297
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 298
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mPadLayer:Landroid/graphics/Bitmap;

    invoke-direct {v3, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 299
    .local v3, "canvas":Landroid/graphics/Canvas;
    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v8, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 301
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 302
    .local v1, "abRectF":Landroid/graphics/RectF;
    invoke-direct {p0, v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 303
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 304
    .local v0, "abRect":Landroid/graphics/Rect;
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    .line 305
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 306
    .local v4, "dstRect":Landroid/graphics/RectF;
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mHeight:I

    int-to-float v7, v7

    invoke-virtual {v4, v9, v9, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 309
    const/4 v5, 0x0

    .local v5, "layer":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v5, v6, :cond_1

    .line 321
    if-eqz p1, :cond_0

    .line 322
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->postInvalidate()V

    .line 325
    .end local v0    # "abRect":Landroid/graphics/Rect;
    .end local v1    # "abRectF":Landroid/graphics/RectF;
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "dstRect":Landroid/graphics/RectF;
    .end local v5    # "layer":I
    :cond_0
    return-void

    .line 310
    .restart local v0    # "abRect":Landroid/graphics/Rect;
    .restart local v1    # "abRectF":Landroid/graphics/RectF;
    .restart local v3    # "canvas":Landroid/graphics/Canvas;
    .restart local v4    # "dstRect":Landroid/graphics/RectF;
    .restart local v5    # "layer":I
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 311
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_2

    .line 312
    invoke-virtual {v2, v8, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-virtual {v2, v8, v8, v6}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 313
    if-nez v5, :cond_3

    .line 314
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2, v0, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 309
    :cond_2
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 316
    :cond_3
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2, v0, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public updateRect(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->mAutoMovingRect:Landroid/graphics/RectF;

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenNotePadDrawing;->invalidate()V

    .line 220
    return-void
.end method
