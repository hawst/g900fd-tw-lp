.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    .line 1019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1024
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1025
    .local v1, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1027
    .local v2, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 1058
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 1059
    return v6

    .line 1029
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomLayout:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$71(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/View;

    move-result-object v3

    .line 1030
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1029
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1031
    .local v0, "bottomParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int v4, v1, v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V

    .line 1032
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    sub-int v4, v2, v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$26(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V

    .line 1033
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setPressed(Z)V

    goto :goto_0

    .line 1037
    .end local v0    # "bottomParams":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$73(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    move-result-object v3

    invoke-virtual {v3, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->smoothScrollBy(II)V

    .line 1038
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$73(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v5, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->scrollBy(II)V

    .line 1040
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mYDelta:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v3

    sub-int v3, v2, v3

    if-lez v3, :cond_0

    .line 1041
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-static {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$74(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 1042
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mYDelta:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v4

    sub-int v4, v2, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    goto :goto_0

    .line 1046
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mExpandFlag:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$75(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1047
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1048
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;->onResized()V

    .line 1050
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-static {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$74(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Z)V

    .line 1052
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 1055
    :pswitch_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$19;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mBottomExtendBg:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/16 :goto_0

    .line 1027
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
