.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;
.super Ljava/lang/Object;
.source "SpenPenPresetListAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .param p1, "paramView"    # Landroid/view/View;

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 89
    .local v0, "i":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;->selectPresetItem(I)V

    .line 92
    :cond_0
    const/4 v1, 0x1

    return v1
.end method
