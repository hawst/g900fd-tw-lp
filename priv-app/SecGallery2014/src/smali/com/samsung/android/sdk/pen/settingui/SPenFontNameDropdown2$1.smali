.class Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;
.super Ljava/lang/Object;
.source "SPenFontNameDropdown2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 150
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowHeight:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)I

    move-result v0

    .line 152
    .local v0, "mtempHeight":I
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->window:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->anchor:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v1

    if-le v0, v1, :cond_1

    .line 153
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->window:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->anchor:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v0

    .line 157
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->window:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->anchor:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$2(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/PopupWindow;->update(Landroid/view/View;II)V

    .line 165
    .end local v0    # "mtempHeight":I
    :cond_0
    :goto_1
    return-void

    .line 155
    .restart local v0    # "mtempHeight":I
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowHeight:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 162
    .end local v0    # "mtempHeight":I
    :catch_0
    move-exception v1

    goto :goto_1
.end method
