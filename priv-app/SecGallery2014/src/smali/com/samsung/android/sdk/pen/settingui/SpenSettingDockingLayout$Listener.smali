.class public interface abstract Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
.super Ljava/lang/Object;
.source "SpenSettingDockingLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# static fields
.field public static final MODE_ERASER:I = 0x2

.field public static final MODE_PEN:I = 0x1

.field public static final MODE_SELECTION:I = 0x4

.field public static final MODE_TEXT:I = 0x3


# virtual methods
.method public abstract onClearAll()V
.end method

.method public abstract onPopup(I)V
.end method
