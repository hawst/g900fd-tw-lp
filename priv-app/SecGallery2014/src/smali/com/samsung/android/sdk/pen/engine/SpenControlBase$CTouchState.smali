.class public Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;
.super Ljava/lang/Object;
.source "SpenControlBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "CTouchState"
.end annotation


# instance fields
.field public mState:I

.field public mTouchedObjectIndex:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 564
    return-void
.end method


# virtual methods
.method isCornerZonePressed()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 579
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    .line 580
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 583
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isHorizontalResizeZonePressed()Z
    .locals 2

    .prologue
    .line 587
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 588
    :cond_0
    const/4 v0, 0x1

    .line 590
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isMoveZonePressed()Z
    .locals 2

    .prologue
    .line 601
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 602
    const/4 v0, 0x1

    .line 604
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isPressed()Z
    .locals 2

    .prologue
    .line 615
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 616
    const/4 v0, 0x1

    .line 618
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isResizeZonePressed()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 572
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-lt v1, v0, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_0

    .line 575
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isRotateZonePressed()Z
    .locals 1

    .prologue
    .line 608
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-nez v0, :cond_0

    .line 609
    const/4 v0, 0x1

    .line 611
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isVerticalResizeZonePressed()Z
    .locals 2

    .prologue
    .line 594
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 595
    :cond_0
    const/4 v0, 0x1

    .line 597
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method reset()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 567
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    .line 568
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    .line 569
    return-void
.end method
