.class public Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;
.super Ljava/lang/Object;
.source "TextRecognitionPlugin.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$HandleInfo;,
        Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "recognition-TextRecognitionPlugin"


# instance fields
.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

.field private final mRecogHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 56
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogHandler:Landroid/os/Handler;

    .line 71
    const-string v0, "recognition-TextRecognitionPlugin"

    const-string v1, "creating text recognition plugin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->getCurrentLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 264
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 248
    return-void
.end method

.method public getSupportedLanguage()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v0, :cond_0

    .line 295
    const/4 v0, 0x0

    .line 298
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->getSupportedLanguage()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public initRecognizer(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 79
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v1, :cond_1

    .line 80
    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    .line 82
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v1, :cond_0

    .line 83
    const-string v1, "recognition-TextRecognitionPlugin"

    const-string v2, "Fail to create TextRecognition instance"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :goto_0
    return v0

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    const-string v2, "eng"

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->init(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 88
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    goto :goto_0

    .line 92
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 174
    const-string v4, "Text Recognition"

    const-string v5, "Load libSPenVITextAll.so."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    sget-boolean v4, Lcom/samsung/android/sdk/pen/Spen;->IS_SPEN_PRELOAD_MODE:Z

    if-eqz v4, :cond_1

    .line 178
    :try_start_0
    const-string v4, "SPenVITextAll"

    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->initRecognizer(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 198
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Fail to load Text recognition engine"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 179
    :catch_0
    move-exception v1

    .line 180
    .local v1, "error":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 181
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "libSPenVITextAll.so is not loaded."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 184
    .end local v1    # "error":Ljava/lang/Exception;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/data/data/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/lib/lib"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SPen"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 185
    const-string v5, "VIText"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "All"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".so"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 184
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 186
    .local v3, "libFullName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 187
    .local v2, "libFilePath":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 189
    :try_start_1
    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 190
    :catch_1
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 192
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "libSPenVITextAll.so is not loaded."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 200
    .end local v0    # "e":Ljava/lang/Throwable;
    .end local v2    # "libFilePath":Ljava/io/File;
    .end local v3    # "libFullName":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public declared-synchronized onUnload()V
    .locals 1

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->dispose()V

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :cond_0
    monitor-exit p0

    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public request(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    .local p1, "input":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    if-nez v1, :cond_0

    .line 274
    const-string v1, "recognition-TextRecognitionPlugin"

    const-string v2, "The result listener isn\'t set yet!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 279
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-eqz v1, :cond_1

    .line 280
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 281
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 286
    .end local v0    # "thread":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 283
    :cond_1
    const-string v1, "recognition-TextRecognitionPlugin"

    const-string v2, "The recognition engine is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->setLanguage(Ljava/lang/String;)V

    .line 228
    :cond_0
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 256
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    .line 162
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 307
    const/4 v0, 0x1

    return v0
.end method
