.class Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;
.super Ljava/lang/Object;
.source "Signature.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SignatureVerificationRunnable"
.end annotation


# instance fields
.field private mInput:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p2, "stroke":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->mInput:Ljava/util/List;

    .line 257
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->mInput:Ljava/util/List;

    .line 258
    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 263
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->mInput:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 266
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 278
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)I

    move-result v8

    # invokes: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->verifySignature(I)Z
    invoke-static {v7, v8}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$3(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;I)Z

    move-result v5

    .line 280
    .local v5, "ret":Z
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$4(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x0

    new-instance v9, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->mInput:Ljava/util/List;

    invoke-direct {v9, v10, v5}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;-><init>(Ljava/util/List;Z)V

    invoke-static {v7, v8, v9}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 281
    .local v2, "msg":Landroid/os/Message;
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$4(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 282
    return-void

    .line 269
    .end local v2    # "msg":Landroid/os/Message;
    .end local v5    # "ret":Z
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 271
    .local v0, "__stroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v3

    .line 272
    .local v3, "points":[Landroid/graphics/PointF;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v6

    .line 273
    .local v6, "stamp":[I
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v4

    .line 275
    .local v4, "pressure":[F
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    move-result-object v7

    invoke-virtual {v7, v3, v6, v4}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->setDrawData([Landroid/graphics/PointF;[I[F)V

    goto :goto_0
.end method
