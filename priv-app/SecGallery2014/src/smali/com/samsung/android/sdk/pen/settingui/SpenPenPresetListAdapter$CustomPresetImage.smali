.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;
.super Ljava/lang/Object;
.source "SpenPenPresetListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomPresetImage"
.end annotation


# instance fields
.field private mImage:Landroid/graphics/drawable/Drawable;

.field private mName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/drawable/Drawable;
    .param p2, "penName"    # Ljava/lang/String;

    .prologue
    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->mImage:Landroid/graphics/drawable/Drawable;

    .line 483
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->mName:Ljava/lang/String;

    .line 484
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;)V
    .locals 0

    .prologue
    .line 498
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->close()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 486
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->getPenName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 490
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private close()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 499
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->mImage:Landroid/graphics/drawable/Drawable;

    .line 500
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->mName:Ljava/lang/String;

    .line 501
    return-void
.end method

.method private getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->mImage:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private getPenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->mName:Ljava/lang/String;

    return-object v0
.end method

.method private setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 495
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$CustomPresetImage;->mImage:Landroid/graphics/drawable/Drawable;

    .line 496
    return-void
.end method
