.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
.super Ljava/lang/Object;
.source "SpenTextBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CContextMenu"
.end annotation


# instance fields
.field public mDirtyFlag:Z

.field public mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 342
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mDirtyFlag:Z

    .line 340
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;)V
    .locals 0

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->close()Z

    .line 366
    :cond_0
    return-void
.end method

.method public getMenuHeight()F
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getPopupHeight()F

    move-result v0

    .line 392
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    .line 372
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->isShowing()Z

    move-result v0

    .line 384
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 345
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mDirtyFlag:Z

    .line 346
    return-void
.end method

.method public setDirty()V
    .locals 1

    .prologue
    .line 349
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mDirtyFlag:Z

    .line 350
    return-void
.end method

.method public setInstance(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 0
    .param p1, "instance"    # Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .prologue
    .line 359
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    .line 360
    return-void
.end method

.method public setRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->setRect(Landroid/graphics/Rect;)V

    .line 356
    :cond_0
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->show()V

    .line 378
    :cond_0
    return-void
.end method
