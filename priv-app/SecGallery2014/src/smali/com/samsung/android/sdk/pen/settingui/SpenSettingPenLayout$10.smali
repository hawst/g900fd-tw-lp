.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private preIndex:I

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 1536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1537
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1541
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    .line 1542
    .local v8, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    .line 1543
    .local v9, "y":F
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42480000    # 50.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    .line 1544
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x428a0000    # 69.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    .line 1545
    const/high16 v12, 0x428a0000    # 69.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v11, v9

    mul-float/2addr v10, v11

    .line 1543
    sub-float v10, v8, v10

    .line 1545
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v11}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v11

    const/high16 v12, 0x42300000    # 44.0f

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v11

    int-to-float v11, v11

    .line 1543
    div-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1547
    .local v1, "index":I
    if-gez v1, :cond_2

    .line 1548
    const/4 v1, 0x0

    .line 1553
    :cond_0
    :goto_0
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    if-gez v10, :cond_3

    .line 1554
    const/4 v10, 0x0

    iput v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    .line 1559
    :cond_1
    :goto_1
    const/4 v2, 0x0

    .line 1560
    .local v2, "indexTypeView":I
    const/4 v4, 0x0

    .line 1561
    .local v4, "preIndexTypeView":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$60(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/RelativeLayout;

    move-result-object v10

    invoke-virtual {v10, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1562
    .local v6, "v1":Landroid/view/View;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$60(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/RelativeLayout;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 1563
    .local v7, "v2":Landroid/view/View;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v0, v10, :cond_4

    .line 1571
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1572
    .local v3, "penName":Ljava/lang/String;
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1573
    .local v5, "prePenName":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    const/16 v11, 0xa

    if-ne v10, v11, :cond_7

    .line 1574
    const/16 v10, 0xa

    invoke-virtual {p2, v10}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1575
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, p2}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 1576
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    .line 1577
    const/4 v10, 0x1

    .line 1606
    :goto_3
    return v10

    .line 1549
    .end local v0    # "i":I
    .end local v2    # "indexTypeView":I
    .end local v3    # "penName":Ljava/lang/String;
    .end local v4    # "preIndexTypeView":I
    .end local v5    # "prePenName":Ljava/lang/String;
    .end local v6    # "v1":Landroid/view/View;
    .end local v7    # "v2":Landroid/view/View;
    :cond_2
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    add-int/lit8 v10, v10, -0x1

    if-le v1, v10, :cond_0

    .line 1550
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    add-int/lit8 v1, v10, -0x1

    goto/16 :goto_0

    .line 1555
    :cond_3
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget v11, v11, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    add-int/lit8 v11, v11, -0x1

    if-le v10, v11, :cond_1

    .line 1556
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget v10, v10, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    goto/16 :goto_1

    .line 1564
    .restart local v0    # "i":I
    .restart local v2    # "indexTypeView":I
    .restart local v4    # "preIndexTypeView":I
    .restart local v6    # "v1":Landroid/view/View;
    .restart local v7    # "v2":Landroid/view/View;
    :cond_4
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    if-ne v6, v10, :cond_5

    .line 1565
    move v2, v0

    .line 1567
    :cond_5
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    if-ne v7, v10, :cond_6

    .line 1568
    move v4, v0

    .line 1563
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 1580
    .restart local v3    # "penName":Ljava/lang/String;
    .restart local v5    # "prePenName":Ljava/lang/String;
    :cond_7
    iget v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    if-eq v1, v10, :cond_d

    .line 1581
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42300000    # 44.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    cmpg-float v10, v9, v10

    if-ltz v10, :cond_8

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1582
    :cond_8
    const/16 v10, 0xa

    invoke-virtual {p2, v10}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1583
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, p2}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 1586
    :cond_9
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42300000    # 44.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    cmpg-float v10, v9, v10

    if-ltz v10, :cond_a

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1587
    :cond_a
    const/16 v10, 0xa

    invoke-virtual {p2, v10}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1588
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, p2}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 1589
    const/16 v10, 0x9

    invoke-virtual {p2, v10}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1590
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    const/16 v11, 0x80

    invoke-virtual {v10, v11}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 1599
    :cond_b
    :goto_4
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42300000    # 44.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    cmpg-float v10, v9, v10

    if-ltz v10, :cond_c

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 1600
    :cond_c
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, p2}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 1605
    :goto_5
    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->preIndex:I

    .line 1606
    const/4 v10, 0x1

    goto/16 :goto_3

    .line 1593
    :cond_d
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v10

    const/high16 v11, 0x42300000    # 44.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    int-to-float v10, v10

    cmpg-float v10, v9, v10

    if-ltz v10, :cond_e

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1594
    :cond_e
    const/16 v10, 0x9

    invoke-virtual {p2, v10}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1595
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, p2}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    goto :goto_4

    .line 1602
    :cond_f
    const/16 v10, 0xa

    invoke-virtual {p2, v10}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1603
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, p2}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    goto :goto_5
.end method
