.class Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;
.super Landroid/os/Handler;
.source "SpenCanvasViewScroll.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;
    }
.end annotation


# static fields
.field private static final SCROLL_BAR_MARGIN:I = 0xa

.field private static final SCROLL_BAR_MARGIN_DENSITY_1:I = 0x4

.field private static final SCROLL_BAR_THICK:I = 0xa

.field private static final SCROLL_BAR_THICK_DENSITY_1:I = 0x4


# instance fields
.field private mDeltaX:F

.field private mDeltaY:F

.field private mEnable:Z

.field private mHorizontalScrollEnable:Z

.field private mIsUpdate:Z

.field private final mListener:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mPaint:Landroid/graphics/Paint;

.field private mRatioBitmapH:I

.field private mRatioBitmapW:I

.field private mRectLR:Landroid/graphics/Rect;

.field private mRectTB:Landroid/graphics/Rect;

.field private mScreenH:I

.field private mScreenW:I

.field private mScrollBarMagin:I

.field private mScrollBarThick:I

.field private mShow:Z

.field private mVerticalScrollEnable:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;F)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;
    .param p2, "density"    # F

    .prologue
    const/16 v3, 0xa

    const/4 v2, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 34
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 32
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mIsUpdate:Z

    .line 35
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    .line 36
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mHorizontalScrollEnable:Z

    .line 37
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mVerticalScrollEnable:Z

    .line 38
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mShow:Z

    .line 40
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectLR:Landroid/graphics/Rect;

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectTB:Landroid/graphics/Rect;

    .line 43
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    const v1, -0x7fa0a0a1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 46
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;

    .line 47
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarThick:I

    .line 48
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarMagin:I

    .line 49
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    .line 50
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarThick:I

    .line 51
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarMagin:I

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectLR:Landroid/graphics/Rect;

    .line 57
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectTB:Landroid/graphics/Rect;

    .line 59
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    .line 60
    return-void
.end method

.method public drawScroll(Landroid/graphics/Canvas;)Z
    .locals 3
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 63
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    if-nez v1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mShow:Z

    if-eqz v1, :cond_0

    .line 71
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mHorizontalScrollEnable:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaX:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectLR:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 75
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mVerticalScrollEnable:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaY:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_3

    .line 76
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectTB:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 78
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public enableHorizontalScroll(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mHorizontalScrollEnable:Z

    .line 143
    return-void
.end method

.method public enableScroll(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 126
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    .line 127
    return-void
.end method

.method public enableUpdate(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mIsUpdate:Z

    .line 159
    return-void
.end method

.method public enableVerticalScroll(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mVerticalScrollEnable:Z

    .line 135
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mShow:Z

    .line 152
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;->onUpdate()V

    .line 155
    :cond_0
    return-void
.end method

.method public isHorizontalScroll()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mHorizontalScrollEnable:Z

    return v0
.end method

.method public isScroll()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    return v0
.end method

.method public isVerticalScroll()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mVerticalScrollEnable:Z

    return v0
.end method

.method public setDeltaValue(FFFF)V
    .locals 15
    .param p1, "deltaX"    # F
    .param p2, "deltaY"    # F
    .param p3, "maxDeltaX"    # F
    .param p4, "maxDeltaY"    # F

    .prologue
    .line 92
    move/from16 v0, p1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mDeltaX:F

    .line 93
    move/from16 v0, p2

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mDeltaY:F

    .line 95
    move/from16 v0, p3

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaX:F

    .line 96
    move/from16 v0, p4

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaY:F

    .line 98
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaX:F

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_0

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapW:I

    if-lez v10, :cond_0

    .line 99
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    mul-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapW:I

    div-int v7, v10, v11

    .line 100
    .local v7, "scrollbarW":I
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    sub-int/2addr v10, v7

    add-int/lit8 v3, v10, 0x1

    .line 101
    .local v3, "canMoveDistanceW":I
    int-to-float v10, v3

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaX:F

    div-float v5, v10, v11

    .line 102
    .local v5, "factorW":F
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mDeltaX:F

    mul-float/2addr v10, v5

    float-to-int v8, v10

    .line 103
    .local v8, "startLR":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectLR:Landroid/graphics/Rect;

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarThick:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarMagin:I

    sub-int/2addr v11, v12

    add-int v12, v8, v7

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    .line 104
    iget v14, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarMagin:I

    sub-int/2addr v13, v14

    .line 103
    invoke-virtual {v10, v8, v11, v12, v13}, Landroid/graphics/Rect;->set(IIII)V

    .line 107
    .end local v3    # "canMoveDistanceW":I
    .end local v5    # "factorW":F
    .end local v7    # "scrollbarW":I
    .end local v8    # "startLR":I
    :cond_0
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaY:F

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_1

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapH:I

    if-lez v10, :cond_1

    .line 108
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    mul-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapH:I

    div-int v6, v10, v11

    .line 109
    .local v6, "scrollbarH":I
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    sub-int/2addr v10, v6

    add-int/lit8 v2, v10, 0x1

    .line 110
    .local v2, "canMoveDistanceH":I
    int-to-float v10, v2

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaY:F

    div-float v4, v10, v11

    .line 111
    .local v4, "factorH":F
    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mDeltaY:F

    mul-float/2addr v10, v4

    float-to-int v9, v10

    .line 112
    .local v9, "startTB":I
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectTB:Landroid/graphics/Rect;

    iget v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarThick:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarMagin:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    iget v13, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScrollBarMagin:I

    sub-int/2addr v12, v13

    .line 113
    add-int v13, v9, v6

    .line 112
    invoke-virtual {v10, v11, v9, v12, v13}, Landroid/graphics/Rect;->set(IIII)V

    .line 116
    .end local v2    # "canMoveDistanceH":I
    .end local v4    # "factorH":F
    .end local v6    # "scrollbarH":I
    .end local v9    # "startTB":I
    :cond_1
    iget-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    if-eqz v10, :cond_2

    .line 117
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mShow:Z

    .line 118
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->removeMessages(I)V

    .line 119
    iget-boolean v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mIsUpdate:Z

    if-eqz v10, :cond_2

    .line 120
    const/4 v10, 0x0

    const-wide/16 v12, 0x12c

    invoke-virtual {p0, v10, v12, v13}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->sendEmptyMessageDelayed(IJ)Z

    .line 123
    :cond_2
    return-void
.end method

.method public setRatioBitmapSize(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapW:I

    .line 88
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapH:I

    .line 89
    return-void
.end method

.method public setScreenSize(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    .line 83
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    .line 84
    return-void
.end method
