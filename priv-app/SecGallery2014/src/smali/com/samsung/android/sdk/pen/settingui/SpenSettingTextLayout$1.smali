.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x1

    .line 405
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/GestureDetector;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 406
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mGestureDetector:Landroid/view/GestureDetector;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/view/GestureDetector;

    move-result-object v6

    invoke-virtual {v6, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 482
    :goto_0
    return v10

    .line 410
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v6}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 411
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontNameDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->dismiss()V

    .line 413
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v6}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 414
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->dismiss()V

    .line 416
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 417
    .local v4, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 418
    .local v5, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 481
    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 423
    :pswitch_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 427
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mXDelta:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v6

    sub-int v2, v4, v6

    .line 428
    .local v2, "mMoveX":I
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mYDelta:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)I

    move-result v6

    sub-int v3, v5, v6

    .line 429
    .local v3, "mMoveY":I
    if-gez v2, :cond_4

    .line 430
    const/4 v2, 0x0

    .line 432
    :cond_4
    if-gez v3, :cond_5

    .line 433
    const/4 v3, 0x0

    .line 435
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/RelativeLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getWidth()I

    move-result v7

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x2

    if-le v2, v6, :cond_6

    .line 436
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/RelativeLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getWidth()I

    move-result v7

    sub-int/2addr v6, v7

    add-int/lit8 v2, v6, -0x2

    .line 439
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getHeight()I

    move-result v1

    .line 440
    .local v1, "height":I
    const-string/jumbo v6, "settingui-settingText"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "height = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    const-string/jumbo v6, "settingui-settingText"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "before mMoveY = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$7()I

    move-result v6

    if-gt v1, v6, :cond_a

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$7()I

    move-result v7

    sub-int/2addr v6, v7

    if-le v3, v6, :cond_a

    .line 444
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$7()I

    move-result v7

    sub-int v3, v6, v7

    .line 452
    :cond_7
    :goto_2
    const-string/jumbo v6, "settingui-settingText"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "after mMoveY = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    if-gez v2, :cond_8

    .line 455
    const/4 v2, 0x0

    .line 457
    :cond_8
    if-gez v3, :cond_9

    .line 458
    const/4 v3, 0x0

    .line 461
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 462
    .local v0, "Params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 463
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 464
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;I)V

    .line 465
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 445
    .end local v0    # "Params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_a
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    sub-int/2addr v6, v1

    if-le v3, v6, :cond_7

    .line 446
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    sub-int/2addr v7, v3

    .line 447
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v8

    const/high16 v9, 0x42860000    # 67.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v8

    sub-int/2addr v7, v8

    .line 446
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setExpandBarPosition(I)V

    .line 448
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$7()I

    move-result v7

    sub-int/2addr v6, v7

    if-le v3, v6, :cond_7

    .line 449
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->minHeight:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$7()I

    move-result v7

    sub-int v3, v6, v7

    goto :goto_2

    .line 470
    .end local v1    # "height":I
    .end local v2    # "mMoveX":I
    .end local v3    # "mMoveY":I
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMoveSettingLayout:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 471
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 473
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$ActionListener;->onMoved()V

    .line 476
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldMovableRect:Landroid/graphics/Rect;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 477
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mOldLocation:[I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)[I

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getLocationOnScreen([I)V

    goto/16 :goto_1

    .line 418
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
