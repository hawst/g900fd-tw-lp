.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
.super Landroid/os/Handler;
.source "SpenTextBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Blink"
.end annotation


# static fields
.field private static final CURSOR_DELAY:I = 0x258

.field public static final CURSOR_MESSAGE:I = 0x1


# instance fields
.field private mIsStartBlink:Z

.field private final mTextBox:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenTextBox;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1
    .param p1, "textBox"    # Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .prologue
    .line 5695
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 5691
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    .line 5696
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    .line 5697
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 5701
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5724
    :cond_0
    :goto_0
    return-void

    .line 5704
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5705
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5709
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 5723
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 5711
    :pswitch_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    .line 5712
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    if-eqz v1, :cond_2

    .line 5713
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    .line 5714
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5715
    const-wide/16 v4, 0x258

    invoke-virtual {p0, v2, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_3
    move v1, v2

    .line 5714
    goto :goto_2

    .line 5709
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public restartBlink()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5745
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5760
    :cond_0
    :goto_0
    return-void

    .line 5748
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5749
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5753
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5755
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    .line 5757
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    if-eqz v1, :cond_0

    .line 5758
    const-wide/16 v2, 0x258

    invoke-virtual {p0, v4, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public startBlink()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5727
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5742
    :cond_0
    :goto_0
    return-void

    .line 5730
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5731
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5735
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5737
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    .line 5739
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    .line 5741
    const-wide/16 v2, 0x258

    invoke-virtual {p0, v4, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public stopBlink()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5763
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5776
    :cond_0
    :goto_0
    return-void

    .line 5766
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5767
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5771
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    .line 5773
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    .line 5775
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    goto :goto_0
.end method
