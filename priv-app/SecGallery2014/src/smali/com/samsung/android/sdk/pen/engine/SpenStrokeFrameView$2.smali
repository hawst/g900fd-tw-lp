.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$2;
.super Ljava/lang/Object;
.source "SpenStrokeFrameView.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    .line 348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 2
    .param p1, "arg0"    # [B
    .param p2, "arg1"    # Landroid/hardware/Camera;

    .prologue
    .line 353
    if-eqz p1, :cond_0

    .line 354
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 355
    const-string v0, "KYKY"

    const-string v1, "KYKY start oncomplete"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;->onComplete([B)V

    .line 359
    :cond_0
    return-void
.end method
