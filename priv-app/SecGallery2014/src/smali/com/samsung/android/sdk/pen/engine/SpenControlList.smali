.class public Lcom/samsung/android/sdk/pen/engine/SpenControlList;
.super Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.source "SpenControlList.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;,
        Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    }
.end annotation


# static fields
.field private static final CONFIG_LIST_GROUP:Z = true


# instance fields
.field private mBoundRectHeight:F

.field private mBoundRectWidth:F

.field private final mDeltaPoint:Landroid/graphics/PointF;

.field private mDrawingBitmap:Landroid/graphics/Bitmap;

.field private mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

.field private final mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

.field private mTextBoxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenTextBox;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v1, 0x0

    .line 671
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 50
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mMatrix:Landroid/graphics/Matrix;

    .line 51
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDrawingBitmap:Landroid/graphics/Bitmap;

    .line 949
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    .line 1017
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    .line 672
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->initialize()V

    .line 673
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlList;F)V
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)F
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenControlList;FFFFF)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1096
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method private drawListHighlight(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 833
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    .line 835
    .local v1, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->isDimEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 836
    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 837
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 838
    .local v2, "roundRect":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 839
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 840
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v4

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    .line 841
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v5

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    .line 840
    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 842
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 843
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 846
    .end local v2    # "roundRect":Landroid/graphics/Rect;
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 849
    return-void

    .line 846
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 847
    .local v0, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->drawHighlightObject(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 676
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    .line 677
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->set(Z)V

    .line 679
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    .line 680
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    .line 681
    return-void
.end method

.method private rotatePoint(FFFFF)Landroid/graphics/PointF;
    .locals 4
    .param p1, "px"    # F
    .param p2, "py"    # F
    .param p3, "angle"    # F
    .param p4, "x"    # F
    .param p5, "y"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1097
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mMatrix:Landroid/graphics/Matrix;

    if-nez v1, :cond_0

    .line 1098
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mMatrix:Landroid/graphics/Matrix;

    .line 1101
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, p3, p1, p2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 1103
    const/4 v1, 0x2

    new-array v0, v1, [F

    .line 1104
    .local v0, "pts":[F
    aput p4, v0, v2

    .line 1105
    aput p5, v0, v3

    .line 1107
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1109
    new-instance v1, Landroid/graphics/PointF;

    aget v2, v0, v2

    aget v3, v0, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v1
.end method

.method private updateGroup()V
    .locals 3

    .prologue
    .line 818
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mDirtyFlag:Z

    if-eqz v1, :cond_0

    .line 819
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->reset()V

    .line 820
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-eqz v1, :cond_1

    .line 821
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 822
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 823
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v1

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->appendObject(Ljava/util/ArrayList;)V

    .line 824
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObjectList(Ljava/util/ArrayList;)V

    .line 828
    .end local v0    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->fit()V

    .line 830
    :cond_0
    return-void

    .line 826
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObjectList(Ljava/util/ArrayList;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    .line 1056
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1057
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1061
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1064
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->recycleDrawingBitmap()V

    .line 1066
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    .line 1067
    return-void

    .line 1057
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 1058
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getObjectText()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V

    .line 1059
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    goto :goto_0
.end method

.method public fit()V
    .locals 3

    .prologue
    .line 1077
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    .line 1079
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1080
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1084
    :cond_0
    return-void

    .line 1080
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 1081
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    goto :goto_0
.end method

.method protected getBoundBoxObject(Landroid/graphics/RectF;F)Landroid/graphics/RectF;
    .locals 14
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "degree"    # F

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 1119
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    .line 1120
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1123
    :cond_0
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12}, Landroid/graphics/RectF;-><init>()V

    .line 1124
    .local v12, "tmpRect":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v12, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 1126
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v3, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    .line 1161
    :goto_0
    return-object v12

    .line 1130
    :cond_1
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    .line 1131
    .local v1, "centerX":F
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    .line 1133
    .local v2, "centerY":F
    const/4 v0, 0x4

    new-array v11, v0, [Landroid/graphics/PointF;

    .line 1135
    .local v11, "rotatePoint":[Landroid/graphics/PointF;
    const/4 v13, 0x0

    iget v4, v12, Landroid/graphics/RectF;->left:F

    iget v5, v12, Landroid/graphics/RectF;->top:F

    move-object v0, p0

    move/from16 v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v11, v13

    .line 1136
    const/4 v13, 0x1

    iget v4, v12, Landroid/graphics/RectF;->right:F

    iget v5, v12, Landroid/graphics/RectF;->top:F

    move-object v0, p0

    move/from16 v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v11, v13

    .line 1137
    const/4 v13, 0x2

    iget v4, v12, Landroid/graphics/RectF;->left:F

    iget v5, v12, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    move/from16 v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v11, v13

    .line 1138
    const/4 v13, 0x3

    iget v4, v12, Landroid/graphics/RectF;->right:F

    iget v5, v12, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    move/from16 v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->rotatePoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v11, v13

    .line 1140
    const/4 v0, 0x0

    aget-object v0, v11, v0

    iget v8, v0, Landroid/graphics/PointF;->x:F

    .line 1141
    .local v8, "pointX_min":F
    const/4 v0, 0x0

    aget-object v0, v11, v0

    iget v10, v0, Landroid/graphics/PointF;->y:F

    .line 1142
    .local v10, "pointY_min":F
    const/4 v0, 0x0

    aget-object v0, v11, v0

    iget v7, v0, Landroid/graphics/PointF;->x:F

    .line 1143
    .local v7, "pointX_max":F
    const/4 v0, 0x0

    aget-object v0, v11, v0

    iget v9, v0, Landroid/graphics/PointF;->y:F

    .line 1144
    .local v9, "pointY_max":F
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    array-length v0, v11

    if-lt v6, v0, :cond_2

    .line 1159
    invoke-virtual {v12, v8, v10, v7, v9}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 1145
    :cond_2
    aget-object v0, v11, v6

    iget v0, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v8, v0

    if-ltz v0, :cond_3

    .line 1146
    aget-object v0, v11, v6

    iget v8, v0, Landroid/graphics/PointF;->x:F

    .line 1148
    :cond_3
    aget-object v0, v11, v6

    iget v0, v0, Landroid/graphics/PointF;->x:F

    cmpg-float v0, v7, v0

    if-gtz v0, :cond_4

    .line 1149
    aget-object v0, v11, v6

    iget v7, v0, Landroid/graphics/PointF;->x:F

    .line 1151
    :cond_4
    aget-object v0, v11, v6

    iget v0, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_5

    .line 1152
    aget-object v0, v11, v6

    iget v10, v0, Landroid/graphics/PointF;->y:F

    .line 1154
    :cond_5
    aget-object v0, v11, v6

    iget v0, v0, Landroid/graphics/PointF;->y:F

    cmpg-float v0, v9, v0

    if-gtz v0, :cond_6

    .line 1155
    aget-object v0, v11, v6

    iget v9, v0, Landroid/graphics/PointF;->y:F

    .line 1144
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public getObject()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 706
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 942
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    .line 945
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 718
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    if-eqz v2, :cond_1

    .line 719
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 720
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 721
    .local v0, "rect":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 722
    .local v1, "relativeRect":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 724
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v2, :cond_0

    .line 725
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 728
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v1, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 732
    .end local v0    # "rect":Landroid/graphics/RectF;
    .end local v1    # "relativeRect":Landroid/graphics/RectF;
    :goto_0
    return-object v1

    :cond_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    goto :goto_0
.end method

.method public isGroup()Z
    .locals 1

    .prologue
    .line 930
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1026
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getStyle()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 1027
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setVisibility(I)V

    .line 1028
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v1

    .line 1029
    .local v1, "mPageDocWidth":I
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v0

    .line 1030
    .local v0, "mPageDocHeight":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getObjectList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1045
    .end local v0    # "mPageDocHeight":I
    .end local v1    # "mPageDocWidth":I
    :cond_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onAttachedToWindow()V

    .line 1046
    return-void

    .line 1030
    .restart local v0    # "mPageDocHeight":I
    .restart local v1    # "mPageDocWidth":I
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 1031
    .local v2, "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v4

    const/4 v6, 0x2

    if-ne v4, v6, :cond_0

    .line 1033
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-direct {v3, v6, v4, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V

    .line 1035
    .local v3, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .end local v2    # "objectBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 1036
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBoxListener(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;)V

    .line 1037
    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setViewModeEnabled(Z)V

    .line 1038
    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    .line 1039
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1040
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 873
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->updateGroup()V

    .line 875
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getStyle()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 890
    :goto_0
    return-void

    .line 879
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-eqz v1, :cond_2

    .line 880
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDrawingBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 881
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDrawingBitmap:Landroid/graphics/Bitmap;

    .line 882
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDrawingBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 883
    .local v0, "tmpCanvas":Landroid/graphics/Canvas;
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->drawListHighlight(Landroid/graphics/Canvas;)V

    .line 886
    .end local v0    # "tmpCanvas":Landroid/graphics/Canvas;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDrawingBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 889
    :cond_2
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onObjectChanged()V
    .locals 0

    .prologue
    .line 813
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->recycleDrawingBitmap()V

    .line 814
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    .line 815
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 11
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 776
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_1

    .line 805
    :cond_0
    :goto_0
    return-void

    .line 780
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->recycleDrawingBitmap()V

    .line 782
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onSizeChanged(IIII)V

    .line 784
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 786
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v8, :cond_2

    .line 787
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 790
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 791
    .local v1, "firstAbsoluteObjRect":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 792
    .local v2, "firstRelativeObjRect":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v2, v1, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 794
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v9, v10

    add-float v4, v8, v9

    .line 795
    .local v4, "newCenterX":F
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v9, v10

    add-float v5, v8, v9

    .line 797
    .local v5, "newCenterY":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    sub-float v3, v4, v8

    .line 798
    .local v3, "left":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    add-float v6, v3, v8

    .line 799
    .local v6, "right":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    sub-float v7, v5, v8

    .line 800
    .local v7, "top":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    add-float v0, v7, v8

    .line 802
    .local v0, "bottom":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8, v3, v7, v6, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 804
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->fit()V

    goto/16 :goto_0
.end method

.method protected onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V
    .locals 2
    .param p1, "objectTextBox"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .param p2, "visible"    # Z

    .prologue
    .line 1009
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v1, :cond_0

    .line 1010
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1011
    .local v0, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v1, v0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onVisibleUpdated(Ljava/util/ArrayList;Z)V

    .line 1015
    .end local v0    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    :cond_0
    return-void
.end method

.method protected onZoom()V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    .line 741
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_1

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 745
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    .line 747
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v8, :cond_2

    .line 748
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 751
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 752
    .local v1, "firstAbsoluteObjRect":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 753
    .local v2, "firstRelativeObjRect":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v2, v1, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    .line 755
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v9, v10

    add-float v4, v8, v9

    .line 756
    .local v4, "newCenterX":F
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDeltaPoint:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v9, v10

    add-float v5, v8, v9

    .line 758
    .local v5, "newCenterY":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F

    div-float/2addr v8, v11

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    sub-float v3, v4, v8

    .line 759
    .local v3, "left":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectWidth:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    add-float v6, v3, v8

    .line 760
    .local v6, "right":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F

    div-float/2addr v8, v11

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    sub-float v7, v5, v8

    .line 761
    .local v7, "top":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mBoundRectHeight:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    add-float v0, v7, v8

    .line 763
    .local v0, "bottom":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8, v3, v7, v6, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 765
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->recycleDrawingBitmap()V

    .line 766
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->fit()V

    .line 767
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->invalidate()V

    goto/16 :goto_0
.end method

.method protected recycleDrawingBitmap()V
    .locals 1

    .prologue
    .line 1169
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDrawingBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1170
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDrawingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mDrawingBitmap:Landroid/graphics/Bitmap;

    .line 1173
    :cond_0
    return-void
.end method

.method public setDimEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 857
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setDimEnabled(Z)V

    .line 858
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->recycleDrawingBitmap()V

    .line 859
    return-void
.end method

.method public setGroup(Z)V
    .locals 1
    .param p1, "groupFlag"    # Z

    .prologue
    .line 913
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->set(Z)V

    .line 914
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    .line 915
    return-void
.end method

.method protected setListener(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    .prologue
    .line 1092
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    .line 1093
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;)V

    .line 1094
    return-void
.end method

.method public setObject(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 693
    .local p1, "objectBaseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObjectList(Ljava/util/ArrayList;)V

    .line 694
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->appendObject(Ljava/util/ArrayList;)V

    .line 695
    return-void
.end method
