.class Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;
.super Ljava/lang/Object;
.source "SPenSeekBarView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    .line 573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x14

    .line 577
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoIncrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 578
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 579
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$8(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mAutoDecrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 582
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView$11;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;->access$8(Lcom/samsung/android/sdk/pen/settingui/SPenSeekBarView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
