.class public Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;
.super Ljava/lang/Object;
.source "SpenCapturePage.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SpenCapturePage"


# instance fields
.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mCanvasLayer:Landroid/graphics/Bitmap;

.field private mIs64:Z

.field private mIsHyperText:Z

.field private mNativeCapture:J

.field private mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    .line 29
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    .line 31
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIsHyperText:Z

    .line 32
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    .line 44
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    .line 46
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_init()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    .line 47
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_construct(JLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    .line 51
    :cond_0
    if-nez p1, :cond_3

    .line 52
    const-string v1, " : context must not be null"

    invoke-static {v4, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 63
    :cond_1
    :goto_1
    return-void

    .line 44
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 56
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 58
    .local v0, "mDisplayMetrics":Landroid/util/DisplayMetrics;
    if-eqz v0, :cond_1

    .line 59
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v2, :cond_4

    iget v6, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 61
    .local v6, "baseRate":I
    :goto_2
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    const/4 v4, 0x2

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    goto :goto_1

    .line 60
    .end local v6    # "baseRate":I
    :cond_4
    iget v6, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_2
.end method

.method private Native_captureRect(JLandroid/graphics/Bitmap;Landroid/graphics/RectF;)Z
    .locals 1
    .param p1, "nativeCapture"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    if-eqz v0, :cond_0

    .line 351
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_captureRect(JLandroid/graphics/Bitmap;Landroid/graphics/RectF;)Z

    move-result v0

    .line 353
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_captureRect(ILandroid/graphics/Bitmap;Landroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "nativeCapture"    # J
    .param p3, "command"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 358
    .local p4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    if-eqz v0, :cond_0

    .line 359
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 361
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_construct(JLandroid/content/Context;)Z
    .locals 1
    .param p1, "nativeCapture"    # J
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 318
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    if-eqz v0, :cond_0

    .line 319
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_construct(JLandroid/content/Context;)Z

    move-result v0

    .line 321
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_construct(ILandroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_finalize(J)V
    .locals 1
    .param p1, "nativeCapture"    # J

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    if-eqz v0, :cond_0

    .line 311
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_finalize(J)V

    .line 315
    :goto_0
    return-void

    .line 313
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_finalize(I)V

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    if-eqz v0, :cond_0

    .line 303
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_init_64()J

    move-result-wide v0

    .line 305
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "nativeCapture"    # J
    .param p3, "layerId"    # I
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 334
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    if-eqz v0, :cond_0

    .line 335
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    .line 339
    :goto_0
    return-void

    .line 337
    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z
    .locals 1
    .param p1, "nativeCapture"    # J
    .param p3, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    if-eqz v0, :cond_0

    .line 327
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z

    move-result v0

    .line 329
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setScreenSize(JII)Z
    .locals 1
    .param p1, "nativeCapture"    # J
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIs64:Z

    if-eqz v0, :cond_0

    .line 343
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setScreenSize(JII)Z

    move-result v0

    .line 345
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setScreenSize(III)Z

    move-result v0

    goto :goto_0
.end method

.method private createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 7
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    const/4 v6, 0x6

    .line 268
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    .line 273
    .local v1, "width":I
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    .line 275
    .local v0, "height":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    .line 276
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    .line 278
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    if-nez v2, :cond_2

    .line 279
    const-string v2, "The width of pageDoc is 0"

    invoke-static {v6, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 283
    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    if-nez v2, :cond_3

    .line 284
    const-string v2, "The height of pageDoc is 0"

    invoke-static {v6, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    .line 288
    :cond_3
    const-string v2, "SpenCapturePage"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createBitmap Width="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Height="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    if-ne v1, v2, :cond_4

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    if-eq v0, v2, :cond_0

    .line 293
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 294
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 297
    :cond_5
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    .line 298
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerIdByIndex(I)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private static native native_captureRect(ILandroid/graphics/Bitmap;Landroid/graphics/RectF;)Z
.end method

.method private static native native_captureRect(JLandroid/graphics/Bitmap;Landroid/graphics/RectF;)Z
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(ILandroid/content/Context;)Z
.end method

.method private static native native_construct(JLandroid/content/Context;)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setCanvasBitmap(JILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z
.end method

.method private static native native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z
.end method

.method private static native native_setScreenSize(III)Z
.end method

.method private static native native_setScreenSize(JII)Z
.end method


# virtual methods
.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/high16 v0, 0x40a00000    # 5.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 213
    :cond_0
    const/4 v0, 0x0

    .line 216
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    .line 217
    const/4 v3, 0x1

    .line 216
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public captureRect(Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v2, 0x0

    .line 176
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    move-object v0, v2

    .line 194
    :cond_0
    :goto_0
    return-object v0

    .line 180
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move-object v0, v2

    .line 181
    goto :goto_0

    .line 184
    :cond_3
    const/4 v0, 0x0

    .line 186
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 187
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    invoke-direct {p0, v4, v5, v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_captureRect(JLandroid/graphics/Bitmap;Landroid/graphics/RectF;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move-object v0, v2

    .line 194
    goto :goto_0

    .line 190
    :catch_0
    move-exception v1

    .line 191
    .local v1, "e":Ljava/lang/Throwable;
    const-string v3, "SpenCapturePage"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to create bitmap w = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " h = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const/4 v3, 0x2

    const-string v4, " : fail createBitmap."

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public close()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 227
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 239
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_finalize(J)V

    .line 232
    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 235
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    .line 238
    :cond_1
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_0
.end method

.method public compressPage(Ljava/lang/String;F)V
    .locals 12
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "ratio"    # F

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 104
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    cmpl-float v6, p2, v6

    if-nez v6, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    if-nez p1, :cond_2

    .line 109
    const/16 v6, 0x9

    invoke-static {v6}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    .line 113
    :cond_2
    const/4 v0, 0x0

    .line 114
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    float-to-double v6, p2

    cmpl-double v6, v6, v10

    if-eqz v6, :cond_5

    .line 116
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    int-to-float v7, v7

    mul-float/2addr v7, p2

    float-to-int v7, v7

    .line 117
    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    int-to-float v8, v8

    mul-float/2addr v8, p2

    float-to-int v8, v8

    const/4 v9, 0x1

    .line 116
    invoke-static {v6, v7, v8, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 129
    :goto_1
    const/4 v4, 0x0

    .line 131
    .local v4, "out":Ljava/io/OutputStream;
    :try_start_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 132
    .local v3, "file":Ljava/io/File;
    if-eqz v3, :cond_3

    .line 133
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 134
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    .end local v4    # "out":Ljava/io/OutputStream;
    .local v5, "out":Ljava/io/OutputStream;
    :try_start_2
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v4, v5

    .line 150
    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    :cond_3
    if-eqz v4, :cond_4

    .line 151
    :try_start_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    .line 159
    :cond_4
    float-to-double v6, p2

    cmpl-double v6, v6, v10

    if-eqz v6, :cond_0

    .line 160
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 118
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v1

    .line 119
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 121
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v1

    .line 122
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 126
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 138
    .restart local v4    # "out":Ljava/io/OutputStream;
    :catch_2
    move-exception v2

    .line 139
    .local v2, "e1":Ljava/io/IOException;
    :goto_2
    :try_start_4
    const-string v6, "SpenCapturePage"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "filename = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " width = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " height = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 140
    const-string v8, " ratio = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 139
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 150
    if-eqz v4, :cond_0

    .line 151
    :try_start_5
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 153
    :catch_3
    move-exception v1

    .line 154
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 143
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 144
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_6
    const-string v6, "SpenCapturePage"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "filename = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " width = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " height = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 145
    const-string v8, " ratio = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 144
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 150
    if-eqz v4, :cond_0

    .line 151
    :try_start_7
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto/16 :goto_0

    .line 153
    :catch_5
    move-exception v1

    .line 154
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 148
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 150
    :goto_4
    if-eqz v4, :cond_6

    .line 151
    :try_start_8
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 157
    :cond_6
    throw v6

    .line 153
    :catch_6
    move-exception v1

    .line 154
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 153
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v3    # "file":Ljava/io/File;
    :catch_7
    move-exception v1

    .line 154
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 148
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    goto :goto_4

    .line 143
    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    :catch_8
    move-exception v1

    move-object v4, v5

    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    goto :goto_3

    .line 138
    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    :catch_9
    move-exception v2

    move-object v4, v5

    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    goto/16 :goto_2
.end method

.method public isHyperTextViewEnabled()Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIsHyperText:Z

    return v0
.end method

.method public setHyperTextViewEnabled(Z)V
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    const/4 v4, 0x1

    .line 249
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIsHyperText:Z

    .line 253
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIsHyperText:Z

    if-eqz v0, :cond_1

    move v6, v4

    :goto_1
    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 4
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 77
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    const-string v0, "SpenCapturePage"

    const-string v1, "setPageDoc is closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 82
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    .line 83
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 85
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_setScreenSize(JII)Z

    .line 86
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->Native_setPageDoc(JLcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z

    goto :goto_0
.end method
