.class Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SPenFontNameDropdown.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    .line 58
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->items:Ljava/util/List;

    .line 59
    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->items:Ljava/util/List;

    .line 60
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->context:Landroid/content/Context;

    .line 61
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/high16 v5, 0x40a00000    # 5.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/16 v8, 0x640

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 66
    if-nez p2, :cond_0

    .line 67
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 68
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x1090008

    invoke-virtual {v0, v3, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 70
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v3, 0x1020014

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 72
    .local v2, "tv":Landroid/widget/TextView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 73
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 74
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 76
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    if-ne v3, v8, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v7

    if-nez v3, :cond_6

    .line 77
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const/high16 v4, 0x40400000    # 3.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_5

    .line 78
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41400000    # 12.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 85
    :goto_0
    const/16 v3, 0x13

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 87
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    if-ne v3, v8, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v7

    if-eqz v3, :cond_2

    .line 88
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    const/16 v4, 0x300

    if-ne v3, v4, :cond_8

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v9

    if-nez v3, :cond_8

    .line 89
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v7

    if-nez v3, :cond_7

    .line 90
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41800000    # 16.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 91
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 90
    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 100
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    const/16 v4, 0x300

    if-ne v3, v4, :cond_9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v9

    if-nez v3, :cond_9

    .line 101
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const v4, 0x42466666    # 49.6f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 111
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSelectedIndex:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I

    move-result v3

    if-ne v3, p1, :cond_b

    .line 112
    const v3, -0x2fbf00

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 116
    :goto_3
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 117
    .local v1, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 119
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_4

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    const/16 v4, 0x15

    if-ge v3, v4, :cond_4

    .line 120
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_4

    .line 121
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const-string v4, ""

    const-string/jumbo v5, "tw_spinner_list_pressed_holo_light"

    .line 122
    const-string/jumbo v6, "tw_spinner_list_focused_holo_light"

    .line 121
    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 125
    :cond_4
    return-object p2

    .line 80
    .end local v1    # "tf":Landroid/graphics/Typeface;
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const v4, 0x414dc28f    # 12.86f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 83
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 93
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const v4, 0x419e6666    # 19.8f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 94
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 93
    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 97
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x40c00000    # 6.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 98
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    const/high16 v5, 0x40c00000    # 6.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 97
    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 103
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const v4, 0x40333333    # 2.8f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    if-ne v3, v8, :cond_a

    .line 104
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v7

    if-nez v3, :cond_a

    .line 105
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const v4, 0x42146666    # 37.1f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 107
    :cond_a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x42200000    # 40.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 114
    :cond_b
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/high16 v5, 0x40a00000    # 5.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/16 v8, 0x640

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 132
    if-nez p2, :cond_0

    .line 133
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 135
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x1090008

    invoke-virtual {v0, v3, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 137
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v3, 0x1020014

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 139
    .local v2, "tv":Landroid/widget/TextView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 140
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 141
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 143
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    if-ne v3, v8, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v7

    if-nez v3, :cond_6

    .line 144
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const/high16 v4, 0x40400000    # 3.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_5

    .line 145
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41400000    # 12.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 152
    :goto_0
    const/16 v3, 0x13

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 154
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    if-ne v3, v8, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v7

    if-eqz v3, :cond_2

    .line 155
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    const/16 v4, 0x300

    if-ne v3, v4, :cond_8

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v9

    if-nez v3, :cond_8

    .line 156
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v7

    if-nez v3, :cond_7

    .line 157
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41800000    # 16.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 158
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 157
    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 167
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    const/16 v4, 0x300

    if-ne v3, v4, :cond_9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v9

    if-nez v3, :cond_9

    .line 168
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const v4, 0x42466666    # 49.6f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 178
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSelectedIndex:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$6(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I

    move-result v3

    if-ne v3, p1, :cond_b

    .line 179
    const v3, -0x2fbf00

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 183
    :goto_3
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 184
    .local v1, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 186
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_4

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mSdkVersion:I

    const/16 v4, 0x15

    if-ge v3, v4, :cond_4

    .line 187
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$7(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_4

    .line 188
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const-string v4, ""

    const-string/jumbo v5, "tw_spinner_list_pressed_holo_light"

    .line 189
    const-string/jumbo v6, "tw_spinner_list_focused_holo_light"

    .line 188
    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 191
    :cond_4
    return-object p2

    .line 147
    .end local v1    # "tf":Landroid/graphics/Typeface;
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const v4, 0x414dc28f    # 12.86f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 150
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 160
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const v4, 0x419e6666    # 19.8f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 161
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 160
    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 164
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x40c00000    # 6.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    .line 165
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v4

    const/high16 v5, 0x40c00000    # 6.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    .line 164
    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 170
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const v4, 0x40333333    # 2.8f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getScreenWidth()I

    move-result v3

    if-ne v3, v8, :cond_a

    .line 171
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->getDensity()F

    move-result v3

    cmpl-float v3, v3, v7

    if-nez v3, :cond_a

    .line 172
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const v4, 0x42146666    # 37.1f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 174
    :cond_a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v3

    const/high16 v4, 0x42200000    # 40.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 181
    :cond_b
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3
.end method
