.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$VideoColumns;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"

# interfaces
.implements Landroid/provider/MediaStore$Video$VideoColumns;
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$MediaColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideoColumns"
.end annotation


# static fields
.field public static final CAPTION_TYPE:Ljava/lang/String; = "caption_type"

.field public static final IS_PLAYED:Ljava/lang/String; = "isPlayed"

.field public static final LOCAL_CAPTION_INDEX_PATH:Ljava/lang/String; = "local_caption_index_path"

.field public static final LOCAL_CAPTION_PATH:Ljava/lang/String; = "local_caption_path"

.field public static final ORIENTATION:Ljava/lang/String; = "orientation"
