.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;
.super Ljava/lang/Object;
.source "SpenPenPluginManager.java"


# instance fields
.field private mPenNumber:I

.field private final mPenPluginInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;)V
    .locals 1
    .param p1, "pluginManager"    # Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    .line 31
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 32
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->initPenPlugin()V

    .line 33
    return-void
.end method

.method private initPenPlugin()V
    .locals 6

    .prologue
    .line 36
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v4, :cond_1

    .line 60
    :cond_0
    return-void

    .line 40
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    const-string v5, "Pen"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 42
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 44
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenNumber:I

    .line 46
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 49
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 51
    .local v3, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;-><init>(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;)V

    .line 52
    .local v2, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    if-eqz v2, :cond_2

    .line 53
    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->setName(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;)V

    .line 55
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenNumber:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenNumber:I

    goto :goto_0
.end method


# virtual methods
.method public getPenCount()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenNumber:I

    return v0
.end method

.method public getPenPluginIndexByPackageName(Ljava/lang/String;)I
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 118
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    move v0, v4

    .line 142
    :goto_0
    return v0

    .line 121
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 123
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    const/4 v0, 0x0

    .line 125
    .local v0, "index":I
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    move v0, v4

    .line 142
    goto :goto_0

    .line 128
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    .line 129
    .local v3, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    if-eqz v3, :cond_1

    .line 130
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 132
    .local v2, "localPenName":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 133
    const/4 v2, 0x0

    .line 134
    goto :goto_0

    .line 136
    :cond_3
    const/4 v2, 0x0

    .line 137
    add-int/lit8 v0, v0, 0x1

    .line 140
    goto :goto_1
.end method

.method public getPenPluginIndexByPenName(Ljava/lang/String;)I
    .locals 6
    .param p1, "penName"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 81
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    move v0, v4

    .line 106
    :goto_0
    return v0

    .line 85
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 87
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    const/4 v0, 0x0

    .line 89
    .local v0, "index":I
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    move v0, v4

    .line 106
    goto :goto_0

    .line 92
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    .line 93
    .local v3, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    if-eqz v3, :cond_1

    .line 94
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 96
    .local v2, "localPenName":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 97
    const/4 v2, 0x0

    .line 98
    goto :goto_0

    .line 100
    :cond_3
    const/4 v2, 0x0

    .line 101
    add-int/lit8 v0, v0, 0x1

    .line 104
    goto :goto_1
.end method

.method public getPenPluginInfoList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public installPenPlugin(Ljava/lang/String;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 219
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-eqz v5, :cond_0

    if-nez p1, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    const-string v6, "Pen"

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getPluginList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 224
    .local v1, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 228
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 231
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    .line 232
    .local v4, "pluginInfo":Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    if-eqz v4, :cond_2

    .line 233
    new-instance v2, Ljava/lang/String;

    iget-object v5, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 234
    .local v2, "localPackageName":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 235
    new-instance v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;-><init>(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;)V

    .line 236
    .local v3, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->setName(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;)V

    .line 238
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenNumber:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenNumber:I

    .line 240
    const/4 v2, 0x0

    .line 241
    goto :goto_0
.end method

.method public loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "penName"    # Ljava/lang/String;

    .prologue
    .line 173
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v4, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 179
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 182
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    .line 183
    .local v3, "penPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    if-eqz v3, :cond_2

    .line 184
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 186
    .local v2, "localPenName":Ljava/lang/String;
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 187
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v4

    if-nez v4, :cond_0

    .line 189
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 190
    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v5

    const-string v6, ""

    .line 189
    invoke-virtual {v4, p1, v5, v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->loadPlugin(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->setPenPluginObject(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 191
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 194
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 197
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 200
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setPluginManager(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;)V
    .locals 0
    .param p1, "pluginManager"    # Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    .line 164
    return-void
.end method

.method public uninstallPenPlugin(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 255
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    if-nez p1, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 261
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 264
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    .line 265
    .local v2, "localPenPluginInfo":Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;
    if-eqz v2, :cond_2

    .line 266
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 267
    .local v1, "localPackageName":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 268
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 269
    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenNumber:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->mPenNumber:I

    .line 270
    const/4 v1, 0x0

    .line 271
    goto :goto_0
.end method
