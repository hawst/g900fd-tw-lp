.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;
.super Ljava/lang/Object;
.source "SpenInView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenInView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnTextActionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    .prologue
    .line 6035
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;)V
    .locals 0

    .prologue
    .line 6035
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onFocusChanged(Z)V
    .locals 1
    .param p1, "gainFocus"    # Z

    .prologue
    .line 6061
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6062
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onFocusChanged(Z)V

    .line 6064
    :cond_0
    return-void
.end method

.method public onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 1
    .param p1, "object"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 6054
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6055
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    .line 6057
    :cond_0
    return-void
.end method

.method public onSelectionChanged(II)Z
    .locals 1
    .param p1, "selStart"    # I
    .param p2, "selEnd"    # I

    .prologue
    .line 6046
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6047
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onSelectionChanged(II)Z

    move-result v0

    .line 6049
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 6039
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6040
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;I)V

    .line 6042
    :cond_0
    return-void
.end method
