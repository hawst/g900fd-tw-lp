.class public Lcom/samsung/android/sdk/pen/util/SpenFont;
.super Ljava/lang/Object;
.source "SpenFont.java"


# static fields
.field private static isInit:Z

.field private static mFontNameMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mFontNames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mFontTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mTypeFaceMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->isInit:Z

    .line 19
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    .line 20
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNames:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "fontNames":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-boolean v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->isInit:Z

    if-nez v0, :cond_0

    .line 31
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 32
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 33
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 35
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 36
    sput-object p2, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNames:Ljava/util/HashMap;

    .line 37
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontTypeList(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->isInit:Z

    .line 40
    :cond_0
    return-void
.end method

.method public static getFontList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getFontName(I)Ljava/lang/String;
    .locals 2
    .param p0, "index"    # I

    .prologue
    .line 57
    new-instance v0, Ljava/util/ArrayList;

    .line 58
    sget-object v1, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 57
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 59
    .local v0, "mTypeFaceMapAccess":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/graphics/Typeface;>;>;"
    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1
.end method

.method public static getFontName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "fontName"    # Ljava/lang/String;

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private getFontTypeList(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x0

    .line 67
    new-instance v8, Ljava/io/File;

    const-string v9, "/system/fonts/"

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    .local v8, "systemFontFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 71
    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 72
    .local v1, "fontFile":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 74
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v9, v1

    if-lt v4, v9, :cond_2

    .line 160
    .end local v1    # "fontFile":[Ljava/io/File;
    .end local v4    # "i":I
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    .line 161
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v10, v13, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 162
    const-string v10, "/fonts/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 160
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 163
    .local v7, "localFontPath":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 165
    .local v6, "localFontFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 167
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 168
    .restart local v1    # "fontFile":[Ljava/io/File;
    if-eqz v1, :cond_1

    .line 170
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    array-length v9, v1

    if-lt v4, v9, :cond_14

    .line 193
    .end local v1    # "fontFile":[Ljava/io/File;
    .end local v4    # "i":I
    :cond_1
    const-string v9, "/system/csc/common/system/fonts/"

    invoke-static {v9}, Lcom/samsung/android/sdk/pen/util/SpenFont;->native_appendFontPath(Ljava/lang/String;)Z

    .line 194
    const-string v9, "/system/fonts/"

    invoke-static {v9}, Lcom/samsung/android/sdk/pen/util/SpenFont;->native_appendFontPath(Ljava/lang/String;)Z

    .line 195
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/util/SpenFont;->native_appendFontPath(Ljava/lang/String;)Z

    .line 196
    return-void

    .line 75
    .end local v6    # "localFontFile":Ljava/io/File;
    .end local v7    # "localFontPath":Ljava/lang/String;
    .restart local v1    # "fontFile":[Ljava/io/File;
    .restart local v4    # "i":I
    :cond_2
    aget-object v0, v1, v4

    .line 76
    .local v0, "file1":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Roboto-Regular.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 77
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Roboto"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "Roboto-Regular"

    const-string v11, "/system/fonts/Roboto-Regular.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "Roboto-Regular"

    const-string v11, "Roboto"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    :cond_3
    :goto_2
    new-instance v3, Ljava/util/ArrayList;

    .line 141
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNames:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    .line 140
    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 142
    .local v3, "fontNameAccess":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_3
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNames:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    if-lt v5, v9, :cond_11

    .line 74
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 80
    .end local v3    # "fontNameAccess":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v5    # "j":I
    :cond_4
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Roboto-Light.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 81
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Roboto Light"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "Roboto-Light"

    const-string v11, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "Roboto-Light"

    const-string v11, "Roboto Light"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 84
    :cond_5
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "RobotoCondensed-Regular.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 85
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Roboto Condensed"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "RobotoCondensed-Regular"

    .line 87
    const-string v11, "/system/fonts/RobotoCondensed-Regular.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    .line 86
    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "RobotoCondensed-Regular"

    const-string v11, "Roboto Condensed"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 89
    :cond_6
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "DroidSerif-Regular.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 90
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Droid Serif"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "DroidSerif-Regular"

    .line 92
    const-string v11, "/system/fonts/DroidSerif-Regular.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    .line 91
    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "DroidSerif-Regular"

    const-string v11, "Droid Serif"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 94
    :cond_7
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "SamsungSans_Regular.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 95
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Samsung Sans"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "SamsungSans_Regular"

    .line 97
    const-string v11, "/system/fonts/SamsungSans_Regular.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    .line 96
    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "SamsungSans_Regular"

    const-string v11, "Samsung Sans"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 99
    :cond_8
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "SamsungSans-Regular.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 100
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Samsung Sans"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "SamsungSans-Regular"

    .line 102
    const-string v11, "/system/fonts/SamsungSans-Regular.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    .line 101
    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "SamsungSans-Regular"

    const-string v11, "Samsung Sans"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 104
    :cond_9
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Arial.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 105
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Arial"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "Arial"

    const-string v11, "/system/fonts/Arial.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "Arial"

    const-string v11, "Arial"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 108
    :cond_a
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Ttahoma.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 109
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Tahoma"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "Ttahoma"

    const-string v11, "/system/fonts/Ttahoma.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "Ttahoma"

    const-string v11, "Tahoma"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 112
    :cond_b
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Verdana.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 113
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Verdana"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "Verdana"

    const-string v11, "/system/fonts/Verdana.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "Verdana"

    const-string v11, "Verdana"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 116
    :cond_c
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Times.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 117
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Times New Roman"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "Times"

    const-string v11, "/system/fonts/Times.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "Times"

    const-string v11, "Times New Roman"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 120
    :cond_d
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "DroidSansGeorgian.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 121
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Droid Sans Georgian"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "DroidSansGeorgian"

    .line 123
    const-string v11, "/system/fonts/DroidSansGeorgian.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    .line 122
    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "DroidSansGeorgian"

    const-string v11, "Droid Sans Georgian"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 125
    :cond_e
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Cour.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 126
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Courier New"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "Cour"

    const-string v11, "/system/fonts/Cour.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "Cour"

    const-string v11, "Courier New"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 129
    :cond_f
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "NotoSerif-Regular.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 130
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Noto Serif"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "NotoSerif-Regular"

    .line 132
    const-string v11, "/system/fonts/NotoSerif-Regular.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    .line 131
    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "NotoSerif-Regular"

    const-string v11, "Noto Serif"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 134
    :cond_10
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Georgia.ttf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 135
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    const-string v10, "Georgia"

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    const-string v10, "Georgia"

    const-string v11, "/system/fonts/Georgia.ttf"

    invoke-static {v11}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    const-string v10, "Georgia"

    const-string v11, "Georgia"

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 143
    .restart local v3    # "fontNameAccess":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v5    # "j":I
    :cond_11
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 144
    sget-object v10, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_12

    .line 145
    sget-object v10, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_12
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x4

    invoke-virtual {v9, v13, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 148
    const-string v10, "-Bold"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "-BoldItalic"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "-Italic"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 149
    const-string v10, " "

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 150
    .local v2, "fontName":Ljava/lang/String;
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v10

    invoke-virtual {v9, v2, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v10, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v2, v9}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 142
    .end local v2    # "fontName":Ljava/lang/String;
    :cond_13
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 171
    .end local v0    # "file1":Ljava/io/File;
    .end local v3    # "fontNameAccess":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v5    # "j":I
    .restart local v6    # "localFontFile":Ljava/io/File;
    .restart local v7    # "localFontPath":Ljava/lang/String;
    :cond_14
    aget-object v0, v1, v4

    .line 173
    .restart local v0    # "file1":Ljava/io/File;
    new-instance v3, Ljava/util/ArrayList;

    .line 174
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNames:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    .line 173
    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 175
    .restart local v3    # "fontNameAccess":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v5, 0x0

    .restart local v5    # "j":I
    :goto_5
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNames:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    if-lt v5, v9, :cond_15

    .line 170
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 176
    :cond_15
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_17

    .line 177
    sget-object v10, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_16

    .line 178
    sget-object v10, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontTypeList:Ljava/util/ArrayList;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_16
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x4

    invoke-virtual {v9, v13, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 181
    const-string v10, "-Bold"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "-BoldItalic"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "-Italic"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 182
    const-string v10, " "

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 183
    .restart local v2    # "fontName":Ljava/lang/String;
    sget-object v9, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v10

    invoke-virtual {v9, v2, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v10, Lcom/samsung/android/sdk/pen/util/SpenFont;->mFontNameMap:Ljava/util/LinkedHashMap;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v2, v9}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 175
    .end local v2    # "fontName":Ljava/lang/String;
    :cond_17
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_5
.end method

.method public static getTypeFace(I)Landroid/graphics/Typeface;
    .locals 2
    .param p0, "index"    # I

    .prologue
    .line 47
    new-instance v0, Ljava/util/ArrayList;

    .line 48
    sget-object v1, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 47
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 49
    .local v0, "mTypeFaceMapAccess":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/graphics/Typeface;>;>;"
    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Typeface;

    return-object v1
.end method

.method public static getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 1
    .param p0, "fontName"    # Ljava/lang/String;

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenFont;->mTypeFaceMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    return-object v0
.end method

.method private static native native_appendFontPath(Ljava/lang/String;)Z
.end method
