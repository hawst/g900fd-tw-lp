.class public Lcom/samsung/android/strokemanager/vo/VOManager;
.super Ljava/lang/Object;
.source "VOManager.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEngine:Lcom/visionobjects/myscript/engine/Engine;

.field private mEngineConfig:Lcom/samsung/android/strokemanager/vo/EngineConfig;

.field private mEnginePath:Ljava/lang/String;

.field private mHelper:Lcom/samsung/android/strokemanager/vo/RMHelper;

.field private mVOAnalyzer:Lcom/samsung/android/strokemanager/vo/VOAnalyzer;

.field private mVOHWR:Lcom/samsung/android/strokemanager/vo/VOHWR;

.field private mVOUDS:Lcom/samsung/android/strokemanager/vo/VOUDS;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/strokemanager/StrokeManager$Switch;)V
    .locals 2
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/samsung/android/strokemanager/StrokeManager$Switch;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOHWR:Lcom/samsung/android/strokemanager/vo/VOHWR;

    .line 38
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOUDS:Lcom/samsung/android/strokemanager/vo/VOUDS;

    .line 40
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOAnalyzer:Lcom/samsung/android/strokemanager/vo/VOAnalyzer;

    .line 42
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEnginePath:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngineConfig:Lcom/samsung/android/strokemanager/vo/EngineConfig;

    .line 52
    iput-object p1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mContext:Landroid/content/Context;

    .line 53
    new-instance v0, Lcom/samsung/android/strokemanager/vo/RMHelper;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/strokemanager/vo/RMHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mHelper:Lcom/samsung/android/strokemanager/vo/RMHelper;

    .line 54
    new-instance v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mHelper:Lcom/samsung/android/strokemanager/vo/RMHelper;

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/strokemanager/vo/EngineConfig;-><init>(Landroid/content/Context;Lcom/samsung/android/strokemanager/vo/RMHelper;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngineConfig:Lcom/samsung/android/strokemanager/vo/EngineConfig;

    .line 56
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mHelper:Lcom/samsung/android/strokemanager/vo/RMHelper;

    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/vo/RMHelper;->getEngineList()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEnginePath:Ljava/lang/String;

    .line 57
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOManager;->enableOTA()V

    .line 59
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->TEXT_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    if-ne p2, v0, :cond_1

    .line 60
    new-instance v0, Lcom/samsung/android/strokemanager/vo/VOHWR;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/strokemanager/vo/VOHWR;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOHWR:Lcom/samsung/android/strokemanager/vo/VOHWR;

    .line 61
    new-instance v0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOAnalyzer:Lcom/samsung/android/strokemanager/vo/VOAnalyzer;

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->GESTURE_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    if-ne p2, v0, :cond_2

    .line 63
    new-instance v0, Lcom/samsung/android/strokemanager/vo/VOUDS;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEnginePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/android/strokemanager/vo/VOUDS;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOUDS:Lcom/samsung/android/strokemanager/vo/VOUDS;

    goto :goto_0

    .line 64
    :cond_2
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->ALL:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    if-ne p2, v0, :cond_0

    .line 65
    new-instance v0, Lcom/samsung/android/strokemanager/vo/VOHWR;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/strokemanager/vo/VOHWR;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOHWR:Lcom/samsung/android/strokemanager/vo/VOHWR;

    .line 66
    new-instance v0, Lcom/samsung/android/strokemanager/vo/VOUDS;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEnginePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/samsung/android/strokemanager/vo/VOUDS;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOUDS:Lcom/samsung/android/strokemanager/vo/VOUDS;

    .line 67
    new-instance v0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOAnalyzer:Lcom/samsung/android/strokemanager/vo/VOAnalyzer;

    goto :goto_0
.end method

.method private destroyEngine()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/engine/Engine;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/engine/Engine;->dispose()V

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    .line 103
    :cond_0
    return-void
.end method

.method private enableOTA()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEnginePath:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/libMyScriptEngine.so"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "libEngine":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isAbsolute()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    const-string v1, "com.visionobjects.myscript.engine.library"

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 84
    return-void

    .line 81
    :cond_0
    const-string v1, "StrokeManager"

    const-string v2, "setProperty Fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "VO engine is not loaded"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private startEngine()Z
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOManager;->destroyEngine()V

    .line 89
    invoke-static {}, Lcom/samsung/android/strokemanager/vo/certification/MyCertificate;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/visionobjects/myscript/engine/Engine;->create([B)Lcom/visionobjects/myscript/engine/Engine;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    .line 91
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    if-nez v0, :cond_0

    .line 92
    const-string v0, "StrokeManager"

    const-string v1, "VOManager Engine creation failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public analyseDocument(Ljava/lang/String;Lcom/samsung/android/strokemanager/data/Stroker;)V
    .locals 6
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "stroker"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOManager;->startEngine()Z

    .line 142
    invoke-virtual {p2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOAnalyzer:Lcom/samsung/android/strokemanager/vo/VOAnalyzer;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mHelper:Lcom/samsung/android/strokemanager/vo/RMHelper;

    iget-object v3, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngineConfig:Lcom/samsung/android/strokemanager/vo/EngineConfig;

    move-object v4, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->analyseDocuments(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;Lcom/samsung/android/strokemanager/vo/EngineConfig;Lcom/samsung/android/strokemanager/data/Stroker;Ljava/lang/String;)Z

    .line 148
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOManager;->destroyEngine()V

    goto :goto_0
.end method

.method public close(Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;)V
    .locals 1
    .param p1, "type"    # Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    .prologue
    .line 130
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    if-ne v0, p1, :cond_1

    .line 131
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOHWR:Lcom/samsung/android/strokemanager/vo/VOHWR;

    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/vo/VOHWR;->close()V

    .line 136
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOManager;->destroyEngine()V

    .line 137
    return-void

    .line 132
    :cond_1
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    if-ne v0, p1, :cond_0

    .line 133
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOUDS:Lcom/samsung/android/strokemanager/vo/VOUDS;

    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/vo/VOUDS;->close()V

    goto :goto_0
.end method

.method public getAvailableLangList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mHelper:Lcom/samsung/android/strokemanager/vo/RMHelper;

    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/vo/RMHelper;->getLangList()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public prepare(Ljava/lang/String;Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;Ljava/util/ArrayList;)Z
    .locals 6
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 106
    .local p3, "userDictionary":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOManager;->startEngine()Z

    .line 108
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    if-ne v0, p2, :cond_0

    .line 109
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOHWR:Lcom/samsung/android/strokemanager/vo/VOHWR;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mHelper:Lcom/samsung/android/strokemanager/vo/RMHelper;

    iget-object v3, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngineConfig:Lcom/samsung/android/strokemanager/vo/EngineConfig;

    move-object v4, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/strokemanager/vo/VOHWR;->prepare(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;Lcom/samsung/android/strokemanager/vo/EngineConfig;Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v0

    .line 113
    :goto_0
    return v0

    .line 110
    :cond_0
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    if-ne v0, p2, :cond_1

    .line 111
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOUDS:Lcom/samsung/android/strokemanager/vo/VOUDS;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mHelper:Lcom/samsung/android/strokemanager/vo/RMHelper;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/strokemanager/vo/VOUDS;->prepare(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;)Z

    move-result v0

    goto :goto_0

    .line 113
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public recognize(Lcom/samsung/android/strokemanager/data/Stroker;Lcom/samsung/android/strokemanager/data/Stroker;Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;)Ljava/util/List;
    .locals 3
    .param p1, "srcStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .param p2, "dstStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .param p3, "type"    # Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            "Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/CandidateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v0, "tmpDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    if-ne v1, p3, :cond_1

    .line 121
    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOHWR:Lcom/samsung/android/strokemanager/vo/VOHWR;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    invoke-virtual {v1, v2, p1, p2}, Lcom/samsung/android/strokemanager/vo/VOHWR;->startRecognition(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/data/Stroker;Lcom/samsung/android/strokemanager/data/Stroker;)Ljava/util/List;

    move-result-object v0

    .line 126
    :cond_0
    :goto_0
    return-object v0

    .line 122
    :cond_1
    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    if-ne v1, p3, :cond_0

    .line 123
    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mVOUDS:Lcom/samsung/android/strokemanager/vo/VOUDS;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOManager;->mEngine:Lcom/visionobjects/myscript/engine/Engine;

    invoke-virtual {v1, v2, p1}, Lcom/samsung/android/strokemanager/vo/VOUDS;->startRecognition(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/data/Stroker;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
