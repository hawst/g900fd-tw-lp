.class public Lcom/samsung/android/strokemanager/control/StrokeControl;
.super Ljava/lang/Object;
.source "StrokeControl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetDegreeFromRadien(D)D
    .locals 2
    .param p0, "angRadien"    # D

    .prologue
    .line 158
    const-wide v0, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr p0, v0

    .line 160
    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    .line 161
    const-wide v0, 0x4076800000000000L    # 360.0

    add-double/2addr p0, v0

    .line 163
    :cond_0
    const-wide v0, 0x4075900000000000L    # 345.0

    cmpl-double v0, p0, v0

    if-ltz v0, :cond_1

    .line 164
    const-wide/high16 p0, 0x3ff0000000000000L    # 1.0

    .line 167
    .end local p0    # "angRadien":D
    :cond_1
    return-wide p0
.end method

.method private static clearSelectedFlag(Lcom/samsung/android/strokemanager/data/Stroker;)V
    .locals 3
    .param p0, "entireStrokes"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 593
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 596
    return-void

    .line 594
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/strokemanager/data/Stroke;->setSelected(Z)V

    .line 593
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static findEdgeRect(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "entireRect":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    const/4 v7, 0x0

    .line 527
    if-eqz p0, :cond_3

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 528
    new-instance v3, Landroid/graphics/PointF;

    invoke-interface {p0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->left:F

    invoke-interface {p0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v3, v6, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 529
    .local v3, "leftBottomEdge":Landroid/graphics/PointF;
    new-instance v4, Landroid/graphics/PointF;

    invoke-interface {p0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->right:F

    invoke-interface {p0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v4, v6, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 532
    .local v4, "rightBottomEdge":Landroid/graphics/PointF;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-lt v1, v5, :cond_0

    .line 546
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 547
    .local v0, "edgeRect":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 548
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    .end local v0    # "edgeRect":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    .end local v1    # "i":I
    .end local v3    # "leftBottomEdge":Landroid/graphics/PointF;
    .end local v4    # "rightBottomEdge":Landroid/graphics/PointF;
    :goto_1
    return-object v0

    .line 533
    .restart local v1    # "i":I
    .restart local v3    # "leftBottomEdge":Landroid/graphics/PointF;
    .restart local v4    # "rightBottomEdge":Landroid/graphics/PointF;
    :cond_0
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 535
    .local v2, "indexRect":Landroid/graphics/RectF;
    iget v5, v2, Landroid/graphics/RectF;->left:F

    iget v6, v3, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_1

    .line 536
    iget v5, v2, Landroid/graphics/RectF;->left:F

    iput v5, v3, Landroid/graphics/PointF;->x:F

    .line 537
    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    iput v5, v3, Landroid/graphics/PointF;->y:F

    .line 540
    :cond_1
    iget v5, v2, Landroid/graphics/RectF;->right:F

    iget v6, v4, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    .line 541
    iget v5, v2, Landroid/graphics/RectF;->right:F

    iput v5, v4, Landroid/graphics/PointF;->x:F

    .line 542
    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    iput v5, v4, Landroid/graphics/PointF;->y:F

    .line 532
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 552
    .end local v1    # "i":I
    .end local v2    # "indexRect":Landroid/graphics/RectF;
    .end local v3    # "leftBottomEdge":Landroid/graphics/PointF;
    .end local v4    # "rightBottomEdge":Landroid/graphics/PointF;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static findStroker(FFLcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/Stroker;
    .locals 9
    .param p0, "x_coordinates"    # F
    .param p1, "y_coordinates"    # F
    .param p2, "baseStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 378
    const-string v6, "StrokeManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "StrokeControl - << findStroker >> x / y "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 379
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 378
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    new-instance v4, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v4}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 382
    .local v4, "resultStrokes":Lcom/samsung/android/strokemanager/data/Stroker;
    invoke-virtual {p2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v3

    .line 383
    .local v3, "nCount":I
    const/4 v0, 0x0

    .line 385
    .local v0, "baseRect":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_1

    .line 410
    :cond_0
    invoke-static {p2}, Lcom/samsung/android/strokemanager/control/StrokeControl;->clearSelectedFlag(Lcom/samsung/android/strokemanager/data/Stroker;)V

    .line 412
    return-object v4

    .line 386
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v6}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v0

    .line 389
    if-eqz v0, :cond_3

    invoke-virtual {v0, p0, p1}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 391
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 392
    .local v5, "tmpRectFList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 395
    invoke-static {p2, v5}, Lcom/samsung/android/strokemanager/control/StrokeControl;->getIntersectRect(Lcom/samsung/android/strokemanager/data/Stroker;Ljava/util/List;)V

    .line 397
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    if-ge v2, v3, :cond_0

    .line 398
    invoke-virtual {p2, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/strokemanager/data/Stroke;->isSelected()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 399
    const-string v6, "StrokeManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "StrokeControl << findStroker >> finding Strokes ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 400
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 399
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    invoke-virtual {p2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v6}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 397
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 385
    .end local v2    # "index":I
    .end local v5    # "tmpRectFList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static findStroker(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/Stroker;
    .locals 7
    .param p0, "baseRect"    # Landroid/graphics/RectF;
    .param p1, "baseStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 353
    new-instance v2, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v2}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 354
    .local v2, "resultStrokes":Lcom/samsung/android/strokemanager/data/Stroker;
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v1

    .line 356
    .local v1, "nCount":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 357
    .local v3, "tmpRectFList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    invoke-interface {v3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    invoke-static {p1, v3}, Lcom/samsung/android/strokemanager/control/StrokeControl;->getIntersectRect(Lcom/samsung/android/strokemanager/data/Stroker;Ljava/util/List;)V

    .line 362
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 372
    invoke-static {p1}, Lcom/samsung/android/strokemanager/control/StrokeControl;->clearSelectedFlag(Lcom/samsung/android/strokemanager/data/Stroker;)V

    .line 374
    return-object v2

    .line 363
    :cond_0
    invoke-virtual {p1, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/strokemanager/data/Stroke;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 364
    const-string v4, "StrokeManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "StrokeControl << findStroker >> finding Strokes ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 365
    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 364
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v4}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 362
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static findStrokers(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/StrokerList;
    .locals 12
    .param p0, "rectF"    # Landroid/graphics/RectF;
    .param p1, "includedStrokes"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 416
    const/4 v4, 0x0

    .line 418
    .local v4, "nOldCount":I
    new-instance v7, Lcom/samsung/android/strokemanager/data/StrokerList;

    invoke-direct {v7}, Lcom/samsung/android/strokemanager/data/StrokerList;-><init>()V

    .line 419
    .local v7, "strokerList":Lcom/samsung/android/strokemanager/data/StrokerList;
    move-object v0, p0

    .line 421
    .local v0, "baseRect":Landroid/graphics/RectF;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 422
    .local v8, "tmpRectFList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 425
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v9

    if-gtz v9, :cond_1

    .line 486
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/strokemanager/control/StrokeControl;->clearSelectedFlag(Lcom/samsung/android/strokemanager/data/Stroker;)V

    .line 488
    return-object v7

    .line 426
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v3

    .line 429
    .local v3, "nIncluded":I
    const-string v9, "StrokeManager"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "StrokeControl << findStrokers >> remain : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    new-instance v5, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v5}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 432
    .local v5, "resultStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    if-eq v4, v3, :cond_0

    .line 435
    move v4, v3

    .line 438
    invoke-static {p1, v8}, Lcom/samsung/android/strokemanager/control/StrokeControl;->getIntersectRect(Lcom/samsung/android/strokemanager/data/Stroker;Ljava/util/List;)V

    .line 440
    add-int/lit8 v2, v3, -0x1

    .local v2, "index":I
    :goto_1
    if-gez v2, :cond_2

    .line 465
    new-instance v6, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v6}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 468
    .local v6, "reverseStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v9

    add-int/lit8 v1, v9, -0x1

    .local v1, "i":I
    :goto_2
    if-gez v1, :cond_7

    .line 474
    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroker;->clear()V

    .line 477
    invoke-virtual {v7}, Lcom/samsung/android/strokemanager/data/StrokerList;->getList()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v6}, Lcom/samsung/android/strokemanager/data/Stroker;->copy()Lcom/samsung/android/strokemanager/data/Stroker;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 478
    invoke-virtual {v6}, Lcom/samsung/android/strokemanager/data/Stroker;->clear()V

    .line 480
    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 481
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 441
    .end local v1    # "i":I
    .end local v6    # "reverseStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    :cond_2
    invoke-virtual {p1, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 442
    invoke-virtual {p1, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->removeStroke(I)V

    .line 440
    :goto_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 446
    :cond_3
    invoke-virtual {p1, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->isDust()Z

    move-result v9

    if-nez v9, :cond_4

    .line 447
    invoke-virtual {p1, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->isGesture()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 448
    :cond_4
    invoke-virtual {p1, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->removeStroke(I)V

    goto :goto_3

    .line 452
    :cond_5
    invoke-virtual {p1, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->isSelected()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 454
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 457
    invoke-virtual {p1, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->removeStroke(I)V

    goto :goto_3

    .line 461
    :cond_6
    invoke-virtual {p1, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_3

    .line 471
    .restart local v1    # "i":I
    .restart local v6    # "reverseStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    :cond_7
    invoke-virtual {v5, v1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 468
    add-int/lit8 v1, v1, -0x1

    goto :goto_2
.end method

.method public static getAverageDegree(Lcom/samsung/android/strokemanager/data/Stroker;)D
    .locals 10
    .param p0, "stroker"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    const-wide/16 v8, 0x0

    .line 106
    new-instance v2, Lcom/samsung/android/strokemanager/data/StrokerList;

    invoke-direct {v2}, Lcom/samsung/android/strokemanager/data/StrokerList;-><init>()V

    .line 107
    .local v2, "strokerList":Lcom/samsung/android/strokemanager/data/StrokerList;
    invoke-static {p0}, Lcom/samsung/android/strokemanager/control/StrokeControl;->getCorrectedStrokers(Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/StrokerList;

    move-result-object v2

    .line 109
    invoke-virtual {v2}, Lcom/samsung/android/strokemanager/data/StrokerList;->getList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    .line 110
    .local v1, "strokerCount":I
    move v3, v1

    .line 111
    .local v3, "tempCount":I
    const-wide/16 v4, 0x0

    .line 113
    .local v4, "tempTotalDegree":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 122
    if-eqz v3, :cond_2

    .line 123
    int-to-double v6, v3

    div-double v6, v4, v6

    .line 125
    :goto_1
    return-wide v6

    .line 115
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/strokemanager/data/StrokerList;->getList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-static {v6}, Lcom/samsung/android/strokemanager/control/StrokeControl;->getSelectedStrokerDegree(Lcom/samsung/android/strokemanager/data/Stroker;)D

    move-result-wide v6

    add-double/2addr v4, v6

    .line 117
    cmpl-double v6, v4, v8

    if-nez v6, :cond_1

    .line 118
    add-int/lit8 v3, v3, -0x1

    .line 113
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-wide v6, v8

    .line 125
    goto :goto_1
.end method

.method public static getClosedCircleStroker(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;Ljava/util/List;)Lcom/samsung/android/strokemanager/data/StrokerList;
    .locals 13
    .param p0, "rectF"    # Landroid/graphics/RectF;
    .param p1, "entireStrokes"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Point;",
            ">;)",
            "Lcom/samsung/android/strokemanager/data/StrokerList;"
        }
    .end annotation

    .prologue
    .local p2, "circlePoints":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Point;>;"
    const/4 v12, 0x0

    .line 272
    const-string v9, "StrokeManager"

    .line 273
    const-string v10, "StrokeControl << getClosedCircleStroker(RectF, Stroker, List<Point>) - Stroker >> "

    .line 272
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    new-instance v7, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v7}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 276
    .local v7, "resultStrokes":Lcom/samsung/android/strokemanager/data/Stroker;
    new-instance v8, Lcom/samsung/android/strokemanager/data/StrokerList;

    invoke-direct {v8}, Lcom/samsung/android/strokemanager/data/StrokerList;-><init>()V

    .line 278
    .local v8, "strokerList":Lcom/samsung/android/strokemanager/data/StrokerList;
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v5

    .line 280
    .local v5, "nCount":I
    invoke-static {p2}, Lcom/samsung/android/strokemanager/control/StrokeControl;->getPointsXY(Ljava/util/List;)[[I

    move-result-object v6

    .line 281
    .local v6, "point":[[I
    const/4 v0, 0x0

    .line 285
    .local v0, "baseRect":Landroid/graphics/RectF;
    new-instance v3, Lcom/samsung/android/strokemanager/control/ClosedCircle;

    aget-object v9, v6, v12

    const/4 v10, 0x1

    aget-object v10, v6, v10

    aget-object v11, v6, v12

    array-length v11, v11

    invoke-direct {v3, v9, v10, v11}, Lcom/samsung/android/strokemanager/control/ClosedCircle;-><init>([I[II)V

    .line 287
    .local v3, "closedCircle":Lcom/samsung/android/strokemanager/control/ClosedCircle;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v5, :cond_1

    .line 305
    invoke-virtual {v7}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 306
    invoke-virtual {v7}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v9

    invoke-static {v9, v7}, Lcom/samsung/android/strokemanager/control/StrokeControl;->findStrokers(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/StrokerList;

    move-result-object v8

    .line 310
    :cond_0
    return-object v8

    .line 288
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v0

    .line 291
    invoke-virtual {p0, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 292
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 293
    .local v1, "centerX":I
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 295
    .local v2, "centerY":I
    invoke-virtual {v3, v1, v2}, Lcom/samsung/android/strokemanager/control/ClosedCircle;->contains(II)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 296
    const-string v9, "StrokeManager"

    .line 297
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "StrokeControl << getClosedCircleStroker >> finding Strokes ["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 298
    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 297
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 296
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 287
    .end local v1    # "centerX":I
    .end local v2    # "centerY":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static getCorrectedStroker(FFLcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/Stroker;
    .locals 2
    .param p0, "x"    # F
    .param p1, "y"    # F
    .param p2, "entireStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 176
    const-string v0, "StrokeManager"

    const-string v1, "StrokeControl << getCorrectedStroker(x,y,Stroker) - Stroker >> "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-static {p0, p1, p2}, Lcom/samsung/android/strokemanager/control/StrokeControl;->findStroker(FFLcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/Stroker;

    move-result-object v0

    return-object v0
.end method

.method public static getCorrectedStroker(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/Stroker;
    .locals 2
    .param p0, "baseRect"    # Landroid/graphics/RectF;
    .param p1, "entireStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 171
    const-string v0, "StrokeManager"

    const-string v1, "StrokeControl << getCorrectedStroker(RectF,Stroker) - Stroker >> "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-static {p0, p1}, Lcom/samsung/android/strokemanager/control/StrokeControl;->findStroker(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/Stroker;

    move-result-object v0

    return-object v0
.end method

.method public static getCorrectedStrokers(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/StrokerList;
    .locals 7
    .param p0, "rectF"    # Landroid/graphics/RectF;
    .param p1, "entireStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 188
    const-string v5, "StrokeManager"

    .line 189
    const-string v6, "StrokeControl << getCorrectedStrokers(rectF,Stroker) - List<Stroker> >> "

    .line 188
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    new-instance v4, Lcom/samsung/android/strokemanager/data/StrokerList;

    invoke-direct {v4}, Lcom/samsung/android/strokemanager/data/StrokerList;-><init>()V

    .line 193
    .local v4, "strokerList":Lcom/samsung/android/strokemanager/data/StrokerList;
    new-instance v2, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v2}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 194
    .local v2, "includedStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v3

    .line 195
    .local v3, "nCount":I
    const/4 v0, 0x0

    .line 197
    .local v0, "baseRect":Landroid/graphics/RectF;
    const-string v5, "StrokeControl << getCorrectedRectStrokes >> Correction"

    invoke-static {v5, p0}, Lcom/samsung/android/strokemanager/control/StrokeControl;->logRectF(Ljava/lang/String;Landroid/graphics/RectF;)V

    .line 200
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_1

    .line 209
    invoke-virtual {v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 210
    invoke-virtual {v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/samsung/android/strokemanager/control/StrokeControl;->findStrokers(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/StrokerList;

    move-result-object v4

    .line 214
    :cond_0
    return-object v4

    .line 201
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v0

    .line 203
    invoke-virtual {p0, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 204
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 200
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getCorrectedStrokers(Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/StrokerList;
    .locals 12
    .param p0, "entireStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 218
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 220
    .local v2, "cs":J
    const-string v7, "StrokeManager"

    const-string v8, "StrokeControl << getCorrectedStrokers(Stroker) - List<Stroker> >> "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    new-instance v6, Lcom/samsung/android/strokemanager/data/StrokerList;

    invoke-direct {v6}, Lcom/samsung/android/strokemanager/data/StrokerList;-><init>()V

    .line 223
    .local v6, "strokerList":Lcom/samsung/android/strokemanager/data/StrokerList;
    new-instance v5, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v5}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 225
    .local v5, "includedStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v7

    if-lt v4, v7, :cond_1

    .line 232
    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 233
    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v7}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/samsung/android/strokemanager/control/StrokeControl;->findStrokers(Landroid/graphics/RectF;Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/StrokerList;

    move-result-object v6

    .line 237
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 238
    .local v0, "ce":J
    const-string v7, "StrokeManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Correct Process Time : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v10, v0, v2

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " milisec"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    return-object v6

    .line 226
    .end local v0    # "ce":J
    :cond_1
    invoke-virtual {p0, v4}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/strokemanager/data/Stroke;->isGesture()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p0, v4}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/strokemanager/data/Stroke;->isDust()Z

    move-result v7

    if-nez v7, :cond_2

    .line 227
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-virtual {v7}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 225
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private static getIntersectRect(Lcom/samsung/android/strokemanager/data/Stroker;Ljava/util/List;)V
    .locals 10
    .param p0, "candidateStrokes"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 314
    .local p1, "standardRects":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 315
    .local v2, "nextDepthRects":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    const/4 v1, 0x0

    .line 317
    .local v1, "nSelectedCount":I
    if-eqz p0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v4

    if-lt v0, v4, :cond_2

    .line 342
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 345
    if-eqz v1, :cond_0

    .line 348
    invoke-static {p0, v2}, Lcom/samsung/android/strokemanager/control/StrokeControl;->getIntersectRect(Lcom/samsung/android/strokemanager/data/Stroker;Ljava/util/List;)V

    goto :goto_0

    .line 322
    :cond_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/strokemanager/data/Stroke;->isSelected()Z

    move-result v4

    if-nez v4, :cond_3

    .line 323
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/strokemanager/data/Stroke;->isDust()Z

    move-result v4

    if-nez v4, :cond_3

    .line 324
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/strokemanager/data/Stroke;->isGesture()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 320
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 327
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 329
    .local v3, "standardRect":Landroid/graphics/RectF;
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v5

    .line 330
    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroke;->getBoundRectF()Landroid/graphics/RectF;

    move-result-object v5

    .line 331
    iget v6, v3, Landroid/graphics/RectF;->left:F

    iget v7, v3, Landroid/graphics/RectF;->top:F

    iget v8, v3, Landroid/graphics/RectF;->right:F

    .line 332
    iget v9, v3, Landroid/graphics/RectF;->bottom:F

    .line 331
    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v5

    .line 332
    if-eqz v5, :cond_5

    .line 333
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroke;->isSelected()Z

    move-result v5

    if-nez v5, :cond_5

    .line 335
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroke;->getBoundRectF()Landroid/graphics/RectF;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/strokemanager/data/Stroke;->setSelected(Z)V

    .line 337
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public static getPointsXY(Ljava/util/List;)[[I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Point;",
            ">;)[[I"
        }
    .end annotation

    .prologue
    .line 244
    .local p0, "p":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Point;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 245
    .local v3, "tmpPoint":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Point;>;"
    const/4 v0, 0x0

    .line 246
    .local v0, "index":I
    const/4 v1, 0x0

    .line 248
    .local v1, "nSamples":I
    const-string v4, "StrokeManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "StrokeControl << getPointsXY >> Original : ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 255
    const-string v4, "StrokeManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "StrokeControl << getPointsXY >> Sample : ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const/4 v4, 0x2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[I

    .line 259
    .local v2, "point":[[I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 265
    return-object v2

    .line 251
    .end local v2    # "point":[[I
    :cond_0
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    add-int/lit8 v1, v1, 0x5

    goto :goto_0

    .line 260
    .restart local v2    # "point":[[I
    :cond_1
    const/4 v4, 0x0

    aget-object v5, v2, v4

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    aput v4, v5, v0

    .line 261
    const/4 v4, 0x1

    aget-object v5, v2, v4

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    aput v4, v5, v0

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static getSelectedStrokerDegree(Lcom/samsung/android/strokemanager/data/Stroker;)D
    .locals 14
    .param p0, "selectedStrokes"    # Lcom/samsung/android/strokemanager/data/Stroker;

    .prologue
    .line 132
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v10, "edge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v11

    const/4 v12, 0x3

    if-ge v11, v12, :cond_0

    .line 135
    const-wide/16 v12, 0x0

    .line 153
    :goto_0
    return-wide v12

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeRects()Ljava/util/List;

    move-result-object v11

    invoke-static {v11}, Lcom/samsung/android/strokemanager/control/StrokeControl;->findEdgeRect(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v10

    .line 139
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 140
    :cond_1
    const-wide/16 v12, 0x0

    goto :goto_0

    .line 143
    :cond_2
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    float-to-double v0, v11

    .line 144
    .local v0, "left":D
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    float-to-double v2, v11

    .line 145
    .local v2, "bottom":D
    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    float-to-double v4, v11

    .line 146
    .local v4, "right":D
    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    float-to-double v6, v11

    .line 148
    .local v6, "top":D
    const-string v12, "StrokeManager"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "StrokeControl Base_l / r : ["

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " , "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 149
    const-string v13, "] / ["

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " , "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, "]"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 148
    invoke-static {v12, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-static/range {v0 .. v7}, Lcom/samsung/android/strokemanager/control/StrokeControl;->xyConverToRadian(DDDD)D

    move-result-wide v8

    .line 153
    .local v8, "angRad":D
    const-wide/high16 v12, -0x4010000000000000L    # -1.0

    mul-double/2addr v12, v8

    invoke-static {v12, v13}, Lcom/samsung/android/strokemanager/control/StrokeControl;->GetDegreeFromRadien(D)D

    move-result-wide v12

    goto/16 :goto_0
.end method

.method private static hasDirection(Ljava/util/List;)Z
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "tmpRectList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    const/4 v4, 0x0

    .line 25
    .local v4, "bDirection":Z
    const/4 v12, 0x0

    .line 26
    .local v12, "nPositive":I
    const/4 v11, 0x0

    .line 28
    .local v11, "nNegative":I
    const-wide/16 v2, 0x0

    .line 29
    .local v2, "angRad":D
    const-wide/16 v6, 0x0

    .line 30
    .local v6, "deltaX":D
    const-wide/16 v8, 0x0

    .line 32
    .local v8, "deltaY":D
    const/4 v10, 0x0

    .line 34
    .local v10, "nMagnigication":F
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-lt v5, v13, :cond_1

    .line 47
    if-eqz v12, :cond_0

    if-nez v11, :cond_4

    .line 48
    :cond_0
    if-le v12, v11, :cond_3

    int-to-float v10, v12

    .line 54
    :goto_1
    const v13, 0x40533333    # 3.3f

    cmpl-float v13, v10, v13

    if-ltz v13, :cond_6

    .line 55
    const/4 v4, 0x1

    .line 56
    const-string v13, "StrokeManager"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "StrokeControl Has Direction / point : ["

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :goto_2
    return v4

    .line 35
    :cond_1
    add-int/lit8 v13, v5, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/RectF;

    invoke-virtual {v13}, Landroid/graphics/RectF;->centerX()F

    move-result v13

    float-to-double v14, v13

    .line 36
    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/RectF;

    invoke-virtual {v13}, Landroid/graphics/RectF;->centerX()F

    move-result v13

    float-to-double v0, v13

    move-wide/from16 v16, v0

    .line 35
    sub-double v6, v14, v16

    .line 37
    add-int/lit8 v13, v5, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/RectF;

    iget v13, v13, Landroid/graphics/RectF;->bottom:F

    float-to-double v14, v13

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/RectF;

    iget v13, v13, Landroid/graphics/RectF;->bottom:F

    float-to-double v0, v13

    move-wide/from16 v16, v0

    sub-double v8, v14, v16

    .line 38
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 40
    const-wide/16 v14, 0x0

    cmpl-double v13, v2, v14

    if-ltz v13, :cond_2

    .line 41
    add-int/lit8 v12, v12, 0x1

    .line 34
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 43
    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 48
    :cond_3
    int-to-float v10, v11

    goto :goto_1

    .line 50
    :cond_4
    if-le v12, v11, :cond_5

    int-to-float v13, v12

    int-to-float v14, v11

    div-float v10, v13, v14

    :goto_4
    goto :goto_1

    .line 51
    :cond_5
    int-to-float v13, v11

    int-to-float v14, v12

    div-float v10, v13, v14

    goto :goto_4

    .line 58
    :cond_6
    const/4 v4, 0x0

    .line 59
    const-string v13, "StrokeManager"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "StrokeControl No Direction ["

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private static logRectF(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "r"    # Landroid/graphics/RectF;

    .prologue
    .line 599
    const-string v0, "StrokeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " (RectF) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 600
    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 599
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    return-void
.end method

.method public static rotateSelectedStroker(Lcom/samsung/android/strokemanager/data/Stroker;Z)Lcom/samsung/android/strokemanager/data/Stroker;
    .locals 17
    .param p0, "selectedStrokes"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .param p1, "bCheckDirectionForAll"    # Z

    .prologue
    .line 68
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v8, "edge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/PointF;>;"
    if-eqz p1, :cond_3

    .line 71
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v9

    const/16 v12, 0x8

    if-lt v9, v12, :cond_0

    .line 72
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeRects()Ljava/util/List;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/strokemanager/control/StrokeControl;->hasDirection(Ljava/util/List;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 101
    .end local p0    # "selectedStrokes":Lcom/samsung/android/strokemanager/data/Stroker;
    :cond_0
    :goto_0
    return-object p0

    .line 75
    .restart local p0    # "selectedStrokes":Lcom/samsung/android/strokemanager/data/Stroker;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeRects()Ljava/util/List;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/strokemanager/control/StrokeControl;->findEdgeRect(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v8

    .line 85
    :goto_1
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 86
    :cond_2
    const/16 p0, 0x0

    goto :goto_0

    .line 78
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v9

    const/4 v12, 0x4

    if-lt v9, v12, :cond_0

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeRects()Ljava/util/List;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/strokemanager/control/StrokeControl;->hasDirection(Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeRects()Ljava/util/List;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/strokemanager/control/StrokeControl;->findEdgeRect(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v8

    goto :goto_1

    .line 89
    :cond_4
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    float-to-double v0, v9

    .line 90
    .local v0, "left":D
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    float-to-double v2, v9

    .line 91
    .local v2, "bottom":D
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    float-to-double v4, v9

    .line 92
    .local v4, "right":D
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    float-to-double v6, v9

    .line 94
    .local v6, "top":D
    const-string v12, "StrokeManager"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v9, "StrokeControl Base_l / r : ["

    invoke-direct {v13, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, " , "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 95
    const-string v13, "] / ["

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, " , "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, "]"

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 94
    invoke-static {v12, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-static/range {v0 .. v7}, Lcom/samsung/android/strokemanager/control/StrokeControl;->xyConverToRadian(DDDD)D

    move-result-wide v10

    .local v10, "angRad":D
    move-object/from16 v9, p0

    move-wide v12, v0

    move-wide v14, v2

    .line 99
    invoke-static/range {v9 .. v15}, Lcom/samsung/android/strokemanager/control/StrokeControl;->rotateStrokes(Lcom/samsung/android/strokemanager/data/Stroker;DDD)Lcom/samsung/android/strokemanager/data/Stroker;

    move-result-object v16

    .local v16, "rotatedStroke":Lcom/samsung/android/strokemanager/data/Stroker;
    move-object/from16 p0, v16

    .line 101
    goto/16 :goto_0
.end method

.method private static rotateStrokes(Lcom/samsung/android/strokemanager/data/Stroker;DDD)Lcom/samsung/android/strokemanager/data/Stroker;
    .locals 23
    .param p0, "originStrokes"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .param p1, "angInRad"    # D
    .param p3, "x0"    # D
    .param p5, "y0"    # D

    .prologue
    .line 493
    new-instance v7, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v7}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 497
    .local v7, "rotatedStrokes":Lcom/samsung/android/strokemanager/data/Stroker;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v16

    move/from16 v0, v16

    if-lt v2, v0, :cond_0

    .line 518
    return-object v7

    .line 498
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v5

    .line 499
    .local v5, "oneStrokePoints":Lcom/samsung/android/strokemanager/data/Stroke;
    new-instance v6, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-direct {v6}, Lcom/samsung/android/strokemanager/data/Stroke;-><init>()V

    .line 501
    .local v6, "rotatedPoints":Lcom/samsung/android/strokemanager/data/Stroke;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v16

    move/from16 v0, v16

    if-lt v3, v0, :cond_1

    .line 515
    invoke-virtual {v6}, Lcom/samsung/android/strokemanager/data/Stroke;->setRectF()V

    .line 516
    invoke-virtual {v7, v6}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 497
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 502
    :cond_1
    invoke-virtual {v5, v3}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v4

    .line 504
    .local v4, "onePoint":Landroid/graphics/PointF;
    iget v0, v4, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    move/from16 v0, v16

    float-to-double v8, v0

    .line 505
    .local v8, "x":D
    iget v0, v4, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    move/from16 v0, v16

    float-to-double v12, v0

    .line 508
    .local v12, "y":D
    sub-double v16, v8, p3

    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v16, v16, p3

    sub-double v18, v12, p5

    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    add-double v10, v16, v18

    .line 510
    .local v10, "x1":D
    sub-double v16, v8, p3

    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    sub-double v16, p5, v16

    sub-double v18, v12, p5

    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    add-double v14, v16, v18

    .line 512
    .local v14, "y1":D
    double-to-float v0, v10

    move/from16 v16, v0

    double-to-float v0, v14

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/strokemanager/data/Stroke;->addPoint(FF)V

    .line 501
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static xyConverToRadian(DDDD)D
    .locals 4
    .param p0, "x0"    # D
    .param p2, "y0"    # D
    .param p4, "x1"    # D
    .param p6, "y1"    # D

    .prologue
    .line 522
    sub-double v0, p6, p2

    sub-double v2, p4, p0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method
