.class public Lcom/samsung/android/strokemanager/Constants$Rect;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/strokemanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Rect"
.end annotation


# static fields
.field public static final INTERSECT_COMPOSITE_OFFSET_X:F = 0.55f

.field public static final INTERSECT_COMPOSITE_OFFSET_Y:F = 0.4f

.field public static final INTERSECT_OFFSET_X:F = 0.33f

.field public static final INTERSECT_OFFSET_Y:F = 0.1f

.field public static final MAX_RECT_HEIGHT:F = 90.0f

.field public static final MAX_RECT_WIDTH:F = 100.0f

.field public static final MIN_RECT_HEIGHT:F = 55.0f

.field public static final MIN_RECT_WIDTH:F = 55.0f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
