.class public final enum Lcom/samsung/android/strokemanager/view/DebugView$DebugType;
.super Ljava/lang/Enum;
.source "DebugView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/strokemanager/view/DebugView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DebugType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/strokemanager/view/DebugView$DebugType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALL:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

.field public static final enum GESTURE:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

.field public static final enum TEXT:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->TEXT:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    new-instance v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    const-string v1, "GESTURE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->GESTURE:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    new-instance v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->ALL:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->TEXT:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->GESTURE:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->ALL:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->ENUM$VALUES:[Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/strokemanager/view/DebugView$DebugType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/strokemanager/view/DebugView$DebugType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->ENUM$VALUES:[Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
