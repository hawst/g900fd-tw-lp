.class public Lcom/samsung/android/strokemanager/util/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyAssets(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 19
    .local v1, "assetManager":Landroid/content/res/AssetManager;
    const/4 v3, 0x0

    .line 21
    .local v3, "files":[Ljava/lang/String;
    :try_start_0
    const-string v8, ""

    invoke-virtual {v1, v8}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 25
    :goto_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v8, v3

    if-lt v4, v8, :cond_0

    .line 51
    return-void

    .line 22
    .end local v4    # "i":I
    :catch_0
    move-exception v2

    .line 23
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v8, "tag"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 26
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v4    # "i":I
    :cond_0
    aget-object v8, v3, v4

    invoke-static {v8}, Lcom/samsung/android/strokemanager/util/FileUtils;->fileCheck(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 25
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 30
    :cond_1
    const/4 v5, 0x0

    .line 31
    .local v5, "in":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 34
    .local v6, "out":Ljava/io/OutputStream;
    :try_start_1
    aget-object v8, v3, v4

    invoke-virtual {v1, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 36
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "S":Ljava/lang/String;
    new-instance v7, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v9, v3, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 40
    .end local v6    # "out":Ljava/io/OutputStream;
    .local v7, "out":Ljava/io/OutputStream;
    :try_start_2
    invoke-static {v5, v7}, Lcom/samsung/android/strokemanager/util/FileUtils;->copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 41
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 42
    const/4 v5, 0x0

    .line 43
    invoke-virtual {v7}, Ljava/io/OutputStream;->flush()V

    .line 44
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 45
    const/4 v6, 0x0

    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 46
    .end local v0    # "S":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 48
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    const-string/jumbo v8, "tag"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 46
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v0    # "S":Ljava/lang/String;
    .restart local v7    # "out":Ljava/io/OutputStream;
    :catch_2
    move-exception v2

    move-object v6, v7

    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v6    # "out":Ljava/io/OutputStream;
    goto :goto_3
.end method

.method private static copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    const/16 v2, 0x400

    new-array v0, v2, [B

    .line 66
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "read":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 69
    return-void

    .line 67
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method private static fileCheck(Ljava/lang/String;)Z
    .locals 1
    .param p0, "assetFileName"    # Ljava/lang/String;

    .prologue
    .line 54
    const-string v0, "notes.res"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    const-string v0, "profile.res"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    const-string v0, "ank-standard.res"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    const/4 v0, 0x1

    .line 59
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isExistSymbolResource(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceFileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 72
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 73
    .local v2, "files":[Ljava/io/File;
    array-length v5, v2

    move v4, v3

    :goto_0
    if-lt v4, v5, :cond_0

    .line 79
    :goto_1
    return v3

    .line 73
    :cond_0
    aget-object v0, v2, v4

    .line 74
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "filename":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 76
    const/4 v3, 0x1

    goto :goto_1

    .line 73
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
