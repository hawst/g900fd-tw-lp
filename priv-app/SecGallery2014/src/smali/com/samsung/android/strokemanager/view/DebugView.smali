.class public Lcom/samsung/android/strokemanager/view/DebugView;
.super Landroid/view/View;
.source "DebugView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/strokemanager/view/DebugView$DebugType;
    }
.end annotation


# instance fields
.field private gestureRect:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private paint:Landroid/graphics/Paint;

.field private path:Landroid/graphics/Path;

.field private textRect:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private totalRect:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 16
    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->totalRect:Ljava/util/List;

    .line 18
    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->textRect:Ljava/util/List;

    .line 20
    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->gestureRect:Ljava/util/List;

    .line 22
    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    .line 24
    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    .line 32
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/view/DebugView;->init()V

    .line 33
    return-void
.end method

.method private drawPath(Landroid/graphics/Canvas;Lcom/samsung/android/strokemanager/view/DebugView$DebugType;Ljava/util/List;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "type"    # Lcom/samsung/android/strokemanager/view/DebugView$DebugType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Lcom/samsung/android/strokemanager/view/DebugView$DebugType;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "rectList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    const/high16 v3, 0x40000000    # 2.0f

    .line 45
    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->ALL:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    if-ne p2, v1, :cond_2

    .line 46
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 56
    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    .line 57
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 68
    :cond_1
    return-void

    .line 48
    :cond_2
    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->TEXT:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    if-ne p2, v1, :cond_3

    .line 49
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0

    .line 51
    :cond_3
    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->GESTURE:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    if-ne p2, v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0

    .line 57
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 58
    .local v0, "rectF":Landroid/graphics/RectF;
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    .line 59
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 60
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 61
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 62
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 63
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 64
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 65
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->path:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private init()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->totalRect:Ljava/util/List;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->textRect:Ljava/util/List;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->gestureRect:Ljava/util/List;

    .line 40
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    .line 41
    iget-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 42
    return-void
.end method


# virtual methods
.method public cleanAll()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->totalRect:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 81
    iget-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->textRect:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/strokemanager/view/DebugView;->gestureRect:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 84
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/view/DebugView;->invalidate()V

    .line 85
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 74
    sget-object v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->ALL:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->totalRect:Ljava/util/List;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/strokemanager/view/DebugView;->drawPath(Landroid/graphics/Canvas;Lcom/samsung/android/strokemanager/view/DebugView$DebugType;Ljava/util/List;)V

    .line 75
    sget-object v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->TEXT:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->textRect:Ljava/util/List;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/strokemanager/view/DebugView;->drawPath(Landroid/graphics/Canvas;Lcom/samsung/android/strokemanager/view/DebugView$DebugType;Ljava/util/List;)V

    .line 76
    sget-object v0, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->GESTURE:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->gestureRect:Ljava/util/List;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/strokemanager/view/DebugView;->drawPath(Landroid/graphics/Canvas;Lcom/samsung/android/strokemanager/view/DebugView$DebugType;Ljava/util/List;)V

    .line 77
    return-void
.end method

.method public showStrokeRect(Ljava/util/List;Lcom/samsung/android/strokemanager/view/DebugView$DebugType;)V
    .locals 3
    .param p2, "type"    # Lcom/samsung/android/strokemanager/view/DebugView$DebugType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "Lcom/samsung/android/strokemanager/view/DebugView$DebugType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "rectList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/samsung/android/strokemanager/view/DebugView;->setVisibility(I)V

    .line 90
    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->ALL:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    if-ne p2, v1, :cond_2

    .line 91
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->totalRect:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 93
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/view/DebugView;->invalidate()V

    .line 114
    return-void

    .line 93
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 94
    .local v0, "r":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->totalRect:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    .end local v0    # "r":Landroid/graphics/RectF;
    :cond_2
    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->TEXT:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    if-ne p2, v1, :cond_3

    .line 98
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->textRect:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 100
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 101
    .restart local v0    # "r":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->textRect:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 104
    .end local v0    # "r":Landroid/graphics/RectF;
    :cond_3
    sget-object v1, Lcom/samsung/android/strokemanager/view/DebugView$DebugType;->GESTURE:Lcom/samsung/android/strokemanager/view/DebugView$DebugType;

    if-ne p2, v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/samsung/android/strokemanager/view/DebugView;->gestureRect:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 107
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 108
    .restart local v0    # "r":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/samsung/android/strokemanager/view/DebugView;->gestureRect:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method
