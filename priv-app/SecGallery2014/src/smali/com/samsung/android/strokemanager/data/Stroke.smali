.class public Lcom/samsung/android/strokemanager/data/Stroke;
.super Ljava/lang/Object;
.source "Stroke.java"


# instance fields
.field private mBoundRectF:Landroid/graphics/RectF;

.field private mDust:Z

.field private mGesture:Z

.field private final mPoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private mRectF:Landroid/graphics/RectF;

.field private mSelected:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    .line 40
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    .line 41
    iput-boolean v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mSelected:Z

    .line 43
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    .line 44
    iput-boolean v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mDust:Z

    .line 45
    iput-boolean v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mGesture:Z

    .line 46
    return-void
.end method


# virtual methods
.method public addPoint(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method

.method public addPoint(Landroid/graphics/PointF;)V
    .locals 4
    .param p1, "p"    # Landroid/graphics/PointF;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    new-instance v1, Landroid/graphics/PointF;

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    .line 99
    iput-boolean v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mSelected:Z

    .line 101
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    .line 102
    iput-boolean v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mDust:Z

    .line 103
    iput-boolean v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mGesture:Z

    .line 104
    return-void
.end method

.method public copy()Lcom/samsung/android/strokemanager/data/Stroke;
    .locals 6

    .prologue
    .line 68
    new-instance v2, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-direct {v2}, Lcom/samsung/android/strokemanager/data/Stroke;-><init>()V

    .line 70
    .local v2, "tmpStroke":Lcom/samsung/android/strokemanager/data/Stroke;
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v1

    .line 71
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 74
    iget-object v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 75
    iget-object v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 76
    iget-object v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 77
    iget-object v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 79
    iget-boolean v3, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mSelected:Z

    iput-boolean v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mSelected:Z

    .line 81
    iget-object v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 82
    iget-object v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 83
    iget-object v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 84
    iget-object v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 86
    iget-boolean v3, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mDust:Z

    iput-boolean v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mDust:Z

    .line 87
    iget-boolean v3, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mGesture:Z

    iput-boolean v3, v2, Lcom/samsung/android/strokemanager/data/Stroke;->mGesture:Z

    .line 89
    return-object v2

    .line 72
    :cond_0
    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/strokemanager/data/Stroke;->addPoint(Landroid/graphics/PointF;)V

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getBottom()F
    .locals 3

    .prologue
    .line 387
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v1, v2, Landroid/graphics/PointF;->y:F

    .line 388
    .local v1, "max":F
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 393
    return v1

    .line 389
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_1

    .line 390
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v1, v2, Landroid/graphics/PointF;->y:F

    .line 388
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getBoundRect()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 238
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 239
    .local v0, "rectI":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 240
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 241
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 242
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 243
    return-object v0
.end method

.method public getBoundRectF()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getIntPointsX()[I
    .locals 3

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    new-array v1, v2, [I

    .line 179
    .local v1, "x":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 181
    return-object v1

    .line 180
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v1, v0

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getIntPointsY()[I
    .locals 3

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    new-array v1, v2, [I

    .line 191
    .local v1, "y":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 193
    return-object v1

    .line 192
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v1, v0

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getLeft()F
    .locals 3

    .prologue
    .line 357
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v1, v2, Landroid/graphics/PointF;->x:F

    .line 358
    .local v1, "min":F
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 363
    return v1

    .line 359
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    .line 360
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v1, v2, Landroid/graphics/PointF;->x:F

    .line 358
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getPoint(I)Landroid/graphics/PointF;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    return-object v0
.end method

.method public getPointCount()I
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    return-object v0
.end method

.method public getPointsToArray()[Landroid/graphics/PointF;
    .locals 6

    .prologue
    .line 135
    iget-object v3, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 136
    .local v1, "nSize":I
    if-gtz v1, :cond_1

    .line 137
    const/4 v2, 0x0

    .line 145
    :cond_0
    return-object v2

    .line 139
    :cond_1
    new-array v2, v1, [Landroid/graphics/PointF;

    .line 141
    .local v2, "tmpPointF":[Landroid/graphics/PointF;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 142
    new-instance v4, Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mPoints:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v2, v0

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getPointsX()[F
    .locals 3

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    new-array v1, v2, [F

    .line 155
    .local v1, "x":[F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 157
    return-object v1

    .line 156
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v1, v0

    .line 155
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getPointsY()[F
    .locals 3

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    new-array v1, v2, [F

    .line 167
    .local v1, "y":[F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 169
    return-object v1

    .line 168
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v1, v0

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 225
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 226
    .local v0, "rectI":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 227
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 228
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 229
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 230
    return-object v0
.end method

.method public getRectF()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getRight()F
    .locals 3

    .prologue
    .line 367
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v1, v2, Landroid/graphics/PointF;->x:F

    .line 368
    .local v1, "max":F
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 373
    return v1

    .line 369
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_1

    .line 370
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v1, v2, Landroid/graphics/PointF;->x:F

    .line 368
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getRoundedPointsX()[F
    .locals 4

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    new-array v0, v2, [F

    .line 198
    .local v0, "fXs":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 201
    return-object v0

    .line 199
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/strokemanager/data/Stroke;->rounded(FF)F

    move-result v2

    aput v2, v0, v1

    .line 198
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getRoundedPointsY()[F
    .locals 4

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    new-array v0, v2, [F

    .line 206
    .local v0, "fYs":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 209
    return-object v0

    .line 207
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/strokemanager/data/Stroke;->rounded(FF)F

    move-result v2

    aput v2, v0, v1

    .line 206
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getTop()F
    .locals 3

    .prologue
    .line 377
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v1, v2, Landroid/graphics/PointF;->y:F

    .line 378
    .local v1, "min":F
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 383
    return v1

    .line 379
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    .line 380
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    iget v1, v2, Landroid/graphics/PointF;->y:F

    .line 378
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public isDust()Z
    .locals 1

    .prologue
    .line 405
    iget-boolean v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mDust:Z

    return v0
.end method

.method public isGesture()Z
    .locals 1

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mGesture:Z

    return v0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 397
    iget-boolean v0, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mSelected:Z

    return v0
.end method

.method public rounded(FF)F
    .locals 6
    .param p1, "number"    # F
    .param p2, "factor"    # F

    .prologue
    .line 213
    float-to-long v2, p1

    .line 214
    .local v2, "integer":J
    long-to-float v4, v2

    sub-float v0, p1, v4

    .line 215
    .local v0, "fraction":F
    div-float v1, v0, p2

    .line 216
    .local v1, "multiple":F
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v1, v4

    .line 217
    mul-float v4, p2, v1

    long-to-float v5, v2

    add-float/2addr v4, v5

    return v4
.end method

.method public setDust(Z)V
    .locals 0
    .param p1, "mDust"    # Z

    .prologue
    .line 409
    iput-boolean p1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mDust:Z

    .line 410
    return-void
.end method

.method public setGesture(Z)V
    .locals 0
    .param p1, "mGesture"    # Z

    .prologue
    .line 417
    iput-boolean p1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mGesture:Z

    .line 418
    return-void
.end method

.method public setRectF()V
    .locals 13

    .prologue
    const/high16 v12, 0x425c0000    # 55.0f

    const/high16 v11, 0x40000000    # 2.0f

    const v10, 0x3ea8f5c3    # 0.33f

    const v9, 0x3dcccccd    # 0.1f

    const/high16 v8, 0x41dc0000    # 27.5f

    .line 291
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getLeft()F

    move-result v1

    .line 292
    .local v1, "Left":F
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getTop()F

    move-result v3

    .line 293
    .local v3, "Top":F
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getRight()F

    move-result v2

    .line 294
    .local v2, "Right":F
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroke;->getBottom()F

    move-result v0

    .line 296
    .local v0, "Bottom":F
    sub-float v6, v2, v1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 297
    .local v5, "width":F
    sub-float v6, v0, v3

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 299
    .local v4, "height":F
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iput v1, v6, Landroid/graphics/RectF;->left:F

    .line 300
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iput v3, v6, Landroid/graphics/RectF;->top:F

    .line 301
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iput v2, v6, Landroid/graphics/RectF;->right:F

    .line 302
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iput v0, v6, Landroid/graphics/RectF;->bottom:F

    .line 304
    cmpg-float v6, v5, v12

    if-gez v6, :cond_2

    .line 305
    iget-object v7, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    cmpl-float v6, v4, v5

    if-lez v6, :cond_0

    div-float v6, v4, v11

    sub-float v6, v1, v6

    :goto_0
    iput v6, v7, Landroid/graphics/RectF;->left:F

    .line 307
    iget-object v7, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    cmpl-float v6, v4, v5

    if-lez v6, :cond_1

    div-float v6, v4, v11

    add-float/2addr v6, v2

    :goto_1
    iput v6, v7, Landroid/graphics/RectF;->right:F

    .line 314
    :goto_2
    cmpg-float v6, v4, v12

    if-gez v6, :cond_3

    .line 315
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    sub-float v7, v3, v8

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 316
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    add-float v7, v0, v8

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    .line 321
    :goto_3
    return-void

    .line 306
    :cond_0
    sub-float v6, v1, v8

    goto :goto_0

    .line 308
    :cond_1
    add-float v6, v2, v8

    goto :goto_1

    .line 310
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v5, v10

    sub-float v7, v1, v7

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 311
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v5, v10

    add-float/2addr v7, v2

    iput v7, v6, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 318
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v4, v9

    sub-float v7, v3, v7

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 319
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v4, v9

    add-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    goto :goto_3
.end method

.method public setRectF(Landroid/graphics/RectF;Z)V
    .locals 13
    .param p1, "r"    # Landroid/graphics/RectF;
    .param p2, "bComposite"    # Z

    .prologue
    const v12, 0x3f0ccccd    # 0.55f

    const v11, 0x3ecccccd    # 0.4f

    const v10, 0x3ea8f5c3    # 0.33f

    const v9, 0x3dcccccd    # 0.1f

    const/high16 v8, 0x41dc0000    # 27.5f

    .line 247
    if-nez p1, :cond_0

    .line 288
    :goto_0
    return-void

    .line 250
    :cond_0
    iget v1, p1, Landroid/graphics/RectF;->left:F

    .line 251
    .local v1, "Left":F
    iget v3, p1, Landroid/graphics/RectF;->top:F

    .line 252
    .local v3, "Top":F
    iget v2, p1, Landroid/graphics/RectF;->right:F

    .line 253
    .local v2, "Right":F
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 255
    .local v0, "Bottom":F
    sub-float v6, v2, v1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 256
    .local v5, "width":F
    sub-float v6, v0, v3

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 258
    .local v4, "height":F
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iput v1, v6, Landroid/graphics/RectF;->left:F

    .line 259
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iput v3, v6, Landroid/graphics/RectF;->top:F

    .line 260
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iput v2, v6, Landroid/graphics/RectF;->right:F

    .line 261
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mRectF:Landroid/graphics/RectF;

    iput v0, v6, Landroid/graphics/RectF;->bottom:F

    .line 263
    const/high16 v6, 0x425c0000    # 55.0f

    cmpg-float v6, v5, v6

    if-gez v6, :cond_3

    .line 264
    iget-object v7, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    cmpl-float v6, v4, v5

    if-lez v6, :cond_1

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v4, v6

    sub-float v6, v1, v6

    :goto_1
    iput v6, v7, Landroid/graphics/RectF;->left:F

    .line 266
    iget-object v7, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    cmpl-float v6, v4, v5

    if-lez v6, :cond_2

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v4, v6

    add-float/2addr v6, v2

    :goto_2
    iput v6, v7, Landroid/graphics/RectF;->right:F

    .line 277
    :goto_3
    const/high16 v6, 0x425c0000    # 55.0f

    cmpg-float v6, v4, v6

    if-gez v6, :cond_5

    .line 278
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    sub-float v7, v3, v8

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 279
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    add-float v7, v0, v8

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 265
    :cond_1
    sub-float v6, v1, v8

    goto :goto_1

    .line 267
    :cond_2
    add-float v6, v2, v8

    goto :goto_2

    .line 268
    :cond_3
    if-eqz p2, :cond_4

    .line 269
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v5, v12

    sub-float v7, v1, v7

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 270
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    .line 271
    mul-float v7, v5, v12

    add-float/2addr v7, v2

    .line 270
    iput v7, v6, Landroid/graphics/RectF;->right:F

    goto :goto_3

    .line 273
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v5, v10

    sub-float v7, v1, v7

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 274
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v5, v10

    add-float/2addr v7, v2

    iput v7, v6, Landroid/graphics/RectF;->right:F

    goto :goto_3

    .line 280
    :cond_5
    if-eqz p2, :cond_6

    .line 281
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v4, v11

    sub-float v7, v3, v7

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 282
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    .line 283
    mul-float v7, v4, v11

    add-float/2addr v7, v0

    .line 282
    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 285
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v4, v9

    sub-float v7, v3, v7

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 286
    iget-object v6, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mBoundRectF:Landroid/graphics/RectF;

    mul-float v7, v4, v9

    add-float/2addr v7, v0

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "mSelected"    # Z

    .prologue
    .line 401
    iput-boolean p1, p0, Lcom/samsung/android/strokemanager/data/Stroke;->mSelected:Z

    .line 402
    return-void
.end method
