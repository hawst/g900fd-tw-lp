.class public Lcom/samsung/android/strokemanager/util/LanguageUtils;
.super Ljava/lang/Object;
.source "LanguageUtils.java"


# static fields
.field private static CHINESE:Ljava/lang/String;

.field private static mLanguage:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 14
    const-string/jumbo v0, "zh"

    sput-object v0, Lcom/samsung/android/strokemanager/util/LanguageUtils;->CHINESE:Ljava/lang/String;

    .line 16
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 17
    const-string/jumbo v2, "zh"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ko"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ja"

    aput-object v2, v0, v1

    .line 16
    sput-object v0, Lcom/samsung/android/strokemanager/util/LanguageUtils;->mLanguage:[Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLanguage(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 22
    const-string v2, "handwriting_language"

    .line 21
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, "lang":Ljava/lang/String;
    const-string v1, "StrokeManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting Language : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    if-nez v0, :cond_0

    .line 26
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Setting Language is not prepared"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 29
    :cond_0
    return-object v0
.end method

.method public static getLocaleString(Lcom/samsung/android/strokemanager/vo/RMHelper;)Ljava/lang/String;
    .locals 5
    .param p0, "helper"    # Lcom/samsung/android/strokemanager/vo/RMHelper;

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "bLocaleIsExist":Z
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 58
    .local v2, "localeCode":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/vo/RMHelper;->getLangList()[Ljava/lang/String;

    move-result-object v3

    .line 60
    .local v3, "localeList":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-lt v1, v4, :cond_1

    .line 67
    :goto_1
    if-nez v0, :cond_0

    .line 68
    const-string v2, "en_GB"

    .line 70
    :cond_0
    return-object v2

    .line 61
    :cond_1
    aget-object v4, v3, v1

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 62
    aget-object v2, v3, v1

    .line 63
    const/4 v0, 0x1

    .line 64
    goto :goto_1

    .line 60
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static isChineseLanguage(Ljava/lang/String;)Z
    .locals 3
    .param p0, "language"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 45
    if-nez p0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    sget-object v1, Lcom/samsung/android/strokemanager/util/LanguageUtils;->CHINESE:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isCompositeLanguage(Ljava/lang/String;)Z
    .locals 4
    .param p0, "language"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 33
    if-nez p0, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/samsung/android/strokemanager/util/LanguageUtils;->mLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 37
    sget-object v2, Lcom/samsung/android/strokemanager/util/LanguageUtils;->mLanguage:[Ljava/lang/String;

    aget-object v2, v2, v0

    const/4 v3, 0x2

    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 38
    const/4 v1, 0x1

    goto :goto_0

    .line 36
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
