.class public Lcom/samsung/android/strokemanager/vo/EngineConfig;
.super Ljava/lang/Object;
.source "EngineConfig.java"


# static fields
.field private static ISO_hi_IN_AK:Ljava/lang/String;

.field private static ISO_hi_IN_LK:Ljava/lang/String;

.field private static ISO_mr_IN_AK:Ljava/lang/String;

.field private static ISO_mr_IN_LK:Ljava/lang/String;

.field private static ISO_ta_IN_AK:Ljava/lang/String;

.field private static ISO_ta_IN_LK:Ljava/lang/String;

.field public static pathMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private AK_CUR_RES:Ljava/lang/String;

.field private CHINESES_GB18030:Ljava/lang/String;

.field private CHINESES_HK:Ljava/lang/String;

.field private CHINESES_HKSCS:Ljava/lang/String;

.field private FOLDER_PATH:Ljava/lang/String;

.field private JAPANESE:Ljava/lang/String;

.field private KOR:Ljava/lang/String;

.field private LATIN_RS:Ljava/lang/String;

.field private LK_TEXT_RES:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "devanagari-ak-iso.res"

    sput-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_hi_IN_AK:Ljava/lang/String;

    .line 49
    const-string v0, "hindi-lk-free.res"

    sput-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_hi_IN_LK:Ljava/lang/String;

    .line 51
    const-string v0, "devanagari-ak-iso.res"

    sput-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_mr_IN_AK:Ljava/lang/String;

    .line 53
    const-string v0, "marathi-lk-free.res"

    sput-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_mr_IN_LK:Ljava/lang/String;

    .line 55
    const-string/jumbo v0, "tamil-ak-iso.res"

    sput-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_ta_IN_AK:Ljava/lang/String;

    .line 57
    const-string/jumbo v0, "tamil-lk-free.res"

    sput-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_ta_IN_LK:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/strokemanager/vo/RMHelper;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "helper"    # Lcom/samsung/android/strokemanager/vo/RMHelper;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "-ak-cur.lite.res"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    .line 24
    const-string v0, "-lk-text.lite.res"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    .line 29
    const-string v0, "_johab"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->KOR:Ljava/lang/String;

    .line 33
    const-string v0, "_gb18030"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_GB18030:Ljava/lang/String;

    .line 35
    const-string v0, "_hkscs"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_HKSCS:Ljava/lang/String;

    .line 37
    const-string v0, "_jisx0213"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->JAPANESE:Ljava/lang/String;

    .line 39
    const-string v0, "_Latin_RS"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LATIN_RS:Ljava/lang/String;

    .line 41
    const-string/jumbo v0, "zh_HK"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_HK:Ljava/lang/String;

    .line 43
    const-string v0, "/system/VODB/"

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->FOLDER_PATH:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    .line 69
    invoke-virtual {p0, p2}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->putLanFolderPath(Lcom/samsung/android/strokemanager/vo/RMHelper;)V

    .line 70
    return-void
.end method

.method private getLanFolderPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 183
    sget-object v1, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 185
    .local v0, "languagePath":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->FOLDER_PATH:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 190
    .end local v0    # "languagePath":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getLanFolderPathForSK(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 200
    sget-object v1, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 202
    .local v0, "languagePath":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->FOLDER_PATH:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 207
    .end local v0    # "languagePath":Ljava/lang/String;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getAkResourceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 79
    const-string/jumbo v0, "zh_HK"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_HK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_GB18030:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_HKSCS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    :goto_0
    return-object v0

    .line 81
    :cond_0
    const-string/jumbo v0, "zh_CN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "zh_TW"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_GB18030:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_2
    const-string v0, "ja_JP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->JAPANESE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_3
    const-string v0, "ko_KR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->KOR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_4
    const-string/jumbo v0, "sr"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LATIN_RS:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 89
    :cond_5
    const-string v0, "es_US"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "es_MX"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 91
    :cond_6
    const-string/jumbo v0, "vi"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "vi_VN"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 93
    :cond_7
    const-string v0, "hi_IN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 94
    sget-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_hi_IN_AK:Ljava/lang/String;

    goto/16 :goto_0

    .line 95
    :cond_8
    const-string v0, "mr_IN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 96
    sget-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_mr_IN_AK:Ljava/lang/String;

    goto/16 :goto_0

    .line 97
    :cond_9
    const-string/jumbo v0, "ta_IN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 98
    sget-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_ta_IN_AK:Ljava/lang/String;

    goto/16 :goto_0

    .line 101
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->AK_CUR_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public getAkResourcePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getLanFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getAkResourceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLkResourceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 111
    const-string/jumbo v0, "zh_HK"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_HK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_GB18030:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_HKSCS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    .line 113
    :cond_0
    const-string/jumbo v0, "zh_CN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "zh_TW"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->CHINESES_GB18030:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 115
    :cond_2
    const-string v0, "ja_JP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->JAPANESE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_3
    const-string v0, "ko_KR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->KOR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 119
    :cond_4
    const-string/jumbo v0, "sr"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LATIN_RS:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 121
    :cond_5
    const-string v0, "es_US"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "es_MX"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 123
    :cond_6
    const-string/jumbo v0, "vi"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "vi_VN"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 125
    :cond_7
    const-string v0, "hi_IN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 126
    sget-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_hi_IN_LK:Ljava/lang/String;

    goto/16 :goto_0

    .line 127
    :cond_8
    const-string v0, "mr_IN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 128
    sget-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_mr_IN_LK:Ljava/lang/String;

    goto/16 :goto_0

    .line 129
    :cond_9
    const-string/jumbo v0, "ta_IN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 130
    sget-object v0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->ISO_ta_IN_LK:Ljava/lang/String;

    goto/16 :goto_0

    .line 133
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/strokemanager/vo/EngineConfig;->LK_TEXT_RES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public getLkResourcePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getLanFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getLkResourceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSkResourceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_sk.res"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSkResourcePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getLanFolderPathForSK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getSkResourceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public putLanFolderPath(Lcom/samsung/android/strokemanager/vo/RMHelper;)V
    .locals 9
    .param p1, "helper"    # Lcom/samsung/android/strokemanager/vo/RMHelper;

    .prologue
    .line 215
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/vo/RMHelper;->getLangPath()Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "helperPath":Ljava/lang/String;
    const-string v6, ":"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 217
    .local v3, "lanPathList":[Ljava/lang/String;
    array-length v4, v3

    .line 219
    .local v4, "numPath":I
    new-array v2, v4, [Ljava/lang/String;

    .line 222
    .local v2, "lanList":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v4, :cond_0

    .line 227
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v4, :cond_1

    .line 340
    return-void

    .line 223
    :cond_0
    aget-object v6, v3, v1

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 224
    .local v5, "temp":[Ljava/lang/String;
    array-length v6, v5

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v5, v6

    aput-object v6, v2, v1

    .line 222
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 228
    .end local v5    # "temp":[Ljava/lang/String;
    :cond_1
    aget-object v6, v2, v1

    const-string v7, "ar"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 229
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "ar"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 230
    :cond_3
    aget-object v6, v2, v1

    const-string v7, "az_AZ"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 231
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "az_AZ"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 232
    :cond_4
    aget-object v6, v2, v1

    const-string v7, "bg_BG"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 233
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "bg_BG"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 234
    :cond_5
    aget-object v6, v2, v1

    const-string v7, "ca_ES"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 235
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "ca_ES"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 236
    :cond_6
    aget-object v6, v2, v1

    const-string v7, "cs_CZ"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 237
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "cs_CZ"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 238
    :cond_7
    aget-object v6, v2, v1

    const-string v7, "da_DK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 239
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "da_DK"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 240
    :cond_8
    aget-object v6, v2, v1

    const-string v7, "de_DE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 241
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "de_DE"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 242
    :cond_9
    aget-object v6, v2, v1

    const-string v7, "el_GR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 243
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "el_GR"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 244
    :cond_a
    aget-object v6, v2, v1

    const-string v7, "en_GB"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 245
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "en_GB"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 246
    :cond_b
    aget-object v6, v2, v1

    const-string v7, "en_US"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 247
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "en_US"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 248
    :cond_c
    aget-object v6, v2, v1

    const-string v7, "es_ES"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 249
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "es_ES"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 250
    :cond_d
    aget-object v6, v2, v1

    const-string v7, "es_US"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 251
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "es_US"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 252
    :cond_e
    aget-object v6, v2, v1

    const-string v7, "et_EE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 253
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "et_EE"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 254
    :cond_f
    aget-object v6, v2, v1

    const-string v7, "eu_ES"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 255
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "eu_ES"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 256
    :cond_10
    aget-object v6, v2, v1

    const-string v7, "fa_IR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 257
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "fa_IR"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 258
    :cond_11
    aget-object v6, v2, v1

    const-string v7, "fi_FI"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 259
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "fi_FI"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 260
    :cond_12
    aget-object v6, v2, v1

    const-string v7, "fr_CA"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 261
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "fr_CA"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 262
    :cond_13
    aget-object v6, v2, v1

    const-string v7, "fr_FR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 263
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "fr_FR"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 264
    :cond_14
    aget-object v6, v2, v1

    const-string v7, "gl_ES"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 265
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "gl_ES"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 266
    :cond_15
    aget-object v6, v2, v1

    const-string v7, "he_IL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 267
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "he_IL"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 268
    :cond_16
    aget-object v6, v2, v1

    const-string v7, "hr_HR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 269
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "hr_HR"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 270
    :cond_17
    aget-object v6, v2, v1

    const-string v7, "hu_HU"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 271
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "hu_HU"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 272
    :cond_18
    aget-object v6, v2, v1

    const-string v7, "hy_AM"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 273
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "hy_AM"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 274
    :cond_19
    aget-object v6, v2, v1

    const-string v7, "id_ID"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 275
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "id_ID"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 276
    :cond_1a
    aget-object v6, v2, v1

    const-string v7, "is_IS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 277
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "is_IS"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 278
    :cond_1b
    aget-object v6, v2, v1

    const-string v7, "it_IT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 279
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "it_IT"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 280
    :cond_1c
    aget-object v6, v2, v1

    const-string v7, "ja_JP"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 281
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "ja_JP"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 282
    :cond_1d
    aget-object v6, v2, v1

    const-string v7, "ka_GE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 283
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "ka_GE"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 284
    :cond_1e
    aget-object v6, v2, v1

    const-string v7, "kk_KZ"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1f

    .line 285
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "kk_KZ"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 286
    :cond_1f
    aget-object v6, v2, v1

    const-string v7, "ko_KR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 287
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "ko_KR"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 288
    :cond_20
    aget-object v6, v2, v1

    const-string v7, "lt_LT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_21

    .line 289
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "lt_LT"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 290
    :cond_21
    aget-object v6, v2, v1

    const-string v7, "lv_LV"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 291
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "lv_LV"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 292
    :cond_22
    aget-object v6, v2, v1

    const-string v7, "ms_MY"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_23

    .line 293
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "ms_MY"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 294
    :cond_23
    aget-object v6, v2, v1

    const-string v7, "nb_NO"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 295
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "nb_NO"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 296
    :cond_24
    aget-object v6, v2, v1

    const-string v7, "nl_NL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_25

    .line 297
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "nl_NL"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 298
    :cond_25
    aget-object v6, v2, v1

    const-string v7, "pl_PL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_26

    .line 299
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "pl_PL"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 300
    :cond_26
    aget-object v6, v2, v1

    const-string v7, "pt_BR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_27

    .line 301
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "pt_BR"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 302
    :cond_27
    aget-object v6, v2, v1

    const-string v7, "pt_PT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_28

    .line 303
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "pt_PT"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 304
    :cond_28
    aget-object v6, v2, v1

    const-string v7, "ro_RO"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_29

    .line 305
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "ro_RO"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 306
    :cond_29
    aget-object v6, v2, v1

    const-string v7, "ru_RU"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2a

    .line 307
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "ru_RU"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 308
    :cond_2a
    aget-object v6, v2, v1

    const-string/jumbo v7, "sk_SK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2b

    .line 309
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "sk_SK"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 310
    :cond_2b
    aget-object v6, v2, v1

    const-string/jumbo v7, "sl_SI"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2c

    .line 311
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "sl_SI"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 312
    :cond_2c
    aget-object v6, v2, v1

    const-string/jumbo v7, "sr_RS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2d

    .line 313
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "sr_RS"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 314
    :cond_2d
    aget-object v6, v2, v1

    const-string/jumbo v7, "sv_SE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2e

    .line 315
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "sv_SE"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 316
    :cond_2e
    aget-object v6, v2, v1

    const-string/jumbo v7, "th_TH"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2f

    .line 317
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "th_TH"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 318
    :cond_2f
    aget-object v6, v2, v1

    const-string/jumbo v7, "tr_TR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_30

    .line 319
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "tr_TR"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 320
    :cond_30
    aget-object v6, v2, v1

    const-string/jumbo v7, "uk_UA"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_31

    .line 321
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "uk_UA"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 322
    :cond_31
    aget-object v6, v2, v1

    const-string/jumbo v7, "ur_PK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_32

    .line 323
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "ur_PK"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 324
    :cond_32
    aget-object v6, v2, v1

    const-string/jumbo v7, "vi_VN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_33

    .line 325
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "vi_VN"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 326
    :cond_33
    aget-object v6, v2, v1

    const-string/jumbo v7, "zh_CN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_34

    .line 327
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "zh_CN"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 328
    :cond_34
    aget-object v6, v2, v1

    const-string/jumbo v7, "zh_HK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_35

    .line 329
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "zh_HK"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 330
    :cond_35
    aget-object v6, v2, v1

    const-string/jumbo v7, "zh_TW"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_36

    .line 331
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "zh_TW"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 332
    :cond_36
    aget-object v6, v2, v1

    const-string v7, "hi_IN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_37

    .line 333
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "hi_IN"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 334
    :cond_37
    aget-object v6, v2, v1

    const-string v7, "mr_IN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_38

    .line 335
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string v7, "mr_IN"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 336
    :cond_38
    aget-object v6, v2, v1

    const-string/jumbo v7, "ta_IN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 337
    sget-object v6, Lcom/samsung/android/strokemanager/vo/EngineConfig;->pathMap:Ljava/util/HashMap;

    const-string/jumbo v7, "ta_IN"

    aget-object v8, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2
.end method
