.class public final enum Lcom/samsung/android/strokemanager/StrokeManager$Switch;
.super Ljava/lang/Enum;
.source "StrokeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/strokemanager/StrokeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Switch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/strokemanager/StrokeManager$Switch;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALL:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/strokemanager/StrokeManager$Switch;

.field public static final enum GESTURE_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

.field public static final enum TEXT_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    const-string v1, "TEXT_ONLY"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/strokemanager/StrokeManager$Switch;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->TEXT_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    new-instance v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    const-string v1, "GESTURE_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/strokemanager/StrokeManager$Switch;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->GESTURE_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    new-instance v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/strokemanager/StrokeManager$Switch;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->ALL:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->TEXT_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->GESTURE_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->ALL:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->ENUM$VALUES:[Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/strokemanager/StrokeManager$Switch;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/strokemanager/StrokeManager$Switch;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->ENUM$VALUES:[Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
