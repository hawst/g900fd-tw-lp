.class public Lcom/samsung/android/strokemanager/vo/VOAnalyzer;
.super Ljava/lang/Object;
.source "VOAnalyzer.java"


# instance fields
.field private mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

.field private mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

.field private mLanguageInfo:Lcom/samsung/android/strokemanager/vo/LanguageInfo;

.field private mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mLanguageInfo:Lcom/samsung/android/strokemanager/vo/LanguageInfo;

    .line 35
    new-instance v0, Lcom/samsung/android/strokemanager/vo/LanguageInfo;

    invoke-direct {v0}, Lcom/samsung/android/strokemanager/vo/LanguageInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mLanguageInfo:Lcom/samsung/android/strokemanager/vo/LanguageInfo;

    .line 36
    return-void
.end method

.method private attachResource(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;)V
    .locals 5
    .param p1, "engine"    # Lcom/visionobjects/myscript/engine/Engine;
    .param p2, "helper"    # Lcom/samsung/android/strokemanager/vo/RMHelper;

    .prologue
    .line 39
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/samsung/android/strokemanager/vo/RMHelper;->getEngineList()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/samsung/android/strokemanager/Constants;->FILE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 40
    const-string v4, "ank-standard.res"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "szAnalyzerKnowledgePath":Ljava/lang/String;
    invoke-static {p1, v2}, Lcom/visionobjects/myscript/engine/EngineObject;->load(Lcom/visionobjects/myscript/engine/Engine;Ljava/lang/String;)Lcom/visionobjects/myscript/engine/EngineObject;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/myscript/analyzer/AnalyzerKnowledge;

    .line 45
    .local v0, "ank":Lcom/visionobjects/myscript/analyzer/AnalyzerKnowledge;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    invoke-virtual {v3, v0}, Lcom/visionobjects/myscript/analyzer/Analyzer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/visionobjects/myscript/analyzer/AnalyzerKnowledge;->isDisposed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 51
    invoke-virtual {v0}, Lcom/visionobjects/myscript/analyzer/AnalyzerKnowledge;->dispose()V

    .line 53
    :cond_1
    return-void

    .line 46
    :catch_0
    move-exception v1

    .line 47
    .local v1, "e":Ljava/lang/Exception;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/visionobjects/myscript/analyzer/AnalyzerKnowledge;->isDisposed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 48
    invoke-virtual {v0}, Lcom/visionobjects/myscript/analyzer/AnalyzerKnowledge;->dispose()V

    goto :goto_0
.end method

.method private attachResourceLang(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;Lcom/samsung/android/strokemanager/vo/EngineConfig;Ljava/lang/String;)Z
    .locals 9
    .param p1, "engine"    # Lcom/visionobjects/myscript/engine/Engine;
    .param p2, "helper"    # Lcom/samsung/android/strokemanager/vo/RMHelper;
    .param p3, "engineConfig"    # Lcom/samsung/android/strokemanager/vo/EngineConfig;
    .param p4, "language"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v7, "StrokeManager"

    const-string v8, "Analyzer << attachResource >>"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const/4 v3, 0x0

    .line 61
    .local v3, "langResource":Ljava/lang/String;
    if-nez p4, :cond_2

    .line 62
    invoke-static {p2}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->getLocaleString(Lcom/samsung/android/strokemanager/vo/RMHelper;)Ljava/lang/String;

    move-result-object v6

    .line 63
    .local v6, "localeCode":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mLanguageInfo:Lcom/samsung/android/strokemanager/vo/LanguageInfo;

    invoke-virtual {v7, v6}, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->getVODBFolder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 68
    .end local v6    # "localeCode":Ljava/lang/String;
    :goto_0
    invoke-virtual {p3, v3}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getAkResourcePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "akPath":Ljava/lang/String;
    invoke-virtual {p3, v3}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getLkResourcePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, "lkPath":Ljava/lang/String;
    :try_start_0
    invoke-static {p1, v1}, Lcom/visionobjects/myscript/engine/EngineObject;->load(Lcom/visionobjects/myscript/engine/Engine;Ljava/lang/String;)Lcom/visionobjects/myscript/engine/EngineObject;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/myscript/hwr/Resource;

    .line 73
    .local v0, "ak":Lcom/visionobjects/myscript/hwr/Resource;
    invoke-static {p1, v5}, Lcom/visionobjects/myscript/engine/EngineObject;->load(Lcom/visionobjects/myscript/engine/Engine;Ljava/lang/String;)Lcom/visionobjects/myscript/engine/EngineObject;

    move-result-object v4

    check-cast v4, Lcom/visionobjects/myscript/hwr/Resource;

    .line 75
    .local v4, "lk":Lcom/visionobjects/myscript/hwr/Resource;
    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v7, v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 76
    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v7, v4}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 78
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/Resource;->isDisposed()Z

    move-result v7

    if-nez v7, :cond_0

    .line 79
    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/Resource;->dispose()V

    .line 81
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/visionobjects/myscript/hwr/Resource;->isDisposed()Z

    move-result v7

    if-nez v7, :cond_1

    .line 82
    invoke-virtual {v4}, Lcom/visionobjects/myscript/hwr/Resource;->dispose()V
    :try_end_0
    .catch Lcom/visionobjects/myscript/engine/IOFailureException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_1
    const/4 v7, 0x1

    .end local v0    # "ak":Lcom/visionobjects/myscript/hwr/Resource;
    .end local v4    # "lk":Lcom/visionobjects/myscript/hwr/Resource;
    :goto_1
    return v7

    .line 65
    .end local v1    # "akPath":Ljava/lang/String;
    .end local v5    # "lkPath":Ljava/lang/String;
    :cond_2
    move-object v3, p4

    goto :goto_0

    .line 85
    .restart local v1    # "akPath":Ljava/lang/String;
    .restart local v5    # "lkPath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 86
    .local v2, "e":Lcom/visionobjects/myscript/engine/IOFailureException;
    invoke-virtual {v2}, Lcom/visionobjects/myscript/engine/IOFailureException;->printStackTrace()V

    .line 88
    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    invoke-virtual {v7}, Lcom/visionobjects/myscript/analyzer/Analyzer;->isDisposed()Z

    move-result v7

    if-nez v7, :cond_3

    .line 89
    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    invoke-virtual {v7}, Lcom/visionobjects/myscript/analyzer/Analyzer;->dispose()V

    .line 92
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v7}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->isDisposed()Z

    move-result v7

    if-nez v7, :cond_4

    .line 93
    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v7}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->dispose()V

    .line 96
    :cond_4
    const-string v7, "StrokeManager"

    const-string v8, "Analyzer << dispose >>"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private dispose()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->dispose()V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;->dispose()V

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/analyzer/Analyzer;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 192
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/analyzer/Analyzer;->dispose()V

    .line 194
    :cond_2
    return-void
.end method


# virtual methods
.method public analyseDocuments(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;Lcom/samsung/android/strokemanager/vo/EngineConfig;Lcom/samsung/android/strokemanager/data/Stroker;Ljava/lang/String;)Z
    .locals 9
    .param p1, "engine"    # Lcom/visionobjects/myscript/engine/Engine;
    .param p2, "helper"    # Lcom/samsung/android/strokemanager/vo/RMHelper;
    .param p3, "engineConfig"    # Lcom/samsung/android/strokemanager/vo/EngineConfig;
    .param p4, "ink"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .param p5, "lang"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 106
    const-string v6, "StrokeManager"

    const-string v7, "Analyzer << analyseDocuments >>"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-static {p1}, Lcom/visionobjects/myscript/analyzer/Analyzer;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/analyzer/Analyzer;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    .line 109
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    if-nez v6, :cond_0

    .line 110
    const-string v5, "StrokeManager"

    const-string v6, "Analyzer analyzer creation failed"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :goto_0
    return v4

    .line 114
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->attachResource(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;)V

    .line 116
    invoke-static {p1}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    .line 117
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    if-nez v6, :cond_1

    .line 118
    const-string v5, "StrokeManager"

    const-string v6, "Analyzer recognizercreation failed"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 122
    :cond_1
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->attachResourceLang(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;Lcom/samsung/android/strokemanager/vo/EngineConfig;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 123
    const-string v5, "StrokeManager"

    const-string v6, "Analyzer attachResourceLang failed"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 127
    :cond_2
    invoke-static {p1}, Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    .line 128
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    if-nez v6, :cond_3

    .line 129
    const-string v5, "StrokeManager"

    const-string v6, "Analyzer analyzer document creation failed"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 134
    :cond_3
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v6, v7}, Lcom/visionobjects/myscript/analyzer/Analyzer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    sget-object v7, Lcom/visionobjects/myscript/analyzer/AnalyzerProcessingLevel;->ALL:Lcom/visionobjects/myscript/analyzer/AnalyzerProcessingLevel;

    invoke-virtual {v6, v7}, Lcom/visionobjects/myscript/analyzer/Analyzer;->setProcessingLevel(Lcom/visionobjects/myscript/analyzer/AnalyzerProcessingLevel;)V

    .line 144
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_1
    invoke-virtual {p4}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    if-lt v1, v6, :cond_4

    .line 154
    :try_start_2
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mAnalyzer:Lcom/visionobjects/myscript/analyzer/Analyzer;

    iget-object v7, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    invoke-virtual {v6, v7}, Lcom/visionobjects/myscript/analyzer/Analyzer;->process(Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;)V
    :try_end_2
    .catch Lcom/visionobjects/myscript/hwr/MissingAlphabetKnowledgeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 160
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    invoke-virtual {v6}, Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;->getStrokeCount()I

    move-result v2

    .line 162
    .local v2, "nDocuCount":I
    if-gtz v2, :cond_5

    .line 163
    const-string v5, "StrokeManager"

    const-string v6, "Analyzer no analysis result"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 135
    .end local v1    # "i":I
    .end local v2    # "nDocuCount":I
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "StrokeManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Analyzer attaching failed"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->dispose()V

    goto :goto_0

    .line 145
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    :cond_4
    :try_start_3
    invoke-virtual {p4, v1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v3

    .line 146
    .local v3, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    invoke-virtual {v3}, Lcom/samsung/android/strokemanager/data/Stroke;->getRoundedPointsX()[F

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/android/strokemanager/data/Stroke;->getRoundedPointsY()[F

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;->addStroke([F[F)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    .line 144
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 148
    .end local v3    # "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    :catch_1
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->dispose()V

    goto/16 :goto_0

    .line 155
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 156
    .local v0, "e":Lcom/visionobjects/myscript/hwr/MissingAlphabetKnowledgeException;
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->dispose()V

    goto/16 :goto_0

    .line 167
    .end local v0    # "e":Lcom/visionobjects/myscript/hwr/MissingAlphabetKnowledgeException;
    .restart local v2    # "nDocuCount":I
    :cond_5
    const/4 v1, 0x0

    :goto_2
    if-lt v1, v2, :cond_6

    .line 174
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->dispose()V

    .line 176
    const-string v4, "StrokeManager"

    const-string v6, "Analyzer << close >>"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 178
    goto/16 :goto_0

    .line 168
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/strokemanager/vo/VOAnalyzer;->mDocument:Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;

    invoke-virtual {v4, v1}, Lcom/visionobjects/myscript/analyzer/AnalyzerDocument;->getStrokeTypeAt(I)Lcom/visionobjects/myscript/analyzer/AnalyzerStrokeType;

    move-result-object v4

    sget-object v6, Lcom/visionobjects/myscript/analyzer/AnalyzerStrokeType;->NON_TEXT:Lcom/visionobjects/myscript/analyzer/AnalyzerStrokeType;

    if-ne v4, v6, :cond_7

    .line 169
    invoke-virtual {p4, v1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/samsung/android/strokemanager/data/Stroke;->setDust(Z)V

    .line 170
    const-string v4, "StrokeManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Analyzer Dust stroke is ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
