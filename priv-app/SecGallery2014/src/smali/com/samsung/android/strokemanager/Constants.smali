.class public interface abstract Lcom/samsung/android/strokemanager/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/strokemanager/Constants$Msg;,
        Lcom/samsung/android/strokemanager/Constants$RecoState;,
        Lcom/samsung/android/strokemanager/Constants$Rect;,
        Lcom/samsung/android/strokemanager/Constants$Util;,
        Lcom/samsung/android/strokemanager/Constants$VO;
    }
.end annotation


# static fields
.field public static final DEBUG_VIEW:Z = false

.field public static final DOT_TOERANCE:F = 10.0f

.field public static final ENGINE_PATH:Ljava/lang/String; = "system/VODB/lib"

.field public static final FILE_SEPARATOR:Ljava/lang/String;

.field public static final ROTATION_COMPOSITE_LIMIT:I = 0x8

.field public static final ROTATION_DIRECTION_POINT:F = 3.3f

.field public static final ROTATION_LATIN_LIMIT:I = 0x4

.field public static final TAG:Ljava/lang/String; = "StrokeManager"

.field public static final TOTAL_PAGE_POINTS:I = 0xc8

.field public static final TOUCH_TOERANCE:F = 2.0f

.field public static final VERSION:Ljava/lang/String; = "1.5.7"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-string v0, "file.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/strokemanager/Constants;->FILE_SEPARATOR:Ljava/lang/String;

    .line 26
    return-void
.end method
