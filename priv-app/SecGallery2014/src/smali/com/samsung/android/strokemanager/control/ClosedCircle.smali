.class public Lcom/samsung/android/strokemanager/control/ClosedCircle;
.super Ljava/lang/Object;
.source "ClosedCircle.java"


# instance fields
.field private edge:I

.field private verTexX:[I

.field private verTexY:[I


# direct methods
.method public constructor <init>([I[II)V
    .locals 0
    .param p1, "x"    # [I
    .param p2, "y"    # [I
    .param p3, "edge"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexX:[I

    .line 12
    iput-object p2, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexY:[I

    .line 13
    iput p3, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->edge:I

    .line 14
    return-void
.end method


# virtual methods
.method public contains(II)Z
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 17
    const/4 v0, 0x0

    .line 18
    .local v0, "compare":Z
    const/4 v3, 0x0

    .line 19
    .local v3, "j":I
    const/4 v1, 0x0

    .local v1, "i":I
    iget v4, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->edge:I

    add-int/lit8 v3, v4, -0x1

    :goto_0
    iget v4, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->edge:I

    if-lt v1, v4, :cond_0

    .line 25
    return v0

    .line 20
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexY:[I

    aget v4, v4, v1

    if-le v4, p2, :cond_2

    move v4, v5

    :goto_1
    iget-object v7, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexY:[I

    aget v7, v7, v3

    if-le v7, p2, :cond_3

    move v7, v5

    :goto_2
    if-eq v4, v7, :cond_1

    .line 21
    iget-object v4, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexX:[I

    aget v4, v4, v3

    iget-object v7, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexX:[I

    aget v7, v7, v1

    sub-int/2addr v4, v7

    iget-object v7, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexY:[I

    aget v7, v7, v1

    sub-int v7, p2, v7

    mul-int/2addr v4, v7

    .line 22
    iget-object v7, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexY:[I

    aget v7, v7, v3

    iget-object v8, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexY:[I

    aget v8, v8, v1

    sub-int/2addr v7, v8

    div-int/2addr v4, v7

    iget-object v7, p0, Lcom/samsung/android/strokemanager/control/ClosedCircle;->verTexX:[I

    aget v7, v7, v1

    add-int/2addr v4, v7

    if-ge p1, v4, :cond_1

    .line 23
    if-eqz v0, :cond_4

    move v0, v6

    .line 19
    :cond_1
    :goto_3
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    move v3, v1

    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    :cond_2
    move v4, v6

    .line 20
    goto :goto_1

    :cond_3
    move v7, v6

    goto :goto_2

    :cond_4
    move v0, v5

    .line 23
    goto :goto_3
.end method
