.class public Lcom/samsung/android/strokemanager/data/Stroker;
.super Ljava/lang/Object;
.source "Stroker.java"


# instance fields
.field private final mStrokeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/Stroke;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    .line 22
    return-void
.end method


# virtual methods
.method public addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V
    .locals 1
    .param p1, "stroke"    # Lcom/samsung/android/strokemanager/data/Stroke;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 190
    return-void
.end method

.method public copy()Lcom/samsung/android/strokemanager/data/Stroker;
    .locals 4

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v1

    .line 26
    .local v1, "n":I
    new-instance v2, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v2}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 28
    .local v2, "tmpStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 32
    return-object v2

    .line 29
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getIndexStrokeX(I)[F
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v0

    .line 98
    .local v0, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointsX()[F

    move-result-object v1

    return-object v1
.end method

.method public getIndexStrokeY(I)[F
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v0

    .line 109
    .local v0, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointsY()[F

    move-result-object v1

    return-object v1
.end method

.method public getPoints(I)Ljava/util/List;
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v0

    .line 136
    .local v0, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getPoints()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getRoundedIntStrokeX(I)[I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v0

    .line 120
    .local v0, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getIntPointsX()[I

    move-result-object v1

    return-object v1
.end method

.method public getRoundedIntStrokeY(I)[I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v0

    .line 131
    .local v0, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/data/Stroke;->getIntPointsY()[I

    move-result-object v1

    return-object v1
.end method

.method public getSizeOfPoints()I
    .locals 4

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 144
    return v0

    .line 141
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/strokemanager/data/Stroke;

    .line 142
    .local v1, "s":Lcom/samsung/android/strokemanager/data/Stroke;
    invoke-virtual {v1}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method public getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/strokemanager/data/Stroke;

    return-object v0
.end method

.method public getStrokeCount()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getStrokeRects()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v1, "rects":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 156
    const/4 v1, 0x0

    .line 162
    .end local v1    # "rects":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    :cond_0
    return-object v1

    .line 159
    .restart local v1    # "rects":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/RectF;>;"
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 160
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getStrokerData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/Stroke;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    return-object v0
.end method

.method public getStrokerRect()Landroid/graphics/RectF;
    .locals 3

    .prologue
    .line 171
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 173
    .local v1, "rectF":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    const/4 v1, 0x0

    .line 182
    .end local v1    # "rectF":Landroid/graphics/RectF;
    :cond_0
    return-object v1

    .line 177
    .restart local v1    # "rectF":Landroid/graphics/RectF;
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 178
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/strokemanager/data/Stroke;->isDust()Z

    move-result v2

    if-nez v2, :cond_2

    .line 179
    invoke-virtual {p0, v0}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 177
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public removeStroke(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 66
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    monitor-enter v1

    .line 67
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 66
    monitor-exit v1

    .line 69
    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V
    .locals 2
    .param p1, "stroke"    # Lcom/samsung/android/strokemanager/data/Stroke;

    .prologue
    .line 60
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    monitor-enter v1

    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/Stroker;->mStrokeList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 60
    monitor-exit v1

    .line 63
    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
