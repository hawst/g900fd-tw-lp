.class public Lcom/samsung/android/strokemanager/vo/RMHelper;
.super Ljava/lang/Object;
.source "RMHelper.java"


# static fields
.field private static final PATH_SEPARATOR:Ljava/lang/String;


# instance fields
.field private final client:Landroid/content/ContentProviderClient;

.field private final cr:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "path.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/strokemanager/vo/RMHelper;->PATH_SEPARATOR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->cr:Landroid/content/ContentResolver;

    .line 32
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->cr:Landroid/content/ContentResolver;

    const-string v1, "com.visionobjects.resourcemanager"

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    .line 33
    return-void
.end method


# virtual methods
.method public getEngineList()Ljava/lang/String;
    .locals 9

    .prologue
    .line 111
    const-string v8, ""

    .line 112
    .local v8, "enginePath":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_0

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerContract$Engine;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 115
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 114
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 116
    .local v6, "c":Landroid/database/Cursor;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 119
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 124
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_1
    return-object v8

    .line 117
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    const-string v0, "path"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_0

    .line 120
    .end local v6    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 121
    .local v7, "e":Landroid/os/RemoteException;
    const-string v0, "StrokeManager"

    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getLangList()[Ljava/lang/String;
    .locals 11

    .prologue
    .line 42
    const/4 v0, 0x0

    new-array v10, v0, [Ljava/lang/String;

    .line 43
    .local v10, "langList":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_0

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerContract$Langs;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 46
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 45
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 47
    .local v6, "c":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v10, v0, [Ljava/lang/String;

    .line 48
    const/4 v8, 0x0

    .line 49
    .local v8, "i":I
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 61
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v8    # "i":I
    :cond_0
    :goto_1
    return-object v10

    .line 51
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v8    # "i":I
    :cond_1
    const-string v0, "lang"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 50
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 52
    .local v9, "lang":Ljava/lang/String;
    aput-object v9, v10, v8
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 57
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v8    # "i":I
    .end local v9    # "lang":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 58
    .local v7, "e":Landroid/os/RemoteException;
    const-string v0, "StrokeManager"

    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getLangPath()Ljava/lang/String;
    .locals 12

    .prologue
    .line 72
    const-string v11, ""

    .line 74
    .local v11, "paths":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/strokemanager/vo/RMHelper;->getLangList()[Ljava/lang/String;

    move-result-object v9

    .line 76
    .local v9, "langList":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_0

    .line 79
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_0
    :try_start_0
    array-length v0, v9

    if-lt v8, v0, :cond_1

    .line 101
    .end local v8    # "j":I
    :cond_0
    :goto_1
    return-object v11

    .line 81
    .restart local v8    # "j":I
    :cond_1
    sget-object v0, Lcom/visionobjects/resourcemanager/ResourceManagerContract$Langs;->CONTENT_URI:Landroid/net/Uri;

    aget-object v2, v9, v8

    .line 80
    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 81
    const-string v2, "langfile"

    .line 80
    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 82
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 83
    .local v6, "c":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 85
    const-string v0, "resource"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 84
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 86
    .local v10, "path":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 88
    const/4 v0, 0x0

    const-string v2, "/"

    invoke-virtual {v10, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v10, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 89
    if-nez v8, :cond_2

    .line 90
    move-object v11, v10

    .line 79
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 92
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/samsung/android/strokemanager/vo/RMHelper;->PATH_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v11

    goto :goto_2

    .line 95
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v10    # "path":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 96
    .local v7, "e":Landroid/os/RemoteException;
    const-string v0, "StrokeManager"

    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 97
    .end local v7    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v7

    .line 98
    .local v7, "e":Landroid/database/CursorIndexOutOfBoundsException;
    const-string v0, "StrokeManager"

    invoke-virtual {v7}, Landroid/database/CursorIndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getResources(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "mode"    # Ljava/lang/String;

    .prologue
    .line 128
    const/4 v10, 0x0

    .line 129
    .local v10, "result":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lcom/visionobjects/resourcemanager/ResourceManagerContract$Langs;->CONTENT_URI:Landroid/net/Uri;

    .line 131
    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 133
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 135
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/RMHelper;->client:Landroid/content/ContentProviderClient;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 139
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v10, v0, [Ljava/lang/String;

    .line 140
    const/4 v8, 0x0

    .line 141
    .local v8, "i":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    if-eqz v6, :cond_0

    .line 146
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 148
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v8    # "i":I
    :cond_0
    return-object v10

    .line 136
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 137
    .local v7, "e":Landroid/os/RemoteException;
    const-string v0, "StrokeManager"

    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 142
    .end local v7    # "e":Landroid/os/RemoteException;
    .restart local v8    # "i":I
    :cond_1
    add-int/lit8 v9, v8, 0x1

    .line 143
    .end local v8    # "i":I
    .local v9, "i":I
    const-string v0, "resource"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 142
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v8

    move v8, v9

    .end local v9    # "i":I
    .restart local v8    # "i":I
    goto :goto_1
.end method
