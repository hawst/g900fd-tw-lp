.class public Lcom/samsung/android/strokemanager/vo/VOHWR;
.super Ljava/lang/Object;
.source "VOHWR.java"


# instance fields
.field private mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

.field private mLanguageInfo:Lcom/samsung/android/strokemanager/vo/LanguageInfo;

.field private mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    .line 35
    new-instance v0, Lcom/samsung/android/strokemanager/vo/LanguageInfo;

    invoke-direct {v0}, Lcom/samsung/android/strokemanager/vo/LanguageInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mLanguageInfo:Lcom/samsung/android/strokemanager/vo/LanguageInfo;

    .line 36
    return-void
.end method

.method private getSegmentCount(Lcom/visionobjects/myscript/hwr/SegmentIterator;)I
    .locals 2
    .param p1, "iterator"    # Lcom/visionobjects/myscript/hwr/SegmentIterator;

    .prologue
    .line 276
    const/4 v0, 0x1

    .line 277
    .local v0, "current":I
    :goto_0
    invoke-virtual {p1}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->isAtEnd()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    add-int/lit8 v1, v0, -0x1

    return v1

    .line 278
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 279
    invoke-virtual {p1}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->next()V

    goto :goto_0
.end method

.method private isSpace(Ljava/lang/String;)Z
    .locals 1
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 264
    const-string v0, "\n"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, " "

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 265
    const-string/jumbo v0, "\u00a0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 250
    const-string v0, "StrokeManager"

    const-string v1, "HWR << close >>"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/engine/FloatStructuredInput;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/engine/FloatStructuredInput;->dispose()V

    .line 254
    iput-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->dispose()V

    .line 259
    iput-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    .line 261
    :cond_1
    return-void
.end method

.method public prepare(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;Lcom/samsung/android/strokemanager/vo/EngineConfig;Ljava/lang/String;Ljava/util/ArrayList;)Z
    .locals 10
    .param p1, "engine"    # Lcom/visionobjects/myscript/engine/Engine;
    .param p2, "helper"    # Lcom/samsung/android/strokemanager/vo/RMHelper;
    .param p3, "engineCongig"    # Lcom/samsung/android/strokemanager/vo/EngineConfig;
    .param p4, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/visionobjects/myscript/engine/Engine;",
            "Lcom/samsung/android/strokemanager/vo/RMHelper;",
            "Lcom/samsung/android/strokemanager/vo/EngineConfig;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p5, "userDictionary":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 57
    const-string v8, "StrokeManager"

    const-string v9, "HWR << prepare >>"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {p1}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    .line 61
    iget-object v8, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    if-nez v8, :cond_1

    .line 62
    const-string v8, "StrokeManager"

    const-string v9, "HWR creation failed"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    :goto_0
    return v7

    .line 67
    :cond_1
    if-eqz p5, :cond_4

    :try_start_0
    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_4

    .line 68
    invoke-virtual {p0, p1, p5}, Lcom/samsung/android/strokemanager/vo/VOHWR;->registerUserDic(Lcom/visionobjects/myscript/engine/Engine;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_1
    const/4 v3, 0x0

    .line 80
    .local v3, "langResource":Ljava/lang/String;
    if-nez p4, :cond_5

    .line 81
    invoke-static {p2}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->getLocaleString(Lcom/samsung/android/strokemanager/vo/RMHelper;)Ljava/lang/String;

    move-result-object v6

    .line 82
    .local v6, "localeCode":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mLanguageInfo:Lcom/samsung/android/strokemanager/vo/LanguageInfo;

    invoke-virtual {v8, v6}, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->getVODBFolder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 87
    .end local v6    # "localeCode":Ljava/lang/String;
    :goto_2
    invoke-virtual {p3, v3}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getAkResourcePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "akPath":Ljava/lang/String;
    invoke-virtual {p3, v3}, Lcom/samsung/android/strokemanager/vo/EngineConfig;->getLkResourcePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 91
    .local v5, "lkPath":Ljava/lang/String;
    :try_start_1
    invoke-static {p1, v1}, Lcom/visionobjects/myscript/engine/EngineObject;->load(Lcom/visionobjects/myscript/engine/Engine;Ljava/lang/String;)Lcom/visionobjects/myscript/engine/EngineObject;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/myscript/hwr/Resource;

    .line 92
    .local v0, "ak":Lcom/visionobjects/myscript/hwr/Resource;
    invoke-static {p1, v5}, Lcom/visionobjects/myscript/engine/EngineObject;->load(Lcom/visionobjects/myscript/engine/Engine;Ljava/lang/String;)Lcom/visionobjects/myscript/engine/EngineObject;

    move-result-object v4

    check-cast v4, Lcom/visionobjects/myscript/hwr/Resource;

    .line 94
    .local v4, "lk":Lcom/visionobjects/myscript/hwr/Resource;
    iget-object v8, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v8, v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 95
    iget-object v8, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v8, v4}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 97
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/Resource;->isDisposed()Z

    move-result v8

    if-nez v8, :cond_2

    .line 98
    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/Resource;->dispose()V

    .line 100
    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/visionobjects/myscript/hwr/Resource;->isDisposed()Z

    move-result v8

    if-nez v8, :cond_3

    .line 101
    invoke-virtual {v4}, Lcom/visionobjects/myscript/hwr/Resource;->dispose()V
    :try_end_1
    .catch Lcom/visionobjects/myscript/engine/IOFailureException; {:try_start_1 .. :try_end_1} :catch_1

    .line 112
    :cond_3
    const/4 v7, 0x1

    goto :goto_0

    .line 70
    .end local v0    # "ak":Lcom/visionobjects/myscript/hwr/Resource;
    .end local v1    # "akPath":Ljava/lang/String;
    .end local v3    # "langResource":Ljava/lang/String;
    .end local v4    # "lk":Lcom/visionobjects/myscript/hwr/Resource;
    .end local v5    # "lkPath":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v8, "StrokeManager"

    const-string v9, "HWR Non userdic"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 72
    :catch_0
    move-exception v2

    .line 75
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 84
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    .restart local v3    # "langResource":Ljava/lang/String;
    :cond_5
    move-object v3, p4

    goto :goto_2

    .line 103
    .restart local v1    # "akPath":Ljava/lang/String;
    .restart local v5    # "lkPath":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 104
    .local v2, "e":Lcom/visionobjects/myscript/engine/IOFailureException;
    invoke-virtual {v2}, Lcom/visionobjects/myscript/engine/IOFailureException;->printStackTrace()V

    .line 106
    iget-object v8, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v8}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->isDisposed()Z

    move-result v8

    if-nez v8, :cond_0

    .line 107
    iget-object v8, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v8}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->dispose()V

    goto :goto_0
.end method

.method public registerUserDic(Lcom/visionobjects/myscript/engine/Engine;Ljava/util/ArrayList;)V
    .locals 9
    .param p1, "engine"    # Lcom/visionobjects/myscript/engine/Engine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/visionobjects/myscript/engine/Engine;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 293
    .local p2, "userDictionary":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/visionobjects/myscript/hwr/Lexicon;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/hwr/Lexicon;

    move-result-object v2

    .line 295
    .local v2, "mLexicon":Lcom/visionobjects/myscript/hwr/Lexicon;
    if-nez v2, :cond_1

    .line 296
    const-string v6, "StrokeManager"

    const-string/jumbo v7, "voLexiconEngine() failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 315
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/Lexicon;->compile()V

    .line 317
    const-string v6, "StrokeManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "User Dic count = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/Lexicon;->getWordCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v6, p0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v6, v2}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 324
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/Lexicon;->isDisposed()Z

    move-result v6

    if-nez v6, :cond_0

    .line 325
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/Lexicon;->dispose()V

    goto :goto_0

    .line 300
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 302
    .local v5, "str":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2, v5}, Lcom/visionobjects/myscript/hwr/Lexicon;->addWord(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/visionobjects/myscript/engine/ModificationAccessDeniedException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    .line 303
    :catch_0
    move-exception v4

    .line 304
    .local v4, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v4}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 305
    .end local v4    # "npe":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 306
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1

    .line 307
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_2
    move-exception v1

    .line 308
    .local v1, "iae":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 309
    .end local v1    # "iae":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v3

    .line 310
    .local v3, "made":Lcom/visionobjects/myscript/engine/ModificationAccessDeniedException;
    invoke-virtual {v3}, Lcom/visionobjects/myscript/engine/ModificationAccessDeniedException;->printStackTrace()V

    goto :goto_1
.end method

.method public startRecognition(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/data/Stroker;Lcom/samsung/android/strokemanager/data/Stroker;)Ljava/util/List;
    .locals 18
    .param p1, "engine"    # Lcom/visionobjects/myscript/engine/Engine;
    .param p2, "srcStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .param p3, "recoStroker"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/visionobjects/myscript/engine/Engine;",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/CandidateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    const-string v15, "StrokeManager"

    const-string v16, "HWR << startRecognition >>"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v7, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    invoke-static/range {p1 .. p1}, Lcom/visionobjects/myscript/engine/FloatStructuredInput;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    .line 130
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    if-nez v15, :cond_1

    .line 131
    const-string v15, "StrokeManager"

    const-string v16, "HWR Structured input creation failed"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const/4 v7, 0x0

    .line 243
    .end local v7    # "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    :cond_0
    :goto_0
    return-object v7

    .line 135
    .restart local v7    # "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/visionobjects/myscript/engine/FloatStructuredInput;->clear(Z)V

    .line 136
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->setSource(Lcom/visionobjects/myscript/engine/IInput;)V

    .line 138
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v15

    if-lt v2, v15, :cond_5

    .line 147
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->getResult()Lcom/visionobjects/myscript/hwr/RecognitionResult;

    move-result-object v6

    .line 154
    .local v6, "result":Lcom/visionobjects/myscript/hwr/RecognitionResult;
    invoke-virtual {v6}, Lcom/visionobjects/myscript/hwr/RecognitionResult;->getCandidates()Lcom/visionobjects/myscript/hwr/CandidateIterator;

    move-result-object v9

    .line 156
    .local v9, "textCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    invoke-virtual {v9}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getSegments()Lcom/visionobjects/myscript/hwr/SegmentIterator;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/strokemanager/vo/VOHWR;->getSegmentCount(Lcom/visionobjects/myscript/hwr/SegmentIterator;)I

    move-result v8

    .line 158
    .local v8, "segmentCount":I
    const/4 v15, 0x1

    if-ne v8, v15, :cond_8

    .line 159
    new-instance v10, Lcom/samsung/android/strokemanager/data/CandidateData;

    invoke-direct {v10}, Lcom/samsung/android/strokemanager/data/CandidateData;-><init>()V

    .line 161
    .local v10, "tmpCandidate":Lcom/samsung/android/strokemanager/data/CandidateData;
    invoke-virtual {v9}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getSegments()Lcom/visionobjects/myscript/hwr/SegmentIterator;

    move-result-object v14

    .line 162
    .local v14, "wordSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->getCandidates()Lcom/visionobjects/myscript/hwr/CandidateIterator;

    move-result-object v13

    .line 165
    .local v13, "wordCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    invoke-virtual {v10}, Lcom/samsung/android/strokemanager/data/CandidateData;->getCandidate()Ljava/util/List;

    move-result-object v15

    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getLabel()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v15, "StrokeManager"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "HWR Candidate : "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getLabel()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokerRect()Landroid/graphics/RectF;

    move-result-object v15

    invoke-virtual {v10, v15}, Lcom/samsung/android/strokemanager/data/CandidateData;->setRectF(Landroid/graphics/RectF;)V

    .line 172
    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    if-eqz v13, :cond_2

    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->isDisposed()Z

    move-result v15

    if-nez v15, :cond_2

    .line 175
    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->dispose()V

    .line 176
    const/4 v13, 0x0

    .line 179
    :cond_2
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->isDisposed()Z

    move-result v15

    if-nez v15, :cond_3

    .line 180
    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->dispose()V

    .line 233
    .end local v10    # "tmpCandidate":Lcom/samsung/android/strokemanager/data/CandidateData;
    .end local v13    # "wordCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    :cond_3
    :goto_2
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->isDisposed()Z

    move-result v15

    if-nez v15, :cond_4

    .line 234
    invoke-virtual {v9}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->dispose()V

    .line 235
    const/4 v9, 0x0

    .line 238
    :cond_4
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/visionobjects/myscript/hwr/RecognitionResult;->isDisposed()Z

    move-result v15

    if-nez v15, :cond_0

    .line 239
    invoke-virtual {v6}, Lcom/visionobjects/myscript/hwr/RecognitionResult;->dispose()V

    .line 240
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 139
    .end local v6    # "result":Lcom/visionobjects/myscript/hwr/RecognitionResult;
    .end local v8    # "segmentCount":I
    .end local v9    # "textCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    .end local v14    # "wordSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    :cond_5
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v15

    invoke-virtual {v15}, Lcom/samsung/android/strokemanager/data/Stroke;->isGesture()Z

    move-result v15

    if-nez v15, :cond_6

    .line 140
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v15

    invoke-virtual {v15}, Lcom/samsung/android/strokemanager/data/Stroke;->isDust()Z

    move-result v15

    if-eqz v15, :cond_7

    .line 138
    :cond_6
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 143
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/strokemanager/vo/VOHWR;->mInput:Lcom/visionobjects/myscript/engine/FloatStructuredInput;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getIndexStrokeX(I)[F

    move-result-object v16

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/android/strokemanager/data/Stroker;->getIndexStrokeY(I)[F

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Lcom/visionobjects/myscript/engine/FloatStructuredInput;->addStroke([F[F)V

    goto :goto_3

    .line 148
    :catch_0
    move-exception v1

    .line 149
    .local v1, "e":Ljava/lang/Exception;
    const-string v15, "StrokeManager"

    const-string v16, "HWR fail mRecognizer.run()"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 185
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v6    # "result":Lcom/visionobjects/myscript/hwr/RecognitionResult;
    .restart local v8    # "segmentCount":I
    .restart local v9    # "textCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    :cond_8
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 186
    .local v12, "tmpStringList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    .line 187
    .local v11, "tmpRect":Landroid/graphics/RectF;
    invoke-virtual {v9}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getSegments()Lcom/visionobjects/myscript/hwr/SegmentIterator;

    move-result-object v14

    .line 189
    .restart local v14    # "wordSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    const/4 v2, 0x0

    :goto_4
    if-lt v2, v8, :cond_9

    .line 227
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->isDisposed()Z

    move-result v15

    if-nez v15, :cond_3

    .line 228
    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->dispose()V

    goto :goto_2

    .line 190
    :cond_9
    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->getCandidates()Lcom/visionobjects/myscript/hwr/CandidateIterator;

    move-result-object v13

    .line 192
    .restart local v13    # "wordCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getLabel()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/strokemanager/vo/VOHWR;->isSpace(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 193
    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->next()V

    .line 189
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 198
    :cond_a
    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getLabel()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v12, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    const-string v15, "StrokeManager"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "HWR Recognizer Candidate : "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getLabel()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->getInputRange()Lcom/visionobjects/myscript/hwr/InputRange;

    move-result-object v15

    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/InputRange;->getElements()[Lcom/visionobjects/myscript/hwr/InputRangeElement;

    move-result-object v15

    const/16 v16, 0x0

    aget-object v15, v15, v16

    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/InputRangeElement;->getFirst()Lcom/visionobjects/myscript/hwr/InputItemLocator;

    move-result-object v15

    .line 205
    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/InputItemLocator;->getComponentIndex()I

    move-result v4

    .line 207
    .local v4, "nFirstIndex":I
    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->getInputRange()Lcom/visionobjects/myscript/hwr/InputRange;

    move-result-object v15

    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/InputRange;->getElements()[Lcom/visionobjects/myscript/hwr/InputRangeElement;

    move-result-object v15

    const/16 v16, 0x0

    aget-object v15, v15, v16

    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/InputRangeElement;->getLast()Lcom/visionobjects/myscript/hwr/InputItemLocator;

    move-result-object v15

    .line 208
    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/InputItemLocator;->getComponentIndex()I

    move-result v5

    .line 210
    .local v5, "nLastIndex":I
    move v3, v4

    .local v3, "index":I
    :goto_6
    if-le v3, v5, :cond_c

    .line 214
    new-instance v15, Lcom/samsung/android/strokemanager/data/CandidateData;

    invoke-direct {v15, v12, v11}, Lcom/samsung/android/strokemanager/data/CandidateData;-><init>(Ljava/util/List;Landroid/graphics/RectF;)V

    invoke-interface {v7, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 217
    invoke-virtual {v11}, Landroid/graphics/RectF;->setEmpty()V

    .line 219
    if-eqz v13, :cond_b

    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->isDisposed()Z

    move-result v15

    if-nez v15, :cond_b

    .line 220
    invoke-virtual {v13}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->dispose()V

    .line 221
    const/4 v13, 0x0

    .line 224
    :cond_b
    invoke-virtual {v14}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->next()V

    goto :goto_5

    .line 211
    :cond_c
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v15

    invoke-virtual {v15}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v15

    invoke-virtual {v11, v15}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 210
    add-int/lit8 v3, v3, 0x1

    goto :goto_6
.end method
