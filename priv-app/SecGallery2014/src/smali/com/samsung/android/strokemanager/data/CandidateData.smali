.class public Lcom/samsung/android/strokemanager/data/CandidateData;
.super Ljava/lang/Object;
.source "CandidateData.java"


# instance fields
.field private candidate:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rectF:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    .line 12
    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    .line 16
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    .line 17
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Landroid/graphics/RectF;)V
    .locals 3
    .param p2, "rectF"    # Landroid/graphics/RectF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/graphics/RectF;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "candidate":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    .line 12
    iput-object v1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    .line 20
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    .line 21
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    .line 23
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 27
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v2, p2, Landroid/graphics/RectF;->left:F

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 28
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 29
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v2, p2, Landroid/graphics/RectF;->right:F

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 30
    iget-object v1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 31
    return-void

    .line 24
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public change(Ljava/util/List;Landroid/graphics/RectF;)V
    .locals 0
    .param p2, "rectF"    # Landroid/graphics/RectF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/graphics/RectF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "candidate":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    .line 35
    iput-object p2, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    .line 36
    return-void
.end method

.method public getCandidate()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    return-object v0
.end method

.method public getCandidateList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v1, "tmpList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 64
    return-object v1

    .line 61
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getRectF()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getRectFString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "rect":Ljava/lang/String;
    return-object v0
.end method

.method public setCandidate(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "candidate":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->candidate:Ljava/util/List;

    .line 48
    return-void
.end method

.method public setRectF(Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "rectF"    # Landroid/graphics/RectF;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 52
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 53
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 54
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/CandidateData;->rectF:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 55
    return-void
.end method
