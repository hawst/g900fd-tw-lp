.class public Lcom/samsung/android/strokemanager/vo/VOUDS;
.super Ljava/lang/Object;
.source "VOUDS.java"


# static fields
.field private static final CODE_POINT_CIRCLE:Ljava/lang/String; = "\udb84\udc00"

.field private static final CODE_POINT_HEART:Ljava/lang/String; = "\udb84\udc01"

.field private static final CODE_POINT_QUESTION:Ljava/lang/String; = "\udb84\udc05"

.field private static final CODE_POINT_SHARP:Ljava/lang/String; = "\udb84\udc06"

.field private static final CODE_POINT_SMILE:Ljava/lang/String; = "\udb84\udc02"

.field private static final CODE_POINT_STAR:Ljava/lang/String; = "\udb84\udc03"

.field private static final FONT_CIRCLE:Ljava/lang/String; = "\uf893"

.field private static final FONT_HEART:Ljava/lang/String; = "\uf894"

.field private static final FONT_QUESTION:Ljava/lang/String; = "?"

.field private static final FONT_SHARP:Ljava/lang/String; = "#"

.field private static final FONT_SMILE:Ljava/lang/String; = "\uf895"

.field private static final FONT_STAR:Ljava/lang/String; = "\uf892"


# instance fields
.field private mInput:Lcom/visionobjects/myscript/engine/IntStructuredInput;

.field private mProfile:Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

.field private mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

.field private mUserSymbolCollector:Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

.field private mlibPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "libPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    .line 29
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mInput:Lcom/visionobjects/myscript/engine/IntStructuredInput;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mProfile:Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mUserSymbolCollector:Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

    .line 63
    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mlibPath:Ljava/lang/String;

    .line 66
    iput-object p1, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mlibPath:Ljava/lang/String;

    .line 67
    return-void
.end method

.method private convertCodePointToFont(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "recoResult"    # Ljava/lang/String;

    .prologue
    .line 214
    const-string/jumbo v0, "\udb84\udc03"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-string/jumbo v0, "\uf892"

    .line 227
    :goto_0
    return-object v0

    .line 216
    :cond_0
    const-string/jumbo v0, "\udb84\udc00"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    const-string/jumbo v0, "\uf893"

    goto :goto_0

    .line 218
    :cond_1
    const-string/jumbo v0, "\udb84\udc01"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    const-string/jumbo v0, "\uf894"

    goto :goto_0

    .line 220
    :cond_2
    const-string/jumbo v0, "\udb84\udc02"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 221
    const-string/jumbo v0, "\uf895"

    goto :goto_0

    .line 222
    :cond_3
    const-string/jumbo v0, "\udb84\udc05"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 223
    const-string v0, "?"

    goto :goto_0

    .line 224
    :cond_4
    const-string/jumbo v0, "\udb84\udc06"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 225
    const-string v0, "#"

    goto :goto_0

    .line 227
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private convertStringToHex(Ljava/lang/String;)V
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 232
    if-nez p1, :cond_0

    .line 243
    :goto_0
    return-void

    .line 235
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 236
    .local v0, "chars":[C
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 238
    .local v1, "hex":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    .line 242
    const-string v3, "StrokeManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UDS [ Value ] : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 239
    :cond_1
    aget-char v3, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 238
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private isNotUsedChar(Ljava/lang/String;)Z
    .locals 1
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 209
    const-string/jumbo v0, "\ufffd"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\n"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    const-string v0, " "

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "\u00a0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "\udb84\udc04"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 246
    const-string v0, "StrokeManager"

    const-string v1, "UDS << close >>"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mInput:Lcom/visionobjects/myscript/engine/IntStructuredInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mInput:Lcom/visionobjects/myscript/engine/IntStructuredInput;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/engine/IntStructuredInput;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mInput:Lcom/visionobjects/myscript/engine/IntStructuredInput;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/engine/IntStructuredInput;->dispose()V

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mProfile:Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mProfile:Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mProfile:Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;->dispose()V

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mUserSymbolCollector:Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mUserSymbolCollector:Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mUserSymbolCollector:Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;->dispose()V

    .line 260
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 261
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->dispose()V

    .line 263
    :cond_3
    return-void
.end method

.method public prepare(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/vo/RMHelper;)Z
    .locals 4
    .param p1, "engine"    # Lcom/visionobjects/myscript/engine/Engine;
    .param p2, "helper"    # Lcom/samsung/android/strokemanager/vo/RMHelper;

    .prologue
    .line 74
    const-string v2, "StrokeManager"

    const-string v3, "UDS << prepare >>"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-static {p1}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    .line 80
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mlibPath:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 81
    sget-object v3, Lcom/samsung/android/strokemanager/Constants;->FILE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "notes.res"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-static {p1, v2}, Lcom/visionobjects/myscript/engine/EngineObject;->load(Lcom/visionobjects/myscript/engine/Engine;Ljava/lang/String;)Lcom/visionobjects/myscript/engine/EngineObject;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/myscript/hwr/Resource;

    .line 82
    .local v0, "ak":Lcom/visionobjects/myscript/hwr/Resource;
    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    invoke-virtual {v2, v0}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mlibPath:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 86
    sget-object v3, Lcom/samsung/android/strokemanager/Constants;->FILE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "profile.res"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-static {p1, v2}, Lcom/visionobjects/myscript/engine/EngineObject;->load(Lcom/visionobjects/myscript/engine/Engine;Ljava/lang/String;)Lcom/visionobjects/myscript/engine/EngineObject;

    move-result-object v2

    check-cast v2, Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

    iput-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mProfile:Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

    .line 87
    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    iget-object v3, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mProfile:Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

    invoke-virtual {v2, v3}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 89
    invoke-static {p1}, Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mUserSymbolCollector:Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

    .line 90
    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mUserSymbolCollector:Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

    invoke-virtual {v2, v0}, Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 91
    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mUserSymbolCollector:Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;

    iget-object v3, p0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mProfile:Lcom/visionobjects/myscript/writeradaptation/WriterAdaptationProfile;

    invoke-virtual {v2, v3}, Lcom/visionobjects/myscript/usersymbols/UserSymbolCollector;->attach(Lcom/visionobjects/myscript/engine/EngineObject;)V

    .line 93
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/Resource;->isDisposed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    invoke-virtual {v0}, Lcom/visionobjects/myscript/hwr/Resource;->dispose()V
    :try_end_0
    .catch Lcom/visionobjects/myscript/engine/IOFailureException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :cond_0
    const/4 v2, 0x1

    .end local v0    # "ak":Lcom/visionobjects/myscript/hwr/Resource;
    :goto_0
    return v2

    .line 97
    :catch_0
    move-exception v1

    .line 98
    .local v1, "e":Lcom/visionobjects/myscript/engine/IOFailureException;
    invoke-virtual {v1}, Lcom/visionobjects/myscript/engine/IOFailureException;->printStackTrace()V

    .line 99
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public startRecognition(Lcom/visionobjects/myscript/engine/Engine;Lcom/samsung/android/strokemanager/data/Stroker;)Ljava/util/List;
    .locals 20
    .param p1, "engine"    # Lcom/visionobjects/myscript/engine/Engine;
    .param p2, "stroker"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/visionobjects/myscript/engine/Engine;",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/CandidateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    const-string v17, "StrokeManager"

    const-string v18, "UDS << startRecognition >>"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v11, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v14, "tmpStringList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 112
    .local v13, "tmpRect":Landroid/graphics/RectF;
    invoke-static/range {p1 .. p1}, Lcom/visionobjects/myscript/engine/IntStructuredInput;->create(Lcom/visionobjects/myscript/engine/Engine;)Lcom/visionobjects/myscript/engine/IntStructuredInput;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/strokemanager/vo/VOUDS;->mInput:Lcom/visionobjects/myscript/engine/IntStructuredInput;

    .line 115
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    move/from16 v0, v17

    if-lt v5, v0, :cond_5

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mInput:Lcom/visionobjects/myscript/engine/IntStructuredInput;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->setSource(Lcom/visionobjects/myscript/engine/IInput;)V

    .line 126
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->run()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mRecognizer:Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/StructuredInputRecognizer;->getResult()Lcom/visionobjects/myscript/hwr/RecognitionResult;

    move-result-object v10

    .line 134
    .local v10, "result":Lcom/visionobjects/myscript/hwr/RecognitionResult;
    invoke-virtual {v10}, Lcom/visionobjects/myscript/hwr/RecognitionResult;->getCandidates()Lcom/visionobjects/myscript/hwr/CandidateIterator;

    move-result-object v12

    .line 138
    .local v12, "textCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    invoke-virtual {v12}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getSegments()Lcom/visionobjects/myscript/hwr/SegmentIterator;

    move-result-object v16

    .line 139
    .local v16, "wordSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    invoke-virtual/range {v16 .. v16}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->getCandidates()Lcom/visionobjects/myscript/hwr/CandidateIterator;

    move-result-object v15

    .line 140
    .local v15, "wordCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    const-string v17, "StrokeManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "UDS wordCandidates : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getLabel()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getSegments()Lcom/visionobjects/myscript/hwr/SegmentIterator;

    move-result-object v2

    .line 145
    .local v2, "charSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    :goto_1
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->isAtEnd()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 185
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->isDisposed()Z

    move-result v17

    if-nez v17, :cond_0

    .line 186
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->dispose()V

    .line 189
    :cond_0
    if-eqz v15, :cond_1

    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->isDisposed()Z

    move-result v17

    if-nez v17, :cond_1

    .line 190
    invoke-virtual {v15}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->dispose()V

    .line 193
    :cond_1
    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->isDisposed()Z

    move-result v17

    if-nez v17, :cond_2

    .line 194
    invoke-virtual/range {v16 .. v16}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->dispose()V

    .line 197
    :cond_2
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->isDisposed()Z

    move-result v17

    if-nez v17, :cond_3

    .line 198
    invoke-virtual {v12}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->dispose()V

    .line 201
    :cond_3
    if-eqz v10, :cond_4

    invoke-virtual {v10}, Lcom/visionobjects/myscript/hwr/RecognitionResult;->isDisposed()Z

    move-result v17

    if-nez v17, :cond_4

    .line 202
    invoke-virtual {v10}, Lcom/visionobjects/myscript/hwr/RecognitionResult;->dispose()V

    .line 205
    .end local v2    # "charSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    .end local v10    # "result":Lcom/visionobjects/myscript/hwr/RecognitionResult;
    .end local v11    # "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    .end local v12    # "textCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    .end local v15    # "wordCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    .end local v16    # "wordSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    :cond_4
    :goto_2
    return-object v11

    .line 116
    .restart local v11    # "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/strokemanager/vo/VOUDS;->mInput:Lcom/visionobjects/myscript/engine/IntStructuredInput;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/samsung/android/strokemanager/data/Stroker;->getRoundedIntStrokeX(I)[I

    move-result-object v18

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/samsung/android/strokemanager/data/Stroker;->getRoundedIntStrokeY(I)[I

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lcom/visionobjects/myscript/engine/IntStructuredInput;->addStroke([I[I)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    .line 115
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 118
    :catch_0
    move-exception v3

    .line 119
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 120
    const/4 v11, 0x0

    goto :goto_2

    .line 127
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v3

    .line 128
    .local v3, "e":Ljava/lang/Exception;
    const-string v17, "StrokeManager"

    const-string v18, "VOUDS fail mRecognizer.run()"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v11, 0x0

    goto :goto_2

    .line 146
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v2    # "charSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    .restart local v10    # "result":Lcom/visionobjects/myscript/hwr/RecognitionResult;
    .restart local v12    # "textCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    .restart local v15    # "wordCandidates":Lcom/visionobjects/myscript/hwr/CandidateIterator;
    .restart local v16    # "wordSegments":Lcom/visionobjects/myscript/hwr/SegmentIterator;
    :cond_6
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->getCandidates()Lcom/visionobjects/myscript/hwr/CandidateIterator;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/CandidateIterator;->getLabel()Ljava/lang/String;

    move-result-object v9

    .line 148
    .local v9, "recoResult":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/strokemanager/vo/VOUDS;->isNotUsedChar(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 149
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->next()V

    goto :goto_1

    .line 154
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/strokemanager/vo/VOUDS;->convertCodePointToFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 156
    .local v4, "font":Ljava/lang/String;
    if-nez v4, :cond_8

    .line 157
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->next()V

    goto/16 :goto_1

    .line 161
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/strokemanager/vo/VOUDS;->convertStringToHex(Ljava/lang/String;)V

    .line 164
    invoke-interface {v14, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->getInputRange()Lcom/visionobjects/myscript/hwr/InputRange;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/InputRange;->getElements()[Lcom/visionobjects/myscript/hwr/InputRangeElement;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v17, v17, v18

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/InputRangeElement;->getFirst()Lcom/visionobjects/myscript/hwr/InputItemLocator;

    move-result-object v17

    .line 167
    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/InputItemLocator;->getComponentIndex()I

    move-result v7

    .line 169
    .local v7, "nFirstIndex":I
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->getInputRange()Lcom/visionobjects/myscript/hwr/InputRange;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/InputRange;->getElements()[Lcom/visionobjects/myscript/hwr/InputRangeElement;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v17, v17, v18

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/InputRangeElement;->getLast()Lcom/visionobjects/myscript/hwr/InputItemLocator;

    move-result-object v17

    .line 170
    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/myscript/hwr/InputItemLocator;->getComponentIndex()I

    move-result v8

    .line 172
    .local v8, "nLastIndex":I
    move v6, v7

    .local v6, "index":I
    :goto_3
    if-le v6, v8, :cond_9

    .line 178
    new-instance v17, Lcom/samsung/android/strokemanager/data/CandidateData;

    move-object/from16 v0, v17

    invoke-direct {v0, v14, v13}, Lcom/samsung/android/strokemanager/data/CandidateData;-><init>(Ljava/util/List;Landroid/graphics/RectF;)V

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    invoke-interface {v14}, Ljava/util/List;->clear()V

    .line 180
    invoke-virtual {v13}, Landroid/graphics/RectF;->setEmpty()V

    .line 182
    invoke-virtual {v2}, Lcom/visionobjects/myscript/hwr/SegmentIterator;->next()V

    goto/16 :goto_1

    .line 173
    :cond_9
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/strokemanager/data/Stroke;->getRectF()Landroid/graphics/RectF;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 174
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/strokemanager/data/Stroke;->setGesture(Z)V

    .line 172
    add-int/lit8 v6, v6, 0x1

    goto :goto_3
.end method
