.class public Lcom/samsung/android/strokemanager/util/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createBitmapFile(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 9
    .param p0, "bmp"    # Landroid/graphics/Bitmap;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "imageName"    # Ljava/lang/String;
    .param p3, "quality"    # I
    .param p4, "reduceScale"    # I

    .prologue
    const/4 v6, 0x1

    .line 31
    const/4 v0, 0x0

    .line 32
    .local v0, "bRes":Z
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v6, v0

    .line 70
    :goto_0
    return v6

    .line 35
    :cond_1
    if-nez p3, :cond_2

    const/16 p3, 0x64

    .line 36
    :cond_2
    if-nez p4, :cond_3

    move p4, v6

    .line 38
    :cond_3
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    div-int/2addr v7, p4

    .line 39
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    div-int/2addr v8, p4

    .line 38
    invoke-static {p0, v7, v8, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 40
    .local v5, "scaledBitmap":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 43
    .local v3, "out":Ljava/io/BufferedOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 46
    new-instance v4, Ljava/io/BufferedOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    .end local v3    # "out":Ljava/io/BufferedOutputStream;
    .local v4, "out":Ljava/io/BufferedOutputStream;
    :try_start_1
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v5, v6, p3, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 49
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V

    .line 50
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 52
    const/4 v0, 0x1

    .line 57
    if-eqz v4, :cond_4

    .line 59
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 64
    :cond_4
    :goto_1
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_5

    .line 65
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 67
    :cond_5
    if-eqz p0, :cond_6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_6

    .line 68
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_6
    move v6, v0

    .line 70
    goto :goto_0

    .line 53
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .restart local v3    # "out":Ljava/io/BufferedOutputStream;
    :catch_0
    move-exception v1

    .line 54
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    if-eqz v3, :cond_7

    .line 59
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 55
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_3
    const/4 v6, 0x0

    goto :goto_0

    .line 60
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 61
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 56
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 57
    :goto_4
    if-eqz v3, :cond_8

    .line 59
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 63
    :cond_8
    :goto_5
    throw v6

    .line 60
    :catch_2
    move-exception v1

    .line 61
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 60
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/BufferedOutputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "out":Ljava/io/BufferedOutputStream;
    :catch_3
    move-exception v1

    .line 61
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 56
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .restart local v3    # "out":Ljava/io/BufferedOutputStream;
    goto :goto_4

    .line 53
    .end local v3    # "out":Ljava/io/BufferedOutputStream;
    .restart local v4    # "out":Ljava/io/BufferedOutputStream;
    :catch_4
    move-exception v1

    move-object v3, v4

    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .restart local v3    # "out":Ljava/io/BufferedOutputStream;
    goto :goto_2
.end method

.method public static createCropBitmapFile(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)Ljava/io/File;
    .locals 11
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "imageName"    # Ljava/lang/String;
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 204
    const/4 v0, 0x0

    .line 205
    .local v0, "croppedBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, p1}, Lcom/samsung/android/strokemanager/util/ImageUtils;->loadBitmapFromFile(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 207
    .local v4, "originalBitmap":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_0

    .line 208
    iget v7, p2, Landroid/graphics/Rect;->left:I

    iget v8, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v9

    .line 209
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v10

    .line 208
    invoke-static {v4, v7, v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 212
    :cond_0
    const/4 v1, 0x0

    .line 214
    .local v1, "croppedFile":Ljava/io/File;
    if-eqz v0, :cond_1

    .line 215
    const/4 v5, 0x0

    .line 218
    .local v5, "out":Ljava/io/BufferedOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "_thumb.png"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, p0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    .end local v1    # "croppedFile":Ljava/io/File;
    .local v2, "croppedFile":Ljava/io/File;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 221
    new-instance v6, Ljava/io/BufferedOutputStream;

    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 222
    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .local v6, "out":Ljava/io/BufferedOutputStream;
    :try_start_2
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x64

    invoke-virtual {v0, v7, v8, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 224
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->flush()V

    .line 225
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 230
    if-eqz v6, :cond_4

    .line 232
    :try_start_3
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-object v1, v2

    .end local v2    # "croppedFile":Ljava/io/File;
    .end local v6    # "out":Ljava/io/BufferedOutputStream;
    .restart local v1    # "croppedFile":Ljava/io/File;
    :cond_1
    :goto_0
    move-object v7, v1

    .line 239
    :goto_1
    return-object v7

    .line 226
    .restart local v5    # "out":Ljava/io/BufferedOutputStream;
    :catch_0
    move-exception v3

    .line 227
    .local v3, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 230
    if-eqz v5, :cond_2

    .line 232
    :try_start_5
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 228
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_3
    const/4 v7, 0x0

    goto :goto_1

    .line 233
    .restart local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 234
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 229
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 230
    :goto_4
    if-eqz v5, :cond_3

    .line 232
    :try_start_6
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 236
    :cond_3
    :goto_5
    throw v7

    .line 233
    :catch_2
    move-exception v3

    .line 234
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 233
    .end local v1    # "croppedFile":Ljava/io/File;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .restart local v2    # "croppedFile":Ljava/io/File;
    .restart local v6    # "out":Ljava/io/BufferedOutputStream;
    :catch_3
    move-exception v3

    .line 234
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .end local v3    # "e":Ljava/io/IOException;
    :cond_4
    move-object v1, v2

    .end local v2    # "croppedFile":Ljava/io/File;
    .restart local v1    # "croppedFile":Ljava/io/File;
    goto :goto_0

    .line 229
    .end local v1    # "croppedFile":Ljava/io/File;
    .end local v6    # "out":Ljava/io/BufferedOutputStream;
    .restart local v2    # "croppedFile":Ljava/io/File;
    .restart local v5    # "out":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v7

    move-object v1, v2

    .end local v2    # "croppedFile":Ljava/io/File;
    .restart local v1    # "croppedFile":Ljava/io/File;
    goto :goto_4

    .end local v1    # "croppedFile":Ljava/io/File;
    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .restart local v2    # "croppedFile":Ljava/io/File;
    .restart local v6    # "out":Ljava/io/BufferedOutputStream;
    :catchall_2
    move-exception v7

    move-object v5, v6

    .end local v6    # "out":Ljava/io/BufferedOutputStream;
    .restart local v5    # "out":Ljava/io/BufferedOutputStream;
    move-object v1, v2

    .end local v2    # "croppedFile":Ljava/io/File;
    .restart local v1    # "croppedFile":Ljava/io/File;
    goto :goto_4

    .line 226
    .end local v1    # "croppedFile":Ljava/io/File;
    .restart local v2    # "croppedFile":Ljava/io/File;
    :catch_4
    move-exception v3

    move-object v1, v2

    .end local v2    # "croppedFile":Ljava/io/File;
    .restart local v1    # "croppedFile":Ljava/io/File;
    goto :goto_2

    .end local v1    # "croppedFile":Ljava/io/File;
    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .restart local v2    # "croppedFile":Ljava/io/File;
    .restart local v6    # "out":Ljava/io/BufferedOutputStream;
    :catch_5
    move-exception v3

    move-object v5, v6

    .end local v6    # "out":Ljava/io/BufferedOutputStream;
    .restart local v5    # "out":Ljava/io/BufferedOutputStream;
    move-object v1, v2

    .end local v2    # "croppedFile":Ljava/io/File;
    .restart local v1    # "croppedFile":Ljava/io/File;
    goto :goto_2
.end method

.method public static createHighLightBitmapFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;I)Ljava/io/File;
    .locals 38
    .param p0, "srcPath"    # Ljava/lang/String;
    .param p1, "srcImageName"    # Ljava/lang/String;
    .param p2, "dstPath"    # Ljava/lang/String;
    .param p3, "dstImageName"    # Ljava/lang/String;
    .param p4, "rect"    # Landroid/graphics/Rect;
    .param p5, "reduceScale"    # I

    .prologue
    .line 102
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v37

    .line 104
    .local v37, "start":Ljava/lang/Long;
    invoke-static/range {p0 .. p1}, Lcom/samsung/android/strokemanager/util/ImageUtils;->loadBitmapFromFile(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 105
    .local v4, "originalBitmap":Landroid/graphics/Bitmap;
    new-instance v35, Landroid/graphics/Rect;

    invoke-direct/range {v35 .. v35}, Landroid/graphics/Rect;-><init>()V

    .line 107
    .local v35, "rec":Landroid/graphics/Rect;
    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->left:I

    div-int v6, v6, p5

    if-gez v6, :cond_3

    const/4 v6, 0x0

    :goto_0
    move-object/from16 v0, v35

    iput v6, v0, Landroid/graphics/Rect;->left:I

    .line 108
    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->top:I

    div-int v6, v6, p5

    if-gez v6, :cond_4

    const/4 v6, 0x0

    :goto_1
    move-object/from16 v0, v35

    iput v6, v0, Landroid/graphics/Rect;->top:I

    .line 109
    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->right:I

    div-int v6, v6, p5

    if-gez v6, :cond_5

    const/4 v6, 0x0

    :goto_2
    move-object/from16 v0, v35

    iput v6, v0, Landroid/graphics/Rect;->right:I

    .line 110
    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    div-int v6, v6, p5

    if-gez v6, :cond_6

    const/4 v6, 0x0

    :goto_3
    move-object/from16 v0, v35

    iput v6, v0, Landroid/graphics/Rect;->bottom:I

    .line 112
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 113
    .local v7, "width":I
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    .line 115
    .local v11, "height":I
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v11, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 116
    .local v24, "destBitmap":Landroid/graphics/Bitmap;
    mul-int v6, v7, v11

    new-array v5, v6, [I

    .line 117
    .local v5, "pixels":[I
    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->height()I

    move-result v8

    mul-int/2addr v6, v8

    new-array v13, v6, [I

    .line 119
    .local v13, "highLightpixels":[I
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move v10, v7

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 120
    const/4 v14, 0x0

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->width()I

    move-result v15

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v16, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->width()I

    move-result v18

    .line 121
    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->height()I

    move-result v19

    move-object v12, v4

    .line 120
    invoke-virtual/range {v12 .. v19}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 123
    const/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v14, v24

    move-object v15, v5

    move/from16 v17, v7

    move/from16 v20, v7

    move/from16 v21, v11

    invoke-virtual/range {v14 .. v21}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 125
    const/16 v27, 0x0

    .local v27, "i":I
    :goto_4
    array-length v6, v13

    move/from16 v0, v27

    if-lt v0, v6, :cond_7

    .line 154
    const/4 v14, 0x0

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->width()I

    move-result v15

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v16, v0

    move-object/from16 v0, v35

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->width()I

    move-result v18

    .line 155
    invoke-virtual/range {v35 .. v35}, Landroid/graphics/Rect;->height()I

    move-result v19

    move-object/from16 v12, v24

    .line 154
    invoke-virtual/range {v12 .. v19}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 157
    const/16 v22, 0x0

    .line 158
    .local v22, "bitmapFile":Ljava/io/File;
    if-eqz v24, :cond_1

    .line 159
    const/16 v33, 0x0

    .line 162
    .local v33, "out":Ljava/io/BufferedOutputStream;
    :try_start_0
    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    .end local v22    # "bitmapFile":Ljava/io/File;
    .local v23, "bitmapFile":Ljava/io/File;
    :try_start_1
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->createNewFile()Z

    .line 165
    new-instance v34, Ljava/io/BufferedOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    move-object/from16 v0, v23

    invoke-direct {v6, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v34

    invoke-direct {v0, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 166
    .end local v33    # "out":Ljava/io/BufferedOutputStream;
    .local v34, "out":Ljava/io/BufferedOutputStream;
    :try_start_2
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x14

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v6, v8, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 168
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedOutputStream;->flush()V

    .line 169
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedOutputStream;->close()V

    .line 171
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 172
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 182
    :cond_0
    if-eqz v34, :cond_11

    .line 184
    :try_start_3
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedOutputStream;->close()V

    .line 186
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_11

    .line 187
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-object/from16 v22, v23

    .line 194
    .end local v23    # "bitmapFile":Ljava/io/File;
    .end local v34    # "out":Ljava/io/BufferedOutputStream;
    .restart local v22    # "bitmapFile":Ljava/io/File;
    :cond_1
    :goto_5
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_2

    .line 195
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->recycle()V

    .line 197
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    .line 198
    .local v26, "end":Ljava/lang/Long;
    const-string v6, "StrokeManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "zzz Time : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual/range {v37 .. v37}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    sub-long v14, v14, v16

    invoke-virtual {v8, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v6, v22

    .line 200
    .end local v26    # "end":Ljava/lang/Long;
    :goto_6
    return-object v6

    .line 107
    .end local v5    # "pixels":[I
    .end local v7    # "width":I
    .end local v11    # "height":I
    .end local v13    # "highLightpixels":[I
    .end local v22    # "bitmapFile":Ljava/io/File;
    .end local v24    # "destBitmap":Landroid/graphics/Bitmap;
    .end local v27    # "i":I
    :cond_3
    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->left:I

    div-int v6, v6, p5

    goto/16 :goto_0

    .line 108
    :cond_4
    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->top:I

    div-int v6, v6, p5

    goto/16 :goto_1

    .line 109
    :cond_5
    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->right:I

    div-int v6, v6, p5

    goto/16 :goto_2

    .line 110
    :cond_6
    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    div-int v6, v6, p5

    goto/16 :goto_3

    .line 126
    .restart local v5    # "pixels":[I
    .restart local v7    # "width":I
    .restart local v11    # "height":I
    .restart local v13    # "highLightpixels":[I
    .restart local v24    # "destBitmap":Landroid/graphics/Bitmap;
    .restart local v27    # "i":I
    :cond_7
    aget v30, v13, v27

    .line 128
    .local v30, "nColor":I
    invoke-static/range {v30 .. v30}, Landroid/graphics/Color;->alpha(I)I

    move-result v28

    .line 129
    .local v28, "nAlpha":I
    invoke-static/range {v30 .. v30}, Landroid/graphics/Color;->red(I)I

    move-result v32

    .line 130
    .local v32, "nRed":I
    invoke-static/range {v30 .. v30}, Landroid/graphics/Color;->green(I)I

    move-result v31

    .line 131
    .local v31, "nGreen":I
    invoke-static/range {v30 .. v30}, Landroid/graphics/Color;->blue(I)I

    move-result v29

    .line 133
    .local v29, "nBlue":I
    move/from16 v0, v32

    int-to-double v8, v0

    const-wide v14, 0x3ff4cccccccccccdL    # 1.3

    mul-double/2addr v8, v14

    double-to-int v0, v8

    move/from16 v32, v0

    .line 134
    move/from16 v0, v31

    int-to-double v8, v0

    const-wide v14, 0x3ff4cccccccccccdL    # 1.3

    mul-double/2addr v8, v14

    double-to-int v0, v8

    move/from16 v31, v0

    .line 135
    move/from16 v0, v29

    int-to-double v8, v0

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v8, v14

    double-to-int v0, v8

    move/from16 v29, v0

    .line 137
    const/16 v6, 0xff

    move/from16 v0, v32

    if-le v0, v6, :cond_8

    .line 138
    const/16 v32, 0xff

    .line 139
    :cond_8
    if-gtz v32, :cond_9

    .line 140
    const/16 v32, 0x0

    .line 141
    :cond_9
    const/16 v6, 0xff

    move/from16 v0, v31

    if-le v0, v6, :cond_a

    .line 142
    const/16 v31, 0xff

    .line 143
    :cond_a
    if-gtz v31, :cond_b

    .line 144
    const/16 v31, 0x0

    .line 145
    :cond_b
    const/16 v6, 0xff

    move/from16 v0, v29

    if-le v0, v6, :cond_c

    .line 146
    const/16 v29, 0xff

    .line 147
    :cond_c
    if-gtz v29, :cond_d

    .line 148
    const/16 v29, 0x0

    .line 150
    :cond_d
    move/from16 v0, v28

    move/from16 v1, v32

    move/from16 v2, v31

    move/from16 v3, v29

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v36

    .line 151
    .local v36, "resultColor":I
    aput v36, v13, v27

    .line 125
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_4

    .line 174
    .end local v28    # "nAlpha":I
    .end local v29    # "nBlue":I
    .end local v30    # "nColor":I
    .end local v31    # "nGreen":I
    .end local v32    # "nRed":I
    .end local v36    # "resultColor":I
    .restart local v22    # "bitmapFile":Ljava/io/File;
    .restart local v33    # "out":Ljava/io/BufferedOutputStream;
    :catch_0
    move-exception v25

    .line 175
    .local v25, "e":Ljava/lang/Exception;
    :goto_7
    :try_start_4
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V

    .line 177
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_e

    .line 178
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 182
    :cond_e
    if-eqz v33, :cond_f

    .line 184
    :try_start_5
    invoke-virtual/range {v33 .. v33}, Ljava/io/BufferedOutputStream;->close()V

    .line 186
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_f

    .line 187
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 180
    .end local v25    # "e":Ljava/lang/Exception;
    :cond_f
    :goto_8
    const/4 v6, 0x0

    goto/16 :goto_6

    .line 188
    .restart local v25    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v25

    .line 189
    .local v25, "e":Ljava/io/IOException;
    invoke-virtual/range {v25 .. v25}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 181
    .end local v25    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 182
    :goto_9
    if-eqz v33, :cond_10

    .line 184
    :try_start_6
    invoke-virtual/range {v33 .. v33}, Ljava/io/BufferedOutputStream;->close()V

    .line 186
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_10

    .line 187
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 191
    :cond_10
    :goto_a
    throw v6

    .line 188
    :catch_2
    move-exception v25

    .line 189
    .restart local v25    # "e":Ljava/io/IOException;
    invoke-virtual/range {v25 .. v25}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 188
    .end local v22    # "bitmapFile":Ljava/io/File;
    .end local v25    # "e":Ljava/io/IOException;
    .end local v33    # "out":Ljava/io/BufferedOutputStream;
    .restart local v23    # "bitmapFile":Ljava/io/File;
    .restart local v34    # "out":Ljava/io/BufferedOutputStream;
    :catch_3
    move-exception v25

    .line 189
    .restart local v25    # "e":Ljava/io/IOException;
    invoke-virtual/range {v25 .. v25}, Ljava/io/IOException;->printStackTrace()V

    .end local v25    # "e":Ljava/io/IOException;
    :cond_11
    move-object/from16 v22, v23

    .end local v23    # "bitmapFile":Ljava/io/File;
    .restart local v22    # "bitmapFile":Ljava/io/File;
    goto/16 :goto_5

    .line 181
    .end local v22    # "bitmapFile":Ljava/io/File;
    .end local v34    # "out":Ljava/io/BufferedOutputStream;
    .restart local v23    # "bitmapFile":Ljava/io/File;
    .restart local v33    # "out":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v6

    move-object/from16 v22, v23

    .end local v23    # "bitmapFile":Ljava/io/File;
    .restart local v22    # "bitmapFile":Ljava/io/File;
    goto :goto_9

    .end local v22    # "bitmapFile":Ljava/io/File;
    .end local v33    # "out":Ljava/io/BufferedOutputStream;
    .restart local v23    # "bitmapFile":Ljava/io/File;
    .restart local v34    # "out":Ljava/io/BufferedOutputStream;
    :catchall_2
    move-exception v6

    move-object/from16 v33, v34

    .end local v34    # "out":Ljava/io/BufferedOutputStream;
    .restart local v33    # "out":Ljava/io/BufferedOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "bitmapFile":Ljava/io/File;
    .restart local v22    # "bitmapFile":Ljava/io/File;
    goto :goto_9

    .line 174
    .end local v22    # "bitmapFile":Ljava/io/File;
    .restart local v23    # "bitmapFile":Ljava/io/File;
    :catch_4
    move-exception v25

    move-object/from16 v22, v23

    .end local v23    # "bitmapFile":Ljava/io/File;
    .restart local v22    # "bitmapFile":Ljava/io/File;
    goto :goto_7

    .end local v22    # "bitmapFile":Ljava/io/File;
    .end local v33    # "out":Ljava/io/BufferedOutputStream;
    .restart local v23    # "bitmapFile":Ljava/io/File;
    .restart local v34    # "out":Ljava/io/BufferedOutputStream;
    :catch_5
    move-exception v25

    move-object/from16 v33, v34

    .end local v34    # "out":Ljava/io/BufferedOutputStream;
    .restart local v33    # "out":Ljava/io/BufferedOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "bitmapFile":Ljava/io/File;
    .restart local v22    # "bitmapFile":Ljava/io/File;
    goto :goto_7
.end method

.method private static loadBitmapFromFile(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "imageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 74
    if-nez p0, :cond_0

    move v5, v3

    :goto_0
    if-nez p1, :cond_1

    :goto_1
    or-int/2addr v3, v5

    if-eqz v3, :cond_2

    .line 75
    const/4 v0, 0x0

    .line 85
    :goto_2
    return-object v0

    :cond_0
    move v5, v4

    .line 74
    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    .line 77
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/samsung/android/strokemanager/Constants;->FILE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "filePathname":Ljava/lang/String;
    const/4 v0, 0x0

    .line 81
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_2

    .line 82
    :catch_0
    move-exception v1

    .line 83
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "StrokeManager"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
