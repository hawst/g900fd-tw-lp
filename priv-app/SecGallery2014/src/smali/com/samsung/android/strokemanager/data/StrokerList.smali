.class public Lcom/samsung/android/strokemanager/data/StrokerList;
.super Ljava/lang/Object;
.source "StrokerList.java"


# instance fields
.field private final mStrokeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/data/StrokerList;->mStrokeList:Ljava/util/List;

    .line 16
    return-void
.end method


# virtual methods
.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/android/strokemanager/data/StrokerList;->mStrokeList:Ljava/util/List;

    return-object v0
.end method
