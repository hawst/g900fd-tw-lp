.class public Lcom/samsung/android/strokemanager/util/ConvertData;
.super Ljava/lang/Object;
.source "ConvertData.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertSpenPageDocToStroker(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Landroid/content/Context;)Lcom/samsung/android/strokemanager/data/Stroker;
    .locals 18
    .param p0, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const-string v12, "StrokeManager"

    const-string v13, "convertSpenPageDocToStroker"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    new-instance v1, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v1}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 25
    .local v1, "convertedStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v12

    if-nez v12, :cond_1

    .line 26
    const-string v12, "StrokeManager"

    const-string v13, "convertSpenPageDocToStroker PageDoc is not valid"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    const/4 v1, 0x0

    .line 95
    .end local v1    # "convertedStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    :cond_0
    :goto_0
    return-object v1

    .line 30
    .restart local v1    # "convertedStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 32
    .local v6, "objBaseList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->getLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 33
    .local v4, "language":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->isCompositeLanguage(Ljava/lang/String;)Z

    move-result v3

    .line 37
    .local v3, "isComposieLang":Z
    const/4 v12, 0x1

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectList(I)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 45
    const/4 v9, 0x0

    .line 46
    .local v9, "preX":F
    const/4 v10, 0x0

    .line 48
    .local v10, "preY":F
    if-eqz v6, :cond_8

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_8

    .line 50
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_3

    .line 90
    invoke-virtual {v1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v12

    if-gtz v12, :cond_0

    .line 91
    const-string v12, "StrokeManager"

    const-string v13, "convertSpenPageDocToStroker there are no strokes for recognize"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const/4 v1, 0x0

    goto :goto_0

    .line 38
    .end local v9    # "preX":F
    .end local v10    # "preY":F
    :catch_0
    move-exception v2

    .line 39
    .local v2, "e":Ljava/lang/Exception;
    const-string v12, "StrokeManager"

    .line 40
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "convertSpenPageDocToStroker SpenPageDoc.getObjectList() has exception : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 41
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 40
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 39
    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    const/4 v1, 0x0

    goto :goto_0

    .line 50
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v9    # "preX":F
    .restart local v10    # "preY":F
    :cond_3
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 52
    .local v5, "objBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v5, :cond_2

    .line 56
    instance-of v12, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v12, :cond_2

    move-object v7, v5

    .line 57
    check-cast v7, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 58
    .local v7, "objStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    new-instance v11, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-direct {v11}, Lcom/samsung/android/strokemanager/data/Stroke;-><init>()V

    .line 60
    .local v11, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 61
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 64
    if-eqz v3, :cond_4

    .line 65
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v12

    const/4 v14, 0x1

    invoke-virtual {v11, v12, v14}, Lcom/samsung/android/strokemanager/data/Stroke;->setRectF(Landroid/graphics/RectF;Z)V

    .line 70
    :goto_2
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v14

    array-length v15, v14

    const/4 v12, 0x0

    :goto_3
    if-lt v12, v15, :cond_5

    .line 80
    invoke-virtual {v11}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v12

    if-lez v12, :cond_2

    .line 81
    invoke-virtual {v11}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    goto :goto_1

    .line 67
    :cond_4
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v12

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v14}, Lcom/samsung/android/strokemanager/data/Stroke;->setRectF(Landroid/graphics/RectF;Z)V

    goto :goto_2

    .line 70
    :cond_5
    aget-object v8, v14, v12

    .line 71
    .local v8, "p":Landroid/graphics/PointF;
    iget v0, v8, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    sub-float v16, v16, v9

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v16

    const/high16 v17, 0x40000000    # 2.0f

    cmpl-float v16, v16, v17

    if-gez v16, :cond_6

    .line 72
    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    sub-float v16, v16, v10

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v16

    const/high16 v17, 0x40000000    # 2.0f

    cmpl-float v16, v16, v17

    if-ltz v16, :cond_7

    .line 73
    :cond_6
    invoke-virtual {v11, v8}, Lcom/samsung/android/strokemanager/data/Stroke;->addPoint(Landroid/graphics/PointF;)V

    .line 75
    :cond_7
    iget v9, v8, Landroid/graphics/PointF;->x:F

    .line 76
    iget v10, v8, Landroid/graphics/PointF;->y:F

    .line 70
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 86
    .end local v5    # "objBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v7    # "objStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    .end local v8    # "p":Landroid/graphics/PointF;
    .end local v11    # "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    :cond_8
    const-string v12, "StrokeManager"

    const-string v13, "convertSpenPageDocToStroker there are no objBase for recognize"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
