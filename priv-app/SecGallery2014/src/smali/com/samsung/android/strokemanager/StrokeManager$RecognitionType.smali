.class public final enum Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;
.super Ljava/lang/Enum;
.source "StrokeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/strokemanager/StrokeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RecognitionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ANALYZE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

.field public static final enum GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

.field public static final enum TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    new-instance v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    new-instance v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    const-string v1, "GESTURE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    new-instance v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    const-string v1, "ANALYZE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->ANALYZE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    .line 57
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->ANALYZE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->ENUM$VALUES:[Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->ENUM$VALUES:[Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
