.class public Lcom/samsung/android/strokemanager/vo/LanguageInfo;
.super Ljava/lang/Object;
.source "LanguageInfo.java"


# static fields
.field private static locale_matrix:[[Ljava/lang/String;


# instance fields
.field private mVODBInfoHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 25
    const/16 v0, 0x2f

    new-array v0, v0, [[Ljava/lang/String;

    .line 26
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "ar"

    aput-object v2, v1, v4

    const-string v2, "ar"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    .line 27
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "az"

    aput-object v2, v1, v4

    const-string v2, "az_AZ"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    .line 28
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "bg"

    aput-object v2, v1, v4

    const-string v2, "bg_BG"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    .line 29
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "ca"

    aput-object v2, v1, v4

    const-string v2, "ca_ES"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    .line 30
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "cs"

    aput-object v2, v1, v4

    const-string v2, "cs_CZ"

    aput-object v2, v1, v5

    aput-object v1, v0, v8

    const/4 v1, 0x5

    .line 31
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "da"

    aput-object v3, v2, v4

    const-string v3, "da_DK"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 32
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "de"

    aput-object v3, v2, v4

    const-string v3, "de_DE"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 33
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "el"

    aput-object v3, v2, v4

    const-string v3, "el_GR"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 34
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "en"

    aput-object v3, v2, v4

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    const-string v3, "en_US"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 35
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "es"

    aput-object v3, v2, v4

    const-string v3, "es_ES"

    aput-object v3, v2, v5

    const-string v3, "es_US"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 36
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "et"

    aput-object v3, v2, v4

    const-string v3, "et_EE"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 37
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "eu"

    aput-object v3, v2, v4

    const-string v3, "eu_ES"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 38
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "fa"

    aput-object v3, v2, v4

    const-string v3, "fa_IR"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 39
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "fi"

    aput-object v3, v2, v4

    const-string v3, "fi_FI"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 40
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "fr"

    aput-object v3, v2, v4

    const-string v3, "fr_CA"

    aput-object v3, v2, v5

    const-string v3, "fr_FR"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 41
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "gl"

    aput-object v3, v2, v4

    const-string v3, "gl_ES"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 42
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "he"

    aput-object v3, v2, v4

    const-string v3, "he_IL"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 43
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "hr"

    aput-object v3, v2, v4

    const-string v3, "hr_HR"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 44
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "hu"

    aput-object v3, v2, v4

    const-string v3, "hu_HU"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 45
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "hy"

    aput-object v3, v2, v4

    const-string v3, "hy_AM"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 46
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v4

    const-string v3, "id_ID"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 47
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "is"

    aput-object v3, v2, v4

    const-string v3, "is_IS"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 48
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "it"

    aput-object v3, v2, v4

    const-string v3, "it_IT"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 49
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ja"

    aput-object v3, v2, v4

    const-string v3, "ja_JP"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 50
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ka"

    aput-object v3, v2, v4

    const-string v3, "ka_GE"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 51
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "kk"

    aput-object v3, v2, v4

    const-string v3, "kk_KZ"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 52
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ko"

    aput-object v3, v2, v4

    const-string v3, "ko_KR"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 53
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "lt"

    aput-object v3, v2, v4

    const-string v3, "lt_LT"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 54
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "lv"

    aput-object v3, v2, v4

    const-string v3, "lv_LV"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 55
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ms"

    aput-object v3, v2, v4

    const-string v3, "ms_MY"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 56
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "mul"

    aput-object v3, v2, v4

    const-string v3, "mul"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 57
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "nb"

    aput-object v3, v2, v4

    const-string v3, "nb_NO"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 58
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "nl"

    aput-object v3, v2, v4

    const-string v3, "nl_NL"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 59
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "pl"

    aput-object v3, v2, v4

    const-string v3, "pl_PL"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 60
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "pt"

    aput-object v3, v2, v4

    const-string v3, "pt_BR"

    aput-object v3, v2, v5

    const-string v3, "pt_PT"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 61
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ro"

    aput-object v3, v2, v4

    const-string v3, "ro_RO"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 62
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ru"

    aput-object v3, v2, v4

    const-string v3, "ru_RU"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 63
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "sk"

    aput-object v3, v2, v4

    const-string/jumbo v3, "sk_SK"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 64
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "sl"

    aput-object v3, v2, v4

    const-string/jumbo v3, "sl_SI"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 65
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "sr"

    aput-object v3, v2, v4

    const-string/jumbo v3, "sr_RS"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 66
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "sv"

    aput-object v3, v2, v4

    const-string/jumbo v3, "sv_SE"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 67
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "th"

    aput-object v3, v2, v4

    const-string/jumbo v3, "th_TH"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 68
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "tr"

    aput-object v3, v2, v4

    const-string/jumbo v3, "tr_TR"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 69
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "uk"

    aput-object v3, v2, v4

    const-string/jumbo v3, "uk_UA"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 70
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "ur"

    aput-object v3, v2, v4

    const-string/jumbo v3, "ur_PK"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 71
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "vi"

    aput-object v3, v2, v4

    const-string/jumbo v3, "vi_VN"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 72
    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "zh"

    aput-object v3, v2, v4

    const-string/jumbo v3, "zh_CN"

    aput-object v3, v2, v5

    const-string/jumbo v3, "zh_HK"

    aput-object v3, v2, v6

    const-string/jumbo v3, "zh_TW"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    .line 24
    sput-object v0, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->locale_matrix:[[Ljava/lang/String;

    .line 73
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->mVODBInfoHash:Ljava/util/HashMap;

    .line 21
    invoke-direct {p0}, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->makeLanguageInfoHashMap()V

    .line 22
    return-void
.end method

.method private getVODBInfoHashMap(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->mVODBInfoHash:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private makeLanguageInfoHashMap()V
    .locals 5

    .prologue
    .line 79
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->locale_matrix:[[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 84
    return-void

    .line 80
    :cond_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    sget-object v2, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->locale_matrix:[[Ljava/lang/String;

    aget-object v2, v2, v0

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->mVODBInfoHash:Ljava/util/HashMap;

    sget-object v3, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->locale_matrix:[[Ljava/lang/String;

    aget-object v3, v3, v0

    const/4 v4, 0x0

    aget-object v3, v3, v4

    sget-object v4, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->locale_matrix:[[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getVODBFolder(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 101
    move-object v3, p1

    .line 102
    .local v3, "mLanguageStr":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v3, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 104
    .local v2, "mLanguageISO":Ljava/lang/String;
    const-string v4, "StrokeManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mLanguageStr : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/4 v0, 0x0

    .line 107
    .local v0, "folderList":[Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/samsung/android/strokemanager/vo/LanguageInfo;->getVODBInfoHashMap(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 109
    if-nez v0, :cond_1

    .line 110
    const-string v3, "en_GB"

    .line 122
    .end local v3    # "mLanguageStr":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v3

    .line 113
    .restart local v3    # "mLanguageStr":Ljava/lang/String;
    :cond_1
    array-length v4, v0

    if-ne v4, v8, :cond_2

    .line 114
    aget-object v3, v0, v7

    goto :goto_0

    .line 116
    :cond_2
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    array-length v4, v0

    if-lt v1, v4, :cond_3

    .line 122
    aget-object v3, v0, v7

    goto :goto_0

    .line 117
    :cond_3
    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 116
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
