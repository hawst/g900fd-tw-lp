.class public Lcom/samsung/android/strokemanager/StrokeManager;
.super Ljava/lang/Object;
.source "StrokeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;,
        Lcom/samsung/android/strokemanager/StrokeManager$Switch;
    }
.end annotation


# static fields
.field private static final mSwitch:Lcom/samsung/android/strokemanager/StrokeManager$Switch;


# instance fields
.field private addedSymbols:[Ljava/lang/String;

.field private mCandidateCb:Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;

.field private mCandidateDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/CandidateData;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCoordinator:Landroid/os/HandlerThread;

.field private mDebugRects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private mDebugView:Lcom/samsung/android/strokemanager/view/DebugView;

.field private mGestureDebugRects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private mLanguage:Ljava/lang/String;

.field private mLooper:Landroid/os/Looper;

.field private mPagePoints:I

.field private mRecognitionHandler:Landroid/os/Handler;

.field private mTextDebugRects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

.field private mUserDicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserDictionary:[Ljava/lang/String;

.field private mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->ALL:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    sput-object v0, Lcom/samsung/android/strokemanager/StrokeManager;->mSwitch:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mContext:Landroid/content/Context;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateCb:Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCoordinator:Landroid/os/HandlerThread;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLooper:Landroid/os/Looper;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mRecognitionHandler:Landroid/os/Handler;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    .line 51
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    .line 53
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    .line 55
    iput v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    .line 68
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mDebugView:Lcom/samsung/android/strokemanager/view/DebugView;

    .line 73
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mDebugRects:Ljava/util/List;

    .line 75
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mGestureDebugRects:Ljava/util/List;

    .line 77
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextDebugRects:Ljava/util/List;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDictionary:[Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDicList:Ljava/util/ArrayList;

    .line 84
    new-array v1, v4, [Ljava/lang/String;

    .line 85
    const-string v2, "#"

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v3, "?"

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->addedSymbols:[Ljava/lang/String;

    .line 89
    const-string v1, "StrokeManager"

    const-string v2, "StrokeManager Ver. [ 1.5.7 ]"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iput-object p1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mContext:Landroid/content/Context;

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 94
    const-string v2, "com.sec.feature.spen_usp"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v0

    .line 96
    .local v0, "uspLevel":I
    if-ge v0, v4, :cond_0

    .line 97
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "This device is less than level 2"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 100
    :cond_0
    sget-object v1, Lcom/samsung/android/strokemanager/StrokeManager;->mSwitch:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    invoke-direct {p0, v1}, Lcom/samsung/android/strokemanager/StrokeManager;->initStrokeManager(Lcom/samsung/android/strokemanager/StrokeManager$Switch;)V

    .line 101
    invoke-static {p1}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->getLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    .line 103
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDicList:Ljava/util/ArrayList;

    .line 108
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/strokemanager/StrokeManager;Z)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/samsung/android/strokemanager/StrokeManager;->doRecognize(Z)V

    return-void
.end method

.method private doRecognize(Z)V
    .locals 6
    .param p1, "bCallBack"    # Z

    .prologue
    .line 147
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v3}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v3

    if-gtz v3, :cond_0

    .line 204
    :goto_0
    return-void

    .line 150
    :cond_0
    const/4 v0, 0x0

    .line 151
    .local v0, "bMoreStroke":Z
    new-instance v2, Lcom/samsung/android/strokemanager/data/StrokerList;

    invoke-direct {v2}, Lcom/samsung/android/strokemanager/data/StrokerList;-><init>()V

    .line 153
    .local v2, "strokerList":Lcom/samsung/android/strokemanager/data/StrokerList;
    sget-object v3, Lcom/samsung/android/strokemanager/StrokeManager;->mSwitch:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    sget-object v4, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->GESTURE_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    if-eq v3, v4, :cond_1

    sget-object v3, Lcom/samsung/android/strokemanager/StrokeManager;->mSwitch:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    sget-object v4, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->ALL:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    if-ne v3, v4, :cond_5

    .line 155
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {p0, v3}, Lcom/samsung/android/strokemanager/StrokeManager;->recognizeGesture(Lcom/samsung/android/strokemanager/data/Stroker;)V

    .line 157
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v3}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v3

    if-lt v1, v3, :cond_3

    .line 164
    :goto_2
    if-nez v0, :cond_5

    .line 165
    if-eqz p1, :cond_2

    .line 166
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateCb:Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v3, v4}, Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;->onRecognized(Ljava/util/List;)V

    .line 169
    :cond_2
    const-string v3, "StrokeManager"

    const-string v4, "Manager << end >> ------------------------------------------"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 158
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v3, v1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/strokemanager/data/Stroke;->isGesture()Z

    move-result v3

    if-nez v3, :cond_4

    .line 159
    const/4 v0, 0x1

    .line 160
    goto :goto_2

    .line 157
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 174
    .end local v1    # "i":I
    :cond_5
    const/4 v0, 0x0

    .line 176
    sget-object v3, Lcom/samsung/android/strokemanager/StrokeManager;->mSwitch:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    sget-object v4, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->TEXT_ONLY:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    if-eq v3, v4, :cond_6

    sget-object v3, Lcom/samsung/android/strokemanager/StrokeManager;->mSwitch:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    sget-object v4, Lcom/samsung/android/strokemanager/StrokeManager$Switch;->ALL:Lcom/samsung/android/strokemanager/StrokeManager$Switch;

    if-ne v3, v4, :cond_8

    .line 178
    :cond_6
    iget v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    const/16 v4, 0xc8

    if-le v3, v4, :cond_7

    .line 179
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->isChineseLanguage(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 180
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/strokemanager/vo/VOManager;->analyseDocument(Ljava/lang/String;Lcom/samsung/android/strokemanager/data/Stroker;)V

    .line 183
    :cond_7
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v3}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v3

    if-lt v1, v3, :cond_a

    .line 190
    :goto_4
    if-eqz v0, :cond_8

    .line 192
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-static {v3}, Lcom/samsung/android/strokemanager/control/StrokeControl;->getCorrectedStrokers(Lcom/samsung/android/strokemanager/data/Stroker;)Lcom/samsung/android/strokemanager/data/StrokerList;

    move-result-object v2

    .line 195
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/strokemanager/StrokeManager;->recognizeText(Lcom/samsung/android/strokemanager/data/StrokerList;Ljava/lang/String;)V

    .line 199
    .end local v1    # "i":I
    :cond_8
    if-eqz p1, :cond_9

    .line 200
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateCb:Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v3, v4}, Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;->onRecognized(Ljava/util/List;)V

    .line 203
    :cond_9
    const-string v3, "StrokeManager"

    const-string v4, "Manager << end >> ------------------------------------------"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 184
    .restart local v1    # "i":I
    :cond_a
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v3, v1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStroke(I)Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/strokemanager/data/Stroke;->isDust()Z

    move-result v3

    if-nez v3, :cond_b

    .line 185
    const/4 v0, 0x1

    .line 186
    goto :goto_4

    .line 183
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private dummyData()Lcom/samsung/android/strokemanager/data/CandidateData;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 550
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 551
    .local v2, "dummyString":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, " "

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v4, v4, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 553
    .local v1, "dummyRect":Landroid/graphics/RectF;
    new-instance v0, Lcom/samsung/android/strokemanager/data/CandidateData;

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/strokemanager/data/CandidateData;-><init>(Ljava/util/List;Landroid/graphics/RectF;)V

    .line 555
    .local v0, "dummyData":Lcom/samsung/android/strokemanager/data/CandidateData;
    return-object v0
.end method

.method private initDebug()V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mDebugRects:Ljava/util/List;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mGestureDebugRects:Ljava/util/List;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextDebugRects:Ljava/util/List;

    .line 114
    return-void
.end method

.method private initStrokeManager(Lcom/samsung/android/strokemanager/StrokeManager$Switch;)V
    .locals 2
    .param p1, "type"    # Lcom/samsung/android/strokemanager/StrokeManager$Switch;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 118
    new-instance v0, Lcom/samsung/android/strokemanager/vo/VOManager;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/samsung/android/strokemanager/vo/VOManager;-><init>(Landroid/content/Context;Lcom/samsung/android/strokemanager/StrokeManager$Switch;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    .line 119
    new-instance v0, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v0}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    .line 124
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Coordinator"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCoordinator:Landroid/os/HandlerThread;

    .line 125
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCoordinator:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 126
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCoordinator:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLooper:Landroid/os/Looper;

    .line 128
    new-instance v0, Lcom/samsung/android/strokemanager/StrokeManager$1;

    iget-object v1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLooper:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/strokemanager/StrokeManager$1;-><init>(Lcom/samsung/android/strokemanager/StrokeManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mRecognitionHandler:Landroid/os/Handler;

    .line 144
    return-void
.end method

.method private recognizeGesture(Lcom/samsung/android/strokemanager/data/Stroker;)V
    .locals 7
    .param p1, "stroker"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 266
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .local v2, "textDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    const-string v3, "StrokeManager"

    .line 269
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Manager recognizeGesture Added Stroke Cnt : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 268
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    iget-object v4, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/strokemanager/vo/VOManager;->prepare(Ljava/lang/String;Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;Ljava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 272
    const-string v3, "StrokeManager"

    const-string v4, "Manager VOUDS prepare failed"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    sget-object v4, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    invoke-virtual {v3, v4}, Lcom/samsung/android/strokemanager/vo/VOManager;->close(Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;)V

    .line 296
    :goto_0
    return-void

    .line 278
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    const/4 v4, 0x0

    sget-object v5, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    invoke-virtual {v3, p1, v4, v5}, Lcom/samsung/android/strokemanager/vo/VOManager;->recognize(Lcom/samsung/android/strokemanager/data/Stroker;Lcom/samsung/android/strokemanager/data/Stroker;Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 284
    :goto_1
    if-eqz v2, :cond_1

    .line 285
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 295
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    sget-object v4, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->GESTURE:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    invoke-virtual {v3, v4}, Lcom/samsung/android/strokemanager/vo/VOManager;->close(Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;)V

    goto :goto_0

    .line 279
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "StrokeManager"

    const-string v4, "Manager Gesture Recognition Fail "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 285
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/strokemanager/data/CandidateData;

    .line 286
    .local v0, "d":Lcom/samsung/android/strokemanager/data/CandidateData;
    iget-object v4, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private recognizeText(Lcom/samsung/android/strokemanager/data/StrokerList;Ljava/lang/String;)V
    .locals 13
    .param p1, "strokerList"    # Lcom/samsung/android/strokemanager/data/StrokerList;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 207
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/StrokerList;->getList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    .line 208
    .local v3, "index":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .local v7, "textDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    iget-object v8, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    invoke-static {v8}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->isCompositeLanguage(Ljava/lang/String;)Z

    move-result v4

    .line 216
    .local v4, "isComposieLang":Z
    iget-object v8, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDicList:Ljava/util/ArrayList;

    if-eqz v8, :cond_0

    .line 217
    iget-object v10, p0, Lcom/samsung/android/strokemanager/StrokeManager;->addedSymbols:[Ljava/lang/String;

    array-length v11, v10

    move v8, v9

    :goto_0
    if-lt v8, v11, :cond_1

    .line 222
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    iget-object v10, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    sget-object v11, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    iget-object v12, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDicList:Ljava/util/ArrayList;

    invoke-virtual {v8, v10, v11, v12}, Lcom/samsung/android/strokemanager/vo/VOManager;->prepare(Ljava/lang/String;Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;Ljava/util/ArrayList;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 223
    const-string v8, "StrokeManager"

    const-string v9, "Manager VOHWR prepare failed"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v8, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    sget-object v9, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    invoke-virtual {v8, v9}, Lcom/samsung/android/strokemanager/vo/VOManager;->close(Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;)V

    .line 263
    :goto_1
    return-void

    .line 217
    :cond_1
    aget-object v6, v10, v8

    .line 218
    .local v6, "str":Ljava/lang/String;
    iget-object v12, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDicList:Ljava/util/ArrayList;

    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 228
    .end local v6    # "str":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-lt v2, v3, :cond_3

    .line 262
    iget-object v8, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    sget-object v9, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    invoke-virtual {v8, v9}, Lcom/samsung/android/strokemanager/vo/VOManager;->close(Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;)V

    goto :goto_1

    .line 229
    :cond_3
    new-instance v5, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-direct {v5}, Lcom/samsung/android/strokemanager/data/Stroker;-><init>()V

    .line 231
    .local v5, "rotatedStroker":Lcom/samsung/android/strokemanager/data/Stroker;
    if-eqz v4, :cond_5

    .line 232
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/StrokerList;->getList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/strokemanager/data/Stroker;

    .line 233
    const/4 v10, 0x1

    .line 232
    invoke-static {v8, v10}, Lcom/samsung/android/strokemanager/control/StrokeControl;->rotateSelectedStroker(Lcom/samsung/android/strokemanager/data/Stroker;Z)Lcom/samsung/android/strokemanager/data/Stroker;

    move-result-object v5

    .line 239
    :goto_3
    const-string v8, "StrokeManager"

    .line 240
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Manager recognizeText Added Stroke Cnt : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 239
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :try_start_0
    iget-object v10, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/StrokerList;->getList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/strokemanager/data/Stroker;

    .line 244
    sget-object v11, Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;->TEXT:Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;

    .line 243
    invoke-virtual {v10, v8, v5, v11}, Lcom/samsung/android/strokemanager/vo/VOManager;->recognize(Lcom/samsung/android/strokemanager/data/Stroker;Lcom/samsung/android/strokemanager/data/Stroker;Lcom/samsung/android/strokemanager/StrokeManager$RecognitionType;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 246
    if-nez v7, :cond_6

    .line 228
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 235
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/StrokerList;->getList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-static {v8, v9}, Lcom/samsung/android/strokemanager/control/StrokeControl;->rotateSelectedStroker(Lcom/samsung/android/strokemanager/data/Stroker;Z)Lcom/samsung/android/strokemanager/data/Stroker;

    move-result-object v5

    goto :goto_3

    .line 249
    :catch_0
    move-exception v1

    .line 250
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 253
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/strokemanager/data/CandidateData;

    .line 254
    .local v0, "d":Lcom/samsung/android/strokemanager/data/CandidateData;
    iget-object v10, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 625
    const-string v0, "StrokeManager"

    const-string v1, "Manager << close >> "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 629
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    .line 632
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLooper:Landroid/os/Looper;

    if-eqz v0, :cond_1

    .line 633
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 636
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCoordinator:Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    .line 637
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCoordinator:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 640
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mRecognitionHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 641
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mRecognitionHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 643
    :cond_3
    return-void
.end method

.method public getAvailableLangList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mVOManager:Lcom/samsung/android/strokemanager/vo/VOManager;

    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/vo/VOManager;->getAvailableLangList()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 609
    const-string v0, "1.5.7"

    return-object v0
.end method

.method public setDebugView(Lcom/samsung/android/strokemanager/view/DebugView;)V
    .locals 0
    .param p1, "dv"    # Lcom/samsung/android/strokemanager/view/DebugView;

    .prologue
    .line 568
    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "language"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 586
    if-eqz p1, :cond_0

    .line 587
    iput-object p1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    .line 588
    :cond_0
    return-void
.end method

.method public setOnCandidateCallback(Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;

    .prologue
    .line 618
    iput-object p1, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateCb:Lcom/samsung/android/strokemanager/callback/ICandidateCallBack;

    .line 619
    return-void
.end method

.method public setUserDictionary(ILjava/lang/String;)V
    .locals 6
    .param p1, "nArrayID"    # I
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 591
    iget-object v2, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 593
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDictionary:[Ljava/lang/String;

    .line 595
    iget-object v3, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDictionary:[Ljava/lang/String;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 606
    :goto_1
    return-void

    .line 595
    :cond_0
    aget-object v1, v3, v2

    .line 596
    .local v1, "str":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mUserDicList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 599
    .end local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_1

    .line 603
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_1
    const-string v2, "StrokeManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Manager This dictionary ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 604
    const-string v4, ") is not matched : Setting Language ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 603
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public showDebugView()V
    .locals 0

    .prologue
    .line 300
    return-void
.end method

.method public startRecognize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Ljava/util/List;
    .locals 17
    .param p1, "arg0"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "bCallBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sdk/pen/document/SpenPageDoc;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/CandidateData;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 416
    const-string v11, "StrokeManager"

    const-string v12, "Manager << startRecognize >> ----------------------------------"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v11

    if-nez v11, :cond_0

    .line 419
    const-string v11, "StrokeManager"

    const-string v12, "PageDoc is not valid"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    const/4 v11, 0x0

    .line 517
    :goto_0
    return-object v11

    .line 423
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 424
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v11}, Lcom/samsung/android/strokemanager/data/Stroker;->clear()V

    .line 426
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 428
    .local v5, "objBaseList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    if-nez v11, :cond_1

    .line 429
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->getLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    .line 432
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    invoke-static {v11}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->isCompositeLanguage(Ljava/lang/String;)Z

    move-result v2

    .line 435
    .local v2, "isComposieLang":Z
    const/4 v11, 0x1

    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectList(I)Ljava/util/ArrayList;

    move-result-object v11

    .line 436
    invoke-virtual {v11}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v5

    .line 435
    .end local v5    # "objBaseList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    check-cast v5, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 442
    .restart local v5    # "objBaseList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v8, 0x0

    .line 443
    .local v8, "preX":F
    const/4 v9, 0x0

    .line 445
    .local v9, "preY":F
    if-eqz v5, :cond_8

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_8

    .line 447
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_3

    .line 484
    const-string v11, "StrokeManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Manager get point counts : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v11}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v11

    if-gtz v11, :cond_9

    .line 492
    const-string v11, "StrokeManager"

    const-string v12, "Manager there are no strokes for recognize"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    const/4 v11, 0x0

    goto :goto_0

    .line 437
    .end local v5    # "objBaseList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .end local v8    # "preX":F
    .end local v9    # "preY":F
    :catch_0
    move-exception v1

    .line 438
    .local v1, "e":Ljava/lang/Exception;
    const-string v11, "StrokeManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "SpenPageDoc.getObjectList() has exception : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 447
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v5    # "objBaseList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .restart local v8    # "preX":F
    .restart local v9    # "preY":F
    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 449
    .local v4, "objBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v4, :cond_2

    .line 453
    instance-of v11, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v11, :cond_2

    move-object v6, v4

    .line 454
    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 455
    .local v6, "objStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    new-instance v10, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-direct {v10}, Lcom/samsung/android/strokemanager/data/Stroke;-><init>()V

    .line 457
    .local v10, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 458
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 462
    if-eqz v2, :cond_4

    .line 463
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v11

    const/4 v13, 0x1

    invoke-virtual {v10, v11, v13}, Lcom/samsung/android/strokemanager/data/Stroke;->setRectF(Landroid/graphics/RectF;Z)V

    .line 468
    :goto_2
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v13

    array-length v14, v13

    const/4 v11, 0x0

    :goto_3
    if-lt v11, v14, :cond_5

    .line 478
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    invoke-virtual {v10}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v13

    add-int/2addr v11, v13

    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    .line 480
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v10}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    goto/16 :goto_1

    .line 465
    :cond_4
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v11

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v13}, Lcom/samsung/android/strokemanager/data/Stroke;->setRectF(Landroid/graphics/RectF;Z)V

    goto :goto_2

    .line 468
    :cond_5
    aget-object v7, v13, v11

    .line 469
    .local v7, "p":Landroid/graphics/PointF;
    iget v15, v7, Landroid/graphics/PointF;->x:F

    sub-float/2addr v15, v8

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    const/high16 v16, 0x40000000    # 2.0f

    cmpl-float v15, v15, v16

    if-gez v15, :cond_6

    .line 470
    iget v15, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v15, v9

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    const/high16 v16, 0x40000000    # 2.0f

    cmpl-float v15, v15, v16

    if-ltz v15, :cond_7

    .line 471
    :cond_6
    invoke-virtual {v10, v7}, Lcom/samsung/android/strokemanager/data/Stroke;->addPoint(Landroid/graphics/PointF;)V

    .line 473
    :cond_7
    iget v8, v7, Landroid/graphics/PointF;->x:F

    .line 474
    iget v9, v7, Landroid/graphics/PointF;->y:F

    .line 468
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 487
    .end local v4    # "objBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v6    # "objStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    .end local v7    # "p":Landroid/graphics/PointF;
    .end local v10    # "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    :cond_8
    const-string v11, "StrokeManager"

    const-string v12, "Manager there are no objBase for recognize"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 504
    :cond_9
    if-eqz p2, :cond_a

    .line 505
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mRecognitionHandler:Landroid/os/Handler;

    const/16 v12, 0x3e9

    invoke-virtual {v11, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 506
    .local v3, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mRecognitionHandler:Landroid/os/Handler;

    invoke-virtual {v11, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 517
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 508
    .end local v3    # "msg":Landroid/os/Message;
    :cond_a
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/strokemanager/StrokeManager;->doRecognize(Z)V

    .line 510
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-gtz v11, :cond_b

    .line 511
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/strokemanager/StrokeManager;->dummyData()Lcom/samsung/android/strokemanager/data/CandidateData;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 514
    :cond_b
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    goto/16 :goto_0
.end method

.method public startRecognize(Lcom/samsung/android/strokemanager/data/Stroker;)Ljava/util/List;
    .locals 3
    .param p1, "stroker"    # Lcom/samsung/android/strokemanager/data/Stroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/strokemanager/data/Stroker;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/CandidateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521
    const-string v0, "StrokeManager"

    const-string v1, "Manager << startRecognize >> ----------------------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 525
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 526
    :cond_0
    const-string v0, "StrokeManager"

    const-string v1, "Manager there are no strokes for recognize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    const/4 v0, 0x0

    .line 546
    :goto_0
    return-object v0

    .line 530
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->getSizeOfPoints()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    .line 531
    const-string v0, "StrokeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Manager get point counts : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/data/Stroker;->clear()V

    .line 534
    invoke-virtual {p1}, Lcom/samsung/android/strokemanager/data/Stroker;->copy()Lcom/samsung/android/strokemanager/data/Stroker;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    .line 536
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 537
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->getLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    .line 540
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/strokemanager/StrokeManager;->doRecognize(Z)V

    .line 542
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 543
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-direct {p0}, Lcom/samsung/android/strokemanager/StrokeManager;->dummyData()Lcom/samsung/android/strokemanager/data/CandidateData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    goto :goto_0
.end method

.method public startRecognize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 17
    .param p1, "arg0"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 324
    const-string v11, "StrokeManager"

    const-string v12, "Manager << startRecognize >> ----------------------------------"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mCandidateDataList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 327
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v11}, Lcom/samsung/android/strokemanager/data/Stroker;->clear()V

    .line 329
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 331
    .local v5, "objBaseList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    if-nez v11, :cond_0

    .line 332
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->getLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    .line 335
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mLanguage:Ljava/lang/String;

    invoke-static {v11}, Lcom/samsung/android/strokemanager/util/LanguageUtils;->isCompositeLanguage(Ljava/lang/String;)Z

    move-result v2

    .line 338
    .local v2, "isComposieLang":Z
    const/4 v11, 0x1

    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectList(I)Ljava/util/ArrayList;

    move-result-object v11

    .line 339
    invoke-virtual {v11}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v11

    .line 338
    move-object v0, v11

    check-cast v0, Ljava/util/List;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    const/4 v8, 0x0

    .line 346
    .local v8, "preX":F
    const/4 v9, 0x0

    .line 348
    .local v9, "preY":F
    if-eqz p1, :cond_2

    .line 349
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_3

    .line 386
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v11}, Lcom/samsung/android/strokemanager/data/Stroker;->getStrokeCount()I

    move-result v11

    if-gtz v11, :cond_8

    .line 387
    const-string v11, "StrokeManager"

    const-string v12, "Manager there are no strokes for recognize"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    .end local v8    # "preX":F
    .end local v9    # "preY":F
    :goto_1
    return-void

    .line 340
    :catch_0
    move-exception v1

    .line 341
    .local v1, "e":Ljava/lang/Exception;
    const-string v11, "StrokeManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "SpenPageDoc.getObjectList() has exception : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 349
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v8    # "preX":F
    .restart local v9    # "preY":F
    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .line 351
    .local v4, "objBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    if-eqz v4, :cond_1

    .line 355
    instance-of v11, v4, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v11, :cond_1

    move-object v6, v4

    .line 356
    check-cast v6, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    .line 357
    .local v6, "objStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    new-instance v10, Lcom/samsung/android/strokemanager/data/Stroke;

    invoke-direct {v10}, Lcom/samsung/android/strokemanager/data/Stroke;-><init>()V

    .line 359
    .local v10, "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 360
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 363
    if-eqz v2, :cond_4

    .line 364
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v11

    const/4 v13, 0x1

    invoke-virtual {v10, v11, v13}, Lcom/samsung/android/strokemanager/data/Stroke;->setRectF(Landroid/graphics/RectF;Z)V

    .line 369
    :goto_2
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v13

    array-length v14, v13

    const/4 v11, 0x0

    :goto_3
    if-lt v11, v14, :cond_5

    .line 379
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    invoke-virtual {v10}, Lcom/samsung/android/strokemanager/data/Stroke;->getPointCount()I

    move-result v13

    add-int/2addr v11, v13

    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mPagePoints:I

    .line 381
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mTextStroker:Lcom/samsung/android/strokemanager/data/Stroker;

    invoke-virtual {v10}, Lcom/samsung/android/strokemanager/data/Stroke;->copy()Lcom/samsung/android/strokemanager/data/Stroke;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/strokemanager/data/Stroker;->addStroke(Lcom/samsung/android/strokemanager/data/Stroke;)V

    goto :goto_0

    .line 366
    :cond_4
    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getRect()Landroid/graphics/RectF;

    move-result-object v11

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v13}, Lcom/samsung/android/strokemanager/data/Stroke;->setRectF(Landroid/graphics/RectF;Z)V

    goto :goto_2

    .line 369
    :cond_5
    aget-object v7, v13, v11

    .line 370
    .local v7, "p":Landroid/graphics/PointF;
    iget v15, v7, Landroid/graphics/PointF;->x:F

    sub-float/2addr v15, v8

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    const/high16 v16, 0x40000000    # 2.0f

    cmpl-float v15, v15, v16

    if-gez v15, :cond_6

    .line 371
    iget v15, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v15, v9

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    const/high16 v16, 0x40000000    # 2.0f

    cmpl-float v15, v15, v16

    if-ltz v15, :cond_7

    .line 372
    :cond_6
    invoke-virtual {v10, v7}, Lcom/samsung/android/strokemanager/data/Stroke;->addPoint(Landroid/graphics/PointF;)V

    .line 374
    :cond_7
    iget v8, v7, Landroid/graphics/PointF;->x:F

    .line 375
    iget v9, v7, Landroid/graphics/PointF;->y:F

    .line 369
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 399
    .end local v4    # "objBase":Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
    .end local v6    # "objStroke":Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
    .end local v7    # "p":Landroid/graphics/PointF;
    .end local v10    # "stroke":Lcom/samsung/android/strokemanager/data/Stroke;
    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mRecognitionHandler:Landroid/os/Handler;

    const/16 v12, 0x3e9

    invoke-virtual {v11, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 400
    .local v3, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/strokemanager/StrokeManager;->mRecognitionHandler:Landroid/os/Handler;

    invoke-virtual {v11, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1
.end method
