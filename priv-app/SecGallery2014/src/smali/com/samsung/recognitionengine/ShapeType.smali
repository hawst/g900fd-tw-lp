.class public final enum Lcom/samsung/recognitionengine/ShapeType;
.super Ljava/lang/Enum;
.source "ShapeType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/recognitionengine/ShapeType$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/recognitionengine/ShapeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Angle:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Arc:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Arrow:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_BarChart:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_BulletItem:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_ChartAxis:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_CheckItem:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Chevron:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Chord:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Circle:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_ColumnChart:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_ColumnDownDirection:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_ColumnUpDirection:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_ConcaveArrow:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_ConcaveFlag:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Cone:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Cross:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Cube:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_CubicBezierCurve:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Cylinder:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Diamond:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_DoubleArrow:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Ellipse:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Ellipsoid:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Elongated_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Envelope:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_FlowchartPredefinedProcess:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_FlowchartStoredData:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_FlowchartTerminator:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Heart:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Heptagon:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Hexagram:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Infinity:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Integral:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Isosceles_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_LShape:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_LeftArrow:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_LeftBrace:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_LeftRightArrow:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_LeftSquareBracket:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Line:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_LineChart:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_MarkItem:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Moon:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Parallelogram:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Pentagon:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Pentagon_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Pentagram:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Pie:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_PieChart:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_PyramidChartHorizontal:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_PyramidChartVertical:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Radical:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Rect:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_RectCallout:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_RightAngled_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_RightBrace:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_RightSquareBracket:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_SandGlass:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_ShapesCount:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Sigma:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_SmileConfused:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_SmileDisappointed:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_SmileHappy:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_SmileSad:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_SmileSurprised:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Sphere:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Square:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Star4:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Star5:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Star6:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Summation:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Table:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Tetrahedron:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_TetrahedronTopView:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Triangle:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Triangle_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Triangle_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Triangle_RightAngled:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Triangle_RightAngled_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

.field public static final enum ShapeType_Unknown:Lcom/samsung/recognitionengine/ShapeType;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Unknown"

    invoke-direct {v0, v1, v3, v3}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Unknown:Lcom/samsung/recognitionengine/ShapeType;

    .line 13
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Circle"

    invoke-direct {v0, v1, v4}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Circle:Lcom/samsung/recognitionengine/ShapeType;

    .line 14
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Ellipse"

    invoke-direct {v0, v1, v5}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Ellipse:Lcom/samsung/recognitionengine/ShapeType;

    .line 15
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Rect"

    invoke-direct {v0, v1, v6}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Rect:Lcom/samsung/recognitionengine/ShapeType;

    .line 16
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Star4"

    invoke-direct {v0, v1, v7}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star4:Lcom/samsung/recognitionengine/ShapeType;

    .line 17
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Star5"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star5:Lcom/samsung/recognitionengine/ShapeType;

    .line 18
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Star6"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star6:Lcom/samsung/recognitionengine/ShapeType;

    .line 19
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Cross"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cross:Lcom/samsung/recognitionengine/ShapeType;

    .line 20
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_LShape"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LShape:Lcom/samsung/recognitionengine/ShapeType;

    .line 21
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Chevron"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Chevron:Lcom/samsung/recognitionengine/ShapeType;

    .line 22
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Hexagon"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

    .line 23
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Elongated_Hexagon"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Elongated_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

    .line 24
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Triangle"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle:Lcom/samsung/recognitionengine/ShapeType;

    .line 25
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Parallelogram"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Parallelogram:Lcom/samsung/recognitionengine/ShapeType;

    .line 26
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Line"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Line:Lcom/samsung/recognitionengine/ShapeType;

    .line 27
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Arc"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Arc:Lcom/samsung/recognitionengine/ShapeType;

    .line 28
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Angle"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Angle:Lcom/samsung/recognitionengine/ShapeType;

    .line 29
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_CubicBezierCurve"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_CubicBezierCurve:Lcom/samsung/recognitionengine/ShapeType;

    .line 30
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Arrow"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Arrow:Lcom/samsung/recognitionengine/ShapeType;

    .line 31
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_DoubleArrow"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_DoubleArrow:Lcom/samsung/recognitionengine/ShapeType;

    .line 32
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_LeftArrow"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftArrow:Lcom/samsung/recognitionengine/ShapeType;

    .line 33
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_LeftRightArrow"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftRightArrow:Lcom/samsung/recognitionengine/ShapeType;

    .line 34
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_ConcaveArrow"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ConcaveArrow:Lcom/samsung/recognitionengine/ShapeType;

    .line 35
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Moon"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Moon:Lcom/samsung/recognitionengine/ShapeType;

    .line 36
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Diamond"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Diamond:Lcom/samsung/recognitionengine/ShapeType;

    .line 37
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Square"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Square:Lcom/samsung/recognitionengine/ShapeType;

    .line 38
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Trapezium"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    .line 39
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Isosceles_Trapezium"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Isosceles_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    .line 40
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_RightAngled_Trapezium"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightAngled_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    .line 41
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Pentagon"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagon:Lcom/samsung/recognitionengine/ShapeType;

    .line 42
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Pentagon_Equilateral"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagon_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

    .line 43
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_ConcaveFlag"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ConcaveFlag:Lcom/samsung/recognitionengine/ShapeType;

    .line 44
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Heptagon"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Heptagon:Lcom/samsung/recognitionengine/ShapeType;

    .line 45
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_SandGlass"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SandGlass:Lcom/samsung/recognitionengine/ShapeType;

    .line 46
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Triangle_Equilateral"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

    .line 47
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Triangle_Isosceles"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

    .line 48
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Triangle_RightAngled"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_RightAngled:Lcom/samsung/recognitionengine/ShapeType;

    .line 49
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Triangle_RightAngled_Isosceles"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_RightAngled_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

    .line 50
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Table"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Table:Lcom/samsung/recognitionengine/ShapeType;

    .line 51
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_SmileHappy"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileHappy:Lcom/samsung/recognitionengine/ShapeType;

    .line 52
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_SmileSad"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileSad:Lcom/samsung/recognitionengine/ShapeType;

    .line 53
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_SmileDisappointed"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileDisappointed:Lcom/samsung/recognitionengine/ShapeType;

    .line 54
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_SmileSurprised"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileSurprised:Lcom/samsung/recognitionengine/ShapeType;

    .line 55
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_SmileConfused"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileConfused:Lcom/samsung/recognitionengine/ShapeType;

    .line 56
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_PieChart"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PieChart:Lcom/samsung/recognitionengine/ShapeType;

    .line 57
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_ChartAxis"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ChartAxis:Lcom/samsung/recognitionengine/ShapeType;

    .line 58
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Radical"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Radical:Lcom/samsung/recognitionengine/ShapeType;

    .line 59
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Summation"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Summation:Lcom/samsung/recognitionengine/ShapeType;

    .line 60
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Integral"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Integral:Lcom/samsung/recognitionengine/ShapeType;

    .line 61
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Sigma"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Sigma:Lcom/samsung/recognitionengine/ShapeType;

    .line 62
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Pentagram"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagram:Lcom/samsung/recognitionengine/ShapeType;

    .line 63
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Hexagram"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagram:Lcom/samsung/recognitionengine/ShapeType;

    .line 64
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Infinity"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Infinity:Lcom/samsung/recognitionengine/ShapeType;

    .line 65
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Heart"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Heart:Lcom/samsung/recognitionengine/ShapeType;

    .line 66
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Pie"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pie:Lcom/samsung/recognitionengine/ShapeType;

    .line 67
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Chord"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Chord:Lcom/samsung/recognitionengine/ShapeType;

    .line 68
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_LeftBrace"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftBrace:Lcom/samsung/recognitionengine/ShapeType;

    .line 69
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_RightBrace"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightBrace:Lcom/samsung/recognitionengine/ShapeType;

    .line 70
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Cone"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cone:Lcom/samsung/recognitionengine/ShapeType;

    .line 71
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Sphere"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Sphere:Lcom/samsung/recognitionengine/ShapeType;

    .line 72
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Ellipsoid"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Ellipsoid:Lcom/samsung/recognitionengine/ShapeType;

    .line 73
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Tetrahedron"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Tetrahedron:Lcom/samsung/recognitionengine/ShapeType;

    .line 74
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_ColumnChart"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ColumnChart:Lcom/samsung/recognitionengine/ShapeType;

    .line 75
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_BarChart"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_BarChart:Lcom/samsung/recognitionengine/ShapeType;

    .line 76
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_PyramidChartVertical"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PyramidChartVertical:Lcom/samsung/recognitionengine/ShapeType;

    .line 77
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_PyramidChartHorizontal"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PyramidChartHorizontal:Lcom/samsung/recognitionengine/ShapeType;

    .line 78
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_LineChart"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LineChart:Lcom/samsung/recognitionengine/ShapeType;

    .line 79
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Cylinder"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cylinder:Lcom/samsung/recognitionengine/ShapeType;

    .line 80
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Cube"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cube:Lcom/samsung/recognitionengine/ShapeType;

    .line 81
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_LeftSquareBracket"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftSquareBracket:Lcom/samsung/recognitionengine/ShapeType;

    .line 82
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_RightSquareBracket"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightSquareBracket:Lcom/samsung/recognitionengine/ShapeType;

    .line 83
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_FlowchartTerminator"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_FlowchartTerminator:Lcom/samsung/recognitionengine/ShapeType;

    .line 84
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_CheckItem"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_CheckItem:Lcom/samsung/recognitionengine/ShapeType;

    .line 85
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_ColumnUpDirection"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ColumnUpDirection:Lcom/samsung/recognitionengine/ShapeType;

    .line 86
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_ColumnDownDirection"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ColumnDownDirection:Lcom/samsung/recognitionengine/ShapeType;

    .line 87
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_Envelope"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Envelope:Lcom/samsung/recognitionengine/ShapeType;

    .line 88
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_BulletItem"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_BulletItem:Lcom/samsung/recognitionengine/ShapeType;

    .line 89
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_FlowchartPredefinedProcess"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_FlowchartPredefinedProcess:Lcom/samsung/recognitionengine/ShapeType;

    .line 90
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_TetrahedronTopView"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_TetrahedronTopView:Lcom/samsung/recognitionengine/ShapeType;

    .line 91
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_MarkItem"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_MarkItem:Lcom/samsung/recognitionengine/ShapeType;

    .line 92
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_RectCallout"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RectCallout:Lcom/samsung/recognitionengine/ShapeType;

    .line 93
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_FlowchartStoredData"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_FlowchartStoredData:Lcom/samsung/recognitionengine/ShapeType;

    .line 94
    new-instance v0, Lcom/samsung/recognitionengine/ShapeType;

    const-string v1, "ShapeType_ShapesCount"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ShapesCount:Lcom/samsung/recognitionengine/ShapeType;

    .line 11
    const/16 v0, 0x53

    new-array v0, v0, [Lcom/samsung/recognitionengine/ShapeType;

    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Unknown:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Circle:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Ellipse:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Rect:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star4:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star5:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Star6:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cross:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LShape:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Chevron:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Elongated_Hexagon:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Parallelogram:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Line:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Arc:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Angle:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_CubicBezierCurve:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Arrow:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_DoubleArrow:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftArrow:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftRightArrow:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ConcaveArrow:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Moon:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Diamond:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Square:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Isosceles_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightAngled_Trapezium:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagon:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagon_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ConcaveFlag:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Heptagon:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SandGlass:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_Equilateral:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_RightAngled:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Triangle_RightAngled_Isosceles:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Table:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileHappy:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileSad:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileDisappointed:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileSurprised:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_SmileConfused:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PieChart:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ChartAxis:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Radical:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Summation:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Integral:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Sigma:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pentagram:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Hexagram:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Infinity:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Heart:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Pie:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Chord:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftBrace:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightBrace:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cone:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Sphere:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Ellipsoid:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Tetrahedron:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ColumnChart:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_BarChart:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PyramidChartVertical:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_PyramidChartHorizontal:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LineChart:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cylinder:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Cube:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_LeftSquareBracket:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RightSquareBracket:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_FlowchartTerminator:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_CheckItem:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ColumnUpDirection:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ColumnDownDirection:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_Envelope:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_BulletItem:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_FlowchartPredefinedProcess:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_TetrahedronTopView:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_MarkItem:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_RectCallout:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_FlowchartStoredData:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/samsung/recognitionengine/ShapeType;->ShapeType_ShapesCount:Lcom/samsung/recognitionengine/ShapeType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/recognitionengine/ShapeType;->$VALUES:[Lcom/samsung/recognitionengine/ShapeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112
    # operator++ for: Lcom/samsung/recognitionengine/ShapeType$SwigNext;->next:I
    invoke-static {}, Lcom/samsung/recognitionengine/ShapeType$SwigNext;->access$008()I

    move-result v0

    iput v0, p0, Lcom/samsung/recognitionengine/ShapeType;->swigValue:I

    .line 113
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 117
    iput p3, p0, Lcom/samsung/recognitionengine/ShapeType;->swigValue:I

    .line 118
    add-int/lit8 v0, p3, 0x1

    # setter for: Lcom/samsung/recognitionengine/ShapeType$SwigNext;->next:I
    invoke-static {v0}, Lcom/samsung/recognitionengine/ShapeType$SwigNext;->access$002(I)I

    .line 119
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/samsung/recognitionengine/ShapeType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/recognitionengine/ShapeType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 122
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 123
    iget v0, p3, Lcom/samsung/recognitionengine/ShapeType;->swigValue:I

    iput v0, p0, Lcom/samsung/recognitionengine/ShapeType;->swigValue:I

    .line 124
    iget v0, p0, Lcom/samsung/recognitionengine/ShapeType;->swigValue:I

    add-int/lit8 v0, v0, 0x1

    # setter for: Lcom/samsung/recognitionengine/ShapeType$SwigNext;->next:I
    invoke-static {v0}, Lcom/samsung/recognitionengine/ShapeType$SwigNext;->access$002(I)I

    .line 125
    return-void
.end method

.method public static swigToEnum(I)Lcom/samsung/recognitionengine/ShapeType;
    .locals 5

    .prologue
    .line 101
    const-class v0, Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/recognitionengine/ShapeType;

    .line 102
    array-length v1, v0

    if-ge p0, v1, :cond_0

    if-ltz p0, :cond_0

    aget-object v1, v0, p0

    iget v1, v1, Lcom/samsung/recognitionengine/ShapeType;->swigValue:I

    if-ne v1, p0, :cond_0

    .line 103
    aget-object v0, v0, p0

    .line 106
    :goto_0
    return-object v0

    .line 104
    :cond_0
    array-length v3, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 105
    iget v4, v1, Lcom/samsung/recognitionengine/ShapeType;->swigValue:I

    if-ne v4, p0, :cond_1

    move-object v0, v1

    .line 106
    goto :goto_0

    .line 104
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 107
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/recognitionengine/ShapeType;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/samsung/recognitionengine/ShapeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/recognitionengine/ShapeType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/recognitionengine/ShapeType;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/samsung/recognitionengine/ShapeType;->$VALUES:[Lcom/samsung/recognitionengine/ShapeType;

    invoke-virtual {v0}, [Lcom/samsung/recognitionengine/ShapeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/recognitionengine/ShapeType;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/samsung/recognitionengine/ShapeType;->swigValue:I

    return v0
.end method
