.class public final enum Lcom/samsung/recognitionengine/Trainer$EResult;
.super Ljava/lang/Enum;
.source "Trainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/recognitionengine/Trainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/recognitionengine/Trainer$EResult$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/recognitionengine/Trainer$EResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/recognitionengine/Trainer$EResult;

.field public static final enum ERR_DECREASING_TIMESTAMPS:Lcom/samsung/recognitionengine/Trainer$EResult;

.field public static final enum ERR_EMPTY_STROKES_NOT_ALLOWED:Lcom/samsung/recognitionengine/Trainer$EResult;

.field public static final enum ERR_MAX_SIGNATURES_AMOUNT_EXCEEDED:Lcom/samsung/recognitionengine/Trainer$EResult;

.field public static final enum ERR_NOT_ENOUGH_POINTS_IN_SIGNATURE:Lcom/samsung/recognitionengine/Trainer$EResult;

.field public static final enum ERR_SIGNATURE_WOULD_INVALIDATE_MODEL:Lcom/samsung/recognitionengine/Trainer$EResult;

.field public static final enum RES_OK:Lcom/samsung/recognitionengine/Trainer$EResult;

.field public static final enum WARN_SIGNATURE_IS_SIMPLE:Lcom/samsung/recognitionengine/Trainer$EResult;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 83
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    const-string v1, "RES_OK"

    invoke-direct {v0, v1, v3, v3}, Lcom/samsung/recognitionengine/Trainer$EResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->RES_OK:Lcom/samsung/recognitionengine/Trainer$EResult;

    .line 84
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    const-string v1, "ERR_DECREASING_TIMESTAMPS"

    invoke-direct {v0, v1, v4}, Lcom/samsung/recognitionengine/Trainer$EResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_DECREASING_TIMESTAMPS:Lcom/samsung/recognitionengine/Trainer$EResult;

    .line 85
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    const-string v1, "ERR_NOT_ENOUGH_POINTS_IN_SIGNATURE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/recognitionengine/Trainer$EResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_NOT_ENOUGH_POINTS_IN_SIGNATURE:Lcom/samsung/recognitionengine/Trainer$EResult;

    .line 86
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    const-string v1, "ERR_EMPTY_STROKES_NOT_ALLOWED"

    invoke-direct {v0, v1, v6}, Lcom/samsung/recognitionengine/Trainer$EResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_EMPTY_STROKES_NOT_ALLOWED:Lcom/samsung/recognitionengine/Trainer$EResult;

    .line 87
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    const-string v1, "ERR_MAX_SIGNATURES_AMOUNT_EXCEEDED"

    invoke-direct {v0, v1, v7}, Lcom/samsung/recognitionengine/Trainer$EResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_MAX_SIGNATURES_AMOUNT_EXCEEDED:Lcom/samsung/recognitionengine/Trainer$EResult;

    .line 88
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    const-string v1, "ERR_SIGNATURE_WOULD_INVALIDATE_MODEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/Trainer$EResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_SIGNATURE_WOULD_INVALIDATE_MODEL:Lcom/samsung/recognitionengine/Trainer$EResult;

    .line 89
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    const-string v1, "WARN_SIGNATURE_IS_SIMPLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/Trainer$EResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->WARN_SIGNATURE_IS_SIMPLE:Lcom/samsung/recognitionengine/Trainer$EResult;

    .line 82
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/recognitionengine/Trainer$EResult;

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$EResult;->RES_OK:Lcom/samsung/recognitionengine/Trainer$EResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_DECREASING_TIMESTAMPS:Lcom/samsung/recognitionengine/Trainer$EResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_NOT_ENOUGH_POINTS_IN_SIGNATURE:Lcom/samsung/recognitionengine/Trainer$EResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_EMPTY_STROKES_NOT_ALLOWED:Lcom/samsung/recognitionengine/Trainer$EResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_MAX_SIGNATURES_AMOUNT_EXCEEDED:Lcom/samsung/recognitionengine/Trainer$EResult;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/recognitionengine/Trainer$EResult;->ERR_SIGNATURE_WOULD_INVALIDATE_MODEL:Lcom/samsung/recognitionengine/Trainer$EResult;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/recognitionengine/Trainer$EResult;->WARN_SIGNATURE_IS_SIMPLE:Lcom/samsung/recognitionengine/Trainer$EResult;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->$VALUES:[Lcom/samsung/recognitionengine/Trainer$EResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 107
    # operator++ for: Lcom/samsung/recognitionengine/Trainer$EResult$SwigNext;->next:I
    invoke-static {}, Lcom/samsung/recognitionengine/Trainer$EResult$SwigNext;->access$008()I

    move-result v0

    iput v0, p0, Lcom/samsung/recognitionengine/Trainer$EResult;->swigValue:I

    .line 108
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112
    iput p3, p0, Lcom/samsung/recognitionengine/Trainer$EResult;->swigValue:I

    .line 113
    add-int/lit8 v0, p3, 0x1

    # setter for: Lcom/samsung/recognitionengine/Trainer$EResult$SwigNext;->next:I
    invoke-static {v0}, Lcom/samsung/recognitionengine/Trainer$EResult$SwigNext;->access$002(I)I

    .line 114
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/samsung/recognitionengine/Trainer$EResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/recognitionengine/Trainer$EResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 118
    iget v0, p3, Lcom/samsung/recognitionengine/Trainer$EResult;->swigValue:I

    iput v0, p0, Lcom/samsung/recognitionengine/Trainer$EResult;->swigValue:I

    .line 119
    iget v0, p0, Lcom/samsung/recognitionengine/Trainer$EResult;->swigValue:I

    add-int/lit8 v0, v0, 0x1

    # setter for: Lcom/samsung/recognitionengine/Trainer$EResult$SwigNext;->next:I
    invoke-static {v0}, Lcom/samsung/recognitionengine/Trainer$EResult$SwigNext;->access$002(I)I

    .line 120
    return-void
.end method

.method public static swigToEnum(I)Lcom/samsung/recognitionengine/Trainer$EResult;
    .locals 5

    .prologue
    .line 96
    const-class v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/recognitionengine/Trainer$EResult;

    .line 97
    array-length v1, v0

    if-ge p0, v1, :cond_0

    if-ltz p0, :cond_0

    aget-object v1, v0, p0

    iget v1, v1, Lcom/samsung/recognitionengine/Trainer$EResult;->swigValue:I

    if-ne v1, p0, :cond_0

    .line 98
    aget-object v0, v0, p0

    .line 101
    :goto_0
    return-object v0

    .line 99
    :cond_0
    array-length v3, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 100
    iget v4, v1, Lcom/samsung/recognitionengine/Trainer$EResult;->swigValue:I

    if-ne v4, p0, :cond_1

    move-object v0, v1

    .line 101
    goto :goto_0

    .line 99
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 102
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/samsung/recognitionengine/Trainer$EResult;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/recognitionengine/Trainer$EResult;
    .locals 1

    .prologue
    .line 82
    const-class v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/recognitionengine/Trainer$EResult;

    return-object v0
.end method

.method public static values()[Lcom/samsung/recognitionengine/Trainer$EResult;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/samsung/recognitionengine/Trainer$EResult;->$VALUES:[Lcom/samsung/recognitionengine/Trainer$EResult;

    invoke-virtual {v0}, [Lcom/samsung/recognitionengine/Trainer$EResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/recognitionengine/Trainer$EResult;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/recognitionengine/Trainer$EResult;->swigValue:I

    return v0
.end method
