.class public final enum Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;
.super Ljava/lang/Enum;
.source "Trainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/recognitionengine/Trainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SimplicityLevel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/recognitionengine/Trainer$SimplicityLevel$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

.field public static final enum SimplicityLevel_High:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

.field public static final enum SimplicityLevel_Low:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

.field public static final enum SimplicityLevel_Medium:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 130
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    const-string v1, "SimplicityLevel_Low"

    invoke-direct {v0, v1, v2, v2}, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->SimplicityLevel_Low:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    .line 131
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    const-string v1, "SimplicityLevel_Medium"

    invoke-direct {v0, v1, v3}, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->SimplicityLevel_Medium:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    .line 132
    new-instance v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    const-string v1, "SimplicityLevel_High"

    invoke-direct {v0, v1, v4}, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->SimplicityLevel_High:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    .line 129
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->SimplicityLevel_Low:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->SimplicityLevel_Medium:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->SimplicityLevel_High:Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->$VALUES:[Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 150
    # operator++ for: Lcom/samsung/recognitionengine/Trainer$SimplicityLevel$SwigNext;->next:I
    invoke-static {}, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel$SwigNext;->access$108()I

    move-result v0

    iput v0, p0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue:I

    .line 151
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 155
    iput p3, p0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue:I

    .line 156
    add-int/lit8 v0, p3, 0x1

    # setter for: Lcom/samsung/recognitionengine/Trainer$SimplicityLevel$SwigNext;->next:I
    invoke-static {v0}, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel$SwigNext;->access$102(I)I

    .line 157
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/samsung/recognitionengine/Trainer$SimplicityLevel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 161
    iget v0, p3, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue:I

    iput v0, p0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue:I

    .line 162
    iget v0, p0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue:I

    add-int/lit8 v0, v0, 0x1

    # setter for: Lcom/samsung/recognitionengine/Trainer$SimplicityLevel$SwigNext;->next:I
    invoke-static {v0}, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel$SwigNext;->access$102(I)I

    .line 163
    return-void
.end method

.method public static swigToEnum(I)Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;
    .locals 5

    .prologue
    .line 139
    const-class v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    .line 140
    array-length v1, v0

    if-ge p0, v1, :cond_0

    if-ltz p0, :cond_0

    aget-object v1, v0, p0

    iget v1, v1, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue:I

    if-ne v1, p0, :cond_0

    .line 141
    aget-object v0, v0, p0

    .line 144
    :goto_0
    return-object v0

    .line 142
    :cond_0
    array-length v3, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 143
    iget v4, v1, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue:I

    if-ne v4, p0, :cond_1

    move-object v0, v1

    .line 144
    goto :goto_0

    .line 142
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 145
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;
    .locals 1

    .prologue
    .line 129
    const-class v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    return-object v0
.end method

.method public static values()[Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->$VALUES:[Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    invoke-virtual {v0}, [Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue:I

    return v0
.end method
