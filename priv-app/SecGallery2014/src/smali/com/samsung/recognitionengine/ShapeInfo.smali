.class public Lcom/samsung/recognitionengine/ShapeInfo;
.super Ljava/lang/Object;
.source "ShapeInfo.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeInfo__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/ShapeInfo;)V
    .locals 3

    .prologue
    .line 67
    invoke-static {p1}, Lcom/samsung/recognitionengine/ShapeInfo;->getCPtr(Lcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeInfo__SWIG_7(JLcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/ShapeType;Lcom/samsung/recognitionengine/PointFVector;)V
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/samsung/recognitionengine/ShapeType;->swigValue()I

    move-result v0

    invoke-static {p2}, Lcom/samsung/recognitionengine/PointFVector;->getCPtr(Lcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v2

    invoke-static {v0, v2, v3, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeInfo__SWIG_2(IJLcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/ShapeType;Lcom/samsung/recognitionengine/PointFVector;F)V
    .locals 4

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/samsung/recognitionengine/ShapeType;->swigValue()I

    move-result v0

    invoke-static {p2}, Lcom/samsung/recognitionengine/PointFVector;->getCPtr(Lcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v2

    invoke-static {v0, v2, v3, p2, p3}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeInfo__SWIG_4(IJLcom/samsung/recognitionengine/PointFVector;F)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/ShapeType;Lcom/samsung/recognitionengine/PointFVector;FI)V
    .locals 7

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/samsung/recognitionengine/ShapeType;->swigValue()I

    move-result v1

    invoke-static {p2}, Lcom/samsung/recognitionengine/PointFVector;->getCPtr(Lcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v2

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v1 .. v6}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeInfo__SWIG_3(IJLcom/samsung/recognitionengine/PointFVector;FI)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/ShapeType;Lcom/samsung/recognitionengine/PointFVector;I)V
    .locals 4

    .prologue
    .line 43
    invoke-virtual {p1}, Lcom/samsung/recognitionengine/ShapeType;->swigValue()I

    move-result v0

    invoke-static {p2}, Lcom/samsung/recognitionengine/PointFVector;->getCPtr(Lcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v2

    invoke-static {v0, v2, v3, p2, p3}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeInfo__SWIG_1(IJLcom/samsung/recognitionengine/PointFVector;I)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/ShapeType;Lcom/samsung/recognitionengine/SWIGTYPE_p_SShapesSDK__RectT_float_t;)V
    .locals 4

    .prologue
    .line 63
    invoke-virtual {p1}, Lcom/samsung/recognitionengine/ShapeType;->swigValue()I

    move-result v0

    invoke-static {p2}, Lcom/samsung/recognitionengine/SWIGTYPE_p_SShapesSDK__RectT_float_t;->getCPtr(Lcom/samsung/recognitionengine/SWIGTYPE_p_SShapesSDK__RectT_float_t;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeInfo__SWIG_6(IJ)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/ShapeType;Lcom/samsung/recognitionengine/SWIGTYPE_p_SShapesSDK__RectT_float_t;I)V
    .locals 4

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/samsung/recognitionengine/ShapeType;->swigValue()I

    move-result v0

    invoke-static {p2}, Lcom/samsung/recognitionengine/SWIGTYPE_p_SShapesSDK__RectT_float_t;->getCPtr(Lcom/samsung/recognitionengine/SWIGTYPE_p_SShapesSDK__RectT_float_t;)J

    move-result-wide v2

    invoke-static {v0, v2, v3, p3}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeInfo__SWIG_5(IJI)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    .line 60
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/ShapeInfo;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public adjustToAxis()V
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_adjustToAxis__SWIG_1(JLcom/samsung/recognitionengine/ShapeInfo;)V

    .line 116
    return-void
.end method

.method public adjustToAxis(F)V
    .locals 2

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_adjustToAxis__SWIG_0(JLcom/samsung/recognitionengine/ShapeInfo;F)V

    .line 112
    return-void
.end method

.method public angle()F
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_angle(JLcom/samsung/recognitionengine/ShapeInfo;)F

    move-result v0

    return v0
.end method

.method public clone()Lcom/samsung/recognitionengine/ShapeInfo;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lcom/samsung/recognitionengine/ShapeInfo;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_clone(JLcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/ShapeInfo;-><init>(JZ)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/ShapeInfo;->clone()Lcom/samsung/recognitionengine/ShapeInfo;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_ShapeInfo(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Lcom/samsung/recognitionengine/ShapeInfo;)Z
    .locals 6

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/ShapeInfo;->getCPtr(Lcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_equals(JLcom/samsung/recognitionengine/ShapeInfo;JLcom/samsung/recognitionengine/ShapeInfo;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/ShapeInfo;->delete()V

    .line 26
    return-void
.end method

.method public generatePoints(J)Lcom/samsung/recognitionengine/VectorPointFVectors;
    .locals 5

    .prologue
    .line 119
    new-instance v0, Lcom/samsung/recognitionengine/VectorPointFVectors;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v2, v3, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_generatePoints(JLcom/samsung/recognitionengine/ShapeInfo;J)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/VectorPointFVectors;-><init>(JZ)V

    return-object v0
.end method

.method public getRecognizedPoints()Lcom/samsung/recognitionengine/PointFVector;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Lcom/samsung/recognitionengine/PointFVector;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_getRecognizedPoints(JLcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/PointFVector;-><init>(JZ)V

    return-object v0
.end method

.method public getRelevance()I
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_getRelevance(JLcom/samsung/recognitionengine/ShapeInfo;)I

    move-result v0

    return v0
.end method

.method public getShapeType()Lcom/samsung/recognitionengine/ShapeType;
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_getShapeType(JLcom/samsung/recognitionengine/ShapeInfo;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/recognitionengine/ShapeType;->swigToEnum(I)Lcom/samsung/recognitionengine/ShapeType;

    move-result-object v0

    return-object v0
.end method

.method public isClosedShape()Z
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_isClosedShape(JLcom/samsung/recognitionengine/ShapeInfo;)Z

    move-result v0

    return v0
.end method

.method public isComplexShape()Z
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_isComplexShape(JLcom/samsung/recognitionengine/ShapeInfo;)Z

    move-result v0

    return v0
.end method

.method public setAngle(FLcom/samsung/recognitionengine/PointF;)V
    .locals 7

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v4

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_setAngle(JLcom/samsung/recognitionengine/ShapeInfo;FJLcom/samsung/recognitionengine/PointF;)V

    .line 108
    return-void
.end method

.method public setRecognizedPoints(Lcom/samsung/recognitionengine/PointFVector;)V
    .locals 6

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointFVector;->getCPtr(Lcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_setRecognizedPoints(JLcom/samsung/recognitionengine/ShapeInfo;JLcom/samsung/recognitionengine/PointFVector;)V

    .line 84
    return-void
.end method

.method public shapeTypeToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeInfo_shapeTypeToString(JLcom/samsung/recognitionengine/ShapeInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
