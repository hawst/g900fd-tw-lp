.class public Lcom/samsung/recognitionengine/ShapeTypeVector;
.super Ljava/lang/Object;
.source "ShapeTypeVector.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeTypeVector__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeTypeVector;-><init>(JZ)V

    .line 40
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 43
    invoke-static {p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeTypeVector__SWIG_1(J)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeTypeVector;-><init>(JZ)V

    .line 44
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    .line 18
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/ShapeTypeVector;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/samsung/recognitionengine/ShapeType;)V
    .locals 3

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/recognitionengine/ShapeType;->swigValue()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeTypeVector_add(JLcom/samsung/recognitionengine/ShapeTypeVector;I)V

    .line 68
    return-void
.end method

.method public capacity()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeTypeVector_capacity(JLcom/samsung/recognitionengine/ShapeTypeVector;)J

    move-result-wide v0

    return-wide v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeTypeVector_clear(JLcom/samsung/recognitionengine/ShapeTypeVector;)V

    .line 64
    return-void
.end method

.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_ShapeTypeVector(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/ShapeTypeVector;->delete()V

    .line 26
    return-void
.end method

.method public get(I)Lcom/samsung/recognitionengine/ShapeType;
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeTypeVector_get(JLcom/samsung/recognitionengine/ShapeTypeVector;I)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/recognitionengine/ShapeType;->swigToEnum(I)Lcom/samsung/recognitionengine/ShapeType;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeTypeVector_isEmpty(JLcom/samsung/recognitionengine/ShapeTypeVector;)Z

    move-result v0

    return v0
.end method

.method public reserve(J)V
    .locals 3

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeTypeVector_reserve(JLcom/samsung/recognitionengine/ShapeTypeVector;J)V

    .line 56
    return-void
.end method

.method public set(ILcom/samsung/recognitionengine/ShapeType;)V
    .locals 3

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-virtual {p2}, Lcom/samsung/recognitionengine/ShapeType;->swigValue()I

    move-result v2

    invoke-static {v0, v1, p0, p1, v2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeTypeVector_set(JLcom/samsung/recognitionengine/ShapeTypeVector;II)V

    .line 76
    return-void
.end method

.method public size()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeTypeVector;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeTypeVector_size(JLcom/samsung/recognitionengine/ShapeTypeVector;)J

    move-result-wide v0

    return-wide v0
.end method
