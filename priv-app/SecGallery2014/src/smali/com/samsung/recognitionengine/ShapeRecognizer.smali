.class public Lcom/samsung/recognitionengine/ShapeRecognizer;
.super Ljava/lang/Object;
.source "ShapeRecognizer.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeRecognizer__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeRecognizer;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/ShapeTypeVector;)V
    .locals 3

    .prologue
    .line 43
    invoke-static {p1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->getCPtr(Lcom/samsung/recognitionengine/ShapeTypeVector;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_ShapeRecognizer__SWIG_1(JLcom/samsung/recognitionengine/ShapeTypeVector;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeRecognizer;-><init>(JZ)V

    .line 44
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/ShapeRecognizer;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_ShapeRecognizer(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/ShapeRecognizer;->delete()V

    .line 26
    return-void
.end method

.method public getGesturesForShapeInfo(Lcom/samsung/recognitionengine/ShapeInfo;)Lcom/samsung/recognitionengine/VectorPointFVectors;
    .locals 7

    .prologue
    .line 55
    new-instance v6, Lcom/samsung/recognitionengine/VectorPointFVectors;

    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/ShapeInfo;->getCPtr(Lcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeRecognizer_getGesturesForShapeInfo(JLcom/samsung/recognitionengine/ShapeRecognizer;JLcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/VectorPointFVectors;-><init>(JZ)V

    return-object v6
.end method

.method public getIndexesOfGesturesForShapeInfo(Lcom/samsung/recognitionengine/ShapeInfo;)Lcom/samsung/recognitionengine/SizeTVector;
    .locals 7

    .prologue
    .line 59
    new-instance v6, Lcom/samsung/recognitionengine/SizeTVector;

    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/ShapeInfo;->getCPtr(Lcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeRecognizer_getIndexesOfGesturesForShapeInfo(JLcom/samsung/recognitionengine/ShapeRecognizer;JLcom/samsung/recognitionengine/ShapeInfo;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/SizeTVector;-><init>(JZ)V

    return-object v6
.end method

.method public recognize(Lcom/samsung/recognitionengine/VectorPointFVectors;)Lcom/samsung/recognitionengine/VectorShapeInfoVectors;
    .locals 7

    .prologue
    .line 51
    new-instance v6, Lcom/samsung/recognitionengine/VectorShapeInfoVectors;

    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/VectorPointFVectors;->getCPtr(Lcom/samsung/recognitionengine/VectorPointFVectors;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeRecognizer_recognize(JLcom/samsung/recognitionengine/ShapeRecognizer;JLcom/samsung/recognitionengine/VectorPointFVectors;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/VectorShapeInfoVectors;-><init>(JZ)V

    return-object v6
.end method

.method public setEnabledShapes(Lcom/samsung/recognitionengine/ShapeTypeVector;)V
    .locals 6

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/ShapeTypeVector;->getCPtr(Lcom/samsung/recognitionengine/ShapeTypeVector;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeRecognizer_setEnabledShapes(JLcom/samsung/recognitionengine/ShapeRecognizer;JLcom/samsung/recognitionengine/ShapeTypeVector;)V

    .line 64
    return-void
.end method

.method public setPPI(F)V
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/recognitionengine/ShapeRecognizer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->ShapeRecognizer_setPPI(JLcom/samsung/recognitionengine/ShapeRecognizer;F)V

    .line 48
    return-void
.end method
