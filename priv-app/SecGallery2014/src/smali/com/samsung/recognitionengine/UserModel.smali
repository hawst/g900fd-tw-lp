.class public Lcom/samsung/recognitionengine/UserModel;
.super Ljava/lang/Object;
.source "UserModel.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_UserModel__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/UserModel;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/UserModel;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/UserModel;->swigCPtr:J

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/UserModel;)V
    .locals 3

    .prologue
    .line 43
    invoke-static {p1}, Lcom/samsung/recognitionengine/UserModel;->getCPtr(Lcom/samsung/recognitionengine/UserModel;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_UserModel__SWIG_1(JLcom/samsung/recognitionengine/UserModel;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/UserModel;-><init>(JZ)V

    .line 44
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/UserModel;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCPtr:J

    goto :goto_0
.end method

.method public static readModel(Lcom/samsung/recognitionengine/UserModelReader;)Lcom/samsung/recognitionengine/UserModel;
    .locals 4

    .prologue
    .line 55
    new-instance v0, Lcom/samsung/recognitionengine/UserModel;

    invoke-static {p0}, Lcom/samsung/recognitionengine/UserModelReader;->getCPtr(Lcom/samsung/recognitionengine/UserModelReader;)J

    move-result-wide v2

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->UserModel_readModel(JLcom/samsung/recognitionengine/UserModelReader;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/UserModel;-><init>(JZ)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_UserModel(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/UserModel;->delete()V

    .line 26
    return-void
.end method

.method public getSignaturesNumber()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->UserModel_getSignaturesNumber(JLcom/samsung/recognitionengine/UserModel;)J

    move-result-wide v0

    return-wide v0
.end method

.method public isValid()Z
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->UserModel_isValid(JLcom/samsung/recognitionengine/UserModel;)Z

    move-result v0

    return v0
.end method

.method public writeModel(Lcom/samsung/recognitionengine/UserModelWriter;)Z
    .locals 6

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModel;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/UserModelWriter;->getCPtr(Lcom/samsung/recognitionengine/UserModelWriter;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->UserModel_writeModel(JLcom/samsung/recognitionengine/UserModel;JLcom/samsung/recognitionengine/UserModelWriter;)Z

    move-result v0

    return v0
.end method
