.class public Lcom/samsung/recognitionengine/Trainer;
.super Ljava/lang/Object;
.source "Trainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;,
        Lcom/samsung/recognitionengine/Trainer$EResult;
    }
.end annotation


# static fields
.field public static final MAX_SIGNATURES_AMOUNT:I = 0x14

.field public static final MIN_NUMBER_OF_POINTS_IN_SIGNATURE:I = 0x5

.field public static final MIN_SIGNATURES_AMOUNT:I = 0x3


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_Trainer__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/Trainer;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/Trainer;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/UserModel;)V
    .locals 3

    .prologue
    .line 43
    invoke-static {p1}, Lcom/samsung/recognitionengine/UserModel;->getCPtr(Lcom/samsung/recognitionengine/UserModel;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_Trainer__SWIG_1(JLcom/samsung/recognitionengine/UserModel;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/Trainer;-><init>(JZ)V

    .line 44
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/Trainer;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public addSignature(Lcom/samsung/recognitionengine/Signature;)Lcom/samsung/recognitionengine/Trainer$EResult;
    .locals 6

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/Signature;->getCPtr(Lcom/samsung/recognitionengine/Signature;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_addSignature(JLcom/samsung/recognitionengine/Trainer;JLcom/samsung/recognitionengine/Signature;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/recognitionengine/Trainer$EResult;->swigToEnum(I)Lcom/samsung/recognitionengine/Trainer$EResult;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_Trainer(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/Trainer;->delete()V

    .line 26
    return-void
.end method

.method public getSignaturesNumber()J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_getSignaturesNumber(JLcom/samsung/recognitionengine/Trainer;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSimplicityLevel()Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_getSimplicityLevel(JLcom/samsung/recognitionengine/Trainer;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigToEnum(I)Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;

    move-result-object v0

    return-object v0
.end method

.method public getValidateNextSignature()Z
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_getValidateNextSignature(JLcom/samsung/recognitionengine/Trainer;)Z

    move-result v0

    return v0
.end method

.method public isSimplicityChecking()Z
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_isSimplicityChecking(JLcom/samsung/recognitionengine/Trainer;)Z

    move-result v0

    return v0
.end method

.method public setSimplicityChecking(Z)V
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_setSimplicityChecking(JLcom/samsung/recognitionengine/Trainer;Z)V

    .line 48
    return-void
.end method

.method public setSimplicityLevel(Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;)V
    .locals 3

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/recognitionengine/Trainer$SimplicityLevel;->swigValue()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_setSimplicityLevel(JLcom/samsung/recognitionengine/Trainer;I)V

    .line 56
    return-void
.end method

.method public setValidateNextSignature(Z)V
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_setValidateNextSignature(JLcom/samsung/recognitionengine/Trainer;Z)V

    .line 76
    return-void
.end method

.method public trainModel()Lcom/samsung/recognitionengine/UserModel;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lcom/samsung/recognitionengine/UserModel;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/Trainer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Trainer_trainModel(JLcom/samsung/recognitionengine/Trainer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/UserModel;-><init>(JZ)V

    return-object v0
.end method
