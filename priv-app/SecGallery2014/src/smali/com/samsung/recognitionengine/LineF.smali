.class public Lcom/samsung/recognitionengine/LineF;
.super Ljava/lang/Object;
.source "LineF.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_LineF__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/LineF;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/LineF;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/PointF;Lcom/samsung/recognitionengine/PointF;)V
    .locals 6

    .prologue
    .line 43
    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    invoke-static {p2}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_LineF__SWIG_1(JLcom/samsung/recognitionengine/PointF;JLcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/LineF;-><init>(JZ)V

    .line 44
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/LineF;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    goto :goto_0
.end method

.method public static getOverlappedLineSegment(Lcom/samsung/recognitionengine/LineF;Lcom/samsung/recognitionengine/LineF;)Lcom/samsung/recognitionengine/LineF;
    .locals 7

    .prologue
    .line 111
    new-instance v6, Lcom/samsung/recognitionengine/LineF;

    invoke-static {p0}, Lcom/samsung/recognitionengine/LineF;->getCPtr(Lcom/samsung/recognitionengine/LineF;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/samsung/recognitionengine/LineF;->getCPtr(Lcom/samsung/recognitionengine/LineF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getOverlappedLineSegment(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/LineF;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/LineF;-><init>(JZ)V

    return-object v6
.end method


# virtual methods
.method public containsPoint(Lcom/samsung/recognitionengine/PointF;)Z
    .locals 6

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_containsPoint(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/PointF;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_LineF(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Lcom/samsung/recognitionengine/LineF;)Z
    .locals 6

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/LineF;->getCPtr(Lcom/samsung/recognitionengine/LineF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_equals(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/LineF;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/LineF;->delete()V

    .line 26
    return-void
.end method

.method public getA()F
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getA(JLcom/samsung/recognitionengine/LineF;)F

    move-result v0

    return v0
.end method

.method public getB()F
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getB(JLcom/samsung/recognitionengine/LineF;)F

    move-result v0

    return v0
.end method

.method public getC()F
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getC(JLcom/samsung/recognitionengine/LineF;)F

    move-result v0

    return v0
.end method

.method public getCrossPoint(Lcom/samsung/recognitionengine/LineF;)Lcom/samsung/recognitionengine/PointF;
    .locals 7

    .prologue
    .line 59
    new-instance v6, Lcom/samsung/recognitionengine/PointF;

    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/LineF;->getCPtr(Lcom/samsung/recognitionengine/LineF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getCrossPoint(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/LineF;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    return-object v6
.end method

.method public getEndPoint()Lcom/samsung/recognitionengine/PointF;
    .locals 4

    .prologue
    .line 91
    new-instance v0, Lcom/samsung/recognitionengine/PointF;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getEndPoint(JLcom/samsung/recognitionengine/LineF;)J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    return-object v0
.end method

.method public getHalfPlaneIndex(Lcom/samsung/recognitionengine/PointF;)I
    .locals 6

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getHalfPlaneIndex(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/PointF;)I

    move-result v0

    return v0
.end method

.method public getLength()F
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getLength(JLcom/samsung/recognitionengine/LineF;)F

    move-result v0

    return v0
.end method

.method public getMiddlePoint()Lcom/samsung/recognitionengine/PointF;
    .locals 4

    .prologue
    .line 95
    new-instance v0, Lcom/samsung/recognitionengine/PointF;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getMiddlePoint(JLcom/samsung/recognitionengine/LineF;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    return-object v0
.end method

.method public getModuleAB()F
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getModuleAB(JLcom/samsung/recognitionengine/LineF;)F

    move-result v0

    return v0
.end method

.method public getParallelLine(Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/recognitionengine/LineF;
    .locals 7

    .prologue
    .line 67
    new-instance v6, Lcom/samsung/recognitionengine/LineF;

    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getParallelLine(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/LineF;-><init>(JZ)V

    return-object v6
.end method

.method public getPerpendicularLine(Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/recognitionengine/LineF;
    .locals 7

    .prologue
    .line 63
    new-instance v6, Lcom/samsung/recognitionengine/LineF;

    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getPerpendicularLine(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/LineF;-><init>(JZ)V

    return-object v6
.end method

.method public getPointProjection(Lcom/samsung/recognitionengine/PointF;)Lcom/samsung/recognitionengine/PointF;
    .locals 7

    .prologue
    .line 71
    new-instance v6, Lcom/samsung/recognitionengine/PointF;

    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getPointProjection(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/PointF;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    return-object v6
.end method

.method public getSignedDistanceToPoint(Lcom/samsung/recognitionengine/PointF;)F
    .locals 6

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getSignedDistanceToPoint(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/PointF;)F

    move-result v0

    return v0
.end method

.method public getStartPoint()Lcom/samsung/recognitionengine/PointF;
    .locals 4

    .prologue
    .line 87
    new-instance v0, Lcom/samsung/recognitionengine/PointF;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_getStartPoint(JLcom/samsung/recognitionengine/LineF;)J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    return-object v0
.end method

.method public hasIntersection(Lcom/samsung/recognitionengine/LineF;)Z
    .locals 6

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/LineF;->getCPtr(Lcom/samsung/recognitionengine/LineF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_hasIntersection(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/LineF;)Z

    move-result v0

    return v0
.end method

.method public isValid()Z
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_isValid(JLcom/samsung/recognitionengine/LineF;)Z

    move-result v0

    return v0
.end method

.method public squareDistanceToLine(Lcom/samsung/recognitionengine/PointF;)F
    .locals 6

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/recognitionengine/LineF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->LineF_squareDistanceToLine(JLcom/samsung/recognitionengine/LineF;JLcom/samsung/recognitionengine/PointF;)F

    move-result v0

    return v0
.end method
