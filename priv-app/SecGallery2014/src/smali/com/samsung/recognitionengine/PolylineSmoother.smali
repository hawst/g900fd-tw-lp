.class public Lcom/samsung/recognitionengine/PolylineSmoother;
.super Ljava/lang/Object;
.source "PolylineSmoother.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_PolylineSmoother()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/PolylineSmoother;-><init>(JZ)V

    .line 48
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCPtr:J

    .line 18
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/PolylineSmoother;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCPtr:J

    goto :goto_0
.end method

.method public static linearize(Lcom/samsung/recognitionengine/PointFVector;FFF)Lcom/samsung/recognitionengine/LinesFVector;
    .locals 7

    .prologue
    .line 43
    new-instance v6, Lcom/samsung/recognitionengine/LinesFVector;

    invoke-static {p0}, Lcom/samsung/recognitionengine/PointFVector;->getCPtr(Lcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v0

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->PolylineSmoother_linearize(JLcom/samsung/recognitionengine/PointFVector;FFF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/LinesFVector;-><init>(JZ)V

    return-object v6
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_PolylineSmoother(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/PolylineSmoother;->delete()V

    .line 26
    return-void
.end method

.method public smoothPolyline(Lcom/samsung/recognitionengine/PointFVector;)Lcom/samsung/recognitionengine/ShapeInfoVector;
    .locals 7

    .prologue
    .line 39
    new-instance v6, Lcom/samsung/recognitionengine/ShapeInfoVector;

    iget-wide v0, p0, Lcom/samsung/recognitionengine/PolylineSmoother;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointFVector;->getCPtr(Lcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->PolylineSmoother_smoothPolyline(JLcom/samsung/recognitionengine/PolylineSmoother;JLcom/samsung/recognitionengine/PointFVector;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/recognitionengine/ShapeInfoVector;-><init>(JZ)V

    return-object v6
.end method
