.class public final enum Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;
.super Ljava/lang/Enum;
.source "Verifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/recognitionengine/Verifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StrictnessLevel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/recognitionengine/Verifier$StrictnessLevel$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

.field public static final enum StrictnessLevel_High:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

.field public static final enum StrictnessLevel_Low:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

.field public static final enum StrictnessLevel_Medium:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    const-string v1, "StrictnessLevel_High"

    invoke-direct {v0, v1, v2}, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_High:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    .line 80
    new-instance v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    const-string v1, "StrictnessLevel_Medium"

    invoke-direct {v0, v1, v3}, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_Medium:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    .line 81
    new-instance v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    const-string v1, "StrictnessLevel_Low"

    invoke-direct {v0, v1, v4}, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_Low:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    .line 78
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    sget-object v1, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_High:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_Medium:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->StrictnessLevel_Low:Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->$VALUES:[Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 99
    # operator++ for: Lcom/samsung/recognitionengine/Verifier$StrictnessLevel$SwigNext;->next:I
    invoke-static {}, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel$SwigNext;->access$008()I

    move-result v0

    iput v0, p0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue:I

    .line 100
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 104
    iput p3, p0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue:I

    .line 105
    add-int/lit8 v0, p3, 0x1

    # setter for: Lcom/samsung/recognitionengine/Verifier$StrictnessLevel$SwigNext;->next:I
    invoke-static {v0}, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel$SwigNext;->access$002(I)I

    .line 106
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/samsung/recognitionengine/Verifier$StrictnessLevel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 110
    iget v0, p3, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue:I

    iput v0, p0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue:I

    .line 111
    iget v0, p0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue:I

    add-int/lit8 v0, v0, 0x1

    # setter for: Lcom/samsung/recognitionengine/Verifier$StrictnessLevel$SwigNext;->next:I
    invoke-static {v0}, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel$SwigNext;->access$002(I)I

    .line 112
    return-void
.end method

.method public static swigToEnum(I)Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;
    .locals 5

    .prologue
    .line 88
    const-class v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    .line 89
    array-length v1, v0

    if-ge p0, v1, :cond_0

    if-ltz p0, :cond_0

    aget-object v1, v0, p0

    iget v1, v1, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue:I

    if-ne v1, p0, :cond_0

    .line 90
    aget-object v0, v0, p0

    .line 93
    :goto_0
    return-object v0

    .line 91
    :cond_0
    array-length v3, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 92
    iget v4, v1, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue:I

    if-ne v4, p0, :cond_1

    move-object v0, v1

    .line 93
    goto :goto_0

    .line 91
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 94
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    return-object v0
.end method

.method public static values()[Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->$VALUES:[Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    invoke-virtual {v0}, [Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue:I

    return v0
.end method
