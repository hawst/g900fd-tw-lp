.class public Lcom/samsung/recognitionengine/Verifier;
.super Ljava/lang/Object;
.source "Verifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;
    }
.end annotation


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/Verifier;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/recognitionengine/UserModel;)V
    .locals 3

    .prologue
    .line 39
    invoke-static {p1}, Lcom/samsung/recognitionengine/UserModel;->getCPtr(Lcom/samsung/recognitionengine/UserModel;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_Verifier(JLcom/samsung/recognitionengine/UserModel;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/Verifier;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/Verifier;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_Verifier(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/Verifier;->delete()V

    .line 26
    return-void
.end method

.method public getModel()Lcom/samsung/recognitionengine/UserModel;
    .locals 4

    .prologue
    .line 75
    new-instance v0, Lcom/samsung/recognitionengine/UserModel;

    iget-wide v2, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_getModel(JLcom/samsung/recognitionengine/Verifier;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/UserModel;-><init>(JZ)V

    return-object v0
.end method

.method public getStrictnessLevel()Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_getStrictnessLevel(JLcom/samsung/recognitionengine/Verifier;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigToEnum(I)Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;

    move-result-object v0

    return-object v0
.end method

.method public isAddExtraSignatures()Z
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_isAddExtraSignatures(JLcom/samsung/recognitionengine/Verifier;)Z

    move-result v0

    return v0
.end method

.method public isAddExtraSignaturesToFullModel()Z
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_isAddExtraSignaturesToFullModel(JLcom/samsung/recognitionengine/Verifier;)Z

    move-result v0

    return v0
.end method

.method public isAuthentic(Lcom/samsung/recognitionengine/Signature;)Z
    .locals 6

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/Signature;->getCPtr(Lcom/samsung/recognitionengine/Signature;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_isAuthentic(JLcom/samsung/recognitionengine/Verifier;JLcom/samsung/recognitionengine/Signature;)Z

    move-result v0

    return v0
.end method

.method public isModelChanged()Z
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_isModelChanged(JLcom/samsung/recognitionengine/Verifier;)Z

    move-result v0

    return v0
.end method

.method public setAddExtraSignatures(Z)V
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_setAddExtraSignatures(JLcom/samsung/recognitionengine/Verifier;Z)V

    .line 56
    return-void
.end method

.method public setAddExtraSignaturesToFullModel(Z)V
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_setAddExtraSignaturesToFullModel(JLcom/samsung/recognitionengine/Verifier;Z)V

    .line 64
    return-void
.end method

.method public setStrictnessLevel(Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;)V
    .locals 3

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/recognitionengine/Verifier;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/recognitionengine/Verifier$StrictnessLevel;->swigValue()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->Verifier_setStrictnessLevel(JLcom/samsung/recognitionengine/Verifier;I)V

    .line 48
    return-void
.end method
