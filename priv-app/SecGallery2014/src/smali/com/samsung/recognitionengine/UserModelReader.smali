.class public Lcom/samsung/recognitionengine/UserModelReader;
.super Ljava/lang/Object;
.source "UserModelReader.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCPtr:J

    .line 18
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/UserModelReader;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_UserModelReader(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/UserModelReader;->delete()V

    .line 26
    return-void
.end method

.method public read(Lcom/samsung/recognitionengine/FloatVector;)Z
    .locals 6

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/FloatVector;->getCPtr(Lcom/samsung/recognitionengine/FloatVector;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->UserModelReader_read__SWIG_0(JLcom/samsung/recognitionengine/UserModelReader;JLcom/samsung/recognitionengine/FloatVector;)Z

    move-result v0

    return v0
.end method

.method public read(Lcom/samsung/recognitionengine/UInt32Vector;)Z
    .locals 6

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/recognitionengine/UserModelReader;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/UInt32Vector;->getCPtr(Lcom/samsung/recognitionengine/UInt32Vector;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->UserModelReader_read__SWIG_1(JLcom/samsung/recognitionengine/UserModelReader;JLcom/samsung/recognitionengine/UInt32Vector;)Z

    move-result v0

    return v0
.end method
