.class public Lcom/samsung/recognitionengine/PointF;
.super Ljava/lang/Object;
.source "PointF.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 43
    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_PointF__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    .line 44
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 3

    .prologue
    .line 47
    invoke-static {p1, p2}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->new_PointF__SWIG_1(FF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    .line 48
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/samsung/recognitionengine/PointF;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    .line 18
    return-void
.end method

.method protected static getCPtr(Lcom/samsung/recognitionengine/PointF;)J
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    goto :goto_0
.end method

.method public static infinitePoint()Lcom/samsung/recognitionengine/PointF;
    .locals 4

    .prologue
    .line 39
    new-instance v0, Lcom/samsung/recognitionengine/PointF;

    invoke-static {}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->PointF_infinitePoint()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/recognitionengine/PointF;-><init>(JZ)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->delete_PointF(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Lcom/samsung/recognitionengine/PointF;)Z
    .locals 6

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/recognitionengine/PointF;->getCPtr(Lcom/samsung/recognitionengine/PointF;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->PointF_equals(JLcom/samsung/recognitionengine/PointF;JLcom/samsung/recognitionengine/PointF;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/recognitionengine/PointF;->delete()V

    .line 26
    return-void
.end method

.method public getX()F
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->PointF_getX(JLcom/samsung/recognitionengine/PointF;)F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->PointF_getY(JLcom/samsung/recognitionengine/PointF;)F

    move-result v0

    return v0
.end method

.method public setX(F)V
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->PointF_setX(JLcom/samsung/recognitionengine/PointF;F)V

    .line 60
    return-void
.end method

.method public setY(F)V
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/recognitionengine/PointF;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/recognitionengine/RecognitionEngineJNI;->PointF_setY(JLcom/samsung/recognitionengine/PointF;F)V

    .line 64
    return-void
.end method
