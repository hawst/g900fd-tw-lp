.class public Lcom/dmc/ocr/OcrMovementDetector;
.super Ljava/lang/Object;
.source "OcrMovementDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dmc/ocr/OcrMovementDetector$SimpleOnGestureListener;,
        Lcom/dmc/ocr/OcrMovementDetector$OcrMovementDetectorListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "OcrMovementDetector"


# instance fields
.field private final DELTA:I

.field private final OCR_MINIMUM_DETECT_AREA:I

.field private mContext:Landroid/content/Context;

.field private mDetectListener:Lcom/dmc/ocr/OcrMovementDetector$OcrMovementDetectorListener;

.field private mMaxX:I

.field private mPressed:Z

.field private mStartX:I

.field private mStartY:I

.field private mStatusBarHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v1, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartX:I

    .line 18
    iput v1, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartY:I

    .line 19
    iput v1, p0, Lcom/dmc/ocr/OcrMovementDetector;->mMaxX:I

    .line 20
    const/16 v0, 0x1e

    iput v0, p0, Lcom/dmc/ocr/OcrMovementDetector;->OCR_MINIMUM_DETECT_AREA:I

    .line 22
    iput-boolean v1, p0, Lcom/dmc/ocr/OcrMovementDetector;->mPressed:Z

    .line 24
    iput v1, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStatusBarHeight:I

    .line 25
    const/16 v0, 0xa

    iput v0, p0, Lcom/dmc/ocr/OcrMovementDetector;->DELTA:I

    .line 29
    iput-object p1, p0, Lcom/dmc/ocr/OcrMovementDetector;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public onStylusButtonEvent(Landroid/view/MotionEvent;I)V
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "clipboardId"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 33
    const-string v6, "OcrMovementDetector"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EVENT :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",clipboardId:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 37
    .local v0, "action":I
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    const/4 v7, 0x2

    if-eq v6, v7, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    const-string v6, "OcrMovementDetector"

    const-string v7, "onStylusButtonEvent start"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    packed-switch v0, :pswitch_data_0

    .line 124
    :cond_2
    :goto_1
    const-string v6, "OcrMovementDetector"

    const-string v7, "onStylusButtonEvent END"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 45
    :pswitch_0
    iget-boolean v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mPressed:Z

    if-ne v6, v10, :cond_2

    .line 46
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mMaxX:I

    int-to-float v6, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_3

    .line 47
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    iput v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mMaxX:I

    goto :goto_1

    .line 48
    :cond_3
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mMaxX:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    sub-int/2addr v6, v7

    const/16 v7, 0xa

    if-le v6, v7, :cond_2

    .line 49
    const-string v6, "OcrMovementDetector"

    const-string v7, "TextSelection is canceled because of wrong X position"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iput-boolean v9, p0, Lcom/dmc/ocr/OcrMovementDetector;->mPressed:Z

    goto :goto_1

    .line 56
    :pswitch_1
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStatusBarHeight:I

    if-nez v6, :cond_4

    .line 57
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 58
    .local v5, "rectgle":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/dmc/ocr/OcrMovementDetector;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    .line 59
    .local v1, "activity":Landroid/app/Activity;
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 60
    iget v6, v5, Landroid/graphics/Rect;->top:I

    iput v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStatusBarHeight:I

    .line 63
    .end local v1    # "activity":Landroid/app/Activity;
    .end local v5    # "rectgle":Landroid/graphics/Rect;
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    iput v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartX:I

    .line 64
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartX:I

    iput v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mMaxX:I

    .line 65
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    iget v7, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStatusBarHeight:I

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartY:I

    .line 66
    iput-boolean v10, p0, Lcom/dmc/ocr/OcrMovementDetector;->mPressed:Z

    goto :goto_1

    .line 71
    :pswitch_2
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 72
    .local v4, "rect":Landroid/graphics/Rect;
    const/4 v2, 0x0

    .line 73
    .local v2, "endX":I
    const/4 v3, 0x0

    .line 75
    .local v3, "endY":I
    iget-boolean v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mPressed:Z

    if-eqz v6, :cond_0

    .line 78
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 81
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartX:I

    if-gez v6, :cond_5

    .line 82
    iput v9, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartX:I

    .line 84
    :cond_5
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartY:I

    if-gez v6, :cond_6

    .line 85
    iput v9, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartY:I

    .line 87
    :cond_6
    if-gez v2, :cond_7

    .line 88
    const/4 v2, 0x0

    .line 90
    :cond_7
    if-gez v3, :cond_8

    .line 91
    const/4 v3, 0x0

    .line 94
    :cond_8
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartX:I

    if-le v6, v2, :cond_9

    .line 95
    iput v2, v4, Landroid/graphics/Rect;->left:I

    .line 96
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartX:I

    iput v6, v4, Landroid/graphics/Rect;->right:I

    .line 102
    :goto_2
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartY:I

    if-lez v6, :cond_a

    .line 103
    iput v3, v4, Landroid/graphics/Rect;->top:I

    .line 104
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartY:I

    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    .line 110
    :goto_3
    iget v6, v4, Landroid/graphics/Rect;->right:I

    iget v7, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    const/16 v7, 0x1e

    if-lt v6, v7, :cond_0

    .line 113
    iget-object v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mDetectListener:Lcom/dmc/ocr/OcrMovementDetector$OcrMovementDetectorListener;

    invoke-interface {v6, v4}, Lcom/dmc/ocr/OcrMovementDetector$OcrMovementDetectorListener;->onRecognize(Landroid/graphics/Rect;)Z

    .line 115
    iput-boolean v9, p0, Lcom/dmc/ocr/OcrMovementDetector;->mPressed:Z

    goto/16 :goto_1

    .line 98
    :cond_9
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartX:I

    iput v6, v4, Landroid/graphics/Rect;->left:I

    .line 99
    iput v2, v4, Landroid/graphics/Rect;->right:I

    goto :goto_2

    .line 106
    :cond_a
    iget v6, p0, Lcom/dmc/ocr/OcrMovementDetector;->mStartY:I

    iput v6, v4, Landroid/graphics/Rect;->top:I

    .line 107
    iput v3, v4, Landroid/graphics/Rect;->bottom:I

    goto :goto_3

    .line 121
    .end local v2    # "endX":I
    .end local v3    # "endY":I
    .end local v4    # "rect":Landroid/graphics/Rect;
    :pswitch_3
    iput-boolean v9, p0, Lcom/dmc/ocr/OcrMovementDetector;->mPressed:Z

    goto/16 :goto_1

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setMoveDetectorListener(Lcom/dmc/ocr/OcrMovementDetector$OcrMovementDetectorListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/dmc/ocr/OcrMovementDetector$OcrMovementDetectorListener;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/dmc/ocr/OcrMovementDetector;->mDetectListener:Lcom/dmc/ocr/OcrMovementDetector$OcrMovementDetectorListener;

    .line 130
    return-void
.end method
