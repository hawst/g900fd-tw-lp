.class public Lcom/dmc/ocr/SecMOCR;
.super Ljava/lang/Object;
.source "SecMOCR.java"


# static fields
.field public static final MAX_RECOG_SIZE_8M:I = 0x79ec00

.field public static final MOCR_LANG_AZERBAIJANI:I = 0x1d

.field public static final MOCR_LANG_BASQUE:I = 0x1e

.field public static final MOCR_LANG_BULGARIAN:I = 0x24

.field public static final MOCR_LANG_CATALAN:I = 0xc

.field public static final MOCR_LANG_CHN_MIXED:I = 0x6

.field public static final MOCR_LANG_CHN_SIM:I = 0x7

.field public static final MOCR_LANG_CHN_TRD:I = 0x8

.field public static final MOCR_LANG_CROATIAN:I = 0xd

.field public static final MOCR_LANG_CZECH:I = 0xe

.field public static final MOCR_LANG_DANISH:I = 0xf

.field public static final MOCR_LANG_DUTCH:I = 0x11

.field public static final MOCR_LANG_END:I = 0x2a

.field public static final MOCR_LANG_ENG:I = 0x0

.field public static final MOCR_LANG_ESP:I = 0x4

.field public static final MOCR_LANG_ESTONIAN:I = 0x12

.field public static final MOCR_LANG_FINNISH:I = 0x13

.field public static final MOCR_LANG_FRA:I = 0x1

.field public static final MOCR_LANG_GALICIAN:I = 0x1f

.field public static final MOCR_LANG_GER:I = 0x2

.field public static final MOCR_LANG_HANGUL:I = 0x5

.field public static final MOCR_LANG_HUNGARIAN:I = 0x14

.field public static final MOCR_LANG_ICELANDIC:I = 0x20

.field public static final MOCR_LANG_INDONESIAN:I = 0x15

.field public static final MOCR_LANG_IRISH:I = 0x21

.field public static final MOCR_LANG_ITA:I = 0x3

.field public static final MOCR_LANG_JAPANESE:I = 0x9

.field public static final MOCR_LANG_LATVIAN:I = 0x16

.field public static final MOCR_LANG_LITHUANIAN:I = 0x17

.field public static final MOCR_LANG_MACEDONIAN:I = 0x27

.field public static final MOCR_LANG_MALAY:I = 0x22

.field public static final MOCR_LANG_NORWEGIAN:I = 0x10

.field public static final MOCR_LANG_POLISH:I = 0x18

.field public static final MOCR_LANG_PORTUGUESE:I = 0xa

.field public static final MOCR_LANG_ROMANIAN:I = 0x19

.field public static final MOCR_LANG_RUSSIAN:I = 0x23

.field public static final MOCR_LANG_SERBIAN_CYR:I = 0x28

.field public static final MOCR_LANG_SLOVAK:I = 0x1a

.field public static final MOCR_LANG_SLOVENIAN:I = 0x1b

.field public static final MOCR_LANG_SWEDISH:I = 0x1c

.field public static final MOCR_LANG_TATAR:I = 0x25

.field public static final MOCR_LANG_TURKISH:I = 0xb

.field public static final MOCR_LANG_UKRAINIAN:I = 0x26

.field public static final MOCR_LANG_UZBEK_LAT:I = 0x29

.field public static final MOCR_RECOG_MODE_CAPTURED:I = 0x1

.field public static final MOCR_RECOG_MODE_PREV:I = 0x0

.field public static final MOCR_RECOG_TYPE_HTML:I = 0x0

.field public static final MOCR_RECOG_TYPE_NORM:I = 0x2

.field public static final MOCR_RECOG_TYPE_TRANS:I = 0x1

.field public static final RECOG_COMPLETE:I = 0x0

.field public static final RECOG_FAIL:I = 0x3

.field public static final RECOG_INIT_FAIL:I = 0x2

.field private static RECOG_LANGUAGE:[I = null

.field private static RECOG_MODE:I = 0x0

.field public static final RECOG_OK:I = 0x0

.field public static final RECOG_PROCESSING:I = 0x1

.field private static RECOG_TYPE:I

.field private static final TAG:Ljava/lang/String;

.field private static mImageData:[I

.field private static mPreviewData:[B

.field private static mProgressCallback:Landroid/os/Handler;

.field private static mRegionBottom:I

.field private static mRegionLeft:I

.field private static mRegionRight:I

.field private static mRegionTop:I

.field private static mjpegHeight:I

.field private static mjpegWidth:I

.field private static mpreviewHeight:I

.field private static mpreviewSensorHeight:I

.field private static mpreviewSensorWidth:I

.field private static mpreviewWidth:I

.field private static volatile secMOCR:Lcom/dmc/ocr/SecMOCR;


# instance fields
.field private final lock:Ljava/util/concurrent/locks/Lock;

.field private mCancelCallbackFlag:I

.field public mLinePerWordNum:[I

.field public mLookUpWord:Ljava/lang/String;

.field public mLookUpWordNum:I

.field public mLookUpWordRect:[Landroid/graphics/Rect;

.field protected mOCRState:I

.field private mProgPos:I

.field public mSpecialWordNum:I

.field public mSpecialWordRect:[Landroid/graphics/Rect;

.field public mSpecialWordText:[Ljava/lang/String;

.field public mSpecialWordType:[I

.field private mStrDBPath:Ljava/lang/String;

.field public mWholeWordNum:I

.field public mWholeWordRect:[Landroid/graphics/Rect;

.field public mWholeWordText:[Ljava/lang/String;

.field public mWholeWordType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    const-class v1, Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    .line 103
    sput v2, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    .line 104
    const/4 v1, 0x2

    sput v1, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    .line 105
    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    .line 112
    sput v2, Lcom/dmc/ocr/SecMOCR;->mRegionLeft:I

    .line 113
    sput v2, Lcom/dmc/ocr/SecMOCR;->mRegionTop:I

    .line 114
    sput v2, Lcom/dmc/ocr/SecMOCR;->mRegionRight:I

    .line 115
    sput v2, Lcom/dmc/ocr/SecMOCR;->mRegionBottom:I

    .line 465
    :try_start_0
    const-string v1, "mOCR_Lib"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 466
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 467
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 105
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x2a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    .line 56
    const/4 v0, 0x3

    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dmc/ocr/SecMOCR;->mStrDBPath:Ljava/lang/String;

    .line 142
    const/4 v0, 0x0

    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mCancelCallbackFlag:I

    return-void
.end method

.method public static getInstance()Lcom/dmc/ocr/SecMOCR;
    .locals 2

    .prologue
    .line 151
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->secMOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v0, :cond_1

    .line 152
    const-class v1, Lcom/dmc/ocr/SecMOCR;

    monitor-enter v1

    .line 153
    :try_start_0
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->secMOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Lcom/dmc/ocr/SecMOCR;

    invoke-direct {v0}, Lcom/dmc/ocr/SecMOCR;-><init>()V

    sput-object v0, Lcom/dmc/ocr/SecMOCR;->secMOCR:Lcom/dmc/ocr/SecMOCR;

    .line 155
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    :cond_1
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->secMOCR:Lcom/dmc/ocr/SecMOCR;

    return-object v0

    .line 155
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public native MOCR_Close()V
.end method

.method public native MOCR_GetLinePerWordCount()[I
.end method

.method public native MOCR_GetSelectedRecogWord()Ljava/lang/String;
.end method

.method public native MOCR_GetSelectedRecogWordRect()[I
.end method

.method public native MOCR_GetSpecialWord()[Ljava/lang/String;
.end method

.method public native MOCR_GetSpecialWordRect()[I
.end method

.method public native MOCR_GetSpecialWordType()[I
.end method

.method public native MOCR_GetWholeWord()[Ljava/lang/String;
.end method

.method public native MOCR_GetWholeWordRect()[I
.end method

.method public native MOCR_GetWholeWordType()[I
.end method

.method public native MOCR_Init([IILjava/lang/String;Lcom/dmc/ocr/SecMOCR;)I
.end method

.method public native MOCR_LookupSpecialWord()I
.end method

.method public native MOCR_LookupWholeWord()I
.end method

.method public native MOCR_LookupWord(Ljava/lang/String;)[I
.end method

.method public native MOCR_Recognize_Image([IIIIIII)I
.end method

.method public native MOCR_Recognize_Preview([BIIIIII)I
.end method

.method public native MOCR_SetSelectedRecogWord_ByPoint(IIIIII)Z
.end method

.method public getLookUpWord(Ljava/lang/String;)I
    .locals 5
    .param p1, "nWord"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 371
    iget v2, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    if-eqz v2, :cond_1

    .line 372
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v3, "[getLookUpWord()] first, do recognize!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    iput v1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 374
    iget v1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 405
    :cond_0
    :goto_0
    return v1

    .line 377
    :cond_1
    iput-object p1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWord:Ljava/lang/String;

    .line 378
    iput v1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 379
    iget v2, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    new-array v2, v2, [Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    .line 381
    sget-object v2, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[getLookUpWord] => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWord:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 386
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    if-ge v0, v1, :cond_2

    .line 387
    iget-object v1, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    aget-object v1, v1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(?i).*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 388
    iget-object v1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    iget v2, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    iget-object v3, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    aget-object v3, v3, v0

    aput-object v3, v1, v2

    .line 389
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[getLookUpWord("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <== match "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mLookUpWordRect("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    iget v4, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    iget v4, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    iget v4, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordRect:[Landroid/graphics/Rect;

    iget v4, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget v1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 400
    :goto_2
    iget v1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    iget v2, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    if-lt v1, v2, :cond_4

    .line 403
    :cond_2
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v2, "[getLookUpWord()] END"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    iget v1, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    goto/16 :goto_0

    .line 397
    :cond_3
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[getLookUpWord("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <== No match "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 386
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method public getRecogResult()I
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 257
    iget v4, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    if-eqz v4, :cond_0

    .line 258
    sget-object v4, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v5, "[getRecogResult()] first, do recognize!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    iput v12, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 260
    iget v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 304
    :goto_0
    return v4

    .line 263
    :cond_0
    sget-object v4, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v5, "[getRecogResult()] START"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_LookupSpecialWord()I

    move-result v4

    if-nez v4, :cond_5

    .line 266
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetSpecialWord()[Ljava/lang/String;

    move-result-object v2

    .line 267
    .local v2, "specialWord":[Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 268
    array-length v4, v2

    iput v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 269
    array-length v4, v2

    new-array v4, v4, [Ljava/lang/String;

    iput-object v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    .line 270
    iget-object v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    array-length v5, v2

    invoke-static {v2, v12, v4, v12, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 273
    :cond_1
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetSpecialWordRect()[I

    move-result-object v1

    .line 274
    .local v1, "nSpecialWordRect":[I
    if-eqz v1, :cond_3

    .line 275
    array-length v4, v1

    div-int/lit8 v4, v4, 0x4

    new-array v4, v4, [Landroid/graphics/Rect;

    iput-object v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    .line 276
    sget v4, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v4, :cond_2

    .line 277
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v4, v1

    if-ge v0, v4, :cond_3

    .line 278
    iget-object v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v5, v0, 0x4

    new-instance v6, Landroid/graphics/Rect;

    aget v7, v1, v0

    sget v8, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v7, v8

    sget v8, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v7, v8

    add-int/lit8 v8, v0, 0x1

    aget v8, v1, v8

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v8, v9

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v8, v9

    add-int/lit8 v9, v0, 0x2

    aget v9, v1, v9

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v9, v10

    add-int/lit8 v10, v0, 0x3

    aget v10, v1, v10

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v10, v11

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, v4, v5

    .line 277
    add-int/lit8 v0, v0, 0x4

    goto :goto_1

    .line 284
    .end local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    array-length v4, v1

    if-ge v0, v4, :cond_3

    .line 285
    iget-object v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v5, v0, 0x4

    new-instance v6, Landroid/graphics/Rect;

    aget v7, v1, v0

    add-int/lit8 v8, v0, 0x1

    aget v8, v1, v8

    add-int/lit8 v9, v0, 0x2

    aget v9, v1, v9

    add-int/lit8 v10, v0, 0x3

    aget v10, v1, v10

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v6, v4, v5

    .line 284
    add-int/lit8 v0, v0, 0x4

    goto :goto_2

    .line 293
    .end local v0    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetSpecialWordType()[I

    move-result-object v3

    .line 294
    .local v3, "type":[I
    if-eqz v3, :cond_4

    .line 295
    array-length v4, v3

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    .line 296
    iget-object v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    array-length v5, v3

    invoke-static {v3, v12, v4, v12, v5}, Ljava/lang/System;->arraycopy([II[III)V

    .line 302
    .end local v1    # "nSpecialWordRect":[I
    .end local v2    # "specialWord":[Ljava/lang/String;
    .end local v3    # "type":[I
    :cond_4
    :goto_3
    sget-object v4, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v5, "[getRecogResult()] END"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    iget v4, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    goto/16 :goto_0

    .line 299
    :cond_5
    iput v12, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    goto :goto_3
.end method

.method public getRecognitionMode()I
    .locals 1

    .prologue
    .line 204
    sget v0, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    return v0
.end method

.method public getRecognitionState()I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    return v0
.end method

.method public getWholeRecogResult()I
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 312
    iget v5, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    if-eqz v5, :cond_0

    .line 313
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v6, "[getWholeRecogResult()] first, do recognize!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    iput v13, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    .line 315
    iget v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    .line 366
    :goto_0
    return v5

    .line 318
    :cond_0
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v6, "[getWholeRecogResult()] START"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_LookupWholeWord()I

    move-result v5

    if-nez v5, :cond_6

    .line 321
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetWholeWord()[Ljava/lang/String;

    move-result-object v4

    .line 322
    .local v4, "wholeWord":[Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 323
    array-length v5, v4

    iput v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    .line 324
    array-length v5, v4

    new-array v5, v5, [Ljava/lang/String;

    iput-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    .line 325
    iget-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    array-length v6, v4

    invoke-static {v4, v13, v5, v13, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 328
    :cond_1
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetWholeWordRect()[I

    move-result-object v2

    .line 329
    .local v2, "nwholeWordRect":[I
    if-eqz v2, :cond_3

    .line 330
    array-length v5, v2

    div-int/lit8 v5, v5, 0x4

    new-array v5, v5, [Landroid/graphics/Rect;

    iput-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    .line 332
    sget v5, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v5, :cond_2

    .line 333
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v5, v2

    if-ge v0, v5, :cond_3

    .line 334
    iget-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v6, v0, 0x4

    new-instance v7, Landroid/graphics/Rect;

    aget v8, v2, v0

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v8, v9

    sget v9, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v8, v9

    add-int/lit8 v9, v0, 0x1

    aget v9, v2, v9

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v9, v10

    sget v10, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v9, v10

    add-int/lit8 v10, v0, 0x2

    aget v10, v2, v10

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v10, v11

    sget v11, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v10, v11

    add-int/lit8 v11, v0, 0x3

    aget v11, v2, v11

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    mul-int/2addr v11, v12

    sget v12, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    div-int/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v7, v5, v6

    .line 333
    add-int/lit8 v0, v0, 0x4

    goto :goto_1

    .line 340
    .end local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    array-length v5, v2

    if-ge v0, v5, :cond_3

    .line 341
    iget-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordRect:[Landroid/graphics/Rect;

    div-int/lit8 v6, v0, 0x4

    new-instance v7, Landroid/graphics/Rect;

    aget v8, v2, v0

    add-int/lit8 v9, v0, 0x1

    aget v9, v2, v9

    add-int/lit8 v10, v0, 0x2

    aget v10, v2, v10

    add-int/lit8 v11, v0, 0x3

    aget v11, v2, v11

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v7, v5, v6

    .line 340
    add-int/lit8 v0, v0, 0x4

    goto :goto_2

    .line 349
    .end local v0    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetWholeWordType()[I

    move-result-object v3

    .line 350
    .local v3, "type":[I
    if-eqz v3, :cond_4

    .line 351
    array-length v5, v3

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordType:[I

    .line 352
    iget-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordType:[I

    array-length v6, v3

    invoke-static {v3, v13, v5, v13, v6}, Ljava/lang/System;->arraycopy([II[III)V

    .line 355
    :cond_4
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_GetLinePerWordCount()[I

    move-result-object v1

    .line 356
    .local v1, "nlinePerWord":[I
    if-eqz v1, :cond_5

    .line 357
    array-length v5, v1

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    .line 358
    iget-object v5, p0, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    array-length v6, v1

    invoke-static {v1, v13, v5, v13, v6}, Ljava/lang/System;->arraycopy([II[III)V

    .line 364
    .end local v1    # "nlinePerWord":[I
    .end local v2    # "nwholeWordRect":[I
    .end local v3    # "type":[I
    .end local v4    # "wholeWord":[Ljava/lang/String;
    :cond_5
    :goto_3
    sget-object v5, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v6, "[getWholeRecogResult()] END"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget v5, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    goto/16 :goto_0

    .line 361
    :cond_6
    iput v13, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    goto :goto_3
.end method

.method public recogCancel()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/dmc/ocr/SecMOCR;->mCancelCallbackFlag:I

    return v0
.end method

.method public recogProgress(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 163
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "===> recogProgress    : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return-void
.end method

.method public resetRecogResult()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 250
    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    .line 251
    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    .line 252
    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mLookUpWordNum:I

    .line 253
    return-void
.end method

.method public setCancelCallbackFlag(Z)V
    .locals 1
    .param p1, "bFlag"    # Z

    .prologue
    .line 183
    if-eqz p1, :cond_0

    .line 184
    const/4 v0, 0x1

    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mCancelCallbackFlag:I

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mCancelCallbackFlag:I

    goto :goto_0
.end method

.method public setDataBasePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "strPath"    # Ljava/lang/String;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/dmc/ocr/SecMOCR;->mStrDBPath:Ljava/lang/String;

    .line 214
    return-void
.end method

.method public setJPEGData([IIIIIII)Z
    .locals 3
    .param p1, "ImgData"    # [I
    .param p2, "nImgWidth"    # I
    .param p3, "nImgHeight"    # I
    .param p4, "nRegionLeft"    # I
    .param p5, "nRegionTop"    # I
    .param p6, "nRegionRight"    # I
    .param p7, "nRegionBottom"    # I

    .prologue
    .line 218
    array-length v0, p1

    const v1, 0x79ec00

    if-le v0, v1, :cond_0

    .line 219
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exceeded max jpeg size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v0, 0x0

    .line 232
    :goto_0
    return v0

    .line 223
    :cond_0
    sput-object p1, Lcom/dmc/ocr/SecMOCR;->mImageData:[I

    .line 224
    sput p2, Lcom/dmc/ocr/SecMOCR;->mjpegWidth:I

    .line 225
    sput p3, Lcom/dmc/ocr/SecMOCR;->mjpegHeight:I

    .line 227
    sput p4, Lcom/dmc/ocr/SecMOCR;->mRegionLeft:I

    .line 228
    sput p5, Lcom/dmc/ocr/SecMOCR;->mRegionTop:I

    .line 229
    sput p6, Lcom/dmc/ocr/SecMOCR;->mRegionRight:I

    .line 230
    sput p7, Lcom/dmc/ocr/SecMOCR;->mRegionBottom:I

    .line 232
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPreviewData([BIIIIIIII)V
    .locals 0
    .param p1, "PrevData"    # [B
    .param p2, "nPrevSensorWidth"    # I
    .param p3, "nPrevSensorHeight"    # I
    .param p4, "nPrevWidth"    # I
    .param p5, "nPrevHeight"    # I
    .param p6, "nRegionLeft"    # I
    .param p7, "nRegionTop"    # I
    .param p8, "nRegionRight"    # I
    .param p9, "nRegionBottom"    # I

    .prologue
    .line 237
    sput-object p1, Lcom/dmc/ocr/SecMOCR;->mPreviewData:[B

    .line 238
    sput p2, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    .line 239
    sput p3, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorHeight:I

    .line 240
    sput p4, Lcom/dmc/ocr/SecMOCR;->mpreviewWidth:I

    .line 241
    sput p5, Lcom/dmc/ocr/SecMOCR;->mpreviewHeight:I

    .line 243
    sput p6, Lcom/dmc/ocr/SecMOCR;->mRegionLeft:I

    .line 244
    sput p7, Lcom/dmc/ocr/SecMOCR;->mRegionTop:I

    .line 245
    sput p8, Lcom/dmc/ocr/SecMOCR;->mRegionRight:I

    .line 246
    sput p9, Lcom/dmc/ocr/SecMOCR;->mRegionBottom:I

    .line 247
    return-void
.end method

.method public setProgressCallback(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 175
    sput-object p1, Lcom/dmc/ocr/SecMOCR;->mProgressCallback:Landroid/os/Handler;

    .line 176
    return-void
.end method

.method public setRecognitionLanguage([I)V
    .locals 3
    .param p1, "lang"    # [I

    .prologue
    const/4 v2, 0x0

    .line 200
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    const/4 v1, 0x5

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy([II[III)V

    .line 201
    return-void
.end method

.method public setRecognitionMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 192
    sput p1, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    .line 193
    return-void
.end method

.method public setRecognitionType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 196
    sput p1, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    .line 197
    return-void
.end method

.method public startRecognition()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    .line 409
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "[startRecognize()] START"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    const/4 v9, 0x0

    .line 413
    .local v9, "nRet":I
    iget v0, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    if-ne v0, v3, :cond_0

    .line 414
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "[startRecognize()] error!! now processing...."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :goto_0
    return-void

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 421
    const/4 v0, 0x1

    :try_start_0
    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 423
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->RECOG_LANGUAGE:[I

    sget v1, Lcom/dmc/ocr/SecMOCR;->RECOG_TYPE:I

    iget-object v2, p0, Lcom/dmc/ocr/SecMOCR;->mStrDBPath:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_Init([IILjava/lang/String;Lcom/dmc/ocr/SecMOCR;)I

    move-result v9

    .line 425
    if-eqz v9, :cond_1

    .line 426
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Initial Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    const/4 v0, 0x2

    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 428
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V

    .line 431
    :cond_1
    sget v0, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-nez v0, :cond_3

    .line 432
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->mPreviewData:[B

    sget v2, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorWidth:I

    sget v3, Lcom/dmc/ocr/SecMOCR;->mpreviewSensorHeight:I

    sget v4, Lcom/dmc/ocr/SecMOCR;->mRegionLeft:I

    sget v5, Lcom/dmc/ocr/SecMOCR;->mRegionTop:I

    sget v6, Lcom/dmc/ocr/SecMOCR;->mRegionRight:I

    sget v7, Lcom/dmc/ocr/SecMOCR;->mRegionBottom:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/dmc/ocr/SecMOCR;->MOCR_Recognize_Preview([BIIIIII)I

    move-result v9

    .line 439
    :goto_1
    if-nez v9, :cond_5

    .line 440
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recognization success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    const/4 v0, 0x0

    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 457
    :goto_3
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string v1, "[startRecognize()] END"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 433
    :cond_3
    :try_start_1
    sget v0, Lcom/dmc/ocr/SecMOCR;->RECOG_MODE:I

    if-ne v0, v3, :cond_4

    .line 434
    sget-object v1, Lcom/dmc/ocr/SecMOCR;->mImageData:[I

    sget v2, Lcom/dmc/ocr/SecMOCR;->mjpegWidth:I

    sget v3, Lcom/dmc/ocr/SecMOCR;->mjpegHeight:I

    sget v4, Lcom/dmc/ocr/SecMOCR;->mRegionLeft:I

    sget v5, Lcom/dmc/ocr/SecMOCR;->mRegionTop:I

    sget v6, Lcom/dmc/ocr/SecMOCR;->mRegionRight:I

    sget v7, Lcom/dmc/ocr/SecMOCR;->mRegionBottom:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/dmc/ocr/SecMOCR;->MOCR_Recognize_Image([IIIIIII)I

    move-result v9

    goto :goto_1

    .line 436
    :cond_4
    const/4 v9, 0x1

    goto :goto_1

    .line 442
    :cond_5
    if-eqz v9, :cond_2

    .line 443
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recognization fail : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v0, 0x3

    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 445
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 448
    :catch_0
    move-exception v8

    .line 449
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 450
    sget-object v0, Lcom/dmc/ocr/SecMOCR;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startRecognize() Exception error!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    const/4 v0, 0x3

    iput v0, p0, Lcom/dmc/ocr/SecMOCR;->mOCRState:I

    .line 452
    invoke-virtual {p0}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 454
    iget-object v0, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/dmc/ocr/SecMOCR;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
