.class public Lcom/dmc/ocr/OcrRecognizer;
.super Ljava/lang/Object;
.source "OcrRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dmc/ocr/OcrRecognizer$OcrRecongizerLisenter;
    }
.end annotation


# static fields
.field public static final GET_TEXT_RESULT_ERROR:I = 0x2

.field public static final GET_TEXT_RESULT_NO_TEXT:I = 0x1

.field public static final GET_TEXT_RESULT_OK:I = 0x0

.field public static final KEY_GET_TEXT_RESULT:Ljava/lang/String; = "gettext_result"

.field private static final TAG:Ljava/lang/String; = "OcrRecognizer"

.field public static mCapturedHeight:I

.field public static mCapturedWidth:I

.field public static mHandler:Landroid/os/Handler;

.field public static mImageBmp:Landroid/graphics/Bitmap;

.field private static mImageBmpTemp:Landroid/graphics/Bitmap;

.field public static mImgHeight:I

.field public static mImgWidth:I

.field public static myImageData:[I


# instance fields
.field public mOCR:Lcom/dmc/ocr/SecMOCR;

.field mlistener:Lcom/dmc/ocr/OcrRecognizer$OcrRecongizerLisenter;

.field public nNumberOfWholeWord:I

.field private nSpecialWordNum:I

.field private nSpecialWordText:[Ljava/lang/String;

.field private nSpecialWordType:[I

.field public nWholeWordPerLine:[I

.field public nWholeWordType:[I

.field public nWoleWordText:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 29
    sput-object v1, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 30
    sput v0, Lcom/dmc/ocr/OcrRecognizer;->mCapturedWidth:I

    .line 31
    sput v0, Lcom/dmc/ocr/OcrRecognizer;->mCapturedHeight:I

    .line 33
    sput-object v1, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    .line 35
    sput v0, Lcom/dmc/ocr/OcrRecognizer;->mImgWidth:I

    .line 37
    sput v0, Lcom/dmc/ocr/OcrRecognizer;->mImgHeight:I

    .line 39
    sput-object v1, Lcom/dmc/ocr/OcrRecognizer;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Lcom/dmc/ocr/OcrRecognizer$OcrRecongizerLisenter;)V
    .locals 1
    .param p1, "listener"    # Lcom/dmc/ocr/OcrRecognizer$OcrRecongizerLisenter;

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/dmc/ocr/OcrRecognizer;->mlistener:Lcom/dmc/ocr/OcrRecognizer$OcrRecongizerLisenter;

    .line 32
    iput-object v0, p0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/dmc/ocr/OcrRecognizer;->nNumberOfWholeWord:I

    .line 56
    iput-object p1, p0, Lcom/dmc/ocr/OcrRecognizer;->mlistener:Lcom/dmc/ocr/OcrRecognizer$OcrRecongizerLisenter;

    .line 57
    return-void
.end method


# virtual methods
.method public declared-synchronized getOCRWholeDataFromImage(Ljava/lang/String;Landroid/graphics/Rect;)I
    .locals 22
    .param p1, "aGalleryFilePath"    # Ljava/lang/String;
    .param p2, "ocrRect"    # Landroid/graphics/Rect;

    .prologue
    .line 60
    monitor-enter p0

    const/16 v21, 0x1

    .line 61
    .local v21, "result":Z
    :try_start_0
    const-string v2, "OcrRecognizer"

    const-string v3, "getOCRWholeDataFromImage"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    if-nez p1, :cond_0

    .line 64
    const/4 v2, 0x2

    .line 239
    :goto_0
    monitor-exit p0

    return v2

    .line 68
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    :goto_1
    :try_start_2
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    if-nez v2, :cond_2

    .line 90
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage() : mImageBmpTemp is null !"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const/4 v2, 0x2

    goto :goto_0

    .line 71
    :catch_0
    move-exception v17

    .line 72
    .local v17, "e":Ljava/lang/OutOfMemoryError;
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage() : mImageBmpTemp] Out of memory !"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 75
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 76
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage()] mImageBmp = null! Retry"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 79
    :cond_1
    const/4 v2, 0x0

    :try_start_3
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 80
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage()] file path = "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 81
    :catch_1
    move-exception v19

    .line 82
    .local v19, "e2":Ljava/lang/OutOfMemoryError;
    :try_start_4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 83
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage() : mImageBmpTemp ] Out of memory !"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 85
    const/4 v2, 0x2

    goto :goto_0

    .line 95
    .end local v17    # "e":Ljava/lang/OutOfMemoryError;
    .end local v19    # "e2":Ljava/lang/OutOfMemoryError;
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExifOrientation(Ljava/lang/String;)I

    move-result v16

    .line 96
    .local v16, "degree":I
    const/16 v2, 0x5a

    move/from16 v0, v16

    if-ne v0, v2, :cond_4

    .line 97
    sget-object v1, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 98
    .local v1, "bitMapTemp":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 100
    .local v6, "m":Landroid/graphics/Matrix;
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v6, v2}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 101
    const/4 v15, 0x0

    .line 103
    .local v15, "b2":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_5
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 108
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 109
    const/4 v1, 0x0

    .line 110
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 111
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 112
    const/4 v15, 0x0

    .line 121
    :try_start_6
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mCapturedWidth:I

    .line 122
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mCapturedHeight:I

    .line 124
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mCapturedWidth:I

    .line 125
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mCapturedHeight:I

    .line 126
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 127
    sput-object v1, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    .line 163
    .end local v1    # "bitMapTemp":Landroid/graphics/Bitmap;
    .end local v6    # "m":Landroid/graphics/Matrix;
    .end local v15    # "b2":Landroid/graphics/Bitmap;
    :goto_2
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mImgWidth:I

    .line 164
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mImgHeight:I

    .line 166
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->myImageData:[I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v2, :cond_3

    .line 168
    :try_start_7
    sget v2, Lcom/dmc/ocr/OcrRecognizer;->mImgWidth:I

    sget v3, Lcom/dmc/ocr/OcrRecognizer;->mImgHeight:I

    mul-int/2addr v2, v3

    new-array v2, v2, [I

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->myImageData:[I
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 178
    :cond_3
    :try_start_8
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_6

    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->myImageData:[I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v2, :cond_6

    .line 180
    :try_start_9
    sget-object v7, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    sget-object v8, Lcom/dmc/ocr/OcrRecognizer;->myImageData:[I

    const/4 v9, 0x0

    sget v10, Lcom/dmc/ocr/OcrRecognizer;->mImgWidth:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    sget v13, Lcom/dmc/ocr/OcrRecognizer;->mImgWidth:I

    sget v14, Lcom/dmc/ocr/OcrRecognizer;->mImgHeight:I

    invoke-virtual/range {v7 .. v14}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 197
    :goto_3
    if-nez v21, :cond_7

    .line 198
    const/4 v2, 0x0

    :try_start_a
    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 199
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    .line 200
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 201
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 113
    .restart local v1    # "bitMapTemp":Landroid/graphics/Bitmap;
    .restart local v6    # "m":Landroid/graphics/Matrix;
    .restart local v15    # "b2":Landroid/graphics/Bitmap;
    :catch_2
    move-exception v20

    .line 115
    .local v20, "ex":Ljava/lang/OutOfMemoryError;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 116
    const-string v2, "OcrRecognizer"

    const-string v3, "[ocrRecognizeCapturedImage()] Out of memory when rotating !"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 118
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 128
    .end local v1    # "bitMapTemp":Landroid/graphics/Bitmap;
    .end local v6    # "m":Landroid/graphics/Matrix;
    .end local v15    # "b2":Landroid/graphics/Bitmap;
    .end local v20    # "ex":Ljava/lang/OutOfMemoryError;
    :cond_4
    const/16 v2, 0xb4

    move/from16 v0, v16

    if-ne v0, v2, :cond_5

    .line 129
    sget-object v1, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 130
    .restart local v1    # "bitMapTemp":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 132
    .restart local v6    # "m":Landroid/graphics/Matrix;
    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {v6, v2}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 133
    const/4 v15, 0x0

    .line 135
    .restart local v15    # "b2":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_b
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 140
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 141
    const/4 v1, 0x0

    .line 142
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 143
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 144
    const/4 v15, 0x0

    .line 153
    :try_start_c
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mCapturedWidth:I

    .line 154
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mCapturedHeight:I

    .line 155
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 156
    sput-object v1, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_2

    .line 60
    .end local v1    # "bitMapTemp":Landroid/graphics/Bitmap;
    .end local v6    # "m":Landroid/graphics/Matrix;
    .end local v15    # "b2":Landroid/graphics/Bitmap;
    .end local v16    # "degree":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 145
    .restart local v1    # "bitMapTemp":Landroid/graphics/Bitmap;
    .restart local v6    # "m":Landroid/graphics/Matrix;
    .restart local v15    # "b2":Landroid/graphics/Bitmap;
    .restart local v16    # "degree":I
    :catch_3
    move-exception v20

    .line 147
    .restart local v20    # "ex":Ljava/lang/OutOfMemoryError;
    :try_start_d
    invoke-virtual/range {v20 .. v20}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 148
    const-string v2, "OcrRecognizer"

    const-string v3, "[ocrRecognizeCapturedImage()] Out of memory when rotating !"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 150
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 158
    .end local v1    # "bitMapTemp":Landroid/graphics/Bitmap;
    .end local v6    # "m":Landroid/graphics/Matrix;
    .end local v15    # "b2":Landroid/graphics/Bitmap;
    .end local v20    # "ex":Ljava/lang/OutOfMemoryError;
    :cond_5
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mCapturedWidth:I

    .line 159
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sput v2, Lcom/dmc/ocr/OcrRecognizer;->mCapturedHeight:I

    .line 160
    sget-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 169
    :catch_4
    move-exception v17

    .line 170
    .restart local v17    # "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 171
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage() : myImageData] Out of memory !"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    .line 173
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 174
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 182
    .end local v17    # "e":Ljava/lang/OutOfMemoryError;
    :catch_5
    move-exception v18

    .line 183
    .local v18, "e1":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 184
    const/16 v21, 0x0

    .line 185
    const-string v2, "OcrRecognizer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[getOCRWholeDataFromImage()] IllegalArgumentException: x + width("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/dmc/ocr/OcrRecognizer;->mImgWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") must be <= bitmap.width():"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 188
    .end local v18    # "e1":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v19

    .line 189
    .local v19, "e2":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    .line 190
    const/16 v21, 0x0

    .line 191
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage()] Error ArrayIndexOutOfBoundsException !"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 194
    .end local v19    # "e2":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_6
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage()] mImageBmp = null or myImageData = null !"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 204
    :cond_7
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage()] decoding success "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v2, :cond_8

    .line 210
    invoke-static {}, Lcom/dmc/ocr/SecMOCR;->getInstance()Lcom/dmc/ocr/SecMOCR;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    .line 211
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    if-nez v2, :cond_8

    .line 212
    const-string v2, "OcrRecognizer"

    const-string v3, "[getOCRWholeDataFromImage()] mOCR is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 214
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmp:Landroid/graphics/Bitmap;

    .line 215
    const/4 v2, 0x0

    sput-object v2, Lcom/dmc/ocr/OcrRecognizer;->mImageBmpTemp:Landroid/graphics/Bitmap;

    .line 216
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 219
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/dmc/ocr/SecMOCR;->setRecognitionMode(I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    const-string v3, "/system/point_db/"

    invoke-virtual {v2, v3}, Lcom/dmc/ocr/SecMOCR;->setDataBasePath(Ljava/lang/String;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    sget-object v8, Lcom/dmc/ocr/OcrRecognizer;->myImageData:[I

    sget v9, Lcom/dmc/ocr/OcrRecognizer;->mImgWidth:I

    sget v10, Lcom/dmc/ocr/OcrRecognizer;->mImgHeight:I

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p2

    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual/range {v7 .. v14}, Lcom/dmc/ocr/SecMOCR;->setJPEGData([IIIIIII)Z

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v2}, Lcom/dmc/ocr/SecMOCR;->startRecognition()V

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v2}, Lcom/dmc/ocr/SecMOCR;->getRecogResult()I

    .line 225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v2}, Lcom/dmc/ocr/SecMOCR;->getWholeRecogResult()I

    .line 226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    invoke-virtual {v2}, Lcom/dmc/ocr/SecMOCR;->MOCR_Close()V

    .line 228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    iget v2, v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordNum:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/dmc/ocr/OcrRecognizer;->nNumberOfWholeWord:I

    .line 229
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    iget-object v2, v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordText:[Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->nWoleWordText:[Ljava/lang/String;

    .line 230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    iget-object v2, v2, Lcom/dmc/ocr/SecMOCR;->mLinePerWordNum:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->nWholeWordPerLine:[I

    .line 231
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    iget-object v2, v2, Lcom/dmc/ocr/SecMOCR;->mWholeWordType:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->nWholeWordType:[I

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    iget-object v2, v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordText:[Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->nSpecialWordText:[Ljava/lang/String;

    .line 233
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    iget-object v2, v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordType:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->nSpecialWordType:[I

    .line 234
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dmc/ocr/OcrRecognizer;->mOCR:Lcom/dmc/ocr/SecMOCR;

    iget v2, v2, Lcom/dmc/ocr/SecMOCR;->mSpecialWordNum:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/dmc/ocr/OcrRecognizer;->nSpecialWordNum:I

    .line 236
    move-object/from16 v0, p0

    iget v2, v0, Lcom/dmc/ocr/OcrRecognizer;->nNumberOfWholeWord:I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    if-nez v2, :cond_9

    .line 237
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 239
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public processTextRect(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "photo"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "textRect"    # Landroid/graphics/Rect;

    .prologue
    .line 254
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v1, :cond_0

    .line 255
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, "mimeType":Ljava/lang/String;
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p1, v1, v0, p3, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->startOCRActivity(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;Z)V

    .line 258
    .end local v0    # "mimeType":Ljava/lang/String;
    :cond_0
    return-void
.end method
