.class public Lcom/dmc/ocr/OcrUtils;
.super Ljava/lang/Object;
.source "OcrUtils.java"


# static fields
.field public static final OCR_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.ocr3"

.field private static final PREFIX_GET_TEXT_UPDATE:Ljava/lang/String; = "get-text-update-"

.field private static final PREFIX_HAS_GET_TEXT:Ljava/lang/String; = "has-get-text-"

.field private static final PREFIX_HAS_SAMSUNG_APPS:Ljava/lang/String; = "has-samsung-apps-"

.field private static final PREFIX_SAMSUNG_APPS_UPDATE:Ljava/lang/String; = "samsung-apps-update-"

.field private static final SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    const-class v0, Lcom/dmc/ocr/OcrUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dmc/ocr/OcrUtils;->TAG:Ljava/lang/String;

    .line 92
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/dmc/ocr/OcrUtils;->SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;

    .line 94
    sget-object v0, Lcom/dmc/ocr/OcrUtils;->SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/jpg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/dmc/ocr/OcrUtils;->SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/jpeg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lcom/dmc/ocr/OcrUtils;->SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/bmp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/dmc/ocr/OcrUtils;->SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/x-ms-bmp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v0, Lcom/dmc/ocr/OcrUtils;->SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/gif"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Lcom/dmc/ocr/OcrUtils;->SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;

    const-string v1, "image/png"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isGetTextMenuAvailable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-static {p0}, Lcom/dmc/ocr/OcrUtils;->isOCRAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOCRAvailable(Landroid/content/Context;)Z
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 61
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCRPreloadService:Z

    if-eqz v11, :cond_3

    .line 62
    const-string v1, "com.sec.android.app.ocr3"

    .line 64
    .local v1, "OCRPKGName":Ljava/lang/String;
    const-string v0, "com.sec.android.app.ocr3.GetTextActivity"

    .line 65
    .local v0, "OCRActivity":Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 66
    .local v5, "intent":Landroid/content/Intent;
    invoke-virtual {v5, v1, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 68
    .local v6, "pm":Landroid/content/pm/PackageManager;
    if-nez v6, :cond_1

    move v9, v10

    .line 88
    .end local v0    # "OCRActivity":Ljava/lang/String;
    .end local v1    # "OCRPKGName":Ljava/lang/String;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    return v9

    .line 70
    .restart local v0    # "OCRActivity":Ljava/lang/String;
    .restart local v1    # "OCRPKGName":Ljava/lang/String;
    .restart local v5    # "intent":Landroid/content/Intent;
    .restart local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    invoke-virtual {v6, v5, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 71
    .local v2, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    if-gtz v11, :cond_0

    :cond_2
    move v9, v10

    goto :goto_0

    .line 73
    .end local v0    # "OCRActivity":Ljava/lang/String;
    .end local v1    # "OCRPKGName":Ljava/lang/String;
    .end local v2    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v8

    .line 74
    .local v8, "version":I
    const-string v11, "get-text-update-"

    invoke-static {p0, v11, v10}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    .line 75
    .local v7, "updateKey":I
    if-eq v7, v8, :cond_7

    .line 76
    const/4 v4, 0x0

    .line 78
    .local v4, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    const-string v12, "com.sec.android.app.ocr3"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 79
    if-eqz v4, :cond_5

    .line 84
    const-string v11, "get-text-update-"

    invoke-static {p0, v11, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 85
    const-string v11, "has-get-text-"

    if-eqz v4, :cond_4

    move v10, v9

    :cond_4
    invoke-static {p0, v11, v10}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 84
    :cond_5
    const-string v11, "get-text-update-"

    invoke-static {p0, v11, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 85
    const-string v11, "has-get-text-"

    if-eqz v4, :cond_6

    move v10, v9

    :cond_6
    invoke-static {p0, v11, v10}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 88
    .end local v4    # "info":Landroid/content/pm/PackageInfo;
    :cond_7
    :goto_1
    const-string v10, "has-get-text-"

    invoke-static {p0, v10, v9}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v9

    goto :goto_0

    .line 81
    .restart local v4    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v3

    .line 82
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string v11, "Get text"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Samsung OCR is not exist. "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    const-string v11, "get-text-update-"

    invoke-static {p0, v11, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 85
    const-string v11, "has-get-text-"

    if-eqz v4, :cond_8

    move v10, v9

    :cond_8
    invoke-static {p0, v11, v10}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_1

    .line 84
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v11

    const-string v12, "get-text-update-"

    invoke-static {p0, v12, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 85
    const-string v12, "has-get-text-"

    if-eqz v4, :cond_9

    :goto_2
    invoke-static {p0, v12, v9}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    throw v11

    :cond_9
    move v9, v10

    goto :goto_2
.end method

.method public static isSamsungAppsAvailable(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 40
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v3

    .line 42
    .local v3, "version":I
    const-string v6, "samsung-apps-update-"

    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    .line 43
    .local v2, "updateKey":I
    if-eq v2, v3, :cond_3

    .line 44
    const/4 v1, 0x0

    .line 46
    .local v1, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.samsungapps"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 48
    if-eqz v1, :cond_1

    .line 53
    const-string v6, "samsung-apps-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 54
    const-string v6, "has-samsung-apps-"

    if-eqz v1, :cond_0

    move v4, v5

    :cond_0
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 57
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return v5

    .line 53
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    const-string v6, "samsung-apps-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 54
    const-string v6, "has-samsung-apps-"

    if-eqz v1, :cond_2

    move v4, v5

    :cond_2
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 57
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_3
    :goto_1
    const-string v4, "has-samsung-apps-"

    invoke-static {p0, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v5

    goto :goto_0

    .line 50
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    sget-object v6, Lcom/dmc/ocr/OcrUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Samsung apps is not exist. "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53
    const-string v6, "samsung-apps-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 54
    const-string v6, "has-samsung-apps-"

    if-eqz v1, :cond_4

    move v4, v5

    :cond_4
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_1

    .line 53
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v6

    const-string v7, "samsung-apps-update-"

    invoke-static {p0, v7, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 54
    const-string v7, "has-samsung-apps-"

    if-eqz v1, :cond_5

    :goto_2
    invoke-static {p0, v7, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    throw v6

    :cond_5
    move v5, v4

    goto :goto_2
.end method

.method public static isSupportedFormat(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 103
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v2, :cond_0

    if-nez p0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v1

    .line 106
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 107
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 108
    sget-object v2, Lcom/dmc/ocr/OcrUtils;->SEARCH_TEXT_MIME_TYPES:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    const/4 v1, 0x1

    goto :goto_0
.end method
