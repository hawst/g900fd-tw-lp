.class public interface abstract Lcom/android/email/backgroundsender/IEmailRemoteService;
.super Ljava/lang/Object;
.source "IEmailRemoteService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/backgroundsender/IEmailRemoteService$Stub;
    }
.end annotation


# virtual methods
.method public abstract registerCallback(Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract unregisterCallback(Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
