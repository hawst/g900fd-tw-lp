.class public Lorg/puremvc/java/patterns/command/MacroCommand;
.super Lorg/puremvc/java/patterns/observer/Notifier;

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# instance fields
.field private subCommands:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lorg/puremvc/java/interfaces/ICommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/puremvc/java/patterns/observer/Notifier;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/puremvc/java/patterns/command/MacroCommand;->subCommands:Ljava/util/Collection;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/puremvc/java/patterns/command/MacroCommand;->subCommands:Ljava/util/Collection;

    invoke-virtual {p0}, Lorg/puremvc/java/patterns/command/MacroCommand;->initializeMacroCommand()V

    return-void
.end method


# virtual methods
.method protected addSubCommand(Lorg/puremvc/java/interfaces/ICommand;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/command/MacroCommand;->subCommands:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2

    iget-object v0, p0, Lorg/puremvc/java/patterns/command/MacroCommand;->subCommands:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/puremvc/java/interfaces/ICommand;

    invoke-interface {v0, p1}, Lorg/puremvc/java/interfaces/ICommand;->execute(Lorg/puremvc/java/interfaces/INotification;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected initializeMacroCommand()V
    .locals 0

    return-void
.end method
