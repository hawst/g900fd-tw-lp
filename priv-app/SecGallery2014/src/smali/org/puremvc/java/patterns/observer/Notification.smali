.class public Lorg/puremvc/java/patterns/observer/Notification;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/puremvc/java/interfaces/INotification;


# instance fields
.field private body:Ljava/lang/Object;

.field private name:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    iput-object p1, p0, Lorg/puremvc/java/patterns/observer/Notification;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    iput-object p1, p0, Lorg/puremvc/java/patterns/observer/Notification;->name:Ljava/lang/String;

    iput-object p2, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    iput-object p1, p0, Lorg/puremvc/java/patterns/observer/Notification;->name:Ljava/lang/String;

    iput-object p2, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    iput-object p3, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setBody(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Notification Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/puremvc/java/patterns/observer/Notification;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Body:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/puremvc/java/patterns/observer/Notification;->body:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/puremvc/java/patterns/observer/Notification;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "null Type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "null "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
