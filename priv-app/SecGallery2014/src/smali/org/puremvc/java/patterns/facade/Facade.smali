.class public Lorg/puremvc/java/patterns/facade/Facade;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/puremvc/java/interfaces/IFacade;


# static fields
.field protected static instance:Lorg/puremvc/java/patterns/facade/Facade;


# instance fields
.field protected controller:Lorg/puremvc/java/core/Controller;

.field protected model:Lorg/puremvc/java/core/Model;

.field protected view:Lorg/puremvc/java/core/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lorg/puremvc/java/patterns/facade/Facade;->instance:Lorg/puremvc/java/patterns/facade/Facade;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->controller:Lorg/puremvc/java/core/Controller;

    iput-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->model:Lorg/puremvc/java/core/Model;

    iput-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    invoke-virtual {p0}, Lorg/puremvc/java/patterns/facade/Facade;->initializeFacade()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lorg/puremvc/java/patterns/facade/Facade;
    .locals 2

    const-class v1, Lorg/puremvc/java/patterns/facade/Facade;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/puremvc/java/patterns/facade/Facade;->instance:Lorg/puremvc/java/patterns/facade/Facade;

    if-nez v0, :cond_0

    new-instance v0, Lorg/puremvc/java/patterns/facade/Facade;

    invoke-direct {v0}, Lorg/puremvc/java/patterns/facade/Facade;-><init>()V

    sput-object v0, Lorg/puremvc/java/patterns/facade/Facade;->instance:Lorg/puremvc/java/patterns/facade/Facade;

    :cond_0
    sget-object v0, Lorg/puremvc/java/patterns/facade/Facade;->instance:Lorg/puremvc/java/patterns/facade/Facade;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public hasCommand(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->controller:Lorg/puremvc/java/core/Controller;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/Controller;->hasCommand(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasMediator(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/View;->hasMediator(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasProxy(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->model:Lorg/puremvc/java/core/Model;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/Model;->hasProxy(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected initializeController()V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->controller:Lorg/puremvc/java/core/Controller;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lorg/puremvc/java/core/Controller;->getInstance()Lorg/puremvc/java/core/Controller;

    move-result-object v0

    iput-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->controller:Lorg/puremvc/java/core/Controller;

    goto :goto_0
.end method

.method protected initializeFacade()V
    .locals 0

    invoke-virtual {p0}, Lorg/puremvc/java/patterns/facade/Facade;->initializeModel()V

    invoke-virtual {p0}, Lorg/puremvc/java/patterns/facade/Facade;->initializeController()V

    invoke-virtual {p0}, Lorg/puremvc/java/patterns/facade/Facade;->initializeView()V

    return-void
.end method

.method protected initializeModel()V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->model:Lorg/puremvc/java/core/Model;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lorg/puremvc/java/core/Model;->getInstance()Lorg/puremvc/java/core/Model;

    move-result-object v0

    iput-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->model:Lorg/puremvc/java/core/Model;

    goto :goto_0
.end method

.method protected initializeView()V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lorg/puremvc/java/core/View;->getInstance()Lorg/puremvc/java/core/View;

    move-result-object v0

    iput-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    goto :goto_0
.end method

.method public notifyObservers(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/View;->notifyObservers(Lorg/puremvc/java/interfaces/INotification;)V

    :cond_0
    return-void
.end method

.method public registerCommand(Ljava/lang/String;Lorg/puremvc/java/interfaces/ICommand;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->controller:Lorg/puremvc/java/core/Controller;

    invoke-virtual {v0, p1, p2}, Lorg/puremvc/java/core/Controller;->registerCommand(Ljava/lang/String;Lorg/puremvc/java/interfaces/ICommand;)V

    return-void
.end method

.method public registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/View;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    :cond_0
    return-void
.end method

.method public registerProxy(Lorg/puremvc/java/interfaces/IProxy;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->model:Lorg/puremvc/java/core/Model;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/Model;->registerProxy(Lorg/puremvc/java/interfaces/IProxy;)V

    return-void
.end method

.method public removeCommand(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->controller:Lorg/puremvc/java/core/Controller;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/Controller;->removeCommand(Ljava/lang/String;)V

    return-void
.end method

.method public removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/View;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->model:Lorg/puremvc/java/core/Model;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->model:Lorg/puremvc/java/core/Model;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/Model;->removeProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public retrieveMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->view:Lorg/puremvc/java/core/View;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/View;->retrieveMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    move-result-object v0

    return-object v0
.end method

.method public retrieveProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/facade/Facade;->model:Lorg/puremvc/java/core/Model;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/core/Model;->retrieveProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;

    move-result-object v0

    return-object v0
.end method

.method public sendNotification(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lorg/puremvc/java/patterns/facade/Facade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public sendNotification(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/puremvc/java/patterns/facade/Facade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public sendNotification(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lorg/puremvc/java/patterns/observer/Notification;

    invoke-direct {v0, p1, p2, p3}, Lorg/puremvc/java/patterns/observer/Notification;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/puremvc/java/patterns/facade/Facade;->notifyObservers(Lorg/puremvc/java/interfaces/INotification;)V

    return-void
.end method
