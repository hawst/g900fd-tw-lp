.class public Lorg/puremvc/java/patterns/observer/Notifier;
.super Ljava/lang/Object;


# instance fields
.field protected facade:Lorg/puremvc/java/patterns/facade/Facade;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lorg/puremvc/java/patterns/facade/Facade;->getInstance()Lorg/puremvc/java/patterns/facade/Facade;

    move-result-object v0

    iput-object v0, p0, Lorg/puremvc/java/patterns/observer/Notifier;->facade:Lorg/puremvc/java/patterns/facade/Facade;

    return-void
.end method


# virtual methods
.method public sendNotification(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/observer/Notifier;->facade:Lorg/puremvc/java/patterns/facade/Facade;

    invoke-virtual {v0, p1}, Lorg/puremvc/java/patterns/facade/Facade;->sendNotification(Ljava/lang/String;)V

    return-void
.end method

.method public sendNotification(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/observer/Notifier;->facade:Lorg/puremvc/java/patterns/facade/Facade;

    invoke-virtual {v0, p1, p2}, Lorg/puremvc/java/patterns/facade/Facade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public sendNotification(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/patterns/observer/Notifier;->facade:Lorg/puremvc/java/patterns/facade/Facade;

    invoke-virtual {v0, p1, p2, p3}, Lorg/puremvc/java/patterns/facade/Facade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
