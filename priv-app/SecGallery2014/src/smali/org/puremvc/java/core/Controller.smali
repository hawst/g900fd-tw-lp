.class public Lorg/puremvc/java/core/Controller;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/puremvc/java/interfaces/IController;


# static fields
.field protected static instance:Lorg/puremvc/java/core/Controller;


# instance fields
.field protected commandMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/puremvc/java/interfaces/ICommand;",
            ">;"
        }
    .end annotation
.end field

.field protected view:Lorg/puremvc/java/core/View;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sput-object p0, Lorg/puremvc/java/core/Controller;->instance:Lorg/puremvc/java/core/Controller;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/puremvc/java/core/Controller;->commandMap:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/puremvc/java/core/Controller;->initializeController()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lorg/puremvc/java/core/Controller;
    .locals 2

    const-class v1, Lorg/puremvc/java/core/Controller;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/puremvc/java/core/Controller;->instance:Lorg/puremvc/java/core/Controller;

    if-nez v0, :cond_0

    new-instance v0, Lorg/puremvc/java/core/Controller;

    invoke-direct {v0}, Lorg/puremvc/java/core/Controller;-><init>()V

    sput-object v0, Lorg/puremvc/java/core/Controller;->instance:Lorg/puremvc/java/core/Controller;

    :cond_0
    sget-object v0, Lorg/puremvc/java/core/Controller;->instance:Lorg/puremvc/java/core/Controller;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public executeCommand(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2

    iget-object v0, p0, Lorg/puremvc/java/core/Controller;->commandMap:Ljava/util/Map;

    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/puremvc/java/interfaces/ICommand;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lorg/puremvc/java/interfaces/ICommand;->execute(Lorg/puremvc/java/interfaces/INotification;)V

    :cond_0
    return-void
.end method

.method public hasCommand(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/core/Controller;->commandMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected initializeController()V
    .locals 1

    invoke-static {}, Lorg/puremvc/java/core/View;->getInstance()Lorg/puremvc/java/core/View;

    move-result-object v0

    iput-object v0, p0, Lorg/puremvc/java/core/Controller;->view:Lorg/puremvc/java/core/View;

    return-void
.end method

.method public registerCommand(Ljava/lang/String;Lorg/puremvc/java/interfaces/ICommand;)V
    .locals 3

    iget-object v0, p0, Lorg/puremvc/java/core/Controller;->commandMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/puremvc/java/core/Controller;->view:Lorg/puremvc/java/core/View;

    new-instance v1, Lorg/puremvc/java/patterns/observer/Observer;

    new-instance v2, Lorg/puremvc/java/core/Controller$1;

    invoke-direct {v2, p0}, Lorg/puremvc/java/core/Controller$1;-><init>(Lorg/puremvc/java/core/Controller;)V

    invoke-direct {v1, v2, p0}, Lorg/puremvc/java/patterns/observer/Observer;-><init>(Lorg/puremvc/java/interfaces/IFunction;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Lorg/puremvc/java/core/View;->registerObserver(Ljava/lang/String;Lorg/puremvc/java/interfaces/IObserver;)V

    goto :goto_0
.end method

.method public removeCommand(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p1}, Lorg/puremvc/java/core/Controller;->hasCommand(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/puremvc/java/core/Controller;->view:Lorg/puremvc/java/core/View;

    invoke-virtual {v0, p1, p0}, Lorg/puremvc/java/core/View;->removeObserver(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/puremvc/java/core/Controller;->commandMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
