.class public Lorg/puremvc/java/core/View;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/puremvc/java/interfaces/IView;


# static fields
.field private static instance:Lorg/puremvc/java/core/View;


# instance fields
.field private mediatorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/puremvc/java/interfaces/IMediator;",
            ">;"
        }
    .end annotation
.end field

.field private observerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/puremvc/java/interfaces/IObserver;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sput-object p0, Lorg/puremvc/java/core/View;->instance:Lorg/puremvc/java/core/View;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/puremvc/java/core/View;->mediatorMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/puremvc/java/core/View;->observerMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Lorg/puremvc/java/core/View;->initializeView()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lorg/puremvc/java/core/View;
    .locals 2

    const-class v1, Lorg/puremvc/java/core/View;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/puremvc/java/core/View;->instance:Lorg/puremvc/java/core/View;

    if-nez v0, :cond_0

    new-instance v0, Lorg/puremvc/java/core/View;

    invoke-direct {v0}, Lorg/puremvc/java/core/View;-><init>()V

    sput-object v0, Lorg/puremvc/java/core/View;->instance:Lorg/puremvc/java/core/View;

    :cond_0
    sget-object v0, Lorg/puremvc/java/core/View;->instance:Lorg/puremvc/java/core/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public hasMediator(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/core/View;->mediatorMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected initializeView()V
    .locals 0

    return-void
.end method

.method public notifyObservers(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3

    iget-object v0, p0, Lorg/puremvc/java/core/View;->observerMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v1, v0

    if-ge v2, v1, :cond_0

    aget-object v1, v0, v2

    check-cast v1, Lorg/puremvc/java/interfaces/IObserver;

    invoke-interface {v1, p1}, Lorg/puremvc/java/interfaces/IObserver;->notifyObserver(Lorg/puremvc/java/interfaces/INotification;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V
    .locals 4

    iget-object v0, p0, Lorg/puremvc/java/core/View;->mediatorMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lorg/puremvc/java/interfaces/IMediator;->getMediatorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/puremvc/java/core/View;->mediatorMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lorg/puremvc/java/interfaces/IMediator;->getMediatorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lorg/puremvc/java/interfaces/IMediator;->listNotificationInterests()[Ljava/lang/String;

    move-result-object v1

    array-length v0, v1

    if-eqz v0, :cond_1

    new-instance v0, Lorg/puremvc/java/core/View$1;

    invoke-direct {v0, p0, p1}, Lorg/puremvc/java/core/View$1;-><init>(Lorg/puremvc/java/core/View;Lorg/puremvc/java/interfaces/IMediator;)V

    new-instance v2, Lorg/puremvc/java/patterns/observer/Observer;

    invoke-direct {v2, v0, p1}, Lorg/puremvc/java/patterns/observer/Observer;-><init>(Lorg/puremvc/java/interfaces/IFunction;Ljava/lang/Object;)V

    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_1

    aget-object v3, v1, v0

    invoke-virtual {p0, v3, v2}, Lorg/puremvc/java/core/View;->registerObserver(Ljava/lang/String;Lorg/puremvc/java/interfaces/IObserver;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/IMediator;->onRegister()V

    goto :goto_0
.end method

.method public registerObserver(Ljava/lang/String;Lorg/puremvc/java/interfaces/IObserver;)V
    .locals 2

    iget-object v0, p0, Lorg/puremvc/java/core/View;->observerMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/puremvc/java/core/View;->observerMap:Ljava/util/HashMap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lorg/puremvc/java/core/View;->observerMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;
    .locals 4

    iget-object v0, p0, Lorg/puremvc/java/core/View;->mediatorMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/puremvc/java/interfaces/IMediator;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/puremvc/java/interfaces/IMediator;->listNotificationInterests()[Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    aget-object v3, v2, v1

    invoke-virtual {p0, v3, v0}, Lorg/puremvc/java/core/View;->removeObserver(Ljava/lang/String;Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lorg/puremvc/java/core/View;->mediatorMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Lorg/puremvc/java/interfaces/IMediator;->onRemove()V

    :cond_1
    return-object v0
.end method

.method public removeObserver(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lorg/puremvc/java/core/View;->observerMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/puremvc/java/patterns/observer/Observer;

    invoke-virtual {v1, p2}, Lorg/puremvc/java/patterns/observer/Observer;->compareNotifyContext(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/puremvc/java/core/View;->observerMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-void
.end method

.method public retrieveMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;
    .locals 1

    iget-object v0, p0, Lorg/puremvc/java/core/View;->mediatorMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/puremvc/java/interfaces/IMediator;

    return-object v0
.end method
