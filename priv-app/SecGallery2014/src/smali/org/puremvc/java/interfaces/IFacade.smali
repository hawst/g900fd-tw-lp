.class public interface abstract Lorg/puremvc/java/interfaces/IFacade;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/puremvc/java/interfaces/INotifier;


# virtual methods
.method public abstract hasCommand(Ljava/lang/String;)Z
.end method

.method public abstract hasMediator(Ljava/lang/String;)Z
.end method

.method public abstract hasProxy(Ljava/lang/String;)Z
.end method

.method public abstract notifyObservers(Lorg/puremvc/java/interfaces/INotification;)V
.end method

.method public abstract registerCommand(Ljava/lang/String;Lorg/puremvc/java/interfaces/ICommand;)V
.end method

.method public abstract registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V
.end method

.method public abstract registerProxy(Lorg/puremvc/java/interfaces/IProxy;)V
.end method

.method public abstract removeCommand(Ljava/lang/String;)V
.end method

.method public abstract removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;
.end method

.method public abstract removeProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;
.end method

.method public abstract retrieveMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;
.end method

.method public abstract retrieveProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;
.end method
