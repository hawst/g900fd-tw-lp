.class public interface abstract Lorg/puremvc/java/interfaces/IMediator;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/puremvc/java/interfaces/INotifier;


# virtual methods
.method public abstract getMediatorName()Ljava/lang/String;
.end method

.method public abstract getViewComponent()Ljava/lang/Object;
.end method

.method public abstract handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
.end method

.method public abstract listNotificationInterests()[Ljava/lang/String;
.end method

.method public abstract onRegister()V
.end method

.method public abstract onRemove()V
.end method

.method public abstract setViewComponent(Ljava/lang/Object;)V
.end method
