.class public interface abstract Lorg/puremvc/java/interfaces/IController;
.super Ljava/lang/Object;


# virtual methods
.method public abstract executeCommand(Lorg/puremvc/java/interfaces/INotification;)V
.end method

.method public abstract hasCommand(Ljava/lang/String;)Z
.end method

.method public abstract registerCommand(Ljava/lang/String;Lorg/puremvc/java/interfaces/ICommand;)V
.end method

.method public abstract removeCommand(Ljava/lang/String;)V
.end method
