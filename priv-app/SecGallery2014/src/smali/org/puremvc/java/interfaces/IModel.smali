.class public interface abstract Lorg/puremvc/java/interfaces/IModel;
.super Ljava/lang/Object;


# virtual methods
.method public abstract hasProxy(Ljava/lang/String;)Z
.end method

.method public abstract registerProxy(Lorg/puremvc/java/interfaces/IProxy;)V
.end method

.method public abstract removeProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;
.end method

.method public abstract retrieveProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;
.end method
