.class public interface abstract Lorg/puremvc/java/interfaces/IObserver;
.super Ljava/lang/Object;


# virtual methods
.method public abstract compareNotifyContext(Ljava/lang/Object;)Z
.end method

.method public abstract notifyObserver(Lorg/puremvc/java/interfaces/INotification;)V
.end method

.method public abstract setNotifyContext(Ljava/lang/Object;)V
.end method

.method public abstract setNotifyMethod(Lorg/puremvc/java/interfaces/IFunction;)V
.end method
