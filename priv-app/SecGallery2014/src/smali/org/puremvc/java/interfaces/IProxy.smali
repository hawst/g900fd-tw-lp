.class public interface abstract Lorg/puremvc/java/interfaces/IProxy;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/puremvc/java/interfaces/INotifier;


# virtual methods
.method public abstract getData()Ljava/lang/Object;
.end method

.method public abstract getProxyName()Ljava/lang/String;
.end method

.method public abstract onRegister()V
.end method

.method public abstract onRemove()V
.end method

.method public abstract setData(Ljava/lang/Object;)V
.end method
