.class public Lsstream/lib/covers/CoverLockManager;
.super Ljava/lang/Object;
.source "CoverLockManager.java"


# static fields
.field private static final EMPTY_STORY_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsstream/lib/objs/StoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final INSTANCE:Lsstream/lib/covers/CoverLockManager;

.field private static mContext:Landroid/content/Context;

.field private static mIsStreamCompatible:Z

.field private static mMessenger:Lsstream/lib/covers/MessengerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lsstream/lib/covers/CoverLockManager;->EMPTY_STORY_LIST:Ljava/util/List;

    .line 26
    new-instance v0, Lsstream/lib/covers/CoverLockManager;

    invoke-direct {v0}, Lsstream/lib/covers/CoverLockManager;-><init>()V

    sput-object v0, Lsstream/lib/covers/CoverLockManager;->INSTANCE:Lsstream/lib/covers/CoverLockManager;

    .line 32
    const/4 v0, 0x0

    sput-boolean v0, Lsstream/lib/covers/CoverLockManager;->mIsStreamCompatible:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lsstream/lib/covers/CoverLockManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lsstream/lib/covers/CoverLockManager;->mContext:Landroid/content/Context;

    .line 47
    sget-object v0, Lsstream/lib/covers/CoverLockManager;->mMessenger:Lsstream/lib/covers/MessengerProvider;

    if-eqz v0, :cond_0

    .line 48
    sget-object v0, Lsstream/lib/covers/CoverLockManager;->INSTANCE:Lsstream/lib/covers/CoverLockManager;

    .line 52
    :goto_0
    return-object v0

    .line 50
    :cond_0
    new-instance v0, Lsstream/lib/covers/MessengerProvider;

    sget-object v1, Lsstream/lib/covers/CoverLockManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lsstream/lib/covers/MessengerProvider;-><init>(Landroid/content/Context;)V

    sput-object v0, Lsstream/lib/covers/CoverLockManager;->mMessenger:Lsstream/lib/covers/MessengerProvider;

    .line 52
    sget-object v0, Lsstream/lib/covers/CoverLockManager;->INSTANCE:Lsstream/lib/covers/CoverLockManager;

    goto :goto_0
.end method

.method private isStringEmptyOrNull(Ljava/lang/String;)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 151
    const/4 v0, 0x1

    .line 152
    .local v0, "isStringValid":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 153
    const/4 v0, 0x0

    .line 155
    :cond_0
    return v0
.end method

.method private queryStories(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;
    .locals 32
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "stream"    # Ljava/lang/String;
    .param p3, "numOfItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lsstream/lib/objs/StoryItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    if-eqz p2, :cond_5

    .line 90
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v28, "stories":Ljava/util/List;, "Ljava/util/List<Lsstream/lib/objs/StoryItem;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lsstream/lib/constants/StreamProviderConstants;->STORY_URI:Landroid/net/Uri;

    sget-object v6, Lsstream/lib/constants/StreamProviderConstants;->STORY_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v7, "stream_id IN(?)"

    const/4 v11, 0x1

    new-array v8, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object p2, v8, v11

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "time_stamp DESC LIMIT "

    move-object/from16 v0, v30

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 92
    .local v23, "cursor":Landroid/database/Cursor;
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_4

    .line 93
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    .line 95
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 96
    .local v6, "id":Ljava/lang/String;
    const/4 v4, 0x1

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 97
    .local v7, "streamId":Ljava/lang/String;
    const/4 v4, 0x2

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 98
    .local v8, "appName":Ljava/lang/String;
    const/4 v4, 0x4

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 99
    .local v29, "type":Ljava/lang/String;
    const/4 v4, 0x5

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 100
    .local v9, "title":Ljava/lang/String;
    const/4 v4, 0x6

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 101
    .local v10, "body":Ljava/lang/String;
    const/4 v4, 0x7

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 102
    .local v26, "imageUrl":Ljava/lang/String;
    const/16 v4, 0x8

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 103
    .local v25, "imageHints":Ljava/lang/String;
    const/16 v4, 0x9

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 104
    .local v24, "imageHeight":I
    const/16 v4, 0xa

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    .line 105
    .local v27, "imageWidth":I
    const/16 v4, 0xb

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 106
    .local v22, "authorName":Ljava/lang/String;
    const/16 v4, 0xc

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 107
    .local v20, "authorImageUrl":Ljava/lang/String;
    const/16 v4, 0xd

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 108
    .local v19, "authorImageHeight":I
    const/16 v4, 0xe

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 109
    .local v21, "authorImageWidth":I
    const/16 v4, 0x10

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 110
    .local v16, "more":I
    const/16 v4, 0x11

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 112
    .local v17, "source":Ljava/lang/String;
    const/4 v12, 0x0

    .line 113
    .local v12, "author":Lsstream/lib/objs/Author;
    const/4 v13, 0x0

    .line 115
    .local v13, "image":Lsstream/lib/objs/Image;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lsstream/lib/covers/CoverLockManager;->isStringEmptyOrNull(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 117
    const/16 v18, 0x0

    .line 119
    .local v18, "authorImage":Lsstream/lib/objs/Image;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lsstream/lib/covers/CoverLockManager;->isStringEmptyOrNull(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 120
    new-instance v18, Lsstream/lib/objs/Image;

    .end local v18    # "authorImage":Lsstream/lib/objs/Image;
    const/4 v4, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3, v4}, Lsstream/lib/objs/Image;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 123
    .restart local v18    # "authorImage":Lsstream/lib/objs/Image;
    :cond_1
    new-instance v12, Lsstream/lib/objs/Author;

    .end local v12    # "author":Lsstream/lib/objs/Author;
    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-direct {v12, v0, v1}, Lsstream/lib/objs/Author;-><init>(Ljava/lang/String;Lsstream/lib/objs/Image;)V

    .line 126
    .end local v18    # "authorImage":Lsstream/lib/objs/Image;
    .restart local v12    # "author":Lsstream/lib/objs/Author;
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lsstream/lib/covers/CoverLockManager;->isStringEmptyOrNull(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 127
    new-instance v13, Lsstream/lib/objs/Image;

    .end local v13    # "image":Lsstream/lib/objs/Image;
    move-object/from16 v0, v26

    move/from16 v1, v27

    move/from16 v2, v24

    move-object/from16 v3, v25

    invoke-direct {v13, v0, v1, v2, v3}, Lsstream/lib/objs/Image;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 130
    .restart local v13    # "image":Lsstream/lib/objs/Image;
    :cond_3
    new-instance v4, Ljava/sql/Timestamp;

    const/16 v11, 0xf

    move-object/from16 v0, v23

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    move-wide/from16 v0, v30

    invoke-direct {v4, v0, v1}, Ljava/sql/Timestamp;-><init>(J)V

    invoke-virtual {v4}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v14

    .line 132
    .local v14, "timeStamp":J
    new-instance v5, Lsstream/lib/objs/StoryItem;

    invoke-static/range {v29 .. v29}, Lsstream/lib/objs/StoryItem;->getTypeFromString(Ljava/lang/String;)Lsstream/lib/objs/StoryItem$StoryType;

    move-result-object v11

    invoke-direct/range {v5 .. v17}, Lsstream/lib/objs/StoryItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsstream/lib/objs/StoryItem$StoryType;Lsstream/lib/objs/Author;Lsstream/lib/objs/Image;JILjava/lang/String;)V

    .line 133
    .local v5, "story":Lsstream/lib/objs/StoryItem;
    move-object/from16 v0, v28

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 136
    .end local v5    # "story":Lsstream/lib/objs/StoryItem;
    .end local v6    # "id":Ljava/lang/String;
    .end local v7    # "streamId":Ljava/lang/String;
    .end local v8    # "appName":Ljava/lang/String;
    .end local v9    # "title":Ljava/lang/String;
    .end local v10    # "body":Ljava/lang/String;
    .end local v12    # "author":Lsstream/lib/objs/Author;
    .end local v13    # "image":Lsstream/lib/objs/Image;
    .end local v14    # "timeStamp":J
    .end local v16    # "more":I
    .end local v17    # "source":Ljava/lang/String;
    .end local v19    # "authorImageHeight":I
    .end local v20    # "authorImageUrl":Ljava/lang/String;
    .end local v21    # "authorImageWidth":I
    .end local v22    # "authorName":Ljava/lang/String;
    .end local v24    # "imageHeight":I
    .end local v25    # "imageHints":Ljava/lang/String;
    .end local v26    # "imageUrl":Ljava/lang/String;
    .end local v27    # "imageWidth":I
    .end local v29    # "type":Ljava/lang/String;
    :cond_4
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 139
    .end local v23    # "cursor":Landroid/database/Cursor;
    .end local v28    # "stories":Ljava/util/List;, "Ljava/util/List<Lsstream/lib/objs/StoryItem;>;"
    :goto_0
    return-object v28

    :cond_5
    sget-object v28, Lsstream/lib/covers/CoverLockManager;->EMPTY_STORY_LIST:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public getCoverItems(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "streamId"    # Ljava/lang/String;
    .param p3, "numOfItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lsstream/lib/objs/StoryItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 69
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    const-string v0, "flipboard.temporary.will.change.later.news"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "flipboard.temporary.will.change.later.social"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    :cond_0
    if-nez p3, :cond_1

    const/4 p3, 0x3

    .line 71
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lsstream/lib/covers/CoverLockManager;->queryStories(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 73
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Stream Id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isSstreamAvailable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 180
    sget-boolean v1, Lsstream/lib/covers/CoverLockManager;->mIsStreamCompatible:Z

    if-nez v1, :cond_0

    .line 181
    sget-object v1, Lsstream/lib/covers/CoverLockManager;->mMessenger:Lsstream/lib/covers/MessengerProvider;

    invoke-virtual {v1}, Lsstream/lib/covers/MessengerProvider;->connectServer()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 182
    invoke-static {}, Lsstream/lib/covers/MessengerProvider;->disconnectServer()Z

    .line 183
    sput-boolean v0, Lsstream/lib/covers/CoverLockManager;->mIsStreamCompatible:Z

    .line 184
    sget-boolean v0, Lsstream/lib/covers/CoverLockManager;->mIsStreamCompatible:Z

    .line 188
    :cond_0
    :goto_0
    return v0

    .line 186
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateItemsFromSever(Landroid/content/Context;Ljava/lang/String;Lsstream/lib/covers/StreamUpdateListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "streamId"    # Ljava/lang/String;
    .param p3, "listener"    # Lsstream/lib/covers/StreamUpdateListener;

    .prologue
    .line 167
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const-string v0, "flipboard.temporary.will.change.later.news"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "flipboard.temporary.will.change.later.social"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    :cond_0
    sget-object v0, Lsstream/lib/covers/CoverLockManager;->mMessenger:Lsstream/lib/covers/MessengerProvider;

    invoke-virtual {v0, p2, p3}, Lsstream/lib/covers/MessengerProvider;->updateStoryItems(Ljava/lang/String;Lsstream/lib/covers/StreamUpdateListener;)V

    .line 172
    return-void

    .line 170
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Stream Id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
