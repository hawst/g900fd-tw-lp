.class public Lcom/android/backupconfirm/BackupRestoreConfirmation;
.super Landroid/app/Activity;
.source "BackupRestoreConfirmation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;,
        Lcom/android/backupconfirm/BackupRestoreConfirmation$ObserverHandler;
    }
.end annotation


# instance fields
.field mAllowButton:Landroid/widget/Button;

.field mBackupManager:Landroid/app/backup/IBackupManager;

.field mCurPassword:Landroid/widget/TextView;

.field mDenyButton:Landroid/widget/Button;

.field mDidAcknowledge:Z

.field mEncPassword:Landroid/widget/TextView;

.field mHandler:Landroid/os/Handler;

.field mIsEncrypted:Z

.field mMountService:Landroid/os/storage/IMountService;

.field mObserver:Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;

.field mStatusView:Landroid/widget/TextView;

.field mToken:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 277
    return-void
.end method


# virtual methods
.method deviceIsEncrypted()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 253
    :try_start_0
    iget-object v2, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mMountService:Landroid/os/storage/IMountService;

    invoke-interface {v2}, Landroid/os/storage/IMountService;->getEncryptionState()I

    move-result v2

    if-eq v2, v1, :cond_0

    iget-object v2, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mMountService:Landroid/os/storage/IMountService;

    invoke-interface {v2}, Landroid/os/storage/IMountService;->getPasswordType()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eq v2, v1, :cond_0

    .line 261
    :goto_0
    return v1

    .line 253
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 257
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "BackupRestoreConfirmation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to communicate with mount service: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method haveBackupPassword()Z
    .locals 2

    .prologue
    .line 267
    :try_start_0
    iget-object v1, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mBackupManager:Landroid/app/backup/IBackupManager;

    invoke-interface {v1}, Landroid/app/backup/IBackupManager;->hasBackupPassword()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 269
    :goto_0
    return v1

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/16 v11, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 133
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 135
    invoke-virtual {p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 136
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "action":Ljava/lang/String;
    const-string v7, "fullback"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 141
    const/high16 v4, 0x7f020000

    .line 142
    .local v4, "layoutId":I
    const/high16 v6, 0x7f030000

    .line 152
    .local v6, "titleId":I
    :goto_0
    const-string v7, "conftoken"

    const/4 v10, -0x1

    invoke-virtual {v3, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mToken:I

    .line 153
    iget v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mToken:I

    if-gez v7, :cond_3

    .line 154
    const-string v7, "BackupRestoreConfirmation"

    const-string v8, "Backup/restore confirmation requested but no token passed!"

    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-virtual {p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->finish()V

    .line 221
    .end local v4    # "layoutId":I
    .end local v6    # "titleId":I
    :cond_0
    :goto_1
    return-void

    .line 143
    :cond_1
    const-string v7, "fullrest"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 144
    const v4, 0x7f020001

    .line 145
    .restart local v4    # "layoutId":I
    const v6, 0x7f030001

    .restart local v6    # "titleId":I
    goto :goto_0

    .line 147
    .end local v4    # "layoutId":I
    .end local v6    # "titleId":I
    :cond_2
    const-string v7, "BackupRestoreConfirmation"

    const-string v8, "Backup/restore confirmation activity launched with invalid action!"

    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-virtual {p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->finish()V

    goto :goto_1

    .line 159
    .restart local v4    # "layoutId":I
    .restart local v6    # "titleId":I
    :cond_3
    const-string v7, "backup"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    move-result-object v7

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mBackupManager:Landroid/app/backup/IBackupManager;

    .line 160
    const-string v7, "mount"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    move-result-object v7

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mMountService:Landroid/os/storage/IMountService;

    .line 162
    new-instance v7, Lcom/android/backupconfirm/BackupRestoreConfirmation$ObserverHandler;

    invoke-virtual {p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v7, p0, v10}, Lcom/android/backupconfirm/BackupRestoreConfirmation$ObserverHandler;-><init>(Lcom/android/backupconfirm/BackupRestoreConfirmation;Landroid/content/Context;)V

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mHandler:Landroid/os/Handler;

    .line 163
    invoke-virtual {p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v5

    .line 164
    .local v5, "oldObserver":Ljava/lang/Object;
    if-nez v5, :cond_5

    .line 165
    new-instance v7, Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;

    iget-object v10, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mHandler:Landroid/os/Handler;

    invoke-direct {v7, p0, v10}, Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;-><init>(Lcom/android/backupconfirm/BackupRestoreConfirmation;Landroid/os/Handler;)V

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mObserver:Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;

    .line 171
    .end local v5    # "oldObserver":Ljava/lang/Object;
    :goto_2
    invoke-virtual {p0, v6}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->setTitle(I)V

    .line 172
    invoke-virtual {p0, v4}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->setContentView(I)V

    .line 175
    const v7, 0x7f040005

    invoke-virtual {p0, v7}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mStatusView:Landroid/widget/TextView;

    .line 176
    const v7, 0x7f040007

    invoke-virtual {p0, v7}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mAllowButton:Landroid/widget/Button;

    .line 177
    const v7, 0x7f040006

    invoke-virtual {p0, v7}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDenyButton:Landroid/widget/Button;

    .line 179
    const v7, 0x7f040002

    invoke-virtual {p0, v7}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mCurPassword:Landroid/widget/TextView;

    .line 180
    const v7, 0x7f040004

    invoke-virtual {p0, v7}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mEncPassword:Landroid/widget/TextView;

    .line 181
    const v7, 0x7f040001

    invoke-virtual {p0, v7}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 185
    .local v1, "curPwDesc":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->deviceIsEncrypted()Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mIsEncrypted:Z

    .line 186
    invoke-virtual {p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->haveBackupPassword()Z

    move-result v7

    if-nez v7, :cond_4

    .line 187
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    iget-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mCurPassword:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    const/high16 v7, 0x7f020000

    if-ne v4, v7, :cond_4

    .line 190
    const v7, 0x7f040003

    invoke-virtual {p0, v7}, Lcom/android/backupconfirm/BackupRestoreConfirmation;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 191
    .local v2, "encPwDesc":Landroid/widget/TextView;
    iget-boolean v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mIsEncrypted:Z

    if-eqz v7, :cond_6

    const v7, 0x7f03000d

    :goto_3
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 197
    .end local v2    # "encPwDesc":Landroid/widget/TextView;
    :cond_4
    iget-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mAllowButton:Landroid/widget/Button;

    new-instance v10, Lcom/android/backupconfirm/BackupRestoreConfirmation$1;

    invoke-direct {v10, p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation$1;-><init>(Lcom/android/backupconfirm/BackupRestoreConfirmation;)V

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDenyButton:Landroid/widget/Button;

    new-instance v10, Lcom/android/backupconfirm/BackupRestoreConfirmation$2;

    invoke-direct {v10, p0}, Lcom/android/backupconfirm/BackupRestoreConfirmation$2;-><init>(Lcom/android/backupconfirm/BackupRestoreConfirmation;)V

    invoke-virtual {v7, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    if-eqz p1, :cond_0

    .line 217
    const-string v7, "did_acknowledge"

    invoke-virtual {p1, v7, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDidAcknowledge:Z

    .line 218
    iget-object v10, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mAllowButton:Landroid/widget/Button;

    iget-boolean v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDidAcknowledge:Z

    if-nez v7, :cond_7

    move v7, v8

    :goto_4
    invoke-virtual {v10, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 219
    iget-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDenyButton:Landroid/widget/Button;

    iget-boolean v10, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDidAcknowledge:Z

    if-nez v10, :cond_8

    :goto_5
    invoke-virtual {v7, v8}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_1

    .line 167
    .end local v1    # "curPwDesc":Landroid/widget/TextView;
    .restart local v5    # "oldObserver":Ljava/lang/Object;
    :cond_5
    check-cast v5, Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;

    .end local v5    # "oldObserver":Ljava/lang/Object;
    iput-object v5, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mObserver:Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;

    .line 168
    iget-object v7, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mObserver:Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;

    iget-object v10, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;->setHandler(Landroid/os/Handler;)V

    goto/16 :goto_2

    .line 191
    .restart local v1    # "curPwDesc":Landroid/widget/TextView;
    .restart local v2    # "encPwDesc":Landroid/widget/TextView;
    :cond_6
    const v7, 0x7f03000c

    goto :goto_3

    .end local v2    # "encPwDesc":Landroid/widget/TextView;
    :cond_7
    move v7, v9

    .line 218
    goto :goto_4

    :cond_8
    move v8, v9

    .line 219
    goto :goto_5
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mObserver:Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 231
    const-string v0, "did_acknowledge"

    iget-boolean v1, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDidAcknowledge:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 232
    return-void
.end method

.method sendAcknowledgement(IZLandroid/app/backup/IFullBackupRestoreObserver;)V
    .locals 7
    .param p1, "token"    # I
    .param p2, "allow"    # Z
    .param p3, "observer"    # Landroid/app/backup/IFullBackupRestoreObserver;

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDidAcknowledge:Z

    if-nez v0, :cond_0

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mDidAcknowledge:Z

    .line 239
    :try_start_0
    iget-object v0, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mEncPassword:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    .line 240
    .local v6, "encPassword":Ljava/lang/CharSequence;
    iget-object v0, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mBackupManager:Landroid/app/backup/IBackupManager;

    iget v1, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mToken:I

    iget-object v2, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mCurPassword:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/backupconfirm/BackupRestoreConfirmation;->mObserver:Lcom/android/backupconfirm/BackupRestoreConfirmation$FullObserver;

    move v2, p2

    invoke-interface/range {v0 .. v5}, Landroid/app/backup/IBackupManager;->acknowledgeFullBackupOrRestore(IZLjava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    .end local v6    # "encPassword":Ljava/lang/CharSequence;
    :cond_0
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v0

    goto :goto_0
.end method
