.class public Lcom/samsung/android/scloud/proxy/sbrowser/SyncService;
.super Landroid/app/Service;
.source "SyncService.java"


# static fields
.field private static sSyncAdapter:Lcom/samsung/android/scloud/proxy/sbrowser/SyncAdapter;

.field private static final sSyncAdapterLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/proxy/sbrowser/SyncService;->sSyncAdapterLock:Ljava/lang/Object;

    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/proxy/sbrowser/SyncService;->sSyncAdapter:Lcom/samsung/android/scloud/proxy/sbrowser/SyncAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/android/scloud/proxy/sbrowser/SyncService;->sSyncAdapter:Lcom/samsung/android/scloud/proxy/sbrowser/SyncAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/proxy/sbrowser/SyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 36
    sget-object v1, Lcom/samsung/android/scloud/proxy/sbrowser/SyncService;->sSyncAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    .line 37
    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/proxy/sbrowser/SyncService;->sSyncAdapter:Lcom/samsung/android/scloud/proxy/sbrowser/SyncAdapter;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/samsung/android/scloud/proxy/sbrowser/SyncAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/proxy/sbrowser/SyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/scloud/proxy/sbrowser/SyncAdapter;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/samsung/android/scloud/proxy/sbrowser/SyncService;->sSyncAdapter:Lcom/samsung/android/scloud/proxy/sbrowser/SyncAdapter;

    .line 40
    :cond_0
    monitor-exit v1

    .line 41
    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
