.class public Lcom/google/android/setupwizard/util/SetupWizardIllustration;
.super Landroid/widget/FrameLayout;
.source "SetupWizardIllustration.java"


# instance fields
.field private mAspectRatio:F

.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mBaselineGridSize:F

.field private mForeground:Landroid/graphics/drawable/Drawable;

.field private mForegroundHeight:I

.field private mScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    .line 46
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->init()V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 66
    sget-object v1, Lcom/google/android/gsf/login/R$styleable;->SetupWizardIllustration:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 68
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    .line 69
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->init()V

    .line 71
    return-void
.end method


# virtual methods
.method protected init()V
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41000000    # 8.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBaselineGridSize:F

    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setWillNotDraw(Z)V

    .line 77
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 135
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 137
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 138
    .local v0, "saveCount":I
    iget v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundHeight:I

    int-to-float v1, v1

    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 140
    iget v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    iget v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    invoke-virtual {p1, v1, v2, v3, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 141
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 142
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 144
    .end local v0    # "saveCount":I
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 148
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 149
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 110
    sub-int v3, p4, p2

    .line 111
    .local v3, "layoutWidth":I
    sub-int v2, p5, p3

    .line 112
    .local v2, "layoutHeight":I
    iget-object v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_2

    .line 113
    iget-object v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    int-to-float v1, v4

    .line 114
    .local v1, "intrinsicWidth":F
    iget-object v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v0, v4

    .line 115
    .local v0, "intrinsicHeight":F
    cmpg-float v4, v1, v5

    if-ltz v4, :cond_0

    cmpg-float v4, v0, v5

    if-gez v4, :cond_1

    .line 116
    :cond_0
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    const-string v5, "Foreground drawable must have intrinsic size defined"

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 120
    :cond_1
    int-to-float v4, v3

    div-float/2addr v4, v1

    iput v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    .line 121
    iget v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    mul-float/2addr v4, v0

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundHeight:I

    .line 122
    iget-object v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundHeight:I

    invoke-virtual {v4, v8, v8, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 124
    .end local v0    # "intrinsicHeight":F
    .end local v1    # "intrinsicWidth":F
    :cond_2
    iget-object v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_3

    .line 127
    iget-object v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    int-to-float v5, v3

    iget v6, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    iget v6, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundHeight:I

    sub-int v6, v2, v6

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScale:F

    div-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    invoke-virtual {v4, v8, v8, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 130
    :cond_3
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 131
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v5, 0x0

    .line 99
    iget v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 100
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 101
    .local v1, "parentWidth":I
    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    div-float/2addr v2, v3

    float-to-int v0, v2

    .line 102
    .local v0, "illustrationHeight":I
    int-to-float v2, v0

    int-to-float v3, v0

    iget v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBaselineGridSize:F

    rem-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v0, v2

    .line 103
    invoke-virtual {p0, v5, v0, v5, v5}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setPadding(IIII)V

    .line 105
    .end local v0    # "illustrationHeight":I
    .end local v1    # "parentWidth":I
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 106
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 86
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "foreground"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    .line 95
    return-void
.end method
