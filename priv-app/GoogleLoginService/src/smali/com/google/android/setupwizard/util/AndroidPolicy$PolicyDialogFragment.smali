.class public Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;
.super Landroid/app/DialogFragment;
.source "AndroidPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/AndroidPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PolicyDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 315
    return-void
.end method

.method public static newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;
    .locals 3
    .param p0, "policy"    # Lcom/google/android/setupwizard/util/AndroidPolicy;

    .prologue
    .line 298
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 299
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "policy"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 301
    new-instance v1, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    invoke-direct {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;-><init>()V

    .line 302
    .local v1, "fragment":Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 303
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "policy"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 320
    .local v0, "policy":Lcom/google/android/setupwizard/util/AndroidPolicy;
    new-instance v1, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0, p1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;-><init>(Landroid/content/Context;Lcom/google/android/setupwizard/util/AndroidPolicy;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;

    .line 309
    .local v0, "dialog":Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;
    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->saveState(Landroid/os/Bundle;)V

    .line 310
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 311
    return-void
.end method
