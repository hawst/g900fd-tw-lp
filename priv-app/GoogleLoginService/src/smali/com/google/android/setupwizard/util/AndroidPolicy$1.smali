.class final Lcom/google/android/setupwizard/util/AndroidPolicy$1;
.super Ljava/lang/Object;
.source "AndroidPolicy.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/AndroidPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/setupwizard/util/AndroidPolicy;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/setupwizard/util/AndroidPolicy;
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 342
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 343
    .local v3, "titleRes":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 344
    .local v2, "gservicesProperty":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 345
    .local v1, "fallbackUrl":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, "failUrl":Ljava/lang/String;
    new-instance v4, Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 339
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/util/AndroidPolicy$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/setupwizard/util/AndroidPolicy;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/setupwizard/util/AndroidPolicy;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 351
    new-array v0, p1, [Lcom/google/android/setupwizard/util/AndroidPolicy;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 339
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/util/AndroidPolicy$1;->newArray(I)[Lcom/google/android/setupwizard/util/AndroidPolicy;

    move-result-object v0

    return-object v0
.end method
