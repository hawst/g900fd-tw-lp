.class public final Lcom/google/android/gsf/login/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/login/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AdsAttrs:[I

.field public static final AppDataSearch:[I

.field public static final Corpus:[I

.field public static final FeatureParam:[I

.field public static final GlobalSearch:[I

.field public static final GlobalSearchCorpus:[I

.field public static final GlobalSearchSection:[I

.field public static final IMECorpus:[I

.field public static final MapAttrs:[I

.field public static final Section:[I

.field public static final SectionFeature:[I

.field public static final SetupWizardIllustration:[I

.field public static final WalletFragmentOptions:[I

.field public static final WalletFragmentStyle:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3177
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->AdsAttrs:[I

    .line 3247
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->AppDataSearch:[I

    .line 3273
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->Corpus:[I

    .line 3373
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->FeatureParam:[I

    .line 3431
    new-array v0, v5, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->GlobalSearch:[I

    .line 3553
    new-array v0, v3, [I

    const v1, 0x7f010018

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->GlobalSearchCorpus:[I

    .line 3593
    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->GlobalSearchSection:[I

    .line 3664
    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->IMECorpus:[I

    .line 3810
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->MapAttrs:[I

    .line 4042
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->Section:[I

    .line 4177
    new-array v0, v3, [I

    const v1, 0x7f01000f

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->SectionFeature:[I

    .line 4215
    new-array v0, v3, [I

    const v1, 0x7f010041

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->SetupWizardIllustration:[I

    .line 4248
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->WalletFragmentOptions:[I

    .line 4352
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/google/android/gsf/login/R$styleable;->WalletFragmentStyle:[I

    return-void

    .line 3177
    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
    .end array-data

    .line 3273
    :array_1
    .array-data 4
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
    .end array-data

    .line 3373
    :array_2
    .array-data 4
        0x7f010010
        0x7f010011
    .end array-data

    .line 3431
    :array_3
    .array-data 4
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
    .end array-data

    .line 3593
    :array_4
    .array-data 4
        0x7f01001f
        0x7f010020
    .end array-data

    .line 3664
    :array_5
    .array-data 4
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
    .end array-data

    .line 3810
    :array_6
    .array-data 4
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
    .end array-data

    .line 4042
    :array_7
    .array-data 4
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
    .end array-data

    .line 4248
    :array_8
    .array-data 4
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
    .end array-data

    .line 4352
    :array_9
    .array-data 4
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
    .end array-data
.end method
