.class public Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;
.super Ljava/lang/Object;
.source "SyncSettingsObserver.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/login/SyncSettingsObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncSettingsItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final adapterType:Landroid/content/SyncAdapterType;

.field public final label:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/SyncAdapterType;Ljava/lang/String;)V
    .locals 0
    .param p1, "adapterType"    # Landroid/content/SyncAdapterType;
    .param p2, "label"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->adapterType:Landroid/content/SyncAdapterType;

    .line 36
    iput-object p2, p0, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->label:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;)I
    .locals 2
    .param p1, "another"    # Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->label:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p1, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->compareTo(Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;)I

    move-result v0

    return v0
.end method

.method public getSyncAdapterType()Landroid/content/SyncAdapterType;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->adapterType:Landroid/content/SyncAdapterType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->label:Ljava/lang/String;

    return-object v0
.end method
