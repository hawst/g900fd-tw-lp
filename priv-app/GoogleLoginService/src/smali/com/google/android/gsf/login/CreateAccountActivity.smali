.class public Lcom/google/android/gsf/login/CreateAccountActivity;
.super Lcom/google/android/gsf/login/AddAccountActivity;
.source "CreateAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/CreateAccountActivity$4;
    }
.end annotation


# static fields
.field private static final LOG_PREFIX:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/google/android/gsf/login/CreateAccountActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gsf/login/CreateAccountActivity;->LOG_PREFIX:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gsf/login/AddAccountActivity;-><init>()V

    .line 330
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/CreateAccountActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/login/CreateAccountActivity;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->launchGoogleServicesOrTos()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gsf/login/CreateAccountActivity;)Lcom/google/android/gsf/loginservice/GLSSession;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/login/CreateAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/gsf/login/CreateAccountActivity;)Lcom/google/android/gsf/loginservice/GLSSession;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/login/CreateAccountActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    return-object v0
.end method

.method private buildRecoveryActivityIntent(Lcom/google/android/gsf/loginservice/GLSSession;)Landroid/content/Intent;
    .locals 2
    .param p1, "session"    # Lcom/google/android/gsf/loginservice/GLSSession;

    .prologue
    .line 443
    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/gsf/login/RecoveryDataActivity;->getIntentBuilder(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;

    move-result-object v0

    .line 445
    .local v0, "builder":Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;
    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setCountry(Ljava/lang/String;)V

    .line 446
    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setCountryListJson(Ljava/lang/String;)V

    .line 450
    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mSecondaryEmail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setSecondaryEmail(Ljava/lang/String;)V

    .line 451
    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setPhoneNumber(Ljava/lang/String;)V

    .line 452
    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setCountry(Ljava/lang/String;)V

    .line 453
    invoke-virtual {v0}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->getIntent()Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private handleBack(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0x3fa

    const/16 v4, 0x40d

    const/16 v5, 0x404

    .line 58
    sparse-switch p1, :sswitch_data_0

    .line 161
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/AddAccountActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 61
    :sswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/CreateAccountActivity;->setResult(I)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->finish()V

    goto :goto_0

    .line 66
    :sswitch_1
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gsf/login/NameActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3, v6}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 73
    :sswitch_2
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gsf/login/UsernameActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3, v5}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 81
    :sswitch_3
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gsf/login/NameActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3, v6}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 87
    :sswitch_4
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gsf/login/UsernameActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3, v5}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 93
    :sswitch_5
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gsf/login/ChoosePasswordActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v4, 0x3f4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 100
    :sswitch_6
    invoke-static {p0}, Lcom/google/android/gsf/login/RecoveryIntroActivity;->getIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 106
    :sswitch_7
    invoke-static {p0}, Lcom/google/android/gsf/login/RecoveryIntroActivity;->getIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 112
    :sswitch_8
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->getGoogleServicesActivityIntent()Landroid/content/Intent;

    move-result-object v3

    const/16 v4, 0x411

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 118
    :sswitch_9
    invoke-virtual {p0, p2, p3}, Lcom/google/android/gsf/login/CreateAccountActivity;->finishGoogleServicesActivity(ILandroid/content/Intent;)V

    .line 119
    iget-object v3, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    invoke-direct {p0, v3}, Lcom/google/android/gsf/login/CreateAccountActivity;->isRecoveryDataPresent(Lcom/google/android/gsf/loginservice/GLSSession;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 120
    iget-object v3, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    invoke-direct {p0, v3}, Lcom/google/android/gsf/login/CreateAccountActivity;->buildRecoveryActivityIntent(Lcom/google/android/gsf/loginservice/GLSSession;)Landroid/content/Intent;

    move-result-object v2

    .line 121
    .local v2, "recoveryActivityIntent":Landroid/content/Intent;
    const/16 v3, 0x40e

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 124
    .end local v2    # "recoveryActivityIntent":Landroid/content/Intent;
    :cond_1
    invoke-static {p0}, Lcom/google/android/gsf/login/RecoveryIntroActivity;->getIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 132
    :sswitch_a
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gsf/login/UsernameActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3, v5}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 138
    :sswitch_b
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/CreateAccountActivity;->setResult(I)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->finish()V

    goto/16 :goto_0

    .line 145
    :sswitch_c
    const-string v3, "GLSActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/google/android/gsf/login/CreateAccountActivity;->LOG_PREFIX:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Unexpected back button event from MARKET_REQUEST"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 148
    :sswitch_d
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->tryGetMarketIntent()Landroid/content/Intent;

    move-result-object v1

    .line 151
    .local v1, "marketIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->isESEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mAllowGooglePlus:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mHasGooglePlus:Z

    if-eqz v3, :cond_2

    const/4 v0, 0x1

    .line 153
    .local v0, "isPostGplusSignUpScreenBackButtonDisabled":Z
    :goto_1
    if-eqz v1, :cond_0

    .line 154
    const-string v3, "noBack"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 157
    const/16 v3, 0x3fc

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 151
    .end local v0    # "isPostGplusSignUpScreenBackButtonDisabled":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 58
    nop

    :sswitch_data_0
    .sparse-switch
        0x3eb -> :sswitch_a
        0x3ed -> :sswitch_d
        0x3f1 -> :sswitch_b
        0x3f2 -> :sswitch_8
        0x3f4 -> :sswitch_4
        0x3f8 -> :sswitch_2
        0x3f9 -> :sswitch_3
        0x3fa -> :sswitch_0
        0x3fc -> :sswitch_c
        0x404 -> :sswitch_1
        0x40d -> :sswitch_5
        0x40e -> :sswitch_7
        0x40f -> :sswitch_6
        0x411 -> :sswitch_9
    .end sparse-switch
.end method

.method private isRecoveryDataPresent(Lcom/google/android/gsf/loginservice/GLSSession;)Z
    .locals 3
    .param p1, "session"    # Lcom/google/android/gsf/loginservice/GLSSession;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 432
    iget-object v2, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryList:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 439
    :cond_1
    :goto_0
    return v0

    .line 436
    :cond_2
    iget-object v2, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mSecondaryEmail:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 439
    iget-object v2, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private launchGoogleServicesOrTos()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mSuppressGoogleServicesActivity:Z

    if-eqz v0, :cond_0

    .line 421
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/TermsOfServiceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3f2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 429
    :goto_0
    return-void

    .line 425
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->getGoogleServicesActivityIntent()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x411

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private showTryAgainDialog()V
    .locals 6

    .prologue
    .line 378
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030025

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 379
    .local v0, "content":Landroid/view/View;
    const v3, 0x7f0d0034

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 380
    .local v2, "msg":Landroid/widget/TextView;
    const v3, 0x7f070147

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 381
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f070149

    new-instance v5, Lcom/google/android/gsf/login/CreateAccountActivity$3;

    invoke-direct {v5, p0}, Lcom/google/android/gsf/login/CreateAccountActivity$3;-><init>(Lcom/google/android/gsf/login/CreateAccountActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f070148

    new-instance v5, Lcom/google/android/gsf/login/CreateAccountActivity$2;

    invoke-direct {v5, p0}, Lcom/google/android/gsf/login/CreateAccountActivity$2;-><init>(Lcom/google/android/gsf/login/CreateAccountActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/google/android/gsf/login/CreateAccountActivity$1;

    invoke-direct {v4, p0}, Lcom/google/android/gsf/login/CreateAccountActivity$1;-><init>(Lcom/google/android/gsf/login/CreateAccountActivity;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    .line 417
    .local v1, "dialog":Landroid/app/AlertDialog;
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v10, 0x3f9

    const/16 v8, 0x3f8

    const/16 v7, 0x3f4

    const/4 v5, -0x1

    const/4 v9, 0x1

    .line 167
    if-ne p2, v9, :cond_1

    .line 168
    invoke-virtual {p0, v9}, Lcom/google/android/gsf/login/CreateAccountActivity;->setResult(I)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->finish()V

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    if-nez p2, :cond_2

    .line 174
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gsf/login/CreateAccountActivity;->handleBack(IILandroid/content/Intent;)V

    goto :goto_0

    .line 178
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 368
    const-string v5, "%s Unknown activity result req=%s  rc=%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/gsf/login/CreateAccountActivity;->LOG_PREFIX:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    const/4 v7, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 372
    .local v2, "err":Ljava/lang/String;
    const-string v5, "GLSActivity"

    invoke-static {v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/AddAccountActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 180
    .end local v2    # "err":Ljava/lang/String;
    :sswitch_0
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v9, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mNameActivityCompleted:Z

    .line 181
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/UsernameActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v6, 0x404

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 187
    :sswitch_1
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/CheckAvailTask;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5, v8}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 193
    :sswitch_2
    if-ne p2, v5, :cond_3

    .line 194
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/ChoosePasswordActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5, v7}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 198
    :cond_3
    invoke-static {p3}, Lcom/google/android/gsf/loginservice/StatusHelper;->fromExtra(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/shared/Status;

    move-result-object v3

    .line 199
    .local v3, "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/SuggestUsernameActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5, v10}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 206
    .end local v3    # "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :sswitch_3
    if-ne p2, v5, :cond_4

    .line 207
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/ChoosePasswordActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5, v7}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 211
    :cond_4
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/CheckAvailTask;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5, v8}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 219
    :sswitch_4
    if-ne p2, v5, :cond_0

    .line 220
    invoke-static {p0}, Lcom/google/android/gsf/login/RecoveryIntroActivity;->getIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    const/16 v6, 0x40d

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 230
    :sswitch_5
    if-ne p2, v5, :cond_5

    .line 231
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSSession;->mCallingAppDescription:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-static {p0, v5, v6}, Lcom/google/android/gsf/login/GetCountryListTask;->getIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Landroid/content/Intent;

    move-result-object v5

    const/16 v6, 0x40f

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 238
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->launchGoogleServicesOrTos()V

    goto/16 :goto_0

    .line 244
    :sswitch_6
    if-ne p2, v5, :cond_a

    .line 245
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/google/android/gsf/login/RecoveryDataActivity;->getIntentBuilder(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;

    move-result-object v0

    .line 247
    .local v0, "builder":Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 248
    .local v1, "data":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v6, "r_country_list"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryList:Ljava/lang/String;

    .line 249
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    if-nez v5, :cond_6

    .line 253
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v6, "r_country"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    .line 255
    :cond_6
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setCountry(Ljava/lang/String;)V

    .line 256
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryList:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setCountryListJson(Ljava/lang/String;)V

    .line 260
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mSecondaryEmail:Ljava/lang/String;

    if-eqz v5, :cond_7

    .line 261
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mSecondaryEmail:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setSecondaryEmail(Ljava/lang/String;)V

    .line 263
    :cond_7
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneNumber:Ljava/lang/String;

    if-eqz v5, :cond_8

    .line 264
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setPhoneNumber(Ljava/lang/String;)V

    .line 266
    :cond_8
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    if-eqz v5, :cond_9

    .line 267
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->setCountry(Ljava/lang/String;)V

    .line 269
    :cond_9
    invoke-virtual {v0}, Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v6, 0x40e

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 271
    .end local v0    # "builder":Lcom/google/android/gsf/login/RecoveryDataActivity$IntentBuilder;
    .end local v1    # "data":Landroid/os/Bundle;
    :cond_a
    invoke-direct {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->showTryAgainDialog()V

    goto/16 :goto_0

    .line 276
    :sswitch_7
    if-eqz p3, :cond_b

    .line 277
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v6, "r_secondaryEmail"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mSecondaryEmail:Ljava/lang/String;

    .line 279
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v6, "r_phone_number"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneNumber:Ljava/lang/String;

    .line 280
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v6, "r_country"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mPhoneCountryCode:Ljava/lang/String;

    .line 282
    :cond_b
    invoke-direct {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->launchGoogleServicesOrTos()V

    goto/16 :goto_0

    .line 286
    :sswitch_8
    invoke-virtual {p0, p2, p3}, Lcom/google/android/gsf/login/CreateAccountActivity;->finishGoogleServicesActivity(ILandroid/content/Intent;)V

    .line 287
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/TermsOfServiceActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v6, 0x3f2

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 293
    :sswitch_9
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v5, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mTermsOfServiceShown:Z

    if-eqz v5, :cond_c

    .line 294
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/CreateAccountTask;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v6, 0x3eb

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 299
    :cond_c
    invoke-virtual {p0, v9}, Lcom/google/android/gsf/login/CreateAccountActivity;->setResult(I)V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->finish()V

    goto/16 :goto_0

    .line 305
    :sswitch_a
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/CreateAccountActivity;->setResult(I)V

    .line 306
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->finish()V

    goto/16 :goto_0

    .line 310
    :sswitch_b
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/ChoosePasswordActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5, v7}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 315
    :sswitch_c
    if-eq p2, v5, :cond_d

    .line 316
    invoke-static {p3}, Lcom/google/android/gsf/loginservice/StatusHelper;->fromExtra(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/shared/Status;

    move-result-object v3

    .line 317
    .restart local v3    # "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    sget-object v5, Lcom/google/android/gsf/login/CreateAccountActivity$4;->$SwitchMap$com$google$android$gms$auth$firstparty$shared$Status:[I

    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/shared/Status;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 329
    const/16 v5, 0x3f1

    invoke-virtual {p0, p3, v5}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 340
    .end local v3    # "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :cond_d
    :goto_1
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v9, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mIsNewAccount:Z

    .line 341
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->signalAccountInstallation()V

    .line 342
    invoke-virtual {p0, v9}, Lcom/google/android/gsf/login/CreateAccountActivity;->doMarketAndSyncWork(Z)V

    goto/16 :goto_0

    .line 319
    .restart local v3    # "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :pswitch_0
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/SuggestUsernameActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5, v10}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 325
    :pswitch_1
    const/16 v5, 0x3f5

    invoke-virtual {p0, p3, v5}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 346
    .end local v3    # "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :sswitch_d
    if-eqz p3, :cond_e

    .line 347
    iget-object v5, p0, Lcom/google/android/gsf/login/CreateAccountActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v6, "redeemed_offer_message_html"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/gsf/loginservice/GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    .line 353
    :cond_e
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gsf/login/SyncIntroActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "noBack"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    .line 355
    .local v4, "syncIntroIntent":Landroid/content/Intent;
    const/16 v5, 0x3ed

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 359
    .end local v4    # "syncIntroIntent":Landroid/content/Intent;
    :sswitch_e
    invoke-virtual {p0, v9}, Lcom/google/android/gsf/login/CreateAccountActivity;->onSetupComplete(Z)V

    .line 360
    if-nez p3, :cond_f

    .line 361
    new-instance p3, Landroid/content/Intent;

    .end local p3    # "intent":Landroid/content/Intent;
    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    .line 363
    .restart local p3    # "intent":Landroid/content/Intent;
    :cond_f
    invoke-virtual {p0, p3}, Lcom/google/android/gsf/login/CreateAccountActivity;->bundleResultData(Landroid/content/Intent;)V

    .line 364
    invoke-virtual {p0, v5, p3}, Lcom/google/android/gsf/login/CreateAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 365
    invoke-virtual {p0}, Lcom/google/android/gsf/login/CreateAccountActivity;->finish()V

    goto/16 :goto_0

    .line 178
    :sswitch_data_0
    .sparse-switch
        0x3eb -> :sswitch_c
        0x3ed -> :sswitch_e
        0x3f1 -> :sswitch_a
        0x3f2 -> :sswitch_9
        0x3f4 -> :sswitch_4
        0x3f5 -> :sswitch_b
        0x3f8 -> :sswitch_2
        0x3f9 -> :sswitch_3
        0x3fa -> :sswitch_0
        0x3fc -> :sswitch_d
        0x404 -> :sswitch_1
        0x40d -> :sswitch_5
        0x40e -> :sswitch_7
        0x40f -> :sswitch_6
        0x411 -> :sswitch_8
    .end sparse-switch

    .line 317
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/AddAccountActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    if-nez p1, :cond_0

    .line 53
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/NameActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3fa

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/CreateAccountActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 55
    :cond_0
    return-void
.end method
