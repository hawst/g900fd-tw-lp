.class public Lcom/google/android/gsf/login/LoginActivity;
.super Lcom/google/android/gsf/login/AddAccountActivity;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/LoginActivity$1;
    }
.end annotation


# instance fields
.field private mCarrierSetupLaunchIntent:Landroid/content/Intent;

.field private mCarrierSetupSuccessfullyRan:Z

.field private final mPreExistingAccounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/gsf/login/AddAccountActivity;-><init>()V

    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mPreExistingAccounts:Ljava/util/Set;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupSuccessfullyRan:Z

    return-void
.end method

.method private createCarrierSetupLaunchIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 100
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/LoginActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 101
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.ACTION_CARRIER_SETUP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 102
    .local v1, "carrierSetupIntent":Landroid/content/Intent;
    const-string v3, "device_setup"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 103
    invoke-virtual {v2, v1}, Landroid/telephony/TelephonyManager;->getCarrierPackageNamesForIntent(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 106
    .local v0, "carrierPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 107
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 108
    const-string v3, "GLSActivity"

    const-string v4, "Multiple matching carrier apps found, launching the first."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_0
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    .end local v1    # "carrierSetupIntent":Landroid/content/Intent;
    :goto_0
    return-object v1

    .restart local v1    # "carrierSetupIntent":Landroid/content/Intent;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isCarrierSetupAllowed()Z
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->isSetupWizard()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupLaunchIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupSuccessfullyRan:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchPasswordPromptTask()V
    .locals 4

    .prologue
    .line 443
    const/4 v0, 0x0

    .line 446
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mIsBrowserFlowRequired:Z

    if-nez v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 447
    .local v1, "result":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 448
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v2, Lcom/google/android/gsf/login/UsernamePasswordActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 449
    .restart local v0    # "intent":Landroid/content/Intent;
    const/16 v2, 0x402

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 455
    :goto_1
    return-void

    .line 446
    .end local v1    # "result":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 451
    .restart local v1    # "result":Z
    :cond_2
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v2, Lcom/google/android/gsf/login/BrowserActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 452
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v2, "authAccount"

    iget-object v3, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 453
    const/16 v2, 0x3ec

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method private onGoogleServicesActivityFinished()V
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->tryGetMarketIntent()Landroid/content/Intent;

    move-result-object v0

    .line 186
    .local v0, "marketIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 187
    const/16 v1, 0x3fc

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->onMarketActivityFinished()V

    goto :goto_0
.end method

.method private onMarketActivityFinished()V
    .locals 5

    .prologue
    .line 194
    iget-object v3, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mSetupWizard:Z

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->isFirstAccount()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 195
    :cond_0
    new-instance v0, Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google"

    invoke-direct {v0, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->isPrimaryUser()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mPerformBackup:Z

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    .line 200
    .local v1, "performBackup":Z
    :goto_0
    invoke-static {v0}, Lcom/google/android/gsf/login/SyncIntroActivity;->enableAllSyncAdapters(Landroid/accounts/Account;)V

    .line 202
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->isPrimaryUser()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 203
    invoke-static {p0, v0}, Lcom/google/android/gsf/login/util/BackupHelper;->setBackupAccount(Landroid/content/Context;Landroid/accounts/Account;)Z

    .line 205
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->isFirstAccount()Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->isPrimaryUser()Z

    move-result v4

    invoke-static {p0, v0, v3, v4, v1}, Lcom/google/android/gsf/login/SyncIntroActivity;->setupDefaultSyncAndBackup(Landroid/content/Context;Landroid/accounts/Account;ZZZ)V

    .line 211
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/google/android/gsf/login/LoginActivity;->onSyncIntroActivityFinished(Landroid/content/Intent;)V

    .line 222
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "performBackup":Z
    :goto_1
    return-void

    .line 199
    .restart local v0    # "account":Landroid/accounts/Account;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 219
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_3
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gsf/login/SyncIntroActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 220
    .local v2, "syncIntroIntent":Landroid/content/Intent;
    const/16 v3, 0x3ed

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method private onPostAccountSignIn()V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->isCarrierSetupAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mCarrierSetupLaunched:Z

    if-nez v0, :cond_0

    .line 174
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->runCarrierSetup()V

    .line 182
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mSetupWizard:Z

    if-nez v0, :cond_1

    .line 176
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->runGoogleServices()V

    goto :goto_0

    .line 180
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->onGoogleServicesActivityFinished()V

    goto :goto_0
.end method

.method private onSyncIntroActivityFinished(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->signalAccountInstallation()V

    .line 226
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/LoginActivity;->onSetupComplete(Z)V

    .line 227
    if-nez p1, :cond_0

    .line 228
    new-instance p1, Landroid/content/Intent;

    .end local p1    # "intent":Landroid/content/Intent;
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 230
    .restart local p1    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/login/LoginActivity;->bundleResultData(Landroid/content/Intent;)V

    .line 231
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gsf/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 232
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    .line 233
    return-void
.end method

.method private runCarrierSetup()V
    .locals 4

    .prologue
    const/16 v3, 0x414

    .line 251
    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupLaunchIntent:Landroid/content/Intent;

    const-string v1, "disable_back"

    invoke-direct {p0, v3}, Lcom/google/android/gsf/login/LoginActivity;->shouldDisableBackButton(I)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 253
    iget-object v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupLaunchIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 254
    return-void
.end method

.method private runGoogleServices()V
    .locals 4

    .prologue
    const/16 v3, 0x411

    .line 257
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->getGoogleServicesActivityIntent()Landroid/content/Intent;

    move-result-object v0

    .line 258
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "noBack"

    invoke-direct {p0, v3}, Lcom/google/android/gsf/login/LoginActivity;->shouldDisableBackButton(I)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 259
    invoke-virtual {p0, v0, v3}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 260
    return-void
.end method

.method private shouldDisableBackButton(I)Z
    .locals 3
    .param p1, "activityRequestId"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 129
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 141
    :cond_0
    :goto_0
    :pswitch_1
    return v0

    .line 138
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->isCarrierSetupAllowed()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x411
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getStyleForTheme(Ljava/lang/String;)I
    .locals 1
    .param p1, "themeName"    # Ljava/lang/String;

    .prologue
    .line 94
    const v0, 0x7f060014

    return v0
.end method

.method protected handleBack(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 458
    iget-boolean v0, p0, Lcom/google/android/gsf/login/LoginActivity;->mAddAccount:Z

    if-nez v0, :cond_0

    .line 460
    invoke-virtual {p0, p2, p3}, Lcom/google/android/gsf/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 461
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    .line 515
    :goto_0
    return-void

    .line 465
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 511
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/LoginActivity;->setResult(I)V

    .line 512
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    goto :goto_0

    .line 470
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/UsernamePasswordActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x402

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 477
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/LoginActivity;->launchSyncIntroActivity(Z)V

    goto :goto_0

    .line 481
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/PlusFaqActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3ff

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 487
    :sswitch_3
    invoke-virtual {p0, p2, p3}, Lcom/google/android/gsf/login/LoginActivity;->finishGoogleServicesActivity(ILandroid/content/Intent;)V

    .line 494
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->isCarrierSetupAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 495
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->runCarrierSetup()V

    goto :goto_0

    .line 499
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->runGoogleServices()V

    goto :goto_0

    .line 506
    :sswitch_4
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->runCarrierSetup()V

    goto :goto_0

    .line 465
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_0
        0x3ec -> :sswitch_0
        0x3fc -> :sswitch_1
        0x405 -> :sswitch_2
        0x411 -> :sswitch_3
        0x414 -> :sswitch_4
    .end sparse-switch
.end method

.method public handleStatus(Lcom/google/android/gms/auth/firstparty/shared/Status;)V
    .locals 3
    .param p1, "status"    # Lcom/google/android/gms/auth/firstparty/shared/Status;

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/google/android/gsf/login/LoginActivity$1;->$SwitchMap$com$google$android$gms$auth$firstparty$shared$Status:[I

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 166
    :goto_0
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v1, Lcom/google/android/gsf/login/ShowErrorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 167
    .restart local v0    # "intent":Landroid/content/Intent;
    const/16 v1, 0x3f1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 169
    :goto_1
    return-void

    .line 150
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->onPostAccountSignIn()V

    goto :goto_1

    .line 153
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->launchPasswordPromptTask()V

    goto :goto_1

    .line 156
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v1, Lcom/google/android/gsf/login/CaptchaActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    .restart local v0    # "intent":Landroid/content/Intent;
    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 160
    :pswitch_3
    const-string v1, "GLSActivity"

    const-string v2, "LoginActivity#handleStatus: Unexpected NEED_PERMISSION status! Token requests should be routed to Google Play Services."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v7, 0x3ea

    const/4 v6, 0x6

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, -0x1

    .line 274
    if-ne p2, v3, :cond_0

    .line 276
    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/LoginActivity;->setResult(I)V

    .line 277
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    .line 440
    :goto_0
    return-void

    .line 281
    :cond_0
    invoke-static {p3}, Lcom/google/android/gsf/loginservice/StatusHelper;->fromExtra(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/shared/Status;

    move-result-object v1

    .line 282
    .local v1, "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    if-nez p2, :cond_1

    .line 283
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gsf/login/LoginActivity;->handleBack(IILandroid/content/Intent;)V

    goto :goto_0

    .line 287
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 436
    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown activity result req="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/AddAccountActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 289
    :sswitch_0
    if-ne p2, v6, :cond_2

    .line 290
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gsf/login/BrowserActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v3, 0x3ec

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 300
    :cond_2
    :sswitch_1
    if-ne p2, v4, :cond_3

    .line 301
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gsf/login/LoginActivityTask;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2, v7}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 305
    :cond_3
    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/LoginActivity;->setResult(I)V

    .line 306
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    goto :goto_0

    .line 312
    :sswitch_2
    if-ne p2, v4, :cond_4

    .line 315
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->accountAuthenticatorRetryResult()V

    .line 316
    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/LoginActivity;->setResult(I)V

    .line 321
    :goto_1
    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mCallingUID:I

    invoke-static {p0, v2, v3}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->cancelPermissionNotification(Landroid/content/Context;Ljava/lang/String;I)V

    .line 323
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    goto :goto_0

    .line 318
    :cond_4
    invoke-virtual {p0, v5, p3}, Lcom/google/android/gsf/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    .line 328
    :sswitch_3
    iget-boolean v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mConfirmCredentials:Z

    if-eqz v2, :cond_5

    .line 330
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/LoginActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 331
    invoke-virtual {p0, v4, p3}, Lcom/google/android/gsf/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 332
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    goto/16 :goto_0

    .line 336
    :cond_5
    if-ne p2, v4, :cond_7

    .line 337
    iget-boolean v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mAddAccount:Z

    if-nez v2, :cond_6

    .line 339
    iput-boolean v5, p0, Lcom/google/android/gsf/login/LoginActivity;->mCallAuthenticatorResponseOnFinish:Z

    .line 340
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/LoginActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 341
    invoke-virtual {p0, v4, p3}, Lcom/google/android/gsf/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 343
    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-static {p0, v2, v3}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->onIntentDone(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 347
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    goto/16 :goto_0

    .line 351
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->onPostAccountSignIn()V

    goto/16 :goto_0

    .line 353
    :cond_7
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/firstparty/shared/Status;

    if-ne v1, v2, :cond_8

    .line 354
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gsf/login/ShowErrorActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 355
    .local v0, "showError":Landroid/content/Intent;
    const/16 v2, 0x409

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 357
    .end local v0    # "showError":Landroid/content/Intent;
    :cond_8
    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/LoginActivity;->handleStatus(Lcom/google/android/gms/auth/firstparty/shared/Status;)V

    goto/16 :goto_0

    .line 365
    :sswitch_4
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->launchPasswordPromptTask()V

    goto/16 :goto_0

    .line 370
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-static {p0, v2, v3}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->onIntentDone(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 372
    if-ne p2, v6, :cond_9

    .line 373
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gsf/login/BrowserActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v3, 0x3ec

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 376
    :cond_9
    const/4 v2, 0x5

    if-ne p2, v2, :cond_a

    .line 377
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gsf/login/UsernamePasswordActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v3, 0x402

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 380
    :cond_a
    if-ne p2, v4, :cond_b

    .line 382
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gsf/login/LoginActivityTask;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2, v7}, Lcom/google/android/gsf/login/LoginActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 387
    :cond_b
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/LoginActivity;->setResult(I)V

    .line 388
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->finish()V

    goto/16 :goto_0

    .line 393
    :sswitch_6
    packed-switch p2, :pswitch_data_0

    .line 414
    :goto_2
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->runGoogleServices()V

    goto/16 :goto_0

    .line 396
    :pswitch_1
    iput-boolean v3, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupSuccessfullyRan:Z

    .line 397
    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v3, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mCarrierSetupLaunched:Z

    goto :goto_2

    .line 401
    :pswitch_2
    iput-boolean v5, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupSuccessfullyRan:Z

    .line 402
    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v5, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mCarrierSetupLaunched:Z

    goto :goto_2

    .line 410
    :pswitch_3
    iput-boolean v5, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupSuccessfullyRan:Z

    .line 411
    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v3, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mCarrierSetupLaunched:Z

    goto :goto_2

    .line 418
    :sswitch_7
    invoke-virtual {p0, p2, p3}, Lcom/google/android/gsf/login/LoginActivity;->finishGoogleServicesActivity(ILandroid/content/Intent;)V

    .line 419
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->onGoogleServicesActivityFinished()V

    goto/16 :goto_0

    .line 424
    :sswitch_8
    if-eqz p3, :cond_c

    .line 425
    iget-object v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v3, "redeemed_offer_message_html"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    .line 428
    :cond_c
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->onMarketActivityFinished()V

    goto/16 :goto_0

    .line 432
    :sswitch_9
    invoke-direct {p0, p3}, Lcom/google/android/gsf/login/LoginActivity;->onSyncIntroActivityFinished(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 287
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_3
        0x3ec -> :sswitch_1
        0x3ed -> :sswitch_9
        0x3f1 -> :sswitch_5
        0x3fc -> :sswitch_8
        0x402 -> :sswitch_0
        0x403 -> :sswitch_2
        0x409 -> :sswitch_4
        0x411 -> :sswitch_7
        0x414 -> :sswitch_6
    .end sparse-switch

    .line 393
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/AddAccountActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->isFinishing()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 90
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Landroid/view/Window;->clearFlags(I)V

    .line 69
    invoke-direct {p0}, Lcom/google/android/gsf/login/LoginActivity;->createCarrierSetupLaunchIntent()Landroid/content/Intent;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupLaunchIntent:Landroid/content/Intent;

    .line 71
    if-nez p1, :cond_3

    .line 72
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    const-string v11, "com.google"

    invoke-virtual {v10, v11}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 73
    .local v3, "accounts":[Landroid/accounts/Account;
    move-object v4, v3

    .local v4, "arr$":[Landroid/accounts/Account;
    array-length v7, v4

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v7, :cond_1

    aget-object v0, v4, v5

    .line 74
    .local v0, "account":Landroid/accounts/Account;
    iget-object v10, p0, Lcom/google/android/gsf/login/LoginActivity;->mPreExistingAccounts:Ljava/util/Set;

    iget-object v11, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v10, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 77
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/login/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 78
    .local v6, "intent":Landroid/content/Intent;
    sget-object v10, Lcom/google/android/gms/auth/firstparty/shared/Status;->EXTRA_KEY_STATUS:Ljava/lang/String;

    invoke-virtual {v6, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 79
    .local v9, "statusStr":Ljava/lang/String;
    if-nez v9, :cond_2

    sget-object v8, Lcom/google/android/gms/auth/firstparty/shared/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/firstparty/shared/Status;

    .line 82
    .local v8, "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :goto_2
    invoke-virtual {p0, v8}, Lcom/google/android/gsf/login/LoginActivity;->handleStatus(Lcom/google/android/gms/auth/firstparty/shared/Status;)V

    goto :goto_0

    .line 79
    .end local v8    # "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :cond_2
    invoke-static {v6}, Lcom/google/android/gsf/loginservice/StatusHelper;->fromIntent(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/shared/Status;

    move-result-object v8

    goto :goto_2

    .line 84
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v4    # "arr$":[Landroid/accounts/Account;
    .end local v5    # "i$":I
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "len$":I
    .end local v9    # "statusStr":Ljava/lang/String;
    :cond_3
    const-string v10, "preExistingAccounts"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "accountNames":[Ljava/lang/String;
    move-object v4, v2

    .local v4, "arr$":[Ljava/lang/String;
    array-length v7, v4

    .restart local v7    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_3
    if-ge v5, v7, :cond_4

    aget-object v1, v4, v5

    .line 86
    .local v1, "accountName":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/gsf/login/LoginActivity;->mPreExistingAccounts:Ljava/util/Set;

    invoke-interface {v10, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 85
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 88
    .end local v1    # "accountName":Ljava/lang/String;
    :cond_4
    const-string v10, "carrierSetupSuccessfullyRan"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    iput-boolean v10, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupSuccessfullyRan:Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/AddAccountActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/gsf/login/LoginActivity;->mPreExistingAccounts:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    .line 122
    .local v0, "accountNames":[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/gsf/login/LoginActivity;->mPreExistingAccounts:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 123
    const-string v1, "preExistingAccounts"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 124
    const-string v1, "carrierSetupSuccessfullyRan"

    iget-boolean v2, p0, Lcom/google/android/gsf/login/LoginActivity;->mCarrierSetupSuccessfullyRan:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 125
    return-void
.end method
