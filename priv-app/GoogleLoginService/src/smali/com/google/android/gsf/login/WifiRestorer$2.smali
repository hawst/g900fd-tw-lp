.class final Lcom/google/android/gsf/login/WifiRestorer$2;
.super Landroid/app/backup/RestoreObserver;
.source "WifiRestorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/login/WifiRestorer;->applyRestore(Landroid/content/Context;Landroid/app/backup/RestoreSession;Landroid/app/backup/RestoreSet;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$restoreError:[I

.field final synthetic val$restoreLock:Ljava/util/concurrent/Semaphore;

.field final synthetic val$wifiRestored:[Z


# direct methods
.method constructor <init>([Z[ILjava/util/concurrent/Semaphore;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/gsf/login/WifiRestorer$2;->val$wifiRestored:[Z

    iput-object p2, p0, Lcom/google/android/gsf/login/WifiRestorer$2;->val$restoreError:[I

    iput-object p3, p0, Lcom/google/android/gsf/login/WifiRestorer$2;->val$restoreLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0}, Landroid/app/backup/RestoreObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public restoreFinished(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    const/4 v2, 0x0

    .line 216
    iget-object v0, p0, Lcom/google/android/gsf/login/WifiRestorer$2;->val$wifiRestored:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, v2

    .line 217
    iget-object v0, p0, Lcom/google/android/gsf/login/WifiRestorer$2;->val$restoreError:[I

    aput p1, v0, v2

    .line 218
    iget-object v0, p0, Lcom/google/android/gsf/login/WifiRestorer$2;->val$restoreLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Wifi restored with error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/google/android/gsf/login/WifiRestorer;->access$000(Ljava/lang/String;)V

    .line 220
    return-void
.end method
