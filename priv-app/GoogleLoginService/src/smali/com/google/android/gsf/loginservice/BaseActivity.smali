.class public Lcom/google/android/gsf/loginservice/BaseActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;
    }
.end annotation


# static fields
.field public static final EXTRAS_AUTH_TOKEN_TYPE:Ljava/lang/String;

.field protected static final LOCAL_LOGV:Z

.field protected static mTabletLayout:Z

.field public static sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

.field public static testCheckAuthenticatorResult:Ljava/lang/Object;


# instance fields
.field protected mAddAccount:Z

.field protected mCallAuthenticatorResponseOnFinish:Z

.field protected mCaptchaData:[B

.field protected mCaptchaToken:Ljava/lang/String;

.field protected mConfirmCredentials:Z

.field protected mGlsHelper:Lcom/google/android/gsf/loginservice/GLSHelper;

.field protected mHasMultipleUsers:Z

.field protected mService:Ljava/lang/String;

.field protected mSession:Lcom/google/android/gsf/loginservice/GLSSession;

.field protected mSessionId:Ljava/lang/String;

.field private mShowingProgressDialog:Z

.field protected mThemeType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gsf/loginservice/BaseActivity;->LOCAL_LOGV:Z

    .line 76
    sget-object v0, Lcom/google/android/gsf/loginservice/RequestKey;->SERVICE:Lcom/google/android/gsf/loginservice/RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gsf/loginservice/BaseActivity;->EXTRAS_AUTH_TOKEN_TYPE:Ljava/lang/String;

    .line 260
    new-instance v0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    invoke-direct {v0}, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;-><init>()V

    sput-object v0, Lcom/google/android/gsf/loginservice/BaseActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 263
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mGlsHelper:Lcom/google/android/gsf/loginservice/GLSHelper;

    .line 209
    iput-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCallAuthenticatorResponseOnFinish:Z

    .line 215
    iput-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mConfirmCredentials:Z

    .line 217
    iput-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mAddAccount:Z

    .line 263
    return-void
.end method

.method private checkNotification(Landroid/content/Intent;)Z
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 277
    const-string v2, "notificationId"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "nid":Ljava/lang/String;
    if-nez v0, :cond_0

    move v2, v3

    .line 300
    :goto_0
    return v2

    .line 281
    :cond_0
    const-string v2, "session"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 282
    .local v1, "sessionId":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 283
    invoke-static {v1}, Lcom/google/android/gsf/loginservice/GLSSession;->getSessionOrNull(Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSSession;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    .line 284
    iget-object v2, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    if-eqz v2, :cond_1

    move v2, v3

    .line 285
    goto :goto_0

    .line 289
    :cond_1
    const-string v2, "GLSActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Notification without session "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    .line 292
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/loginservice/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    invoke-virtual {v2, v0, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 298
    :goto_1
    invoke-virtual {p0, v3}, Lcom/google/android/gsf/loginservice/BaseActivity;->setResult(I)V

    .line 299
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->finish()V

    move v2, v4

    .line 300
    goto :goto_0

    .line 295
    :cond_2
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/loginservice/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    const/4 v5, 0x2

    invoke-virtual {v2, v0, v5}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private copyParams(Landroid/content/Intent;)V
    .locals 2
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 516
    if-nez p1, :cond_0

    .line 536
    :goto_0
    return-void

    .line 519
    :cond_0
    const-string v0, "authAccount"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 520
    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSessionId:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 523
    const-string v0, "GLSActivity"

    const-string v1, "Start intent without session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    :cond_2
    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 526
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mError:Lcom/google/android/gms/auth/firstparty/shared/Status;

    if-eqz v0, :cond_3

    .line 528
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mError:Lcom/google/android/gms/auth/firstparty/shared/Status;

    invoke-static {v0}, Lcom/google/android/gsf/loginservice/StatusHelper;->get(Lcom/google/android/gms/auth/firstparty/shared/Status;)Lcom/google/android/gsf/loginservice/StatusHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/loginservice/StatusHelper;->toIntent(Landroid/content/Intent;)V

    .line 531
    :cond_3
    sget-object v0, Lcom/google/android/gsf/loginservice/BaseActivity;->EXTRAS_AUTH_TOKEN_TYPE:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mService:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 532
    const-string v0, "confirmCredentials"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mConfirmCredentials:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 533
    const-string v0, "addAccount"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mAddAccount:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 534
    const-string v0, "hasMultipleUsers"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mHasMultipleUsers:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 535
    const-string v0, "theme"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mThemeType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private copyParams(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 502
    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSessionId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 504
    const-string v0, "GLSActivity"

    const-string v1, "Start intent without session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    :cond_0
    const-string v0, "session"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    sget-object v0, Lcom/google/android/gsf/loginservice/BaseActivity;->EXTRAS_AUTH_TOKEN_TYPE:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mService:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const-string v0, "confirmCredentials"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mConfirmCredentials:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 510
    const-string v0, "addAccount"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mAddAccount:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 511
    const-string v0, "hasMultipleUsers"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mHasMultipleUsers:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 512
    const-string v0, "theme"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mThemeType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    return-void
.end method

.method public static initScreen(Landroid/app/Activity;)V
    .locals 2
    .param p0, "ctx"    # Landroid/app/Activity;

    .prologue
    .line 271
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v1, 0xf

    .line 273
    .local v0, "screenSize":I
    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lcom/google/android/gsf/loginservice/BaseActivity;->mTabletLayout:Z

    .line 274
    return-void

    .line 273
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isTabletLayout()Z
    .locals 1

    .prologue
    .line 681
    sget-boolean v0, Lcom/google/android/gsf/loginservice/BaseActivity;->mTabletLayout:Z

    return v0
.end method

.method public static log(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 763
    if-nez p0, :cond_1

    .line 784
    :cond_0
    :goto_0
    return-void

    .line 766
    :cond_1
    const-string v0, "Token=[^&\n;]*"

    const-string v1, "Token=SECRET"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 767
    const-string v0, "LSID=[^&\n;]*"

    const-string v1, "LSID=SECRET"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 768
    const-string v0, "SID=[^&\n;]*"

    const-string v1, "SID=SECRET"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 769
    const-string v0, "auth=[^&\n;]*"

    const-string v1, "auth=SECRET"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 770
    const-string v0, "EncryptedPasswd=[^&\n;]*"

    const-string v1, "EncryptedPasswd=SECRET"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 771
    const-string v0, "Passwd=[^&\n;]*"

    const-string v1, "Passwd=SECRET"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 772
    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 773
    const-string v0, "GLSActivity"

    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private multiProcessHopFix(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "options"    # Landroid/os/Bundle;

    .prologue
    .line 473
    const-string v0, "accountAuthenticatorResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    if-nez v0, :cond_0

    .line 475
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v0, "accountAuthenticatorResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    iput-object v0, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    .line 478
    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    if-nez v0, :cond_1

    .line 479
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v0, "pendingIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    .line 481
    :cond_1
    const-string v0, "hasAccountManagerOptions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    if-nez v0, :cond_2

    .line 483
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-object p1, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    .line 485
    :cond_2
    const-string v0, "showOffer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 486
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v1, "showOffer"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mShowOffer:Z

    .line 488
    :cond_3
    const-string v0, "offerIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mOfferIntent:Landroid/content/Intent;

    if-nez v0, :cond_4

    .line 489
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v0, "offerIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mOfferIntent:Landroid/content/Intent;

    .line 491
    :cond_4
    const-string v0, "offerMessageHtml"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 492
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v1, "offerMessageHtml"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    .line 494
    :cond_5
    return-void
.end method


# virtual methods
.method protected accountAuthenticatorResult(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 596
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSessionId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSessionId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gsf/loginservice/GLSSession;->cleanUp(Ljava/lang/String;)V

    .line 598
    iput-object v2, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSessionId:Ljava/lang/String;

    .line 601
    :cond_0
    if-nez p1, :cond_1

    .line 602
    const/4 v0, 0x4

    const-string v1, "canceled"

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->accountAuthenticatorResult(Landroid/os/Bundle;ILjava/lang/String;)V

    .line 607
    :goto_0
    return-void

    .line 605
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2}, Lcom/google/android/gsf/loginservice/BaseActivity;->accountAuthenticatorResult(Landroid/os/Bundle;ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected accountAuthenticatorResult(Landroid/os/Bundle;ILjava/lang/String;)V
    .locals 5
    .param p1, "result"    # Landroid/os/Bundle;
    .param p2, "errorCode"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x3

    .line 621
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCallAuthenticatorResponseOnFinish:Z

    .line 622
    iget-object v2, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    .line 623
    .local v0, "res":Landroid/accounts/AccountAuthenticatorResponse;
    if-eqz v0, :cond_1

    .line 625
    if-eqz p1, :cond_3

    .line 626
    invoke-virtual {v0, p1}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    .line 627
    const-string v2, "GLSActivity"

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 628
    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AccountAuthenticatorResult: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    :cond_0
    sput-object p1, Lcom/google/android/gsf/loginservice/BaseActivity;->testCheckAuthenticatorResult:Ljava/lang/Object;

    .line 638
    :goto_0
    iget-object v2, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    .line 640
    :cond_1
    iget-object v2, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v2, :cond_2

    .line 641
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 642
    .local v1, "resIntent":Landroid/content/Intent;
    if-eqz p1, :cond_2

    .line 643
    invoke-virtual {v1, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 646
    .end local v1    # "resIntent":Landroid/content/Intent;
    :cond_2
    return-void

    .line 632
    :cond_3
    invoke-virtual {v0, p2, p3}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    .line 633
    const-string v2, "GLSActivity"

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 634
    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AccountAuthenticatorResult: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    :cond_4
    sput-object p3, Lcom/google/android/gsf/loginservice/BaseActivity;->testCheckAuthenticatorResult:Ljava/lang/Object;

    goto :goto_0
.end method

.method protected accountAuthenticatorResultForAdd(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 664
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 665
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const-string v1, "accountType"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/BaseActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 669
    return-void
.end method

.method protected accountAuthenticatorResultForSkip()V
    .locals 3

    .prologue
    .line 675
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 676
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "setupSkipped"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 677
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/BaseActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 678
    return-void
.end method

.method protected accountAuthenticatorRetryResult()V
    .locals 3

    .prologue
    .line 654
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 655
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "retry"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 656
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gsf/loginservice/BaseActivity;->setResult(ILandroid/content/Intent;)V

    .line 657
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 658
    return-void
.end method

.method protected applyTransition(I)V
    .locals 2
    .param p1, "transitionId"    # I

    .prologue
    const/4 v1, 0x0

    .line 831
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 832
    const v0, 0x7f040004

    const v1, 0x7f040005

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->overridePendingTransition(II)V

    .line 841
    :goto_0
    return-void

    .line 833
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 834
    const v0, 0x7f040002

    const v1, 0x7f040003

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 835
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 836
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 839
    :cond_2
    invoke-virtual {p0, v1, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method protected disableImmersiveMode()V
    .locals 3

    .prologue
    .line 809
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 810
    .local v0, "decorView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/google/android/gsf/loginservice/BaseActivity$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gsf/loginservice/BaseActivity$2;-><init>(Lcom/google/android/gsf/loginservice/BaseActivity;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 820
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    and-int/lit16 v1, v1, -0x1603

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 822
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, -0x80000000

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 823
    return-void
.end method

.method protected enableImmersiveMode()V
    .locals 4

    .prologue
    const v3, 0x106000d

    .line 787
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 788
    .local v0, "decorView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/google/android/gsf/loginservice/BaseActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gsf/loginservice/BaseActivity$1;-><init>(Lcom/google/android/gsf/loginservice/BaseActivity;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 798
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    or-int/lit16 v1, v1, 0x1602

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 800
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, -0x80000000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 804
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 805
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/Window;->setNavigationBarColor(I)V

    .line 806
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 702
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 704
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mActivities:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 709
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCallAuthenticatorResponseOnFinish:Z

    if-eqz v0, :cond_2

    .line 712
    const-string v0, "GLSActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 713
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AccountAuthenticatorResult: finish on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/BaseActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 717
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mActivities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 718
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Remaining GLS activities after end: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mActivities:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :cond_2
    return-void
.end method

.method public getCountry()Ljava/lang/String;
    .locals 2

    .prologue
    .line 742
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_country"

    invoke-static {v0, v1}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSession()Lcom/google/android/gsf/loginservice/GLSSession;
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    return-object v0
.end method

.method public getStyleForTheme(Ljava/lang/String;)I
    .locals 1
    .param p1, "themeName"    # Ljava/lang/String;

    .prologue
    .line 432
    const-string v0, "holo_light"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    const v0, 0x7f06000b

    .line 441
    :goto_0
    return v0

    .line 434
    :cond_0
    const-string v0, "holo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 435
    const v0, 0x7f06000a

    goto :goto_0

    .line 436
    :cond_1
    const-string v0, "material_light"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    const v0, 0x7f06000d

    goto :goto_0

    .line 438
    :cond_2
    const-string v0, "material"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 439
    const v0, 0x7f06000c

    goto :goto_0

    .line 441
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNetworkConnection()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 561
    sget-object v1, Lcom/google/android/gsf/loginservice/BaseActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mForceNoNetwork:Z

    if-eqz v1, :cond_0

    .line 566
    :goto_0
    return v2

    .line 564
    :cond_0
    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 566
    .local v0, "ni":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public isESEnabled()Z
    .locals 4

    .prologue
    .line 729
    sget-object v0, Lcom/google/android/gsf/loginservice/BaseActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mGPlus:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 730
    const-string v0, "1"

    sget-object v1, Lcom/google/android/gsf/loginservice/BaseActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mGPlus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 732
    :goto_0
    return v0

    :cond_0
    const-string v0, "1"

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "google_login_gplus"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isXLargeScreen()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 689
    sget-boolean v0, Lcom/google/android/gsf/loginservice/BaseActivity;->mTabletLayout:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 305
    sget-boolean v3, Lcom/google/android/gsf/loginservice/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v3, :cond_0

    const-string v3, "GLSActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreate("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 308
    invoke-static {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->initScreen(Landroid/app/Activity;)V

    .line 309
    new-instance v3, Lcom/google/android/gsf/loginservice/GLSHelper;

    new-instance v4, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;

    invoke-direct {v4, p0}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;-><init>(Landroid/content/Context;)V

    invoke-direct {v3, v4, p0}, Lcom/google/android/gsf/loginservice/GLSHelper;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mGlsHelper:Lcom/google/android/gsf/loginservice/GLSHelper;

    .line 312
    if-eqz p1, :cond_c

    .line 317
    const-string v3, "session"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 318
    .local v2, "sessionId":Ljava/lang/String;
    if-eqz v2, :cond_b

    .line 326
    invoke-static {v2}, Lcom/google/android/gsf/loginservice/GLSSession;->getSessionOrNull(Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSSession;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    .line 327
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    if-nez v3, :cond_1

    .line 328
    invoke-static {p0, v2, p1}, Lcom/google/android/gsf/loginservice/GLSSession;->fromBundle(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gsf/loginservice/GLSSession;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    .line 350
    .end local v2    # "sessionId":Ljava/lang/String;
    :cond_1
    :goto_0
    const-string v3, "isTop"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCallAuthenticatorResponseOnFinish:Z

    .line 353
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mKey:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSessionId:Ljava/lang/String;

    .line 356
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mActivities:Ljava/util/List;

    invoke-interface {v3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 361
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v4, "authAccount"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    .line 365
    :cond_2
    sget-object v3, Lcom/google/android/gsf/loginservice/ResponseKey;->STATUS:Lcom/google/android/gsf/loginservice/ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 366
    .local v0, "err":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mError:Lcom/google/android/gms/auth/firstparty/shared/Status;

    if-nez v3, :cond_3

    if-eqz v0, :cond_3

    .line 367
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    invoke-static {v0}, Lcom/google/android/gsf/loginservice/StatusHelper;->fromWire(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/Status;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mError:Lcom/google/android/gms/auth/firstparty/shared/Status;

    .line 370
    :cond_3
    sget-object v3, Lcom/google/android/gsf/loginservice/BaseActivity;->EXTRAS_AUTH_TOKEN_TYPE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mService:Ljava/lang/String;

    .line 372
    const-string v3, "confirmCredentials"

    invoke-virtual {p1, v3, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mConfirmCredentials:Z

    .line 373
    const-string v3, "addAccount"

    invoke-virtual {p1, v3, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mAddAccount:Z

    .line 374
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v3, :cond_4

    .line 375
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    invoke-virtual {v3}, Landroid/accounts/AccountAuthenticatorResponse;->onRequestContinued()V

    .line 377
    :cond_4
    sget-object v3, Lcom/google/android/gsf/loginservice/ResponseKey;->CAPTCHA_DATA:Lcom/google/android/gsf/loginservice/ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCaptchaData:[B

    .line 378
    sget-object v3, Lcom/google/android/gsf/loginservice/ResponseKey;->CAPTCHA_TOKEN_RES:Lcom/google/android/gsf/loginservice/ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCaptchaToken:Ljava/lang/String;

    .line 379
    const-string v3, "showingProgressDialog"

    invoke-virtual {p1, v3, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mShowingProgressDialog:Z

    .line 380
    const-string v3, "hasMultipleUsers"

    invoke-virtual {p1, v3, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mHasMultipleUsers:Z

    .line 381
    const-string v3, "theme"

    const-string v4, "material_light"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mThemeType:Ljava/lang/String;

    .line 384
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v4, "useImmersiveMode"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mUseImmersiveMode:Z

    .line 387
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v4, "allowSkip"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "allowSkip"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 390
    const-string v3, "GLSActivity"

    const-string v4, "Accepting legacy allowSkip from intent"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v4, "allowSkip"

    const-string v5, "allowSkip"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 395
    :cond_5
    const-string v3, "firstRun"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 396
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v4, "firstRun"

    invoke-virtual {p1, v4, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mSetupWizard:Z

    .line 397
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mSetupWizard:Z

    if-eqz v3, :cond_6

    .line 398
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mAddAccount:Z

    .line 402
    :cond_6
    const-string v3, "accountManagerOptions"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 403
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v4, "accountManagerOptions"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    .line 406
    :cond_7
    const-string v3, "is_new_account"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 407
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v4, "is_new_account"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mIsNewAccount:Z

    .line 411
    :cond_8
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mCallingAppDescription:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    if-nez v3, :cond_9

    .line 412
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 413
    .restart local v2    # "sessionId":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    new-instance v4, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {v4, v5, v6, v2, v2}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v4, v3, Lcom/google/android/gsf/loginservice/GLSSession;->mCallingAppDescription:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    .line 422
    .end local v2    # "sessionId":Ljava/lang/String;
    :cond_9
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 423
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "callerExtras"

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 424
    invoke-virtual {p0, v7, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->setResult(ILandroid/content/Intent;)V

    .line 426
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mThemeType:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/loginservice/BaseActivity;->getStyleForTheme(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/loginservice/BaseActivity;->setTheme(I)V

    .line 427
    .end local v0    # "err":Ljava/lang/String;
    :cond_a
    return-void

    .line 331
    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "sessionId":Ljava/lang/String;
    :cond_b
    invoke-static {p1}, Lcom/google/android/gsf/loginservice/GLSSession;->getOrCreateSession(Landroid/os/Bundle;)Lcom/google/android/gsf/loginservice/GLSSession;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    goto/16 :goto_0

    .line 336
    .end local v2    # "sessionId":Ljava/lang/String;
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 337
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .line 338
    if-nez p1, :cond_d

    .line 339
    new-instance p1, Landroid/os/Bundle;

    .end local p1    # "icicle":Landroid/os/Bundle;
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 341
    .restart local p1    # "icicle":Landroid/os/Bundle;
    :cond_d
    invoke-direct {p0, v1}, Lcom/google/android/gsf/loginservice/BaseActivity;->checkNotification(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 345
    invoke-static {p1}, Lcom/google/android/gsf/loginservice/GLSSession;->getOrCreateSession(Landroid/os/Bundle;)Lcom/google/android/gsf/loginservice/GLSSession;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    .line 346
    invoke-direct {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->multiProcessHopFix(Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 447
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 449
    sget-object v0, Lcom/google/android/gsf/loginservice/ResponseKey;->CAPTCHA_DATA:Lcom/google/android/gsf/loginservice/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCaptchaData:[B

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 450
    sget-object v0, Lcom/google/android/gsf/loginservice/ResponseKey;->CAPTCHA_TOKEN_RES:Lcom/google/android/gsf/loginservice/ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCaptchaToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    const-string v0, "showingProgressDialog"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mShowingProgressDialog:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 452
    const-string v0, "isTop"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mCallAuthenticatorResponseOnFinish:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 453
    const-string v0, "hasMultipleUsers"

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mHasMultipleUsers:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 454
    const-string v0, "theme"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mThemeType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    invoke-direct {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->copyParams(Landroid/os/Bundle;)V

    .line 459
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/loginservice/GLSSession;->toBundle(Landroid/os/Bundle;)V

    .line 460
    return-void
.end method

.method public shouldDisplayLastNameFirst()Z
    .locals 3

    .prologue
    .line 753
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "google_setup:lastnamefirst_countries"

    invoke-static {v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 755
    .local v0, "lastFirstCountries":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 756
    const-string v0, "*ja*ko*hu*zh*"

    .line 758
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method public startSessionActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 543
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/BaseActivity;->applyTransition(I)V

    .line 544
    invoke-direct {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->copyParams(Landroid/content/Intent;)V

    .line 545
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 546
    return-void
.end method

.method public startSessionActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 549
    if-nez p1, :cond_0

    .line 555
    :goto_0
    return-void

    .line 552
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/BaseActivity;->applyTransition(I)V

    .line 553
    invoke-direct {p0, p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->copyParams(Landroid/content/Intent;)V

    .line 554
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gsf/loginservice/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
