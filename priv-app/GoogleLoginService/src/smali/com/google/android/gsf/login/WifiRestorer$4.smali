.class final Lcom/google/android/gsf/login/WifiRestorer$4;
.super Landroid/content/BroadcastReceiver;
.source "WifiRestorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/login/WifiRestorer;->waitForWifi(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$wifiConnected:[Z

.field final synthetic val$wifiLock:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>([ZLjava/util/concurrent/Semaphore;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/google/android/gsf/login/WifiRestorer$4;->val$wifiConnected:[Z

    iput-object p2, p0, Lcom/google/android/gsf/login/WifiRestorer$4;->val$wifiLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 264
    const-string v1, "connected"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 266
    .local v0, "isConnected":Z
    const-string v1, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 268
    iget-object v1, p0, Lcom/google/android/gsf/login/WifiRestorer$4;->val$wifiConnected:[Z

    aput-boolean v5, v1, v4

    .line 269
    iget-object v1, p0, Lcom/google/android/gsf/login/WifiRestorer$4;->val$wifiLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 270
    const-string v1, "WiFi connected!"

    # invokes: Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    invoke-static {v1}, Lcom/google/android/gsf/login/WifiRestorer;->access$000(Ljava/lang/String;)V

    .line 275
    :goto_0
    return-void

    .line 272
    :cond_0
    const-string v1, "Not connected (%s and %b)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    invoke-static {v1}, Lcom/google/android/gsf/login/WifiRestorer;->access$000(Ljava/lang/String;)V

    goto :goto_0
.end method
