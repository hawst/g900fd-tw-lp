.class final Lcom/google/android/gsf/login/WifiRestorer$1;
.super Landroid/app/backup/RestoreObserver;
.source "WifiRestorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/login/WifiRestorer;->fetchRestoreSet(Landroid/app/backup/RestoreSession;)Landroid/app/backup/RestoreSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$fetchSetsLock:Ljava/util/concurrent/Semaphore;

.field final synthetic val$restoreSet:[Landroid/app/backup/RestoreSet;


# direct methods
.method constructor <init>([Landroid/app/backup/RestoreSet;Ljava/util/concurrent/Semaphore;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/gsf/login/WifiRestorer$1;->val$restoreSet:[Landroid/app/backup/RestoreSet;

    iput-object p2, p0, Lcom/google/android/gsf/login/WifiRestorer$1;->val$fetchSetsLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0}, Landroid/app/backup/RestoreObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public restoreSetsAvailable([Landroid/app/backup/RestoreSet;)V
    .locals 4
    .param p1, "results"    # [Landroid/app/backup/RestoreSet;

    .prologue
    const/4 v2, 0x0

    .line 186
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/gsf/login/WifiRestorer$1;->val$restoreSet:[Landroid/app/backup/RestoreSet;

    aget-object v1, p1, v2

    aput-object v1, v0, v2

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Restoring from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/login/WifiRestorer$1;->val$restoreSet:[Landroid/app/backup/RestoreSet;

    aget-object v1, v1, v2

    iget-wide v2, v1, Landroid/app/backup/RestoreSet;->token:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/google/android/gsf/login/WifiRestorer;->access$000(Ljava/lang/String;)V

    .line 192
    :goto_0
    iget-object v0, p0, Lcom/google/android/gsf/login/WifiRestorer$1;->val$fetchSetsLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 193
    return-void

    .line 190
    :cond_0
    const-string v0, "No valid restore sets!"

    # invokes: Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/google/android/gsf/login/WifiRestorer;->access$000(Ljava/lang/String;)V

    goto :goto_0
.end method
