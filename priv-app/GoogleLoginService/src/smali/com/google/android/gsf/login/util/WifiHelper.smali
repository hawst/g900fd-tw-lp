.class public final Lcom/google/android/gsf/login/util/WifiHelper;
.super Ljava/lang/Object;
.source "WifiHelper.java"


# direct methods
.method public static isWifiConnected(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 25
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 27
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_1

    .line 28
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 29
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_1

    .line 30
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-ne v4, v2, :cond_0

    .line 33
    .end local v1    # "networkInfo":Landroid/net/NetworkInfo;
    :goto_0
    return v2

    .restart local v1    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_0
    move v2, v3

    .line 30
    goto :goto_0

    .end local v1    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_1
    move v2, v3

    .line 33
    goto :goto_0
.end method
