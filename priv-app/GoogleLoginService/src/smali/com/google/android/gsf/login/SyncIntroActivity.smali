.class public Lcom/google/android/gsf/login/SyncIntroActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "SyncIntroActivity.java"

# interfaces
.implements Lcom/android/setupwizard/navigationbar/SetupWizardNavBar$NavigationBarListener;
.implements Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;


# instance fields
.field private mListView:Landroid/widget/ListView;

.field private mPerformBackup:Z

.field private mSyncSettingsAdapter:Lcom/google/android/gsf/login/SyncSettingsAdapter;

.field private mSyncSettingsItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSyncSettingsObserver:Lcom/google/android/gsf/login/SyncSettingsObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsItems:Ljava/util/List;

    return-void
.end method

.method public static enableAllSyncAdapters(Landroid/accounts/Account;)V
    .locals 7
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 44
    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v4

    .line 45
    .local v4, "syncAdapters":[Landroid/content/SyncAdapterType;
    move-object v0, v4

    .local v0, "arr$":[Landroid/content/SyncAdapterType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 46
    .local v3, "sa":Landroid/content/SyncAdapterType;
    iget-object v5, v3, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-static {p0, v5, v6}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 45
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    .end local v3    # "sa":Landroid/content/SyncAdapterType;
    :cond_0
    return-void
.end method

.method public static setupDefaultSyncAndBackup(Landroid/content/Context;Landroid/accounts/Account;ZZZ)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "isFirstAccount"    # Z
    .param p3, "isPrimaryUser"    # Z
    .param p4, "performBackup"    # Z

    .prologue
    const/4 v2, 0x1

    .line 57
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "expedited"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 59
    const-string v1, "com.google.android.apps.magazines"

    invoke-static {p1, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 60
    const-string v1, "com.google.android.apps.books"

    invoke-static {p1, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 61
    const-string v1, "com.google.android.videos.sync"

    invoke-static {p1, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 62
    const-string v1, "com.google.android.music.MusicContent"

    invoke-static {p1, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 64
    if-eqz p3, :cond_0

    .line 65
    if-eqz p4, :cond_1

    .line 66
    invoke-static {p0, v2}, Lcom/google/android/gsf/login/util/BackupHelper;->enableBackup(Landroid/content/Context;Z)Z

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    if-eqz p2, :cond_0

    .line 69
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/android/gsf/login/util/BackupHelper;->enableBackup(Landroid/content/Context;Z)Z

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 143
    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->finish()V

    .line 145
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/SyncIntroActivity;->applyTransition(I)V

    .line 146
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 122
    const-string v0, "GLSActivity"

    const-string v1, "Ignore the back key."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 82
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    .line 85
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v8, 0x7f030016

    invoke-virtual {p0, v8}, Lcom/google/android/gsf/login/SyncIntroActivity;->setContentView(I)V

    .line 87
    const v8, 0x7f0d005a

    invoke-virtual {p0, v8}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mListView:Landroid/widget/ListView;

    .line 88
    iget-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v8, v10}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 89
    iget-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mListView:Landroid/widget/ListView;

    const v9, 0x7f030032

    invoke-virtual {v5, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 91
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 92
    .local v2, "authorities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v8, v8, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    if-eqz v8, :cond_0

    .line 93
    iget-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v8, v8, Lcom/google/android/gsf/loginservice/GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v9, "syncAuthorities"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 95
    .local v3, "authoritiesString":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 96
    const-string v8, ","

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "authArray":[Ljava/lang/String;
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v7, v0, v4

    .line 98
    .local v7, "sync":Ljava/lang/String;
    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 103
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "authArray":[Ljava/lang/String;
    .end local v3    # "authoritiesString":Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "sync":Ljava/lang/String;
    :cond_0
    new-instance v8, Lcom/google/android/gsf/login/SyncSettingsAdapter;

    invoke-direct {v8, p0, v2}, Lcom/google/android/gsf/login/SyncSettingsAdapter;-><init>(Landroid/content/Context;Ljava/util/Set;)V

    iput-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsAdapter:Lcom/google/android/gsf/login/SyncSettingsAdapter;

    .line 104
    iget-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mListView:Landroid/widget/ListView;

    iget-object v9, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsAdapter:Lcom/google/android/gsf/login/SyncSettingsAdapter;

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 106
    const v8, 0x7f0d002b

    invoke-virtual {p0, v8}, Lcom/google/android/gsf/login/SyncIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v9, 0x7f07017f

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 108
    new-instance v8, Lcom/google/android/gsf/login/SyncSettingsObserver;

    iget-object v9, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v9, v9, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-direct {v8, p0, v9, p0}, Lcom/google/android/gsf/login/SyncSettingsObserver;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;)V

    iput-object v8, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsObserver:Lcom/google/android/gsf/login/SyncSettingsObserver;

    .line 109
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onDestroy()V

    .line 115
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsAdapter:Lcom/google/android/gsf/login/SyncSettingsAdapter;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsObserver:Lcom/google/android/gsf/login/SyncSettingsObserver;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/SyncSettingsObserver;->destroy()V

    .line 118
    :cond_0
    return-void
.end method

.method public onNavigateBack()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public onNavigateNext()V
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->setupSyncEnableBackup()V

    .line 151
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/SyncIntroActivity;->setResult(I)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->finish()V

    .line 153
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mUseImmersiveMode:Z

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mUseImmersiveMode:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->setUseImmersiveMode(ZZ)V

    .line 158
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getBackButton()Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 159
    return-void
.end method

.method public onSyncSettingsChanged(Lcom/google/android/gsf/login/SyncSettingsObserver;Ljava/util/List;)V
    .locals 2
    .param p1, "observer"    # Lcom/google/android/gsf/login/SyncSettingsObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gsf/login/SyncSettingsObserver;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p2, "syncSettingsItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;>;"
    iput-object p2, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsItems:Ljava/util/List;

    .line 170
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsAdapter:Lcom/google/android/gsf/login/SyncSettingsAdapter;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/SyncSettingsAdapter;->clear()V

    .line 171
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsAdapter:Lcom/google/android/gsf/login/SyncSettingsAdapter;

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/login/SyncSettingsAdapter;->addAll(Ljava/util/Collection;)V

    .line 172
    return-void
.end method

.method public setupSyncEnableBackup()V
    .locals 7

    .prologue
    .line 127
    new-instance v0, Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.google"

    invoke-direct {v0, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .local v0, "account":Landroid/accounts/Account;
    iget-object v4, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;

    .line 131
    .local v3, "item":Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;
    invoke-virtual {v3}, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->getSyncAdapterType()Landroid/content/SyncAdapterType;

    move-result-object v4

    iget-object v1, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    .line 132
    .local v1, "authority":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mSyncSettingsAdapter:Lcom/google/android/gsf/login/SyncSettingsAdapter;

    invoke-virtual {v4, v3}, Lcom/google/android/gsf/login/SyncSettingsAdapter;->isChecked(Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;)Z

    move-result v4

    invoke-static {v0, v1, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    goto :goto_0

    .line 138
    .end local v1    # "authority":Ljava/lang/String;
    .end local v3    # "item":Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->isFirstAccount()Z

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncIntroActivity;->isPrimaryUser()Z

    move-result v5

    iget-boolean v6, p0, Lcom/google/android/gsf/login/SyncIntroActivity;->mPerformBackup:Z

    invoke-static {p0, v0, v4, v5, v6}, Lcom/google/android/gsf/login/SyncIntroActivity;->setupDefaultSyncAndBackup(Landroid/content/Context;Landroid/accounts/Account;ZZZ)V

    .line 139
    return-void
.end method
