.class public Lcom/google/android/gsf/login/PlusQueryActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "PlusQueryActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field mNextButton:Landroid/view/View;

.field private mNoBack:Z

.field mSkipButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mNoBack:Z

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/PlusQueryActivity;->setResult(I)V

    .line 65
    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onBackPressed()V

    .line 67
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mNextButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mUserSelectedGooglePlus:Z

    .line 57
    :cond_0
    :goto_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/PlusQueryActivity;->setResult(I)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/gsf/login/PlusQueryActivity;->finish()V

    .line 59
    return-void

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mSkipButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mUserSelectedGooglePlus:Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/PlusQueryActivity;->setContentView(I)V

    .line 36
    const v0, 0x7f0d0031

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/PlusQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mSkipButton:Landroid/view/View;

    .line 37
    iget-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mSkipButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const v0, 0x7f0d0033

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/PlusQueryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mNextButton:Landroid/view/View;

    .line 39
    iget-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    if-nez p1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v2, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mUserSelectedGooglePlus:Z

    .line 44
    iget-object v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v2, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mShownName:Z

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/PlusQueryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "noBack"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/PlusQueryActivity;->mNoBack:Z

    .line 48
    return-void
.end method
