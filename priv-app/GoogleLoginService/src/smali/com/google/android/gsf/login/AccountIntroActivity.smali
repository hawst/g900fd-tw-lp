.class public Lcom/google/android/gsf/login/AccountIntroActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "AccountIntroActivity.java"


# static fields
.field private static final LOG_PREFIX:Ljava/lang/String;


# instance fields
.field private mCheckedForExternalAccountSetupWorkflow:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/google/android/gsf/login/AccountIntroActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gsf/login/AccountIntroActivity;->LOG_PREFIX:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method

.method private afterAccountIntro(I)V
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    const/4 v1, 0x0

    .line 255
    sparse-switch p1, :sswitch_data_0

    .line 279
    :goto_0
    return-void

    .line 257
    :sswitch_0
    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(I)V

    .line 258
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->finish()V

    goto :goto_0

    .line 262
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->accountAuthenticatorResultForSkip()V

    .line 263
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(I)V

    .line 264
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->finish()V

    goto :goto_0

    .line 268
    :sswitch_2
    const/16 v0, 0x3f7

    iput v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    .line 269
    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mCreatingAccount:Z

    .line 270
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->checkConnectionAndCheckin()V

    goto :goto_0

    .line 274
    :sswitch_3
    const/16 v0, 0x3f6

    iput v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    .line 275
    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mCreatingAccount:Z

    .line 276
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->checkConnectionAndCheckin()V

    goto :goto_0

    .line 255
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7 -> :sswitch_1
        0x8 -> :sswitch_2
        0x9 -> :sswitch_3
    .end sparse-switch
.end method

.method private afterWaitForCheckin()V
    .locals 2

    .prologue
    .line 304
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mIsEduSignin:Z

    if-eqz v1, :cond_0

    .line 305
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/EduLoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 306
    .local v0, "intent":Landroid/content/Intent;
    iget v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 310
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 309
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->executeStandardAccountSetupFlows()V

    goto :goto_0
.end method

.method private afterWifi()V
    .locals 2

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->haveCheckin(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 295
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 296
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x3f0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 301
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 300
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterWaitForCheckin()V

    goto :goto_0
.end method

.method private checkConnectionAndCheckin()V
    .locals 2

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->hasNetworkConnection()Z

    move-result v1

    if-nez v1, :cond_0

    .line 284
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/SetupWirelessActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 285
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x3fd

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 289
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 288
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterWifi()V

    goto :goto_0
.end method

.method private executeStandardAccountSetupFlows()V
    .locals 4

    .prologue
    .line 316
    iget v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    const/16 v3, 0x3f6

    if-ne v2, v3, :cond_0

    .line 317
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/gsf/login/CreateAccountActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 324
    .local v1, "workflowComponent":Landroid/content/ComponentName;
    :goto_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 325
    .local v0, "intent":Landroid/content/Intent;
    iget v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mNextRequest:I

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/AccountIntroActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 326
    return-void

    .line 321
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "workflowComponent":Landroid/content/ComponentName;
    :cond_0
    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mError:Lcom/google/android/gms/auth/firstparty/shared/Status;

    .line 322
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/gsf/login/LoginActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v1    # "workflowComponent":Landroid/content/ComponentName;
    goto :goto_0
.end method

.method private maybeSkipAccountSetup()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 435
    sget-object v1, Lcom/google/android/gsf/login/AccountIntroActivity;->sTestHooks:Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSkipExistingAccountCheck:Z

    if-nez v1, :cond_0

    .line 436
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mSetupWizard:Z

    if-eqz v1, :cond_0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    .line 439
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->setSetupWizardResults(Landroid/content/Intent;)V

    .line 440
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->onSetupComplete(Z)V

    .line 441
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->finish()V

    .line 442
    const/4 v0, 0x1

    .line 445
    :cond_0
    return v0
.end method

.method private populateCallingAppDescription()V
    .locals 10

    .prologue
    .line 224
    move-object v0, p0

    .line 225
    .local v0, "current":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    .line 226
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 227
    .local v3, "pk":Landroid/content/pm/PackageManager;
    invoke-virtual {v0}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v4

    .line 228
    .local v4, "pkgName":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 232
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 235
    :cond_0
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v3, v4, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 236
    .local v2, "info":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 237
    .local v5, "sessionId":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    new-instance v7, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iget-object v8, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v9, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {v7, v8, v9, v5, v5}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v7, v6, Lcom/google/android/gsf/loginservice/GLSSession;->mCallingAppDescription:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    .end local v2    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "sessionId":Ljava/lang/String;
    :goto_0
    return-void

    .line 242
    :catch_0
    move-exception v1

    .line 248
    .local v1, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "GLSActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/google/android/gsf/login/AccountIntroActivity;->LOG_PREFIX:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Unable to get caller ApplicationInfo for: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private setSetupWizardResults(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 450
    if-nez p1, :cond_0

    .line 451
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 457
    .local v0, "resultData":Landroid/content/Intent;
    :goto_0
    const-string v1, "specialNotificationMsgHtml"

    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 458
    const-string v1, "nameCompleted"

    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mNameActivityCompleted:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 459
    const-string v1, "photoCompleted"

    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mPhotoActivityCompleted:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 461
    sget-object v1, Lcom/google/android/gsf/loginservice/JsonKey;->FIRST_NAME:Lcom/google/android/gsf/loginservice/JsonKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/JsonKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v1

    sget-object v3, Lcom/google/android/gsf/loginservice/JsonKey;->FIRST_NAME:Lcom/google/android/gsf/loginservice/JsonKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/JsonKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 463
    sget-object v1, Lcom/google/android/gsf/loginservice/JsonKey;->LAST_NAME:Lcom/google/android/gsf/loginservice/JsonKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/JsonKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v1

    sget-object v3, Lcom/google/android/gsf/loginservice/JsonKey;->LAST_NAME:Lcom/google/android/gsf/loginservice/JsonKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/JsonKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    const-string v1, "mUserData"

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 469
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(ILandroid/content/Intent;)V

    .line 470
    return-void

    .line 453
    .end local v0    # "resultData":Landroid/content/Intent;
    :cond_0
    move-object v0, p1

    .restart local v0    # "resultData":Landroid/content/Intent;
    goto :goto_0
.end method

.method private startAccountIntro()V
    .locals 5

    .prologue
    .line 184
    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mIsEduSignin:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mProvisionedEmail:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSSession;->mProvisionedName:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 186
    :cond_0
    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterAccountIntro(I)V

    .line 214
    :goto_0
    return-void

    .line 196
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "GmsCoreAccountSetupWorkflow"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 200
    .local v1, "isGmsCoreAccountSetupWorkflowEnabled":Z
    if-eqz v1, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mCheckedForExternalAccountSetupWorkflow:Z

    if-nez v2, :cond_3

    .line 202
    const-string v2, "GLSActivity"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 203
    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gsf/login/AccountIntroActivity;->LOG_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Use alternate account setup workflow."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mCheckedForExternalAccountSetupWorkflow:Z

    .line 207
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gsf/login/PrepareAccountSetupActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 208
    .local v0, "altAccountSetupWorkflow":Landroid/content/Intent;
    const/16 v2, 0x413

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 212
    .end local v0    # "altAccountSetupWorkflow":Landroid/content/Intent;
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->startStandardAccountIntro()V

    goto :goto_0
.end method

.method private startStandardAccountIntro()V
    .locals 3

    .prologue
    const/16 v2, 0x408

    .line 329
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mSetupWizard:Z

    if-eqz v1, :cond_0

    .line 330
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 331
    .local v0, "introIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/AccountIntroActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    .line 336
    :goto_0
    return-void

    .line 333
    .end local v0    # "introIntent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/AccountIntroUIActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 334
    .restart local v0    # "introIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gsf/login/AccountIntroActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 340
    sparse-switch p1, :sswitch_data_0

    .line 417
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 420
    :goto_0
    return-void

    .line 343
    :sswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterAccountIntro(I)V

    goto :goto_0

    .line 348
    :sswitch_1
    if-nez p2, :cond_0

    .line 351
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-object v2, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mProvisionedEmail:Ljava/lang/String;

    .line 352
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-object v2, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mProvisionedName:Ljava/lang/String;

    .line 353
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->startAccountIntro()V

    goto :goto_0

    .line 355
    :cond_0
    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AccountIntro: activity result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->isSetupWizard()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 357
    invoke-direct {p0, p3}, Lcom/google/android/gsf/login/AccountIntroActivity;->setSetupWizardResults(Landroid/content/Intent;)V

    .line 361
    :goto_1
    iput-boolean v5, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mCallAuthenticatorResponseOnFinish:Z

    .line 364
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mActivities:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 366
    if-ne p2, v4, :cond_2

    .line 367
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->accountAuthenticatorResultForAdd(Ljava/lang/String;)V

    .line 372
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->finish()V

    goto :goto_0

    .line 359
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(I)V

    goto :goto_1

    .line 370
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->accountAuthenticatorResultForSkip()V

    goto :goto_2

    .line 381
    :sswitch_2
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterWaitForCheckin()V

    goto :goto_0

    .line 385
    :sswitch_3
    if-nez p2, :cond_3

    .line 386
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->startAccountIntro()V

    goto :goto_0

    .line 388
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->afterWifi()V

    goto :goto_0

    .line 392
    :sswitch_4
    if-eq p2, v4, :cond_4

    .line 396
    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->startStandardAccountIntro()V

    goto :goto_0

    .line 407
    :cond_4
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-boolean v5, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mAllowGooglePlus:Z

    .line 408
    iget-object v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v2, "authAccount"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    .line 409
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 411
    .local v0, "postSignInIntent":Landroid/content/Intent;
    sget-object v1, Lcom/google/android/gsf/loginservice/StatusHelper;->SUCCESS:Lcom/google/android/gsf/loginservice/StatusHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/loginservice/StatusHelper;->toIntent(Landroid/content/Intent;)V

    .line 413
    const/16 v1, 0x3f7

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountIntroActivity;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 340
    :sswitch_data_0
    .sparse-switch
        0x3f0 -> :sswitch_2
        0x3f6 -> :sswitch_1
        0x3f7 -> :sswitch_1
        0x3fd -> :sswitch_3
        0x408 -> :sswitch_0
        0x413 -> :sswitch_4
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 424
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroActivity;->setResult(I)V

    .line 425
    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onBackPressed()V

    .line 426
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super/range {p0 .. p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    if-eqz p1, :cond_1

    .line 85
    const-string v14, "useAlternateAccountSetupWorkflow"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mCheckedForExternalAccountSetupWorkflow:Z

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->maybeSkipAccountSetup()Z

    move-result v14

    if-nez v14, :cond_0

    .line 114
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v14, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mCallingAppDescription:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    if-nez v14, :cond_2

    .line 115
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->populateCallingAppDescription()V

    .line 118
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 119
    .local v8, "initIntent":Landroid/content/Intent;
    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 120
    .local v6, "extras":Landroid/os/Bundle;
    if-eqz v6, :cond_5

    .line 121
    const-string v14, "allowed_domains"

    invoke-virtual {v6, v14}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 122
    .local v2, "dm":[Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 123
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    iput-object v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mAllowedDomains:Ljava/util/ArrayList;

    .line 124
    move-object v1, v2

    .local v1, "arr$":[Ljava/lang/String;
    array-length v10, v1

    .local v10, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v10, :cond_3

    aget-object v3, v1, v7

    .line 125
    .local v3, "domain":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-object v14, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mAllowedDomains:Ljava/util/ArrayList;

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 129
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v3    # "domain":Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v10    # "len$":I
    :cond_3
    const-string v14, "suppressLoginTos"

    invoke-virtual {v6, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v13

    .line 130
    .local v13, "suppressTos":Z
    if-eqz v13, :cond_4

    .line 131
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v15, 0x1

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mTermsOfServiceShown:Z

    .line 132
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v15, 0x1

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mAgreedToPlayTos:Z

    .line 133
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v15, 0x1

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mAgreedToChromeTosAndPrivacy:Z

    .line 134
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const/4 v15, 0x1

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mSuppressLoginTos:Z

    .line 137
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v15, "isEduSignin"

    invoke-virtual {v6, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mIsEduSignin:Z

    .line 138
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v15, "suppressCreditCardRequestActivity"

    invoke-virtual {v6, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mSuppressCreditCardRequestActivity:Z

    .line 140
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v15, "suppressGoogleServicesActivity"

    invoke-virtual {v6, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mSuppressGoogleServicesActivity:Z

    .line 142
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v15, "suppressNameCheck"

    invoke-virtual {v6, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mSuppressNameCheck:Z

    .line 143
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v15, "suppressLoginTos"

    invoke-virtual {v6, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mSuppressLoginTos:Z

    .line 144
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v15, "carrierSetupLaunched"

    invoke-virtual {v6, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mCarrierSetupLaunched:Z

    .line 145
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    const-string v15, "wifiScreenShown"

    invoke-virtual {v6, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    iput-boolean v15, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mWifiScreenShown:Z

    .line 150
    .end local v2    # "dm":[Ljava/lang/String;
    .end local v13    # "suppressTos":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v14, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mSetupWizard:Z

    if-eqz v14, :cond_6

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "google_setup:provisioned_info"

    invoke-static {v14, v15}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 153
    .local v12, "provisionedInfo":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_7

    .line 155
    :try_start_0
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 157
    .local v9, "jsonObj":Lorg/json/JSONObject;
    const-string v14, "purchaser_gaia_email"

    invoke-virtual {v9, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 158
    .local v5, "email":Ljava/lang/String;
    const-string v14, "purchaser_name"

    invoke-virtual {v9, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 159
    .local v11, "name":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 160
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-object v5, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mProvisionedEmail:Ljava/lang/String;

    .line 161
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-object v11, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mProvisionedName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    .end local v5    # "email":Ljava/lang/String;
    .end local v9    # "jsonObj":Lorg/json/JSONObject;
    .end local v11    # "name":Ljava/lang/String;
    .end local v12    # "provisionedInfo":Ljava/lang/String;
    :cond_6
    :goto_2
    const-string v14, "GLSActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Starting account intro "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/login/AccountIntroActivity;->startAccountIntro()V

    goto/16 :goto_0

    .line 163
    .restart local v12    # "provisionedInfo":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 164
    .local v4, "e":Lorg/json/JSONException;
    const-string v14, "GLSActivity"

    const-string v15, "Unable to read provisionedInfo."

    invoke-static {v14, v15, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 166
    .end local v4    # "e":Lorg/json/JSONException;
    :cond_7
    if-eqz v6, :cond_6

    .line 168
    const-string v14, "purchaser_gaia_email"

    invoke-virtual {v6, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 169
    .restart local v5    # "email":Ljava/lang/String;
    const-string v14, "purchaser_name"

    invoke-virtual {v6, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 170
    .restart local v11    # "name":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 171
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-object v5, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mProvisionedEmail:Ljava/lang/String;

    .line 172
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gsf/login/AccountIntroActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iput-object v11, v14, Lcom/google/android/gsf/loginservice/GLSSession;->mProvisionedName:Ljava/lang/String;

    goto :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 76
    const-string v0, "useAlternateAccountSetupWorkflow"

    iget-boolean v1, p0, Lcom/google/android/gsf/login/AccountIntroActivity;->mCheckedForExternalAccountSetupWorkflow:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 79
    return-void
.end method
