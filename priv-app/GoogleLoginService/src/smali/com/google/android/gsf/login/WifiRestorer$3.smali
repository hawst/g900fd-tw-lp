.class final Lcom/google/android/gsf/login/WifiRestorer$3;
.super Ljava/lang/Object;
.source "WifiRestorer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/login/WifiRestorer;->applyRestore(Landroid/content/Context;Landroid/app/backup/RestoreSession;Landroid/app/backup/RestoreSet;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$restoreLock:Ljava/util/concurrent/Semaphore;

.field final synthetic val$wifiRestored:[Z


# direct methods
.method constructor <init>(Ljava/util/concurrent/Semaphore;[Z)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/gsf/login/WifiRestorer$3;->val$restoreLock:Ljava/util/concurrent/Semaphore;

    iput-object p2, p0, Lcom/google/android/gsf/login/WifiRestorer$3;->val$wifiRestored:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 232
    const-string v0, "Wi-Fi restore timeout"

    # invokes: Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/google/android/gsf/login/WifiRestorer;->access$000(Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/gsf/login/WifiRestorer$3;->val$restoreLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 234
    iget-object v0, p0, Lcom/google/android/gsf/login/WifiRestorer$3;->val$wifiRestored:[Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    .line 235
    return-void
.end method
