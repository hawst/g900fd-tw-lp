.class public Lcom/google/android/gsf/login/SyncSettingsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SyncSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAuthorities:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsSyncEnabled:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Set;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p2, "authorities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const v0, 0x7f030028

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mIsSyncEnabled:Ljava/util/Map;

    .line 26
    new-instance v0, Lcom/google/android/gsf/login/SyncSettingsAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/SyncSettingsAdapter$1;-><init>(Lcom/google/android/gsf/login/SyncSettingsAdapter;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 36
    iput-object p2, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mAuthorities:Ljava/util/Set;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/SyncSettingsAdapter;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/login/SyncSettingsAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mIsSyncEnabled:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/login/SyncSettingsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;

    .line 50
    .local v2, "item":Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;
    invoke-virtual {v2}, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->getSyncAdapterType()Landroid/content/SyncAdapterType;

    move-result-object v4

    iget-object v0, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    .line 52
    .local v0, "authority":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mIsSyncEnabled:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 53
    iget-object v5, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mIsSyncEnabled:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mAuthorities:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mAuthorities:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :cond_1
    if-nez p2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SyncSettingsAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f030028

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    :goto_1
    check-cast v4, Landroid/view/ViewGroup;

    move-object v3, v4

    check-cast v3, Landroid/view/ViewGroup;

    .line 62
    .local v3, "view":Landroid/view/ViewGroup;
    const v4, 0x7f0d007f

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 63
    .local v1, "checkBox":Landroid/widget/CheckBox;
    iget-object v4, v2, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->label:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 64
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 65
    iget-object v4, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mIsSyncEnabled:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 66
    iget-object v4, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 68
    return-object v3

    .line 53
    .end local v1    # "checkBox":Landroid/widget/CheckBox;
    .end local v3    # "view":Landroid/view/ViewGroup;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    move-object v4, p2

    .line 58
    goto :goto_1
.end method

.method public isChecked(Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;)Z
    .locals 3
    .param p1, "item"    # Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;

    .prologue
    .line 40
    invoke-virtual {p1}, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->getSyncAdapterType()Landroid/content/SyncAdapterType;

    move-result-object v2

    iget-object v0, v2, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    .line 41
    .local v0, "authority":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mIsSyncEnabled:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 42
    .local v1, "result":Ljava/lang/Boolean;
    if-nez v1, :cond_2

    iget-object v2, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mAuthorities:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter;->mAuthorities:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto :goto_0
.end method
