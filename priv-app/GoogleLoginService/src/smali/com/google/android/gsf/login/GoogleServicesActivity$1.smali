.class Lcom/google/android/gsf/login/GoogleServicesActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "GoogleServicesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/login/GoogleServicesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/login/GoogleServicesActivity;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 88
    iget-object v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/GoogleServicesActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    const-string v1, "id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "id":Ljava/lang/String;
    const-string v1, "backup_restore_details"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    const v1, 0x7f07019b

    invoke-static {v1}, Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;->newInstance(I)Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_2
    const-string v1, "location_details"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 99
    const v1, 0x7f07019d

    invoke-static {v1}, Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;->newInstance(I)Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_3
    const-string v1, "usage_reporting_details"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 103
    const v1, 0x7f0701a2

    invoke-static {v1}, Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;->newInstance(I)Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_4
    const-string v1, "google_privacy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 107
    iget-object v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    const-string v2, "de"

    # invokes: Lcom/google/android/gsf/login/GoogleServicesActivity;->isUserInCountry(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->access$000(Lcom/google/android/gsf/login/GoogleServicesActivity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 109
    sget-object v1, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PRIVACY_POLICY_GERMANY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-static {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :cond_5
    sget-object v1, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-static {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 118
    :cond_6
    const-string v1, "tos"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    const-string v2, "de"

    # invokes: Lcom/google/android/gsf/login/GoogleServicesActivity;->isUserInCountry(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->access$000(Lcom/google/android/gsf/login/GoogleServicesActivity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 121
    sget-object v1, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_TERMS_OF_SERVICE_GERMANY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-static {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 126
    :cond_7
    sget-object v1, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-static {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;->this$0:Lcom/google/android/gsf/login/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
