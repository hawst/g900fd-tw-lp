.class public abstract Lcom/google/android/gsf/login/BackgroundTask;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "BackgroundTask.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/BackgroundTask$2;
    }
.end annotation


# instance fields
.field mCancelButton:Landroid/widget/Button;

.field mCancelable:Z

.field protected mHandler:Landroid/os/Handler;

.field protected mStartTime:J

.field protected mTaskThread:Lcom/google/android/gsf/login/CancelableCallbackThread;

.field private mVerboseMessage:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mCancelable:Z

    .line 80
    new-instance v0, Lcom/google/android/gsf/login/BackgroundTask$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/BackgroundTask$1;-><init>(Lcom/google/android/gsf/login/BackgroundTask;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/BackgroundTask;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/login/BackgroundTask;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/gsf/login/BackgroundTask;->onReply(Landroid/os/Message;)V

    return-void
.end method

.method private onReply(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->cancelTaskThread()V

    .line 205
    invoke-static {p1}, Lcom/google/android/gsf/loginservice/StatusHelper;->fromMessage(Landroid/os/Message;)Lcom/google/android/gms/auth/firstparty/shared/Status;

    move-result-object v2

    .line 207
    .local v2, "status":Lcom/google/android/gms/auth/firstparty/shared/Status;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "intent"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 209
    .local v1, "res":Landroid/content/Intent;
    iget-boolean v4, p0, Lcom/google/android/gsf/login/BackgroundTask;->LOCAL_LOGV:Z

    if-eqz v4, :cond_0

    const-string v4, "GLSActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReply() - status = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :cond_0
    sget-object v4, Lcom/google/android/gsf/login/BackgroundTask$2;->$SwitchMap$com$google$android$gms$auth$firstparty$shared$Status:[I

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/Status;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 231
    invoke-virtual {p0, v2, v1}, Lcom/google/android/gsf/login/BackgroundTask;->onError(Lcom/google/android/gms/auth/firstparty/shared/Status;Landroid/content/Intent;)V

    .line 233
    :goto_0
    return-void

    .line 216
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/gsf/login/BackgroundTask;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v4, v4, Lcom/google/android/gsf/loginservice/GLSSession;->mWifiScreenShown:Z

    if-nez v4, :cond_1

    invoke-static {p0}, Lcom/google/android/gsf/login/util/WifiHelper;->isWifiConnected(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 217
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gsf/login/SetupWirelessActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 218
    .local v3, "wifiIntent":Landroid/content/Intent;
    const-string v4, "intent"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 219
    const/16 v4, 0x3fd

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gsf/login/BackgroundTask;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 221
    .end local v3    # "wifiIntent":Landroid/content/Intent;
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {p0, v4, v1}, Lcom/google/android/gsf/login/BackgroundTask;->setResult(ILandroid/content/Intent;)V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->finish()V

    goto :goto_0

    .line 226
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 227
    .local v0, "intent":Landroid/content/Intent;
    const-class v4, Lcom/google/android/gsf/login/CaptchaActivity;

    invoke-virtual {v0, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 228
    const/16 v4, 0x3e9

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gsf/login/BackgroundTask;->startSessionActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 211
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected cancelTaskThread()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mTaskThread:Lcom/google/android/gsf/login/CancelableCallbackThread;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mTaskThread:Lcom/google/android/gsf/login/CancelableCallbackThread;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/CancelableCallbackThread;->cancel()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mTaskThread:Lcom/google/android/gsf/login/CancelableCallbackThread;

    .line 172
    :cond_0
    return-void
.end method

.method protected ensureDelay(J)V
    .locals 9
    .param p1, "minTime"    # J

    .prologue
    .line 118
    iget-wide v6, p0, Lcom/google/android/gsf/login/BackgroundTask;->mStartTime:J

    add-long v2, v6, p1

    .line 119
    .local v2, "endTime":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 122
    .local v4, "t1":J
    const-wide/16 v6, 0x0

    cmp-long v1, p1, v6

    if-lez v1, :cond_0

    cmp-long v1, v4, v2

    if-gez v1, :cond_0

    .line 124
    sub-long v6, v2, v4

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    .line 133
    const/16 v0, 0x3f1

    if-ne p1, v0, :cond_0

    .line 134
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/BackgroundTask;->setResult(I)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->finish()V

    .line 165
    :goto_0
    return-void

    .line 136
    :cond_0
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_5

    .line 137
    if-ne p2, v1, :cond_2

    .line 138
    iget-boolean v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->LOCAL_LOGV:Z

    if-eqz v0, :cond_1

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Captcha answered, retry withthread="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_1
    const v0, 0x320ca

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "with action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 143
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->start()V

    goto :goto_0

    .line 144
    :cond_2
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    .line 145
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->onCancel()V

    goto :goto_0

    .line 148
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->LOCAL_LOGV:Z

    if-eqz v0, :cond_4

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Captcha failed with resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->onCancel()V

    goto :goto_0

    .line 151
    :cond_5
    const/16 v0, 0x3fd

    if-ne p1, v0, :cond_7

    .line 152
    if-eqz p3, :cond_6

    .line 153
    const-string v0, "intent"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p3

    .end local p3    # "intent":Landroid/content/Intent;
    check-cast p3, Landroid/content/Intent;

    .line 160
    .restart local p3    # "intent":Landroid/content/Intent;
    :cond_6
    invoke-virtual {p0, v1, p3}, Lcom/google/android/gsf/login/BackgroundTask;->setResult(ILandroid/content/Intent;)V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->finish()V

    goto/16 :goto_0

    .line 163
    :cond_7
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onCancel()V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BackgroundTask;->setResult(I)V

    .line 180
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->finish()V

    .line 181
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 189
    iget-boolean v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mCancelable:Z

    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->onCancel()V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 98
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mStartTime:J

    .line 99
    const v0, 0x7f030029

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BackgroundTask;->setContentView(I)V

    .line 100
    const v0, 0x7f0d0049

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BackgroundTask;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mCancelButton:Landroid/widget/Button;

    .line 101
    iget-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v0, 0x7f0d0080

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BackgroundTask;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mVerboseMessage:Landroid/widget/TextView;

    .line 103
    if-nez p1, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->start()V

    .line 106
    :cond_0
    return-void
.end method

.method protected onError(Lcom/google/android/gms/auth/firstparty/shared/Status;Landroid/content/Intent;)V
    .locals 1
    .param p1, "status"    # Lcom/google/android/gms/auth/firstparty/shared/Status;
    .param p2, "res"    # Landroid/content/Intent;

    .prologue
    .line 236
    if-nez p2, :cond_0

    .line 237
    invoke-virtual {p0, p1}, Lcom/google/android/gsf/login/BackgroundTask;->createErrorIntent(Lcom/google/android/gms/auth/firstparty/shared/Status;)Landroid/content/Intent;

    move-result-object p2

    .line 239
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gsf/login/BackgroundTask;->setResult(ILandroid/content/Intent;)V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/gsf/login/BackgroundTask;->finish()V

    .line 241
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 110
    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onPause()V

    .line 111
    iget-boolean v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause(), class="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    return-void
.end method

.method protected setMessage(I)V
    .locals 1
    .param p1, "messageId"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mVerboseMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 89
    return-void
.end method

.method protected setMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gsf/login/BackgroundTask;->mVerboseMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method
