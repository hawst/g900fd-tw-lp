.class public final Lcom/google/android/gsf/login/WifiRestorer;
.super Ljava/lang/Object;
.source "WifiRestorer.java"


# direct methods
.method static synthetic access$000(Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-static {p0}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    return-void
.end method

.method private static applyRestore(Landroid/content/Context;Landroid/app/backup/RestoreSession;Landroid/app/backup/RestoreSet;)Z
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "session"    # Landroid/app/backup/RestoreSession;
    .param p2, "restoreSet"    # Landroid/app/backup/RestoreSet;

    .prologue
    .line 209
    const/4 v13, 0x1

    new-array v12, v13, [Z

    const/4 v13, 0x0

    const/4 v14, 0x0

    aput-boolean v14, v12, v13

    .line 211
    .local v12, "wifiRestored":[Z
    new-instance v8, Ljava/util/concurrent/Semaphore;

    const/4 v13, 0x0

    invoke-direct {v8, v13}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 212
    .local v8, "restoreLock":Ljava/util/concurrent/Semaphore;
    const/4 v13, 0x1

    new-array v7, v13, [I

    .line 213
    .local v7, "restoreError":[I
    new-instance v5, Lcom/google/android/gsf/login/WifiRestorer$2;

    invoke-direct {v5, v12, v7, v8}, Lcom/google/android/gsf/login/WifiRestorer$2;-><init>([Z[ILjava/util/concurrent/Semaphore;)V

    .line 223
    .local v5, "observer":Landroid/app/backup/RestoreObserver;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "wifi_bounce_delay_override_ms"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-static {v13, v14, v0, v1}, Landroid/provider/Settings$Global;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 226
    move-object/from16 v0, p2

    iget-wide v14, v0, Landroid/app/backup/RestoreSet;->token:J

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "com.android.providers.settings"

    aput-object v17, v13, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15, v5, v13}, Landroid/app/backup/RestoreSession;->restoreSome(JLandroid/app/backup/RestoreObserver;[Ljava/lang/String;)I

    move-result v3

    .line 228
    .local v3, "error":I
    const-string v13, "Restore kicked off!"

    invoke-static {v13}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    .line 230
    new-instance v6, Lcom/google/android/gsf/login/WifiRestorer$3;

    invoke-direct {v6, v8, v12}, Lcom/google/android/gsf/login/WifiRestorer$3;-><init>(Ljava/util/concurrent/Semaphore;[Z)V

    .line 237
    .local v6, "releaseWifiRunnable":Ljava/lang/Runnable;
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v13

    invoke-direct {v4, v13}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 239
    .local v4, "handler":Landroid/os/Handler;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "google_setup:wifi_restore_timeout_ms"

    const-wide/16 v16, 0x2710

    move-wide/from16 v0, v16

    invoke-static {v13, v14, v0, v1}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    .line 241
    .local v10, "wifiRestoreTimeoutMs":J
    invoke-virtual {v4, v6, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 242
    invoke-virtual {v8}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 249
    .end local v10    # "wifiRestoreTimeoutMs":J
    :goto_0
    const-string v13, "wifi_bounce_delay_override_ms"

    invoke-static {v13}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 251
    .local v9, "wifiBounceDelayUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v13, v9, v14, v15}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 253
    const/4 v13, 0x0

    aget-boolean v13, v12, v13

    if-eqz v13, :cond_0

    if-nez v3, :cond_0

    const/4 v13, 0x0

    aget v13, v7, v13

    if-nez v13, :cond_0

    const/4 v13, 0x1

    :goto_1
    return v13

    .line 243
    .end local v9    # "wifiBounceDelayUri":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 244
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v13, "WiFiRestore"

    const-string v14, "Restore lock failed"

    invoke-static {v13, v14, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v13

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    throw v13

    .line 253
    .restart local v9    # "wifiBounceDelayUri":Landroid/net/Uri;
    :cond_0
    const/4 v13, 0x0

    goto :goto_1
.end method

.method public static attemptWifiRestoreBlocking(Landroid/content/Context;Lcom/google/android/gsf/loginservice/GLSSession;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "session"    # Lcom/google/android/gsf/loginservice/GLSSession;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 94
    iget-boolean v1, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mSetupWizard:Z

    if-nez v1, :cond_1

    .line 95
    const-string v1, "Not in setup wizard"

    invoke-static {v1}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-boolean v1, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mRestoreWifi:Z

    if-eqz v1, :cond_0

    .line 101
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "google_setup:wifi_restore_enabled"

    invoke-static {v1, v2, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 103
    .local v0, "wifiRestoreEnabled":Z
    if-nez v0, :cond_2

    .line 104
    const-string v1, "WiFi restore disabled"

    invoke-static {v1}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_2
    const-string v1, "Beginning wifi restore"

    invoke-static {v1}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    .line 111
    const/4 v1, 0x1

    :try_start_0
    invoke-static {p0, p1, v1}, Lcom/google/android/gsf/login/WifiRestorer;->setBackupEnabled(Landroid/content/Context;Lcom/google/android/gsf/loginservice/GLSSession;Z)V

    .line 112
    invoke-static {p0}, Lcom/google/android/gsf/login/WifiRestorer;->enableWifi(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 113
    invoke-static {p0}, Lcom/google/android/gsf/login/WifiRestorer;->restoreWifi(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :goto_1
    invoke-static {p0, p1, v3}, Lcom/google/android/gsf/login/WifiRestorer;->setBackupEnabled(Landroid/content/Context;Lcom/google/android/gsf/loginservice/GLSSession;Z)V

    goto :goto_0

    .line 115
    :cond_3
    :try_start_1
    const-string v1, "Could not enable wifi"

    invoke-static {v1}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 119
    :catchall_0
    move-exception v1

    invoke-static {p0, p1, v3}, Lcom/google/android/gsf/login/WifiRestorer;->setBackupEnabled(Landroid/content/Context;Lcom/google/android/gsf/loginservice/GLSSession;Z)V

    throw v1
.end method

.method private static enableWifi(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 142
    const-string v1, "wifi"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 143
    .local v0, "wm":Landroid/net/wifi/WifiManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v1

    return v1
.end method

.method private static fetchRestoreSet(Landroid/app/backup/RestoreSession;)Landroid/app/backup/RestoreSet;
    .locals 7
    .param p0, "session"    # Landroid/app/backup/RestoreSession;

    .prologue
    const/4 v6, 0x0

    .line 181
    const/4 v4, 0x1

    new-array v3, v4, [Landroid/app/backup/RestoreSet;

    .line 182
    .local v3, "restoreSet":[Landroid/app/backup/RestoreSet;
    new-instance v1, Ljava/util/concurrent/Semaphore;

    invoke-direct {v1, v6}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 183
    .local v1, "fetchSetsLock":Ljava/util/concurrent/Semaphore;
    new-instance v2, Lcom/google/android/gsf/login/WifiRestorer$1;

    invoke-direct {v2, v3, v1}, Lcom/google/android/gsf/login/WifiRestorer$1;-><init>([Landroid/app/backup/RestoreSet;Ljava/util/concurrent/Semaphore;)V

    .line 196
    .local v2, "observer":Landroid/app/backup/RestoreObserver;
    invoke-virtual {p0, v2}, Landroid/app/backup/RestoreSession;->getAvailableRestoreSets(Landroid/app/backup/RestoreObserver;)I

    .line 198
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_0
    const-string v4, "Finished fetching restore sets"

    invoke-static {v4}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    .line 204
    aget-object v4, v3, v6

    return-object v4

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "WiFiRestore"

    const-string v5, "Fetch lock failed"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static isEnabledByDefault(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "google_setup:wifi_restore_by_default"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 2
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 305
    const-string v0, "WiFiRestore"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WiFiRestore"

    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :cond_0
    return-void
.end method

.method private static restoreWifi(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    const/4 v3, 0x0

    .line 152
    .local v3, "session":Landroid/app/backup/RestoreSession;
    :try_start_0
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    .line 154
    .local v0, "bm":Landroid/app/backup/BackupManager;
    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->beginRestoreSession()Landroid/app/backup/RestoreSession;

    move-result-object v3

    .line 155
    if-eqz v3, :cond_4

    .line 156
    invoke-static {v3}, Lcom/google/android/gsf/login/WifiRestorer;->fetchRestoreSet(Landroid/app/backup/RestoreSession;)Landroid/app/backup/RestoreSet;

    move-result-object v2

    .line 157
    .local v2, "restoreSet":Landroid/app/backup/RestoreSet;
    if-eqz v2, :cond_3

    .line 158
    invoke-static {p0, v3, v2}, Lcom/google/android/gsf/login/WifiRestorer;->applyRestore(Landroid/content/Context;Landroid/app/backup/RestoreSession;Landroid/app/backup/RestoreSet;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 159
    invoke-static {p0}, Lcom/google/android/gsf/login/WifiRestorer;->waitForWifi(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    .end local v2    # "restoreSet":Landroid/app/backup/RestoreSet;
    :goto_0
    if-eqz v3, :cond_0

    .line 172
    :try_start_1
    invoke-virtual {v3}, Landroid/app/backup/RestoreSession;->endRestoreSession()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 178
    :cond_0
    :goto_1
    return-void

    .line 161
    .restart local v2    # "restoreSet":Landroid/app/backup/RestoreSet;
    :cond_1
    :try_start_2
    const-string v4, "Unable to start restore"

    invoke-static {v4}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 170
    .end local v0    # "bm":Landroid/app/backup/BackupManager;
    .end local v2    # "restoreSet":Landroid/app/backup/RestoreSet;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_2

    .line 172
    :try_start_3
    invoke-virtual {v3}, Landroid/app/backup/RestoreSession;->endRestoreSession()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 175
    :cond_2
    :goto_2
    throw v4

    .line 164
    .restart local v0    # "bm":Landroid/app/backup/BackupManager;
    .restart local v2    # "restoreSet":Landroid/app/backup/RestoreSet;
    :cond_3
    :try_start_4
    const-string v4, "No restore set!"

    invoke-static {v4}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    .end local v2    # "restoreSet":Landroid/app/backup/RestoreSet;
    :cond_4
    const-string v4, "No restore session"

    invoke-static {v4}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 173
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "WiFiRestore"

    const-string v5, "Failed to end the restore session!"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 173
    .end local v0    # "bm":Landroid/app/backup/BackupManager;
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 174
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v5, "WiFiRestore"

    const-string v6, "Failed to end the restore session!"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method protected static setBackupEnabled(Landroid/content/Context;Lcom/google/android/gsf/loginservice/GLSSession;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "session"    # Lcom/google/android/gsf/loginservice/GLSSession;
    .param p2, "enable"    # Z

    .prologue
    .line 128
    iget-object v1, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p1, Lcom/google/android/gsf/loginservice/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {p0, v0}, Lcom/google/android/gsf/login/util/BackupHelper;->setBackupAccount(Landroid/content/Context;Landroid/accounts/Account;)Z

    .line 134
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 136
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    return-void
.end method

.method private static waitForWifi(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    .line 257
    const/4 v9, 0x1

    new-array v6, v9, [Z

    aput-boolean v10, v6, v10

    .line 258
    .local v6, "wifiConnected":[Z
    new-instance v7, Ljava/util/concurrent/Semaphore;

    invoke-direct {v7, v10}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 259
    .local v7, "wifiLock":Ljava/util/concurrent/Semaphore;
    new-instance v0, Landroid/content/IntentFilter;

    const-string v9, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-direct {v0, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 261
    .local v0, "WIFI_FILTER":Landroid/content/IntentFilter;
    new-instance v8, Lcom/google/android/gsf/login/WifiRestorer$4;

    invoke-direct {v8, v6, v7}, Lcom/google/android/gsf/login/WifiRestorer$4;-><init>([ZLjava/util/concurrent/Semaphore;)V

    .line 278
    .local v8, "wifiReceiver":Landroid/content/BroadcastReceiver;
    invoke-virtual {p0, v8, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 279
    const-string v9, "Grabbing WiFi lock"

    invoke-static {v9}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    .line 281
    new-instance v3, Lcom/google/android/gsf/login/WifiRestorer$5;

    invoke-direct {v3, v7}, Lcom/google/android/gsf/login/WifiRestorer$5;-><init>(Ljava/util/concurrent/Semaphore;)V

    .line 289
    .local v3, "releaseWifiRunnable":Ljava/lang/Runnable;
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v2, v9}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 291
    .local v2, "handler":Landroid/os/Handler;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "google_setup:wifi_connect_timeout_ms"

    const-wide/16 v12, 0x3a98

    invoke-static {v9, v10, v12, v13}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    .line 293
    .local v4, "wifiConnectTimeoutMs":J
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 294
    invoke-virtual {v7}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 299
    invoke-virtual {p0, v8}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 301
    .end local v4    # "wifiConnectTimeoutMs":J
    :goto_0
    const-string v9, "Wi-Fi lock released"

    invoke-static {v9}, Lcom/google/android/gsf/login/WifiRestorer;->log(Ljava/lang/String;)V

    .line 302
    return-void

    .line 295
    :catch_0
    move-exception v1

    .line 296
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v9, "WiFiRestore"

    const-string v10, "WiFi lock failed"

    invoke-static {v9, v10, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 299
    invoke-virtual {p0, v8}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 298
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 299
    invoke-virtual {p0, v8}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    throw v9
.end method
