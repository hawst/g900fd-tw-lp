.class public Lcom/google/android/gsf/login/SyncSettingsObserver;
.super Ljava/lang/Object;
.source "SyncSettingsObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;,
        Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mOnSyncSettingsChangedListener:Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;

.field private final mStatusChangeListenerHandle:Ljava/lang/Object;

.field private final mSyncStatusObserver:Landroid/content/SyncStatusObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "onSyncSettingsChangedListener"    # Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mContext:Landroid/content/Context;

    .line 72
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mAccount:Landroid/accounts/Account;

    .line 73
    iput-object p3, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mOnSyncSettingsChangedListener:Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;

    .line 75
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mHandler:Landroid/os/Handler;

    .line 77
    new-instance v0, Lcom/google/android/gsf/login/SyncSettingsObserver$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/SyncSettingsObserver$1;-><init>(Lcom/google/android/gsf/login/SyncSettingsObserver;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    .line 96
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mStatusChangeListenerHandle:Ljava/lang/Object;

    .line 100
    invoke-direct {p0}, Lcom/google/android/gsf/login/SyncSettingsObserver;->updateSyncSettingsData()V

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/SyncSettingsObserver;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/login/SyncSettingsObserver;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gsf/login/SyncSettingsObserver;->updateSyncSettingsData()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gsf/login/SyncSettingsObserver;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/login/SyncSettingsObserver;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getLabelFromAuthority(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "authority"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 129
    iget-object v3, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 130
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v0, p1, v6}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v1

    .line 131
    .local v1, "providerInfo":Landroid/content/pm/ProviderInfo;
    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 133
    .local v2, "providerLabel":Ljava/lang/CharSequence;
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    const-string v3, "GLSActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Provider needs a label for authority \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    move-object v2, p1

    .line 137
    :cond_0
    iget-object v3, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mContext:Landroid/content/Context;

    const v4, 0x7f07016f

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 131
    .end local v2    # "providerLabel":Ljava/lang/CharSequence;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private updateSyncSettingsData()V
    .locals 10

    .prologue
    .line 108
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 109
    .local v2, "itemsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;>;"
    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v5

    .line 110
    .local v5, "syncAdapters":[Landroid/content/SyncAdapterType;
    const-string v7, "GLSActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Total sync adapters: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v0, 0x0

    .local v0, "i":I
    array-length v3, v5

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 112
    aget-object v4, v5, v0

    .line 113
    .local v4, "sa":Landroid/content/SyncAdapterType;
    iget-object v7, v4, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mAccount:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 114
    iget-object v7, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mAccount:Landroid/accounts/Account;

    iget-object v8, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v6

    .line 115
    .local v6, "syncState":I
    invoke-virtual {v4}, Landroid/content/SyncAdapterType;->isUserVisible()Z

    move-result v7

    if-eqz v7, :cond_0

    if-lez v6, :cond_0

    .line 116
    iget-object v7, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    new-instance v8, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;

    iget-object v9, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/google/android/gsf/login/SyncSettingsObserver;->getLabelFromAuthority(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v4, v9}, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;-><init>(Landroid/content/SyncAdapterType;Ljava/lang/String;)V

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    .end local v6    # "syncState":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    .end local v4    # "sa":Landroid/content/SyncAdapterType;
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 124
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;>;"
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 125
    iget-object v7, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mOnSyncSettingsChangedListener:Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;

    invoke-interface {v7, p0, v1}, Lcom/google/android/gsf/login/SyncSettingsObserver$OnSyncSettingsChangedListener;->onSyncSettingsChanged(Lcom/google/android/gsf/login/SyncSettingsObserver;Ljava/util/List;)V

    .line 126
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gsf/login/SyncSettingsObserver;->mStatusChangeListenerHandle:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 105
    return-void
.end method
