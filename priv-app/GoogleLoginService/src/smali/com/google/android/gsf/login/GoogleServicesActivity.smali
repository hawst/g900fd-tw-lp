.class public Lcom/google/android/gsf/login/GoogleServicesActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "GoogleServicesActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/android/setupwizard/navigationbar/SetupWizardNavBar$NavigationBarListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/GoogleServicesActivity$LearnMoreDialog;
    }
.end annotation


# static fields
.field private static final FILTER_LINK_SPAN_CLICKED:Landroid/content/IntentFilter;


# instance fields
.field private mAgreeBackup:Z

.field private mAgreePlayEmail:Z

.field private mAgreeRestore:Z

.field private mBackupRestoreCheckBox:Landroid/widget/CheckBox;

.field private mPlayEmailCheckBox:Landroid/widget/CheckBox;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSetupWizardNavBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

.field private mShowBackup:Z

.field private mShowPlayEmail:Z

.field private mShowRestore:Z

.field private mViewScrollHelper:Lcom/google/android/gsf/login/ui/ViewScrollHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.setupwizard.LINK_SPAN_CLICKED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gsf/login/GoogleServicesActivity;->FILTER_LINK_SPAN_CLICKED:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    .line 85
    new-instance v0, Lcom/google/android/gsf/login/GoogleServicesActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/GoogleServicesActivity$1;-><init>(Lcom/google/android/gsf/login/GoogleServicesActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 349
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/GoogleServicesActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gsf/login/GoogleServicesActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/gsf/login/GoogleServicesActivity;->isUserInCountry(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/gsf/login/GoogleServicesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/gsf/login/GoogleServicesActivity;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setResultAndFinish()V

    return-void
.end method

.method private getResultData()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 267
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 268
    .local v0, "data":Landroid/content/Intent;
    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowBackup:Z

    if-eqz v1, :cond_0

    .line 269
    const-string v1, "agreeBackup"

    iget-boolean v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mAgreeBackup:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 271
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowRestore:Z

    if-eqz v1, :cond_1

    .line 272
    const-string v1, "agreeRestore"

    iget-boolean v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mAgreeRestore:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 274
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowPlayEmail:Z

    if-eqz v1, :cond_2

    .line 275
    const-string v1, "agreePlayEmail"

    iget-boolean v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mAgreePlayEmail:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 277
    :cond_2
    return-object v0
.end method

.method private getUserCountry()Ljava/lang/String;
    .locals 3

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_country"

    invoke-static {v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "country":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 314
    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    .line 315
    const-string v1, "GLSActivity"

    const-string v2, "Returning user country using Gservices device_country"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    .end local v0    # "country":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 320
    .restart local v0    # "country":Ljava/lang/String;
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_2

    .line 321
    const-string v1, "GLSActivity"

    const-string v2, "Returning user country using Locale"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private isUserInCountry(Ljava/lang/String;)Z
    .locals 4
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 304
    invoke-direct {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getUserCountry()Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, "userCountry":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    .line 306
    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "User country is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private registerViewScrollHelper()V
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mSetupWizardNavBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mViewScrollHelper:Lcom/google/android/gsf/login/ui/ViewScrollHelper;

    if-nez v0, :cond_0

    .line 339
    new-instance v0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;

    iget-object v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mSetupWizardNavBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    iget-object v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    new-instance v3, Lcom/google/android/gsf/login/GoogleServicesActivity$2;

    invoke-direct {v3, p0}, Lcom/google/android/gsf/login/GoogleServicesActivity$2;-><init>(Lcom/google/android/gsf/login/GoogleServicesActivity;)V

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gsf/login/ui/ViewScrollHelper;-><init>(Landroid/app/Activity;Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;Lcom/google/android/setupwizard/util/BottomScrollView;Lcom/google/android/gsf/login/ui/ViewScrollHelper$OnFinishListener;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mViewScrollHelper:Lcom/google/android/gsf/login/ui/ViewScrollHelper;

    .line 347
    :cond_0
    return-void
.end method

.method private setBackup(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 255
    iput-boolean p1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mAgreeBackup:Z

    .line 256
    return-void
.end method

.method private setPlayEmail(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 263
    iput-boolean p1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mAgreePlayEmail:Z

    .line 264
    return-void
.end method

.method private setRestore(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mAgreeRestore:Z

    .line 260
    return-void
.end method

.method private setResultAndFinish()V
    .locals 2

    .prologue
    .line 327
    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getResultData()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setResult(ILandroid/content/Intent;)V

    .line 328
    invoke-virtual {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->finish()V

    .line 329
    return-void
.end method

.method private shouldHideUi()Z
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowBackup:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowRestore:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowPlayEmail:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 229
    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->finish()V

    .line 231
    invoke-direct {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->shouldHideUi()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->applyTransition(I)V

    .line 234
    :cond_0
    return-void
.end method

.method protected initViews()V
    .locals 10

    .prologue
    const v9, 0x7f0d003f

    const/16 v8, 0x8

    const v7, 0x7f0d003e

    .line 178
    const v4, 0x7f03002d

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setContentView(I)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030004

    const v4, 0x7f0d0082

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v5, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 182
    const v4, 0x7f0d002b

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f07008c

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 184
    const v4, 0x7f0d003d

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 185
    .local v3, "descriptionView":Landroid/widget/TextView;
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 186
    .local v2, "descriptionLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v4, "google_privacy"

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 187
    invoke-static {v3, v2}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/widget/TextView;Ljava/util/Set;)V

    .line 189
    const v4, 0x7f0d0081

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/setupwizard/util/BottomScrollView;

    iput-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    .line 193
    iget-boolean v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowBackup:Z

    if-eqz v4, :cond_1

    .line 194
    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mBackupRestoreCheckBox:Landroid/widget/CheckBox;

    .line 195
    iget-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mBackupRestoreCheckBox:Landroid/widget/CheckBox;

    iget-boolean v5, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mAgreeBackup:Z

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 196
    iget-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mBackupRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 199
    iget-boolean v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowRestore:Z

    if-nez v4, :cond_0

    .line 200
    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 202
    .local v1, "backupRestoreTextView":Landroid/widget/TextView;
    const v4, 0x7f070198

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 205
    .end local v1    # "backupRestoreTextView":Landroid/widget/TextView;
    :cond_0
    iget-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mBackupRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 206
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 207
    .local v0, "backupRestoreLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v4, "backup_restore_details"

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 208
    iget-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mBackupRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-static {v4, v0}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/widget/TextView;Ljava/util/Set;)V

    .line 216
    .end local v0    # "backupRestoreLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowPlayEmail:Z

    if-eqz v4, :cond_2

    .line 217
    invoke-virtual {p0, v9}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    .line 218
    iget-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    iget-boolean v5, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mAgreePlayEmail:Z

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 219
    iget-object v4, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 224
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->registerViewScrollHelper()V

    .line 225
    return-void

    .line 212
    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 221
    :cond_2
    invoke-virtual {p0, v9}, Lcom/google/android/gsf/login/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 238
    const-string v0, "GLSActivity"

    const-string v1, "Ignore the back key."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mBackupRestoreCheckBox:Landroid/widget/CheckBox;

    if-ne v0, p1, :cond_1

    .line 245
    invoke-direct {p0, p2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setBackup(Z)V

    .line 246
    iget-boolean v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowRestore:Z

    if-eqz v0, :cond_0

    .line 247
    invoke-direct {p0, p2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setRestore(Z)V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    if-ne v0, p1, :cond_0

    .line 250
    invoke-direct {p0, p2}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setPlayEmail(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 136
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 139
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "agreeBackup"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowBackup:Z

    .line 140
    const-string v1, "agreeRestore"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowRestore:Z

    .line 141
    const-string v1, "agreePlayEmail"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowPlayEmail:Z

    .line 143
    const-string v1, "agreeBackup"

    iget-boolean v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowBackup:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setBackup(Z)V

    .line 144
    const-string v1, "agreeRestore"

    iget-boolean v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowRestore:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setRestore(Z)V

    .line 145
    const-string v1, "agreePlayEmail"

    iget-boolean v2, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowPlayEmail:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setPlayEmail(Z)V

    .line 148
    invoke-direct {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->shouldHideUi()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    invoke-direct {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->setResultAndFinish()V

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->initViews()V

    goto :goto_0
.end method

.method public onNavigateBack()V
    .locals 0

    .prologue
    .line 292
    return-void
.end method

.method public onNavigateNext()V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mSetupWizardNavBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .line 283
    iget-object v0, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v0, v0, Lcom/google/android/gsf/loginservice/GLSSession;->mUseImmersiveMode:Z

    iget-object v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSSession;

    iget-boolean v1, v1, Lcom/google/android/gsf/loginservice/GLSSession;->mUseImmersiveMode:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->setUseImmersiveMode(ZZ)V

    .line 284
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getBackButton()Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 286
    invoke-direct {p0}, Lcom/google/android/gsf/login/GoogleServicesActivity;->registerViewScrollHelper()V

    .line 287
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 164
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 165
    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onPause()V

    .line 166
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 157
    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onResume()V

    .line 158
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/gsf/login/GoogleServicesActivity;->FILTER_LINK_SPAN_CLICKED:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 160
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 170
    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 172
    const-string v0, "agreeBackup"

    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowBackup:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 173
    const-string v0, "agreeRestore"

    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowRestore:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 174
    const-string v0, "agreePlayEmail"

    iget-boolean v1, p0, Lcom/google/android/gsf/login/GoogleServicesActivity;->mShowPlayEmail:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 175
    return-void
.end method
