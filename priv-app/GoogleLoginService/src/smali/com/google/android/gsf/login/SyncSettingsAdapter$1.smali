.class Lcom/google/android/gsf/login/SyncSettingsAdapter$1;
.super Ljava/lang/Object;
.source "SyncSettingsAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/login/SyncSettingsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/login/SyncSettingsAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/login/SyncSettingsAdapter;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter$1;->this$0:Lcom/google/android/gsf/login/SyncSettingsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 29
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;

    .line 30
    .local v0, "item":Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;
    iget-object v1, p0, Lcom/google/android/gsf/login/SyncSettingsAdapter$1;->this$0:Lcom/google/android/gsf/login/SyncSettingsAdapter;

    # getter for: Lcom/google/android/gsf/login/SyncSettingsAdapter;->mIsSyncEnabled:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/gsf/login/SyncSettingsAdapter;->access$000(Lcom/google/android/gsf/login/SyncSettingsAdapter;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gsf/login/SyncSettingsObserver$SyncSettingsItem;->getSyncAdapterType()Landroid/content/SyncAdapterType;

    move-result-object v2

    iget-object v2, v2, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method
