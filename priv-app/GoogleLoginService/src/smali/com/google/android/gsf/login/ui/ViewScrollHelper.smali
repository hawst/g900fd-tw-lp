.class public Lcom/google/android/gsf/login/ui/ViewScrollHelper;
.super Ljava/lang/Object;
.source "ViewScrollHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/ui/ViewScrollHelper$OnFinishListener;
    }
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mBottomScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

.field private final mNextButton:Landroid/widget/Button;

.field private final mOnFinishListener:Lcom/google/android/gsf/login/ui/ViewScrollHelper$OnFinishListener;

.field private mScrollNeeded:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;Lcom/google/android/setupwizard/util/BottomScrollView;Lcom/google/android/gsf/login/ui/ViewScrollHelper$OnFinishListener;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "setupWizardNavBar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;
    .param p3, "bottomScrollView"    # Lcom/google/android/setupwizard/util/BottomScrollView;
    .param p4, "onFinishListener"    # Lcom/google/android/gsf/login/ui/ViewScrollHelper$OnFinishListener;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mActivity:Landroid/app/Activity;

    .line 44
    iput-object p3, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mBottomScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    .line 45
    invoke-virtual {p2}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mNextButton:Landroid/widget/Button;

    .line 46
    iput-object p4, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mOnFinishListener:Lcom/google/android/gsf/login/ui/ViewScrollHelper$OnFinishListener;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mScrollNeeded:Z

    .line 49
    iget-object v0, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mBottomScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->setBottomScrollListener(Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mScrollNeeded:Z

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mBottomScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/BottomScrollView;->pageScroll(I)Z

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mOnFinishListener:Lcom/google/android/gsf/login/ui/ViewScrollHelper$OnFinishListener;

    invoke-interface {v0}, Lcom/google/android/gsf/login/ui/ViewScrollHelper$OnFinishListener;->onFinish()V

    goto :goto_0
.end method

.method public onRequiresScroll()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 71
    iget-boolean v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mScrollNeeded:Z

    if-nez v1, :cond_1

    .line 72
    iget-object v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0d0041

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "buttonFrame":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mActivity:Landroid/app/Activity;

    const/high16 v2, 0x7f040000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mNextButton:Landroid/widget/Button;

    const v2, 0x7f020067

    invoke-virtual {v1, v3, v3, v2, v3}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 80
    iget-object v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mNextButton:Landroid/widget/Button;

    const v2, 0x7f070034

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 81
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mScrollNeeded:Z

    .line 83
    .end local v0    # "buttonFrame":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public onScrolledToBottom()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 55
    iget-boolean v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mScrollNeeded:Z

    if-eqz v1, :cond_1

    .line 56
    iget-object v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0d0041

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 57
    .local v0, "buttonFrame":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 58
    iget-object v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mActivity:Landroid/app/Activity;

    const v2, 0x7f040001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 62
    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mNextButton:Landroid/widget/Button;

    const v2, 0x7f02006d

    invoke-virtual {v1, v3, v3, v2, v3}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 64
    iget-object v1, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mNextButton:Landroid/widget/Button;

    const v2, 0x7f070024

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 65
    iput-boolean v3, p0, Lcom/google/android/gsf/login/ui/ViewScrollHelper;->mScrollNeeded:Z

    .line 67
    .end local v0    # "buttonFrame":Landroid/view/View;
    :cond_1
    return-void
.end method
