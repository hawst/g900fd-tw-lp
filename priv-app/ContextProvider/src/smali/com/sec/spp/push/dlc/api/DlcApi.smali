.class public Lcom/sec/spp/push/dlc/api/DlcApi;
.super Ljava/lang/Object;
.source "DlcApi.java"


# direct methods
.method public static getResultStr(I)Ljava/lang/String;
    .locals 1
    .param p0, "result"    # I

    .prologue
    .line 25
    const-string v0, ""

    .line 26
    .local v0, "str":Ljava/lang/String;
    sparse-switch p0, :sswitch_data_0

    .line 52
    :goto_0
    return-object v0

    .line 28
    :sswitch_0
    const-string v0, "Success"

    .line 29
    goto :goto_0

    .line 31
    :sswitch_1
    const-string v0, "Invalid parameter"

    .line 32
    goto :goto_0

    .line 34
    :sswitch_2
    const-string v0, "Permission error"

    .line 35
    goto :goto_0

    .line 37
    :sswitch_3
    const-string v0, "Application is blocked"

    .line 38
    goto :goto_0

    .line 40
    :sswitch_4
    const-string v0, "Service is unavailable"

    .line 41
    goto :goto_0

    .line 43
    :sswitch_5
    const-string v0, "Service is not connected"

    .line 44
    goto :goto_0

    .line 46
    :sswitch_6
    const-string v0, "Urgent logging is not allowed"

    .line 47
    goto :goto_0

    .line 49
    :sswitch_7
    const-string v0, "This app is not registered yet"

    goto :goto_0

    .line 26
    :sswitch_data_0
    .sparse-switch
        -0x3e8 -> :sswitch_5
        -0x6 -> :sswitch_7
        -0x5 -> :sswitch_6
        -0x4 -> :sswitch_2
        -0x3 -> :sswitch_3
        -0x2 -> :sswitch_4
        -0x1 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method
