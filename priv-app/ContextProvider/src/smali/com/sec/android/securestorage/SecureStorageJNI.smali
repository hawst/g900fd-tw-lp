.class public final Lcom/sec/android/securestorage/SecureStorageJNI;
.super Ljava/lang/Object;
.source "SecureStorageJNI.java"


# static fields
.field private static secureStorage:Lcom/sec/android/securestorage/SecureStorageJNI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-direct {v0}, Lcom/sec/android/securestorage/SecureStorageJNI;-><init>()V

    sput-object v0, Lcom/sec/android/securestorage/SecureStorageJNI;->secureStorage:Lcom/sec/android/securestorage/SecureStorageJNI;

    .line 152
    const-string v0, "secure_storage_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sec/android/securestorage/SecureStorageJNI;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/securestorage/SecureStorageJNI;->secureStorage:Lcom/sec/android/securestorage/SecureStorageJNI;

    return-object v0
.end method


# virtual methods
.method public native get(Ljava/lang/String;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorageExceptionJNI;
        }
    .end annotation
.end method

.method public native isSupported()Z
.end method

.method public native put(Ljava/lang/String;[B)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/securestorage/SecureStorageExceptionJNI;
        }
    .end annotation
.end method
