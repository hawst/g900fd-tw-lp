.class public Lcom/samsung/android/providers/context/ContextInsideBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ContextInsideBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 33
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "action":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v1, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    .line 36
    .local v1, "logVisible":Z
    if-eqz v1, :cond_0

    .line 37
    const-string v2, "ContextFramework"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ContextSecureBroadcastReceiver : onReceive() : received action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    :cond_0
    const-string v2, "com.samsung.android.providers.context.action.RESTART"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 41
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.android.providers.context.action.RESTART"

    const/4 v4, 0x0

    const-class v5, Lcom/samsung/android/providers/context/ContextService;

    invoke-direct {v2, v3, v4, p1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 45
    :cond_1
    :goto_0
    return-void

    .line 42
    :cond_2
    const-string v2, "com.samsung.android.providers.context.action.LOGVISIBLE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 43
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v3

    if-nez v1, :cond_3

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v3, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
