.class public Lcom/samsung/android/providers/context/server/ServerDBProvider;
.super Landroid/content/ContentProvider;
.source "ServerDBProvider.java"


# instance fields
.field private mOpenHelper:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

.field private mUriMatcher:Landroid/content/UriMatcher;

.field private tableName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mOpenHelper:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    .line 40
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 41
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    .line 52
    const-string v0, "ContextFramework/ServerUploader/ServerDBProvider"

    const-string v1, "Construct ServerDBProvider"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    return-void
.end method

.method private declared-synchronized getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;
    .locals 1

    .prologue
    .line 334
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mOpenHelper:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    if-nez v0, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mOpenHelper:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mOpenHelper:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getUriMatcher()Landroid/content/UriMatcher;
    .locals 4

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    if-nez v0, :cond_0

    .line 297
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 298
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.providers.context.server"

    const-string v2, "always_log"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 299
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.providers.context.server"

    const-string v2, "survey_log"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 300
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.providers.context.server"

    const-string v2, "always_log/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 301
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.providers.context.server"

    const-string v2, "survey_log/#"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getUriType(Landroid/net/Uri;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, -0x1

    .line 307
    if-nez p1, :cond_1

    .line 330
    :cond_0
    :goto_0
    return v1

    .line 311
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "uriString":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getUriMatcher()Landroid/content/UriMatcher;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 330
    const/4 v1, 0x0

    goto :goto_0

    .line 320
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 325
    :pswitch_1
    const/4 v1, 0x2

    goto :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 64
    const/4 v2, 0x0

    .line 66
    .local v2, "result":I
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 76
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 68
    :pswitch_0
    const-string v4, "always_log"

    iput-object v4, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    .line 79
    :goto_0
    monitor-enter p0

    .line 80
    const/4 v0, 0x0

    .line 81
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v4

    if-nez v4, :cond_0

    .line 82
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :goto_1
    return v3

    .line 72
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_1
    const-string v4, "survey_log"

    iput-object v4, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    goto :goto_0

    .line 85
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 86
    if-nez v0, :cond_1

    .line 87
    monitor-exit p0

    goto :goto_1

    .line 97
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 90
    :cond_1
    :try_start_2
    iget-object v3, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    invoke-virtual {v0, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 91
    const-string v3, "ContextFramework/ServerUploader/ServerDBProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete sent log result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 97
    :goto_2
    :try_start_3
    monitor-exit p0

    move v3, v2

    .line 98
    goto :goto_1

    .line 92
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 94
    const-string v3, "ContextFramework/ServerUploader/ServerDBProvider"

    const-string v4, "Error while using the ContextProvider ServerUploaderDB"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string v3, "ContextFramework/ServerUploader/ServerDBProvider"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 66
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 291
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.providers.context.server.always_log"

    .line 288
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.providers.context.server.survey_log"

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x0

    .line 104
    const-wide/16 v2, -0x1

    .line 106
    .local v2, "rowID":J
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getUriType(Landroid/net/Uri;)I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_0

    .line 107
    const-string v5, "ContextFramework"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Called insert() with invalid URI(Not table uri) : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :goto_0
    return-object v4

    .line 112
    :cond_0
    if-nez p2, :cond_1

    .line 113
    const-string v5, "ContextFramework"

    const-string v6, "Value is null"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 118
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getUriMatcher()Landroid/content/UriMatcher;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 128
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 120
    :pswitch_0
    const-string v5, "always_log"

    iput-object v5, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    .line 130
    :goto_1
    const-string v5, "ContextFramework/ServerUploader/ServerDBProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Insert Table name : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    monitor-enter p0

    .line 134
    const/4 v0, 0x0

    .line 136
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v5

    if-nez v5, :cond_2

    .line 137
    monitor-exit p0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 124
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_1
    const-string v5, "survey_log"

    iput-object v5, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    goto :goto_1

    .line 140
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 141
    if-nez v0, :cond_3

    .line 142
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 146
    :cond_3
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 148
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_4

    .line 149
    const-string v4, "ContextFramework/ServerUploader/ServerDBProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Insert Error, row ID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    :cond_4
    :goto_2
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 157
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_0

    .line 151
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 153
    const-string v4, "ContextFramework/ServerUploader/ServerDBProvider"

    const-string v5, "Error while using the ContextProvider ServerUploaderDB"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    const-string v4, "ContextFramework/ServerUploader/ServerDBProvider"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 118
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 57
    const-string v0, "ContextFramework/ServerUploader/ServerDBProvider"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 162
    const-string v3, "ContextFramework/ServerUploader/ServerDBProvider"

    const-string v4, "Query ServerUploader DB"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getUriType(Landroid/net/Uri;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 166
    if-eqz p3, :cond_1

    .line 167
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 175
    :cond_0
    :goto_0
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 176
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v8, 0x0

    .line 178
    .local v8, "cursor":Landroid/database/Cursor;
    iget-object v3, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 188
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "Operation not supported for uri:"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 169
    .end local v0    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 171
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getUriType(Landroid/net/Uri;)I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 172
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Called query() with invalid URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 180
    .restart local v0    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    const-string v3, "always_log"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 191
    :goto_1
    monitor-enter p0

    .line 192
    const/4 v1, 0x0

    .line 194
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v3

    if-nez v3, :cond_3

    .line 195
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    :goto_2
    return-object v2

    .line 184
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_1
    const-string v3, "survey_log"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_1

    .line 198
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 199
    if-nez v1, :cond_4

    .line 200
    monitor-exit p0

    goto :goto_2

    .line 213
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 203
    :cond_4
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    :try_start_2
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    .line 210
    :goto_3
    if-eqz v8, :cond_5

    .line 211
    :try_start_3
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v8, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 213
    :cond_5
    monitor-exit p0

    move-object v2, v8

    .line 214
    goto :goto_2

    .line 204
    :catch_0
    move-exception v9

    .line 205
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 206
    const-string v2, "ContextFramework/ServerUploader/ServerDBProvider"

    const-string v3, "Error while using the ContextProvider ServerUploaderDB"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const-string v2, "ContextFramework/ServerUploader/ServerDBProvider"

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 178
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 220
    const/4 v2, 0x0

    .line 222
    .local v2, "result":I
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getUriType(Landroid/net/Uri;)I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 223
    const-string v4, "ContextFramework"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Called update() with invalid URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :goto_0
    return v3

    .line 227
    :cond_0
    if-nez p2, :cond_1

    .line 228
    const-string v4, "ContextFramework"

    const-string v5, "Value is null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 232
    :cond_1
    if-eqz p3, :cond_2

    .line 233
    const-string v4, "ContextFramework"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update() is NOT supported with select : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 236
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 239
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 249
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 241
    :pswitch_0
    const-string v4, "always_log"

    iput-object v4, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    .line 252
    :goto_1
    monitor-enter p0

    .line 253
    const/4 v0, 0x0

    .line 255
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v4

    if-nez v4, :cond_3

    .line 256
    monitor-exit p0

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 245
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_1
    const-string v4, "survey_log"

    iput-object v4, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    goto :goto_1

    .line 259
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerDBProvider;->getDbHelper()Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 260
    if-nez v0, :cond_4

    .line 261
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 264
    :cond_4
    :try_start_2
    iget-object v3, p0, Lcom/samsung/android/providers/context/server/ServerDBProvider;->tableName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, p2, p3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 265
    const-string v3, "ContextFramework/ServerUploader/ServerDBProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const/4 v3, 0x1

    if-ge v2, v3, :cond_5

    .line 268
    const-string v3, "ContextFramework/ServerUploader/ServerDBProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update Error, result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 276
    :cond_5
    :goto_2
    :try_start_3
    monitor-exit p0

    move v3, v2

    .line 277
    goto/16 :goto_0

    .line 271
    :catch_0
    move-exception v1

    .line 272
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 273
    const-string v3, "ContextFramework/ServerUploader/ServerDBProvider"

    const-string v4, "Error while using the ContextProvider ServerUploaderDB"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string v3, "ContextFramework/ServerUploader/ServerDBProvider"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
