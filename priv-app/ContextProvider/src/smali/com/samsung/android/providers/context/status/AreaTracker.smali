.class public Lcom/samsung/android/providers/context/status/AreaTracker;
.super Ljava/lang/Object;
.source "AreaTracker.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/samsung/android/providers/context/status/AreaTracker;


# instance fields
.field private mIsEnabled:Z

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/samsung/android/providers/context/status/AreaTracker;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/AreaTracker;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/status/AreaTracker;->mInstance:Lcom/samsung/android/providers/context/status/AreaTracker;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mIsEnabled:Z

    .line 57
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/providers/context/status/AreaTracker;
    .locals 2

    .prologue
    .line 60
    const-class v0, Lcom/samsung/android/providers/context/status/AreaTracker;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/samsung/android/providers/context/status/AreaTracker;->mInstance:Lcom/samsung/android/providers/context/status/AreaTracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private processAreaBatching(I[J[D[D[I[I)V
    .locals 18
    .param p1, "logSize"    # I
    .param p2, "timeStampList"    # [J
    .param p3, "latitudeList"    # [D
    .param p4, "longitudeList"    # [D
    .param p5, "durationList"    # [I
    .param p6, "radiusList"    # [I

    .prologue
    .line 118
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-nez p6, :cond_2

    .line 120
    :cond_0
    const-string v2, "ContextFramework.AreaTracker"

    const-string v3, "Area log data is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_1
    :goto_0
    return-void

    .line 124
    :cond_2
    if-eqz p1, :cond_3

    move-object/from16 v0, p2

    array-length v2, v0

    move/from16 v0, p1

    if-ne v0, v2, :cond_3

    move-object/from16 v0, p3

    array-length v2, v0

    move/from16 v0, p1

    if-ne v0, v2, :cond_3

    move-object/from16 v0, p5

    array-length v2, v0

    move/from16 v0, p1

    if-ne v0, v2, :cond_3

    move-object/from16 v0, p6

    array-length v2, v0

    move/from16 v0, p1

    if-eq v0, v2, :cond_4

    .line 126
    :cond_3
    const-string v2, "ContextFramework.AreaTracker"

    const-string v3, "Area log data is corrupted"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 130
    :cond_4
    const-string v2, "ContextFramework.AreaTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Processing area batching data (size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    new-instance v11, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;

    invoke-direct {v11}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;-><init>()V

    .line 133
    .local v11, "lastAreaInfo":Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;
    const-string v2, "move_area"

    sget-object v3, Lcom/samsung/android/providers/context/log/ContextLogContract$MoveArea;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->queryLog(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 137
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_6

    .line 138
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 140
    invoke-interface {v8}, Landroid/database/Cursor;->moveToLast()Z

    .line 141
    const-string v2, "timestamp"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 142
    .local v16, "lastTimeStamp":J
    const-string v2, "duration"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v12, v2

    .line 143
    .local v12, "lastDuration":J
    const-string v2, "_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 146
    .local v14, "lastRecordId":I
    sub-long v2, v16, v12

    invoke-virtual {v11, v2, v3}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setStartTime(J)V

    .line 147
    invoke-virtual {v11, v12, v13}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setDuration(J)V

    .line 148
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/android/providers/context/log/ContextLogContract$MoveArea;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setUpdateUri(Ljava/lang/String;)V

    .line 150
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_5

    .line 151
    const-string v2, "ContextFramework.AreaTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Getting the last MoveArea item from DB is successful (Timestamp = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Duration = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    .end local v12    # "lastDuration":J
    .end local v14    # "lastRecordId":I
    .end local v16    # "lastTimeStamp":J
    :cond_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 163
    :cond_6
    :goto_1
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    move/from16 v0, p1

    if-ge v10, v0, :cond_1

    .line 165
    invoke-virtual {v11}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->getStartTime()J

    move-result-wide v2

    aget-wide v4, p2, v10

    cmp-long v2, v2, v4

    if-nez v2, :cond_8

    .line 168
    aget v2, p5, v10

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v11, v2, v3}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setDuration(J)V

    .line 169
    aget v2, p6, v10

    invoke-virtual {v11, v2}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setRadius(I)V

    .line 170
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setSavable(Z)V

    .line 190
    :goto_3
    add-int/lit8 v2, p1, -0x1

    if-ne v10, v2, :cond_7

    .line 191
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/providers/context/status/AreaTracker;->saveAreaLog(Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;)V

    .line 163
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 157
    .end local v10    # "i":I
    :catch_0
    move-exception v9

    .line 158
    .local v9, "ex":Ljava/lang/NumberFormatException;
    const-string v2, "ContextFramework.AreaTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (Getting the last MoveArea item from DB fails)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 159
    .end local v9    # "ex":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v9

    .line 160
    .local v9, "ex":Ljava/lang/IllegalStateException;
    const-string v2, "ContextFramework.AreaTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (Getting the last MoveArea item from DB fails)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 174
    .end local v9    # "ex":Ljava/lang/IllegalStateException;
    .restart local v10    # "i":I
    :cond_8
    invoke-virtual {v11}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->isSavable()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 175
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/providers/context/status/AreaTracker;->saveAreaLog(Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;)V

    .line 179
    :cond_9
    aget-wide v2, p2, v10

    invoke-virtual {v11, v2, v3}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setStartTime(J)V

    .line 180
    aget v2, p5, v10

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v11, v2, v3}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setDuration(J)V

    .line 181
    aget-wide v2, p4, v10

    invoke-virtual {v11, v2, v3}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setLongitude(D)V

    .line 182
    aget-wide v2, p3, v10

    invoke-virtual {v11, v2, v3}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setLatitude(D)V

    .line 183
    aget v2, p6, v10

    invoke-virtual {v11, v2}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setRadius(I)V

    .line 184
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setUpdateUri(Ljava/lang/String;)V

    .line 185
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->setSavable(Z)V

    goto :goto_3
.end method

.method private saveAreaLog(Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;)V
    .locals 14
    .param p1, "areaInfo"    # Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;

    .prologue
    .line 198
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v9

    .line 199
    .local v9, "timeFormat":Ljava/text/DateFormat;
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 200
    .local v6, "logTimeDate":Ljava/util/Date;
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->getDuration()J

    move-result-wide v2

    .line 201
    .local v2, "duration":J
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->getStartTime()J

    move-result-wide v12

    add-long v4, v12, v2

    .line 202
    .local v4, "logTime":J
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->getRadius()I

    move-result v7

    .line 203
    .local v7, "radius":I
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->getUpdateUri()Ljava/lang/String;

    move-result-object v10

    .line 204
    .local v10, "updateUri":Ljava/lang/String;
    invoke-virtual {v6, v4, v5}, Ljava/util/Date;->setTime(J)V

    .line 206
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 207
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 209
    .local v8, "row":Landroid/content/ContentValues;
    if-nez v10, :cond_1

    .line 211
    const-string v11, "timestamp_utc"

    invoke-virtual {v9, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v11, "timestamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 213
    const-string v11, "time_zone"

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v12

    iget-object v12, v12, Lcom/samsung/android/providers/context/GlobalStatus;->timeZone:Ljava/lang/String;

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v11, "longitude"

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->getLongitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 215
    const-string v11, "latitude"

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->getLatitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 216
    const-string v11, "place_name"

    const-string v12, ""

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v11, "place_category"

    const-string v12, ""

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v11, "duration"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 219
    const-string v11, "radius"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 221
    :try_start_0
    sget-object v11, Lcom/samsung/android/providers/context/log/ContextLogContract$MoveArea;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v11, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 222
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v11

    iget-boolean v11, v11, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v11, :cond_0

    .line 223
    const-string v11, "ContextFramework.AreaTracker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "A new area log is inserted (Duration = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", Radius = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 225
    :catch_0
    move-exception v1

    .line 226
    .local v1, "ex":Ljava/lang/Exception;
    const-string v11, "ContextFramework.AreaTracker"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 230
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_1
    const-string v11, "timestamp_utc"

    invoke-virtual {v9, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const-string v11, "timestamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 232
    const-string v11, "duration"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 233
    const-string v11, "radius"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 235
    :try_start_1
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v0, v11, v8, v12, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 239
    :goto_1
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v11

    iget-boolean v11, v11, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v11, :cond_0

    .line 240
    const-string v11, "ContextFramework.AreaTracker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "The last area log is updated (Duration = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", Radius = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 236
    :catch_1
    move-exception v1

    .line 237
    .restart local v1    # "ex":Ljava/lang/Exception;
    const-string v11, "ContextFramework.AreaTracker"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized disable()V
    .locals 2

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mIsEnabled:Z

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x18

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mIsEnabled:Z

    .line 86
    const-string v0, "ContextFramework.AreaTracker"

    const-string v1, "Area tracking is disabled"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :cond_0
    monitor-exit p0

    return-void

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized enable()V
    .locals 9

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mIsEnabled:Z

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    :try_start_1
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    const-string v1, "scontext"

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/ContextApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 69
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v2, 0x18

    const/16 v3, 0x12c

    const/16 v4, 0x5dc

    const/16 v5, 0x64

    const/16 v6, 0x1f4

    const/4 v7, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;IIIIII)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mIsEnabled:Z

    .line 72
    const-string v1, "ContextFramework.AreaTracker"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enabling area tracking with SContext ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker;->mIsEnabled:Z

    if-eqz v0, :cond_1

    const-string v0, "SUCCESS)"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 72
    :cond_1
    :try_start_2
    const-string v0, "FAIL)"
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 73
    :catch_0
    move-exception v8

    .line 74
    .local v8, "ex":Ljava/lang/NullPointerException;
    :try_start_3
    const-string v0, "ContextFramework.AreaTracker"

    const-string v1, "Getting SContextManager fails"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 64
    .end local v8    # "ex":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 77
    :cond_2
    :try_start_4
    const-string v0, "ContextFramework.AreaTracker"

    const-string v1, "SContext is not supported"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    .line 92
    iget-object v8, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    .line 93
    .local v8, "scontext":Landroid/hardware/scontext/SContext;
    const-string v0, "ContextFramework.AreaTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SContext event is received, type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-virtual {v8}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v0

    const/16 v1, 0x18

    if-ne v0, v1, :cond_0

    .line 97
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getActivityLocationLoggingContext()Landroid/hardware/scontext/SContextActivityLocationLogging;

    move-result-object v7

    .line 99
    .local v7, "loggingContext":Landroid/hardware/scontext/SContextActivityLocationLogging;
    invoke-virtual {v7}, Landroid/hardware/scontext/SContextActivityLocationLogging;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 101
    invoke-virtual {v7}, Landroid/hardware/scontext/SContextActivityLocationLogging;->getLoggingSize()I

    move-result v1

    invoke-virtual {v7}, Landroid/hardware/scontext/SContextActivityLocationLogging;->getTimestamp()[J

    move-result-object v2

    invoke-virtual {v7}, Landroid/hardware/scontext/SContextActivityLocationLogging;->getLatitude()[D

    move-result-object v3

    invoke-virtual {v7}, Landroid/hardware/scontext/SContextActivityLocationLogging;->getLongitude()[D

    move-result-object v4

    invoke-virtual {v7}, Landroid/hardware/scontext/SContextActivityLocationLogging;->getStayingTimeDuration()[I

    move-result-object v5

    invoke-virtual {v7}, Landroid/hardware/scontext/SContextActivityLocationLogging;->getStayingAreaRadius()[I

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/providers/context/status/AreaTracker;->processAreaBatching(I[J[D[D[I[I)V

    .line 112
    .end local v7    # "loggingContext":Landroid/hardware/scontext/SContextActivityLocationLogging;
    :cond_0
    :goto_0
    return-void

    .line 109
    .restart local v7    # "loggingContext":Landroid/hardware/scontext/SContextActivityLocationLogging;
    :cond_1
    const-string v0, "ContextFramework.AreaTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The activity-location logging event (type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/hardware/scontext/SContextActivityLocationLogging;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is ignored"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
