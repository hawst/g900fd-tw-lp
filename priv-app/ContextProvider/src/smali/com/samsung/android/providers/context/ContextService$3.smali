.class Lcom/samsung/android/providers/context/ContextService$3;
.super Landroid/content/BroadcastReceiver;
.source "ContextService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/ContextService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/ContextService;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/ContextService;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/android/providers/context/ContextService$3;->this$0:Lcom/samsung/android/providers/context/ContextService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 120
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 125
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 129
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 131
    const/4 v2, 0x0

    # invokes: Lcom/samsung/android/providers/context/ContextService;->logManageApp(Landroid/content/Context;Ljava/lang/String;I)V
    invoke-static {p1, v1, v2}, Lcom/samsung/android/providers/context/ContextService;->access$200(Landroid/content/Context;Ljava/lang/String;I)V

    .line 139
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/providers/context/ContextService$3;->this$0:Lcom/samsung/android/providers/context/ContextService;

    # getter for: Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;
    invoke-static {v2}, Lcom/samsung/android/providers/context/ContextService;->access$300(Lcom/samsung/android/providers/context/ContextService;)Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 140
    iget-object v2, p0, Lcom/samsung/android/providers/context/ContextService$3;->this$0:Lcom/samsung/android/providers/context/ContextService;

    # getter for: Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;
    invoke-static {v2}, Lcom/samsung/android/providers/context/ContextService;->access$300(Lcom/samsung/android/providers/context/ContextService;)Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->onTrimMemory(I)V

    goto :goto_0

    .line 132
    :cond_3
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 133
    invoke-static {v1}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->finalizeUninstalledAppConsent(Ljava/lang/String;)V

    .line 134
    iget-object v2, p0, Lcom/samsung/android/providers/context/ContextService$3;->this$0:Lcom/samsung/android/providers/context/ContextService;

    # getter for: Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;
    invoke-static {v2}, Lcom/samsung/android/providers/context/ContextService;->access$300(Lcom/samsung/android/providers/context/ContextService;)Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->removePackageFromCache(Ljava/lang/String;)V

    .line 135
    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/providers/context/ContextService;->logManageApp(Landroid/content/Context;Ljava/lang/String;I)V
    invoke-static {p1, v1, v2}, Lcom/samsung/android/providers/context/ContextService;->access$200(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1
.end method
