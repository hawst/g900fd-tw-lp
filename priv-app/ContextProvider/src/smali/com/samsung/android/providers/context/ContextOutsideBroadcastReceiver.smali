.class public Lcom/samsung/android/providers/context/ContextOutsideBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ContextOutsideBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static logMessage(Landroid/content/ContentResolver;Landroid/os/Bundle;)V
    .locals 5
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 282
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_MESSAGE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 283
    const-string v2, "ContextFramework:OutsideBR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Log insertion for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "app_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is denied by privacy access control"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    :goto_0
    return-void

    .line 288
    :cond_0
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 289
    .local v1, "row":Landroid/content/ContentValues;
    const-string v2, "app_id"

    const-string v3, "app_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const-string v2, "contact_address"

    const-string v3, "msg_address"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const-string v2, "uri"

    const-string v3, "custom_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v2, "type"

    const-string v3, "box_type"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 294
    sget-object v2, Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeMessage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 295
    const-string v2, "ContextFramework:OutsideBR"

    const-string v3, "Logging Message Log"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 296
    .end local v1    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 297
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v2, "ContextFramework:OutsideBR"

    const-string v3, "ERROR, wrong url of Message\'s bundle"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    const-string v2, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 299
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 300
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static logNewPicture(Landroid/content/ContentResolver;Landroid/os/Bundle;)V
    .locals 5
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 166
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 167
    const-string v2, "ContextFramework:OutsideBR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Log insertion for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "app_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is denied by privacy access control"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :goto_0
    return-void

    .line 173
    :cond_0
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 175
    .local v1, "row":Landroid/content/ContentValues;
    const-string v2, "app_id"

    const-string v3, "app_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v2, "orientation"

    const-string v3, "orientation"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 177
    const-string v2, "pixel_width"

    const-string v3, "pixel_width"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 178
    const-string v2, "pixel_height"

    const-string v3, "pixel_height"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 179
    const-string v2, "uri"

    const-string v3, "uri"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v2, "type"

    const-string v3, "type"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 181
    const-string v2, "burst_shot_count"

    const-string v3, "burst_maxcount"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 183
    sget-object v2, Lcom/samsung/android/providers/context/log/ContextLogContract$TakePhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 184
    const-string v2, "ContextFramework:OutsideBR"

    const-string v3, "TAKE_PHOTO Log is inserted"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 185
    .end local v1    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 186
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v2, "ContextFramework:OutsideBR"

    const-string v3, "ERROR, wrong url of take_photo\'s bundle when inserting into TakePhoto"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    const-string v2, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 188
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 189
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static logNewPictureFeature(Landroid/content/ContentResolver;Landroid/os/Bundle;)V
    .locals 8
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 194
    const/4 v5, 0x6

    invoke-static {v5}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v5

    if-nez v5, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v5, v6}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 199
    const-string v5, "ContextFramework:OutsideBR"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SurveyLog insertion for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "app_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is denied by privacy access control"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 207
    :cond_2
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 208
    .local v2, "row1":Landroid/content/ContentValues;
    const-string v5, "app_id"

    const-string v6, "app_id"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v5, "feature"

    const-string v6, "0001"

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    sget-object v5, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v5, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 218
    .end local v2    # "row1":Landroid/content/ContentValues;
    :goto_1
    const/4 v1, 0x0

    .line 219
    .local v1, "feature":Ljava/lang/String;
    const-string v5, "type"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 252
    :goto_2
    :pswitch_0
    if-eqz v1, :cond_3

    .line 253
    :try_start_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 254
    .local v3, "row2":Landroid/content/ContentValues;
    const-string v5, "app_id"

    const-string v6, "app_id"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v5, "feature"

    invoke-virtual {v3, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    sget-object v5, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v5, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 265
    .end local v3    # "row2":Landroid/content/ContentValues;
    :cond_3
    :goto_3
    const-string v5, "burst_maxcount"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    .line 268
    :try_start_2
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 269
    .local v4, "row3":Landroid/content/ContentValues;
    const-string v5, "app_id"

    const-string v6, "app_id"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v5, "feature"

    const-string v6, "0005"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    sget-object v5, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_0

    .line 272
    .end local v4    # "row3":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 273
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "ERROR, wrong url of take_photo\'s bundle when inserting into UseAppFeatureSurvey (for burst shot)"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string v5, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 211
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    .end local v1    # "feature":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 212
    .restart local v0    # "ex":Ljava/lang/IllegalArgumentException;
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "ERROR, wrong url of take_photo\'s bundle when inserting into UseAPPSurvey (for basic)"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const-string v5, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 214
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 215
    .local v0, "ex":Ljava/lang/Exception;
    const-string v5, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 221
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v1    # "feature":Ljava/lang/String;
    :pswitch_1
    const-string v1, "0003"

    .line 222
    goto :goto_2

    .line 224
    :pswitch_2
    const-string v1, "0004"

    .line 225
    goto :goto_2

    .line 227
    :pswitch_3
    const-string v1, "0006"

    .line 228
    goto :goto_2

    .line 230
    :pswitch_4
    const-string v1, "0007"

    .line 231
    goto :goto_2

    .line 233
    :pswitch_5
    const-string v1, "0008"

    .line 234
    goto :goto_2

    .line 236
    :pswitch_6
    const-string v1, "0009"

    .line 237
    goto/16 :goto_2

    .line 239
    :pswitch_7
    const-string v1, "0010"

    .line 240
    goto/16 :goto_2

    .line 242
    :pswitch_8
    const-string v1, "0011"

    .line 243
    goto/16 :goto_2

    .line 245
    :pswitch_9
    const-string v1, "0012"

    .line 246
    goto/16 :goto_2

    .line 258
    :catch_3
    move-exception v0

    .line 259
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "ERROR, wrong url of take_photo\'s bundle when inserting into UseAppFeatureSurvey (for each shot mode)"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const-string v5, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 261
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 262
    .local v0, "ex":Ljava/lang/Exception;
    const-string v5, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 275
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_5
    move-exception v0

    .line 276
    .restart local v0    # "ex":Ljava/lang/Exception;
    const-string v5, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_5
        :pswitch_8
    .end packed-switch
.end method

.method private static logRecordVideoFeature(Landroid/content/ContentResolver;Landroid/os/Bundle;)V
    .locals 5
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 144
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v2

    if-nez v2, :cond_0

    .line 163
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 149
    const-string v2, "ContextFramework:OutsideBR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SurveyLog insertion for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "app_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is denied by privacy access control"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 156
    :cond_1
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 157
    .local v1, "row1":Landroid/content/ContentValues;
    const-string v2, "app_id"

    const-string v3, "app_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v2, "feature"

    const-string v3, "0002"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    sget-object v2, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 160
    .end local v1    # "row1":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static logWallPaperChange(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 4
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 305
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v2

    if-nez v2, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 310
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_0

    .line 311
    const-string v2, "ContextFramework:OutsideBR"

    const-string v3, "SurveyLog insertion for changing wallpaper is denied by privacy access control."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 319
    :cond_2
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 320
    .local v1, "row":Landroid/content/ContentValues;
    const-string v2, "app_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string v2, "feature"

    const-string v3, "CHWP"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    sget-object v2, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 323
    .end local v1    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 324
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v2, "ContextFramework:OutsideBR"

    const-string v3, "ERROR when inserting into UseAPPSurvey (wallpaper)"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    const-string v2, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 326
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 327
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "ContextFramework:OutsideBR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static requestLogRecordVideoWatcher(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 116
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    const-string v2, "record_state"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 121
    .local v1, "recordState":I
    if-nez v1, :cond_2

    .line 122
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/samsung/android/providers/context/ContextOutsideBroadcastReceiver;->logRecordVideoFeature(Landroid/content/ContentResolver;Landroid/os/Bundle;)V

    .line 125
    :cond_2
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 126
    const-string v2, "ContextFramework:OutsideBR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Log insertion for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "app_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is denied by privacy access control"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 130
    :cond_3
    if-nez v1, :cond_5

    .line 131
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_4

    .line 132
    const-string v2, "ContextFramework:OutsideBR"

    const-string v3, "received record video start action"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_4
    invoke-static {}, Lcom/samsung/android/providers/context/ContextService;->getVideoRecordingWatcher()Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;->logRecordVideoStart(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 135
    :cond_5
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 136
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_6

    .line 137
    const-string v2, "ContextFramework:OutsideBR"

    const-string v3, "received record video stop action"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_6
    invoke-static {}, Lcom/samsung/android/providers/context/ContextService;->getVideoRecordingWatcher()Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;->logRecordVideoStop(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 44
    .local v2, "cr":Landroid/content/ContentResolver;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "action":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v5

    iget-boolean v4, v5, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    .line 47
    .local v4, "logVisible":Z
    if-eqz v4, :cond_0

    .line 48
    const-string v5, "ContextFramework:OutsideBR"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ContextSecureBroadcastReceiver: onReceive() = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_0
    const-string v5, "com.samsung.geofence.SERVICE_READY"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 53
    if-eqz v4, :cond_1

    .line 54
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "received com.samsung.geofence.SERVICE_READY"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_1
    :goto_0
    return-void

    .line 56
    :cond_2
    const-string v5, "android.hardware.action.NEW_VIDEO"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 57
    if-eqz v4, :cond_3

    .line 58
    const-string v5, "ContextFramework:OutsideBR"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "received "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :cond_3
    invoke-static {p1, p2}, Lcom/samsung/android/providers/context/ContextOutsideBroadcastReceiver;->requestLogRecordVideoWatcher(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 62
    :cond_4
    const-string v5, "com.android.camera.NEW_PICTURE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 63
    if-eqz v4, :cond_5

    .line 64
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "received com.android.camera.NEW_PICTURE action"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 68
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 69
    invoke-static {v2, v1}, Lcom/samsung/android/providers/context/ContextOutsideBroadcastReceiver;->logNewPicture(Landroid/content/ContentResolver;Landroid/os/Bundle;)V

    .line 71
    invoke-static {v2, v1}, Lcom/samsung/android/providers/context/ContextOutsideBroadcastReceiver;->logNewPictureFeature(Landroid/content/ContentResolver;Landroid/os/Bundle;)V

    goto :goto_0

    .line 73
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_6
    const-string v5, "com.android.mms.RECEIVED_MSG"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 74
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 75
    .restart local v1    # "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 76
    invoke-static {v2, v1}, Lcom/samsung/android/providers/context/ContextOutsideBroadcastReceiver;->logMessage(Landroid/content/ContentResolver;Landroid/os/Bundle;)V

    goto :goto_0

    .line 78
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_7
    const-string v5, "com.android.mms.SENT_MSG"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 79
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 80
    .restart local v1    # "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 81
    invoke-static {v2, v1}, Lcom/samsung/android/providers/context/ContextOutsideBroadcastReceiver;->logMessage(Landroid/content/ContentResolver;Landroid/os/Bundle;)V

    goto :goto_0

    .line 83
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_8
    const-string v5, "android.settings.DIAGNOSTIC_INFO_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 84
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 85
    .restart local v1    # "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_9

    .line 86
    const-string v5, "diagnostic_info_changed"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 87
    .local v3, "isSurveyEnabled":I
    const/4 v5, 0x1

    if-ne v3, v5, :cond_a

    .line 88
    if-eqz v4, :cond_9

    .line 89
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "Survey log is enabled on Setting."

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    .end local v3    # "isSurveyEnabled":I
    :cond_9
    :goto_1
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/ContextApplication;->checkSurveyLogSetting()V

    goto/16 :goto_0

    .line 92
    .restart local v3    # "isSurveyEnabled":I
    :cond_a
    if-eqz v4, :cond_9

    .line 93
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "Survey log is disabled on Setting."

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 99
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "isSurveyEnabled":I
    :cond_b
    const-string v5, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 100
    if-eqz v4, :cond_c

    .line 101
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "Setup wizard is completed."

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_c
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/ContextApplication;->checkSurveyLogSetting()V

    goto/16 :goto_0

    .line 105
    :cond_d
    const-string v5, "android.intent.action.WALLPAPER_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 106
    if-eqz v4, :cond_e

    .line 107
    const-string v5, "ContextFramework:OutsideBR"

    const-string v6, "Wallpaper is changed."

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_e
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/ContextApplication;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/samsung/android/providers/context/ContextOutsideBroadcastReceiver;->logWallPaperChange(Landroid/content/ContentResolver;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
