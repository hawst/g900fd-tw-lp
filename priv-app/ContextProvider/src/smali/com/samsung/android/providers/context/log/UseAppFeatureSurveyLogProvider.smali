.class public Lcom/samsung/android/providers/context/log/UseAppFeatureSurveyLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;
.source "UseAppFeatureSurveyLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 33
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "app_id"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string v3, "feature"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/UseAppFeatureSurveyLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 40
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "use_app_feature_survey"

    const/4 v2, 0x0

    sget-object v3, Lcom/samsung/android/providers/context/log/UseAppFeatureSurveyLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 44
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    .line 45
    const/4 v1, 0x3

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordLifeTime:I

    .line 47
    new-instance v1, Lcom/samsung/android/providers/context/log/UseAppFeatureSurveyLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/UseAppFeatureSurveyLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/UseAppFeatureSurveyLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 82
    sget-object v1, Lcom/samsung/android/providers/context/log/UseAppFeatureSurveyLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 83
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/samsung/android/providers/context/log/UseAppFeatureSurveyLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 91
    return-void
.end method
