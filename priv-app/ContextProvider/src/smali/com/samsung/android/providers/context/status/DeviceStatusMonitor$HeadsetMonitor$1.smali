.class Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "DeviceStatusMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;

.field final synthetic val$this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;

    iput-object p2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor$1;->val$this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 270
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 271
    const-string v2, "state"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v1, :cond_0

    iget-object v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->isBtHeadsetOn()Z
    invoke-static {v2}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->access$300(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 272
    .local v0, "plugged":Z
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;

    iget-object v2, v2, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBoolean(IZ)Z
    invoke-static {v2, v1, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->access$400(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)Z

    .line 279
    .end local v0    # "plugged":Z
    :goto_0
    return-void

    .line 277
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->checkCurrentStatus()V
    invoke-static {v1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->access$500(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;)V

    goto :goto_0
.end method
