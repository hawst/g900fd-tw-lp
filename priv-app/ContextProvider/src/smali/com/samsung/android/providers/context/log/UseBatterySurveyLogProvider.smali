.class public Lcom/samsung/android/providers/context/log/UseBatterySurveyLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;
.source "UseBatterySurveyLogProvider.java"


# static fields
.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "use_battery_survey"

    invoke-direct {v0, v1}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    const/4 v1, 0x3

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordLifeTime:I

    .line 41
    new-instance v1, Lcom/samsung/android/providers/context/log/UseBatterySurveyLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/UseBatterySurveyLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/UseBatterySurveyLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 76
    sget-object v1, Lcom/samsung/android/providers/context/log/UseBatterySurveyLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 77
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/samsung/android/providers/context/log/UseBatterySurveyLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 90
    return-void
.end method


# virtual methods
.method protected Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 0
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 81
    return-object p1
.end method
