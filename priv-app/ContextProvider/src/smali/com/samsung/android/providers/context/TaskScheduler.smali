.class public Lcom/samsung/android/providers/context/TaskScheduler;
.super Ljava/lang/Object;
.source "TaskScheduler.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private volatile mIsBatchWorking:Z

.field private final mServerUploader:Lcom/samsung/android/providers/context/server/ServerUploader;

.field private final mSupportsServerUpload:Z

.field private final mSupportsSurveyLog:Z

.field private mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mSupportsSurveyLog:Z

    .line 65
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mSupportsServerUpload:Z

    .line 66
    iput-object p1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mContext:Landroid/content/Context;

    .line 67
    new-instance v0, Lcom/samsung/android/providers/context/server/ServerUploader;

    iget-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/providers/context/server/ServerUploader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mServerUploader:Lcom/samsung/android/providers/context/server/ServerUploader;

    .line 68
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/providers/context/TaskScheduler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/TaskScheduler;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mIsBatchWorking:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/providers/context/TaskScheduler;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/TaskScheduler;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/android/providers/context/TaskScheduler;->processTasks()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/providers/context/TaskScheduler;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/TaskScheduler;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/android/providers/context/TaskScheduler;->scheduleBatchProcessing()V

    return-void
.end method

.method static synthetic access$300()V
    .locals 0

    .prologue
    .line 47
    invoke-static {}, Lcom/samsung/android/providers/context/TaskScheduler;->setTestProcessingDate()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/providers/context/TaskScheduler;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/TaskScheduler;
    .param p1, "x1"    # J

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/providers/context/TaskScheduler;->batchProcessing(J)V

    return-void
.end method

.method static synthetic access$500()V
    .locals 0

    .prologue
    .line 47
    invoke-static {}, Lcom/samsung/android/providers/context/TaskScheduler;->changeSentDate()V

    return-void
.end method

.method private batchProcessing(J)V
    .locals 5
    .param p1, "delay"    # J

    .prologue
    .line 160
    iget-boolean v1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mIsBatchWorking:Z

    if-eqz v1, :cond_0

    .line 161
    const-string v1, "ContextFramework"

    const-string v2, "TaskScheduler: another batch task already processing"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_1

    .line 166
    iget-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 167
    iget-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->purge()I

    move-result v0

    .line 168
    .local v0, "i":I
    if-lez v0, :cond_1

    .line 169
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TaskScheduler: cancel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tasks"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    .end local v0    # "i":I
    :cond_1
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mTimer:Ljava/util/Timer;

    .line 174
    iget-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mTimer:Ljava/util/Timer;

    new-instance v2, Lcom/samsung/android/providers/context/TaskScheduler$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/providers/context/TaskScheduler$1;-><init>(Lcom/samsung/android/providers/context/TaskScheduler;)V

    invoke-virtual {v1, v2, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method private static changeSentDate()V
    .locals 4

    .prologue
    .line 284
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v0

    .line 285
    .local v0, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v1, "DeletionDate"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    .line 286
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/samsung/android/providers/context/TaskScheduler;->updateSentDate(Z)V

    .line 287
    return-void
.end method

.method private static checkTime()J
    .locals 10

    .prologue
    .line 87
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    .line 88
    .local v0, "rand":D
    const-wide v6, 0x415b774000000000L    # 7200000.0

    mul-double v4, v0, v6

    .line 89
    .local v4, "randomRange":D
    const-wide/32 v6, 0x36ee80

    double-to-long v8, v4

    add-long v2, v6, v8

    .line 91
    .local v2, "randomDelay":J
    return-wide v2
.end method

.method private static clearMinorField(Ljava/util/Calendar;)V
    .locals 1
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 95
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 96
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 97
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 98
    return-void
.end method

.method public static clearSurveyLoggingTime()V
    .locals 2

    .prologue
    .line 238
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v0

    .line 239
    .local v0, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v1, "SurveyUpdateTime"

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/ContextPreference;->remove(Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method private static needCollectData()Z
    .locals 6

    .prologue
    .line 145
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v0

    .line 146
    .local v0, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v1, "SentDate"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v4, v5}, Lcom/samsung/android/providers/context/ContextPreference;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 148
    .local v2, "sentDate":J
    invoke-static {}, Lcom/samsung/android/providers/context/server/ServerUploader;->checkSentDate()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static needSurveyLogging()Z
    .locals 12

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x1

    .line 213
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v2

    .line 215
    .local v2, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v6, "SurveyUpdateTime"

    invoke-virtual {v2, v6, v8, v9}, Lcom/samsung/android/providers/context/ContextPreference;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 217
    .local v4, "storedTime":J
    cmp-long v6, v4, v8

    if-nez v6, :cond_1

    .line 218
    const-string v6, "ContextFramework"

    const-string v7, "TaskScheduler: no survey log update time"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_0
    :goto_0
    return v3

    .line 222
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 223
    .local v0, "elapsedTime":J
    sub-long v6, v0, v4

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0xc

    sget-object v9, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v10, v11, v9}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    .line 226
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private processTasks()V
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mSupportsSurveyLog:Z

    if-eqz v0, :cond_0

    .line 194
    invoke-static {}, Lcom/samsung/android/providers/context/TaskScheduler;->needCollectData()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/samsung/android/providers/context/TaskScheduler;->needSurveyLogging()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 196
    invoke-static {}, Lcom/samsung/android/providers/context/TaskScheduler;->updateSurveyLoggingTime()V

    .line 201
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/providers/context/status/SystemStatusMonitor;->logWeeklyReport(Landroid/content/Context;)V

    .line 205
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mSupportsServerUpload:Z

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mServerUploader:Lcom/samsung/android/providers/context/server/ServerUploader;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/server/ServerUploader;->start()V

    .line 209
    :cond_1
    invoke-static {}, Lcom/samsung/android/providers/context/log/db/LogDisposer;->triggerDeletionProcess()V

    .line 210
    return-void

    .line 198
    :cond_2
    const-string v0, "ContextFramework"

    const-string v1, "TaskScheduler: no need to collect battery and memory information"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerTimeChangeReceiver()V
    .locals 3

    .prologue
    .line 243
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 244
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 245
    const-string v1, "com.samsung.android.providers.context.action.DUMP_CONTEXT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 246
    iget-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/android/providers/context/TaskScheduler$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/providers/context/TaskScheduler$2;-><init>(Lcom/samsung/android/providers/context/TaskScheduler;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 267
    return-void
.end method

.method private scheduleBatchProcessing()V
    .locals 5

    .prologue
    .line 152
    invoke-static {}, Lcom/samsung/android/providers/context/TaskScheduler;->checkTime()J

    move-result-wide v0

    .line 154
    .local v0, "delay":J
    const-string v2, "ContextFramework"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TaskScheduler: scheduling work on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " msec"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/providers/context/TaskScheduler;->batchProcessing(J)V

    .line 157
    return-void
.end method

.method private static setTestProcessingDate()V
    .locals 8

    .prologue
    .line 270
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v1

    .line 271
    .local v1, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v4, "DeletionDate"

    const-wide/16 v6, 0x0

    invoke-virtual {v1, v4, v6, v7}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    .line 273
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 275
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v4, 0x5

    const/4 v5, -0x2

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 276
    const/16 v4, 0xb

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 277
    invoke-static {v0}, Lcom/samsung/android/providers/context/TaskScheduler;->clearMinorField(Ljava/util/Calendar;)V

    .line 279
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 280
    .local v2, "sentDate":J
    const-string v4, "SentDate"

    invoke-virtual {v1, v4, v2, v3}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    .line 281
    return-void
.end method

.method private static updateSentDate(Z)V
    .locals 15
    .param p0, "isTimeChanged"    # Z

    .prologue
    .line 101
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v1

    .line 102
    .local v1, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v7, "SentDate"

    const-wide/16 v10, 0x0

    invoke-virtual {v1, v7, v10, v11}, Lcom/samsung/android/providers/context/ContextPreference;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 104
    .local v4, "sentDate":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 105
    .local v0, "calendar":Ljava/util/Calendar;
    const-wide/16 v10, 0x0

    cmp-long v7, v4, v10

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    if-ne p0, v7, :cond_3

    .line 106
    :cond_0
    const/4 v7, 0x5

    const/4 v10, -0x1

    invoke-virtual {v0, v7, v10}, Ljava/util/Calendar;->add(II)V

    .line 107
    const/16 v7, 0xb

    const/4 v10, 0x0

    invoke-virtual {v0, v7, v10}, Ljava/util/Calendar;->set(II)V

    .line 108
    invoke-static {v0}, Lcom/samsung/android/providers/context/TaskScheduler;->clearMinorField(Ljava/util/Calendar;)V

    .line 110
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 111
    .local v2, "checkDate":J
    const-wide/16 v10, 0x0

    cmp-long v7, v4, v10

    if-eqz v7, :cond_2

    cmp-long v7, v4, v2

    if-gtz v7, :cond_2

    .line 112
    sub-long v10, v2, v4

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v12, 0x3

    sget-object v14, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v12, v13, v14}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    cmp-long v7, v10, v12

    if-ltz v7, :cond_1

    .line 113
    const/4 v7, 0x5

    const/4 v10, -0x3

    invoke-virtual {v0, v7, v10}, Ljava/util/Calendar;->add(II)V

    .line 120
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 141
    .end local v2    # "checkDate":J
    :goto_1
    const-string v7, "SentDate"

    invoke-virtual {v1, v7, v4, v5}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    .line 142
    :cond_1
    return-void

    .line 118
    .restart local v2    # "checkDate":J
    :cond_2
    const-string v7, "ContextFramework"

    const-string v10, "Set initial date as \"yesterday\""

    invoke-static {v7, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 122
    .end local v2    # "checkDate":J
    :cond_3
    const/4 v7, 0x5

    const/4 v10, -0x4

    invoke-virtual {v0, v7, v10}, Ljava/util/Calendar;->add(II)V

    .line 125
    const/16 v7, 0xb

    const/4 v10, 0x0

    invoke-virtual {v0, v7, v10}, Ljava/util/Calendar;->set(II)V

    .line 126
    invoke-static {v0}, Lcom/samsung/android/providers/context/TaskScheduler;->clearMinorField(Ljava/util/Calendar;)V

    .line 128
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v7

    iget-boolean v7, v7, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v7, :cond_4

    .line 129
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyy-MM-dd HH:mm:ss.SSS"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-direct {v6, v7, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 130
    .local v6, "simpleFormat":Ljava/text/SimpleDateFormat;
    const-string v7, "ContextFramework"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "TaskScheduler: previous sent date is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    .end local v6    # "simpleFormat":Ljava/text/SimpleDateFormat;
    :cond_4
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 134
    .local v8, "threeDays":J
    cmp-long v7, v4, v8

    if-gez v7, :cond_1

    .line 138
    move-wide v4, v8

    goto :goto_1
.end method

.method private static updateSurveyLoggingTime()V
    .locals 4

    .prologue
    .line 230
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v2

    .line 232
    .local v2, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 233
    .local v0, "elapsedTime":J
    const-string v3, "SurveyUpdateTime"

    invoke-virtual {v2, v3, v0, v1}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    .line 234
    return-void
.end method


# virtual methods
.method public fini()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/samsung/android/providers/context/TaskScheduler;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 84
    :cond_0
    return-void
.end method

.method public init()V
    .locals 2

    .prologue
    .line 71
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->waitSamsungAccountReadyAndFree()Z

    .line 73
    const-string v0, "ContextFramework"

    const-string v1, "TaskScheduler: initiate processing"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/providers/context/TaskScheduler;->updateSentDate(Z)V

    .line 76
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/providers/context/TaskScheduler;->batchProcessing(J)V

    .line 77
    invoke-direct {p0}, Lcom/samsung/android/providers/context/TaskScheduler;->registerTimeChangeReceiver()V

    .line 78
    return-void
.end method
