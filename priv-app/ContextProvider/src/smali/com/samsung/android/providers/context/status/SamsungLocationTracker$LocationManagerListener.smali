.class public Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;
.super Ljava/lang/Object;
.source "SamsungLocationTracker.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/SamsungLocationTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LocationManagerListener"
.end annotation


# instance fields
.field private final mName:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/providers/context/status/SamsungLocationTracker;


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/status/SamsungLocationTracker;Ljava/lang/String;)V
    .locals 0
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 334
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;->this$0:Lcom/samsung/android/providers/context/status/SamsungLocationTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    iput-object p2, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;->mName:Ljava/lang/String;

    .line 336
    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 6
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 353
    if-eqz p1, :cond_0

    .line 354
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;->this$0:Lcom/samsung/android/providers/context/status/SamsungLocationTracker;

    # invokes: Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->convertLocationInfo(Landroid/location/Location;)Lcom/samsung/android/providers/context/status/LocationInfo;
    invoke-static {v1, p1}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->access$000(Lcom/samsung/android/providers/context/status/SamsungLocationTracker;Landroid/location/Location;)Lcom/samsung/android/providers/context/status/LocationInfo;

    move-result-object v0

    .line 356
    .local v0, "info":Lcom/samsung/android/providers/context/status/LocationInfo;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/providers/context/status/LocationInfo;->setTimestamp(J)V

    .line 357
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;->this$0:Lcom/samsung/android/providers/context/status/SamsungLocationTracker;

    invoke-virtual {v1, v0}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->insertRealtimeLocation(Lcom/samsung/android/providers/context/status/LocationInfo;)V

    .line 359
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v1, :cond_0

    .line 360
    const-string v1, "ContextFramework.SamsungLocationTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LocationListener("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is updated by "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " provider ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    const-string v1, "ContextFramework.SamsungLocationTracker"

    const-string v2, "Request to insert a location logs from LocationListener is successful"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    .end local v0    # "info":Lcom/samsung/android/providers/context/status/LocationInfo;
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 348
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 344
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 340
    return-void
.end method
