.class Lcom/samsung/android/providers/context/server/SPPBinder$1;
.super Ljava/lang/Object;
.source "SPPBinder.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/server/SPPBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/server/SPPBinder;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/server/SPPBinder;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SPPBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SPPBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .prologue
    .line 75
    # getter for: Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "IDlc service is connected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SPPBinder;

    invoke-static {p2}, Lcom/sec/spp/push/dlc/api/IDlcService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/spp/push/dlc/api/IDlcService;

    move-result-object v1

    # setter for: Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;
    invoke-static {v0, v1}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$102(Lcom/samsung/android/providers/context/server/SPPBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 77
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SPPBinder;

    # getter for: Lcom/samsung/android/providers/context/server/SPPBinder;->mConMsgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$200(Lcom/samsung/android/providers/context/server/SPPBinder;)Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    .line 78
    # getter for: Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ConMsgHandler is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SPPBinder;

    # getter for: Lcom/samsung/android/providers/context/server/SPPBinder;->mConMsgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$200(Lcom/samsung/android/providers/context/server/SPPBinder;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 86
    # getter for: Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "IDlc service is disconnected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SPPBinder;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;
    invoke-static {v0, v1}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$102(Lcom/samsung/android/providers/context/server/SPPBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 88
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SPPBinder;

    # getter for: Lcom/samsung/android/providers/context/server/SPPBinder;->mConMsgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$200(Lcom/samsung/android/providers/context/server/SPPBinder;)Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    .line 89
    # getter for: Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ConMsgHandler is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SPPBinder;

    # getter for: Lcom/samsung/android/providers/context/server/SPPBinder;->mConMsgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/SPPBinder;->access$200(Lcom/samsung/android/providers/context/server/SPPBinder;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
