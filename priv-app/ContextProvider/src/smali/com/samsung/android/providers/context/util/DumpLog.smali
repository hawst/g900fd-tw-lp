.class public Lcom/samsung/android/providers/context/util/DumpLog;
.super Ljava/lang/Object;
.source "DumpLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/util/DumpLog$DumpMessage;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static put(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 48
    :try_start_0
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    .line 49
    .local v0, "app":Lcom/samsung/android/providers/context/ContextApplication;
    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/providers/context/util/DumpLog$DumpMessage;

    invoke-direct {v3, v0, p0, p1}, Lcom/samsung/android/providers/context/util/DumpLog$DumpMessage;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v0    # "app":Lcom/samsung/android/providers/context/ContextApplication;
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "ContextFramework"

    const-string v3, "It failed to get application instance"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
