.class Lcom/samsung/android/providers/context/log/db/SecureDbHelper$1;
.super Landroid/os/FileObserver;
.source "SecureDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->startWatchDBFilesObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/log/db/SecureDbHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/log/db/SecureDbHelper;Ljava/lang/String;I)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # I

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper$1;->this$0:Lcom/samsung/android/providers/context/log/db/SecureDbHelper;

    invoke-direct {p0, p2, p3}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 3
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 168
    and-int/lit16 v0, p1, 0x200

    .line 169
    .local v0, "mask":I
    if-eqz p2, :cond_0

    const-string v1, "databases"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x200

    if-ne v1, v0, :cond_0

    .line 170
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper$1;->this$0:Lcom/samsung/android/providers/context/log/db/SecureDbHelper;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDBFileDeleted:Z
    invoke-static {v1, v2}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->access$102(Lcom/samsung/android/providers/context/log/db/SecureDbHelper;Z)Z

    .line 173
    :cond_0
    return-void
.end method
