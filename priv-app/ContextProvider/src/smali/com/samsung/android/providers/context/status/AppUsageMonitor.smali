.class public Lcom/samsung/android/providers/context/status/AppUsageMonitor;
.super Ljava/lang/Object;
.source "AppUsageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;
    }
.end annotation


# instance fields
.field private final mAppUseMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

.field private mHandler:Landroid/os/Handler;

.field private final mLauncherCache:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field mUsageStats:Landroid/app/usage/IUsageStatsManager;

.field private mUsageStatsWatcher:Landroid/app/usage/IUsageStatsWatcher$Stub;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
    .locals 1
    .param p1, "contex"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "device"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mAppUseMap:Ljava/util/HashMap;

    .line 73
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mLauncherCache:Ljava/util/TreeSet;

    .line 75
    new-instance v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/status/AppUsageMonitor$1;-><init>(Lcom/samsung/android/providers/context/status/AppUsageMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mUsageStatsWatcher:Landroid/app/usage/IUsageStatsWatcher$Stub;

    .line 111
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mContext:Landroid/content/Context;

    .line 112
    iput-object p2, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mHandler:Landroid/os/Handler;

    .line 113
    iput-object p3, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    .line 114
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/providers/context/status/AppUsageMonitor;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private insertItem(Landroid/os/Bundle;I)V
    .locals 20
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "action"    # I

    .prologue
    .line 149
    const-string v15, "ComponentName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    .line 150
    .local v4, "componentName":Landroid/content/ComponentName;
    const-string v15, "Intent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/content/Intent;

    .line 152
    .local v9, "intent":Landroid/content/Intent;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_0

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v15

    if-nez v15, :cond_2

    .line 153
    :cond_0
    const-string v15, "ContextFramework"

    const-string v16, "AppUsageMonitor: componentName is null or invalid"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_1
    :goto_0
    return-void

    .line 157
    :cond_2
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v14

    .line 159
    .local v14, "uniqueName":Ljava/lang/String;
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 161
    :pswitch_0
    invoke-virtual {v9}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v3

    .line 164
    .local v3, "categories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_3

    :try_start_0
    const-string v15, "android.intent.category.LAUNCHER"

    invoke-virtual {v9, v15}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->checkLauncher(Landroid/content/ComponentName;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 166
    new-instance v10, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;

    const/4 v15, 0x1

    invoke-direct {v10, v4, v15}, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;-><init>(Landroid/content/ComponentName;I)V

    .line 168
    .local v10, "resumedAppInfo":Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mAppUseMap:Ljava/util/HashMap;

    invoke-virtual {v15, v14, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    .end local v10    # "resumedAppInfo":Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;
    :catch_0
    move-exception v8

    .line 176
    .local v8, "e":Ljava/lang/RuntimeException;
    const-string v15, "ContextFramework"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "AppUsageMonitor: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v8}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 169
    .end local v8    # "e":Ljava/lang/RuntimeException;
    :cond_3
    if-eqz v3, :cond_4

    :try_start_1
    const-string v15, "android.intent.category.HOME"

    invoke-virtual {v9, v15}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 171
    :cond_4
    new-instance v10, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;

    const/4 v15, 0x0

    invoke-direct {v10, v4, v15}, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;-><init>(Landroid/content/ComponentName;I)V

    .line 173
    .restart local v10    # "resumedAppInfo":Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mAppUseMap:Ljava/util/HashMap;

    invoke-virtual {v15, v14, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 182
    .end local v3    # "categories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v10    # "resumedAppInfo":Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mAppUseMap:Ljava/util/HashMap;

    invoke-virtual {v15, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;

    .line 184
    .local v2, "appInfo":Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;
    if-nez v2, :cond_5

    .line 185
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v15

    iget-boolean v15, v15, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v15, :cond_1

    .line 186
    const-string v15, "ContextFramework"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "AppUsageMonitor: app info not found for "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 189
    :cond_5
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 190
    .local v11, "row":Landroid/content/ContentValues;
    const-string v15, "app_id"

    iget-object v0, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->appId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v15, "app_sub_id"

    iget-object v0, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->appSubId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    new-instance v12, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v15

    iget-object v0, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->startTime:Ljava/util/Date;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v12, v15}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 195
    .local v12, "startTime":Ljava/lang/String;
    const-string v15, "start_time"

    invoke-virtual {v11, v15, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v15, "starttime"

    iget-object v0, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->startTime:Ljava/util/Date;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 198
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 200
    .local v5, "currentTime":Ljava/util/Date;
    new-instance v13, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v13, v15}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 201
    .local v13, "stopTime":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    iget-object v15, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->startTime:Ljava/util/Date;

    invoke-virtual {v15}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    sub-long v6, v16, v18

    .line 203
    .local v6, "dur":J
    invoke-static {v6, v7}, Lcom/samsung/android/providers/context/GlobalStatus;->isValidDuration(J)Z

    move-result v15

    if-nez v15, :cond_7

    .line 204
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v15

    iget-boolean v15, v15, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v15, :cond_6

    .line 205
    const-string v15, "ContextFramework"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "AppUsageMonitor: invalid duration : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :cond_6
    const-wide/16 v6, -0x1

    .line 211
    :cond_7
    const-string v15, "stop_time"

    invoke-virtual {v11, v15, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v15, "stoptime"

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 213
    const-string v15, "launcher_type"

    iget v0, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->launcherType:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 214
    const-string v15, "duration"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 217
    const-string v15, "compat_device_status"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->getDeviceContextVersion1()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 219
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    sget-object v16, Lcom/samsung/android/providers/context/log/ContextLogContract$UseApp;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    invoke-static {v15, v0, v11}, Lcom/samsung/android/providers/context/util/ContextLogger;->logLocalProvider(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Z

    .line 221
    iget v15, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->launcherType:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 222
    const/4 v15, 0x6

    invoke-static {v15}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 223
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 225
    const-string v15, "app_id"

    iget-object v0, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->appId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    sget-object v16, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppSurvey;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    invoke-static {v15, v0, v11}, Lcom/samsung/android/providers/context/util/ContextLogger;->logLocalProvider(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Z

    .line 231
    :cond_8
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v15

    iget-boolean v15, v15, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v15, :cond_9

    .line 232
    const-string v15, "ContextFramework"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "AppUsageMonitor: app log inserted "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " type "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v2, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->launcherType:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mAppUseMap:Ljava/util/HashMap;

    invoke-virtual {v15, v14}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 159
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public checkLauncher(Landroid/content/ComponentName;)Z
    .locals 12
    .param p1, "component"    # Landroid/content/ComponentName;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 262
    iget-object v10, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mLauncherCache:Ljava/util/TreeSet;

    invoke-virtual {v10, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 285
    :goto_0
    return v8

    .line 266
    :cond_0
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 267
    .local v5, "pkgName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 269
    .local v0, "className":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v2, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 270
    .local v2, "intentToResolve":Landroid/content/Intent;
    const-string v10, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    invoke-virtual {v2, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    iget-object v10, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v10, v2, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    .line 274
    .local v7, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 275
    .local v6, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v10, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 276
    .local v4, "packageName":Ljava/lang/String;
    iget-object v10, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 278
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 279
    iget-object v9, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mLauncherCache:Ljava/util/TreeSet;

    invoke-virtual {v9, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 280
    const-string v9, "ContextFramework"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "AppUsageMonitor: added cache "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v6    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_2
    move v8, v9

    .line 285
    goto :goto_0
.end method

.method public destroy()V
    .locals 4

    .prologue
    .line 130
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mUsageStats:Landroid/app/usage/IUsageStatsManager;

    if-eqz v1, :cond_0

    .line 132
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mUsageStats:Landroid/app/usage/IUsageStatsManager;

    iget-object v2, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mUsageStatsWatcher:Landroid/app/usage/IUsageStatsWatcher$Stub;

    invoke-interface {v1, v2}, Landroid/app/usage/IUsageStatsManager;->unregisterUsageStatsWatcher(Landroid/app/usage/IUsageStatsWatcher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception : unregisterUsageStatsWatcher - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 246
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 249
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 250
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 251
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->insertItem(Landroid/os/Bundle;I)V

    goto :goto_0

    .line 246
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public init()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 118
    const-string v1, "usagestats"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/app/usage/IUsageStatsManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/usage/IUsageStatsManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mUsageStats:Landroid/app/usage/IUsageStatsManager;

    .line 119
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mUsageStats:Landroid/app/usage/IUsageStatsManager;

    if-eqz v1, :cond_0

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mUsageStats:Landroid/app/usage/IUsageStatsManager;

    iget-object v2, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mUsageStatsWatcher:Landroid/app/usage/IUsageStatsWatcher$Stub;

    invoke-interface {v1, v2}, Landroid/app/usage/IUsageStatsManager;->registerUsageStatsWatcher(Landroid/app/usage/IUsageStatsWatcher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception : registerUsageStatsWatcher - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mLauncherCache:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 146
    return-void
.end method

.method public removePackageFromCache(Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 289
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mLauncherCache:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 290
    .local v0, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 291
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 292
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AppUsageMonitor: removed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from cache"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mLauncherCache:Ljava/util/TreeSet;

    invoke-virtual {v3, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 297
    .end local v0    # "componentName":Landroid/content/ComponentName;
    .end local v2    # "name":Ljava/lang/String;
    :cond_1
    return-void
.end method
