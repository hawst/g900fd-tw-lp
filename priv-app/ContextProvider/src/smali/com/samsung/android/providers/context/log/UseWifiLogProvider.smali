.class public Lcom/samsung/android/providers/context/log/UseWifiLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "UseWifiLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 35
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "ap_ssid"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "ap_mac"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "ip_address"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/UseWifiLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 42
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "use_wifi"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_WIFI:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/UseWifiLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 47
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    new-instance v1, Lcom/samsung/android/providers/context/log/UseWifiLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/UseWifiLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/UseWifiLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 88
    sget-object v1, Lcom/samsung/android/providers/context/log/UseWifiLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 89
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/samsung/android/providers/context/log/UseWifiLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 132
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x1

    .line 93
    const/4 v1, 0x0

    .line 94
    .local v1, "ap_ssid":Ljava/lang/String;
    const/4 v0, 0x0

    .line 95
    .local v0, "ap_mac":Ljava/lang/String;
    const/4 v3, 0x0

    .line 96
    .local v3, "ip_address":Ljava/lang/String;
    const/4 v4, -0x1

    .line 99
    .local v4, "type":I
    :try_start_0
    const-string v6, "ap_ssid"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    const-string v6, "ap_mac"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    const-string v6, "ip_address"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 102
    const-string v6, "type"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 108
    :goto_0
    if-nez v4, :cond_2

    if-eqz v0, :cond_0

    const-string v6, ""

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 109
    :cond_0
    const-string v5, "ContextFramework"

    const-string v6, "UseWifi : [Drop] - ap_mac is invalid"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const/4 v5, 0x0

    .line 122
    :cond_1
    :goto_1
    return v5

    .line 103
    :catch_0
    move-exception v2

    .line 104
    .local v2, "ex":Ljava/lang/NullPointerException;
    const-string v6, "ContextFramework"

    const-string v7, "UseWifi : ValidBoundaryCheck - NullPointerException."

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 112
    .end local v2    # "ex":Ljava/lang/NullPointerException;
    :cond_2
    if-nez v4, :cond_4

    if-eqz v1, :cond_3

    const-string v6, ""

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 113
    :cond_3
    const-string v6, "ContextFramework"

    const-string v7, "UseWifi : ap_ssid is invalid"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_4
    if-nez v4, :cond_6

    if-eqz v3, :cond_5

    const-string v6, ""

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 116
    :cond_5
    const-string v6, "ContextFramework"

    const-string v7, "UseWifi : ip_address is invalid"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_6
    if-eqz v4, :cond_1

    if-eq v4, v5, :cond_1

    .line 119
    const-string v6, "ContextFramework"

    const-string v7, "UseWifi : type is invalid"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
