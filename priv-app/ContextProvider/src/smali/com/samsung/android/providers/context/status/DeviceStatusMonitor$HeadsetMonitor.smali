.class Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;
.super Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;
.source "DeviceStatusMonitor.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;
.implements Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HeadsetMonitor"
.end annotation


# instance fields
.field private final mBtProfiles:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/bluetooth/BluetoothProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestedBtProfilesCount:I

.field final synthetic this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
    .locals 1

    .prologue
    .line 266
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;)V

    .line 264
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mBtProfiles:Landroid/util/SparseArray;

    .line 265
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mRequestedBtProfilesCount:I

    .line 267
    new-instance v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor$1;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 281
    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->isBtHeadsetOn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->checkCurrentStatus()V

    return-void
.end method

.method private checkCurrentStatus()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 284
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    const-string v4, "audio"

    invoke-virtual {v3, v4}, Lcom/samsung/android/providers/context/ContextApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 285
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->isBtHeadsetOn()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    .line 286
    .local v1, "plugged":Z
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBoolean(IZ)Z
    invoke-static {v3, v2, v1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->access$400(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)Z

    .line 287
    return-void

    .line 285
    .end local v1    # "plugged":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isBtHeadset(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 309
    if-eqz p1, :cond_1

    .line 310
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    .line 311
    .local v0, "btClass":Landroid/bluetooth/BluetoothClass;
    if-eqz v0, :cond_1

    .line 312
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v1

    .line 313
    .local v1, "deviceClass":I
    const/16 v2, 0x408

    if-eq v1, v2, :cond_0

    const/16 v2, 0x418

    if-eq v1, v2, :cond_0

    const/16 v2, 0x404

    if-ne v1, v2, :cond_1

    .line 316
    :cond_0
    const/4 v2, 0x1

    .line 320
    .end local v0    # "btClass":Landroid/bluetooth/BluetoothClass;
    .end local v1    # "deviceClass":I
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isBtHeadsetOn()Z
    .locals 7

    .prologue
    .line 290
    const/4 v4, 0x0

    .line 291
    .local v4, "profile":Landroid/bluetooth/BluetoothProfile;
    const/4 v1, 0x0

    .line 292
    .local v1, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mBtProfiles:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 293
    iget-object v5, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mBtProfiles:Landroid/util/SparseArray;

    iget-object v6, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mBtProfiles:Landroid/util/SparseArray;

    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "profile":Landroid/bluetooth/BluetoothProfile;
    check-cast v4, Landroid/bluetooth/BluetoothProfile;

    .line 294
    .restart local v4    # "profile":Landroid/bluetooth/BluetoothProfile;
    if-eqz v4, :cond_1

    .line 295
    invoke-interface {v4}, Landroid/bluetooth/BluetoothProfile;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 296
    if-eqz v1, :cond_1

    .line 297
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 298
    .local v0, "d":Landroid/bluetooth/BluetoothDevice;
    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->isBtHeadset(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 299
    const/4 v5, 0x1

    .line 305
    .end local v0    # "d":Landroid/bluetooth/BluetoothDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :goto_1
    return v5

    .line 292
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 305
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public disable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 344
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->unregisterReceiver(Landroid/content/Context;)V

    .line 345
    return-void
.end method

.method public enable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 339
    return-void
.end method

.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 2
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 325
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mBtProfiles:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 326
    iget v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mRequestedBtProfilesCount:I

    iget-object v1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mBtProfiles:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->checkCurrentStatus()V

    .line 329
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 1
    .param p1, "profile"    # I

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mBtProfiles:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 334
    return-void
.end method

.method public onUserBackground(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 380
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->unregisterReceiver(Landroid/content/Context;)V

    .line 381
    return-void
.end method

.method public onUserForeground(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 350
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 351
    .local v0, "adapter":Landroid/bluetooth/BluetoothAdapter;
    if-nez v0, :cond_0

    .line 352
    const-string v2, "ContextFramework"

    const-string v3, "This device doesn\'t support Bluetooth"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mBtProfiles:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 357
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mRequestedBtProfilesCount:I

    .line 358
    const/4 v2, 0x1

    invoke-virtual {v0, p1, p0, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 359
    const-string v2, "ContextFramework"

    const-string v3, "BluetoothAdapter.getProfileProxy(HEADSET) failed."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :goto_1
    const/4 v2, 0x2

    invoke-virtual {v0, p1, p0, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 364
    const-string v2, "ContextFramework"

    const-string v3, "BluetoothAdapter.getProfileProxy(A2DP) failed."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    :cond_1
    :goto_2
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 371
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 372
    const-string v2, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 373
    const-string v2, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0, p1, v1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->registerReceiver(Landroid/content/Context;Landroid/content/IntentFilter;)V

    goto :goto_0

    .line 361
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :cond_2
    iget v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mRequestedBtProfilesCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mRequestedBtProfilesCount:I

    goto :goto_1

    .line 366
    :cond_3
    iget v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mRequestedBtProfilesCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;->mRequestedBtProfilesCount:I

    goto :goto_2
.end method
