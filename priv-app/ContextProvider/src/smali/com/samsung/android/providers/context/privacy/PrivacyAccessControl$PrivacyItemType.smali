.class public final enum Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
.super Ljava/lang/Enum;
.source "PrivacyAccessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PrivacyItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_BROWSE_WEB:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_CAPTURE_CONTENT:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_CHANGE_DEVICE_STATUS:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_EXCHANGE_CALL:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_EXCHANGE_EMAIL:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_EXCHANGE_MESSAGE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_MANAGE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_PLAY_MUSIC:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_PLAY_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_RECORD_AUDIO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_SEARCH_KEYWORD:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_TRACK_ROAMING:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_UPLOAD_ALWAYS_SERVICE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_USE_WIFI:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public static final enum PRIVACY_ITEM_TYPE_WRITE_DOCUMENT:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;


# instance fields
.field private final mTableName:Ljava/lang/String;

.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 42
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_USE_APP"

    const/4 v2, 0x0

    const-string v3, "use_app"

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_MANAGE_APP"

    const-string v2, "manage_app"

    invoke-direct {v0, v1, v5, v6, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MANAGE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 44
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_TAKE_PHOTO"

    const-string v2, "take_photo"

    invoke-direct {v0, v1, v6, v7, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 45
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_RECORD_VIDEO"

    const-string v2, "record_video"

    invoke-direct {v0, v1, v7, v8, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 46
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_PLAY_MUSIC"

    const-string v2, "play_music"

    invoke-direct {v0, v1, v8, v9, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_PLAY_MUSIC:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 47
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_PLAY_VIDEO"

    const/4 v2, 0x6

    const-string v3, "play_video"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_PLAY_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 48
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_EXCHANGE_CALL"

    const/4 v2, 0x6

    const/4 v3, 0x7

    const-string v4, "exchange_call"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_CALL:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 49
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_EXCHANGE_MESSAGE"

    const/4 v2, 0x7

    const/16 v3, 0x8

    const-string v4, "exchange_message"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_MESSAGE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 50
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_EXCHANGE_EMAIL"

    const/16 v2, 0x8

    const/16 v3, 0x9

    const-string v4, "exchange_email"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_EMAIL:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 51
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_BROWSE_WEB"

    const/16 v2, 0x9

    const/16 v3, 0xa

    const-string v4, "browse_web"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_BROWSE_WEB:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 52
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_SEARCH_KEYWORD"

    const/16 v2, 0xa

    const/16 v3, 0xb

    const-string v4, "search_keyword"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_SEARCH_KEYWORD:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 53
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_TRACK_ROAMING"

    const/16 v2, 0xb

    const/16 v3, 0xc

    const-string v4, "track_roaming"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_TRACK_ROAMING:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 54
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_CHANGE_DEVICE_STATUS"

    const/16 v2, 0xc

    const/16 v3, 0xd

    const-string v4, "change_device_status"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_DEVICE_STATUS:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 55
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_USE_WIFI"

    const/16 v2, 0xd

    const/16 v3, 0xe

    const-string v4, "use_wifi"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_WIFI:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 56
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_MOVE_PLACE"

    const/16 v2, 0xe

    const/16 v3, 0xf

    const-string v4, "move_place"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 57
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_RECORD_AUDIO"

    const/16 v2, 0xf

    const/16 v3, 0x10

    const-string v4, "record_audio"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_RECORD_AUDIO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 58
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_WRITE_DOCUMENT"

    const/16 v2, 0x10

    const/16 v3, 0x11

    const-string v4, "write_document"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_WRITE_DOCUMENT:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 59
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_CAPTURE_CONTENT"

    const/16 v2, 0x11

    const/16 v3, 0x12

    const-string v4, "capture_content"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CAPTURE_CONTENT:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 60
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_MOVE_LOCATION"

    const/16 v2, 0x12

    const/16 v3, 0x13

    const-string v4, "move_location"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 61
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY"

    const/16 v2, 0x13

    const/16 v3, 0x14

    const-string v4, "change_activity"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 62
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_MOVE_AREA"

    const/16 v2, 0x14

    const/16 v3, 0x15

    const-string v4, "move_area"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 63
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_STATUS_PLACE"

    const/16 v2, 0x15

    const/16 v3, 0x16

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 64
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY"

    const/16 v2, 0x16

    const/16 v3, 0x17

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 65
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const-string v1, "PRIVACY_ITEM_TYPE_UPLOAD_ALWAYS_SERVICE"

    const/16 v2, 0x17

    const/16 v3, 0x18

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_ALWAYS_SERVICE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 41
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MANAGE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_PLAY_MUSIC:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_PLAY_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v1, v0, v9

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_CALL:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_MESSAGE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_EMAIL:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_BROWSE_WEB:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_SEARCH_KEYWORD:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_TRACK_ROAMING:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_DEVICE_STATUS:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_WIFI:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_RECORD_AUDIO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_WRITE_DOCUMENT:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CAPTURE_CONTENT:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_ALWAYS_SERVICE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->$VALUES:[Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # I
    .param p4, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 71
    iput p3, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->mValue:I

    .line 72
    iput-object p4, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->mTableName:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public static getPrivacyItem(I)Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .locals 5
    .param p0, "type"    # I

    .prologue
    .line 85
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->values()[Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 86
    .local v1, "enumType":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    invoke-virtual {v1}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 91
    .end local v1    # "enumType":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    :goto_1
    return-object v1

    .line 85
    .restart local v1    # "enumType":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 91
    .end local v1    # "enumType":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->$VALUES:[Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0}, [Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    return-object v0
.end method


# virtual methods
.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->mTableName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->mValue:I

    return v0
.end method
