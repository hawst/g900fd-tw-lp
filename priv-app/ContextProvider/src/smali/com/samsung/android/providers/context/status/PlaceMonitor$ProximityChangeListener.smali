.class public Lcom/samsung/android/providers/context/status/PlaceMonitor$ProximityChangeListener;
.super Landroid/content/BroadcastReceiver;
.source "PlaceMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/PlaceMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProximityChangeListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 629
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 630
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x0

    .line 635
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 636
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 638
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v9, "ContextFramework.ProximityChangeListener"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Receives an action = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    const-string v9, "com.samsung.android.internal.intelligence.useranalysis.action.PLACE_PROXIMITY_CHANGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 641
    if-nez v1, :cond_1

    .line 642
    const-string v9, "ContextFramework.ProximityChangeListener"

    const-string v10, "There is no bundle in the broadcasted Place detection Intent"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    const-string v9, "proximity_type"

    const/4 v10, -0x1

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 649
    .local v7, "type":I
    const/4 v9, 0x1

    if-ne v7, v9, :cond_2

    .line 650
    invoke-static {}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;

    move-result-object v5

    .line 651
    .local v5, "monitor":Lcom/samsung/android/providers/context/status/PlaceMonitor;
    const-string v9, "_id"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 652
    .local v3, "id":I
    const-string v9, "name"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 653
    .local v6, "name":Ljava/lang/String;
    const-string v9, "uri"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 654
    .local v8, "uri":Ljava/lang/String;
    const-string v9, "category"

    invoke-virtual {v1, v9, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 655
    .local v2, "category":I
    const-string v9, "location_method"

    invoke-virtual {v1, v9, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    # invokes: Lcom/samsung/android/providers/context/status/PlaceMonitor;->convertMethodType(I)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    invoke-static {v9}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->access$100(I)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    move-result-object v4

    .line 657
    .local v4, "methodType":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    const-string v9, "ContextFramework.ProximityChangeListener"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Enter place {id="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "cateogry="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "name="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "method="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "}"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    # invokes: Lcom/samsung/android/providers/context/status/PlaceMonitor;->setPlaceEntered(Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V
    invoke-static {v5, v6, v2, v8, v4}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->access$200(Lcom/samsung/android/providers/context/status/PlaceMonitor;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V

    goto :goto_0

    .line 661
    .end local v2    # "category":I
    .end local v3    # "id":I
    .end local v4    # "methodType":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    .end local v5    # "monitor":Lcom/samsung/android/providers/context/status/PlaceMonitor;
    .end local v6    # "name":Ljava/lang/String;
    .end local v8    # "uri":Ljava/lang/String;
    :cond_2
    const/4 v9, 0x2

    if-ne v7, v9, :cond_3

    .line 662
    invoke-static {}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;

    move-result-object v5

    .line 663
    .restart local v5    # "monitor":Lcom/samsung/android/providers/context/status/PlaceMonitor;
    const-string v9, "_id"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 664
    .restart local v3    # "id":I
    const-string v9, "name"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 665
    .restart local v6    # "name":Ljava/lang/String;
    const-string v9, "uri"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 666
    .restart local v8    # "uri":Ljava/lang/String;
    const-string v9, "category"

    invoke-virtual {v1, v9, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 667
    .restart local v2    # "category":I
    const-string v9, "location_method"

    invoke-virtual {v1, v9, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    # invokes: Lcom/samsung/android/providers/context/status/PlaceMonitor;->convertMethodType(I)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    invoke-static {v9}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->access$100(I)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    move-result-object v4

    .line 669
    .restart local v4    # "methodType":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    const-string v9, "ContextFramework.ProximityChangeListener"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exit place {id="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "cateogry="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "name="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "method="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "}"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    # invokes: Lcom/samsung/android/providers/context/status/PlaceMonitor;->setPlaceExited(Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V
    invoke-static {v5, v6, v2, v8, v4}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->access$300(Lcom/samsung/android/providers/context/status/PlaceMonitor;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V

    goto/16 :goto_0

    .line 674
    .end local v2    # "category":I
    .end local v3    # "id":I
    .end local v4    # "methodType":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    .end local v5    # "monitor":Lcom/samsung/android/providers/context/status/PlaceMonitor;
    .end local v6    # "name":Ljava/lang/String;
    .end local v8    # "uri":Ljava/lang/String;
    :cond_3
    const-string v9, "ContextFramework.ProximityChangeListener"

    const-string v10, "Uknown change type"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
