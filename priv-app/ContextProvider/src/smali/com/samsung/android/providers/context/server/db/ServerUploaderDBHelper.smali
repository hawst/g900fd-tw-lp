.class public Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;
.super Lcom/samsung/android/providers/context/log/db/SecureDbHelper;
.source "ServerUploaderDBHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->sInstance:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/providers/context/security/NoKeyException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 45
    const-string v0, "ServerUploaderDBv4.db"

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 46
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const-class v2, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    const-string v3, "getInstance()"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->sInstance:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    if-nez v1, :cond_0

    .line 51
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    const-string v3, "sInstance is null, new ServerUploaderDBHelper()"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :try_start_1
    new-instance v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;

    invoke-direct {v1, p0}, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->sInstance:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    :cond_0
    :goto_0
    :try_start_2
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->sInstance:Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v2

    return-object v1

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 49
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 56
    :catch_1
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 74
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "createTables()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 78
    :try_start_0
    const-string v1, "CREATE TABLE IF NOT EXISTS always_log (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,timestamp DATETIME,svc_code TEXT,activity TEXT,log_type TEXT,user_id TEXT,app_version TEXT,body TEXT,spp_result INTEGER);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 89
    const-string v1, "CREATE TABLE IF NOT EXISTS survey_log (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,timestamp_long INTEGER,timestamp TEXT,svc_code TEXT,activity TEXT,log_type TEXT,app_version TEXT,body TEXT,spp_result INTEGER);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 106
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 107
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "Copmplete create ServerUploaderDB, SurvetDB"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error executing SQL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDatabaseHandlerList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 71
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 112
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "onUpgrade()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS always_log"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 115
    const-string v1, "DROP TABLE IF EXISTS survey_log"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 116
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "Creative Database again, because of onUpgrade"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v1, Lcom/samsung/android/providers/context/server/db/ServerUploaderDBHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error executing SQL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
