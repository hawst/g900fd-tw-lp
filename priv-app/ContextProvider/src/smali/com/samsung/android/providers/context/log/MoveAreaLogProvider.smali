.class public Lcom/samsung/android/providers/context/log/MoveAreaLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "MoveAreaLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 35
    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "timestamp_utc"

    aput-object v2, v1, v5

    const-string v2, "timestamp"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "time_zone"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "place_name"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "place_category"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "longitude"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "latitude"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "duration"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "radius"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/MoveAreaLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "move_area"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/MoveAreaLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 47
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v5, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    .line 48
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->fillCommonColumnsManually:Z

    .line 49
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->supportUpdate:Z

    .line 51
    new-instance v1, Lcom/samsung/android/providers/context/log/MoveAreaLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/MoveAreaLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/MoveAreaLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 90
    sget-object v1, Lcom/samsung/android/providers/context/log/MoveAreaLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 91
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/samsung/android/providers/context/log/MoveAreaLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 135
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 12
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v8, 0x0

    .line 95
    const-wide/high16 v6, -0x3f97000000000000L    # -200.0

    .line 96
    .local v6, "longitude":D
    const-wide/high16 v4, -0x3f97000000000000L    # -200.0

    .line 97
    .local v4, "latitude":D
    const-wide/16 v0, 0x0

    .line 98
    .local v0, "duration":J
    const/4 v3, 0x0

    .line 101
    .local v3, "radius":I
    :try_start_0
    const-string v9, "longitude"

    invoke-virtual {p1, v9}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 102
    const-string v9, "latitude"

    invoke-virtual {p1, v9}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 103
    const-string v9, "duration"

    invoke-virtual {p1, v9}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 104
    const-string v9, "radius"

    invoke-virtual {p1, v9}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 110
    :goto_0
    const-wide v10, -0x3f99800000000000L    # -180.0

    cmpg-double v9, v6, v10

    if-ltz v9, :cond_0

    const-wide v10, 0x4066800000000000L    # 180.0

    cmpl-double v9, v6, v10

    if-lez v9, :cond_1

    .line 111
    :cond_0
    const-string v9, "ContextFramework"

    const-string v10, "MoveArea : [Drop] - longitude is invalid"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :goto_1
    return v8

    .line 105
    :catch_0
    move-exception v2

    .line 106
    .local v2, "ex":Ljava/lang/NullPointerException;
    const-string v9, "ContextFramework"

    const-string v10, "MoveArea : ValidBoundaryCheck - NullPointerException."

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 114
    .end local v2    # "ex":Ljava/lang/NullPointerException;
    :cond_1
    const-wide v10, -0x3fa9800000000000L    # -90.0

    cmpg-double v9, v4, v10

    if-ltz v9, :cond_2

    const-wide v10, 0x4056800000000000L    # 90.0

    cmpl-double v9, v4, v10

    if-lez v9, :cond_3

    .line 115
    :cond_2
    const-string v9, "ContextFramework"

    const-string v10, "MoveArea : [Drop] - latitude is invalid"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 118
    :cond_3
    const-wide/16 v10, 0x0

    cmp-long v9, v0, v10

    if-gtz v9, :cond_4

    .line 119
    const-string v9, "ContextFramework"

    const-string v10, "MoveArea : [Drop] - duration is invalid"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 122
    :cond_4
    if-gtz v3, :cond_5

    .line 123
    const-string v8, "ContextFramework"

    const-string v9, "MoveArea : radius is invalid"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_5
    const/4 v8, 0x1

    goto :goto_1
.end method
