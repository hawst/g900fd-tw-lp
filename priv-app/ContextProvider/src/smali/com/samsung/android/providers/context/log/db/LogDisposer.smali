.class public final Lcom/samsung/android/providers/context/log/db/LogDisposer;
.super Ljava/lang/Object;
.source "LogDisposer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cleanupDatabase()Z
    .locals 5

    .prologue
    .line 64
    const/4 v0, 0x1

    .line 66
    .local v0, "flag":Z
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getLogDatabase()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v1

    .line 67
    .local v1, "logDbHelper":Lcom/samsung/android/providers/context/log/db/LogDbHelper;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getSurveyLogDatabase()Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

    move-result-object v2

    .line 69
    .local v2, "surveyDbHelper":Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->getDatabaseHandlerList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/providers/context/log/db/LogDisposer;->deleteExpiredLogs(Ljava/util/ArrayList;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 70
    :cond_0
    const/4 v0, 0x0

    .line 72
    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;->getDatabaseHandlerList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/providers/context/log/db/LogDisposer;->deleteExpiredLogs(Ljava/util/ArrayList;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 74
    :cond_2
    const/4 v0, 0x0

    .line 77
    :cond_3
    return v0
.end method

.method private static deleteExpiredLogs(Ljava/util/ArrayList;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 13
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "databaseHandlerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;>;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 25
    :cond_0
    const/4 v9, 0x1

    .line 60
    :cond_1
    return v9

    .line 28
    :cond_2
    const/4 v9, 0x1

    .line 29
    .local v9, "result":Z
    const/4 v8, 0x0

    .line 30
    .local v8, "properties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    new-instance v1, Ljava/util/GregorianCalendar;

    const-string v10, "UTC"

    invoke-static {v10}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v10

    invoke-direct {v1, v10}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 31
    .local v1, "calendar":Ljava/util/GregorianCalendar;
    const/16 v10, 0xb

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Ljava/util/GregorianCalendar;->set(II)V

    .line 32
    const/16 v10, 0xc

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Ljava/util/GregorianCalendar;->set(II)V

    .line 33
    const/16 v10, 0xd

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Ljava/util/GregorianCalendar;->set(II)V

    .line 34
    const/16 v10, 0xe

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Ljava/util/GregorianCalendar;->set(II)V

    .line 35
    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    .line 37
    .local v4, "curTime":J
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 38
    .local v6, "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    invoke-virtual {v6}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v8

    .line 39
    if-eqz v8, :cond_3

    iget-object v10, v8, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    if-eqz v10, :cond_3

    .line 43
    invoke-virtual {v1, v4, v5}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 44
    const/4 v10, 0x5

    iget v11, v8, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordLifeTime:I

    neg-int v11, v11

    invoke-virtual {v1, v10, v11}, Ljava/util/GregorianCalendar;->add(II)V

    .line 45
    const/4 v10, 0x1

    new-array v0, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v11

    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v0, v10

    .line 50
    .local v0, "args":[Ljava/lang/String;
    :try_start_0
    iget-object v10, v8, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    const-string v11, "timestamp_utc < ?"

    invoke-virtual {p1, v10, v11, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 51
    .local v2, "count":I
    if-lez v2, :cond_3

    .line 52
    const-string v10, "ContextFramework"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v12, 0x0

    aget-object v12, v0, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "> "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " expired logs are deleted: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v8, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 54
    .end local v2    # "count":I
    :catch_0
    move-exception v3

    .line 55
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    const-string v10, "ContextFramework"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[Skipping] It failed to delete expired logs: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v8, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 56
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public static triggerDeletionProcess()V
    .locals 16

    .prologue
    const-wide/16 v14, 0x0

    .line 81
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v1

    .line 82
    .local v1, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v7, "DeletionDate"

    invoke-virtual {v1, v7, v14, v15}, Lcom/samsung/android/providers/context/ContextPreference;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 83
    .local v2, "deleteDate":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 86
    .local v4, "now":J
    cmp-long v7, v2, v14

    if-eqz v7, :cond_0

    sub-long v8, v4, v2

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0x7

    sget-object v12, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v10, v11, v12}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-gez v7, :cond_0

    .line 87
    const-string v7, "ContextFramework"

    const-string v8, "LogDisposer: no need to cleanup database at this time"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :goto_0
    return-void

    .line 91
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 92
    .local v0, "currentTime":Ljava/util/Date;
    const-string v7, "ContextFramework"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "LogDisposer: perform database cleanup on "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    cmp-long v7, v2, v14

    if-nez v7, :cond_1

    .line 94
    const-string v7, "ContextFramework"

    const-string v8, "LogDisposer: empty deletion date"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_1
    invoke-static {}, Lcom/samsung/android/providers/context/log/db/LogDisposer;->cleanupDatabase()Z

    move-result v6

    .line 100
    .local v6, "successful":Z
    if-eqz v6, :cond_2

    .line 101
    const-string v7, "DeletionDate"

    invoke-virtual {v1, v7, v4, v5}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    .line 103
    :cond_2
    const-string v7, "ContextFramework"

    const-string v8, "LogDisposer: failure on deletion process"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
