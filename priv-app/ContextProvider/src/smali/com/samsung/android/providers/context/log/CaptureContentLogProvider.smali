.class public Lcom/samsung/android/providers/context/log/CaptureContentLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "CaptureContentLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 35
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "app_id"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string v3, "uri"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "type"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/CaptureContentLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 42
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "capture_content"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CAPTURE_CONTENT:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/CaptureContentLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 49
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    .line 51
    new-instance v1, Lcom/samsung/android/providers/context/log/CaptureContentLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/CaptureContentLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/CaptureContentLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 91
    sget-object v1, Lcom/samsung/android/providers/context/log/CaptureContentLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 92
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/samsung/android/providers/context/log/CaptureContentLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 123
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x1

    .line 96
    const/4 v2, 0x0

    .line 97
    .local v2, "uri":Ljava/lang/String;
    const/4 v1, -0x1

    .line 100
    .local v1, "type":I
    :try_start_0
    const-string v3, "uri"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 101
    const-string v3, "type"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 107
    :goto_0
    if-eqz v2, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 108
    :cond_0
    const-string v3, "ContextFramework"

    const-string v4, "CaptureContent : uri is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    if-eqz v1, :cond_2

    if-eq v1, v5, :cond_2

    .line 111
    const-string v3, "ContextFramework"

    const-string v4, "CaptureContent : type is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_2
    return v5

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "ex":Ljava/lang/NullPointerException;
    const-string v3, "ContextFramework"

    const-string v4, "CaptureContent : ValidBoundaryCheck - NullPointerException."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
