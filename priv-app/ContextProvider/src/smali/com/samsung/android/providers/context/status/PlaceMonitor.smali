.class public Lcom/samsung/android/providers/context/status/PlaceMonitor;
.super Ljava/lang/Object;
.source "PlaceMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/status/PlaceMonitor$1;,
        Lcom/samsung/android/providers/context/status/PlaceMonitor$ProximityChangeListener;,
        Lcom/samsung/android/providers/context/status/PlaceMonitor$PlaceChangeListener;,
        Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;,
        Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    }
.end annotation


# static fields
.field private static final UA_PLACE_PROVIDER_URI:Landroid/net/Uri;

.field private static mInstatnce:Lcom/samsung/android/providers/context/status/PlaceMonitor;


# instance fields
.field private isTrackingEnabled:Z

.field private final mContext:Lcom/samsung/android/providers/context/ContextApplication;

.field private final mCurrentPlaces:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIsBootCompleted:Z

.field private mIsCurrentStatusRestored:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-string v0, "content://com.samsung.android.internal.intelligence.useranalysis/place"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->UA_PLACE_PROVIDER_URI:Landroid/net/Uri;

    .line 61
    new-instance v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mInstatnce:Lcom/samsung/android/providers/context/status/PlaceMonitor;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->isTrackingEnabled:Z

    .line 67
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mIsCurrentStatusRestored:Z

    .line 68
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mIsBootCompleted:Z

    .line 74
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    .line 75
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/providers/context/status/PlaceMonitor;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/PlaceMonitor;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->updatePlaceName(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(I)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 51
    invoke-static {p0}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->convertMethodType(I)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/providers/context/status/PlaceMonitor;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/PlaceMonitor;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->setPlaceEntered(Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/providers/context/status/PlaceMonitor;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/PlaceMonitor;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->setPlaceExited(Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V

    return-void
.end method

.method private static convertMethodType(I)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 500
    sget-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_UNKNOWN:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 502
    .local v0, "methodType":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    packed-switch p0, :pswitch_data_0

    .line 516
    :goto_0
    :pswitch_0
    return-object v0

    .line 504
    :pswitch_1
    sget-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_MAP:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 505
    goto :goto_0

    .line 507
    :pswitch_2
    sget-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_WIFI:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 508
    goto :goto_0

    .line 510
    :pswitch_3
    sget-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_BT:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 511
    goto :goto_0

    .line 502
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private fillPlaceInfo(Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 581
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;>;"
    new-instance v13, Ljava/util/LinkedList;

    invoke-direct {v13}, Ljava/util/LinkedList;-><init>()V

    .line 583
    .local v13, "uriList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    .line 584
    .local v10, "info":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    invoke-virtual {v10}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getUri()Ljava/lang/String;

    move-result-object v12

    .line 585
    .local v12, "uri":Ljava/lang/String;
    invoke-virtual {v13, v12}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 586
    invoke-virtual {v13, v12}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 590
    .end local v10    # "info":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    .end local v12    # "uri":Ljava/lang/String;
    :cond_1
    invoke-virtual {v13}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .end local v8    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 591
    .local v14, "uriString":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "category"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "name"

    aput-object v1, v2, v0

    .line 592
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 593
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_2

    .line 594
    const-string v0, "ContextFramework.PlaceMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "it failed to get place info: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 597
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 598
    const-string v0, "category"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 599
    .local v6, "category":I
    const-string v0, "name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 600
    .local v11, "name":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    .line 601
    .restart local v10    # "info":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    invoke-virtual {v10}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 602
    invoke-virtual {v10, v6}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setCategory(I)V

    .line 603
    invoke-virtual {v10, v11}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setName(Ljava/lang/String;)V

    goto :goto_2

    .line 607
    .end local v6    # "category":I
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "info":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    .end local v11    # "name":Ljava/lang/String;
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 609
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v14    # "uriString":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mInstatnce:Lcom/samsung/android/providers/context/status/PlaceMonitor;

    return-object v0
.end method

.method private notifyStatusPlace(Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;I)V
    .locals 10
    .param p1, "placeInfo"    # Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    .param p2, "type"    # I

    .prologue
    .line 276
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getCategory()I

    move-result v3

    .line 277
    .local v3, "category":I
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getName()Ljava/lang/String;

    move-result-object v5

    .line 278
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getUri()Ljava/lang/String;

    move-result-object v6

    .line 280
    .local v6, "uri":Ljava/lang/String;
    const-string v7, "ContextFramework.PlaceMonitor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Broadcasting Intent(status.place) = {cateogry="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "name="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "type="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "}"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 284
    .local v2, "broadcastIntent":Landroid/content/Intent;
    const-string v7, "com.samsung.android.providers.context.action.STATUS_PLACE_CHANGED"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    const-string v7, "category"

    invoke-virtual {v2, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 286
    const-string v7, "name"

    invoke-virtual {v2, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    const-string v7, "type"

    invoke-virtual {v2, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 288
    const-string v7, "uri"

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    const-string v7, "method"

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getMethodTypeAsInt()I

    move-result v8

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 291
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v7, v8}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getPermittedAppIdList(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Ljava/util/List;

    move-result-object v1

    .line 293
    .local v1, "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 294
    .local v0, "appId":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v7

    const-string v8, "com.samsung.android.providers.context.permission.STATUS_PLACE"

    invoke-virtual {v7, v2, v8}, Lcom/samsung/android/providers/context/ContextApplication;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    .line 297
    .end local v0    # "appId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private restoreCurrentStatusFromDb()V
    .locals 18

    .prologue
    .line 520
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mIsCurrentStatusRestored:Z

    if-nez v3, :cond_0

    .line 521
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    monitor-enter v4

    .line 522
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-eqz v3, :cond_1

    .line 523
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v5, "Illegal State: the number of the current place is not zero"

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    monitor-exit v4

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 526
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 529
    .local v14, "elapsedTime":J
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mIsBootCompleted:Z

    if-nez v3, :cond_2

    const-wide/32 v4, 0x1d4c0

    cmp-long v3, v14, v4

    if-gez v3, :cond_3

    .line 532
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-static {v3}, Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 533
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "place_monitor"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 539
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mIsBootCompleted:Z

    .line 540
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "all records of place_monitor are cleared (first run since boot)"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    :goto_2
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mIsCurrentStatusRestored:Z

    goto :goto_0

    .line 526
    .end local v14    # "elapsedTime":J
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 534
    .restart local v14    # "elapsedTime":J
    :catch_0
    move-exception v12

    .line 535
    .local v12, "e":Landroid/database/sqlite/SQLiteException;
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "It failed to delete all records"

    invoke-static {v3, v4, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 536
    .end local v12    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v12

    .line 537
    .local v12, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v12}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 543
    .end local v12    # "e":Ljava/lang/IllegalStateException;
    :cond_3
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 544
    .local v10, "curPlaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;>;"
    const/4 v11, 0x0

    .line 546
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-static {v3}, Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 547
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "place_monitor"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v11

    .line 554
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_3
    if-eqz v11, :cond_5

    .line 555
    :goto_4
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 556
    const-string v3, "uri"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 558
    .local v17, "uri":Ljava/lang/String;
    const-string v3, "method_type"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 560
    .local v16, "methodType":I
    new-instance v13, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;-><init>(Ljava/lang/String;)V

    .line 561
    .local v13, "info":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->convertMethodType(I)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    move-result-object v3

    invoke-virtual {v13, v3}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setMethodType(Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V

    .line 562
    invoke-virtual {v10, v13}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 549
    .end local v13    # "info":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    .end local v16    # "methodType":I
    .end local v17    # "uri":Ljava/lang/String;
    :catch_2
    move-exception v12

    .line 550
    .local v12, "e":Landroid/database/sqlite/SQLiteException;
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "It failed to query from \'place_monitor\' table"

    invoke-static {v3, v4, v12}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 551
    .end local v12    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_3
    move-exception v12

    .line 552
    .local v12, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v12}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_3

    .line 564
    .end local v12    # "e":Ljava/lang/IllegalStateException;
    :cond_4
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 565
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->fillPlaceInfo(Ljava/util/List;)V

    .line 567
    :cond_5
    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 568
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    monitor-enter v4

    .line 569
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v3, v10}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 570
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 572
    :cond_6
    const-string v3, "ContextFramework.PlaceMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "current places are restored from place_monitor: count="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 570
    :catchall_1
    move-exception v3

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v3
.end method

.method private setPlaceEntered(Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V
    .locals 16
    .param p1, "placeName"    # Ljava/lang/String;
    .param p2, "placeCategory"    # I
    .param p3, "placeUri"    # Ljava/lang/String;
    .param p4, "methodType"    # Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .prologue
    .line 167
    new-instance v7, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    move-object/from16 v0, p3

    invoke-direct {v7, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;-><init>(Ljava/lang/String;)V

    .line 168
    .local v7, "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setName(Ljava/lang/String;)V

    .line 169
    move/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setCategory(I)V

    .line 170
    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setMethodType(Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V

    .line 172
    const/4 v2, 0x0

    .line 173
    .local v2, "added":Z
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->restoreCurrentStatusFromDb()V

    .line 174
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    monitor-enter v12

    .line 176
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v11, v7}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 177
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v11, v7}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 178
    const/4 v2, 0x1

    .line 180
    :cond_0
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    invoke-virtual {v7}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getMethodTypeAsInt()I

    move-result v6

    .line 183
    .local v6, "methodTypeInt":I
    if-eqz v2, :cond_2

    .line 185
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 186
    .local v10, "row":Landroid/content/ContentValues;
    const-string v11, "type"

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 187
    const-string v11, "uri"

    move-object/from16 v0, p3

    invoke-virtual {v10, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :try_start_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-virtual {v11}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v12, Lcom/samsung/android/providers/context/log/ContextLogContract$MovePlace;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v11, v12, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 198
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-static {v11}, Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 199
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    .line 200
    const-string v11, "uri"

    move-object/from16 v0, p3

    invoke-virtual {v10, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v11, "method_type"

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getMethodTypeAsInt()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 202
    const-string v11, "place_monitor"

    const/4 v12, 0x0

    invoke-virtual {v3, v11, v12, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 203
    .local v8, "result":J
    const-wide/16 v12, 0x0

    cmp-long v11, v8, v12

    if-gez v11, :cond_1

    .line 204
    const-string v11, "ContextFramework.PlaceMonitor"

    const-string v12, "It failed to insert a records to place_monitor table"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 213
    .end local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "result":J
    :cond_1
    :goto_1
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v11}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->notifyStatusPlace(Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;I)V

    .line 215
    .end local v10    # "row":Landroid/content/ContentValues;
    :cond_2
    const-string v11, "PM"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "1, "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-static {v13}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/providers/context/util/DumpLog;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    return-void

    .line 180
    .end local v6    # "methodTypeInt":I
    :catchall_0
    move-exception v11

    :try_start_3
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v11

    .line 192
    .restart local v6    # "methodTypeInt":I
    .restart local v10    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v5

    .line 193
    .local v5, "ex":Ljava/lang/Exception;
    const-string v11, "ContextFramework.PlaceMonitor"

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 206
    .end local v5    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    .line 207
    .local v4, "e":Landroid/database/sqlite/SQLiteException;
    const-string v11, "ContextFramework.PlaceMonitor"

    const-string v12, "It failed to insert a records to place_monitor table"

    invoke-static {v11, v12, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 208
    .end local v4    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_2
    move-exception v4

    .line 209
    .local v4, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v4}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method private setPlaceExited(Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V
    .locals 16
    .param p1, "placeName"    # Ljava/lang/String;
    .param p2, "placeCategory"    # I
    .param p3, "placeUri"    # Ljava/lang/String;
    .param p4, "method"    # Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .prologue
    .line 220
    new-instance v7, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    move-object/from16 v0, p3

    invoke-direct {v7, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;-><init>(Ljava/lang/String;)V

    .line 221
    .local v7, "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setName(Ljava/lang/String;)V

    .line 222
    move/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setCategory(I)V

    .line 223
    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setMethodType(Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V

    .line 225
    const/4 v8, 0x0

    .line 226
    .local v8, "removed":Z
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->restoreCurrentStatusFromDb()V

    .line 227
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    monitor-enter v13

    .line 229
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v12, v7}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 230
    .local v5, "index":I
    if-ltz v5, :cond_0

    .line 231
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v12, v5}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 232
    const/4 v8, 0x1

    .line 234
    :cond_0
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    invoke-virtual {v7}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getMethodTypeAsInt()I

    move-result v6

    .line 237
    .local v6, "methodTypeInt":I
    if-eqz v8, :cond_2

    .line 239
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 240
    .local v10, "row":Landroid/content/ContentValues;
    const-string v12, "type"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 241
    const-string v12, "uri"

    move-object/from16 v0, p3

    invoke-virtual {v10, v12, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-virtual {v12}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    sget-object v13, Lcom/samsung/android/providers/context/log/ContextLogContract$MovePlace;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v12, v13, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 252
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-static {v12}, Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/providers/context/status/db/StatusMonitorDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 253
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v11, "uri=? AND method_type=?"

    .line 255
    .local v11, "whereClause":Ljava/lang/String;
    const-string v12, "place_monitor"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object p3, v13, v14

    const/4 v14, 0x1

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getMethodTypeAsInt()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v2, v12, v11, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v9

    .line 258
    .local v9, "result":I
    if-gtz v9, :cond_1

    .line 259
    const-string v12, "ContextFramework.PlaceMonitor"

    const-string v13, "It failed to delete a records from place_monitor table"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 268
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v9    # "result":I
    .end local v11    # "whereClause":Ljava/lang/String;
    :cond_1
    :goto_1
    const/4 v12, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v12}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->notifyStatusPlace(Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;I)V

    .line 270
    .end local v10    # "row":Landroid/content/ContentValues;
    :cond_2
    const-string v12, "PM"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "2, "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    invoke-static {v14}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/providers/context/util/DumpLog;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    return-void

    .line 234
    .end local v5    # "index":I
    .end local v6    # "methodTypeInt":I
    :catchall_0
    move-exception v12

    :try_start_3
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v12

    .line 246
    .restart local v5    # "index":I
    .restart local v6    # "methodTypeInt":I
    .restart local v10    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v4

    .line 247
    .local v4, "ex":Ljava/lang/Exception;
    const-string v12, "ContextFramework.PlaceMonitor"

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 261
    .end local v4    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 262
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    const-string v12, "ContextFramework.PlaceMonitor"

    const-string v13, "It failed to delete a records from place_monitor table"

    invoke-static {v12, v13, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 263
    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_2
    move-exception v3

    .line 264
    .local v3, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v3}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method private updatePlaceName(ILjava/lang/String;)V
    .locals 5
    .param p1, "placeId"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 315
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 316
    .local v2, "uri":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->restoreCurrentStatusFromDb()V

    .line 317
    iget-object v4, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    monitor-enter v4

    .line 318
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    .line 319
    .local v1, "info":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 320
    invoke-virtual {v1, p2}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 323
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 324
    return-void
.end method


# virtual methods
.method public declared-synchronized disable()V
    .locals 8

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->isTrackingEnabled:Z

    if-nez v3, :cond_1

    .line 124
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Place detection is not enabled"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 130
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/providers/context/status/PlaceMonitor;->UA_PLACE_PROVIDER_URI:Landroid/net/Uri;

    const-string v5, "setUserConsent"

    const-string v6, "0"

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 132
    .local v1, "output":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 133
    const-string v3, "result"

    const/4 v4, -0x3

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 134
    .local v2, "result":I
    if-nez v2, :cond_2

    .line 135
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->isTrackingEnabled:Z

    .line 136
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Disabling place detection of UA (SUCCESS)"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 141
    .end local v1    # "output":Landroid/os/Bundle;
    .end local v2    # "result":I
    :catch_0
    move-exception v0

    .line 142
    .local v0, "ex":Ljava/lang/NullPointerException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 143
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Exception has occurred while disabling place detection"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 123
    .end local v0    # "ex":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 138
    .restart local v1    # "output":Landroid/os/Bundle;
    .restart local v2    # "result":I
    :cond_2
    :try_start_3
    const-string v3, "ContextFramework.PlaceMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Disabling place detection to UA (FAIL), Error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 144
    .end local v1    # "output":Landroid/os/Bundle;
    .end local v2    # "result":I
    :catch_1
    move-exception v0

    .line 145
    .local v0, "ex":Ljava/lang/IllegalStateException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 146
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Exception has occurred while disabling place detection"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 147
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 148
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 149
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Exception has occurred while disabling place detection"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized enable()V
    .locals 8

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->isTrackingEnabled:Z

    if-eqz v3, :cond_1

    .line 92
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Place detection is already enabled"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 98
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/providers/context/status/PlaceMonitor;->UA_PLACE_PROVIDER_URI:Landroid/net/Uri;

    const-string v5, "setUserConsent"

    const-string v6, "2"

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 100
    .local v1, "output":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 101
    const-string v3, "result"

    const/4 v4, -0x3

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 102
    .local v2, "result":I
    if-nez v2, :cond_2

    .line 103
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->isTrackingEnabled:Z

    .line 104
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Enabling place detection of UA (SUCCESS)"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 109
    .end local v1    # "output":Landroid/os/Bundle;
    .end local v2    # "result":I
    :catch_0
    move-exception v0

    .line 110
    .local v0, "ex":Ljava/lang/NullPointerException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 111
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Exception has occurred while enabling place detection"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 91
    .end local v0    # "ex":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 106
    .restart local v1    # "output":Landroid/os/Bundle;
    .restart local v2    # "result":I
    :cond_2
    :try_start_3
    const-string v3, "ContextFramework.PlaceMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enabling place detection of UA (FAIL), Error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 112
    .end local v1    # "output":Landroid/os/Bundle;
    .end local v2    # "result":I
    :catch_1
    move-exception v0

    .line 113
    .local v0, "ex":Ljava/lang/IllegalStateException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 114
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Exception has occurred while enabling place detection"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 115
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 116
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 117
    const-string v3, "ContextFramework.PlaceMonitor"

    const-string v4, "Exception has occurred while enabling place detection"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method fillCursor(Landroid/database/MatrixCursor;[Ljava/lang/String;)V
    .locals 12
    .param p1, "matrixCursor"    # Landroid/database/MatrixCursor;
    .param p2, "projections"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 327
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "status place: "

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 328
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->restoreCurrentStatusFromDb()V

    .line 329
    iget-object v9, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    monitor-enter v9

    .line 330
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    .line 331
    .local v5, "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v7

    .line 333
    .local v7, "rb":Landroid/database/MatrixCursor$RowBuilder;
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v6, v0, v3

    .line 334
    .local v6, "property":Ljava/lang/String;
    const-string v8, "category"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 335
    invoke-virtual {v5}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getCategory()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 333
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 336
    :cond_0
    const-string v8, "name"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 337
    invoke-virtual {v5}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    .line 354
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    .end local v6    # "property":Ljava/lang/String;
    .end local v7    # "rb":Landroid/database/MatrixCursor$RowBuilder;
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 338
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v5    # "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    .restart local v6    # "property":Ljava/lang/String;
    .restart local v7    # "rb":Landroid/database/MatrixCursor$RowBuilder;
    :cond_1
    :try_start_1
    const-string v8, "uri"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 339
    invoke-virtual {v5}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getUri()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    .line 340
    :cond_2
    const-string v8, "method"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 341
    invoke-virtual {v5}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getMethodTypeAsInt()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    .line 343
    :cond_3
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->close()V

    .line 344
    const-string v8, "ContextFramework.PlaceMonitor"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Projection("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") is wrong (case sensitive)"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Projection("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") is wrong (case sensitive)"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 348
    .end local v6    # "property":Ljava/lang/String;
    :cond_4
    const/16 v8, 0x28

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ", "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getMethodType()Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ") "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 354
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    .end local v7    # "rb":Landroid/database/MatrixCursor$RowBuilder;
    :cond_5
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355
    const-string v8, "ContextFramework.PlaceMonitor"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    return-void
.end method

.method public getCurrentMyPlaceInfo()Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->restoreCurrentStatusFromDb()V

    .line 158
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    monitor-enter v1

    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mCurrentPlaces:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    monitor-exit v1

    .line 163
    :goto_0
    return-object v0

    .line 162
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    sget-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->UNKNOWN_PLACE:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    goto :goto_0

    .line 162
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method updateAllPlaceNames()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 300
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "name"

    aput-object v1, v2, v0

    .line 301
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor;->mContext:Lcom/samsung/android/providers/context/ContextApplication;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 302
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 303
    const-string v0, "ContextFramework.PlaceMonitor"

    const-string v1, "it failed to get place info"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :goto_0
    return-void

    .line 306
    :cond_0
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 308
    .local v7, "id":I
    const-string v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 309
    .local v8, "name":Ljava/lang/String;
    invoke-direct {p0, v7, v8}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->updatePlaceName(ILjava/lang/String;)V

    goto :goto_1

    .line 311
    .end local v7    # "id":I
    .end local v8    # "name":Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method
