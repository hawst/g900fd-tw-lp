.class Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;
.super Ljava/lang/Object;
.source "AreaTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/AreaTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AreaInfo"
.end annotation


# instance fields
.field private mDuration:J

.field private mIsSavable:Z

.field private mLatitude:D

.field private mLogitude:D

.field private mRadius:I

.field private mStartTime:J

.field private mUpdateUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    iput-wide v4, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mStartTime:J

    .line 249
    iput-wide v4, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mDuration:J

    .line 250
    iput v2, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mRadius:I

    .line 251
    iput-wide v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mLogitude:D

    .line 252
    iput-wide v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mLatitude:D

    .line 254
    iput-boolean v2, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mIsSavable:Z

    .line 257
    return-void
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 264
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mDuration:J

    return-wide v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 276
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mLatitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 272
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mLogitude:D

    return-wide v0
.end method

.method public getRadius()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mRadius:I

    return v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 260
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mStartTime:J

    return-wide v0
.end method

.method public getUpdateUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mUpdateUri:Ljava/lang/String;

    return-object v0
.end method

.method public isSavable()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mIsSavable:Z

    return v0
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 292
    iput-wide p1, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mDuration:J

    .line 293
    return-void
.end method

.method public setLatitude(D)V
    .locals 1
    .param p1, "latitude"    # D

    .prologue
    .line 304
    iput-wide p1, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mLatitude:D

    .line 305
    return-void
.end method

.method public setLongitude(D)V
    .locals 1
    .param p1, "longitude"    # D

    .prologue
    .line 300
    iput-wide p1, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mLogitude:D

    .line 301
    return-void
.end method

.method public setRadius(I)V
    .locals 0
    .param p1, "radius"    # I

    .prologue
    .line 296
    iput p1, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mRadius:I

    .line 297
    return-void
.end method

.method public setSavable(Z)V
    .locals 0
    .param p1, "isSavable"    # Z

    .prologue
    .line 312
    iput-boolean p1, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mIsSavable:Z

    .line 313
    return-void
.end method

.method public setStartTime(J)V
    .locals 1
    .param p1, "startTime"    # J

    .prologue
    .line 288
    iput-wide p1, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mStartTime:J

    .line 289
    return-void
.end method

.method public setUpdateUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 308
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/AreaTracker$AreaInfo;->mUpdateUri:Ljava/lang/String;

    .line 309
    return-void
.end method
