.class public Lcom/samsung/android/providers/context/status/LocationInfo;
.super Ljava/lang/Object;
.source "LocationInfo.java"


# instance fields
.field private mAccuracy:F

.field private mLatitude:D

.field private mLongitude:D

.field private mTimestamp:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x3f97000000000000L    # -200.0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mTimestamp:J

    .line 26
    iput-wide v2, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mLongitude:D

    .line 27
    iput-wide v2, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mLatitude:D

    .line 28
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mAccuracy:F

    .line 31
    return-void
.end method


# virtual methods
.method public clone()Lcom/samsung/android/providers/context/status/LocationInfo;
    .locals 4

    .prologue
    .line 34
    new-instance v0, Lcom/samsung/android/providers/context/status/LocationInfo;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/LocationInfo;-><init>()V

    .line 35
    .local v0, "clone":Lcom/samsung/android/providers/context/status/LocationInfo;
    iget-wide v2, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mTimestamp:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/providers/context/status/LocationInfo;->setTimestamp(J)V

    .line 36
    iget-wide v2, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mLongitude:D

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/providers/context/status/LocationInfo;->setLongitude(D)V

    .line 37
    iget-wide v2, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mLatitude:D

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/providers/context/status/LocationInfo;->setLatitude(D)V

    .line 38
    iget v1, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mAccuracy:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/status/LocationInfo;->setAccuracy(F)V

    .line 40
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/status/LocationInfo;->clone()Lcom/samsung/android/providers/context/status/LocationInfo;

    move-result-object v0

    return-object v0
.end method

.method public getAccuracy()F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mAccuracy:F

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mLatitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mLongitude:D

    return-wide v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mTimestamp:J

    return-wide v0
.end method

.method public isValid()Z
    .locals 4

    .prologue
    .line 76
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mTimestamp:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1b7740

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 77
    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAccuracy(F)V
    .locals 0
    .param p1, "accuracy"    # F

    .prologue
    .line 56
    iput p1, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mAccuracy:F

    .line 57
    return-void
.end method

.method public setLatitude(D)V
    .locals 1
    .param p1, "latitude"    # D

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mLatitude:D

    .line 53
    return-void
.end method

.method public setLongitude(D)V
    .locals 1
    .param p1, "longitude"    # D

    .prologue
    .line 48
    iput-wide p1, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mLongitude:D

    .line 49
    return-void
.end method

.method public setTimestamp(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/samsung/android/providers/context/status/LocationInfo;->mTimestamp:J

    .line 45
    return-void
.end method
