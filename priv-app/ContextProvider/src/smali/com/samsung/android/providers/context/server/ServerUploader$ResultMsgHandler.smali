.class Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;
.super Landroid/os/Handler;
.source "ServerUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/server/ServerUploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResultMsgHandler"
.end annotation


# instance fields
.field private final mServerUploader:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/providers/context/server/ServerUploader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/server/ServerUploader;)V
    .locals 1
    .param p1, "uploader"    # Lcom/samsung/android/providers/context/server/ServerUploader;

    .prologue
    .line 259
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 260
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;->mServerUploader:Ljava/lang/ref/WeakReference;

    .line 261
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 266
    iget-object v2, p0, Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;->mServerUploader:Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_1

    .line 267
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v3, "ServerUploader is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;->mServerUploader:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/server/ServerUploader;

    .line 272
    .local v1, "serConn":Lcom/samsung/android/providers/context/server/ServerUploader;
    if-eqz v1, :cond_0

    .line 276
    # getter for: Lcom/samsung/android/providers/context/server/ServerUploader;->mLogSender:Lcom/samsung/android/providers/context/server/LogSender;
    invoke-static {v1}, Lcom/samsung/android/providers/context/server/ServerUploader;->access$400(Lcom/samsung/android/providers/context/server/ServerUploader;)Lcom/samsung/android/providers/context/server/LogSender;

    move-result-object v2

    if-nez v2, :cond_2

    .line 277
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v3, "SerConn.NormalLogSender is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 281
    :cond_2
    if-nez p1, :cond_3

    .line 282
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v3, "Msg is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 286
    :cond_3
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 305
    :pswitch_1
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v3, "handleMessage : MSG_LOG_QUEUE_IS_EMPTY"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 290
    :pswitch_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {v2}, Lcom/sec/spp/push/dlc/api/DlcApi;->getResultStr(I)Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "resultStr":Ljava/lang/String;
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSG_LOG_RESULT / Send Result(SPP) : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-nez v2, :cond_4

    .line 294
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send Result(SPP) : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " logs left\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 295
    :cond_4
    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, -0x2

    if-eq v2, v3, :cond_5

    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, -0x4

    if-eq v2, v3, :cond_5

    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, -0x3

    if-ne v2, v3, :cond_6

    .line 297
    :cond_5
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v3, "Blocked"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 298
    :cond_6
    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, -0x6

    if-ne v2, v3, :cond_0

    .line 299
    # getter for: Lcom/samsung/android/providers/context/server/ServerUploader;->mLogSender:Lcom/samsung/android/providers/context/server/LogSender;
    invoke-static {v1}, Lcom/samsung/android/providers/context/server/ServerUploader;->access$400(Lcom/samsung/android/providers/context/server/ServerUploader;)Lcom/samsung/android/providers/context/server/LogSender;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/server/LogSender;->stopSender()V

    .line 300
    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/providers/context/server/ServerUploader;->isRegistered:Z
    invoke-static {v1, v2}, Lcom/samsung/android/providers/context/server/ServerUploader;->access$502(Lcom/samsung/android/providers/context/server/ServerUploader;Z)Z

    goto/16 :goto_0

    .line 309
    .end local v0    # "resultStr":Ljava/lang/String;
    :pswitch_3
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v3, "handleMessage : MSG_GENERATE_LOG"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 313
    :pswitch_4
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v3, "handleMessage : MSG_SERVICE_CONNECTED"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 317
    :pswitch_5
    sget-object v2, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v3, "handleMessage : MSG_SERVICE_DISCONNECTED"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 286
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
