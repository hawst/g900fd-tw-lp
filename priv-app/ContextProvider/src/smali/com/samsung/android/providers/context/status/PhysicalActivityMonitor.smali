.class public abstract Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;
.super Ljava/lang/Object;
.source "PhysicalActivityMonitor.java"


# static fields
.field private static mInstance:Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;
    .locals 2

    .prologue
    .line 26
    const-class v1, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->mInstance:Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->mInstance:Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;

    .line 30
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->mInstance:Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract disable()V
.end method

.method public abstract enable()V
.end method
