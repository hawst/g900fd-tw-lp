.class public Lcom/samsung/android/providers/context/log/PlayVideoLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "PlayVideoLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 35
    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "app_id"

    aput-object v3, v1, v2

    const-string v2, "title"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "uri"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "year"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "genre"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "start_time"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "stop_time"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "starttime"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "stoptime"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "duration"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "stop_type"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/PlayVideoLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "play_video"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_PLAY_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/PlayVideoLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 47
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    .line 48
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->supportUpdate:Z

    .line 50
    new-instance v1, Lcom/samsung/android/providers/context/log/PlayVideoLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/PlayVideoLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/PlayVideoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 98
    sget-object v1, Lcom/samsung/android/providers/context/log/PlayVideoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 99
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/samsung/android/providers/context/log/PlayVideoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 122
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x1

    .line 103
    const/4 v0, -0x1

    .line 105
    .local v0, "stop_type":I
    const-string v1, "stop_type"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 106
    const-string v1, "stop_type"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 108
    if-eqz v0, :cond_0

    if-eq v0, v3, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 109
    const-string v1, "ContextFramework"

    const-string v2, "PlayVideo : stop_type is invalid"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    return v3
.end method
