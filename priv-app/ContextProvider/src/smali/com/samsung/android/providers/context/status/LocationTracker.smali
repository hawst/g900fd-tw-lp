.class public abstract Lcom/samsung/android/providers/context/status/LocationTracker;
.super Ljava/lang/Object;
.source "LocationTracker.java"


# static fields
.field private static mInstance:Lcom/samsung/android/providers/context/status/LocationTracker;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/providers/context/status/LocationTracker;
    .locals 2

    .prologue
    .line 28
    const-class v1, Lcom/samsung/android/providers/context/status/LocationTracker;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/status/LocationTracker;->mInstance:Lcom/samsung/android/providers/context/status/LocationTracker;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/status/LocationTracker;->mInstance:Lcom/samsung/android/providers/context/status/LocationTracker;

    .line 32
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/status/LocationTracker;->mInstance:Lcom/samsung/android/providers/context/status/LocationTracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract disable()V
.end method

.method public abstract enable()V
.end method

.method public abstract getLatestLocation()Lcom/samsung/android/providers/context/status/LocationInfo;
.end method
