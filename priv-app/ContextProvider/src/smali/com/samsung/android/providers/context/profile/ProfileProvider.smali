.class public Lcom/samsung/android/providers/context/profile/ProfileProvider;
.super Landroid/content/ContentProvider;
.source "ProfileProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    }
.end annotation


# static fields
.field private static final DEFAULT_ORDER:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field static final sUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 52
    const-class v0, Lcom/samsung/android/providers/context/profile/ProfileProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    .line 56
    const-string v0, " %s desc"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/profile/ProfileProvider;->DEFAULT_ORDER:Ljava/lang/String;

    .line 65
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/providers/context/profile/ProfileProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 66
    sget-object v0, Lcom/samsung/android/providers/context/profile/ProfileProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.providers.context.profile"

    const-string v2, "app_used/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 67
    sget-object v0, Lcom/samsung/android/providers/context/profile/ProfileProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.providers.context.profile"

    const-string v2, "app_used"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 68
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 265
    return-void
.end method

.method private FilterForDeciveStatus(ILandroid/database/Cursor;)Landroid/database/Cursor;
    .locals 22
    .param p1, "deviceType"    # I
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 275
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    .line 276
    .local v6, "datas":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;>;"
    const/4 v9, 0x0

    .line 277
    .local v9, "findData":Z
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v17

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 278
    sget-object v17, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "deviceType ="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_0
    const/4 v8, 0x0

    .line 283
    .local v8, "filter":I
    packed-switch p1, :pswitch_data_0

    .line 308
    :goto_0
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v17

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 309
    sget-object v17, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "filter ="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_1
    if-eqz p2, :cond_5

    .line 315
    :try_start_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 317
    :cond_2
    :goto_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 319
    const-string v17, "compat_device_status"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 320
    .local v14, "rowDeviceStatus":J
    const-string v17, "app_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 321
    .local v2, "appId":Ljava/lang/String;
    const-string v17, "app_sub_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 322
    .local v3, "appSubId":Ljava/lang/String;
    const-string v17, "launcher_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 324
    .local v11, "launcherType":I
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v17

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 325
    sget-object v17, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "app_id ="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "device_status ="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " launcher_type = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :cond_3
    const/4 v9, 0x0

    .line 331
    int-to-long v0, v8

    move-wide/from16 v18, v0

    and-long v18, v18, v14

    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-eqz v17, :cond_2

    .line 333
    const/4 v4, 0x0

    .local v4, "cnt":I
    :goto_2
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v4, v0, :cond_4

    .line 334
    invoke-virtual {v6, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;

    .line 335
    .local v16, "temp":Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->app_id:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 336
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->app_sub_id:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 337
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->launcher_type:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v11, v0, :cond_7

    .line 339
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->frequent_count:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->frequent_count:I

    .line 340
    const/4 v9, 0x1

    .line 348
    .end local v16    # "temp":Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    :cond_4
    if-nez v9, :cond_2

    .line 349
    new-instance v13, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;-><init>(Lcom/samsung/android/providers/context/profile/ProfileProvider$1;)V

    .line 350
    .local v13, "newData":Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    iput-object v2, v13, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->app_id:Ljava/lang/String;

    .line 351
    iput-object v3, v13, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->app_sub_id:Ljava/lang/String;

    .line 352
    const/16 v17, 0x1

    move/from16 v0, v17

    iput v0, v13, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->frequent_count:I

    .line 353
    iput v11, v13, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->launcher_type:I

    .line 354
    invoke-virtual {v6, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 360
    .end local v2    # "appId":Ljava/lang/String;
    .end local v3    # "appSubId":Ljava/lang/String;
    .end local v4    # "cnt":I
    .end local v11    # "launcherType":I
    .end local v13    # "newData":Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    .end local v14    # "rowDeviceStatus":J
    :catch_0
    move-exception v7

    .line 361
    .local v7, "ex":Ljava/lang/Exception;
    sget-object v17, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    .end local v7    # "ex":Ljava/lang/Exception;
    :cond_5
    new-instance v17, Lcom/samsung/android/providers/context/profile/ProfileProvider$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/providers/context/profile/ProfileProvider$1;-><init>(Lcom/samsung/android/providers/context/profile/ProfileProvider;)V

    move-object/from16 v0, v17

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 372
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v5, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    const-string v18, "app_id"

    aput-object v18, v5, v17

    const/16 v17, 0x1

    const-string v18, "app_sub_id"

    aput-object v18, v5, v17

    const/16 v17, 0x2

    const-string v18, "frequency"

    aput-object v18, v5, v17

    const/16 v17, 0x3

    const-string v18, "launcher_type"

    aput-object v18, v5, v17

    .line 373
    .local v5, "columnNames":[Ljava/lang/String;
    new-instance v12, Landroid/database/MatrixCursor;

    invoke-direct {v12, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 375
    .local v12, "matrixCursor":Landroid/database/MatrixCursor;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v10, v0, :cond_8

    .line 376
    invoke-virtual {v6, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;

    .line 378
    .restart local v16    # "temp":Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->app_id:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v17, v18

    const/16 v18, 0x1

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->app_sub_id:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v17, v18

    const/16 v18, 0x2

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->frequent_count:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x3

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->launcher_type:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 382
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v17

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    .line 383
    sget-object v17, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "DeviceType query result = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->app_id:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->app_sub_id:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->frequent_count:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;->launcher_type:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_6
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 285
    .end local v5    # "columnNames":[Ljava/lang/String;
    .end local v10    # "i":I
    .end local v12    # "matrixCursor":Landroid/database/MatrixCursor;
    .end local v16    # "temp":Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    :pswitch_0
    const/4 v8, 0x1

    .line 286
    goto/16 :goto_0

    .line 289
    :pswitch_1
    const/4 v8, 0x0

    .line 290
    goto/16 :goto_0

    .line 292
    :pswitch_2
    const/4 v8, 0x2

    .line 293
    goto/16 :goto_0

    .line 295
    :pswitch_3
    const/4 v8, 0x4

    .line 296
    goto/16 :goto_0

    .line 298
    :pswitch_4
    const/16 v8, 0x8

    .line 299
    goto/16 :goto_0

    .line 301
    :pswitch_5
    const/16 v8, 0x80

    .line 302
    goto/16 :goto_0

    .line 304
    :pswitch_6
    const/16 v8, 0x100

    goto/16 :goto_0

    .line 333
    .restart local v2    # "appId":Ljava/lang/String;
    .restart local v3    # "appSubId":Ljava/lang/String;
    .restart local v4    # "cnt":I
    .restart local v11    # "launcherType":I
    .restart local v14    # "rowDeviceStatus":J
    .restart local v16    # "temp":Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 390
    .end local v2    # "appId":Ljava/lang/String;
    .end local v3    # "appSubId":Ljava/lang/String;
    .end local v4    # "cnt":I
    .end local v11    # "launcherType":I
    .end local v14    # "rowDeviceStatus":J
    .end local v16    # "temp":Lcom/samsung/android/providers/context/profile/ProfileProvider$CursorData;
    .restart local v5    # "columnNames":[Ljava/lang/String;
    .restart local v10    # "i":I
    .restart local v12    # "matrixCursor":Landroid/database/MatrixCursor;
    :cond_8
    return-object v12

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static getDateApplyTimeSpan(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "timeSpan"    # Ljava/lang/String;
    .param p1, "timeSpan2"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x6

    const/4 v12, 0x2

    const/4 v11, 0x1

    .line 403
    const/4 v7, 0x0

    .line 404
    .local v7, "spanIndex":I
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 405
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 406
    .local v2, "currentTimeAtMillis":J
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 407
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v9, "yyyy-MM-dd"

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v6, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 412
    .local v6, "sdf":Ljava/text/SimpleDateFormat;
    const-string v9, "-"

    invoke-virtual {p0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v8, v0, v4

    .line 413
    .local v8, "token":Ljava/lang/String;
    if-le v7, v12, :cond_2

    .line 427
    .end local v8    # "token":Ljava/lang/String;
    :cond_0
    const/4 v7, 0x0

    .line 428
    if-eqz p1, :cond_1

    .line 429
    const-string v9, "-"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_1

    aget-object v8, v0, v4

    .line 430
    .restart local v8    # "token":Ljava/lang/String;
    if-le v7, v12, :cond_5

    .line 445
    .end local v8    # "token":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 417
    .restart local v8    # "token":Ljava/lang/String;
    :cond_2
    if-nez v7, :cond_3

    .line 418
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    neg-int v9, v9

    invoke-virtual {v1, v11, v9}, Ljava/util/Calendar;->add(II)V

    .line 424
    :goto_2
    add-int/lit8 v7, v7, 0x1

    .line 412
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 419
    :cond_3
    if-ne v7, v11, :cond_4

    .line 420
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    neg-int v9, v9

    invoke-virtual {v1, v12, v9}, Ljava/util/Calendar;->add(II)V

    goto :goto_2

    .line 422
    :cond_4
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    neg-int v9, v9

    invoke-virtual {v1, v13, v9}, Ljava/util/Calendar;->add(II)V

    goto :goto_2

    .line 434
    :cond_5
    if-nez v7, :cond_6

    .line 435
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    neg-int v9, v9

    invoke-virtual {v1, v11, v9}, Ljava/util/Calendar;->add(II)V

    .line 441
    :goto_3
    add-int/lit8 v7, v7, 0x1

    .line 429
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 436
    :cond_6
    if-ne v7, v11, :cond_7

    .line 437
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    neg-int v9, v9

    invoke-virtual {v1, v12, v9}, Ljava/util/Calendar;->add(II)V

    goto :goto_3

    .line 439
    :cond_7
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    neg-int v9, v9

    invoke-virtual {v1, v13, v9}, Ljava/util/Calendar;->add(II)V

    goto :goto_3
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 25
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_0

    .line 94
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "query() - uri: "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v23, ", selection: "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v23, ", args: "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static/range {p4 .. p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_0
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/providers/context/ContextApplication;->getLogDatabase()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v16

    .line 98
    .local v16, "helper":Lcom/samsung/android/providers/context/log/db/LogDbHelper;
    if-nez v16, :cond_1

    .line 99
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    const-string v8, "database acquisition failure"

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v11, 0x0

    .line 262
    :goto_0
    return-object v11

    .line 103
    :cond_1
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 105
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v3, :cond_2

    .line 106
    const/4 v11, 0x0

    goto :goto_0

    .line 109
    :cond_2
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 111
    .local v2, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v5, 0x0

    .line 112
    .local v5, "querySelection":Ljava/lang/String;
    const/4 v9, 0x0

    .line 113
    .local v9, "querySortOrder":Ljava/lang/String;
    const/16 v21, 0x0

    .line 114
    .local v21, "order":Ljava/lang/String;
    const/16 v22, 0x0

    .line 115
    .local v22, "timeSpan":Ljava/lang/String;
    const/16 v17, -0x1

    .line 116
    .local v17, "integerDeviceType":I
    const/4 v13, 0x0

    .line 117
    .local v13, "deviceType":Ljava/lang/String;
    const/4 v7, 0x0

    .line 119
    .local v7, "groupBy":Ljava/lang/String;
    const/4 v4, 0x0

    .line 120
    .local v4, "returnValues":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 122
    .local v10, "limitSize":Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 219
    new-instance v6, Ljava/lang/UnsupportedOperationException;

    const-string v8, "Operation not supported for uri:"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 136
    :pswitch_0
    const-string v19, "0000-00-14"

    .line 138
    .local v19, "logTimeSpan":Ljava/lang/String;
    const-string v6, "use_app"

    invoke-virtual {v2, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 140
    const-string v6, "time_span"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 142
    if-nez v22, :cond_3

    .line 143
    const-string v22, "0000-00-14"

    .line 146
    :cond_3
    const-string v6, "device_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 148
    if-nez v13, :cond_9

    .line 149
    const/16 v17, -0x1

    .line 154
    :goto_1
    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v6}, Lcom/samsung/android/providers/context/profile/ProfileProvider;->getDateApplyTimeSpan(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 156
    .local v12, "date":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_4

    .line 157
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "spanned date = "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "( start_time > \'"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\' )"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 165
    const-string v6, "last_access"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 169
    .local v18, "lastAccess":Ljava/lang/String;
    const-string v6, "frequency"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 171
    .local v15, "frequency":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_5

    .line 172
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "TIME_SPAM= "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v23, ", Frequency= "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v23, ", LastAccess= "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_5
    if-eqz v18, :cond_b

    .line 176
    const-string v6, "recent"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 177
    const-string v6, "%s desc "

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v23, 0x0

    const-string v24, "stop_time"

    aput-object v24, v8, v23

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    .line 190
    :cond_6
    :goto_2
    if-nez v13, :cond_7

    .line 191
    const-string v7, "app_id,app_sub_id,launcher_type"

    .line 194
    :cond_7
    if-nez p5, :cond_e

    .line 195
    if-eqz v21, :cond_d

    move-object/from16 v9, v21

    .line 200
    :goto_3
    if-eqz v18, :cond_f

    .line 201
    const/4 v6, 0x5

    new-array v4, v6, [Ljava/lang/String;

    .end local v4    # "returnValues":[Ljava/lang/String;
    const/4 v6, 0x0

    const-string v8, "app_id"

    aput-object v8, v4, v6

    const/4 v6, 0x1

    const-string v8, "app_sub_id"

    aput-object v8, v4, v6

    const/4 v6, 0x2

    const-string v8, "max(stop_time) as latest_time"

    aput-object v8, v4, v6

    const/4 v6, 0x3

    const-string v8, "sum(duration) as duration"

    aput-object v8, v4, v6

    const/4 v6, 0x4

    const-string v8, "launcher_type"

    aput-object v8, v4, v6

    .line 222
    .restart local v4    # "returnValues":[Ljava/lang/String;
    :goto_4
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_8

    .line 223
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "query - [selection] : "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v23, ", [querySortOrder] : "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[selectionArgs] : = "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static/range {p4 .. p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[groupBy] : = "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[deviceType] : = "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_8
    const/4 v11, 0x0

    .line 232
    .local v11, "c":Landroid/database/Cursor;
    const/4 v8, 0x0

    move-object/from16 v6, p4

    :try_start_0
    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 237
    :goto_5
    if-nez v11, :cond_11

    .line 238
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 151
    .end local v11    # "c":Landroid/database/Cursor;
    .end local v12    # "date":Ljava/lang/String;
    .end local v15    # "frequency":Ljava/lang/String;
    .end local v18    # "lastAccess":Ljava/lang/String;
    :cond_9
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 179
    .restart local v12    # "date":Ljava/lang/String;
    .restart local v15    # "frequency":Ljava/lang/String;
    .restart local v18    # "lastAccess":Ljava/lang/String;
    :cond_a
    const-string v6, "%s asc "

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v23, 0x0

    const-string v24, "stop_time"

    aput-object v24, v8, v23

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_2

    .line 181
    :cond_b
    if-eqz v15, :cond_6

    .line 182
    const-string v6, "frequent"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 183
    const-string v21, "frequency desc "

    goto/16 :goto_2

    .line 185
    :cond_c
    const-string v21, "frequency asc "

    goto/16 :goto_2

    .line 195
    :cond_d
    sget-object v9, Lcom/samsung/android/providers/context/profile/ProfileProvider;->DEFAULT_ORDER:Ljava/lang/String;

    goto/16 :goto_3

    .line 197
    :cond_e
    move-object/from16 v9, p5

    goto/16 :goto_3

    .line 205
    :cond_f
    if-eqz v13, :cond_10

    .line 206
    const/4 v6, 0x4

    new-array v4, v6, [Ljava/lang/String;

    .end local v4    # "returnValues":[Ljava/lang/String;
    const/4 v6, 0x0

    const-string v8, "app_id"

    aput-object v8, v4, v6

    const/4 v6, 0x1

    const-string v8, "app_sub_id"

    aput-object v8, v4, v6

    const/4 v6, 0x2

    const-string v8, "compat_device_status"

    aput-object v8, v4, v6

    const/4 v6, 0x3

    const-string v8, "launcher_type"

    aput-object v8, v4, v6

    .restart local v4    # "returnValues":[Ljava/lang/String;
    goto/16 :goto_4

    .line 210
    :cond_10
    const/4 v6, 0x5

    new-array v4, v6, [Ljava/lang/String;

    .end local v4    # "returnValues":[Ljava/lang/String;
    const/4 v6, 0x0

    const-string v8, "app_id"

    aput-object v8, v4, v6

    const/4 v6, 0x1

    const-string v8, "app_sub_id"

    aput-object v8, v4, v6

    const/4 v6, 0x2

    const-string v8, "count(app_id) as frequency"

    aput-object v8, v4, v6

    const/4 v6, 0x3

    const-string v8, "sum(duration) as duration"

    aput-object v8, v4, v6

    const/4 v6, 0x4

    const-string v8, "launcher_type"

    aput-object v8, v4, v6

    .restart local v4    # "returnValues":[Ljava/lang/String;
    goto/16 :goto_4

    .line 233
    .restart local v11    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v14

    .line 234
    .local v14, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_5

    .line 241
    .end local v14    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_11
    const/4 v6, -0x1

    move/from16 v0, v17

    if-eq v0, v6, :cond_13

    .line 242
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_12

    .line 243
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[before filtering]: count= "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_12
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1, v11}, Lcom/samsung/android/providers/context/profile/ProfileProvider;->FilterForDeciveStatus(ILandroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v20

    .line 247
    .local v20, "newCursor":Landroid/database/Cursor;
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 249
    move-object/from16 v11, v20

    .line 252
    .end local v20    # "newCursor":Landroid/database/Cursor;
    :cond_13
    if-nez v11, :cond_14

    .line 253
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 256
    :cond_14
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_15

    .line 257
    sget-object v6, Lcom/samsung/android/providers/context/profile/ProfileProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "query completed - query result count is [ "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v23, " ]"

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/providers/context/profile/ProfileProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-interface {v11, v6, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 395
    const/4 v0, 0x0

    return v0
.end method
