.class Lcom/samsung/android/providers/context/status/AppUsageMonitor$1;
.super Landroid/app/usage/IUsageStatsWatcher$Stub;
.source "AppUsageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/AppUsageMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/status/AppUsageMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/status/AppUsageMonitor;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$1;->this$0:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    invoke-direct {p0}, Landroid/app/usage/IUsageStatsWatcher$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public notePauseComponent(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "pauseComponent"    # Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->foreground:Z

    if-nez v2, :cond_0

    .line 106
    :goto_0
    return-void

    .line 99
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 100
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "ComponentName"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 102
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$1;->this$0:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    # getter for: Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->access$000(Lcom/samsung/android/providers/context/status/AppUsageMonitor;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 103
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 104
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$1;->this$0:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    # getter for: Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->access$000(Lcom/samsung/android/providers/context/status/AppUsageMonitor;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public noteResumeComponent(Landroid/content/ComponentName;Landroid/content/Intent;)V
    .locals 4
    .param p1, "resumeComponent"    # Landroid/content/ComponentName;
    .param p2, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->foreground:Z

    if-nez v2, :cond_0

    .line 90
    :goto_0
    return-void

    .line 82
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 83
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "ComponentName"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 84
    const-string v2, "Intent"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 86
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$1;->this$0:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    # getter for: Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->access$000(Lcom/samsung/android/providers/context/status/AppUsageMonitor;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 87
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 88
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$1;->this$0:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    # getter for: Lcom/samsung/android/providers/context/status/AppUsageMonitor;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->access$000(Lcom/samsung/android/providers/context/status/AppUsageMonitor;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
