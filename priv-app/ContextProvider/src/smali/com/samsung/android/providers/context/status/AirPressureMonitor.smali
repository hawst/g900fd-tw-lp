.class public Lcom/samsung/android/providers/context/status/AirPressureMonitor;
.super Ljava/lang/Object;
.source "AirPressureMonitor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener2;


# instance fields
.field private mAirPressureSensor:Landroid/hardware/Sensor;

.field private mAirPressureSet:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mIsEnabled:Z

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mIsEnabled:Z

    .line 57
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mContext:Landroid/content/Context;

    .line 58
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lcom/samsung/android/providers/context/status/AirPressureMonitor$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/providers/context/status/AirPressureMonitor$1;-><init>(Lcom/samsung/android/providers/context/status/AirPressureMonitor;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mAirPressureSet:Ljava/util/TreeSet;

    .line 63
    return-void
.end method

.method private disposeExpiredCaches()V
    .locals 8

    .prologue
    .line 133
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long v2, v4, v6

    .line 134
    .local v2, "currentTimeUsec":J
    iget-object v4, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mAirPressureSet:Ljava/util/TreeSet;

    invoke-virtual {v4}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 136
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Float;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 137
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 138
    .local v0, "cache":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Float;>;"
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    const-wide/32 v6, 0x30e23400

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 139
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 144
    .end local v0    # "cache":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Float;>;"
    :cond_0
    return-void
.end method


# virtual methods
.method public disable()V
    .locals 2

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mIsEnabled:Z

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mIsEnabled:Z

    .line 80
    const-string v0, "ContextFramework.AirPressureMonitor"

    const-string v1, "Air pressure baching is disabled"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_0
    return-void
.end method

.method public declared-synchronized enable()V
    .locals 4

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mIsEnabled:Z

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mSensorManager:Landroid/hardware/SensorManager;

    .line 69
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mAirPressureSensor:Landroid/hardware/Sensor;

    .line 70
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mAirPressureSensor:Landroid/hardware/Sensor;

    const v2, 0xaba9500

    const v3, 0x6b49d200

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;II)Z

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mIsEnabled:Z

    .line 72
    const-string v0, "ContextFramework.AirPressureMonitor"

    const-string v1, "Air pressure baching is enabled"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :cond_0
    monitor-exit p0

    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public flush()Z
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    .line 89
    :cond_0
    const-string v0, "ContextFramework.AirPressureMonitor"

    const-string v1, "Flush air pressure baching"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->flush(Landroid/hardware/SensorEventListener;)Z

    move-result v0

    goto :goto_0
.end method

.method public getLastestAirPressure(J)F
    .locals 9
    .param p1, "timestampUsec"    # J

    .prologue
    .line 94
    new-instance v1, Landroid/util/Pair;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 96
    .local v1, "comparee":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Float;>;"
    const/high16 v0, -0x40800000    # -1.0f

    .line 98
    .local v0, "airPressure":F
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mAirPressureSet:Ljava/util/TreeSet;

    invoke-virtual {v3, v1}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 100
    .local v2, "latestValue":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Float;>;"
    if-eqz v2, :cond_0

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, p1, v4

    const-wide/32 v6, 0x6b49d200

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 101
    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 104
    :cond_0
    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 110
    return-void
.end method

.method public onFlushCompleted(Landroid/hardware/Sensor;)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;

    .prologue
    .line 130
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const-wide/16 v8, 0x3e8

    .line 114
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->disposeExpiredCaches()V

    .line 122
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v4

    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    sub-long v0, v4, v6

    .line 123
    .local v0, "diff":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    mul-long/2addr v4, v8

    div-long v6, v0, v8

    sub-long v2, v4, v6

    .line 124
    .local v2, "utcTimeUsec":J
    iget-object v4, p0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->mAirPressureSet:Ljava/util/TreeSet;

    new-instance v5, Landroid/util/Pair;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 125
    return-void
.end method
