.class public Lcom/samsung/android/providers/context/util/ContextLogger;
.super Ljava/lang/Object;
.source "ContextLogger.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertStringToTime(Ljava/lang/String;)J
    .locals 7
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 55
    const-wide/16 v2, 0x0

    .line 57
    .local v2, "time":J
    const-string v6, ""

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-wide v4, v2

    .line 69
    .end local v2    # "time":J
    .local v4, "time":J
    :goto_0
    return-wide v4

    .line 61
    .end local v4    # "time":J
    .restart local v2    # "time":J
    :cond_0
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v0

    .line 64
    .local v0, "dateFormat":Ljava/text/DateFormat;
    :try_start_0
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    :goto_1
    move-wide v4, v2

    .line 69
    .end local v2    # "time":J
    .restart local v4    # "time":J
    goto :goto_0

    .line 65
    .end local v4    # "time":J
    .restart local v2    # "time":J
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1
.end method

.method public static logLocalProvider(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Z
    .locals 4
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "row"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 35
    .local v0, "client":Landroid/content/ContentProviderClient;
    if-nez v0, :cond_0

    .line 51
    :goto_0
    return v3

    .line 39
    :cond_0
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v2

    .line 40
    .local v2, "provider":Landroid/content/ContentProvider;
    if-nez v2, :cond_1

    .line 41
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0

    .line 45
    :cond_1
    :try_start_0
    invoke-virtual {v2, p1, p2}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_1
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 51
    const/4 v3, 0x1

    goto :goto_0

    .line 46
    :catch_0
    move-exception v1

    .line 47
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
