.class public Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
.super Ljava/lang/Object;
.source "SamsungAccountBinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mISAService:Lcom/msc/sa/aidl/ISAService;

.field private mRegistrationCode:Ljava/lang/String;

.field private mRequestID:I

.field private mSACallback:Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;

.field private final mServiceConnection:Landroid/content/ServiceConnection;

.field private requestCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mSACallback:Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRequestID:I

    .line 65
    iput-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRegistrationCode:Ljava/lang/String;

    .line 93
    new-instance v0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;-><init>(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 78
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    const-string v1, "Construct SamsungAccountBinder. Starting AIDL Service"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mContext:Landroid/content/Context;

    .line 80
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->init()V

    .line 81
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
    .param p1, "x1"    # Lcom/msc/sa/aidl/ISAService;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mISAService:Lcom/msc/sa/aidl/ISAService;

    return-object p1
.end method

.method static synthetic access$202(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;)Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
    .param p1, "x1"    # Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mSACallback:Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->registerCallback()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestAccessToken()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestCount:I

    return v0
.end method

.method static synthetic access$508(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestCount:I

    return v0
.end method

.method private registerCallback()Z
    .locals 7

    .prologue
    .line 116
    :try_start_0
    const-string v1, "ta858j789y"

    .line 117
    .local v1, "serviceAPPClientID":Ljava/lang/String;
    const-string v3, "E906464D83D15AD7DA89EB3DE44748FF"

    .line 118
    .local v3, "serviceAPPClientSecret":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/ContextApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 121
    .local v2, "serviceAPPClientPackageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mISAService:Lcom/msc/sa/aidl/ISAService;

    iget-object v5, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mSACallback:Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;

    invoke-interface {v4, v1, v3, v2, v5}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRegistrationCode:Ljava/lang/String;

    .line 124
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRegistrationCode:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 125
    sget-object v4, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    const-string v5, "RegisterCallback fail!! Please Check SamsungAccount log. "

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    const/4 v4, 0x0

    .line 134
    .end local v1    # "serviceAPPClientID":Ljava/lang/String;
    .end local v2    # "serviceAPPClientPackageName":Ljava/lang/String;
    .end local v3    # "serviceAPPClientSecret":Ljava/lang/String;
    :goto_0
    return v4

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 131
    sget-object v4, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RegisterCallback RemoteException Error : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    sget-object v4, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RegisterCallback success!! mRegistrationCode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRegistrationCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private requestAccessToken()V
    .locals 9

    .prologue
    .line 153
    iget-object v6, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 154
    .local v0, "accMgr":Landroid/accounts/AccountManager;
    const-string v6, "com.osp.app.signin"

    invoke-virtual {v0, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 155
    .local v1, "accountArr":[Landroid/accounts/Account;
    const/4 v6, 0x2

    new-array v2, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "user_id"

    aput-object v7, v2, v6

    const/4 v6, 0x1

    const-string v7, "login_id"

    aput-object v7, v2, v6

    .line 157
    .local v2, "additionalData":[Ljava/lang/String;
    array-length v6, v1

    if-lez v6, :cond_1

    .line 159
    :try_start_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 160
    .local v3, "data":Landroid/os/Bundle;
    const-string v6, "additional"

    invoke-virtual {v3, v6, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 162
    iget-object v6, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mISAService:Lcom/msc/sa/aidl/ISAService;

    iget v7, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRequestID:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRequestID:I

    iget-object v8, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v6, v7, v8, v3}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 163
    .local v5, "result":Ljava/lang/Boolean;
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_0

    .line 164
    sget-object v6, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    const-string v7, "Request AccessToken Failed"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    .end local v3    # "data":Landroid/os/Bundle;
    .end local v5    # "result":Ljava/lang/Boolean;
    :goto_0
    return-void

    .line 166
    .restart local v3    # "data":Landroid/os/Bundle;
    .restart local v5    # "result":Ljava/lang/Boolean;
    :cond_0
    sget-object v6, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    const-string v7, "Request AccessToken Success"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 168
    .end local v3    # "data":Landroid/os/Bundle;
    .end local v5    # "result":Ljava/lang/Boolean;
    :catch_0
    move-exception v4

    .line 169
    .local v4, "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    .line 170
    sget-object v6, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Request AccessToken exception error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 173
    .end local v4    # "e":Landroid/os/RemoteException;
    :cond_1
    sget-object v6, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    const-string v7, "Not exist samsung account"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private unRegisterCallback()V
    .locals 5

    .prologue
    .line 139
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mISAService:Lcom/msc/sa/aidl/ISAService;

    iget-object v3, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 141
    .local v1, "result":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 142
    sget-object v2, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    const-string v3, "RegisterCallback fail!! Please Check SamsungAccount log. "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    .end local v1    # "result":Ljava/lang/Boolean;
    :cond_0
    :goto_0
    return-void

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 147
    sget-object v2, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UnRegisterCallback RemoteException Error : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public bindService()V
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 91
    return-void
.end method

.method public init()V
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    const-string v1, "Initialized the AccountInfoBinder"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestCount:I

    .line 86
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->bindService()V

    .line 87
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 71
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->unRegisterCallback()V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 75
    return-void
.end method
