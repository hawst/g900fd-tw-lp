.class public abstract Lcom/samsung/android/providers/context/log/BaseLogProvider;
.super Landroid/content/ContentProvider;
.source "BaseLogProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;
    }
.end annotation


# instance fields
.field protected final mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

.field private mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method protected constructor <init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V
    .locals 1
    .param p1, "staticProperties"    # Lcom/samsung/android/providers/context/log/LogTableProperty;

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 75
    iput-object p1, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    .line 76
    return-void
.end method

.method protected static deleteOldRecords(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)I
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "recordsCountPerDeletion"    # I

    .prologue
    .line 420
    const/4 v1, 0x0

    .line 423
    .local v1, "result":I
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id < (SELECT MIN(_id) FROM "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") + "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 424
    .local v2, "whereClause":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 426
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v3, :cond_0

    .line 427
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " DB count reached to maximum. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " old records are deleted."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    .end local v2    # "whereClause":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected static fillCommonColumns(Landroid/content/ContentValues;)V
    .locals 14
    .param p0, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 506
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 507
    .local v0, "currentTime":Ljava/util/Date;
    new-instance v9, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 509
    .local v9, "timestampUtc":Ljava/lang/String;
    const-string v10, "timestamp_utc"

    invoke-virtual {p0, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    const-string v10, "timestamp"

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 511
    const-string v10, "time_zone"

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v11

    iget-object v11, v11, Lcom/samsung/android/providers/context/GlobalStatus;->timeZone:Ljava/lang/String;

    invoke-virtual {p0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    const-wide/high16 v4, -0x3f97000000000000L    # -200.0

    .line 515
    .local v4, "longitude":D
    const-wide/high16 v2, -0x3f97000000000000L    # -200.0

    .line 517
    .local v2, "latitude":D
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v10, v11}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 518
    invoke-static {}, Lcom/samsung/android/providers/context/status/LocationTracker;->getInstance()Lcom/samsung/android/providers/context/status/LocationTracker;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/status/LocationTracker;->getLatestLocation()Lcom/samsung/android/providers/context/status/LocationInfo;

    move-result-object v1

    .line 519
    .local v1, "locatinoInfo":Lcom/samsung/android/providers/context/status/LocationInfo;
    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/LocationInfo;->isValid()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 520
    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/LocationInfo;->getLongitude()D

    move-result-wide v4

    .line 521
    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/LocationInfo;->getLatitude()D

    move-result-wide v2

    .line 525
    .end local v1    # "locatinoInfo":Lcom/samsung/android/providers/context/status/LocationInfo;
    :cond_0
    const-string v10, "latitude"

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 526
    const-string v10, "longitude"

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 529
    const-string v8, ""

    .line 530
    .local v8, "placeName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 532
    .local v6, "placeCategory":I
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v10, v11}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 533
    invoke-static {}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getCurrentMyPlaceInfo()Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    move-result-object v7

    .line 534
    .local v7, "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    invoke-virtual {v7}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getName()Ljava/lang/String;

    move-result-object v8

    .line 535
    invoke-virtual {v7}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getCategory()I

    move-result v6

    .line 538
    .end local v7    # "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    :cond_1
    const-string v10, "place_name"

    invoke-virtual {p0, v10, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    const-string v10, "place_category"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 541
    return-void
.end method

.method private static getDbHelper()Lcom/samsung/android/providers/context/log/db/LogDbHelper;
    .locals 1

    .prologue
    .line 388
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getLogDatabase()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v0

    return-object v0
.end method

.method protected static getProviderName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "tableName"    # Ljava/lang/String;

    .prologue
    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.samsung.android.providers.context.log"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getTotalCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 416
    invoke-static {p0, p1}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private declared-synchronized getUriMatcher()Landroid/content/UriMatcher;
    .locals 4

    .prologue
    .line 392
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mUriMatcher:Landroid/content/UriMatcher;

    if-nez v1, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getProviderName()Ljava/lang/String;

    move-result-object v0

    .line 395
    .local v0, "provider":Ljava/lang/String;
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 396
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 397
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v2, "#"

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 400
    .end local v0    # "provider":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mUriMatcher:Landroid/content/UriMatcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 392
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static insertLog(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZZII)Landroid/net/Uri;
    .locals 13
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "useNotify"    # Z
    .param p4, "useBroadcast"    # Z
    .param p5, "recordsCountPerDeletion"    # I
    .param p6, "maxRecordsCount"    # I

    .prologue
    .line 455
    const-wide/16 v8, -0x1

    .line 456
    .local v8, "rowId":J
    const/4 v7, 0x0

    .line 458
    .local v7, "newUri":Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getDbHelper()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v10

    if-nez v10, :cond_0

    .line 459
    const/4 v10, 0x0

    .line 500
    :goto_0
    return-object v10

    .line 462
    :cond_0
    invoke-static {}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getDbHelper()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 463
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v3, :cond_1

    .line 464
    const/4 v10, 0x0

    goto :goto_0

    .line 468
    :cond_1
    const/4 v10, 0x0

    :try_start_0
    invoke-virtual {v3, p2, v10, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 469
    const-wide/16 v10, -0x1

    cmp-long v10, v8, v10

    if-eqz v10, :cond_2

    .line 470
    invoke-static {p0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    .line 471
    if-eqz p3, :cond_2

    .line 472
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v7, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    :cond_2
    :goto_1
    if-eqz p4, :cond_4

    .line 480
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    .line 481
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 482
    .local v2, "broadcastIntent":Landroid/content/Intent;
    invoke-static {p2}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getProviderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 483
    const-string v10, "type"

    invoke-virtual {v2, v10, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 484
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v10

    iget-boolean v10, v10, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v10, :cond_3

    .line 485
    const-string v10, "ContextFramework"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Intent broadcast table name = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    :cond_3
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v10

    const-string v11, "com.samsung.android.providers.context.permission.READ_LOG"

    invoke-virtual {v10, v2, v11}, Lcom/samsung/android/providers/context/ContextApplication;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 493
    .end local v2    # "broadcastIntent":Landroid/content/Intent;
    :cond_4
    if-lez p5, :cond_5

    .line 494
    invoke-static {v3, p2}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getTotalCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v4

    .line 495
    .local v4, "dbCount":J
    move/from16 v0, p6

    int-to-long v10, v0

    cmp-long v10, v10, v4

    if-gez v10, :cond_5

    .line 496
    move/from16 v0, p5

    invoke-static {v3, p2, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->deleteOldRecords(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)I

    .end local v4    # "dbCount":J
    :cond_5
    move-object v10, v7

    .line 500
    goto :goto_0

    .line 475
    :catch_0
    move-exception v6

    .line 476
    .local v6, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1
.end method

.method protected static insertLogWithHandler(Landroid/os/Handler;ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZZII)V
    .locals 4
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "msgId"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "tableName"    # Ljava/lang/String;
    .param p5, "useNotify"    # Z
    .param p6, "useBroadcast"    # Z
    .param p7, "recordsCountPerDeletion"    # I
    .param p8, "maxRecordsCount"    # I

    .prologue
    .line 438
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 439
    .local v0, "bundle":Landroid/os/Bundle;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, p3}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 440
    .local v1, "clonedValues":Landroid/content/ContentValues;
    const-string v3, "Uri"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 441
    const-string v3, "Values"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 442
    const-string v3, "TableName"

    invoke-virtual {v0, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v3, "UseNotify"

    invoke-virtual {v0, v3, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 444
    const-string v3, "UseBroadcast"

    invoke-virtual {v0, v3, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 445
    const-string v3, "RecordsCount"

    invoke-virtual {v0, v3, p7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 446
    const-string v3, "MaxRecordsCount"

    invoke-virtual {v0, v3, p8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 448
    invoke-virtual {p0, p1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 449
    .local v2, "msg":Landroid/os/Message;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 450
    invoke-virtual {p0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 451
    return-void
.end method

.method public static queryForInternal(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "itemType"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "targetPackageId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 248
    if-eqz p6, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-object v7

    .line 252
    :cond_1
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getTableName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getTableName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    const/4 v6, 0x0

    .line 257
    .local v6, "c":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 259
    .local v3, "privacySelection":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v0

    invoke-virtual {v0, p6, p0, p3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getPrivacySelection(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 263
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getTableName()Ljava/lang/String;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->queryLog(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 265
    if-eqz v6, :cond_0

    .line 269
    new-instance v7, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;

    invoke-direct {v7, v6, p6}, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 270
    .local v7, "cw":Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calling package: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cursor count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", privacy item: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static queryLog(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p0, "tableName"    # Ljava/lang/String;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 282
    invoke-static {}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getDbHelper()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v2

    if-nez v2, :cond_0

    .line 283
    const/4 v9, 0x0

    .line 315
    :goto_0
    return-object v9

    .line 286
    :cond_0
    invoke-static {}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getDbHelper()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 287
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v1, :cond_1

    .line 288
    const/4 v9, 0x0

    goto :goto_0

    .line 291
    :cond_1
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 292
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual {v0, p0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 294
    const/4 v9, 0x0

    .line 297
    .local v9, "c":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 298
    .local v8, "limit":Ljava/lang/String;
    :try_start_0
    const-string v2, "limit"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v11

    .line 299
    .local v11, "limitParam":Ljava/lang/String;
    if-eqz v11, :cond_2

    .line 301
    :try_start_1
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 303
    .local v12, "userLimit":I
    const/4 v2, 0x1

    if-lt v12, v2, :cond_2

    .line 304
    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 310
    .end local v12    # "userLimit":I
    :cond_2
    :goto_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v7, p5

    :try_start_2
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_0

    .line 306
    :catch_0
    move-exception v10

    .line 307
    .local v10, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v10}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 311
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    .end local v11    # "limitParam":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 312
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "ContextFramework"

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 173
    invoke-virtual {p1}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 174
    .local v2, "keyset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/Vector;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/Vector;-><init>(I)V

    .line 176
    .local v4, "toRemoveKeyName":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 177
    .local v1, "keyName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v5, v5, Lcom/samsung/android/providers/context/log/LogTableProperty;->validFieldList:[Ljava/lang/String;

    invoke-static {v5, v1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-gez v5, :cond_0

    .line 178
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v5

    iget-boolean v5, v5, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v5, :cond_1

    .line 179
    const-string v5, "ContextFramework"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FILTERLOG : wrong column "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v7, v7, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_1
    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 186
    .end local v1    # "keyName":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 187
    .local v3, "removeName":Ljava/lang/String;
    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_1

    .line 190
    .end local v3    # "removeName":Ljava/lang/String;
    :cond_3
    return-object p1
.end method

.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 1
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 87
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "delete() is NOT supported for URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v0, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getProviderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getUriType(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 96
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Called getType() with invalid URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "vnd.android.cursor.item/vnd.com.samsung.android.providers.context.log."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v1, v1, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getUriType(Landroid/net/Uri;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getUriMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 20
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 106
    const/16 v19, 0x0

    .line 109
    .local v19, "newUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getUriType(Landroid/net/Uri;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 110
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_0

    .line 111
    const-string v2, "ContextFramework"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Called insert() with invalid URI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v2, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v3, v3, Lcom/samsung/android/providers/context/log/LogTableProperty;->privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 118
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_1

    .line 119
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v17

    .line 120
    .local v17, "caller":Ljava/lang/String;
    const-string v3, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Log insertion denied by privacy for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v17, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "]"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    .end local v17    # "caller":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    .line 165
    :goto_1
    return-object v2

    .line 120
    .restart local v17    # "caller":Ljava/lang/String;
    :cond_2
    const-string v2, ""

    goto :goto_0

    .line 127
    .end local v17    # "caller":Ljava/lang/String;
    :cond_3
    if-nez p2, :cond_4

    .line 128
    const-string v2, "ContextFramework"

    const-string v3, "Value is NULL."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v2, 0x0

    goto :goto_1

    .line 132
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object p2

    .line 135
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->checkBoundaryValidity(Landroid/content/ContentValues;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 136
    const-string v2, "ContextFramework"

    const-string v3, "ValidBoundary Check Fail."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v2, 0x0

    goto :goto_1

    .line 141
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->fillCommonColumnsManually:Z

    if-nez v2, :cond_6

    .line 142
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->fillCommonColumns(Landroid/content/ContentValues;)V

    .line 145
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->setVerification(Landroid/content/ContentValues;)V

    .line 147
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/log/LogTableProperty;->needToCheckMaxRecords()I

    move-result v7

    .line 149
    .local v7, "needToCheckMaxRecords":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    if-eqz v2, :cond_7

    .line 152
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v4, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v5, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v6, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->useBroadcast:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget v8, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v8}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->insertLog(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZZII)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    :goto_2
    move-object/from16 v2, v19

    .line 165
    goto :goto_1

    .line 154
    :catch_0
    move-exception v18

    .line 155
    .local v18, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_2

    .line 159
    .end local v18    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_7
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v8

    .line 161
    .local v8, "logHandler":Landroid/os/Handler;
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v12, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v13, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v14, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->useBroadcast:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget v0, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    move/from16 v16, v0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move v15, v7

    invoke-static/range {v8 .. v16}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->insertLogWithHandler(Landroid/os/Handler;ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZZII)V

    goto :goto_2
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v0, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->validFieldList:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 197
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getUriType(Landroid/net/Uri;)I

    move-result v8

    .line 203
    .local v8, "type":I
    const/4 v0, 0x2

    if-ne v8, v0, :cond_4

    .line 204
    if-eqz p3, :cond_3

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 215
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 218
    .local v3, "privacySelection":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v0, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-eqz v0, :cond_5

    .line 219
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v2, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v1, v2, p3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getPrivacySelection(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 226
    :goto_1
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_1

    .line 227
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Modified selection : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v0, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->queryLog(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 233
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_6

    iget-object v0, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v0, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-eqz v0, :cond_6

    .line 234
    new-instance v7, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v6, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 235
    .local v7, "cw":Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_2

    .line 236
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calling Package (# of cursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", privacy item : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v2, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    .end local v7    # "cw":Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;
    :cond_2
    :goto_2
    return-object v7

    .line 207
    .end local v3    # "privacySelection":Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_0

    .line 209
    :cond_4
    const/4 v0, 0x1

    if-eq v8, v0, :cond_0

    .line 210
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_0

    .line 211
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Called query() with invalid URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 224
    .restart local v3    # "privacySelection":Ljava/lang/String;
    :cond_5
    move-object v3, p3

    goto/16 :goto_1

    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_6
    move-object v7, v6

    .line 243
    goto :goto_2
.end method

.method protected setVerification(Landroid/content/ContentValues;)V
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 545
    const-string v0, "verification"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 546
    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 320
    const/4 v1, 0x0

    .line 322
    .local v1, "count":I
    iget-object v6, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/log/LogTableProperty;->supportUpdate:Z

    if-nez v6, :cond_1

    .line 323
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_0

    .line 324
    const-string v6, "ContextFramework"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "update() is NOT supported for URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_0
    :goto_0
    return v5

    .line 329
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getUriType(Landroid/net/Uri;)I

    move-result v6

    const/4 v7, 0x2

    if-eq v6, v7, :cond_2

    .line 330
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_0

    .line 331
    const-string v6, "ContextFramework"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Called update() with invalid URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 336
    :cond_2
    if-eqz p3, :cond_3

    .line 337
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_0

    .line 338
    const-string v6, "ContextFramework"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "update() is NOT supported with select : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 342
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 346
    iget-object v6, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/log/LogTableProperty;->fillCommonColumnsManually:Z

    if-nez v6, :cond_4

    .line 347
    invoke-static {p2}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->fillCommonColumns(Landroid/content/ContentValues;)V

    .line 350
    :cond_4
    invoke-static {}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getDbHelper()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 354
    invoke-static {}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getDbHelper()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 355
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v2, :cond_0

    .line 360
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v4, v5, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    .line 361
    .local v4, "tableName":Ljava/lang/String;
    invoke-virtual {v2, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 363
    if-lez v1, :cond_6

    .line 364
    iget-object v5, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v5, v5, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    if-eqz v5, :cond_5

    .line 365
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 368
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v5, v5, Lcom/samsung/android/providers/context/log/LogTableProperty;->useUpdateBroadcast:Z

    if-eqz v5, :cond_6

    .line 369
    const-string v5, ""

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 370
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 371
    .local v0, "broadcastIntent":Landroid/content/Intent;
    invoke-static {v4}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->getProviderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    const-string v5, "type"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v5

    const-string v6, "com.samsung.android.providers.context.permission.READ_LOG"

    invoke-virtual {v5, v0, v6}, Lcom/samsung/android/providers/context/ContextApplication;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 374
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v5

    iget-boolean v5, v5, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v5, :cond_6

    .line 375
    const-string v5, "ContextFramework"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Intent broadcast table name = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v4    # "tableName":Ljava/lang/String;
    :cond_6
    :goto_1
    move v5, v1

    .line 384
    goto/16 :goto_0

    .line 380
    :catch_0
    move-exception v3

    .line 381
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1
.end method
