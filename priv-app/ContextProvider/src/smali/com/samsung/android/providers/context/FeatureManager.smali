.class public Lcom/samsung/android/providers/context/FeatureManager;
.super Ljava/lang/Object;
.source "FeatureManager.java"


# static fields
.field private static final sFeatureList:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 59
    const/16 v1, 0x8

    new-array v1, v1, [Z

    sput-object v1, Lcom/samsung/android/providers/context/FeatureManager;->sFeatureList:[Z

    .line 62
    sget-object v1, Lcom/samsung/android/providers/context/FeatureManager;->sFeatureList:[Z

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([ZZ)V

    .line 66
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v1

    const-string v2, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v1, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 67
    const-string v1, "ContextFramework"

    const-string v2, "FeatureManager: survey logging is disabled"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    sget-object v1, Lcom/samsung/android/providers/context/FeatureManager;->sFeatureList:[Z

    const/4 v2, 0x6

    const/4 v3, 0x0

    aput-boolean v3, v1, v2

    .line 73
    :goto_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_ContextService_ConfigLogging"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "cscLogging":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 76
    sget-object v1, Lcom/samsung/android/providers/context/FeatureManager;->sFeatureList:[Z

    invoke-static {v1, v0}, Lcom/samsung/android/providers/context/FeatureManager;->updateFeatureListFromString([ZLjava/lang/String;)V

    .line 80
    :cond_0
    return-void

    .line 70
    .end local v0    # "cscLogging":Ljava/lang/String;
    :cond_1
    const-string v1, "ContextFramework"

    const-string v2, "FeatureManager: survey logging is enabled"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isSupportedFeature(S)Z
    .locals 1
    .param p0, "feature"    # S

    .prologue
    .line 93
    if-ltz p0, :cond_0

    const/16 v0, 0x8

    if-lt p0, v0, :cond_1

    .line 94
    :cond_0
    const/4 v0, 0x0

    .line 97
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/samsung/android/providers/context/FeatureManager;->sFeatureList:[Z

    aget-boolean v0, v0, p0

    goto :goto_0
.end method

.method private static supportSContext(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 112
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 113
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v3, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 114
    const-string v3, "ContextFramework"

    const-string v4, "FeatureManager: This device does not have the SContext"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :goto_0
    return v2

    .line 118
    :cond_0
    const-string v3, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v1

    .line 119
    .local v1, "version":I
    const/4 v3, 0x7

    if-ge v1, v3, :cond_1

    .line 120
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FeatureManager: This device does not support SContext, version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 124
    :cond_1
    const-string v2, "ContextFramework"

    const-string v3, "FeatureManager: This device supports the SContext version 7"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static supportSLocation(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 129
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v2, v3, :cond_0

    .line 142
    :goto_0
    return v1

    .line 135
    :cond_0
    :try_start_0
    const-string v2, "com.samsung.location.SLocationParameter"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    const-string v1, "ContextFramework"

    const-string v2, "FeatureManager: This device supports SLocation"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v1, 0x1

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v2, "ContextFramework"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FeatureManager: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static updateFeature(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-static {p0}, Lcom/samsung/android/providers/context/FeatureManager;->supportSContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    sget-object v0, Lcom/samsung/android/providers/context/FeatureManager;->sFeatureList:[Z

    const/4 v1, 0x4

    aput-boolean v2, v0, v1

    .line 87
    :cond_0
    invoke-static {p0}, Lcom/samsung/android/providers/context/FeatureManager;->supportSLocation(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    sget-object v0, Lcom/samsung/android/providers/context/FeatureManager;->sFeatureList:[Z

    aput-boolean v2, v0, v2

    .line 90
    :cond_1
    return-void
.end method

.method private static updateFeatureListFromString([ZLjava/lang/String;)V
    .locals 7
    .param p0, "featureList"    # [Z
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 147
    const-string v4, ", "

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 148
    .local v3, "token":Ljava/lang/String;
    const-string v4, "-location"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 149
    aput-boolean v6, p0, v6

    .line 150
    const/4 v4, 0x2

    aput-boolean v6, p0, v4

    .line 151
    const/4 v4, 0x4

    aput-boolean v6, p0, v4

    .line 154
    :cond_0
    const-string v4, "-server_upload"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 155
    const/4 v4, 0x3

    aput-boolean v6, p0, v4

    .line 158
    :cond_1
    const-string v4, "-survey"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 159
    sget-object v4, Lcom/samsung/android/providers/context/FeatureManager;->sFeatureList:[Z

    const/4 v5, 0x6

    aput-boolean v6, v4, v5

    .line 147
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    .end local v3    # "token":Ljava/lang/String;
    :cond_3
    return-void
.end method
