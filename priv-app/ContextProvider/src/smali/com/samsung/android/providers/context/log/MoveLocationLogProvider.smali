.class public Lcom/samsung/android/providers/context/log/MoveLocationLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "MoveLocationLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 35
    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "timestamp_utc"

    aput-object v3, v1, v2

    const-string v2, "timestamp"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "time_zone"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "place_name"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "place_category"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "longitude"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "latitude"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "air_pressure"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "accuracy"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/MoveLocationLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "move_location"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/MoveLocationLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 47
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->fillCommonColumnsManually:Z

    .line 49
    new-instance v1, Lcom/samsung/android/providers/context/log/MoveLocationLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/MoveLocationLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/MoveLocationLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 88
    sget-object v1, Lcom/samsung/android/providers/context/log/MoveLocationLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 89
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/samsung/android/providers/context/log/MoveLocationLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 141
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 12
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 93
    const-wide/high16 v8, -0x3f97000000000000L    # -200.0

    .line 94
    .local v8, "longitude":D
    const-wide/high16 v6, -0x3f97000000000000L    # -200.0

    .line 95
    .local v6, "latitude":D
    const/4 v1, 0x0

    .line 96
    .local v1, "air_pressure":F
    const/4 v0, 0x0

    .line 98
    .local v0, "accuracy":F
    const-string v10, "longitude"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    .line 99
    .local v3, "doubleLongitude":Ljava/lang/Double;
    const-string v10, "latitude"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    .line 100
    .local v2, "doubleLatitude":Ljava/lang/Double;
    const-string v10, "air_pressure"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    .line 101
    .local v5, "floatAirPressure":Ljava/lang/Float;
    const-string v10, "accuracy"

    invoke-virtual {p1, v10}, Landroid/content/ContentValues;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    .line 103
    .local v4, "floatAccuracy":Ljava/lang/Float;
    if-eqz v3, :cond_0

    .line 104
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    .line 107
    :cond_0
    if-eqz v2, :cond_1

    .line 108
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 111
    :cond_1
    if-eqz v5, :cond_2

    .line 112
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 115
    :cond_2
    if-eqz v4, :cond_3

    .line 116
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 119
    :cond_3
    const-wide v10, -0x3f99800000000000L    # -180.0

    cmpg-double v10, v8, v10

    if-ltz v10, :cond_4

    const-wide v10, 0x4066800000000000L    # 180.0

    cmpl-double v10, v8, v10

    if-lez v10, :cond_5

    .line 120
    :cond_4
    const-string v10, "ContextFramework"

    const-string v11, "MoveLocation : longitude is invalid"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_5
    const-wide v10, -0x3fa9800000000000L    # -90.0

    cmpg-double v10, v6, v10

    if-ltz v10, :cond_6

    const-wide v10, 0x4056800000000000L    # 90.0

    cmpl-double v10, v6, v10

    if-lez v10, :cond_7

    .line 123
    :cond_6
    const-string v10, "ContextFramework"

    const-string v11, "MoveLocation : latitude is invalid"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_7
    const/high16 v10, -0x40800000    # -1.0f

    cmpl-float v10, v1, v10

    if-nez v10, :cond_8

    .line 126
    const-string v10, "ContextFramework"

    const-string v11, "MoveLocation : air_pressure is invalid"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_8
    const/4 v10, 0x0

    cmpg-float v10, v0, v10

    if-gtz v10, :cond_9

    .line 129
    const-string v10, "ContextFramework"

    const-string v11, "MoveLocation : accuracy is invalid"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_9
    const/4 v10, 0x1

    return v10
.end method
