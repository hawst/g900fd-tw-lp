.class Lcom/samsung/android/providers/context/ContextService$ServiceHandler;
.super Landroid/os/Handler;
.source "ContextService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/ContextService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ServiceHandler"
.end annotation


# instance fields
.field private final mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/providers/context/ContextService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/ContextService;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/android/providers/context/ContextService;

    .prologue
    .line 238
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 239
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextService$ServiceHandler;->mService:Ljava/lang/ref/WeakReference;

    .line 240
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 244
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextService$ServiceHandler;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/ContextService;

    .line 245
    .local v0, "service":Lcom/samsung/android/providers/context/ContextService;
    if-eqz v0, :cond_0

    .line 246
    iget v1, p1, Landroid/os/Message;->what:I

    .line 248
    # getter for: Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;
    invoke-static {v0}, Lcom/samsung/android/providers/context/ContextService;->access$300(Lcom/samsung/android/providers/context/ContextService;)Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->handleMessage(Landroid/os/Message;)V

    .line 252
    :cond_0
    return-void
.end method
