.class Lcom/samsung/android/providers/context/ContextService$2;
.super Landroid/content/BroadcastReceiver;
.source "ContextService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/ContextService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/ContextService;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/ContextService;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/android/providers/context/ContextService$2;->this$0:Lcom/samsung/android/providers/context/ContextService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 96
    if-nez p2, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 102
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextService$2;->this$0:Lcom/samsung/android/providers/context/ContextService;

    # getter for: Lcom/samsung/android/providers/context/ContextService;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
    invoke-static {v1}, Lcom/samsung/android/providers/context/ContextService;->access$100(Lcom/samsung/android/providers/context/ContextService;)Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->onUserForeground()V

    .line 103
    invoke-static {}, Lcom/samsung/android/providers/context/status/WifiMonitor;->getInstance()Lcom/samsung/android/providers/context/status/WifiMonitor;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/providers/context/status/WifiMonitor;->enable(Landroid/content/Context;)V

    .line 104
    invoke-static {}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->getInstance()Lcom/samsung/android/providers/context/status/RoamingMonitor;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->enable(Landroid/content/Context;)V

    .line 105
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/samsung/android/providers/context/GlobalStatus;->foreground:Z

    .line 106
    const-string v1, "ContextFramework"

    const-string v2, "ContextService: handling foreground request"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 107
    :cond_2
    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/samsung/android/providers/context/GlobalStatus;->foreground:Z

    .line 109
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextService$2;->this$0:Lcom/samsung/android/providers/context/ContextService;

    # getter for: Lcom/samsung/android/providers/context/ContextService;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
    invoke-static {v1}, Lcom/samsung/android/providers/context/ContextService;->access$100(Lcom/samsung/android/providers/context/ContextService;)Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->onUserBackground()V

    .line 110
    invoke-static {}, Lcom/samsung/android/providers/context/status/WifiMonitor;->getInstance()Lcom/samsung/android/providers/context/status/WifiMonitor;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/providers/context/status/WifiMonitor;->disable(Landroid/content/Context;)V

    .line 111
    invoke-static {}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->getInstance()Lcom/samsung/android/providers/context/status/RoamingMonitor;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->disable(Landroid/content/Context;)V

    .line 112
    const-string v1, "ContextFramework"

    const-string v2, "ContextService: stop handling foreground request"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
