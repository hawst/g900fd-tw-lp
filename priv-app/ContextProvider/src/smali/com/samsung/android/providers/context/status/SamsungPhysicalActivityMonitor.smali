.class public Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;
.super Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;
.source "SamsungPhysicalActivityMonitor.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# instance fields
.field private mIsEnabled:Z

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mIsEnabled:Z

    .line 47
    return-void
.end method

.method private insertActivityLog([J[I[I)V
    .locals 12
    .param p1, "timeStampList"    # [J
    .param p2, "activityList"    # [I
    .param p3, "accuracyList"    # [I

    .prologue
    .line 91
    const/16 v0, 0x21

    .line 94
    .local v0, "ACCURACY_NORMALIZED_FACTOR":I
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 95
    :cond_0
    const-string v8, "ContextFramework.SamsungPhysicalActivityMonitor"

    const-string v9, "Activity log data is null"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :goto_0
    return-void

    .line 99
    :cond_1
    array-length v5, p1

    .line 102
    .local v5, "logSize":I
    if-eqz v5, :cond_2

    array-length v8, p2

    if-ne v5, v8, :cond_2

    array-length v8, p3

    if-eq v5, v8, :cond_3

    .line 103
    :cond_2
    const-string v8, "ContextFramework.SamsungPhysicalActivityMonitor"

    const-string v9, "Acitivity log data is corrupted"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 108
    :cond_3
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 110
    .local v1, "cr":Landroid/content/ContentResolver;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v2

    .line 111
    .local v2, "dateFormat":Ljava/text/DateFormat;
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 113
    .local v6, "logTime":Ljava/util/Date;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 115
    .local v7, "row":Landroid/content/ContentValues;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v5, :cond_4

    .line 117
    aget-wide v8, p1, v4

    invoke-virtual {v6, v8, v9}, Ljava/util/Date;->setTime(J)V

    .line 118
    const-string v8, "timestamp_utc"

    invoke-virtual {v2, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v8, "timestamp"

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 120
    const-string v8, "time_zone"

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/providers/context/GlobalStatus;->timeZone:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v8, "longitude"

    const/16 v9, -0xc8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    const-string v8, "latitude"

    const/16 v9, -0xc8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 123
    const-string v8, "type"

    aget v9, p2, v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 124
    const-string v8, "accuracy"

    aget v9, p3, v4

    add-int/lit8 v9, v9, 0x1

    mul-int/lit8 v9, v9, 0x21

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 125
    const-string v8, "place_name"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v8, "place_category"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :try_start_0
    sget-object v8, Lcom/samsung/android/providers/context/log/ContextLogContract$ChangeActivity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v8, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_2
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 115
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 130
    :catch_0
    move-exception v3

    .line 131
    .local v3, "ex":Ljava/lang/Exception;
    const-string v8, "ContextFramework.SamsungPhysicalActivityMonitor"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 137
    .end local v3    # "ex":Ljava/lang/Exception;
    :cond_4
    const-string v8, "ContextFramework.SamsungPhysicalActivityMonitor"

    const-string v9, "Logging Activity batching data is completed"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized disable()V
    .locals 2

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mIsEnabled:Z

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x1a

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mIsEnabled:Z

    .line 72
    const-string v0, "ContextFramework.SamsungPhysicalActivityMonitor"

    const-string v1, "Activity monitoring is disabled"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :cond_0
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized enable()V
    .locals 4

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mIsEnabled:Z

    if-nez v1, :cond_0

    .line 52
    const/4 v1, 0x4

    invoke-static {v1}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 54
    :try_start_1
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    const-string v2, "scontext"

    invoke-virtual {v1, v2}, Lcom/samsung/android/providers/context/ContextApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/scontext/SContextManager;

    iput-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 56
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/16 v2, 0x1a

    invoke-virtual {v1, p0, v2}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mIsEnabled:Z

    .line 57
    const-string v2, "ContextFramework.SamsungPhysicalActivityMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enabling activity monitoring with SContext ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v1, p0, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->mIsEnabled:Z

    if-eqz v1, :cond_1

    const-string v1, "SUCCESS)"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 57
    :cond_1
    :try_start_2
    const-string v1, "FAIL)"
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "ex":Ljava/lang/NullPointerException;
    :try_start_3
    const-string v1, "ContextFramework.SamsungPhysicalActivityMonitor"

    const-string v2, "Getting SContextManager fails"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 51
    .end local v0    # "ex":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 62
    :cond_2
    :try_start_4
    const-string v1, "ContextFramework.SamsungPhysicalActivityMonitor"

    const-string v2, "SContext is not supported"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    .line 78
    iget-object v1, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    .line 79
    .local v1, "scontext":Landroid/hardware/scontext/SContext;
    const-string v2, "ContextFramework.SamsungPhysicalActivityMonitor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SContext event is received, type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual {v1}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v2

    const/16 v3, 0x1a

    if-ne v2, v3, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getActivityBatchContext()Landroid/hardware/scontext/SContextActivityBatch;

    move-result-object v0

    .line 83
    .local v0, "loggingContext":Landroid/hardware/scontext/SContextActivityBatch;
    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityBatch;->getTimeStamp()[J

    move-result-object v2

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityBatch;->getStatus()[I

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityBatch;->getAccuracy()[I

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/providers/context/status/SamsungPhysicalActivityMonitor;->insertActivityLog([J[I[I)V

    .line 85
    .end local v0    # "loggingContext":Landroid/hardware/scontext/SContextActivityBatch;
    :cond_0
    return-void
.end method
