.class public abstract Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
.super Ljava/lang/Object;
.source "AbstractDatabaseHandler.java"


# instance fields
.field private final mProviderProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V
    .locals 0
    .param p1, "providerProperties"    # Lcom/samsung/android/providers/context/log/LogTableProperty;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->mProviderProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    .line 21
    return-void
.end method


# virtual methods
.method public abstract createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation
.end method

.method public getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->mProviderProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    return-object v0
.end method

.method final loadExtraInfo(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->mProviderProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->mProviderProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/providers/context/log/LogTableProperty;->loadInsertCount(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 41
    :cond_0
    return-void
.end method

.method public abstract upgradeTable(Landroid/database/sqlite/SQLiteDatabase;II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation
.end method
