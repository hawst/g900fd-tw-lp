.class public Lcom/samsung/android/providers/context/status/WifiMonitor;
.super Landroid/content/BroadcastReceiver;
.source "WifiMonitor.java"


# static fields
.field private static sInstance:Lcom/samsung/android/providers/context/status/WifiMonitor;


# instance fields
.field private mConnected:Z

.field private mIpAddress:Ljava/lang/String;

.field private mIsEnabled:Z

.field private mMacAddress:Ljava/lang/String;

.field private mSsid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/samsung/android/providers/context/status/WifiMonitor;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/WifiMonitor;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/status/WifiMonitor;->sInstance:Lcom/samsung/android/providers/context/status/WifiMonitor;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mIsEnabled:Z

    return-void
.end method

.method private checkWifiStatus(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    .line 84
    const-string v11, "connectivity"

    invoke-virtual {p1, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 85
    .local v2, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v2, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v7

    .line 86
    .local v7, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 87
    .local v5, "isWifiConnected":Z
    :goto_0
    const-string v8, ""

    .line 88
    .local v8, "ssid":Ljava/lang/String;
    const-string v6, ""

    .line 89
    .local v6, "mac":Ljava/lang/String;
    const-string v4, ""

    .line 90
    .local v4, "ip":Ljava/lang/String;
    if-eqz v5, :cond_6

    .line 91
    const-string v11, "wifi"

    invoke-virtual {p1, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/wifi/WifiManager;

    .line 92
    .local v10, "wm":Landroid/net/wifi/WifiManager;
    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v9

    .line 93
    .local v9, "wi":Landroid/net/wifi/WifiInfo;
    if-eqz v9, :cond_2

    .line 94
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/samsung/android/providers/context/status/WifiMonitor;->removeDoubleQuotation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 95
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v6

    .line 97
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v11

    invoke-static {v11}, Lcom/samsung/android/providers/context/status/WifiMonitor;->unpack(I)[B

    move-result-object v1

    .line 100
    .local v1, "bytes":[B
    :try_start_0
    invoke-static {v1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    .line 101
    .local v0, "address":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 106
    .end local v0    # "address":Ljava/net/InetAddress;
    :goto_1
    if-nez v8, :cond_0

    .line 107
    const-string v8, ""

    .line 109
    :cond_0
    if-nez v6, :cond_1

    .line 110
    const-string v6, ""

    .line 112
    :cond_1
    if-nez v4, :cond_2

    .line 113
    const-string v4, ""

    .line 117
    .end local v1    # "bytes":[B
    :cond_2
    iget-boolean v11, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mConnected:Z

    if-ne v11, v5, :cond_3

    iget-object v11, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mSsid:Ljava/lang/String;

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mMacAddress:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mIpAddress:Ljava/lang/String;

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 119
    :cond_3
    invoke-static {p1, v5, v8, v6, v4}, Lcom/samsung/android/providers/context/status/WifiMonitor;->insertWifiInfoLog(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .end local v9    # "wi":Landroid/net/wifi/WifiInfo;
    .end local v10    # "wm":Landroid/net/wifi/WifiManager;
    :cond_4
    :goto_2
    iput-boolean v5, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mConnected:Z

    .line 125
    iput-object v8, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mSsid:Ljava/lang/String;

    .line 126
    iput-object v6, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mMacAddress:Ljava/lang/String;

    .line 127
    iput-object v4, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mIpAddress:Ljava/lang/String;

    .line 128
    return-void

    .line 86
    .end local v4    # "ip":Ljava/lang/String;
    .end local v5    # "isWifiConnected":Z
    .end local v6    # "mac":Ljava/lang/String;
    .end local v8    # "ssid":Ljava/lang/String;
    :cond_5
    const/4 v5, 0x0

    goto :goto_0

    .line 102
    .restart local v1    # "bytes":[B
    .restart local v4    # "ip":Ljava/lang/String;
    .restart local v5    # "isWifiConnected":Z
    .restart local v6    # "mac":Ljava/lang/String;
    .restart local v8    # "ssid":Ljava/lang/String;
    .restart local v9    # "wi":Landroid/net/wifi/WifiInfo;
    .restart local v10    # "wm":Landroid/net/wifi/WifiManager;
    :catch_0
    move-exception v3

    .line 103
    .local v3, "e":Ljava/net/UnknownHostException;
    const-string v11, "ContextFramework"

    const-string v12, "It failed to get the ip address."

    invoke-static {v11, v12, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 121
    .end local v1    # "bytes":[B
    .end local v3    # "e":Ljava/net/UnknownHostException;
    .end local v9    # "wi":Landroid/net/wifi/WifiInfo;
    .end local v10    # "wm":Landroid/net/wifi/WifiManager;
    :cond_6
    iget-boolean v11, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mConnected:Z

    if-eq v11, v5, :cond_4

    .line 122
    invoke-static {p1, v5, v8, v6, v4}, Lcom/samsung/android/providers/context/status/WifiMonitor;->insertWifiInfoLog(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static getInstance()Lcom/samsung/android/providers/context/status/WifiMonitor;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/samsung/android/providers/context/status/WifiMonitor;->sInstance:Lcom/samsung/android/providers/context/status/WifiMonitor;

    return-object v0
.end method

.method private static insertWifiInfoLog(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isWifiConnected"    # Z
    .param p2, "ssid"    # Ljava/lang/String;
    .param p3, "mac"    # Ljava/lang/String;
    .param p4, "ip"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 133
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 134
    .local v2, "row":Landroid/content/ContentValues;
    if-eqz p1, :cond_0

    .line 135
    const-string v3, "type"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 136
    const-string v3, "ap_ssid"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v3, "ap_mac"

    invoke-virtual {v2, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v3, "ip_address"

    invoke-virtual {v2, v3, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :goto_0
    :try_start_0
    sget-object v3, Lcom/samsung/android/providers/context/log/ContextLogContract$UseWifi;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_1
    return-void

    .line 141
    :cond_0
    const-string v3, "type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 145
    :catch_0
    move-exception v1

    .line 146
    .local v1, "ex":Ljava/lang/Exception;
    const-string v3, "ContextFramework"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static removeDoubleQuotation(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "ssid"    # Ljava/lang/String;

    .prologue
    .line 152
    const-string v1, "\""

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "\""

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "res":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 155
    .end local v0    # "res":Ljava/lang/String;
    :cond_0
    move-object v0, p0

    .line 156
    .restart local v0    # "res":Ljava/lang/String;
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "None Quotation SSID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private restoreLastStatusFromDb()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 62
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "type"

    aput-object v0, v2, v9

    const-string v0, "ap_ssid"

    aput-object v0, v2, v8

    const/4 v0, 0x2

    const-string v1, "ap_mac"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "ip_address"

    aput-object v1, v2, v0

    .line 64
    .local v2, "projectionIn":[Ljava/lang/String;
    const-string v0, "use_wifi"

    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$UseWifi;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_id DESC LIMIT 1"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->queryLog(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 67
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 68
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 69
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 70
    const-string v0, "ap_ssid"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mSsid:Ljava/lang/String;

    .line 71
    const-string v0, "ap_mac"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mMacAddress:Ljava/lang/String;

    .line 72
    const-string v0, "ip_address"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mIpAddress:Ljava/lang/String;

    .line 74
    :try_start_0
    const-string v0, "type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mConnected:Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :cond_0
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 81
    :cond_1
    return-void

    :cond_2
    move v0, v9

    .line 74
    goto :goto_0

    .line 75
    :catch_0
    move-exception v7

    .line 76
    .local v7, "ex":Ljava/lang/NumberFormatException;
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mMacAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v8

    :goto_2
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mConnected:Z

    goto :goto_1

    :cond_3
    move v0, v9

    goto :goto_2
.end method

.method private static unpack(I)[B
    .locals 7
    .param p0, "bytes"    # I

    .prologue
    const/4 v2, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 162
    new-array v0, v2, [B

    ushr-int/lit8 v1, p0, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    ushr-int/lit8 v1, p0, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v5

    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v6

    .line 166
    .local v0, "temp":[B
    new-array v1, v2, [B

    aget-byte v2, v0, v6

    aput-byte v2, v1, v3

    aget-byte v2, v0, v5

    aput-byte v2, v1, v4

    aget-byte v2, v0, v4

    aput-byte v2, v1, v5

    aget-byte v2, v0, v3

    aput-byte v2, v1, v6

    return-object v1
.end method


# virtual methods
.method public disable(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mIsEnabled:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mIsEnabled:Z

    .line 59
    :cond_0
    return-void
.end method

.method public enable(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mIsEnabled:Z

    if-nez v0, :cond_0

    .line 47
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/WifiMonitor;->restoreLastStatusFromDb()V

    .line 48
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/WifiMonitor;->checkWifiStatus(Landroid/content/Context;)V

    .line 49
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/WifiMonitor;->mIsEnabled:Z

    .line 52
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 39
    const-string v0, "networkType"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/WifiMonitor;->checkWifiStatus(Landroid/content/Context;)V

    .line 43
    :cond_0
    return-void
.end method
