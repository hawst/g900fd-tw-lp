.class public Lcom/samsung/android/providers/context/status/RoamingMonitor;
.super Landroid/content/BroadcastReceiver;
.source "RoamingMonitor.java"


# static fields
.field private static sInstance:Lcom/samsung/android/providers/context/status/RoamingMonitor;


# instance fields
.field private mIsEnabled:Z

.field private mMcc:I

.field private mMnc:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/samsung/android/providers/context/status/RoamingMonitor;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/RoamingMonitor;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->sInstance:Lcom/samsung/android/providers/context/status/RoamingMonitor;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mIsEnabled:Z

    .line 21
    iput v1, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMcc:I

    .line 22
    iput v1, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMnc:I

    return-void
.end method

.method private checkRoamingStatus(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x3

    .line 53
    const-string v7, "phone"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    .line 55
    .local v6, "tel":Landroid/telephony/TelephonyManager;
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v4

    .line 58
    .local v4, "networkOperator":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v8, :cond_1

    .line 61
    const/4 v7, 0x0

    const/4 v8, 0x3

    :try_start_0
    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 62
    .local v2, "mcc":I
    const/4 v7, 0x3

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 65
    .local v3, "mnc":I
    iget v7, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMcc:I

    if-ne v7, v2, :cond_0

    iget v7, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMnc:I

    if-eq v7, v3, :cond_1

    .line 66
    :cond_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 67
    .local v5, "row":Landroid/content/ContentValues;
    const-string v7, "mcc"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 68
    const-string v7, "mnc"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/providers/context/log/ContextLogContract$TrackRoaming;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 70
    iput v2, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMcc:I

    .line 71
    iput v3, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMnc:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 79
    .end local v2    # "mcc":I
    .end local v3    # "mnc":I
    .end local v5    # "row":Landroid/content/ContentValues;
    :cond_1
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v7, "ContextFramework"

    const-string v8, "It failed to parse as an integer value."

    invoke-static {v7, v8, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 75
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v1

    .line 76
    .local v1, "ex":Ljava/lang/Exception;
    const-string v7, "ContextFramework"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/android/providers/context/status/RoamingMonitor;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->sInstance:Lcom/samsung/android/providers/context/status/RoamingMonitor;

    return-object v0
.end method

.method private restoreLastStatusFromDb()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 82
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "mcc"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "mnc"

    aput-object v1, v2, v0

    .line 84
    .local v2, "projectionIn":[Ljava/lang/String;
    const-string v0, "track_roaming"

    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$TrackRoaming;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "timestamp DESC LIMIT 1"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->queryLog(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 87
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 88
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 90
    const-string v0, "mcc"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMcc:I

    .line 91
    const-string v0, "mnc"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMnc:I

    .line 93
    :cond_0
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_1

    .line 94
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read from DB: MCC("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMcc:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") MNC("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mMnc:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 98
    :cond_2
    return-void
.end method


# virtual methods
.method public disable(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mIsEnabled:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mIsEnabled:Z

    .line 50
    :cond_0
    return-void
.end method

.method public enable(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mIsEnabled:Z

    if-nez v0, :cond_0

    .line 38
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->restoreLastStatusFromDb()V

    .line 39
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->checkRoamingStatus(Landroid/content/Context;)V

    .line 40
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/RoamingMonitor;->mIsEnabled:Z

    .line 43
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 30
    const-string v0, "networkType"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->checkRoamingStatus(Landroid/content/Context;)V

    .line 34
    :cond_0
    return-void
.end method
