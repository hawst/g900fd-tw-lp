.class public Lcom/samsung/android/providers/context/server/LogSender;
.super Ljava/lang/Object;
.source "LogSender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;
    }
.end annotation


# static fields
.field private static final ALWAYS_LOG_DB_URI:Landroid/net/Uri;

.field private static final SURVEY_LOG_DB_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isAlwaysLog:Z

.field private isSurveyLog:Z

.field private final mConMsgHandler:Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;

.field private final mContext:Landroid/content/Context;

.field private final mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

.field private final mRstMsgHandler:Landroid/os/Handler;

.field private final mSPPBinder:Lcom/samsung/android/providers/context/server/SPPBinder;

.field private mSenderRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/providers/context/server/LogSender;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    .line 63
    const-string v0, "content://com.samsung.android.providers.context.server/always_log"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogSender;->ALWAYS_LOG_DB_URI:Landroid/net/Uri;

    .line 65
    const-string v0, "content://com.samsung.android.providers.context.server/survey_log"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogSender;->SURVEY_LOG_DB_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/android/providers/context/server/LogGenerator;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resultMessageHandler"    # Landroid/os/Handler;
    .param p3, "logGenerator"    # Lcom/samsung/android/providers/context/server/LogGenerator;

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/LogSender;->isAlwaysLog:Z

    .line 61
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/LogSender;->isSurveyLog:Z

    .line 70
    sget-object v0, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v1, "Construct LogSender"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mContext:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Lcom/samsung/android/providers/context/server/LogSender;->mRstMsgHandler:Landroid/os/Handler;

    .line 73
    iput-object p3, p0, Lcom/samsung/android/providers/context/server/LogSender;->mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

    .line 74
    new-instance v0, Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;-><init>(Lcom/samsung/android/providers/context/server/LogSender;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogSender;->mConMsgHandler:Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;

    .line 75
    new-instance v0, Lcom/samsung/android/providers/context/server/SPPBinder;

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mConMsgHandler:Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;

    invoke-direct {v0, v1}, Lcom/samsung/android/providers/context/server/SPPBinder;-><init>(Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogSender;->mSPPBinder:Lcom/samsung/android/providers/context/server/SPPBinder;

    .line 76
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/providers/context/server/LogSender;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/LogSender;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/LogSender;->mRstMsgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/providers/context/server/LogSender;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/LogSender;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogSender;->sendAll()V

    return-void
.end method

.method private static deleteLogDB(Landroid/content/ContentResolver;)V
    .locals 3
    .param p0, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 327
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "s"

    aput-object v2, v0, v1

    .line 330
    .local v0, "selectionArg":[Ljava/lang/String;
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Delete sent log : Always Log"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->ALWAYS_LOG_DB_URI:Landroid/net/Uri;

    const-string v2, "spp_result= ?"

    invoke-virtual {p0, v1, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 332
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Delete sent log : Survey Log"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->SURVEY_LOG_DB_URI:Landroid/net/Uri;

    const-string v2, "spp_result= ?"

    invoke-virtual {p0, v1, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 334
    return-void
.end method

.method private static deleteOldLog(Landroid/content/ContentResolver;)V
    .locals 9
    .param p0, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v8, 0x0

    .line 338
    const-string v0, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    .line 339
    .local v0, "SURVEY_BODY_TIME_FORMAT":Ljava/lang/String;
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 340
    .local v5, "queryBoundaryFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 341
    .local v1, "boundaryCal":Ljava/util/GregorianCalendar;
    const/4 v6, 0x5

    const/4 v7, -0x3

    invoke-virtual {v1, v6, v7}, Ljava/util/GregorianCalendar;->add(II)V

    .line 342
    const/16 v6, 0xb

    invoke-virtual {v1, v6, v8}, Ljava/util/GregorianCalendar;->set(II)V

    .line 343
    const/16 v6, 0xc

    invoke-virtual {v1, v6, v8}, Ljava/util/GregorianCalendar;->set(II)V

    .line 344
    const/16 v6, 0xd

    invoke-virtual {v1, v6, v8}, Ljava/util/GregorianCalendar;->set(II)V

    .line 345
    const/16 v6, 0xe

    invoke-virtual {v1, v6, v8}, Ljava/util/GregorianCalendar;->set(II)V

    .line 347
    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    .line 348
    .local v2, "delBoundary":J
    const/4 v6, 0x1

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v8

    .line 349
    .local v4, "deleteQueryBoundary":[Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Delete the old log boundary / UTC: ~"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Local: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    sget-object v6, Lcom/samsung/android/providers/context/server/LogSender;->SURVEY_LOG_DB_URI:Landroid/net/Uri;

    const-string v7, "timestamp_long < ?"

    invoke-virtual {p0, v6, v7, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 353
    return-void
.end method

.method private sendAll()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 183
    const/4 v6, 0x0

    .line 184
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 186
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/LogSender;->deleteOldLog(Landroid/content/ContentResolver;)V

    .line 188
    iget-boolean v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->isAlwaysLog:Z

    if-eqz v1, :cond_0

    .line 189
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v3, "Send Always log"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->ALWAYS_LOG_DB_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 191
    if-eqz v6, :cond_0

    .line 192
    const/4 v1, 0x0

    invoke-direct {p0, v1, v6}, Lcom/samsung/android/providers/context/server/LogSender;->sendSpp(ILandroid/database/Cursor;)V

    .line 193
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->isSurveyLog:Z

    if-eqz v1, :cond_1

    .line 198
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v3, "Send Survey log"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->SURVEY_LOG_DB_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 200
    if-eqz v6, :cond_1

    .line 201
    const/4 v1, 0x1

    invoke-direct {p0, v1, v6}, Lcom/samsung/android/providers/context/server/LogSender;->sendSpp(ILandroid/database/Cursor;)V

    .line 202
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 206
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mSPPBinder:Lcom/samsung/android/providers/context/server/SPPBinder;

    iget-object v2, p0, Lcom/samsung/android/providers/context/server/LogSender;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/samsung/android/providers/context/server/SPPBinder;->unbindService(Landroid/content/Context;)V

    .line 207
    return-void
.end method

.method private sendSpp(ILandroid/database/Cursor;)V
    .locals 29
    .param p1, "type"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 210
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v13, "Request send log to spp"

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const/16 v27, 0x1

    .line 213
    .local v27, "result":I
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v26

    .line 215
    .local v26, "remainLog":I
    const/4 v5, 0x0

    .line 216
    .local v5, "svcCode":Ljava/lang/String;
    const-wide/16 v6, -0x1

    .line 217
    .local v6, "timestamp":J
    const/4 v4, 0x0

    .line 218
    .local v4, "logType":Ljava/lang/String;
    const/4 v8, 0x0

    .line 219
    .local v8, "activity":Ljava/lang/String;
    const/4 v10, 0x0

    .line 220
    .local v10, "userId":Ljava/lang/String;
    const/4 v9, 0x0

    .line 221
    .local v9, "deviceId":Ljava/lang/String;
    const/4 v11, 0x0

    .line 222
    .local v11, "appVersion":Ljava/lang/String;
    const/4 v12, 0x0

    .line 223
    .local v12, "body":Ljava/lang/String;
    const/16 v24, -0x1

    .line 225
    .local v24, "id":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogSender;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 226
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v28, Landroid/content/ContentValues;

    invoke-direct/range {v28 .. v28}, Landroid/content/ContentValues;-><init>()V

    .line 229
    .local v28, "values":Landroid/content/ContentValues;
    :goto_0
    :try_start_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 231
    invoke-virtual/range {v28 .. v28}, Landroid/content/ContentValues;->clear()V

    .line 233
    if-nez p1, :cond_4

    .line 234
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v13, "Always log send"

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const-string v3, "log_type"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 236
    const-string v3, "svc_code"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 237
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 238
    const-string v3, "activity"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 239
    const-string v3, "user_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 240
    const-string v3, "app_version"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 241
    const-string v3, "body"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 243
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogSender;->mSPPBinder:Lcom/samsung/android/providers/context/server/SPPBinder;

    invoke-virtual/range {v3 .. v12}, Lcom/samsung/android/providers/context/server/SPPBinder;->sendLog(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v27

    .line 244
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Send message : [ "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " ]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    const-string v3, "_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    .line 248
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Row ID : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v3, -0x3

    move/from16 v0, v27

    if-eq v0, v3, :cond_0

    const/4 v3, -0x2

    move/from16 v0, v27

    if-eq v0, v3, :cond_0

    const/4 v3, -0x4

    move/from16 v0, v27

    if-eq v0, v3, :cond_0

    const/4 v3, -0x6

    move/from16 v0, v27

    if-ne v0, v3, :cond_2

    .line 252
    :cond_0
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Failed send log, reason : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, v27

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const/4 v3, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v24

    invoke-static {v2, v0, v3, v13, v1}, Lcom/samsung/android/providers/context/server/LogSender;->updateLogDB(Landroid/content/ContentResolver;Landroid/content/ContentValues;III)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 298
    :cond_1
    :goto_1
    invoke-static {v2}, Lcom/samsung/android/providers/context/server/LogSender;->deleteLogDB(Landroid/content/ContentResolver;)V

    .line 299
    return-void

    .line 256
    :cond_2
    if-nez v27, :cond_3

    .line 257
    const/4 v3, 0x0

    const/4 v13, 0x1

    :try_start_1
    move-object/from16 v0, v28

    move/from16 v1, v24

    invoke-static {v2, v0, v3, v13, v1}, Lcom/samsung/android/providers/context/server/LogSender;->updateLogDB(Landroid/content/ContentResolver;Landroid/content/ContentValues;III)V

    .line 286
    :cond_3
    :goto_2
    add-int/lit8 v26, v26, -0x1

    .line 287
    const/4 v3, 0x0

    const/16 v13, 0xb

    move/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v3, v13, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v25

    .line 288
    .local v25, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogSender;->mRstMsgHandler:Landroid/os/Handler;

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 290
    .end local v25    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v23

    .line 291
    .local v23, "e":Ljava/lang/NullPointerException;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 292
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Error while using the ContextProvider : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 259
    .end local v23    # "e":Ljava/lang/NullPointerException;
    :cond_4
    const/4 v3, 0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_3

    .line 260
    :try_start_2
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v13, "Survey log send"

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    const-string v3, "log_type"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 262
    const-string v3, "svc_code"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 263
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 264
    const-string v3, "activity"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 265
    const-string v3, "app_version"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 266
    const-string v3, "body"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 268
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/server/LogSender;->mSPPBinder:Lcom/samsung/android/providers/context/server/SPPBinder;

    const/16 v20, 0x0

    move-object v14, v4

    move-object v15, v5

    move-wide/from16 v16, v6

    move-object/from16 v18, v8

    move-object/from16 v19, v9

    move-object/from16 v21, v11

    move-object/from16 v22, v12

    invoke-virtual/range {v13 .. v22}, Lcom/samsung/android/providers/context/server/SPPBinder;->sendLog(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v27

    .line 269
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Send message : [ "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " ]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const-string v3, "_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    .line 274
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Row ID : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    const/4 v3, -0x3

    move/from16 v0, v27

    if-eq v0, v3, :cond_5

    const/4 v3, -0x2

    move/from16 v0, v27

    if-eq v0, v3, :cond_5

    const/4 v3, -0x4

    move/from16 v0, v27

    if-eq v0, v3, :cond_5

    const/4 v3, -0x6

    move/from16 v0, v27

    if-ne v0, v3, :cond_6

    .line 278
    :cond_5
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Failed send log, reason : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, v27

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const/4 v3, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v24

    invoke-static {v2, v0, v3, v13, v1}, Lcom/samsung/android/providers/context/server/LogSender;->updateLogDB(Landroid/content/ContentResolver;Landroid/content/ContentValues;III)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 293
    :catch_1
    move-exception v23

    .line 294
    .local v23, "e":Ljava/lang/NumberFormatException;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 295
    sget-object v3, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Error while using the ContextProvider : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 282
    .end local v23    # "e":Ljava/lang/NumberFormatException;
    :cond_6
    if-nez v27, :cond_3

    .line 283
    const/4 v3, 0x1

    const/4 v13, 0x1

    :try_start_3
    move-object/from16 v0, v28

    move/from16 v1, v24

    invoke-static {v2, v0, v3, v13, v1}, Lcom/samsung/android/providers/context/server/LogSender;->updateLogDB(Landroid/content/ContentResolver;Landroid/content/ContentValues;III)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2
.end method

.method private static updateLogDB(Landroid/content/ContentResolver;Landroid/content/ContentValues;III)V
    .locals 4
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "row"    # Landroid/content/ContentValues;
    .param p2, "type"    # I
    .param p3, "sendResult"    # I
    .param p4, "id"    # I

    .prologue
    const/4 v3, 0x0

    .line 303
    const/4 v0, 0x0

    .line 304
    .local v0, "updateUri":Landroid/net/Uri;
    if-nez p2, :cond_1

    .line 305
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://com.samsung.android.providers.context.server/always_log/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 312
    :cond_0
    :goto_0
    packed-switch p3, :pswitch_data_0

    .line 323
    :goto_1
    invoke-virtual {p0, v0, p1, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 324
    return-void

    .line 307
    :cond_1
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 308
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://com.samsung.android.providers.context.server/survey_log/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 314
    :pswitch_0
    const-string v1, "spp_result"

    const-string v2, "s"

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 318
    :pswitch_1
    const-string v1, "spp_result"

    const-string v2, "f"

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v9, 0x0

    .line 79
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

    if-nez v1, :cond_0

    .line 80
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Cannot start log sender due to invalid log generator instance"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :goto_0
    return-void

    .line 84
    :cond_0
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "run()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iput-boolean v3, p0, Lcom/samsung/android/providers/context/server/LogSender;->mSenderRunning:Z

    .line 87
    iput-boolean v9, p0, Lcom/samsung/android/providers/context/server/LogSender;->isAlwaysLog:Z

    .line 88
    iput-boolean v9, p0, Lcom/samsung/android/providers/context/server/LogSender;->isSurveyLog:Z

    .line 90
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 93
    .local v0, "cr":Landroid/content/ContentResolver;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/server/LogGenerator;->getAlwaysUpload()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 94
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->ALWAYS_LOG_DB_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 95
    .local v6, "alwaysCursor":Landroid/database/Cursor;
    if-nez v6, :cond_3

    .line 96
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Failed to query the Always log DB"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    .end local v6    # "alwaysCursor":Landroid/database/Cursor;
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/server/LogGenerator;->getSurveyUpload()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 111
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->SURVEY_LOG_DB_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 112
    .local v8, "surveyCursor":Landroid/database/Cursor;
    if-nez v8, :cond_6

    .line 113
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Failed to query the Survey log DB"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    .end local v8    # "surveyCursor":Landroid/database/Cursor;
    :goto_2
    iget-boolean v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->isAlwaysLog:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->isSurveyLog:Z

    if-eqz v1, :cond_2

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mSPPBinder:Lcom/samsung/android/providers/context/server/SPPBinder;

    iget-object v2, p0, Lcom/samsung/android/providers/context/server/LogSender;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/samsung/android/providers/context/server/SPPBinder;->bindService(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :cond_2
    :goto_3
    iput-boolean v9, p0, Lcom/samsung/android/providers/context/server/LogSender;->mSenderRunning:Z

    goto :goto_0

    .line 98
    .restart local v6    # "alwaysCursor":Landroid/database/Cursor;
    :cond_3
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_4

    .line 99
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Always log DB is no information."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 130
    .end local v6    # "alwaysCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 131
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 132
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while using the ContextProvider : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 103
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v6    # "alwaysCursor":Landroid/database/Cursor;
    :cond_4
    const/4 v1, 0x1

    :try_start_2
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->isAlwaysLog:Z

    .line 104
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 107
    .end local v6    # "alwaysCursor":Landroid/database/Cursor;
    :cond_5
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Always log upload service is unavailable"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 115
    .restart local v8    # "surveyCursor":Landroid/database/Cursor;
    :cond_6
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_7

    .line 116
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Survey log DB is no information"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 120
    :cond_7
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->isSurveyLog:Z

    .line 121
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 124
    .end local v8    # "surveyCursor":Landroid/database/Cursor;
    :cond_8
    sget-object v1, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v2, "Survey log upload service is unavailable"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2
.end method

.method public stopSender()V
    .locals 2

    .prologue
    .line 356
    sget-object v0, Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;

    const-string v1, "stopSender()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/server/LogSender;->mSenderRunning:Z

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/LogSender;->mSPPBinder:Lcom/samsung/android/providers/context/server/SPPBinder;

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/server/SPPBinder;->unbindService(Landroid/content/Context;)V

    .line 359
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/LogSender;->mSenderRunning:Z

    .line 361
    :cond_0
    return-void
.end method
