.class Lcom/samsung/android/providers/context/TaskScheduler$2;
.super Landroid/content/BroadcastReceiver;
.source "TaskScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/providers/context/TaskScheduler;->registerTimeChangeReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/TaskScheduler;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/TaskScheduler;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/samsung/android/providers/context/TaskScheduler$2;->this$0:Lcom/samsung/android/providers/context/TaskScheduler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v4, 0x0

    .line 249
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 251
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.android.providers.context.action.DUMP_CONTEXT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 252
    # invokes: Lcom/samsung/android/providers/context/TaskScheduler;->setTestProcessingDate()V
    invoke-static {}, Lcom/samsung/android/providers/context/TaskScheduler;->access$300()V

    .line 255
    iget-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler$2;->this$0:Lcom/samsung/android/providers/context/TaskScheduler;

    # invokes: Lcom/samsung/android/providers/context/TaskScheduler;->batchProcessing(J)V
    invoke-static {v1, v4, v5}, Lcom/samsung/android/providers/context/TaskScheduler;->access$400(Lcom/samsung/android/providers/context/TaskScheduler;J)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    const-string v1, "ContextFramework"

    const-string v2, "TaskScheduler: received ACTION_TIME_CHANGED"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    # invokes: Lcom/samsung/android/providers/context/TaskScheduler;->changeSentDate()V
    invoke-static {}, Lcom/samsung/android/providers/context/TaskScheduler;->access$500()V

    .line 263
    iget-object v1, p0, Lcom/samsung/android/providers/context/TaskScheduler$2;->this$0:Lcom/samsung/android/providers/context/TaskScheduler;

    # invokes: Lcom/samsung/android/providers/context/TaskScheduler;->batchProcessing(J)V
    invoke-static {v1, v4, v5}, Lcom/samsung/android/providers/context/TaskScheduler;->access$400(Lcom/samsung/android/providers/context/TaskScheduler;J)V

    goto :goto_0
.end method
