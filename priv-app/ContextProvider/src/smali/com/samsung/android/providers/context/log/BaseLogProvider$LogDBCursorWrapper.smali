.class Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;
.super Landroid/database/CursorWrapper;
.source "BaseLogProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/log/BaseLogProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LogDBCursorWrapper"
.end annotation


# instance fields
.field final mAppId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 553
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 554
    if-nez p2, :cond_0

    .line 555
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->mAppId:Ljava/lang/String;

    .line 559
    :goto_0
    return-void

    .line 557
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->mAppId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getDouble(I)D
    .locals 10
    .param p1, "columnIndex"    # I

    .prologue
    .line 564
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->getDouble(I)D

    move-result-wide v2

    .line 565
    .local v2, "result":D
    const-string v5, "longitude"

    invoke-virtual {p0, v5}, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    if-eq v5, p1, :cond_0

    const-string v5, "latitude"

    invoke-virtual {p0, v5}, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    if-ne v5, p1, :cond_1

    .line 567
    :cond_0
    const-wide/16 v6, 0x0

    .line 569
    .local v6, "ts":J
    :try_start_0
    const-string v5, "timestamp_utc"

    invoke-super {p0, v5}, Landroid/database/CursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-super {p0, v5}, Landroid/database/CursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 571
    .local v4, "timestamp":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 572
    .local v0, "currentTime":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 578
    .end local v0    # "currentTime":Ljava/util/Date;
    .end local v4    # "timestamp":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v5

    iget-object v8, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->mAppId:Ljava/lang/String;

    sget-object v9, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v5, v8, v9, v6, v7}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;J)Z

    move-result v5

    if-nez v5, :cond_1

    .line 580
    const-wide/high16 v2, -0x3f97000000000000L    # -200.0

    .line 584
    .end local v6    # "ts":J
    :cond_1
    return-wide v2

    .line 573
    .restart local v6    # "ts":J
    :catch_0
    move-exception v1

    .line 574
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 575
    const-wide/high16 v2, -0x3f97000000000000L    # -200.0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 10
    .param p1, "columnIndex"    # I

    .prologue
    .line 590
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->getLong(I)J

    move-result-wide v2

    .line 592
    .local v2, "result":J
    const-string v5, "place_category"

    invoke-virtual {p0, v5}, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    if-ne v5, p1, :cond_0

    .line 593
    const-wide/16 v6, 0x0

    .line 595
    .local v6, "ts":J
    :try_start_0
    const-string v5, "timestamp_utc"

    invoke-super {p0, v5}, Landroid/database/CursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-super {p0, v5}, Landroid/database/CursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 597
    .local v4, "timestamp":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 598
    .local v0, "currentTime":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 604
    .end local v0    # "currentTime":Ljava/util/Date;
    .end local v4    # "timestamp":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v5

    iget-object v8, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->mAppId:Ljava/lang/String;

    sget-object v9, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v5, v8, v9, v6, v7}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;J)Z

    move-result v5

    if-nez v5, :cond_0

    .line 606
    const-wide/16 v2, 0x0

    .line 610
    .end local v6    # "ts":J
    :cond_0
    return-wide v2

    .line 599
    .restart local v6    # "ts":J
    :catch_0
    move-exception v1

    .line 600
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 601
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 9
    .param p1, "columnIndex"    # I

    .prologue
    .line 615
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 616
    .local v2, "result":Ljava/lang/String;
    const-string v6, "place_name"

    invoke-virtual {p0, v6}, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    if-ne v6, p1, :cond_0

    .line 617
    const-wide/16 v4, 0x0

    .line 619
    .local v4, "ts":J
    :try_start_0
    const-string v6, "timestamp_utc"

    invoke-super {p0, v6}, Landroid/database/CursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-super {p0, v6}, Landroid/database/CursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 621
    .local v3, "timestamp":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 622
    .local v0, "currentTime":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 628
    .end local v0    # "currentTime":Ljava/util/Date;
    .end local v3    # "timestamp":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/providers/context/log/BaseLogProvider$LogDBCursorWrapper;->mAppId:Ljava/lang/String;

    sget-object v8, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v6, v7, v8, v4, v5}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;J)Z

    move-result v6

    if-nez v6, :cond_0

    .line 630
    const-string v2, ""

    .line 634
    .end local v4    # "ts":J
    :cond_0
    return-object v2

    .line 623
    .restart local v4    # "ts":J
    :catch_0
    move-exception v1

    .line 624
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    .line 625
    const-string v2, ""

    goto :goto_0
.end method
