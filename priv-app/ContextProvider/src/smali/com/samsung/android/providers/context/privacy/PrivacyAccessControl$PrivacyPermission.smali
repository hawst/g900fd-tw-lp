.class Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
.super Ljava/lang/Object;
.source "PrivacyAccessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PrivacyPermission"
.end annotation


# instance fields
.field private mConsentTimeSet:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLastConsentId:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mLastConsentId:J

    .line 290
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission$1;-><init>(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mConsentTimeSet:Ljava/util/TreeSet;

    .line 295
    return-void
.end method


# virtual methods
.method public getConsentTimeSet()Ljava/util/TreeSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeSet",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 327
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mConsentTimeSet:Ljava/util/TreeSet;

    return-object v0
.end method

.method public getLastConsentId()J
    .locals 2

    .prologue
    .line 348
    iget-wide v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mLastConsentId:J

    return-wide v0
.end method

.method public insertConsentTime(JJJ)V
    .locals 3
    .param p1, "startTime"    # J
    .param p3, "stopTime"    # J
    .param p5, "consentId"    # J

    .prologue
    .line 332
    new-instance v0, Landroid/util/Pair;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 333
    .local v0, "timePair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mConsentTimeSet:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 335
    iput-wide p5, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mLastConsentId:J

    .line 336
    return-void
.end method

.method public isPermittedNow()Z
    .locals 8

    .prologue
    .line 312
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 313
    .local v0, "currentTime":J
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v3

    .line 314
    .local v3, "timeFormatUtc":Ljava/text/DateFormat;
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 317
    .local v4, "timestampUtc":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    .line 318
    .local v5, "utcTime":Ljava/util/Date;
    invoke-virtual {v5}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 323
    .end local v5    # "utcTime":Ljava/util/Date;
    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->isPermittedTime(J)Z

    move-result v6

    return v6

    .line 319
    :catch_0
    move-exception v2

    .line 320
    .local v2, "ex":Ljava/text/ParseException;
    const-string v6, "ContextFramework:PrivacyAccessControl"

    invoke-virtual {v2}, Ljava/text/ParseException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isPermittedTime(J)Z
    .locals 9
    .param p1, "time"    # J

    .prologue
    const/4 v3, 0x0

    .line 299
    new-instance v0, Landroid/util/Pair;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 300
    .local v0, "compareeTime":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mConsentTimeSet:Ljava/util/TreeSet;

    invoke-virtual {v2, v0}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 302
    .local v1, "lastTime":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    if-nez v1, :cond_0

    .line 303
    const-string v2, "ContextFramework:PrivacyAccessControl"

    const-string v4, "User consent have never been acquired before the specified time"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :goto_0
    return v3

    :cond_0
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, p1, v4

    if-lez v2, :cond_2

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-ltz v2, :cond_1

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, p1, v4

    if-gez v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public updateLastConsentTime(J)V
    .locals 5
    .param p1, "stopTime"    # J

    .prologue
    .line 340
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mConsentTimeSet:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 342
    .local v0, "timePair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    if-eqz v0, :cond_0

    .line 343
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->mConsentTimeSet:Ljava/util/TreeSet;

    new-instance v2, Landroid/util/Pair;

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 345
    :cond_0
    return-void
.end method
