.class public Lcom/samsung/android/providers/context/status/MediaStatusMonitor;
.super Ljava/lang/Object;
.source "MediaStatusMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;,
        Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaStatusChangeListener;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/samsung/android/providers/context/status/MediaStatusMonitor;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mMusicPlayMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mVideoPlayMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mInstance:Lcom/samsung/android/providers/context/status/MediaStatusMonitor;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mContext:Landroid/content/Context;

    .line 64
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mMusicPlayMap:Landroid/util/SparseArray;

    .line 65
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mVideoPlayMap:Landroid/util/SparseArray;

    .line 66
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/providers/context/status/MediaStatusMonitor;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mInstance:Lcom/samsung/android/providers/context/status/MediaStatusMonitor;

    return-object v0
.end method


# virtual methods
.method public processMusicEvent(Ljava/lang/String;ILjava/lang/String;)V
    .locals 17
    .param p1, "operation"    # Ljava/lang/String;
    .param p2, "contentId"    # I
    .param p3, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 150
    const-string v13, "start"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 152
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-direct {v4, v14, v15}, Ljava/util/Date;-><init>(J)V

    .line 153
    .local v4, "currentTimeDate":Ljava/util/Date;
    new-instance v5, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 154
    .local v5, "currentTimeString":Ljava/lang/String;
    new-instance v9, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;

    move-object/from16 v0, p3

    invoke-direct {v9, v0}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;-><init>(Ljava/lang/String;)V

    .line 156
    .local v9, "musicMetadata":Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 157
    .local v12, "row":Landroid/content/ContentValues;
    const-string v13, "start_time"

    invoke-virtual {v12, v13, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v13, "stop_time"

    invoke-virtual {v12, v13, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v13, "starttime"

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 160
    const-string v13, "stoptime"

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 161
    const-string v13, "duration"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 162
    const-string v13, "title"

    invoke-virtual {v9}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->getTitle()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v13, "artist"

    invoke-virtual {v9}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->getArtists()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v13, "album"

    invoke-virtual {v9}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->getAlbum()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v13, "year"

    invoke-virtual {v9}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->getYear()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 166
    const-string v13, "genre"

    invoke-virtual {v9}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->getGenre()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v13, "uri"

    invoke-virtual {v9}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->getCotentUri()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v13, "stop_type"

    const/4 v14, -0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 169
    const/4 v11, 0x0

    .line 171
    .local v11, "recordUri":Landroid/net/Uri;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/providers/context/log/ContextLogContract$PlayMusic;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v13, v14, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 176
    :goto_0
    if-eqz v11, :cond_1

    .line 178
    new-instance v10, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;

    invoke-direct {v10, v11}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;-><init>(Landroid/net/Uri;)V

    .line 179
    .local v10, "playMusicInfo":Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mMusicPlayMap:Landroid/util/SparseArray;

    move/from16 v0, p2

    invoke-virtual {v13, v0, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 180
    const-string v13, "ContextFramework:MediaStatusMonitor"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] A new music log (ID:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") is inserted"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    .end local v4    # "currentTimeDate":Ljava/util/Date;
    .end local v5    # "currentTimeString":Ljava/lang/String;
    .end local v9    # "musicMetadata":Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;
    .end local v10    # "playMusicInfo":Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;
    .end local v11    # "recordUri":Landroid/net/Uri;
    .end local v12    # "row":Landroid/content/ContentValues;
    :cond_0
    :goto_1
    return-void

    .line 172
    .restart local v4    # "currentTimeDate":Ljava/util/Date;
    .restart local v5    # "currentTimeString":Ljava/lang/String;
    .restart local v9    # "musicMetadata":Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;
    .restart local v11    # "recordUri":Landroid/net/Uri;
    .restart local v12    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v8

    .line 173
    .local v8, "ex":Ljava/lang/Exception;
    const-string v13, "ContextFramework:MediaStatusMonitor"

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 182
    .end local v8    # "ex":Ljava/lang/Exception;
    :cond_1
    const-string v13, "ContextFramework:MediaStatusMonitor"

    const-string v14, "Inserting a log into \"log.play_music\" fails"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 185
    .end local v4    # "currentTimeDate":Ljava/util/Date;
    .end local v5    # "currentTimeString":Ljava/lang/String;
    .end local v9    # "musicMetadata":Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;
    .end local v11    # "recordUri":Landroid/net/Uri;
    .end local v12    # "row":Landroid/content/ContentValues;
    :cond_2
    const-string v13, "pause"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    const-string v13, "stop"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    const-string v13, "error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 188
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mMusicPlayMap:Landroid/util/SparseArray;

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;

    .line 190
    .restart local v10    # "playMusicInfo":Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;
    if-eqz v10, :cond_7

    .line 192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 193
    .local v2, "currentTime":J
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 194
    .restart local v4    # "currentTimeDate":Ljava/util/Date;
    new-instance v5, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 195
    .restart local v5    # "currentTimeString":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;->getStartTime()J

    move-result-wide v14

    sub-long v6, v2, v14

    .line 197
    .local v6, "duration":J
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 198
    .restart local v12    # "row":Landroid/content/ContentValues;
    const-string v13, "stop_time"

    invoke-virtual {v12, v13, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v13, "stoptime"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 200
    const-string v13, "duration"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 202
    const-string v13, "pause"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 203
    const-string v13, "stop_type"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 210
    :cond_4
    :goto_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;->getRecordUri()Landroid/net/Uri;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v12, v15, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 214
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mMusicPlayMap:Landroid/util/SparseArray;

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 216
    const-string v13, "ContextFramework:MediaStatusMonitor"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] The music log (ID:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") is updated"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 204
    :cond_5
    const-string v13, "stop"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 205
    const-string v13, "stop_type"

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    .line 206
    :cond_6
    const-string v13, "error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 207
    const-string v13, "stop_type"

    const/4 v14, 0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    .line 211
    :catch_1
    move-exception v8

    .line 212
    .restart local v8    # "ex":Ljava/lang/Exception;
    const-string v13, "ContextFramework:MediaStatusMonitor"

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 218
    .end local v2    # "currentTime":J
    .end local v4    # "currentTimeDate":Ljava/util/Date;
    .end local v5    # "currentTimeString":Ljava/lang/String;
    .end local v6    # "duration":J
    .end local v8    # "ex":Ljava/lang/Exception;
    .end local v12    # "row":Landroid/content/ContentValues;
    :cond_7
    const-string v13, "ContextFramework:MediaStatusMonitor"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Updating log fails. No previously inserted log about the music (ID:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public processVideoEvent(Ljava/lang/String;ILjava/lang/String;)V
    .locals 17
    .param p1, "operation"    # Ljava/lang/String;
    .param p2, "contentId"    # I
    .param p3, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 75
    const-string v13, "start"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 77
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-direct {v4, v14, v15}, Ljava/util/Date;-><init>(J)V

    .line 78
    .local v4, "currentTimeDate":Ljava/util/Date;
    new-instance v5, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 79
    .local v5, "currentTimeString":Ljava/lang/String;
    new-instance v12, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;

    move-object/from16 v0, p3

    invoke-direct {v12, v0}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;-><init>(Ljava/lang/String;)V

    .line 81
    .local v12, "videoMetadata":Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 82
    .local v11, "row":Landroid/content/ContentValues;
    const-string v13, "start_time"

    invoke-virtual {v11, v13, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v13, "stop_time"

    invoke-virtual {v11, v13, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v13, "starttime"

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 85
    const-string v13, "stoptime"

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 86
    const-string v13, "duration"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 87
    const-string v13, "title"

    invoke-virtual {v12}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->getTitle()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v13, "year"

    invoke-virtual {v12}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->getYear()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 89
    const-string v13, "genre"

    invoke-virtual {v12}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->getGenre()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v13, "uri"

    invoke-virtual {v12}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->getCotentUri()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v13, "stop_type"

    const/4 v14, -0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 93
    const/4 v10, 0x0

    .line 95
    .local v10, "recordUri":Landroid/net/Uri;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/providers/context/log/ContextLogContract$PlayVideo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 100
    :goto_0
    if-eqz v10, :cond_1

    .line 102
    new-instance v9, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;

    invoke-direct {v9, v10}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;-><init>(Landroid/net/Uri;)V

    .line 104
    .local v9, "playVideoInfo":Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mVideoPlayMap:Landroid/util/SparseArray;

    move/from16 v0, p2

    invoke-virtual {v13, v0, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 105
    const-string v13, "ContextFramework:MediaStatusMonitor"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] A new video log (ID:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") is inserted"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    .end local v4    # "currentTimeDate":Ljava/util/Date;
    .end local v5    # "currentTimeString":Ljava/lang/String;
    .end local v9    # "playVideoInfo":Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;
    .end local v10    # "recordUri":Landroid/net/Uri;
    .end local v11    # "row":Landroid/content/ContentValues;
    .end local v12    # "videoMetadata":Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;
    :cond_0
    :goto_1
    return-void

    .line 96
    .restart local v4    # "currentTimeDate":Ljava/util/Date;
    .restart local v5    # "currentTimeString":Ljava/lang/String;
    .restart local v10    # "recordUri":Landroid/net/Uri;
    .restart local v11    # "row":Landroid/content/ContentValues;
    .restart local v12    # "videoMetadata":Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;
    :catch_0
    move-exception v8

    .line 97
    .local v8, "ex":Ljava/lang/Exception;
    const-string v13, "ContextFramework:MediaStatusMonitor"

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 107
    .end local v8    # "ex":Ljava/lang/Exception;
    :cond_1
    const-string v13, "ContextFramework:MediaStatusMonitor"

    const-string v14, "Inserting a log into \"log.play_video\" fails"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 110
    .end local v4    # "currentTimeDate":Ljava/util/Date;
    .end local v5    # "currentTimeString":Ljava/lang/String;
    .end local v10    # "recordUri":Landroid/net/Uri;
    .end local v11    # "row":Landroid/content/ContentValues;
    .end local v12    # "videoMetadata":Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;
    :cond_2
    const-string v13, "pause"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    const-string v13, "stop"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    const-string v13, "error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 113
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mVideoPlayMap:Landroid/util/SparseArray;

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;

    .line 115
    .restart local v9    # "playVideoInfo":Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;
    if-eqz v9, :cond_7

    .line 117
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 118
    .local v2, "currentTime":J
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 119
    .restart local v4    # "currentTimeDate":Ljava/util/Date;
    new-instance v5, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 120
    .restart local v5    # "currentTimeString":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;->getStartTime()J

    move-result-wide v14

    sub-long v6, v2, v14

    .line 122
    .local v6, "duration":J
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 123
    .restart local v11    # "row":Landroid/content/ContentValues;
    const-string v13, "stop_time"

    invoke-virtual {v11, v13, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v13, "stoptime"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 125
    const-string v13, "duration"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 127
    const-string v13, "pause"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 128
    const-string v13, "stop_type"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 135
    :cond_4
    :goto_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    invoke-virtual {v9}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;->getRecordUri()Landroid/net/Uri;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v11, v15, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->mVideoPlayMap:Landroid/util/SparseArray;

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 141
    const-string v13, "ContextFramework:MediaStatusMonitor"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] The video log (ID:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") is updated"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 129
    :cond_5
    const-string v13, "stop"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 130
    const-string v13, "stop_type"

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    .line 131
    :cond_6
    const-string v13, "error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 132
    const-string v13, "stop_type"

    const/4 v14, 0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    .line 136
    :catch_1
    move-exception v8

    .line 137
    .restart local v8    # "ex":Ljava/lang/Exception;
    const-string v13, "ContextFramework:MediaStatusMonitor"

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 143
    .end local v2    # "currentTime":J
    .end local v4    # "currentTimeDate":Ljava/util/Date;
    .end local v5    # "currentTimeString":Ljava/lang/String;
    .end local v6    # "duration":J
    .end local v8    # "ex":Ljava/lang/Exception;
    .end local v11    # "row":Landroid/content/ContentValues;
    :cond_7
    const-string v13, "ContextFramework:MediaStatusMonitor"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Updating log fails. No previously inserted log about the video (ID:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
