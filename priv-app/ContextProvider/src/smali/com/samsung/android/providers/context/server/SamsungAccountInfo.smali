.class public Lcom/samsung/android/providers/context/server/SamsungAccountInfo;
.super Ljava/lang/Object;
.source "SamsungAccountInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static isInitedSA:I


# instance fields
.field public isInited:Z

.field private isUserId:Z

.field private mBinder:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

.field private final mContext:Landroid/content/Context;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    .line 44
    const/4 v0, 0x2

    sput v0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isInitedSA:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mBinder:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mUserId:Ljava/lang/String;

    .line 49
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isUserId:Z

    .line 50
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isInited:Z

    .line 53
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    const-string v1, "Construct SamsungAccountInfo"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;

    .line 57
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->registLogDataReceiver()V

    .line 59
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isSamsungAccountPackage(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->checkSharedPref()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mBinder:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    .line 66
    :goto_0
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isInited:Z

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mBinder:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SamsungAccountInfo;
    .param p1, "x1"    # Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mBinder:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    return-object p1
.end method

.method private checkSharedPref()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 69
    iget-object v8, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;

    invoke-static {v8}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 70
    .local v0, "accMgr":Landroid/accounts/AccountManager;
    const-string v8, "com.osp.app.signin"

    invoke-virtual {v0, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 72
    .local v1, "accountArr":[Landroid/accounts/Account;
    array-length v8, v1

    if-lez v8, :cond_0

    .line 73
    aget-object v8, v1, v6

    iget-object v2, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 74
    .local v2, "accountName":Ljava/lang/String;
    sget-object v8, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Account Name : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v4

    .line 77
    .local v4, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v8, "login_id"

    invoke-virtual {v4, v8}, Lcom/samsung/android/providers/context/ContextPreference;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 78
    const-string v8, "user_id"

    invoke-virtual {v4, v8}, Lcom/samsung/android/providers/context/ContextPreference;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 79
    const-string v6, "login_id"

    invoke-virtual {v4, v6}, Lcom/samsung/android/providers/context/ContextPreference;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "loginId":Ljava/lang/String;
    const-string v6, "user_id"

    invoke-virtual {v4, v6}, Lcom/samsung/android/providers/context/ContextPreference;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 81
    .local v5, "userId":Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "login_id : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    sget-object v6, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "user_id : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-virtual {p0, v5}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setUserId(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0, v7}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setIsUserId(Z)V

    .end local v2    # "accountName":Ljava/lang/String;
    .end local v3    # "loginId":Ljava/lang/String;
    .end local v4    # "pref":Lcom/samsung/android/providers/context/ContextPreference;
    .end local v5    # "userId":Ljava/lang/String;
    :cond_0
    move v6, v7

    .line 97
    :goto_0
    return v6

    .line 87
    .restart local v2    # "accountName":Ljava/lang/String;
    .restart local v4    # "pref":Lcom/samsung/android/providers/context/ContextPreference;
    :cond_1
    sget-object v7, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    const-string v8, "User id is null in account preference, try request AccessToken."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 91
    :cond_2
    sget-object v7, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    const-string v8, "Not exist guid in account preference, try request AccessToken."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setUserId(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0, v6}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setIsUserId(Z)V

    goto :goto_0
.end method

.method private static isSamsungAccount(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 117
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 118
    .local v0, "accMgr":Landroid/accounts/AccountManager;
    if-nez v0, :cond_0

    .line 119
    sget-object v3, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    const-string v4, "AccountManager is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :goto_0
    return v2

    .line 123
    :cond_0
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 125
    .local v1, "accountArr":[Landroid/accounts/Account;
    array-length v3, v1

    if-lez v3, :cond_1

    .line 126
    sget-object v2, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    const-string v3, "Exist Samsung Account"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const/4 v2, 0x1

    goto :goto_0

    .line 128
    :cond_1
    sget-object v3, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    const-string v4, "Not exist Samsung Account"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static isSamsungAccountPackage(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 103
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "com.osp.app.signin"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 104
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 105
    .local v3, "version":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Samsung Account version : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    const/4 v4, 0x1

    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    .end local v3    # "version":Ljava/lang/String;
    :goto_0
    return v4

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v4, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    const-string v5, "Didn\'t installed Samsung Account package"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 110
    sget-object v4, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception error : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private registLogDataReceiver()V
    .locals 3

    .prologue
    .line 135
    sget-object v1, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    const-string v2, "Register SPP Receiver for samsung account"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 138
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;-><init>(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 178
    return-void
.end method


# virtual methods
.method public getUserId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 181
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Get user id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mUserId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method public setInitedSA(I)V
    .locals 0
    .param p1, "initedSA"    # I

    .prologue
    .line 209
    sput p1, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isInitedSA:I

    .line 210
    return-void
.end method

.method public setIsUserId(Z)V
    .locals 3
    .param p1, "setUserId"    # Z

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isUserId:Z

    .line 197
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set isUserId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isUserId:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 3
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mUserId:Ljava/lang/String;

    .line 187
    sget-object v0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set user id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mUserId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    return-void
.end method
