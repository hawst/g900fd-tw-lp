.class Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;
.super Landroid/content/BroadcastReceiver;
.source "SamsungAccountInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->registLogDataReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 143
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive from Samsung Account"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 146
    :cond_0
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onReceive. Invailid params"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_1
    :goto_0
    return-void

    .line 150
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 151
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Get action : SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$100(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isSamsungAccount(Landroid/content/Context;)Z
    invoke-static {v1}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$200(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 154
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v0

    .line 155
    .local v0, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v1, "login_id"

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/providers/context/ContextPreference;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v1, "user_id"

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/providers/context/ContextPreference;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    invoke-virtual {v1, v3}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setUserId(Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setIsUserId(Z)V

    goto :goto_0

    .line 160
    .end local v0    # "pref":Lcom/samsung/android/providers/context/ContextPreference;
    :cond_3
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Warning! Samsung account wasn\'t deleted, Check your Samsung account"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 163
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Get action : SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$100(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->isSamsungAccount(Landroid/content/Context;)Z
    invoke-static {v1}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$200(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 167
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mBinder:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
    invoke-static {v1}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$300(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    move-result-object v1

    if-nez v1, :cond_5

    .line 168
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    new-instance v2, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    iget-object v3, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$100(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mBinder:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
    invoke-static {v1, v2}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$302(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    goto/16 :goto_0

    .line 170
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->mBinder:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
    invoke-static {v1}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$300(Lcom/samsung/android/providers/context/server/SamsungAccountInfo;)Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->bindService()V

    goto/16 :goto_0

    .line 173
    :cond_6
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Warning! Samsung account not exist, Check your Samsung account"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
