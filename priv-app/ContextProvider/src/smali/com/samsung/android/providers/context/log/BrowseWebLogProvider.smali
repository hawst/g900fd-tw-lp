.class public Lcom/samsung/android/providers/context/log/BrowseWebLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "BrowseWebLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 35
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "app_id"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string v3, "url"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/BrowseWebLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 42
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "browse_web"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_BROWSE_WEB:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/BrowseWebLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 49
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    .line 50
    const/16 v1, 0x1b58

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    .line 51
    const/16 v1, 0x2bc

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    .line 53
    new-instance v1, Lcom/samsung/android/providers/context/log/BrowseWebLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/BrowseWebLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/BrowseWebLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 92
    sget-object v1, Lcom/samsung/android/providers/context/log/BrowseWebLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 93
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/samsung/android/providers/context/log/BrowseWebLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 125
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 5
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    .local v0, "app_id":Ljava/lang/String;
    const/4 v2, 0x0

    .line 101
    .local v2, "url":Ljava/lang/String;
    :try_start_0
    const-string v3, "app_id"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    const-string v3, "url"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 108
    :goto_0
    if-eqz v2, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 109
    :cond_0
    const-string v3, "ContextFramework"

    const-string v4, "BrowseWeb : [Drop] - url is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const/4 v3, 0x0

    .line 116
    :goto_1
    return v3

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, "ex":Ljava/lang/NullPointerException;
    const-string v3, "ContextFramework"

    const-string v4, "BrowseWeb : ValidBoundaryCheck - NullPointerException."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 112
    .end local v1    # "ex":Ljava/lang/NullPointerException;
    :cond_1
    if-eqz v0, :cond_2

    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 113
    :cond_2
    const-string v3, "ContextFramework"

    const-string v4, "BrowseWeb : app_id is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method
