.class Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;
.super Ljava/lang/Object;
.source "MediaStatusMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/MediaStatusMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaPlayInfo"
.end annotation


# instance fields
.field private final mRecordUri:Landroid/net/Uri;

.field private final mStartTime:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2
    .param p1, "recordUri"    # Landroid/net/Uri;

    .prologue
    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;->mRecordUri:Landroid/net/Uri;

    .line 274
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;->mStartTime:J

    .line 275
    return-void
.end method


# virtual methods
.method public getRecordUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;->mRecordUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 282
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaPlayInfo;->mStartTime:J

    return-wide v0
.end method
