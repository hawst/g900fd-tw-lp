.class public Lcom/samsung/android/providers/context/log/RecordAudioLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "RecordAudioLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 38
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "app_id"

    aput-object v3, v1, v2

    const-string v2, "uri"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "stop_time"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "start_time"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "stoptime"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "starttime"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "duration"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/RecordAudioLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 45
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "record_audio"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_RECORD_AUDIO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/RecordAudioLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 49
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    .line 50
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->supportUpdate:Z

    .line 52
    new-instance v1, Lcom/samsung/android/providers/context/log/RecordAudioLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/RecordAudioLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/RecordAudioLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 96
    sget-object v1, Lcom/samsung/android/providers/context/log/RecordAudioLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 97
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 160
    sget-object v0, Lcom/samsung/android/providers/context/log/RecordAudioLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 161
    return-void
.end method


# virtual methods
.method protected Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 12
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v10, 0x0

    .line 101
    const-string v6, "start_time"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 102
    const-string v6, "starttime"

    const-string v7, "start_time"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/providers/context/util/ContextLogger;->convertStringToTime(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 117
    :cond_0
    :goto_0
    const-string v6, "stop_time"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 118
    const-string v6, "stoptime"

    const-string v7, "stop_time"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/providers/context/util/ContextLogger;->convertStringToTime(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 133
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v6

    return-object v6

    .line 103
    :cond_2
    const-string v6, "starttime"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 106
    const-string v6, "starttime"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 107
    .local v0, "startTime":Ljava/lang/Long;
    if-eqz v0, :cond_3

    .line 108
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 113
    .local v1, "startTimeDate":Ljava/util/Date;
    :goto_2
    new-instance v2, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 114
    .local v2, "startTimeString":Ljava/lang/String;
    const-string v6, "start_time"

    invoke-virtual {p1, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    .end local v1    # "startTimeDate":Ljava/util/Date;
    .end local v2    # "startTimeString":Ljava/lang/String;
    :cond_3
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v10, v11}, Ljava/util/Date;-><init>(J)V

    .restart local v1    # "startTimeDate":Ljava/util/Date;
    goto :goto_2

    .line 119
    .end local v0    # "startTime":Ljava/lang/Long;
    .end local v1    # "startTimeDate":Ljava/util/Date;
    :cond_4
    const-string v6, "stoptime"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 122
    const-string v6, "stoptime"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 123
    .local v3, "stopTime":Ljava/lang/Long;
    if-eqz v3, :cond_5

    .line 124
    new-instance v4, Ljava/util/Date;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 129
    .local v4, "stopTimeDate":Ljava/util/Date;
    :goto_3
    new-instance v5, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 130
    .local v5, "stopTimeString":Ljava/lang/String;
    const-string v6, "stop_time"

    invoke-virtual {p1, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 126
    .end local v4    # "stopTimeDate":Ljava/util/Date;
    .end local v5    # "stopTimeString":Ljava/lang/String;
    :cond_5
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v10, v11}, Ljava/util/Date;-><init>(J)V

    .restart local v4    # "stopTimeDate":Ljava/util/Date;
    goto :goto_3
.end method

.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 138
    const/4 v1, 0x0

    .line 141
    .local v1, "uri":Ljava/lang/String;
    :try_start_0
    const-string v2, "uri"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 147
    :goto_0
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 148
    :cond_0
    const-string v2, "ContextFramework"

    const-string v3, "RecordAudio : uri is invalid"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_1
    const/4 v2, 0x1

    return v2

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "ex":Ljava/lang/NullPointerException;
    const-string v2, "ContextFramework"

    const-string v3, "RecordAudio : ValidBoundaryCheck - NullPointerException."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
