.class Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;
.super Landroid/os/Handler;
.source "ContextApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/ContextApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContextHandler"
.end annotation


# instance fields
.field private final mApplication:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/providers/context/ContextApplication;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/ContextApplication;)V
    .locals 1
    .param p1, "application"    # Lcom/samsung/android/providers/context/ContextApplication;

    .prologue
    .line 259
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 260
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;->mApplication:Ljava/lang/ref/WeakReference;

    .line 261
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 19
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 265
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;->mApplication:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/providers/context/ContextApplication;

    .line 266
    .local v14, "application":Lcom/samsung/android/providers/context/ContextApplication;
    if-nez v14, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 272
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v15

    .line 274
    .local v15, "bundle":Landroid/os/Bundle;
    if-eqz v15, :cond_0

    .line 275
    const-string v4, "Values"

    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 276
    .local v2, "values":Landroid/content/ContentValues;
    const-string v4, "TableName"

    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 277
    .local v3, "table":Ljava/lang/String;
    const-string v4, "Uri"

    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 278
    .local v1, "uri":Landroid/net/Uri;
    const-string v4, "UseNotify"

    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    .line 279
    .local v18, "useNotify":Ljava/lang/Boolean;
    const-string v4, "UseBroadcast"

    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    .line 280
    .local v17, "useBroadcast":Ljava/lang/Boolean;
    const-string v4, "RecordsCount"

    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 281
    .local v6, "recordsCount":I
    const-string v4, "MaxRecordsCount"

    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 283
    .local v7, "maxRecordsCount":I
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-static/range {v1 .. v7}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->insertLog(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZZII)Landroid/net/Uri;

    goto :goto_0

    .line 288
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "values":Landroid/content/ContentValues;
    .end local v3    # "table":Ljava/lang/String;
    .end local v6    # "recordsCount":I
    .end local v7    # "maxRecordsCount":I
    .end local v15    # "bundle":Landroid/os/Bundle;
    .end local v17    # "useBroadcast":Ljava/lang/Boolean;
    .end local v18    # "useNotify":Ljava/lang/Boolean;
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v16

    .line 290
    .local v16, "surveyBundle":Landroid/os/Bundle;
    if-eqz v16, :cond_0

    .line 291
    const-string v4, "Values"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 292
    .restart local v2    # "values":Landroid/content/ContentValues;
    const-string v4, "TableName"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 293
    .restart local v3    # "table":Ljava/lang/String;
    const-string v4, "Uri"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 294
    .restart local v1    # "uri":Landroid/net/Uri;
    const-string v4, "UseNotify"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    .line 295
    .restart local v18    # "useNotify":Ljava/lang/Boolean;
    const-string v4, "RecordsCount"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 296
    .restart local v6    # "recordsCount":I
    const-string v4, "MaxRecordsCount"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 298
    .restart local v7    # "maxRecordsCount":I
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move v12, v6

    move v13, v7

    invoke-static/range {v8 .. v13}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->insertSurveyLog(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZII)Landroid/net/Uri;

    goto/16 :goto_0

    .line 303
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "values":Landroid/content/ContentValues;
    .end local v3    # "table":Ljava/lang/String;
    .end local v6    # "recordsCount":I
    .end local v7    # "maxRecordsCount":I
    .end local v16    # "surveyBundle":Landroid/os/Bundle;
    .end local v18    # "useNotify":Ljava/lang/Boolean;
    :pswitch_3
    # invokes: Lcom/samsung/android/providers/context/ContextApplication;->delayedInitiation()V
    invoke-static {v14}, Lcom/samsung/android/providers/context/ContextApplication;->access$000(Lcom/samsung/android/providers/context/ContextApplication;)V

    goto/16 :goto_0

    .line 307
    :pswitch_4
    const-string v4, "ContextFramework"

    const-string v5, "Start LocationTracker..."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    invoke-static {}, Lcom/samsung/android/providers/context/status/LocationTracker;->getInstance()Lcom/samsung/android/providers/context/status/LocationTracker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/status/LocationTracker;->enable()V

    goto/16 :goto_0

    .line 312
    :pswitch_5
    const-string v4, "ContextFramework"

    const-string v5, "Stop LocationTracker..."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    invoke-static {}, Lcom/samsung/android/providers/context/status/LocationTracker;->getInstance()Lcom/samsung/android/providers/context/status/LocationTracker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/status/LocationTracker;->disable()V

    goto/16 :goto_0

    .line 317
    :pswitch_6
    const-string v4, "ContextFramework"

    const-string v5, "Start ActivityMonitor..."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    invoke-static {}, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->enable()V

    goto/16 :goto_0

    .line 322
    :pswitch_7
    const-string v4, "ContextFramework"

    const-string v5, "Stop ActivityMonitor..."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    invoke-static {}, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->disable()V

    goto/16 :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
