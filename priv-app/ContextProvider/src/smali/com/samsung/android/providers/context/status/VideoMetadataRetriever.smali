.class public Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;
.super Ljava/lang/Object;
.source "VideoMetadataRetriever.java"


# instance fields
.field private mContentUri:Ljava/lang/String;

.field private mGenres:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mYear:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mContentUri:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mTitle:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mGenres:Ljava/lang/String;

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mYear:I

    .line 37
    if-nez p1, :cond_0

    .line 38
    const-string v0, "ContextFramework:VideoMetadataRetriever"

    const-string v1, "Video content URI is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :goto_0
    return-void

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mContentUri:Ljava/lang/String;

    .line 41
    const-string v0, "ContextFramework:VideoMetadataRetriever"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video content URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->retrieveVideoMetadata()V

    goto :goto_0
.end method

.method public static isInaccessibleVideo(Ljava/lang/String;)Z
    .locals 4
    .param p0, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 124
    const/4 v2, 0x1

    .line 126
    .local v2, "isInaccessible":Z
    if-eqz p0, :cond_0

    .line 127
    const-string v3, "."

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 129
    .local v1, "indexOfLastPeriod":I
    if-lez v1, :cond_0

    .line 130
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "fileExtension":Ljava/lang/String;
    const-string v3, "wvm"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 133
    const/4 v2, 0x0

    .line 138
    .end local v0    # "fileExtension":Ljava/lang/String;
    .end local v1    # "indexOfLastPeriod":I
    :cond_0
    return v2
.end method

.method public static isVideoContent(Ljava/lang/String;)Z
    .locals 6
    .param p0, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 91
    const/4 v2, 0x0

    .line 93
    .local v2, "isVideo":Z
    invoke-static {p0}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->isInaccessibleVideo(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 96
    const/4 v2, 0x1

    .line 119
    :goto_0
    return v2

    .line 99
    :cond_0
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 102
    .local v3, "metaRetriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v3, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 104
    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    .line 105
    .local v1, "hasVideo":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v4, "yes"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    .line 106
    const/4 v2, 0x1

    .line 116
    .end local v1    # "hasVideo":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 112
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "ContextFramework:VideoMetadataRetriever"

    const-string v5, "fail to retrieve metadata from MediaMetadataRetriever"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v4, "ContextFramework:VideoMetadataRetriever"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private retrieveVideoMetadata()V
    .locals 7

    .prologue
    .line 54
    iget-object v5, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mContentUri:Ljava/lang/String;

    invoke-static {v5}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->isInaccessibleVideo(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 56
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 59
    .local v3, "metaRetriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mContentUri:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 61
    const/4 v5, 0x7

    invoke-virtual {v3, v5}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mTitle:Ljava/lang/String;

    .line 63
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 65
    .local v4, "yearStr":Ljava/lang/String;
    :try_start_1
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mYear:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 70
    :goto_0
    const/4 v5, 0x6

    :try_start_2
    invoke-virtual {v3, v5}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    .line 71
    .local v2, "genres":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 72
    const/16 v5, 0x1e

    invoke-virtual {v3, v5}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    .line 75
    :cond_0
    if-eqz v2, :cond_1

    .line 76
    iput-object v2, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mGenres:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 85
    .end local v2    # "genres":Ljava/lang/String;
    .end local v4    # "yearStr":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 87
    .end local v3    # "metaRetriever":Landroid/media/MediaMetadataRetriever;
    :cond_2
    return-void

    .line 66
    .restart local v3    # "metaRetriever":Landroid/media/MediaMetadataRetriever;
    .restart local v4    # "yearStr":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    const-string v5, "ContextFramework:VideoMetadataRetriever"

    const-string v6, "Fails to retrieve year information from video."

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 78
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v4    # "yearStr":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 81
    .local v1, "ex":Ljava/lang/Exception;
    const-string v5, "ContextFramework:VideoMetadataRetriever"

    const-string v6, "fail to retrieve metadata from MediaMetadataRetriever"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const-string v5, "ContextFramework:VideoMetadataRetriever"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public getCotentUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mContentUri:Ljava/lang/String;

    return-object v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mGenres:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getYear()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->mYear:I

    return v0
.end method
