.class public Lcom/samsung/android/providers/context/status/MediaStatusMonitor$MediaStatusChangeListener;
.super Landroid/content/BroadcastReceiver;
.source "MediaStatusMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/MediaStatusMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaStatusChangeListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 231
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 233
    .local v1, "bundle":Landroid/os/Bundle;
    const/4 v2, -0x1

    .line 234
    .local v2, "contentId":I
    const/4 v3, 0x0

    .line 235
    .local v3, "contentUri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 237
    .local v4, "operation":Ljava/lang/String;
    const-string v5, "ContextFramework:MediaStatusMonitor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Receives an action = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    if-nez v1, :cond_0

    .line 240
    const-string v5, "ContextFramework:MediaStatusMonitor"

    const-string v6, "There is no bundle in the broadcasted media Intent"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :goto_0
    return-void

    .line 244
    :cond_0
    const-string v5, "ID"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 246
    const-string v5, "URI"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 247
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_2

    .line 248
    :cond_1
    const-string v5, "ContextFramework:MediaStatusMonitor"

    const-string v6, "There is no content URI in the broadcasted media Intent"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 252
    :cond_2
    const-string v5, "TYPE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 253
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_4

    .line 254
    :cond_3
    const-string v5, "ContextFramework:MediaStatusMonitor"

    const-string v6, "There is no (operation) type in the broadcasted media Intent"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 258
    :cond_4
    invoke-static {v3}, Lcom/samsung/android/providers/context/status/VideoMetadataRetriever;->isVideoContent(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 259
    invoke-static {}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->getInstance()Lcom/samsung/android/providers/context/status/MediaStatusMonitor;

    move-result-object v5

    invoke-virtual {v5, v4, v2, v3}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->processVideoEvent(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 261
    :cond_5
    invoke-static {}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->getInstance()Lcom/samsung/android/providers/context/status/MediaStatusMonitor;

    move-result-object v5

    invoke-virtual {v5, v4, v2, v3}, Lcom/samsung/android/providers/context/status/MediaStatusMonitor;->processMusicEvent(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method
