.class public final Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
.super Ljava/lang/Object;
.source "DeviceStatusMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;,
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$GpsMonitor;,
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;,
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;,
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;,
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;,
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;,
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;,
        Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$DummyMonitor;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

.field private static sIsEnabled:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceStatus:I

.field private mDeviceStatus_version1:I

.field private final mInternalMonitors:[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sInstance:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    .line 56
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sIsEnabled:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;

    invoke-direct {v2, p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;

    invoke-direct {v2, p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$HeadsetMonitor;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;

    invoke-direct {v2, p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;

    invoke-direct {v2, p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$GpsMonitor;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$GpsMonitor;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mInternalMonitors:[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;

    .line 75
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mContext:Landroid/content/Context;

    .line 76
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->restoreLastStatusFromDb()V

    .line 77
    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBooleanVersion1(IZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBoolean(IZ)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
    .locals 2

    .prologue
    .line 68
    const-class v1, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sInstance:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sInstance:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    .line 71
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sInstance:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private insertDeviceStatusChangeLog(I[Ljava/lang/String;)V
    .locals 5
    .param p1, "mask"    # I
    .param p2, "timestampTimeZone"    # [Ljava/lang/String;

    .prologue
    .line 156
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 157
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 158
    .local v2, "row":Landroid/content/ContentValues;
    const-string v3, "changed_status"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 159
    const-string v3, "device_status"

    iget v4, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 160
    if-eqz p2, :cond_0

    .line 161
    const-string v3, "timestamp"

    const/4 v4, 0x0

    aget-object v4, p2, v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v3, "time_zone"

    const/4 v4, 0x1

    aget-object v4, p2, v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_0
    :try_start_0
    sget-object v3, Lcom/samsung/android/providers/context/log/ContextLogContract$ChangeDeviceStatus;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_0
    return-void

    .line 166
    :catch_0
    move-exception v1

    .line 167
    .local v1, "ex":Ljava/lang/Exception;
    const-string v3, "ContextFramework"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private restoreLastStatusFromDb()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 172
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "device_status"

    aput-object v1, v2, v0

    .line 174
    .local v2, "projectionIn":[Ljava/lang/String;
    const-string v0, "change_device_status"

    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$ChangeDeviceStatus;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_id DESC LIMIT 1"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->queryLog(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 178
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 179
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 180
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 181
    const-string v0, "device_status"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    .line 183
    :cond_0
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_1

    .line 184
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read from DB: device_status("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 188
    :cond_2
    return-void
.end method

.method private setDeviceStatusBoolean(IZ)Z
    .locals 1
    .param p1, "mask"    # I
    .param p2, "value"    # Z

    .prologue
    .line 132
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBooleanWithTimestamp(IZ[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized setDeviceStatusBooleanVersion1(IZ)V
    .locals 2
    .param p1, "mask"    # I
    .param p2, "value"    # Z

    .prologue
    .line 121
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 122
    :try_start_0
    iget v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :goto_0
    monitor-exit p0

    return-void

    .line 124
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setDeviceStatusBooleanWithTimestamp(IZ[Ljava/lang/String;)Z
    .locals 4
    .param p1, "mask"    # I
    .param p2, "value"    # Z
    .param p3, "timestampTimeZone"    # [Ljava/lang/String;

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    .line 138
    .local v1, "old":I
    if-eqz p2, :cond_1

    .line 139
    iget v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    or-int/2addr v2, p1

    iput v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    .line 140
    iget v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I

    or-int/2addr v2, p1

    iput v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I

    .line 146
    :goto_0
    const/4 v0, 0x0

    .line 147
    .local v0, "isChanged":Z
    iget v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    if-eq v1, v2, :cond_0

    .line 149
    invoke-direct {p0, p1, p3}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->insertDeviceStatusChangeLog(I[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    const/4 v0, 0x1

    .line 152
    :cond_0
    monitor-exit p0

    return v0

    .line 142
    .end local v0    # "isChanged":Z
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    xor-int/lit8 v3, p1, -0x1

    and-int/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus:I

    .line 143
    iget v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I

    xor-int/lit8 v3, p1, -0x1

    and-int/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 136
    .end local v1    # "old":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method


# virtual methods
.method public disable()V
    .locals 5

    .prologue
    .line 90
    sget-boolean v4, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sIsEnabled:Z

    if-eqz v4, :cond_1

    .line 91
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mInternalMonitors:[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;

    .local v0, "arr$":[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 92
    .local v3, "monitor":Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    iget-object v4, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mContext:Landroid/content/Context;

    invoke-interface {v3, v4}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;->disable(Landroid/content/Context;)V

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 95
    .end local v3    # "monitor":Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    :cond_0
    const/4 v4, 0x0

    sput-boolean v4, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sIsEnabled:Z

    .line 97
    .end local v0    # "arr$":[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :cond_1
    return-void
.end method

.method public enable()V
    .locals 5

    .prologue
    .line 80
    sget-boolean v4, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sIsEnabled:Z

    if-nez v4, :cond_1

    .line 81
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mInternalMonitors:[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;

    .local v0, "arr$":[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 82
    .local v3, "monitor":Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    iget-object v4, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mContext:Landroid/content/Context;

    invoke-interface {v3, v4}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;->enable(Landroid/content/Context;)V

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    .end local v3    # "monitor":Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    :cond_0
    const/4 v4, 0x1

    sput-boolean v4, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sIsEnabled:Z

    .line 87
    .end local v0    # "arr$":[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :cond_1
    return-void
.end method

.method public getDeviceContextVersion1()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mDeviceStatus_version1:I

    return v0
.end method

.method public onUserBackground()V
    .locals 5

    .prologue
    .line 108
    sget-boolean v4, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sIsEnabled:Z

    if-eqz v4, :cond_0

    .line 109
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mInternalMonitors:[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;

    .local v0, "arr$":[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 110
    .local v3, "monitor":Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    iget-object v4, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mContext:Landroid/content/Context;

    invoke-interface {v3, v4}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;->onUserBackground(Landroid/content/Context;)V

    .line 109
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 113
    .end local v0    # "arr$":[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "monitor":Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    :cond_0
    return-void
.end method

.method public onUserForeground()V
    .locals 5

    .prologue
    .line 100
    sget-boolean v4, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->sIsEnabled:Z

    if-eqz v4, :cond_0

    .line 101
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mInternalMonitors:[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;

    .local v0, "arr$":[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 102
    .local v3, "monitor":Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    iget-object v4, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->mContext:Landroid/content/Context;

    invoke-interface {v3, v4}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;->onUserForeground(Landroid/content/Context;)V

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    .end local v0    # "arr$":[Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "monitor":Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;
    :cond_0
    return-void
.end method
