.class public abstract Lcom/samsung/android/providers/context/log/db/SecureDbHelper;
.super Landroid/database/sqlite/SQLiteSecureOpenHelper;
.source "SecureDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/log/db/SecureDbHelper$CustomSecErrHandler;
    }
.end annotation


# static fields
.field private static final mIsSecure:Z

.field private static final mPrefix:Ljava/lang/String;


# instance fields
.field private mDBFileDeleted:Z

.field private mDbFilesObserver:Landroid/os/FileObserver;

.field private final mPassword:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mPrefix:Ljava/lang/String;

    .line 60
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mIsSecure:Z

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    const-string v0, "Secure"

    sput-object v0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mPrefix:Ljava/lang/String;

    .line 63
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mIsSecure:Z

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dbName"    # Ljava/lang/String;
    .param p3, "dbVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/providers/context/security/NoKeyException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/samsung/android/providers/context/log/db/SecureDbHelper$CustomSecErrHandler;

    invoke-direct {v5, v3}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper$CustomSecErrHandler;-><init>(Lcom/samsung/android/providers/context/log/db/SecureDbHelper$1;)V

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    invoke-direct/range {v0 .. v5}, Landroid/database/sqlite/SQLiteSecureOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;ILandroid/database/DatabaseErrorHandler;)V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDBFileDeleted:Z

    .line 74
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    invoke-static {}, Lcom/samsung/android/providers/context/security/SecureDbManager;->getPassword()[B

    move-result-object v10

    .line 77
    .local v10, "pwd":[B
    if-eqz v10, :cond_1

    array-length v0, v10

    if-lez v0, :cond_1

    .line 78
    iput-object v10, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mPassword:[B

    .line 89
    .end local v10    # "pwd":[B
    :goto_0
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LogDatabase : using "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->getDatabaseName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->startWatchDBFilesObserver(Landroid/content/Context;)V

    .line 94
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->getDatabaseHandlerList()Ljava/util/ArrayList;

    move-result-object v8

    .line 95
    .local v8, "handlerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;>;"
    if-eqz v8, :cond_3

    .line 96
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 97
    .local v6, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v6, :cond_3

    .line 98
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 99
    .local v7, "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    invoke-virtual {v7, v6}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->loadExtraInfo(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 81
    .end local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v7    # "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    .end local v8    # "handlerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .restart local v10    # "pwd":[B
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mPassword:[B

    .line 82
    const-string v0, "ContextFramework"

    const-string v1, "Cannot get password for secure DB"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    new-instance v0, Lcom/samsung/android/providers/context/security/NoKeyException;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/security/NoKeyException;-><init>()V

    throw v0

    .line 86
    .end local v10    # "pwd":[B
    :cond_2
    iput-object v3, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mPassword:[B

    goto :goto_0

    .line 103
    .restart local v8    # "handlerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;>;"
    :cond_3
    return-void
.end method

.method static synthetic access$102(Lcom/samsung/android/providers/context/log/db/SecureDbHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/log/db/SecureDbHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDBFileDeleted:Z

    return p1
.end method

.method private startWatchDBFilesObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    iget-object v2, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDbFilesObserver:Landroid/os/FileObserver;

    if-eqz v2, :cond_1

    .line 152
    iget-object v2, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDbFilesObserver:Landroid/os/FileObserver;

    invoke-virtual {v2}, Landroid/os/FileObserver;->startWatching()V

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDBFileDeleted:Z

    .line 158
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 159
    .local v0, "fileDir":Ljava/io/File;
    if-nez v0, :cond_2

    .line 160
    const-string v2, "ContextFramework"

    const-string v3, "LogDatabase: file directory not existing"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    .line 165
    .local v1, "watchPath":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/providers/context/log/db/SecureDbHelper$1;

    const/16 v3, 0x200

    invoke-direct {v2, p0, v1, v3}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper$1;-><init>(Lcom/samsung/android/providers/context/log/db/SecureDbHelper;Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDbFilesObserver:Landroid/os/FileObserver;

    .line 176
    iget-object v2, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDbFilesObserver:Landroid/os/FileObserver;

    invoke-virtual {v2}, Landroid/os/FileObserver;->startWatching()V

    .line 178
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_0

    .line 179
    const-string v2, "ContextFramework"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LogDatabase : watching "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected abstract getDatabaseHandlerList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;",
            ">;"
        }
    .end annotation
.end method

.method public getDatabaseName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    invoke-super {p0}, Landroid/database/sqlite/SQLiteSecureOpenHelper;->getDatabaseName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 195
    :try_start_0
    sget-boolean v1, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mIsSecure:Z

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mPassword:[B

    invoke-super {p0, v1}, Landroid/database/sqlite/SQLiteSecureOpenHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 203
    :goto_0
    return-object v1

    .line 198
    :cond_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteSecureOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ContextFramework"

    const-string v2, "Readable DB open fails"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 209
    :try_start_0
    sget-boolean v1, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mIsSecure:Z

    if-eqz v1, :cond_0

    .line 210
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mPassword:[B

    invoke-super {p0, v1}, Landroid/database/sqlite/SQLiteSecureOpenHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 217
    :goto_0
    return-object v1

    .line 212
    :cond_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteSecureOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ContextFramework"

    const-string v2, "Writable DB open fails"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->mDBFileDeleted:Z

    return v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->getDatabaseHandlerList()Ljava/util/ArrayList;

    move-result-object v2

    .line 108
    .local v2, "handlerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;>;"
    if-eqz v2, :cond_0

    .line 109
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 112
    :try_start_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 113
    .local v1, "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    invoke-virtual {v1, p1}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 115
    .end local v1    # "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v4

    iget-boolean v4, v4, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v4, :cond_0

    .line 117
    const-string v4, "ContextFramework"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error executing SQL "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    :goto_1
    return-void

    .line 122
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 123
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;->getDatabaseHandlerList()Ljava/util/ArrayList;

    move-result-object v2

    .line 130
    .local v2, "handlerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;>;"
    if-eqz v2, :cond_0

    .line 131
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 134
    :try_start_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 135
    .local v1, "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    invoke-virtual {v1, p1, p2, p3}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->upgradeTable(Landroid/database/sqlite/SQLiteDatabase;II)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    .end local v1    # "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v4

    iget-boolean v4, v4, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v4, :cond_0

    .line 139
    const-string v4, "ContextFramework"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error executing SQL "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    :goto_1
    return-void

    .line 144
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 145
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1
.end method
