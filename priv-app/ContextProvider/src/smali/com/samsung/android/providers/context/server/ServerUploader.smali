.class public Lcom/samsung/android/providers/context/server/ServerUploader;
.super Ljava/lang/Object;
.source "ServerUploader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private isRegistered:Z

.field private final mContext:Landroid/content/Context;

.field private mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

.field private final mLogSender:Lcom/samsung/android/providers/context/server/LogSender;

.field private final mResultMsgHandler:Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContextFramework/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/providers/context/server/ServerUploader;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->isRegistered:Z

    .line 74
    sget-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v1, "Construct ServerUploader"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mContext:Landroid/content/Context;

    .line 76
    new-instance v0, Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;-><init>(Lcom/samsung/android/providers/context/server/ServerUploader;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mResultMsgHandler:Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;

    .line 77
    new-instance v0, Lcom/samsung/android/providers/context/server/LogGenerator;

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mResultMsgHandler:Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;

    invoke-direct {v0, v1, p1}, Lcom/samsung/android/providers/context/server/LogGenerator;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

    .line 78
    new-instance v0, Lcom/samsung/android/providers/context/server/LogSender;

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mResultMsgHandler:Lcom/samsung/android/providers/context/server/ServerUploader$ResultMsgHandler;

    iget-object v3, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/server/LogSender;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/android/providers/context/server/LogGenerator;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mLogSender:Lcom/samsung/android/providers/context/server/LogSender;

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/providers/context/server/ServerUploader;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/ServerUploader;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerUploader;->setRegistered()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/providers/context/server/ServerUploader;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/ServerUploader;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerUploader;->setDeregistered()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/providers/context/server/ServerUploader;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/ServerUploader;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerUploader;->setAvailable()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/providers/context/server/ServerUploader;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/ServerUploader;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerUploader;->setUnavailable()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/providers/context/server/ServerUploader;)Lcom/samsung/android/providers/context/server/LogSender;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/ServerUploader;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mLogSender:Lcom/samsung/android/providers/context/server/LogSender;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/providers/context/server/ServerUploader;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/ServerUploader;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->isRegistered:Z

    return p1
.end method

.method public static checkSentDate()J
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 100
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 101
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->add(II)V

    .line 102
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Ljava/util/GregorianCalendar;->set(II)V

    .line 103
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/GregorianCalendar;->set(II)V

    .line 104
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/GregorianCalendar;->set(II)V

    .line 105
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/GregorianCalendar;->set(II)V

    .line 106
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method private registSPPReceiver()V
    .locals 3

    .prologue
    .line 120
    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v2, "Register SPP broadcast receiver"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.logsample"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 123
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.logsample.DLC_FILTER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 125
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/android/providers/context/server/ServerUploader$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/providers/context/server/ServerUploader$1;-><init>(Lcom/samsung/android/providers/context/server/ServerUploader;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 185
    return-void
.end method

.method private register()V
    .locals 4

    .prologue
    .line 188
    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v2, "Register to spp"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-boolean v1, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->isRegistered:Z

    if-eqz v1, :cond_0

    .line 190
    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v2, "Already registered."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/server/ServerUploader;->startSendLog()V

    .line 201
    :goto_0
    return-void

    .line 195
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.REQUEST_REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 196
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_PACKAGENAME"

    const-class v2, Lcom/samsung/android/providers/context/ContextApplication;

    invoke-virtual {v2}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string v1, "EXTRA_INTENTFILTER"

    const-string v2, "com.sec.logsample.DLC_FILTER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "package name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-class v3, Lcom/samsung/android/providers/context/ContextApplication;

    invoke-virtual {v3}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", intent filter = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "com.sec.logsample.DLC_FILTER"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 200
    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v2, "Send broadcast : ACTION_REGI"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setAvailable()V
    .locals 2

    .prologue
    .line 240
    sget-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v1, "setAvailable()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->isRegistered:Z

    .line 242
    sget-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v1, "Log Sender Service is available"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    return-void
.end method

.method private setDeregistered()V
    .locals 2

    .prologue
    .line 227
    sget-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v1, "setDeregistered()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mLogSender:Lcom/samsung/android/providers/context/server/LogSender;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/server/LogSender;->stopSender()V

    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->isRegistered:Z

    .line 231
    return-void
.end method

.method private setRegistered()V
    .locals 2

    .prologue
    .line 221
    sget-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v1, "setRegistered()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->isRegistered:Z

    .line 223
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/server/ServerUploader;->startSendLog()V

    .line 224
    return-void
.end method

.method private setUnavailable()V
    .locals 2

    .prologue
    .line 234
    sget-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v1, "setUnavailable"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mLogSender:Lcom/samsung/android/providers/context/server/LogSender;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/server/LogSender;->stopSender()V

    .line 236
    sget-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v1, "Log Sender has been stopped\n"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    return-void
.end method


# virtual methods
.method public start()V
    .locals 8

    .prologue
    .line 82
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v0

    .line 83
    .local v0, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v4, "SentDate"

    const-wide/16 v6, 0x0

    invoke-virtual {v0, v4, v6, v7}, Lcom/samsung/android/providers/context/ContextPreference;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 85
    .local v2, "sentDate":J
    invoke-static {}, Lcom/samsung/android/providers/context/server/ServerUploader;->checkSentDate()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    .line 86
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 87
    .local v1, "simpleFormat":Ljava/text/SimpleDateFormat;
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Current time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sent time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v5, "Already uploaded log"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    .end local v1    # "simpleFormat":Ljava/text/SimpleDateFormat;
    :goto_0
    return-void

    .line 91
    :cond_0
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v5, "Start log generator"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mLogGenerator:Lcom/samsung/android/providers/context/server/LogGenerator;

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/server/LogGenerator;->run()V

    .line 94
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerUploader;->registSPPReceiver()V

    .line 95
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/ServerUploader;->register()V

    goto :goto_0
.end method

.method public startSendLog()V
    .locals 2

    .prologue
    .line 246
    sget-object v0, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v1, "startSendLog()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/ServerUploader;->mLogSender:Lcom/samsung/android/providers/context/server/LogSender;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/server/LogSender;->run()V

    .line 249
    return-void
.end method
