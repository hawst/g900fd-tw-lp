.class public Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
.super Ljava/lang/Object;
.source "PrivacyAccessControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;,
        Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    }
.end annotation


# static fields
.field private static mPrivacyAccessControl:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;


# instance fields
.field private final mTotalPermissionMaps:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 7

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    iput-object v5, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    .line 106
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->values()[Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 107
    .local v4, "type":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 108
    .local v2, "itemPermissionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    iget-object v5, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v6

    invoke-virtual {v5, v6, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    .end local v2    # "itemPermissionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    .end local v4    # "type":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    :cond_0
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    .locals 2

    .prologue
    .line 113
    const-class v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mPrivacyAccessControl:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mPrivacyAccessControl:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    .line 117
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mPrivacyAccessControl:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;JJJ)V
    .locals 9
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .param p3, "startTime"    # J
    .param p5, "stopTime"    # J
    .param p7, "consentId"    # J

    .prologue
    .line 209
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {p2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 210
    .local v0, "itemPermissionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 212
    .local v1, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    if-nez v1, :cond_0

    .line 213
    new-instance v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .end local v1    # "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    invoke-direct {v1}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;-><init>()V

    .line 214
    .restart local v1    # "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-wide v2, p3

    move-wide v4, p5

    move-wide/from16 v6, p7

    .line 217
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->insertConsentTime(JJJ)V

    .line 218
    return-void
.end method

.method public getLastConsentId(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)J
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .prologue
    .line 190
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {p2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 192
    .local v0, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    if-nez v0, :cond_0

    .line 193
    const-wide/16 v2, -0x1

    .line 196
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->getLastConsentId()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getPermittedAppIdList(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Ljava/util/List;
    .locals 8
    .param p1, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v6, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 249
    .local v2, "itemPermissionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 251
    .local v1, "appIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 253
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 254
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 255
    .local v4, "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 256
    .local v0, "appId":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 258
    .local v5, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    invoke-virtual {v5}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->isPermittedNow()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 259
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 263
    .end local v0    # "appId":Ljava/lang/String;
    .end local v4    # "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    .end local v5    # "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    :cond_1
    return-object v1
.end method

.method public getPermittedTypeList(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 269
    .local v3, "permittedTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 270
    iget-object v5, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    invoke-static {v5}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getPrivacyItem(I)Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    move-result-object v4

    .line 271
    .local v4, "privacyItem":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    iget-object v5, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 272
    .local v2, "permissionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 274
    .local v1, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->isPermittedNow()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 275
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 279
    .end local v1    # "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    .end local v2    # "permissionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    .end local v4    # "privacyItem":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    :cond_1
    return-object v3
.end method

.method public getPrivacySelection(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Ljava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .param p3, "originalSelection"    # Ljava/lang/String;

    .prologue
    .line 121
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 123
    .local v2, "buffer":Ljava/lang/StringBuffer;
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_0

    .line 124
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    const-string v14, " AND "

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v15

    invoke-virtual {v14, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 130
    .local v5, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    if-nez v5, :cond_1

    .line 131
    const-string v14, "timestamp_utc = -1"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 166
    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    return-object v14

    .line 134
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->getConsentTimeSet()Ljava/util/TreeSet;

    move-result-object v3

    .line 135
    .local v3, "consentTimeSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 136
    .local v4, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    const-string v14, "("

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 139
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/util/Pair;

    .line 140
    .local v13, "timeSpan":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    iget-object v14, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 141
    .local v6, "startTime":J
    iget-object v14, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 142
    .local v10, "stopTime":J
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v12

    .line 143
    .local v12, "timeFormatUtc":Ljava/text/DateFormat;
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 144
    .local v8, "startTimeStr":Ljava/lang/String;
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 146
    .local v9, "stopTimeStr":Ljava/lang/String;
    const-wide/16 v14, 0x0

    cmp-long v14, v10, v14

    if-ltz v14, :cond_3

    .line 147
    const-string v14, "(timestamp_utc BETWEEN \'"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 148
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 149
    const-string v14, "\' AND \'"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 151
    const-string v14, "\')"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 158
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 159
    const-string v14, " OR "

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 153
    :cond_3
    const-string v14, "(timestamp_utc > \'"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 154
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 155
    const-string v14, "\')"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 163
    .end local v6    # "startTime":J
    .end local v8    # "startTimeStr":Ljava/lang/String;
    .end local v9    # "stopTimeStr":Ljava/lang/String;
    .end local v10    # "stopTime":J
    .end local v12    # "timeFormatUtc":Ljava/text/DateFormat;
    .end local v13    # "timeSpan":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    :cond_4
    const-string v14, ")"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z
    .locals 7
    .param p1, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .prologue
    .line 229
    const/4 v4, 0x0

    .line 230
    .local v4, "result":Z
    iget-object v5, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 232
    .local v0, "itemPermissionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 234
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 235
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 236
    .local v2, "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 238
    .local v3, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    invoke-virtual {v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->isPermittedNow()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 239
    const/4 v4, 0x1

    .line 244
    .end local v2    # "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;>;"
    .end local v3    # "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    :cond_1
    return v4
.end method

.method public isConsentNow(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .prologue
    .line 180
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {p2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 182
    .local v0, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    if-nez v0, :cond_0

    .line 183
    const/4 v1, 0x0

    .line 186
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->isPermittedNow()Z

    move-result v1

    goto :goto_0
.end method

.method public isConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;J)Z
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .param p3, "time"    # J

    .prologue
    .line 170
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {p2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 172
    .local v0, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    if-nez v0, :cond_0

    .line 173
    const/4 v1, 0x0

    .line 176
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p3, p4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->isPermittedTime(J)Z

    move-result v1

    goto :goto_0
.end method

.method public updateLastConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;J)V
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .param p3, "stopTime"    # J

    .prologue
    .line 221
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->mTotalPermissionMaps:Landroid/util/SparseArray;

    invoke-virtual {p2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;

    .line 223
    .local v0, "permission":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;
    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {v0, p3, p4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyPermission;->updateLastConsentTime(J)V

    .line 226
    :cond_0
    return-void
.end method
