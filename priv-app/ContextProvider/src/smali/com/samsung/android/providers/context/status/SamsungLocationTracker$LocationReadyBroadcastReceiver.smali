.class public Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SamsungLocationTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/SamsungLocationTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LocationReadyBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/status/SamsungLocationTracker;


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/status/SamsungLocationTracker;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;->this$0:Lcom/samsung/android/providers/context/status/SamsungLocationTracker;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 318
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 320
    .local v0, "action":Ljava/lang/String;
    const-string v1, "ContextFramework.SamsungLocationTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receives an action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    const-string v1, "com.samsung.location.SERVICE_READY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/android/providers/context/ContextApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 324
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;->this$0:Lcom/samsung/android/providers/context/status/SamsungLocationTracker;

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->enable()V

    .line 326
    :cond_0
    return-void
.end method

.method public register()V
    .locals 2

    .prologue
    .line 311
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 312
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.location.SERVICE_READY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 313
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/samsung/android/providers/context/ContextApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 314
    return-void
.end method
