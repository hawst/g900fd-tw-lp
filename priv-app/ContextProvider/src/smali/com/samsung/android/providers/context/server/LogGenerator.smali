.class public Lcom/samsung/android/providers/context/server/LogGenerator;
.super Ljava/lang/Object;
.source "LogGenerator.java"


# static fields
.field private static final ALWAYS_LOG_DB_URI:Landroid/net/Uri;

.field private static final MANAGE_APP_SURVEY_CONTENT_URI:Landroid/net/Uri;

.field private static final MODEL_NAME:Ljava/lang/String;

.field private static final PLAY_MUSIC_CONTENT_URI:Landroid/net/Uri;

.field private static final PLAY_VIDEO_CONTENT_URI:Landroid/net/Uri;

.field private static final RECORD_VIDEO_CONTENT_URI:Landroid/net/Uri;

.field private static final REPORT_APP_ERROR_SURVEY_CONTENT_URI:Landroid/net/Uri;

.field private static final SURVEY_LOG_DB_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;

.field private static final TAKE_PHOTO_CONTENT_URI:Landroid/net/Uri;

.field private static final TRACK_ROAMING_CONTENT_URI:Landroid/net/Uri;

.field private static final USE_APP_CONTENT_URI:Landroid/net/Uri;

.field private static final USE_APP_FEATURE_SURVEY_CONTENT_URI:Landroid/net/Uri;

.field private static final USE_APP_SURVEY_CONTENT_URI:Landroid/net/Uri;

.field private static final USE_BATTERY_SURVEY_CONTENT_URI:Landroid/net/Uri;

.field private static final USE_MEMORY_SURVEY_CONTENT_URI:Landroid/net/Uri;

.field private static final USE_WIFI_CONTENT_URI:Landroid/net/Uri;

.field private static isAlwaysUpload:Z

.field private static isSurveyUpload:Z


# instance fields
.field private bodyUploadTime:Ljava/lang/String;

.field private final cr:Landroid/content/ContentResolver;

.field private fwVer:Ljava/lang/String;

.field private final mAppUsageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mMusicGenreMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final mQueryCal:Ljava/util/GregorianCalendar;

.field private final mResultMsgHandler:Landroid/os/Handler;

.field private final mSurveyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mTelephonyMgr:Landroid/telephony/TelephonyManager;

.field private final mVideoGenreMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mcc:Ljava/lang/String;

.field private mnc:Ljava/lang/String;

.field private normalQueryBoundary:[Ljava/lang/String;

.field private shealthQueryBoundary:[Ljava/lang/String;

.field private surveyQueryBoundary:[Ljava/lang/String;

.field private uploadMcc:Ljava/lang/String;

.field private uploadMnc:Ljava/lang/String;

.field private zoneOffset:I

.field private zoneOffsetMil:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/providers/context/server/LogGenerator;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    .line 73
    const-string v0, "content://com.samsung.android.providers.context.log.take_photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAKE_PHOTO_CONTENT_URI:Landroid/net/Uri;

    .line 75
    const-string v0, "content://com.samsung.android.providers.context.log.record_video"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->RECORD_VIDEO_CONTENT_URI:Landroid/net/Uri;

    .line 77
    const-string v0, "content://com.samsung.android.providers.context.log.play_video"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->PLAY_VIDEO_CONTENT_URI:Landroid/net/Uri;

    .line 79
    const-string v0, "content://com.samsung.android.providers.context.log.play_music"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->PLAY_MUSIC_CONTENT_URI:Landroid/net/Uri;

    .line 81
    const-string v0, "content://com.samsung.android.providers.context.log.use_wifi"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_WIFI_CONTENT_URI:Landroid/net/Uri;

    .line 83
    const-string v0, "content://com.samsung.android.providers.context.log.use_app"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_APP_CONTENT_URI:Landroid/net/Uri;

    .line 85
    const-string v0, "content://com.samsung.android.providers.context.log.track_roaming"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TRACK_ROAMING_CONTENT_URI:Landroid/net/Uri;

    .line 89
    const-string v0, "content://com.samsung.android.providers.context.log.use_app_survey"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_APP_SURVEY_CONTENT_URI:Landroid/net/Uri;

    .line 91
    const-string v0, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_APP_FEATURE_SURVEY_CONTENT_URI:Landroid/net/Uri;

    .line 93
    const-string v0, "content://com.samsung.android.providers.context.log.manage_app_survey"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->MANAGE_APP_SURVEY_CONTENT_URI:Landroid/net/Uri;

    .line 95
    const-string v0, "content://com.samsung.android.providers.context.log.report_app_error_survey"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->REPORT_APP_ERROR_SURVEY_CONTENT_URI:Landroid/net/Uri;

    .line 97
    const-string v0, "content://com.samsung.android.providers.context.log.use_battery_survey"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_BATTERY_SURVEY_CONTENT_URI:Landroid/net/Uri;

    .line 99
    const-string v0, "content://com.samsung.android.providers.context.log.use_memory_survey"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_MEMORY_SURVEY_CONTENT_URI:Landroid/net/Uri;

    .line 103
    const-string v0, "content://com.samsung.android.providers.context.server/always_log"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->ALWAYS_LOG_DB_URI:Landroid/net/Uri;

    .line 105
    const-string v0, "content://com.samsung.android.providers.context.server/survey_log"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->SURVEY_LOG_DB_URI:Landroid/net/Uri;

    .line 177
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    .line 205
    sput-boolean v2, Lcom/samsung/android/providers/context/server/LogGenerator;->isAlwaysUpload:Z

    .line 206
    sput-boolean v2, Lcom/samsung/android/providers/context/server/LogGenerator;->isSurveyUpload:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;)V
    .locals 4
    .param p1, "msgHandler"    # Landroid/os/Handler;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    .line 192
    iput v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffset:I

    .line 193
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mcc:Ljava/lang/String;

    .line 194
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mnc:Ljava/lang/String;

    .line 195
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    .line 196
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    .line 197
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    .line 198
    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->fwVer:Ljava/lang/String;

    .line 201
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    .line 202
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->shealthQueryBoundary:[Ljava/lang/String;

    .line 203
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    .line 210
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "Construct LogGenerator"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iput-object p2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mContext:Landroid/content/Context;

    .line 213
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    .line 214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    .line 215
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    .line 217
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mQueryCal:Ljava/util/GregorianCalendar;

    .line 218
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mResultMsgHandler:Landroid/os/Handler;

    .line 219
    const-string v0, "phone"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    .line 220
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->cr:Landroid/content/ContentResolver;

    .line 221
    return-void
.end method

.method private static checkCursor(Landroid/database/Cursor;)Z
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 1293
    if-nez p0, :cond_0

    .line 1294
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v2, "Failed to query"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1302
    :goto_0
    return v0

    .line 1296
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 1297
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v2, "There is no information."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1302
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private checkUploadSetting()V
    .locals 4

    .prologue
    .line 1360
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v2, "Check upload service on/off"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    const/4 v0, 0x0

    .line 1364
    .local v0, "data":Landroid/os/Bundle;
    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_ALWAYS_SERVICE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-static {v1}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->getLastItem(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Landroid/os/Bundle;

    move-result-object v0

    .line 1365
    if-eqz v0, :cond_0

    .line 1366
    const-string v1, "ONOFF"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/samsung/android/providers/context/server/LogGenerator;->isAlwaysUpload:Z

    .line 1367
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Always upload service status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ONOFF"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1373
    :goto_0
    const/4 v0, 0x0

    .line 1374
    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-static {v1}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->getLastItem(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Landroid/os/Bundle;

    move-result-object v0

    .line 1375
    if-eqz v0, :cond_1

    .line 1376
    const-string v1, "ONOFF"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/samsung/android/providers/context/server/LogGenerator;->isSurveyUpload:Z

    .line 1377
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Survey upload service status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ONOFF"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    :goto_1
    return-void

    .line 1370
    :cond_0
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v2, "Always upload service status = false / bundle is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1379
    :cond_1
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v2, "Survey upload service status = false / bundle is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private generateLog()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 367
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getSimInfo()V

    .line 368
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v1

    .line 369
    .local v1, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v8, "SentDate"

    const-wide/16 v10, 0x0

    invoke-virtual {v1, v8, v10, v11}, Lcom/samsung/android/providers/context/ContextPreference;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 370
    .local v4, "lastSentDate":J
    iget-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mQueryCal:Ljava/util/GregorianCalendar;

    invoke-virtual {v8, v4, v5}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 372
    invoke-static {}, Lcom/samsung/android/providers/context/server/ServerUploader;->checkSentDate()J

    move-result-wide v2

    .line 374
    .local v2, "checkDate":J
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 375
    .local v0, "checkCal":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 377
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v8, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v7, v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 378
    .local v7, "simpleFormat":Ljava/text/SimpleDateFormat;
    sget-object v8, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sent date in pref : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mQueryCal:Ljava/util/GregorianCalendar;

    invoke-virtual {v10}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " / Check sent date : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkUploadSetting()V

    .line 381
    const-string v8, "ro.build.PDA"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->fwVer:Ljava/lang/String;

    .line 383
    :goto_0
    iget-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mQueryCal:Ljava/util/GregorianCalendar;

    invoke-virtual {v8}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    cmp-long v8, v8, v2

    if-gez v8, :cond_3

    .line 385
    iget-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mQueryCal:Ljava/util/GregorianCalendar;

    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->setNextDay(Ljava/util/Calendar;)V

    .line 386
    iget-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mQueryCal:Ljava/util/GregorianCalendar;

    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->writeSentDate(Ljava/util/Calendar;)V

    .line 387
    iget-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mQueryCal:Ljava/util/GregorianCalendar;

    invoke-direct {p0, v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->getBodyUploadTime(Ljava/util/GregorianCalendar;)V

    .line 388
    invoke-direct {p0, v12, v12}, Lcom/samsung/android/providers/context/server/LogGenerator;->getQueryBoundary(II)[Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    .line 389
    invoke-direct {p0, v13, v12}, Lcom/samsung/android/providers/context/server/LogGenerator;->getQueryBoundary(II)[Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->shealthQueryBoundary:[Ljava/lang/String;

    .line 391
    sget-boolean v8, Lcom/samsung/android/providers/context/server/LogGenerator;->isAlwaysUpload:Z

    if-eqz v8, :cond_0

    .line 392
    sget-object v8, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v9, "Generate Always log"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getCameraBody()V

    .line 396
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getCamcorderBody()V

    .line 397
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getVideoBody()V

    .line 398
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getMusicBody()V

    .line 399
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getWifiBody()V

    .line 400
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getAppUsageBody()V

    .line 401
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getRoamingBody()V

    .line 406
    :goto_1
    sget-boolean v8, Lcom/samsung/android/providers/context/server/LogGenerator;->isSurveyUpload:Z

    if-eqz v8, :cond_2

    .line 407
    sget-object v8, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v9, "Generate Survey log"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    const/4 v6, 0x0

    .line 410
    .local v6, "queryHour":I
    :goto_2
    const/16 v8, 0x18

    if-ge v6, v8, :cond_1

    .line 411
    sget-object v8, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Query Hour : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "~"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v6, 0x3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    const/4 v8, 0x2

    invoke-direct {p0, v8, v6}, Lcom/samsung/android/providers/context/server/LogGenerator;->getQueryBoundary(II)[Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    .line 414
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getUseAppSurveyBody()V

    .line 415
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getUseAppFeatureSurveyBody()V

    .line 417
    add-int/lit8 v6, v6, 0x3

    goto :goto_2

    .line 403
    .end local v6    # "queryHour":I
    :cond_0
    sget-object v8, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v9, "Always upload service off"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 420
    .restart local v6    # "queryHour":I
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    iput-object v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    .line 421
    sget-object v8, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Query time(UTC_Survey_Daily) : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    aget-object v10, v10, v12

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ~ "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    aget-object v10, v10, v13

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getManageAppSurveyBody()V

    .line 424
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getReportAppErrorSurveyBody()V

    .line 425
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getUseBatterySurveyBody()V

    .line 426
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getUseMemorySurveyBody()V

    goto/16 :goto_0

    .line 428
    .end local v6    # "queryHour":I
    :cond_2
    sget-object v8, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v9, "Survey upload service off"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 431
    :cond_3
    return-void
.end method

.method private generateLogData(ILjava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p1, "type"    # I
    .param p2, "queryBody"    # Ljava/lang/String;
    .param p3, "activityType"    # Ljava/lang/String;

    .prologue
    .line 278
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "Generate LogData"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const-wide/16 v12, 0x0

    .line 281
    .local v12, "timestampLong":J
    const-string v2, ""

    .line 282
    .local v2, "timestamp":Ljava/lang/String;
    const-string v3, ""

    .line 283
    .local v3, "svcCode":Ljava/lang/String;
    const-string v4, ""

    .line 284
    .local v4, "logType":Ljava/lang/String;
    const-string v5, ""

    .line 285
    .local v5, "activity":Ljava/lang/String;
    const-string v6, ""

    .line 286
    .local v6, "userId":Ljava/lang/String;
    const-string v7, ""

    .line 287
    .local v7, "appVersion":Ljava/lang/String;
    const-string v8, ""

    .line 289
    .local v8, "body":Ljava/lang/String;
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    .line 291
    .local v9, "currentTime":Ljava/util/Date;
    packed-switch p1, :pswitch_data_0

    .line 317
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "Log DB type is invaild"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :goto_0
    return-void

    .line 293
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/providers/context/server/LogGenerator;->getHeaderUploadTime()Ljava/lang/String;

    move-result-object v2

    .line 294
    const-string v3, "704"

    .line 295
    const-string v4, "BIZ"

    .line 296
    move-object/from16 v5, p3

    .line 297
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getSamsungAccountInfo()Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->getUserId()Ljava/lang/String;

    move-result-object v6

    .line 298
    const-string v7, "99.01.00"

    .line 299
    move-object/from16 v8, p2

    .line 301
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->ALWAYS_LOG_DB_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->insertAlwaysDB(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :pswitch_1
    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    .line 306
    invoke-static {}, Lcom/samsung/android/providers/context/server/LogGenerator;->getHeaderUploadTime()Ljava/lang/String;

    move-result-object v2

    .line 307
    const-string v3, "701"

    .line 308
    const-string v4, "BIZ"

    .line 309
    move-object/from16 v5, p3

    .line 310
    const-string v7, "99.01.00"

    .line 311
    move-object/from16 v8, p2

    .line 313
    sget-object v11, Lcom/samsung/android/providers/context/server/LogGenerator;->SURVEY_LOG_DB_URI:Landroid/net/Uri;

    move-object/from16 v10, p0

    move-object v14, v2

    move-object v15, v3

    move-object/from16 v16, v4

    move-object/from16 v17, v5

    move-object/from16 v18, v7

    move-object/from16 v19, v8

    invoke-direct/range {v10 .. v19}, Lcom/samsung/android/providers/context/server/LogGenerator;->insertSurveyDB(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getAppUsageBody()V
    .locals 17

    .prologue
    .line 1196
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v2, "getAppUsageBody() : 010"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1198
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "timestamp_utc"

    aput-object v2, v4, v1

    const/4 v1, 0x1

    const-string v2, "app_id"

    aput-object v2, v4, v1

    .line 1201
    .local v4, "proj":[Ljava/lang/String;
    const-string v8, ""

    .line 1202
    .local v8, "app_id":Ljava/lang/String;
    const-string v16, ""

    .line 1204
    .local v16, "useCount":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1205
    .local v11, "cursor":Landroid/database/Cursor;
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_APP_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "timestamp_utc > ? and timestamp_utc < ?"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1208
    invoke-static {v11}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1248
    :cond_0
    return-void

    .line 1213
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1214
    const-string v1, "app_id"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1215
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    invoke-virtual {v1, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 1216
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    invoke-virtual {v1, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v10, v1, 0x1

    .line 1217
    .local v10, "cnt":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1224
    .end local v10    # "cnt":I
    :catch_0
    move-exception v12

    .line 1225
    .local v12, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1226
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while using the ContextProvider : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1228
    .end local v12    # "e":Ljava/lang/NullPointerException;
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1230
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v13

    .line 1231
    .local v13, "generatingLog":I
    const-string v15, ""

    .line 1232
    .local v15, "queryBody":Ljava/lang/String;
    const-string v9, ""

    .line 1234
    .local v9, "body":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 1236
    .local v14, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1237
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "app_id":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 1238
    .restart local v8    # "app_id":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    invoke-virtual {v1, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 1239
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pkg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u00b6"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "total"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1240
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u00b6"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u00b6"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u00b6"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1243
    if-eqz v13, :cond_2

    .line 1244
    const/4 v1, 0x0

    const-string v2, "010"

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v9, v2}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    .line 1245
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_2

    .line 1219
    .end local v9    # "body":Ljava/lang/String;
    .end local v13    # "generatingLog":I
    .end local v14    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v15    # "queryBody":Ljava/lang/String;
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1222
    :cond_4
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Hash Map"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mAppUsageMap:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private getBodyUploadTime(Ljava/util/GregorianCalendar;)V
    .locals 6
    .param p1, "queryCal"    # Ljava/util/GregorianCalendar;

    .prologue
    .line 239
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd\'T\'HH:00:00Z"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 240
    .local v2, "bodyTimeFormat":Ljava/text/SimpleDateFormat;
    const-wide/16 v0, -0x1

    .line 241
    .local v0, "bodyTime":J
    if-nez p1, :cond_0

    .line 242
    const-string v3, "date="

    iput-object v3, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    .line 243
    sget-object v3, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v4, "Query calendar is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :goto_0
    return-void

    .line 246
    :cond_0
    invoke-virtual {p1}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    .line 247
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "date="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    .line 248
    sget-object v3, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Body Upload Time : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getCamcorderBody()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 988
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "getCamcorderBody() : 005"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "timestamp_utc"

    aput-object v0, v3, v11

    .line 994
    .local v3, "proj":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 995
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->RECORD_VIDEO_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "timestamp_utc > ? and timestamp_utc < ?"

    iget-object v5, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 998
    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1010
    :goto_0
    return-void

    .line 1002
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 1003
    .local v10, "useCount":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1004
    .local v9, "queryBody":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1007
    .local v7, "body":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1009
    const-string v0, "005"

    invoke-direct {p0, v11, v7, v0}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getCameraBody()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 963
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "getCameraBody() : 004"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "timestamp_utc"

    aput-object v0, v3, v11

    .line 969
    .local v3, "proj":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 970
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAKE_PHOTO_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "timestamp_utc > ? and timestamp_utc < ?"

    iget-object v5, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 973
    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 985
    :goto_0
    return-void

    .line 977
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 978
    .local v10, "useCount":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 979
    .local v9, "queryBody":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 982
    .local v7, "body":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 984
    const-string v0, "004"

    invoke-direct {p0, v11, v7, v0}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getHeaderUploadTime()Ljava/lang/String;
    .locals 4

    .prologue
    .line 233
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 234
    .local v0, "currentTime":Ljava/util/Date;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "dd/LLL/yyyy:HH:mm:ss"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 235
    .local v1, "headerTimeFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getManageAppSurveyBody()V
    .locals 15

    .prologue
    .line 732
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "Get manage app survey log / Activity code : 003"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    const-string v10, ""

    .line 735
    .local v10, "installed_app_id":Ljava/lang/String;
    const-string v8, ""

    .line 736
    .local v8, "date":Ljava/lang/String;
    const/4 v14, -0x1

    .line 737
    .local v14, "type":I
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "timestamp_utc"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "installed_app_id"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "type"

    aput-object v1, v2, v0

    .line 741
    .local v2, "proj":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 742
    .local v7, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->MANAGE_APP_SURVEY_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "timestamp_utc > ? and timestamp_utc < ?"

    iget-object v4, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 743
    invoke-static {v7}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 775
    :goto_0
    return-void

    .line 747
    :cond_0
    new-instance v12, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v12, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 748
    .local v12, "mDBFormat":Ljava/text/SimpleDateFormat;
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v11, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 750
    .local v11, "mBodyTimeFormat":Ljava/text/SimpleDateFormat;
    :cond_1
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 753
    :try_start_0
    const-string v0, "timestamp_utc"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget v3, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    int-to-long v4, v3

    add-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 760
    :goto_2
    const-string v0, "installed_app_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 761
    const-string v0, ""

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 765
    const-string v0, "type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 767
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "app_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 769
    .local v13, "queryBody":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "model"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "fw_ver"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->fwVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 772
    .local v6, "body":Ljava/lang/String;
    const/4 v0, 0x1

    const-string v1, "003"

    invoke-direct {p0, v0, v6, v1}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 755
    .end local v6    # "body":Ljava/lang/String;
    .end local v13    # "queryBody":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 756
    .local v9, "e":Ljava/text/ParseException;
    invoke-virtual {v9}, Ljava/text/ParseException;->printStackTrace()V

    .line 757
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while using the ContextProvider : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 774
    .end local v9    # "e":Ljava/text/ParseException;
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private getMusicBody()V
    .locals 24

    .prologue
    .line 1092
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "getMusicBody() : 007"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1094
    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "timestamp_utc"

    aput-object v3, v5, v2

    const/4 v2, 0x1

    const-string v3, "genre"

    aput-object v3, v5, v2

    .line 1097
    .local v5, "proj":[Ljava/lang/String;
    const-string v14, ""

    .line 1098
    .local v14, "genre":Ljava/lang/String;
    const-string v20, ""

    .line 1100
    .local v20, "useCount":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1101
    .local v11, "cursor":Landroid/database/Cursor;
    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_PLAY_MUSIC:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v4, Lcom/samsung/android/providers/context/server/LogGenerator;->PLAY_MUSIC_CONTENT_URI:Landroid/net/Uri;

    const-string v6, "timestamp_utc > ? and timestamp_utc < ?"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1104
    invoke-static {v11}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1168
    :cond_0
    return-void

    .line 1108
    :cond_1
    const/16 v16, 0x0

    .line 1111
    .local v16, "genreArryLength":I
    :cond_2
    :goto_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1112
    const-string v2, "genre"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1113
    if-nez v14, :cond_3

    .line 1114
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "Genre is null."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    const-string v14, ""

    .line 1118
    :cond_3
    const/16 v2, 0x3b

    invoke-virtual {v14, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-le v2, v3, :cond_6

    .line 1119
    const-string v2, ";"

    invoke-virtual {v14, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 1120
    .local v15, "genreArr":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v16, v0

    .line 1121
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v18, v0

    .line 1123
    .local v18, "priority":F
    :goto_1
    if-lez v16, :cond_2

    .line 1124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    add-int/lit8 v3, v16, -0x1

    aget-object v3, v15, v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 1125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    add-int/lit8 v3, v16, -0x1

    aget-object v3, v15, v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v6, v6, v22

    add-double/2addr v2, v6

    double-to-float v10, v2

    .line 1126
    .local v10, "cnt":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    add-int/lit8 v3, v16, -0x1

    aget-object v3, v15, v3

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130
    .end local v10    # "cnt":F
    :goto_2
    add-int/lit8 v16, v16, -0x1

    goto :goto_1

    .line 1128
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    add-int/lit8 v3, v16, -0x1

    aget-object v3, v15, v3

    const/high16 v4, 0x3f800000    # 1.0f

    div-float v4, v4, v18

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1143
    .end local v15    # "genreArr":[Ljava/lang/String;
    .end local v18    # "priority":F
    :catch_0
    move-exception v12

    .line 1144
    .local v12, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1145
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while using the ContextProvider : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1147
    .end local v12    # "e":Ljava/lang/NullPointerException;
    :goto_3
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v13

    .line 1150
    .local v13, "generatingLog":I
    const-string v19, ""

    .line 1151
    .local v19, "queryBody":Ljava/lang/String;
    const-string v9, ""

    .line 1153
    .local v9, "body":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 1155
    .local v17, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_5
    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1156
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "genre":Ljava/lang/String;
    check-cast v14, Ljava/lang/String;

    .line 1157
    .restart local v14    # "genre":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v20

    .line 1158
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "genre = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", useCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "genre="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "total"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1160
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1163
    if-lez v13, :cond_5

    .line 1164
    const/4 v2, 0x0

    const-string v3, "007"

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9, v3}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    .line 1165
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_4

    .line 1133
    .end local v9    # "body":Ljava/lang/String;
    .end local v13    # "generatingLog":I
    .end local v17    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v19    # "queryBody":Ljava/lang/String;
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2, v14}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    .line 1134
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    add-float v10, v2, v3

    .line 1135
    .restart local v10    # "cnt":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v14, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1137
    .end local v10    # "cnt":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v14, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1141
    :cond_8
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Hash Map"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mMusicGenreMap:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method

.method private getQueryBoundary(II)[Ljava/lang/String;
    .locals 12
    .param p1, "type"    # I
    .param p2, "hour"    # I

    .prologue
    .line 491
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 492
    .local v4, "queryBoundaryFormat":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 494
    .local v0, "boundaryCal":Ljava/util/GregorianCalendar;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v2

    .line 497
    .local v2, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v7, "SentDate"

    const-wide/16 v8, 0x0

    invoke-virtual {v2, v7, v8, v9}, Lcom/samsung/android/providers/context/ContextPreference;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 498
    const/4 v5, 0x0

    .line 499
    .local v5, "startQueryTime":Ljava/lang/String;
    const/4 v1, 0x0

    .line 500
    .local v1, "endQueryTime":Ljava/lang/String;
    const/16 v7, 0xf

    invoke-virtual {v0, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    .line 503
    iget v7, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    const v8, 0x36ee80

    div-int/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffset:I

    .line 505
    packed-switch p1, :pswitch_data_0

    .line 553
    sget-object v7, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v8, "Can\'t find query boundary"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    :goto_0
    const/4 v7, 0x2

    new-array v3, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v5, v3, v7

    const/4 v7, 0x1

    aput-object v1, v3, v7

    .line 560
    .local v3, "queryBoundary":[Ljava/lang/String;
    return-object v3

    .line 507
    .end local v3    # "queryBoundary":[Ljava/lang/String;
    :pswitch_0
    const/16 v7, 0xb

    iget v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffset:I

    neg-int v8, v8

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 509
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 511
    const/16 v7, 0xb

    const/16 v8, 0x17

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 512
    const/16 v7, 0xc

    const/16 v8, 0x3b

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 513
    const/16 v7, 0xd

    const/16 v8, 0x3b

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 514
    const/16 v7, 0xe

    const/16 v8, 0x3e7

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->set(II)V

    .line 515
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 517
    sget-object v7, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Query time(UTC) : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ~ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 521
    :pswitch_1
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 522
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 524
    .local v6, "startTime":Ljava/lang/String;
    const/16 v7, 0xb

    const/16 v8, 0x17

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 525
    const/16 v7, 0xc

    const/16 v8, 0x3b

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 526
    const/16 v7, 0xd

    const/16 v8, 0x3b

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 527
    const/16 v7, 0xe

    const/16 v8, 0x3e7

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->set(II)V

    .line 528
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 530
    sget-object v7, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Query time(Local_Shealth) : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") ~ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 534
    .end local v6    # "startTime":Ljava/lang/String;
    :pswitch_2
    const/16 v7, 0xb

    iget v8, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffset:I

    neg-int v8, v8

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 535
    const/16 v7, 0xb

    invoke-virtual {v0, v7, p2}, Ljava/util/GregorianCalendar;->add(II)V

    .line 539
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 542
    const/16 v7, 0xb

    const/4 v8, 0x3

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 543
    const/16 v7, 0xe

    const/4 v8, -0x1

    invoke-virtual {v0, v7, v8}, Ljava/util/GregorianCalendar;->add(II)V

    .line 545
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 548
    sget-object v7, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Query time(UTC_Survey_App) : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ~ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 505
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getReportAppErrorSurveyBody()V
    .locals 15

    .prologue
    .line 778
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "Get report app error survey log / Activity code : 004"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    const-string v6, ""

    .line 781
    .local v6, "app_id":Ljava/lang/String;
    const-string v9, ""

    .line 782
    .local v9, "date":Ljava/lang/String;
    const/4 v14, -0x1

    .line 783
    .local v14, "type":I
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "timestamp_utc"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "app_id"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "type"

    aput-object v1, v2, v0

    .line 787
    .local v2, "proj":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 788
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->REPORT_APP_ERROR_SURVEY_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "timestamp_utc > ? and timestamp_utc < ?"

    iget-object v4, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 789
    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 820
    :goto_0
    return-void

    .line 793
    :cond_0
    new-instance v12, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v12, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 794
    .local v12, "mDBFormat":Ljava/text/SimpleDateFormat;
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v11, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 796
    .local v11, "mBodyTimeFormat":Ljava/text/SimpleDateFormat;
    :cond_1
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 799
    :try_start_0
    const-string v0, "timestamp_utc"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget v3, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    int-to-long v4, v3

    add-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 805
    :goto_2
    const-string v0, "app_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 806
    const-string v0, ""

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 810
    const-string v0, "type"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 812
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "app_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 814
    .local v13, "queryBody":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "model"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "fw_ver"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->fwVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 817
    .local v7, "body":Ljava/lang/String;
    const/4 v0, 0x1

    const-string v1, "004"

    invoke-direct {p0, v0, v7, v1}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 801
    .end local v7    # "body":Ljava/lang/String;
    .end local v13    # "queryBody":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 802
    .local v10, "e":Ljava/text/ParseException;
    invoke-virtual {v10}, Ljava/text/ParseException;->printStackTrace()V

    .line 803
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while using the ContextProvider : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 819
    .end local v10    # "e":Ljava/text/ParseException;
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private getRoamingBody()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1251
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "getRoamingBody() : 011"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "timestamp_utc"

    aput-object v0, v3, v2

    const/4 v0, 0x1

    const-string v1, "mcc"

    aput-object v1, v3, v0

    .line 1256
    .local v3, "proj":[Ljava/lang/String;
    const-string v10, "0"

    .line 1258
    .local v10, "flag":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1259
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_TRACK_ROAMING:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TRACK_ROAMING_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "timestamp_utc > ? and timestamp_utc < ?"

    iget-object v5, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1262
    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1290
    :goto_0
    return-void

    .line 1267
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1268
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mcc:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1269
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mcc:Ljava/lang/String;

    const-string v1, "mcc"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1270
    const-string v10, "0"

    .line 1279
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1280
    .local v11, "queryBody":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1283
    .local v7, "body":Ljava/lang/String;
    const/4 v0, 0x0

    const-string v1, "011"

    invoke-direct {p0, v0, v7, v1}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1285
    .end local v7    # "body":Ljava/lang/String;
    .end local v11    # "queryBody":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 1286
    .local v9, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1287
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error while using the ContextProvider : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1289
    .end local v9    # "e":Ljava/lang/NullPointerException;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1272
    :cond_2
    :try_start_1
    const-string v10, "1"

    goto :goto_2

    .line 1276
    :cond_3
    const-string v10, ""
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private getSimInfo()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x3

    .line 252
    const/4 v0, 0x0

    .line 253
    .local v0, "simOperator":Ljava/lang/String;
    const/4 v1, -0x1

    .line 255
    .local v1, "simState":I
    iget-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    if-eqz v2, :cond_0

    .line 256
    iget-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 257
    iget-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    .line 260
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v4, :cond_1

    if-eq v1, v4, :cond_2

    .line 261
    :cond_1
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "Sim operator is invalid"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const-string v2, "mcc="

    iput-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    .line 263
    const-string v2, "mnc="

    iput-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    .line 271
    :goto_0
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MCC : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mcc:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", MNC : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mnc:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Upload MCC : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Upload MNC : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    return-void

    .line 265
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mcc:Ljava/lang/String;

    .line 266
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mnc:Ljava/lang/String;

    .line 267
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mcc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mcc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    .line 268
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mnc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mnc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private getTargetPackageId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1347
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "Get Target Package Id"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_ALWAYS_SERVICE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-static {v2}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->getLastItem(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Landroid/os/Bundle;

    move-result-object v0

    .line 1350
    .local v0, "data":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 1351
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "bundle is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1352
    const-string v1, ""

    .line 1356
    :goto_0
    return-object v1

    .line 1355
    :cond_0
    const-string v2, "APPID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1356
    .local v1, "targetPackageId":Ljava/lang/String;
    goto :goto_0
.end method

.method private getUseAppFeatureSurveyBody()V
    .locals 24

    .prologue
    .line 638
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "Get use app feature survey log / Activity code : 002"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v12, v2, v3

    .line 641
    .local v12, "date_from":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v13, v2, v3

    .line 642
    .local v13, "date_to":Ljava/lang/String;
    const-string v8, ""

    .line 643
    .local v8, "app_id":Ljava/lang/String;
    const-string v15, ""

    .line 644
    .local v15, "feature":Ljava/lang/String;
    const-string v16, ""

    .line 645
    .local v16, "featureId":Ljava/lang/String;
    const-string v23, ""

    .line 646
    .local v23, "useCount":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "app_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "feature"

    aput-object v3, v4, v2

    .line 650
    .local v4, "proj":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 651
    .local v11, "cursor":Landroid/database/Cursor;
    sget-object v3, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_APP_FEATURE_SURVEY_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "timestamp_utc > ? and timestamp_utc < ?"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 652
    invoke-static {v11}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 729
    :cond_0
    return-void

    .line 656
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    if-eqz v2, :cond_0

    .line 660
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 662
    new-instance v20, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 663
    .local v20, "mDBFormat":Ljava/text/SimpleDateFormat;
    new-instance v19, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 666
    .local v19, "mBodyTimeFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    int-to-long v6, v5

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 667
    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    int-to-long v6, v5

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 673
    :cond_2
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 675
    const-string v2, "feature"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 676
    const-string v2, ""

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 679
    const-string v2, "app_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 680
    const-string v2, ""

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 683
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 684
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 685
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v10, v2, 0x1

    .line 686
    .local v10, "cnt":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 668
    .end local v10    # "cnt":I
    :catch_0
    move-exception v14

    .line 669
    .local v14, "e":Ljava/text/ParseException;
    invoke-virtual {v14}, Ljava/text/ParseException;->printStackTrace()V

    .line 670
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while using the ContextProvider : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 688
    .end local v14    # "e":Ljava/text/ParseException;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 691
    :cond_4
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Hash Map"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 695
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v17

    .line 696
    .local v17, "generatingLog":I
    const-string v21, ""

    .line 697
    .local v21, "queryBody":Ljava/lang/String;
    const-string v9, ""

    .line 699
    .local v9, "body":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .line 703
    .local v18, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_5
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 704
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "featureId":Ljava/lang/String;
    check-cast v16, Ljava/lang/String;

    .line 707
    .restart local v16    # "featureId":Ljava/lang/String;
    :try_start_1
    new-instance v22, Ljava/util/StringTokenizer;

    const-string v2, "\u00b6"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    .local v22, "stok":Ljava/util/StringTokenizer;
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v15

    .line 709
    invoke-virtual/range {v22 .. v22}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 716
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    .line 717
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "app_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "feature"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "count"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 719
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "model"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/providers/context/server/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "fw_ver"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->fwVer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "date_from"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "date_to"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 724
    if-eqz v17, :cond_5

    .line 725
    const/4 v2, 0x1

    const-string v3, "002"

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9, v3}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    .line 726
    add-int/lit8 v17, v17, -0x1

    goto/16 :goto_1

    .line 710
    .end local v22    # "stok":Ljava/util/StringTokenizer;
    :catch_1
    move-exception v14

    .line 711
    .local v14, "e":Ljava/util/NoSuchElementException;
    invoke-virtual {v14}, Ljava/util/NoSuchElementException;->printStackTrace()V

    .line 712
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while using the ContextProvider : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private getUseAppSurveyBody()V
    .locals 21

    .prologue
    .line 566
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "Get use app survey log / Activity code : 001"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v12, v2, v3

    .line 569
    .local v12, "date_from":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v13, v2, v3

    .line 570
    .local v13, "date_to":Ljava/lang/String;
    const-string v8, ""

    .line 571
    .local v8, "app_id":Ljava/lang/String;
    const-string v20, ""

    .line 572
    .local v20, "useCount":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "app_id"

    aput-object v3, v4, v2

    .line 576
    .local v4, "proj":[Ljava/lang/String;
    new-instance v18, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 577
    .local v18, "mDBFormat":Ljava/text/SimpleDateFormat;
    new-instance v17, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 579
    .local v17, "mBodyTimeFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    int-to-long v6, v5

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 580
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->zoneOffsetMil:I

    int-to-long v6, v5

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 586
    :goto_0
    const/4 v11, 0x0

    .line 587
    .local v11, "cursor":Landroid/database/Cursor;
    sget-object v3, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_APP_SURVEY_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "timestamp_utc > ? and timestamp_utc < ?"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 588
    invoke-static {v11}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 635
    :cond_0
    return-void

    .line 581
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v14

    .line 582
    .local v14, "e":Ljava/text/ParseException;
    invoke-virtual {v14}, Ljava/text/ParseException;->printStackTrace()V

    .line 583
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while using the ContextProvider : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 592
    .end local v14    # "e":Ljava/text/ParseException;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    if-eqz v2, :cond_0

    .line 596
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 598
    :cond_2
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 599
    const-string v2, "app_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 600
    const-string v2, ""

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 604
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 605
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v10, v2, 0x1

    .line 606
    .local v10, "cnt":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 608
    .end local v10    # "cnt":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 611
    :cond_4
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Hash Map"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 615
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v15

    .line 616
    .local v15, "generatingLog":I
    const-string v19, ""

    .line 617
    .local v19, "queryBody":Ljava/lang/String;
    const-string v9, ""

    .line 619
    .local v9, "body":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 621
    .local v16, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_5
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 622
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "app_id":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 623
    .restart local v8    # "app_id":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mSurveyMap:Ljava/util/HashMap;

    invoke-virtual {v2, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 624
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "app_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "count"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 625
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "model"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/providers/context/server/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "fw_ver"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->fwVer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "date_from"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "date_to"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 630
    if-eqz v15, :cond_5

    .line 631
    const/4 v2, 0x1

    const-string v3, "001"

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9, v3}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    .line 632
    add-int/lit8 v15, v15, -0x1

    goto/16 :goto_2
.end method

.method private getUseBatterySurveyBody()V
    .locals 13

    .prologue
    const/4 v3, 0x1

    .line 823
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "Get use battery survey log / Activity code : 005"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    const-string v6, ""

    .line 826
    .local v6, "app_id":Ljava/lang/String;
    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    .line 828
    .local v10, "power":D
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "app_id"

    aput-object v1, v2, v0

    const-string v0, "power"

    aput-object v0, v2, v3

    .line 832
    .local v2, "proj":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 833
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_BATTERY_SURVEY_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "timestamp_utc > ? and timestamp_utc < ?"

    iget-object v4, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 834
    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 860
    :goto_0
    return-void

    .line 839
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 841
    const-string v0, "app_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 842
    const-string v0, ""

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 846
    const-string v0, "power"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v10

    .line 848
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "app_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 849
    .local v12, "queryBody":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "model"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "fw_ver"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->fwVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 853
    .local v7, "body":Ljava/lang/String;
    const/4 v0, 0x1

    const-string v1, "005"

    invoke-direct {p0, v0, v7, v1}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 855
    .end local v7    # "body":Ljava/lang/String;
    .end local v12    # "queryBody":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 856
    .local v9, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 857
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while using the ContextProvider : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    .end local v9    # "e":Ljava/lang/NullPointerException;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private getUseMemorySurveyBody()V
    .locals 12

    .prologue
    const/4 v3, 0x1

    .line 863
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "Get use memory survey log / Activity code : 006"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    const-string v6, ""

    .line 866
    .local v6, "app_id":Ljava/lang/String;
    const/4 v10, -0x1

    .line 868
    .local v10, "memory":I
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "app_id"

    aput-object v1, v2, v0

    const-string v0, "memory"

    aput-object v0, v2, v3

    .line 872
    .local v2, "proj":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 873
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_MEMORY_SURVEY_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "timestamp_utc > ? and timestamp_utc < ?"

    iget-object v4, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->surveyQueryBoundary:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 874
    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 899
    :goto_0
    return-void

    .line 879
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 880
    const-string v0, "app_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 881
    const-string v0, ""

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 885
    const-string v0, "memory"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 887
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "app_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "memory"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 888
    .local v11, "queryBody":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "model"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "fw_ver"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->fwVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 892
    .local v7, "body":Ljava/lang/String;
    const/4 v0, 0x1

    const-string v1, "006"

    invoke-direct {p0, v0, v7, v1}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 894
    .end local v7    # "body":Ljava/lang/String;
    .end local v11    # "queryBody":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 895
    .local v9, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 896
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while using the ContextProvider : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    .end local v9    # "e":Ljava/lang/NullPointerException;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private getVideoBody()V
    .locals 24

    .prologue
    .line 1013
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "getVideoBody() : 006"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "timestamp_utc"

    aput-object v3, v5, v2

    const/4 v2, 0x1

    const-string v3, "genre"

    aput-object v3, v5, v2

    .line 1018
    .local v5, "proj":[Ljava/lang/String;
    const-string v14, ""

    .line 1019
    .local v14, "genre":Ljava/lang/String;
    const-string v20, ""

    .line 1021
    .local v20, "useCount":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1022
    .local v11, "cursor":Landroid/database/Cursor;
    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_PLAY_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v4, Lcom/samsung/android/providers/context/server/LogGenerator;->PLAY_VIDEO_CONTENT_URI:Landroid/net/Uri;

    const-string v6, "timestamp_utc > ? and timestamp_utc < ?"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1025
    invoke-static {v11}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1089
    :cond_0
    return-void

    .line 1029
    :cond_1
    const/16 v16, 0x0

    .line 1032
    .local v16, "genreArryLength":I
    :cond_2
    :goto_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1033
    const-string v2, "genre"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1034
    if-nez v14, :cond_3

    .line 1035
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "Genre is null."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    const-string v14, ""

    .line 1039
    :cond_3
    const/16 v2, 0x3b

    invoke-virtual {v14, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-le v2, v3, :cond_6

    .line 1040
    const-string v2, ";"

    invoke-virtual {v14, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 1041
    .local v15, "genreArr":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v16, v0

    .line 1042
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v18, v0

    .line 1044
    .local v18, "priority":F
    :goto_1
    if-lez v16, :cond_2

    .line 1045
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    add-int/lit8 v3, v16, -0x1

    aget-object v3, v15, v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 1046
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    add-int/lit8 v3, v16, -0x1

    aget-object v3, v15, v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v6, v6, v22

    add-double/2addr v2, v6

    double-to-float v10, v2

    .line 1047
    .local v10, "cnt":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    add-int/lit8 v3, v16, -0x1

    aget-object v3, v15, v3

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1051
    .end local v10    # "cnt":F
    :goto_2
    add-int/lit8 v16, v16, -0x1

    goto :goto_1

    .line 1049
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    add-int/lit8 v3, v16, -0x1

    aget-object v3, v15, v3

    const/high16 v4, 0x3f800000    # 1.0f

    div-float v4, v4, v18

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1064
    .end local v15    # "genreArr":[Ljava/lang/String;
    .end local v18    # "priority":F
    :catch_0
    move-exception v12

    .line 1065
    .local v12, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1066
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while using the ContextProvider : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1068
    .end local v12    # "e":Ljava/lang/NullPointerException;
    :goto_3
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1070
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v13

    .line 1071
    .local v13, "generatingLog":I
    const-string v19, ""

    .line 1072
    .local v19, "queryBody":Ljava/lang/String;
    const-string v9, ""

    .line 1074
    .local v9, "body":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 1076
    .local v17, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_5
    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1077
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "genre":Ljava/lang/String;
    check-cast v14, Ljava/lang/String;

    .line 1078
    .restart local v14    # "genre":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v20

    .line 1079
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "genre = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", useCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1080
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "genre="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "total"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1081
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b6"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1084
    if-lez v13, :cond_5

    .line 1085
    const/4 v2, 0x0

    const-string v3, "006"

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9, v3}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    .line 1086
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_4

    .line 1054
    .end local v9    # "body":Ljava/lang/String;
    .end local v13    # "generatingLog":I
    .end local v17    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v19    # "queryBody":Ljava/lang/String;
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2, v14}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    .line 1055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    invoke-virtual {v2, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    add-float v10, v2, v3

    .line 1056
    .restart local v10    # "cnt":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v14, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1058
    .end local v10    # "cnt":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v14, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1062
    :cond_8
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Hash Map"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/providers/context/server/LogGenerator;->mVideoGenreMap:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method

.method private getWifiBody()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1171
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "getWifiBody() : 009"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1173
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "timestamp_utc"

    aput-object v0, v3, v11

    .line 1177
    .local v3, "proj":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 1178
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_WIFI:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->USE_WIFI_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "timestamp_utc > ? and timestamp_utc < ?"

    iget-object v5, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->normalQueryBoundary:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/providers/context/server/LogGenerator;->queryLog(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1181
    invoke-static {v8}, Lcom/samsung/android/providers/context/server/LogGenerator;->checkCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1193
    :goto_0
    return-void

    .line 1185
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 1186
    .local v10, "useCount":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1187
    .local v9, "queryBody":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->uploadMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->bodyUploadTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00b6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1190
    .local v7, "body":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1192
    const-string v0, "009"

    invoke-direct {p0, v11, v7, v0}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLogData(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertAlwaysDB(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "svcCode"    # Ljava/lang/String;
    .param p4, "logType"    # Ljava/lang/String;
    .param p5, "activity"    # Ljava/lang/String;
    .param p6, "userId"    # Ljava/lang/String;
    .param p7, "appVersion"    # Ljava/lang/String;
    .param p8, "body"    # Ljava/lang/String;

    .prologue
    .line 325
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Insert Uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 328
    .local v1, "row":Landroid/content/ContentValues;
    const-string v2, "timestamp"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const-string v2, "svc_code"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v2, "log_type"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v2, "activity"

    invoke-virtual {v1, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v2, "user_id"

    invoke-virtual {v1, v2, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v2, "app_version"

    invoke-virtual {v1, v2, p7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const-string v2, "body"

    invoke-virtual {v1, v2, p8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Insert log to DB : [ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    iget-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->cr:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 337
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "ServerUploader insertion operation is performed."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    .end local v1    # "row":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "Error while using the ContextProvider ServerUploader"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertSurveyDB(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "timstampLong"    # J
    .param p4, "timestamp"    # Ljava/lang/String;
    .param p5, "svcCode"    # Ljava/lang/String;
    .param p6, "logType"    # Ljava/lang/String;
    .param p7, "activity"    # Ljava/lang/String;
    .param p8, "appVersion"    # Ljava/lang/String;
    .param p9, "body"    # Ljava/lang/String;

    .prologue
    .line 345
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Insert Uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 348
    .local v1, "row":Landroid/content/ContentValues;
    const-string v2, "timestamp_long"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 349
    const-string v2, "timestamp"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string v2, "svc_code"

    invoke-virtual {v1, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v2, "log_type"

    invoke-virtual {v1, v2, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v2, "activity"

    invoke-virtual {v1, v2, p7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const-string v2, "app_version"

    invoke-virtual {v1, v2, p8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v2, "body"

    invoke-virtual {v1, v2, p9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Insert log to DB : [ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget-object v2, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->cr:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 357
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "ServerUploader insertion operation is performed."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    .end local v1    # "row":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 358
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v3, "Error while using the ContextProvider ServerUploader"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private queryLog(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "proj"    # [Ljava/lang/String;
    .param p3, "queryCondition"    # Ljava/lang/String;
    .param p4, "queryBoundary"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1307
    const/4 v6, 0x0

    .line 1310
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->cr:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    :goto_0
    move-object v0, v6

    .line 1319
    :goto_1
    return-object v0

    .line 1311
    :catch_0
    move-exception v7

    .line 1312
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1313
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error while using query in the ContextProvider : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1314
    .end local v7    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v7

    .line 1315
    .local v7, "e":Ljava/lang/SecurityException;
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission denied : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1316
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private queryLog(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "privacyItemType"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "proj"    # [Ljava/lang/String;
    .param p4, "queryCondition"    # Ljava/lang/String;
    .param p5, "queryBoundary"    # [Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1325
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->getTargetPackageId()Ljava/lang/String;

    move-result-object v6

    .line 1327
    .local v6, "targetPackageId":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 1328
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "targetPackageId is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1329
    const/4 v7, 0x0

    .line 1343
    :goto_0
    return-object v7

    .line 1332
    :cond_0
    const/4 v7, 0x0

    .local v7, "cursor":Landroid/database/Cursor;
    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 1335
    :try_start_0
    invoke-static/range {v0 .. v6}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->queryForInternal(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_0

    .line 1338
    :catch_0
    move-exception v8

    .line 1339
    .local v8, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v8}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1340
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error while using query in the ContextProvider : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static setNextDay(Ljava/util/Calendar;)V
    .locals 4
    .param p0, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 484
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 485
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 486
    .local v0, "simpleFormat":Ljava/text/SimpleDateFormat;
    sget-object v1, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set next day : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    return-void
.end method

.method private static writeSentDate(Ljava/util/Calendar;)V
    .locals 6
    .param p0, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 477
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v0

    .line 478
    .local v0, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v2, "SentDate"

    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    .line 479
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 480
    .local v1, "simpleFormat":Ljava/text/SimpleDateFormat;
    sget-object v2, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Write sent date : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    return-void
.end method


# virtual methods
.method public getAlwaysUpload()Z
    .locals 1

    .prologue
    .line 1384
    sget-boolean v0, Lcom/samsung/android/providers/context/server/LogGenerator;->isAlwaysUpload:Z

    return v0
.end method

.method public getSurveyUpload()Z
    .locals 1

    .prologue
    .line 1388
    sget-boolean v0, Lcom/samsung/android/providers/context/server/LogGenerator;->isSurveyUpload:Z

    return v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 224
    sget-object v0, Lcom/samsung/android/providers/context/server/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "run()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/LogGenerator;->generateLog()V

    .line 227
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/LogGenerator;->mResultMsgHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 228
    return-void
.end method
