.class Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;
.super Ljava/lang/Object;
.source "AppUsageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/AppUsageMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppUseInfo"
.end annotation


# instance fields
.field public final appId:Ljava/lang/String;

.field public final appSubId:Ljava/lang/String;

.field public final launcherType:I

.field public final startTime:Ljava/util/Date;


# direct methods
.method public constructor <init>(Landroid/content/ComponentName;I)V
    .locals 4
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "type"    # I

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->startTime:Ljava/util/Date;

    .line 301
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->appId:Ljava/lang/String;

    .line 302
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->appSubId:Ljava/lang/String;

    .line 303
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->startTime:Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Date;->setTime(J)V

    .line 304
    iput p2, p0, Lcom/samsung/android/providers/context/status/AppUsageMonitor$AppUseInfo;->launcherType:I

    .line 305
    return-void
.end method
