.class public Lcom/samsung/android/providers/context/log/UseAppLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "UseAppLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 35
    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "app_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "app_sub_id"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "compat_device_status"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "duration"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "launcher_type"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "start_time"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "stop_time"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "starttime"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "stoptime"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/UseAppLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "use_app"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/UseAppLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 47
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    const/16 v1, 0x1770

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    .line 48
    const/16 v1, 0x258

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    .line 50
    new-instance v1, Lcom/samsung/android/providers/context/log/UseAppLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/UseAppLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/UseAppLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 96
    sget-object v1, Lcom/samsung/android/providers/context/log/UseAppLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 97
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/samsung/android/providers/context/log/UseAppLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 130
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x1

    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "app_id":Ljava/lang/String;
    const/4 v2, -0x1

    .line 105
    .local v2, "launcher_type":I
    :try_start_0
    const-string v4, "app_id"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    const-string v4, "launcher_type"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 112
    :goto_0
    if-eqz v0, :cond_0

    const-string v4, ""

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 113
    :cond_0
    const-string v3, "ContextFramework"

    const-string v4, "UseApp : [Drop] - app_id is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v3, 0x0

    .line 121
    :cond_1
    :goto_1
    return v3

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "ex":Ljava/lang/NullPointerException;
    const-string v4, "ContextFramework"

    const-string v5, "UseApp : ValidBoundaryCheck - NullPointerException."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 117
    .end local v1    # "ex":Ljava/lang/NullPointerException;
    :cond_2
    if-eqz v2, :cond_1

    if-eq v2, v3, :cond_1

    .line 118
    const-string v4, "ContextFramework"

    const-string v5, "UseApp : launcher_type is invalid"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
