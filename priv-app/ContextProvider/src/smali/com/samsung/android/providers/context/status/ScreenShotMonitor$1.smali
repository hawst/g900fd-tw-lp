.class Lcom/samsung/android/providers/context/status/ScreenShotMonitor$1;
.super Landroid/os/FileObserver;
.source "ScreenShotMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/providers/context/status/ScreenShotMonitor;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/status/ScreenShotMonitor;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/status/ScreenShotMonitor;Ljava/lang/String;ILandroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # I

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor$1;->this$0:Lcom/samsung/android/providers/context/status/ScreenShotMonitor;

    iput-object p4, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2, p3}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 5
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 50
    and-int/lit16 v2, p1, 0x100

    if-eqz v2, :cond_0

    .line 51
    const-string v2, "ContextFramework"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ScreenShotMonitor: event occurred on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const-string v2, "Screenshot_"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 54
    const-string v2, "ContextFramework"

    const-string v3, "ScreenShotMonitor: Wrong file name format"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor$1;->this$0:Lcom/samsung/android/providers/context/status/ScreenShotMonitor;

    # getter for: Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->mWatchDir:Ljava/io/File;
    invoke-static {v3}, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->access$000(Lcom/samsung/android/providers/context/status/ScreenShotMonitor;)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "file":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor$1;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->logScreenShot(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    invoke-static {v2, v3}, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->access$100(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 61
    .local v0, "b":Z
    if-nez v0, :cond_0

    .line 62
    const-string v2, "ContextFramework"

    const-string v3, "ScreenShotMonitor: No proper content provider found"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
