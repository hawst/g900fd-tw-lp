.class public Lcom/samsung/android/providers/context/log/ExchangeEmailLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "ExchangeEmailLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 38
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "app_id"

    aput-object v2, v1, v4

    const-string v2, "date"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string v3, "datetime"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "email_address"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "cc_address"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "uri"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/ExchangeEmailLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 46
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "exchange_email"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_EMAIL:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/ExchangeEmailLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 50
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v5, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useBroadcast:Z

    .line 51
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    .line 52
    const/16 v1, 0x1f40

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    .line 53
    const/16 v1, 0x320

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    .line 55
    new-instance v1, Lcom/samsung/android/providers/context/log/ExchangeEmailLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/ExchangeEmailLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/ExchangeEmailLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 99
    sget-object v1, Lcom/samsung/android/providers/context/log/ExchangeEmailLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 100
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/samsung/android/providers/context/log/ExchangeEmailLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 164
    return-void
.end method


# virtual methods
.method protected Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 104
    const-string v3, "custom_id"

    .line 106
    .local v3, "deprecatedKey":Ljava/lang/String;
    const-string v4, "custom_id"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 108
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v4

    iget-boolean v4, v4, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v4, :cond_0

    .line 109
    const-string v4, "ContextFramework"

    const-string v5, "FILTERLOG : EXCHANGEEMAIL:: Have to remove = custom_id"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const-string v4, "ContextFramework"

    const-string v5, "FILTERLOG : EXCHANGEEMAIL:: patch to URI = custom_id"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    const-string v4, "uri"

    const-string v5, "custom_id"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_1
    const-string v4, "date"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 117
    const-string v4, "datetime"

    const-string v5, "date"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/providers/context/util/ContextLogger;->convertStringToTime(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 132
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v4

    return-object v4

    .line 118
    :cond_3
    const-string v4, "datetime"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 121
    const-string v4, "datetime"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 122
    .local v0, "dateTime":Ljava/lang/Long;
    if-eqz v0, :cond_4

    .line 123
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 128
    .local v1, "dateTimeDate":Ljava/util/Date;
    :goto_1
    new-instance v2, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 129
    .local v2, "dateTimeString":Ljava/lang/String;
    const-string v4, "date"

    invoke-virtual {p1, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 125
    .end local v1    # "dateTimeDate":Ljava/util/Date;
    .end local v2    # "dateTimeString":Ljava/lang/String;
    :cond_4
    new-instance v1, Ljava/util/Date;

    const-wide/16 v4, 0x0

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .restart local v1    # "dateTimeDate":Ljava/util/Date;
    goto :goto_1
.end method

.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x1

    .line 137
    const/4 v0, 0x0

    .line 138
    .local v0, "app_id":Ljava/lang/String;
    const/4 v2, 0x0

    .line 141
    .local v2, "type":I
    :try_start_0
    const-string v3, "app_id"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    const-string v3, "type"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 148
    :goto_0
    if-eqz v0, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 149
    :cond_0
    const-string v3, "ContextFramework"

    const-string v4, "ExchangeEmail : app_id is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_1
    if-eq v2, v5, :cond_2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    .line 152
    const-string v3, "ContextFramework"

    const-string v4, "ExchangeEmail : type is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_2
    return v5

    .line 143
    :catch_0
    move-exception v1

    .line 144
    .local v1, "ex":Ljava/lang/NullPointerException;
    const-string v3, "ContextFramework"

    const-string v4, "ExchangeEmail : ValidBoundaryCheck - NullPointerExcekption."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
