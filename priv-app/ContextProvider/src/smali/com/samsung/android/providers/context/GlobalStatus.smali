.class public Lcom/samsung/android/providers/context/GlobalStatus;
.super Ljava/lang/Object;
.source "GlobalStatus.java"


# static fields
.field private static sInstance:Lcom/samsung/android/providers/context/GlobalStatus;

.field private static final sIsEngeenerBinary:Z


# instance fields
.field public foreground:Z

.field public logVisible:Z

.field public timeZone:Ljava/lang/String;

.field public wifiOnly:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/providers/context/GlobalStatus;->sInstance:Lcom/samsung/android/providers/context/GlobalStatus;

    .line 61
    const-string v0, "eng"

    const-string v1, "ro.build.type"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/providers/context/GlobalStatus;->sIsEngeenerBinary:Z

    .line 62
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/GlobalStatus;->wifiOnly:Z

    .line 52
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    .line 54
    iput-boolean v0, p0, Lcom/samsung/android/providers/context/GlobalStatus;->foreground:Z

    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/GlobalStatus;->updateTimeZone()V

    .line 74
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->isEngineerBinary()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    .line 77
    :cond_0
    return-void
.end method

.method public static createLogTimeFormat()Ljava/text/DateFormat;
    .locals 3

    .prologue
    .line 93
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 94
    .local v0, "dateFormat":Ljava/text/DateFormat;
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 96
    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/providers/context/GlobalStatus;
    .locals 2

    .prologue
    .line 65
    const-class v1, Lcom/samsung/android/providers/context/GlobalStatus;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/GlobalStatus;->sInstance:Lcom/samsung/android/providers/context/GlobalStatus;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/samsung/android/providers/context/GlobalStatus;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/GlobalStatus;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/GlobalStatus;->sInstance:Lcom/samsung/android/providers/context/GlobalStatus;

    .line 68
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/GlobalStatus;->sInstance:Lcom/samsung/android/providers/context/GlobalStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isEngineerBinary()Z
    .locals 1

    .prologue
    .line 100
    sget-boolean v0, Lcom/samsung/android/providers/context/GlobalStatus;->sIsEngeenerBinary:Z

    return v0
.end method

.method public static isValidDuration(J)Z
    .locals 2
    .param p0, "dur"    # J

    .prologue
    .line 89
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x9a7ec800L

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public updateTimeZone()V
    .locals 6

    .prologue
    .line 80
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 81
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 82
    .local v1, "currentLocalTime":Ljava/util/Date;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "Z"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 83
    .local v2, "date":Ljava/text/DateFormat;
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/providers/context/GlobalStatus;->timeZone:Ljava/lang/String;

    .line 85
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GlobalStatus: updated time zone is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/providers/context/GlobalStatus;->timeZone:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void
.end method
