.class public Lcom/samsung/android/providers/context/privacy/PermissionManagerService;
.super Landroid/app/Service;
.source "PermissionManagerService.java"


# instance fields
.field private mPrivacyManagerStub:Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 57
    const-string v0, "ContextFramework.PermissionManagerService"

    const-string v1, "Privacy Manager Service binding is completed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PermissionManagerService;->mPrivacyManagerStub:Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 37
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/privacy/PermissionManagerService;->mPrivacyManagerStub:Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;

    .line 39
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_0

    .line 40
    const-string v0, "ContextFramework.PermissionManagerService"

    const-string v1, "onCreate() is finished"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 64
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 65
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 48
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_0

    .line 49
    const-string v0, "ContextFramework.PermissionManagerService"

    const-string v1, "onStartCommand() is called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :cond_0
    const/4 v0, 0x3

    return v0
.end method
