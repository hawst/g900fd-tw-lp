.class Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$GpsMonitor;
.super Ljava/lang/Object;
.source "DeviceStatusMonitor.java"

# interfaces
.implements Landroid/location/GpsStatus$Listener;
.implements Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GpsMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;


# direct methods
.method private constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$GpsMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
    .param p2, "x1"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;

    .prologue
    .line 514
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$GpsMonitor;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    return-void
.end method


# virtual methods
.method public disable(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 540
    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 541
    .local v0, "lm":Landroid/location/LocationManager;
    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 542
    return-void
.end method

.method public enable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 536
    return-void
.end method

.method public onGpsStatusChanged(I)V
    .locals 3
    .param p1, "event"    # I

    .prologue
    const/16 v2, 0x20

    .line 518
    packed-switch p1, :pswitch_data_0

    .line 532
    :goto_0
    :pswitch_0
    return-void

    .line 520
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$GpsMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBoolean(IZ)Z
    invoke-static {v0, v2, v1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->access$400(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)Z

    goto :goto_0

    .line 523
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$GpsMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBoolean(IZ)Z
    invoke-static {v0, v2, v1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->access$400(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)Z

    goto :goto_0

    .line 518
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onUserBackground(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 552
    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 553
    .local v0, "lm":Landroid/location/LocationManager;
    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 554
    return-void
.end method

.method public onUserForeground(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 546
    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 547
    .local v0, "lm":Landroid/location/LocationManager;
    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    .line 548
    return-void
.end method
