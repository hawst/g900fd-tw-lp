.class Lcom/samsung/android/providers/context/server/ServerUploader$1;
.super Landroid/content/BroadcastReceiver;
.source "ServerUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/providers/context/server/ServerUploader;->registSPPReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/server/ServerUploader;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/server/ServerUploader;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/ServerUploader$1;->this$0:Lcom/samsung/android/providers/context/server/ServerUploader;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 128
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 129
    :cond_0
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v5, "Invailid Params"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_1
    :goto_0
    return-void

    .line 132
    :cond_2
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Receive broadcast msg - action : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 135
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 137
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 138
    const-string v4, "EXTRA_STR_ACTION"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 141
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v5, "EXTRA_STR_ACTION is null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 144
    :cond_3
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Recieve broadcast msg - extra : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 146
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.logsample.DLC_FILTER"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 147
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 149
    .restart local v1    # "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 150
    const-string v4, "EXTRA_STR_ACTION"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    .restart local v0    # "action":Ljava/lang/String;
    if-nez v0, :cond_5

    .line 153
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v5, "EXTRA_STR_ACTION is null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 156
    :cond_5
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Recieve broadcast msg - extra : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    const-string v4, "ACTION_RESULT_REGISTER"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 159
    const-string v4, "EXTRA_RESULT_CODE"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 160
    .local v3, "resultCode":I
    const-string v4, "EXTRA_STR"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 161
    .local v2, "result":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Register result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/samsung/android/providers/context/server/RegiEvent;->fromInt(I)Lcom/samsung/android/providers/context/server/RegiEvent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/providers/context/server/RegiEvent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    if-eqz v2, :cond_1

    sget-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_SUCCESS:Lcom/samsung/android/providers/context/server/RegiEvent;

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/server/RegiEvent;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 164
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/ServerUploader$1;->this$0:Lcom/samsung/android/providers/context/server/ServerUploader;

    # invokes: Lcom/samsung/android/providers/context/server/ServerUploader;->setRegistered()V
    invoke-static {v4}, Lcom/samsung/android/providers/context/server/ServerUploader;->access$000(Lcom/samsung/android/providers/context/server/ServerUploader;)V

    goto/16 :goto_0

    .line 166
    .end local v2    # "result":Ljava/lang/String;
    .end local v3    # "resultCode":I
    :cond_6
    const-string v4, "ACTION_RESULT_DEREGISTER"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 167
    const-string v4, "EXTRA_RESULT_CODE"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 168
    .restart local v3    # "resultCode":I
    const-string v4, "EXTRA_STR"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 169
    .restart local v2    # "result":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deregister result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/samsung/android/providers/context/server/RegiEvent;->fromInt(I)Lcom/samsung/android/providers/context/server/RegiEvent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/providers/context/server/RegiEvent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    if-eqz v2, :cond_1

    sget-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_SUCCESS:Lcom/samsung/android/providers/context/server/RegiEvent;

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/server/RegiEvent;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 172
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/ServerUploader$1;->this$0:Lcom/samsung/android/providers/context/server/ServerUploader;

    # invokes: Lcom/samsung/android/providers/context/server/ServerUploader;->setDeregistered()V
    invoke-static {v4}, Lcom/samsung/android/providers/context/server/ServerUploader;->access$100(Lcom/samsung/android/providers/context/server/ServerUploader;)V

    goto/16 :goto_0

    .line 174
    .end local v2    # "result":Ljava/lang/String;
    .end local v3    # "resultCode":I
    :cond_7
    const-string v4, "available"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 175
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v5, "ACTION LOG AVAILABLE"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/ServerUploader$1;->this$0:Lcom/samsung/android/providers/context/server/ServerUploader;

    # invokes: Lcom/samsung/android/providers/context/server/ServerUploader;->setAvailable()V
    invoke-static {v4}, Lcom/samsung/android/providers/context/server/ServerUploader;->access$200(Lcom/samsung/android/providers/context/server/ServerUploader;)V

    goto/16 :goto_0

    .line 177
    :cond_8
    const-string v4, "unavailable"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 178
    sget-object v4, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    const-string v5, "ACTION LOG UNAVAILABLE"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v4, p0, Lcom/samsung/android/providers/context/server/ServerUploader$1;->this$0:Lcom/samsung/android/providers/context/server/ServerUploader;

    # invokes: Lcom/samsung/android/providers/context/server/ServerUploader;->setUnavailable()V
    invoke-static {v4}, Lcom/samsung/android/providers/context/server/ServerUploader;->access$300(Lcom/samsung/android/providers/context/server/ServerUploader;)V

    goto/16 :goto_0
.end method
