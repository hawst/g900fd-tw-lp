.class public Lcom/samsung/android/providers/context/log/ContextLogContract;
.super Ljava/lang/Object;
.source "ContextLogContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/log/ContextLogContract$ChangeActivityColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ChangeActivity;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$MoveAreaColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$MoveArea;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$MoveLocationColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$MoveLocation;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ReportAppStatusSurveyColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ReportAppStatusSurvey;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseMemorySurveyColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseMemorySurvey;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseBatterySurveyColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseBatterySurvey;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ReportAppErrorSurveyColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ReportAppErrorSurvey;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ManageAppSurveyColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ManageAppSurvey;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppSurveyColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppSurvey;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurveyColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$CaptureContentColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$CaptureContent;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$WriteDocumentColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$WriteDocument;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$RecordAudioColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$RecordAudio;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$RecordVideoColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$RecordVideo;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ChangeDeviceStatusColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ChangeDeviceStatus;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseWifiColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseWifi;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$TrackRoamingColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$TrackRoaming;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$MovePlaceColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$MovePlace;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$SearchKeywordColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$SearchKeyword;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$PlayVideoColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$PlayVideo;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeEmailColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeEmail;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$TakePhotoColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$TakePhoto;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeMessageColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeMessage;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeCallColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeCall;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$BrowseWebColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$BrowseWeb;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$PlayMusicColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$PlayMusic;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ManageAppColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$ManageApp;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$UseApp;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$BaseContextColumns;,
        Lcom/samsung/android/providers/context/log/ContextLogContract$BaseContextConstants;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3090
    return-void
.end method
