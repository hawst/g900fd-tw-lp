.class public final enum Lcom/samsung/android/providers/context/server/RegiEvent;
.super Ljava/lang/Enum;
.source "RegiEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/providers/context/server/RegiEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_ALREADY_REGISTERED:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_BLOCKED_APP:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_FAIL:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_HTTP_FAIL:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_INTERNAL_DB_ERROR:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_INTERNAL_ERROR:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_INVALID_PARAMS:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_SUCCESS:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESULT_TIMEOUT:Lcom/samsung/android/providers/context/server/RegiEvent;

.field public static final enum RESUlT_PACKAGE_NOT_FOUND:Lcom/samsung/android/providers/context/server/RegiEvent;

.field private static final TAG:Ljava/lang/String;

.field public static final enum UNKNOWN:Lcom/samsung/android/providers/context/server/RegiEvent;

.field private static final amount:I

.field private static eventSet:[Lcom/samsung/android/providers/context/server/RegiEvent;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 28
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_SUCCESS"

    const/16 v6, 0x64

    invoke-direct {v4, v5, v8, v6}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_SUCCESS:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 29
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_ALREADY_REGISTERED"

    const/16 v6, 0xc8

    invoke-direct {v4, v5, v9, v6}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_ALREADY_REGISTERED:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 32
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_FAIL"

    const/4 v6, -0x1

    invoke-direct {v4, v5, v10, v6}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_FAIL:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 33
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_INVALID_PARAMS"

    const/4 v6, -0x2

    invoke-direct {v4, v5, v11, v6}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_INVALID_PARAMS:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 34
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_INTERNAL_ERROR"

    const/4 v6, -0x3

    invoke-direct {v4, v5, v12, v6}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_INTERNAL_ERROR:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 35
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_INTERNAL_DB_ERROR"

    const/4 v6, 0x5

    const/4 v7, -0x4

    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_INTERNAL_DB_ERROR:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 36
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_HTTP_FAIL"

    const/4 v6, 0x6

    const/4 v7, -0x5

    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_HTTP_FAIL:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 37
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_TIMEOUT"

    const/4 v6, 0x7

    const/4 v7, -0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_TIMEOUT:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 38
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESULT_BLOCKED_APP"

    const/16 v6, 0x8

    const/4 v7, -0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_BLOCKED_APP:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 39
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "RESUlT_PACKAGE_NOT_FOUND"

    const/16 v6, 0x9

    const/4 v7, -0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->RESUlT_PACKAGE_NOT_FOUND:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 40
    new-instance v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    const-string v5, "UNKNOWN"

    const/16 v6, 0xa

    const/16 v7, -0x3e8

    invoke-direct {v4, v5, v6, v7}, Lcom/samsung/android/providers/context/server/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->UNKNOWN:Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 25
    const/16 v4, 0xb

    new-array v4, v4, [Lcom/samsung/android/providers/context/server/RegiEvent;

    sget-object v5, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_SUCCESS:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v5, v4, v8

    sget-object v5, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_ALREADY_REGISTERED:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v5, v4, v9

    sget-object v5, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_FAIL:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v5, v4, v10

    sget-object v5, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_INVALID_PARAMS:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v5, v4, v11

    sget-object v5, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_INTERNAL_ERROR:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v5, v4, v12

    const/4 v5, 0x5

    sget-object v6, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_INTERNAL_DB_ERROR:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_HTTP_FAIL:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_TIMEOUT:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/samsung/android/providers/context/server/RegiEvent;->RESULT_BLOCKED_APP:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/samsung/android/providers/context/server/RegiEvent;->RESUlT_PACKAGE_NOT_FOUND:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/samsung/android/providers/context/server/RegiEvent;->UNKNOWN:Lcom/samsung/android/providers/context/server/RegiEvent;

    aput-object v6, v4, v5

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->$VALUES:[Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 42
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/RegiEvent"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->TAG:Ljava/lang/String;

    .line 55
    const-class v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    invoke-static {v4}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/EnumSet;->size()I

    move-result v4

    sput v4, Lcom/samsung/android/providers/context/server/RegiEvent;->amount:I

    .line 56
    sget v4, Lcom/samsung/android/providers/context/server/RegiEvent;->amount:I

    new-array v4, v4, [Lcom/samsung/android/providers/context/server/RegiEvent;

    sput-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->eventSet:[Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "i":I
    const-class v4, Lcom/samsung/android/providers/context/server/RegiEvent;

    invoke-static {v4}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 60
    .local v3, "q":Lcom/samsung/android/providers/context/server/RegiEvent;
    sget-object v4, Lcom/samsung/android/providers/context/server/RegiEvent;->eventSet:[Lcom/samsung/android/providers/context/server/RegiEvent;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput-object v3, v4, v0

    move v0, v1

    .line 61
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 62
    .end local v3    # "q":Lcom/samsung/android/providers/context/server/RegiEvent;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/samsung/android/providers/context/server/RegiEvent;->value:I

    .line 46
    return-void
.end method

.method public static fromInt(I)Lcom/samsung/android/providers/context/server/RegiEvent;
    .locals 4
    .param p0, "i"    # I

    .prologue
    .line 65
    sget-object v2, Lcom/samsung/android/providers/context/server/RegiEvent;->TAG:Ljava/lang/String;

    const-string v3, "fromInt()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-class v2, Lcom/samsung/android/providers/context/server/RegiEvent;

    invoke-static {v2}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/server/RegiEvent;

    .line 67
    .local v1, "q":Lcom/samsung/android/providers/context/server/RegiEvent;
    invoke-virtual {v1}, Lcom/samsung/android/providers/context/server/RegiEvent;->value()I

    move-result v2

    if-ne v2, p0, :cond_0

    .line 71
    .end local v1    # "q":Lcom/samsung/android/providers/context/server/RegiEvent;
    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lcom/samsung/android/providers/context/server/RegiEvent;->UNKNOWN:Lcom/samsung/android/providers/context/server/RegiEvent;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/providers/context/server/RegiEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/samsung/android/providers/context/server/RegiEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/server/RegiEvent;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/providers/context/server/RegiEvent;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/android/providers/context/server/RegiEvent;->$VALUES:[Lcom/samsung/android/providers/context/server/RegiEvent;

    invoke-virtual {v0}, [Lcom/samsung/android/providers/context/server/RegiEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/providers/context/server/RegiEvent;

    return-object v0
.end method


# virtual methods
.method public value()I
    .locals 2

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/android/providers/context/server/RegiEvent;->TAG:Ljava/lang/String;

    const-string v1, "value()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iget v0, p0, Lcom/samsung/android/providers/context/server/RegiEvent;->value:I

    return v0
.end method
