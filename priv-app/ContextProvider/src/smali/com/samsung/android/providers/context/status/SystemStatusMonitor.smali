.class public Lcom/samsung/android/providers/context/status/SystemStatusMonitor;
.super Ljava/lang/Object;
.source "SystemStatusMonitor.java"


# static fields
.field private static final GESTURE_SETTING_LIST:[[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 50
    const/16 v0, 0xb

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "G010"

    aput-object v2, v1, v4

    const-string v2, "G011"

    aput-object v2, v1, v5

    const-string v2, "air_motion_wake_up"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "G020"

    aput-object v2, v1, v4

    const-string v2, "G021"

    aput-object v2, v1, v5

    const-string v2, "smart_scroll"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "G030"

    aput-object v2, v1, v4

    const-string v2, "G031"

    aput-object v2, v1, v5

    const-string v2, "air_motion_call_accept"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "G040"

    aput-object v2, v1, v4

    const-string v2, "G041"

    aput-object v2, v1, v5

    const-string v2, "air_motion_scroll"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "G050"

    aput-object v3, v2, v4

    const-string v3, "G051"

    aput-object v3, v2, v5

    const-string v3, "motion_pick_up_to_call_out"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "G060"

    aput-object v3, v2, v4

    const-string v3, "G061"

    aput-object v3, v2, v5

    const-string v3, "motion_pick_up"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "G070"

    aput-object v3, v2, v4

    const-string v3, "G071"

    aput-object v3, v2, v5

    const-string v3, "motion_merged_mute_pause"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "G080"

    aput-object v3, v2, v4

    const-string v3, "G081"

    aput-object v3, v2, v5

    const-string v3, "surface_palm_swipe"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "G090"

    aput-object v3, v2, v4

    const-string v3, "G091"

    aput-object v3, v2, v5

    const-string v3, "intelligent_rotation_mode"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "G0A0"

    aput-object v3, v2, v4

    const-string v3, "G0A1"

    aput-object v3, v2, v5

    const-string v3, "intelligent_sleep_mode"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "G0B0"

    aput-object v3, v2, v4

    const-string v3, "G0B1"

    aput-object v3, v2, v5

    const-string v3, "finger_air_view"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/providers/context/status/SystemStatusMonitor;->GESTURE_SETTING_LIST:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static logDefaultHome(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v13, 0x80

    .line 146
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v10

    iget-boolean v9, v10, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    .line 148
    .local v9, "visible":Z
    const/4 v3, -0x1

    .line 150
    .local v3, "flags":I
    new-instance v4, Landroid/content/Intent;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 151
    .local v4, "intent":Landroid/content/Intent;
    const-string v10, "android.intent.category.HOME"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 153
    .local v6, "pm":Landroid/content/pm/PackageManager;
    const/high16 v10, 0x10000

    invoke-virtual {v6, v4, v10}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v7

    .line 154
    .local v7, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-nez v7, :cond_1

    .line 155
    if-eqz v9, :cond_0

    .line 156
    const-string v10, "ContextFramework:SystemStatusMonitor"

    const-string v11, "Default home information is not found."

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v0, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 163
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_2

    .line 164
    if-eqz v9, :cond_0

    .line 165
    const-string v10, "ContextFramework:SystemStatusMonitor"

    const-string v11, "Default home\'s activity information is not found."

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 171
    :cond_2
    iget-object v5, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 172
    .local v5, "packageName":Ljava/lang/String;
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 173
    :cond_3
    if-eqz v9, :cond_0

    .line 174
    const-string v10, "ContextFramework:SystemStatusMonitor"

    const-string v11, "Default home\'s package name is not found."

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 181
    :cond_4
    const/16 v10, 0x80

    :try_start_0
    invoke-virtual {v6, v5, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 182
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget v3, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 183
    if-eqz v9, :cond_5

    .line 184
    const-string v10, "ContextFramework:SystemStatusMonitor"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Home\'s application flag is ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :cond_5
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 194
    .local v8, "row":Landroid/content/ContentValues;
    const-string v10, "app_id"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    and-int/lit8 v10, v3, 0x1

    const/4 v11, 0x1

    if-eq v10, v11, :cond_6

    and-int/lit16 v10, v3, 0x80

    if-ne v10, v13, :cond_8

    .line 198
    :cond_6
    if-eqz v9, :cond_7

    .line 199
    const-string v10, "ContextFramework:SystemStatusMonitor"

    const-string v11, "Home is system application."

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_7
    const-string v10, "feature"

    const-string v11, "HOPR"

    invoke-virtual {v8, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v10, v11, v8}, Lcom/samsung/android/providers/context/util/ContextLogger;->logLocalProvider(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Z

    goto :goto_0

    .line 186
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v8    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v2

    .line 187
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v10, "ContextFramework:SystemStatusMonitor"

    const-string v11, "ERROR for getting home\'s application infomation."

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    const-string v10, "ContextFramework:SystemStatusMonitor"

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 203
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v8    # "row":Landroid/content/ContentValues;
    :cond_8
    if-eqz v9, :cond_9

    .line 204
    const-string v10, "ContextFramework:SystemStatusMonitor"

    const-string v11, "Home is not system application."

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_9
    const-string v10, "feature"

    const-string v11, "HODO"

    invoke-virtual {v8, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static logGestureSetting(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v11

    iget-boolean v10, v11, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    .line 92
    .local v10, "visible":Z
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 93
    .local v7, "resolver":Landroid/content/ContentResolver;
    sget-object v11, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v11}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v1

    .line 94
    .local v1, "client":Landroid/content/ContentProviderClient;
    if-nez v1, :cond_1

    .line 95
    if-eqz v10, :cond_0

    .line 96
    const-string v11, "ContextFramework:SystemStatusMonitor"

    const-string v12, "Gesture setting logging fails due to absence of content provider client."

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v6

    .line 101
    .local v6, "provider":Landroid/content/ContentProvider;
    if-nez v6, :cond_2

    .line 102
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    .line 103
    if-eqz v10, :cond_0

    .line 104
    const-string v11, "ContextFramework:SystemStatusMonitor"

    const-string v12, "Gesture setting logging fails due to absence of local content provider."

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 110
    :cond_2
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 111
    .local v8, "row":Landroid/content/ContentValues;
    const/4 v9, 0x0

    .line 113
    .local v9, "settingValue":I
    sget-object v0, Lcom/samsung/android/providers/context/status/SystemStatusMonitor;->GESTURE_SETTING_LIST:[[Ljava/lang/String;

    .local v0, "arr$":[[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_7

    aget-object v4, v0, v3

    .line 114
    .local v4, "item":[Ljava/lang/String;
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 116
    const/4 v11, 0x2

    aget-object v11, v4, v11

    const/4 v12, -0x1

    invoke-static {v7, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    .line 117
    if-nez v9, :cond_5

    .line 119
    const-string v11, "feature"

    const/4 v12, 0x0

    aget-object v12, v4, v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :goto_2
    const-string v11, "app_id"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    if-eqz v10, :cond_3

    .line 132
    const-string v11, "ContextFramework:SystemStatusMonitor"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Insert with value ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "feature"

    invoke-virtual {v8, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_3
    :try_start_0
    sget-object v11, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v11, v8}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :cond_4
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 120
    :cond_5
    const/4 v11, 0x1

    if-ne v9, v11, :cond_6

    .line 122
    const-string v11, "feature"

    const/4 v12, 0x1

    aget-object v12, v4, v12

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 124
    :cond_6
    if-eqz v10, :cond_4

    .line 125
    const-string v11, "ContextFramework:SystemStatusMonitor"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to get setting value for ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x2

    aget-object v13, v4, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 136
    :catch_0
    move-exception v2

    .line 137
    .local v2, "ex":Ljava/lang/Exception;
    const-string v11, "ContextFramework:SystemStatusMonitor"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 142
    .end local v2    # "ex":Ljava/lang/Exception;
    .end local v4    # "item":[Ljava/lang/String;
    :cond_7
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    goto/16 :goto_0
.end method

.method public static logWeeklyReport(Landroid/content/Context;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v6, 0x0

    .line 66
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v2

    .line 67
    .local v2, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v3, "SettingLoggedDate"

    invoke-virtual {v2, v3, v6, v7}, Lcom/samsung/android/providers/context/ContextPreference;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 68
    .local v4, "prevDate":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 71
    .local v0, "now":J
    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 72
    const-string v3, "ContextFramework:SystemStatusMonitor"

    const-string v6, "Current time is earlier than last logged time."

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const-string v3, "SettingLoggedDate"

    invoke-virtual {v2, v3, v0, v1}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    .line 87
    :goto_0
    return-void

    .line 76
    :cond_0
    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    sub-long v6, v0, v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x7

    sget-object v10, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v8, v9, v10}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-gez v3, :cond_1

    .line 77
    const-string v3, "ContextFramework:SystemStatusMonitor"

    const-string v6, "No need to log weekly report at this time"

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 81
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/providers/context/status/SystemStatusMonitor;->logGestureSetting(Landroid/content/Context;)V

    .line 82
    invoke-static {p0}, Lcom/samsung/android/providers/context/status/SystemStatusMonitor;->logDefaultHome(Landroid/content/Context;)V

    .line 85
    const-string v3, "SettingLoggedDate"

    invoke-virtual {v2, v3, v0, v1}, Lcom/samsung/android/providers/context/ContextPreference;->putLong(Ljava/lang/String;J)V

    .line 86
    const-string v3, "ContextFramework:SystemStatusMonitor"

    const-string v6, "Weekly report logging is completed."

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
