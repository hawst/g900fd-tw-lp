.class final Lcom/samsung/android/providers/context/log/UseAppLogProvider$1;
.super Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
.source "UseAppLogProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/log/UseAppLogProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/providers/context/log/LogTableProperty;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    return-void
.end method


# virtual methods
.method public createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 55
    :try_start_0
    const-string v1, "CREATE TABLE IF NOT EXISTS use_app (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,timestamp_utc DATETIME,timestamp INTEGER,time_zone TEXT,place_name TEXT,place_category INTEGER,longitude DOUBLE,latitude DOUBLE,compat_device_status INTEGER,app_id TEXT,app_sub_id TEXT,launcher_type INTEGER,start_time DATETIME,stop_time DATETIME,starttime INTEGER,stoptime INTEGER,duration INTEGER,verification INTEGER);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v1, :cond_0

    .line 76
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error executing SQL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public upgradeTable(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 85
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS use_app"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/log/UseAppLogProvider$1;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v1, :cond_0

    .line 89
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error executing SQL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
