.class public Lcom/samsung/android/providers/context/log/ChangeActivityLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "ChangeActivityLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 35
    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "timestamp_utc"

    aput-object v3, v1, v2

    const-string v2, "timestamp"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "time_zone"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "place_name"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "place_category"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "longitude"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "latitude"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "accuracy"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/ChangeActivityLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "change_activity"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/ChangeActivityLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 47
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->fillCommonColumnsManually:Z

    .line 48
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->supportUpdate:Z

    .line 49
    const/16 v1, 0x1770

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    .line 50
    const/16 v1, 0x258

    iput v1, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    .line 52
    new-instance v1, Lcom/samsung/android/providers/context/log/ChangeActivityLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/ChangeActivityLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/ChangeActivityLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 91
    sget-object v1, Lcom/samsung/android/providers/context/log/ChangeActivityLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 92
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/samsung/android/providers/context/log/ChangeActivityLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 123
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 5
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 96
    const/4 v2, -0x1

    .line 97
    .local v2, "type":I
    const/4 v0, -0x1

    .line 100
    .local v0, "accuracy":I
    :try_start_0
    const-string v3, "type"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 101
    const-string v3, "accuracy"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 107
    :goto_0
    if-ltz v2, :cond_0

    const/4 v3, 0x7

    if-le v2, v3, :cond_1

    .line 108
    :cond_0
    const-string v3, "ContextFramework"

    const-string v4, "ChangeActivity : type is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    if-lez v0, :cond_2

    const/16 v3, 0x64

    if-le v0, v3, :cond_3

    .line 111
    :cond_2
    const-string v3, "ContextFramework"

    const-string v4, "ChangeActivity : accuracy is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_3
    const/4 v3, 0x1

    return v3

    .line 102
    :catch_0
    move-exception v1

    .line 103
    .local v1, "ex":Ljava/lang/NullPointerException;
    const-string v3, "ContextFramework"

    const-string v4, "ChangeActivity : ValidBoundaryCheck - NullPointerException."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
