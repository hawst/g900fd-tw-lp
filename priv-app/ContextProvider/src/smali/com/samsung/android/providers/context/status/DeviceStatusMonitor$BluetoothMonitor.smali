.class Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;
.super Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;
.source "DeviceStatusMonitor.java"

# interfaces
.implements Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
    .locals 1

    .prologue
    .line 455
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;)V

    .line 456
    new-instance v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor$1;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 475
    return-void
.end method

.method private checkCurrentState(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 478
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 479
    .local v0, "adapter":Landroid/bluetooth/BluetoothAdapter;
    const/4 v7, 0x5

    new-array v6, v7, [I

    fill-array-data v6, :array_0

    .line 481
    .local v6, "profiles":[I
    const/4 v2, 0x0

    .line 482
    .local v2, "connected":Z
    move-object v1, v6

    .local v1, "arr$":[I
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget v5, v1, v3

    .line 483
    .local v5, "profile":I
    invoke-virtual {v0, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_1

    .line 484
    const/4 v2, 0x1

    .line 488
    .end local v5    # "profile":I
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/16 v8, 0x10

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBoolean(IZ)Z
    invoke-static {v7, v8, v2}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->access$400(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)Z

    .line 489
    return-void

    .line 482
    .restart local v5    # "profile":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 479
    nop

    :array_0
    .array-data 4
        0x2
        0x1
        0x3
        0x7
        0x8
    .end array-data
.end method


# virtual methods
.method public disable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 497
    return-void
.end method

.method public enable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 493
    return-void
.end method

.method public onUserBackground(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 507
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 508
    return-void
.end method

.method public onUserForeground(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 501
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;->checkCurrentState(Landroid/content/Context;)V

    .line 502
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$BluetoothMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 503
    return-void
.end method
