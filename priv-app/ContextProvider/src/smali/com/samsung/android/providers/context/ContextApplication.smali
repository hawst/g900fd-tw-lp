.class public Lcom/samsung/android/providers/context/ContextApplication;
.super Landroid/app/Application;
.source "ContextApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/samsung/android/providers/context/ContextApplication;


# instance fields
.field private final mHandler:Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;

.field private mInitTask:Lcom/samsung/android/providers/context/ContextInitTask;

.field private mLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/LogDbHelper;

.field private mPermissionDatabaseHelper:Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

.field private mPermissionManager:Lcom/samsung/android/providers/context/privacy/PermissionManager;

.field private mPrefs:Lcom/samsung/android/providers/context/ContextPreference;

.field private mSamsungAccountInfo:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

.field private mSurveyLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

.field private mTask:Lcom/samsung/android/providers/context/TaskScheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/providers/context/ContextApplication;->sInstance:Lcom/samsung/android/providers/context/ContextApplication;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 64
    new-instance v0, Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;-><init>(Lcom/samsung/android/providers/context/ContextApplication;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mHandler:Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;

    .line 77
    sput-object p0, Lcom/samsung/android/providers/context/ContextApplication;->sInstance:Lcom/samsung/android/providers/context/ContextApplication;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/providers/context/ContextApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/ContextApplication;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/samsung/android/providers/context/ContextApplication;->delayedInitiation()V

    return-void
.end method

.method private delayedInitiation()V
    .locals 2

    .prologue
    .line 115
    const-string v0, "ContextFramework"

    const-string v1, "ContextApplication: delayed initialization"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    new-instance v0, Lcom/samsung/android/providers/context/TaskScheduler;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/TaskScheduler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mTask:Lcom/samsung/android/providers/context/TaskScheduler;

    .line 117
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mTask:Lcom/samsung/android/providers/context/TaskScheduler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/TaskScheduler;->init()V

    .line 118
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/providers/context/ContextApplication;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 167
    sget-object v0, Lcom/samsung/android/providers/context/ContextApplication;->sInstance:Lcom/samsung/android/providers/context/ContextApplication;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CaApplication is NOT created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/ContextApplication;->sInstance:Lcom/samsung/android/providers/context/ContextApplication;

    return-object v0
.end method

.method private static isWifiOnly(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 252
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 253
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method static notifyAppCreated(Landroid/content/Context;)V
    .locals 4
    .param p0, "inst"    # Landroid/content/Context;

    .prologue
    .line 213
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.providers.context.intent.APP_CREATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 214
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 215
    .local v2, "time":J
    const-string v1, "time"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 218
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 219
    return-void
.end method


# virtual methods
.method checkSurveyLogSetting()V
    .locals 6

    .prologue
    .line 222
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v3

    iget-boolean v0, v3, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    .line 224
    .local v0, "logVisible":Z
    const/4 v3, 0x6

    invoke-static {v3}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v3

    if-nez v3, :cond_1

    .line 225
    if-eqz v0, :cond_0

    .line 226
    const-string v3, "ContextFramework"

    const-string v4, "Survey log is not supported."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 231
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/ContextApplication;->getInitedPermissionManager()Lcom/samsung/android/providers/context/privacy/PermissionManager;

    move-result-object v1

    .line 234
    .local v1, "manager":Lcom/samsung/android/providers/context/privacy/PermissionManager;
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "samsung_errorlog_agree"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 236
    .local v2, "settingValue":I
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 237
    if-eqz v0, :cond_2

    .line 238
    const-string v3, "ContextFramework"

    const-string v4, "Survey log is enabled."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/ContextApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->setPrivacyItemInternal(Ljava/lang/String;I)V

    goto :goto_0

    .line 243
    :cond_3
    if-eqz v0, :cond_4

    .line 244
    const-string v3, "ContextFramework"

    const-string v4, "Survey log is disabled."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/providers/context/ContextApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->unsetPrivacyItemInternal(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mHandler:Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;

    return-object v0
.end method

.method public declared-synchronized getInitedPermissionManager()Lcom/samsung/android/providers/context/privacy/PermissionManager;
    .locals 1

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionManager:Lcom/samsung/android/providers/context/privacy/PermissionManager;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PermissionManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/privacy/PermissionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionManager:Lcom/samsung/android/providers/context/privacy/PermissionManager;

    .line 107
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionManager:Lcom/samsung/android/providers/context/privacy/PermissionManager;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->init()V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionManager:Lcom/samsung/android/providers/context/privacy/PermissionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLogDatabase()Lcom/samsung/android/providers/context/log/db/LogDbHelper;
    .locals 2

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->isDeleted()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    :cond_0
    :try_start_1
    new-instance v1, Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    invoke-direct {v1, p0}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/LogDbHelper;
    :try_end_1
    .catch Lcom/samsung/android/providers/context/security/NoKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    :cond_1
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/LogDbHelper;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v1

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Lcom/samsung/android/providers/context/security/NoKeyException;
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/LogDbHelper;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 129
    .end local v0    # "e":Lcom/samsung/android/providers/context/security/NoKeyException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;
    .locals 1

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionDatabaseHelper:Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionDatabaseHelper:Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    :cond_0
    new-instance v0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionDatabaseHelper:Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionDatabaseHelper:Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSamsungAccountInfo()Lcom/samsung/android/providers/context/server/SamsungAccountInfo;
    .locals 1

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mSamsungAccountInfo:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getStorage()Lcom/samsung/android/providers/context/ContextPreference;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPrefs:Lcom/samsung/android/providers/context/ContextPreference;

    return-object v0
.end method

.method public declared-synchronized getSurveyLogDatabase()Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    .locals 2

    .prologue
    .line 141
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mSurveyLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mSurveyLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;->isDeleted()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    :cond_0
    :try_start_1
    new-instance v1, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

    invoke-direct {v1, p0}, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mSurveyLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    :try_end_1
    .catch Lcom/samsung/android/providers/context/security/NoKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    :cond_1
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mSurveyLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v1

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Lcom/samsung/android/providers/context/security/NoKeyException;
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/samsung/android/providers/context/ContextApplication;->mSurveyLogDatabaseHelper:Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 141
    .end local v0    # "e":Lcom/samsung/android/providers/context/security/NoKeyException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized initSamsungAccountInfo()V
    .locals 1

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mSamsungAccountInfo:Lcom/samsung/android/providers/context/server/SamsungAccountInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    monitor-exit p0

    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 82
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 84
    invoke-static {p0}, Lcom/samsung/android/providers/context/FeatureManager;->updateFeature(Landroid/content/Context;)V

    .line 86
    invoke-static {p0}, Lcom/samsung/android/providers/context/ContextApplication;->isWifiOnly(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/android/providers/context/GlobalStatus;->wifiOnly:Z

    .line 90
    :cond_0
    new-instance v0, Lcom/samsung/android/providers/context/ContextPreference;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/ContextPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPrefs:Lcom/samsung/android/providers/context/ContextPreference;

    .line 94
    new-instance v0, Lcom/samsung/android/providers/context/ContextInitTask;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/ContextInitTask;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mInitTask:Lcom/samsung/android/providers/context/ContextInitTask;

    .line 95
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mInitTask:Lcom/samsung/android/providers/context/ContextInitTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/ContextInitTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 97
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    const/4 v2, 0x0

    const-class v3, Lcom/samsung/android/providers/context/ContextService;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/providers/context/ContextApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 99
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mHandler:Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;

    const/16 v1, 0x9

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/ContextApplication$ContextHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 101
    const-string v0, "ContextFramework"

    const-string v1, "ContextApplication: onCreate() done"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return-void
.end method

.method public onTerminate()V
    .locals 1

    .prologue
    .line 155
    invoke-static {}, Lcom/samsung/android/providers/context/status/LocationTracker;->getInstance()Lcom/samsung/android/providers/context/status/LocationTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/LocationTracker;->disable()V

    .line 156
    invoke-static {}, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/PhysicalActivityMonitor;->disable()V

    .line 157
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mPermissionManager:Lcom/samsung/android/providers/context/privacy/PermissionManager;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->close()V

    .line 159
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mTask:Lcom/samsung/android/providers/context/TaskScheduler;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextApplication;->mTask:Lcom/samsung/android/providers/context/TaskScheduler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/TaskScheduler;->fini()V

    .line 163
    :cond_0
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 164
    return-void
.end method

.method waitSamsungAccountReadyAndFree()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 183
    iget-object v2, p0, Lcom/samsung/android/providers/context/ContextApplication;->mInitTask:Lcom/samsung/android/providers/context/ContextInitTask;

    if-nez v2, :cond_0

    .line 201
    :goto_0
    return v1

    .line 188
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/providers/context/ContextApplication;->mInitTask:Lcom/samsung/android/providers/context/ContextInitTask;

    const-wide/16 v4, 0x3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Lcom/samsung/android/providers/context/ContextInitTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    .line 190
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/providers/context/ContextApplication;->mInitTask:Lcom/samsung/android/providers/context/ContextInitTask;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 192
    :catch_0
    move-exception v0

    .line 193
    .local v0, "ex":Ljava/util/concurrent/CancellationException;
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialization task is cancelled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/concurrent/CancellationException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    .end local v0    # "ex":Ljava/util/concurrent/CancellationException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 194
    :catch_1
    move-exception v0

    .line 195
    .local v0, "ex":Ljava/util/concurrent/ExecutionException;
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialization task failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 196
    .end local v0    # "ex":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v0

    .line 197
    .local v0, "ex":Ljava/lang/InterruptedException;
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialization task interruped: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 198
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v0

    .line 199
    .local v0, "ex":Ljava/util/concurrent/TimeoutException;
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialization task not finished: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
