.class Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;
.super Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;
.source "DeviceStatusMonitor.java"

# interfaces
.implements Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectivityMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
    .locals 1

    .prologue
    .line 388
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;)V

    .line 389
    new-instance v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 407
    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 387
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkRoamingStatus(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 387
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkWifiStatus(Landroid/content/Context;)V

    return-void
.end method

.method private checkCurrentStatus(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 410
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkRoamingStatus(Landroid/content/Context;)V

    .line 411
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkWifiStatus(Landroid/content/Context;)V

    .line 412
    return-void
.end method

.method private checkRoamingStatus(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 415
    const-string v2, "phone"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 418
    .local v1, "tel":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    .line 419
    .local v0, "isRoaming":Z
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/16 v3, 0x100

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBooleanVersion1(IZ)V
    invoke-static {v2, v3, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->access$200(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)V

    .line 420
    return-void
.end method

.method private checkWifiStatus(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 423
    const-string v3, "connectivity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 424
    .local v0, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 425
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 426
    .local v1, "isWifiConnected":Z
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/16 v4, 0x40

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->setDeviceStatusBoolean(IZ)Z
    invoke-static {v3, v4, v1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->access$400(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;IZ)Z

    .line 427
    return-void

    .line 425
    .end local v1    # "isWifiConnected":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public disable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 435
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->unregisterReceiver(Landroid/content/Context;)V

    .line 436
    return-void
.end method

.method public enable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 431
    return-void
.end method

.method public onUserBackground(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 446
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->unregisterReceiver(Landroid/content/Context;)V

    .line 447
    return-void
.end method

.method public onUserForeground(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 440
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkCurrentStatus(Landroid/content/Context;)V

    .line 441
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->registerReceiver(Landroid/content/Context;Landroid/content/IntentFilter;)V

    .line 442
    return-void
.end method
