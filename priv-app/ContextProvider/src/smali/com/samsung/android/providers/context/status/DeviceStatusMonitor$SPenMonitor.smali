.class Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;
.super Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;
.source "DeviceStatusMonitor.java"

# interfaces
.implements Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalMonitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SPenMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
    .locals 1

    .prologue
    .line 227
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;->this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;)V

    .line 228
    new-instance v0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor$1;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 237
    return-void
.end method


# virtual methods
.method public disable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 245
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;->unregisterReceiver(Landroid/content/Context;)V

    .line 246
    return-void
.end method

.method public enable(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 241
    return-void
.end method

.method public onUserBackground(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;->unregisterReceiver(Landroid/content/Context;)V

    .line 256
    return-void
.end method

.method public onUserForeground(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 250
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.samsung.pen.INSERT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$SPenMonitor;->registerReceiver(Landroid/content/Context;Landroid/content/IntentFilter;)V

    .line 251
    return-void
.end method
