.class public Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;
.super Landroid/os/Handler;
.source "LogSender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/server/LogSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConnectionMsgHandler"
.end annotation


# instance fields
.field private final mLogSender:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/providers/context/server/LogSender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/server/LogSender;)V
    .locals 1
    .param p1, "logSender"    # Lcom/samsung/android/providers/context/server/LogSender;

    .prologue
    .line 141
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 142
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;->mLogSender:Ljava/lang/ref/WeakReference;

    .line 143
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 148
    # getter for: Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/LogSender;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ConnectionMsgHandler()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;->mLogSender:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_0

    .line 150
    # getter for: Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/LogSender;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LogSender is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;->mLogSender:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/server/LogSender;

    .line 156
    .local v0, "logSender":Lcom/samsung/android/providers/context/server/LogSender;
    if-nez v0, :cond_1

    .line 157
    # getter for: Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/LogSender;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LogSender is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 161
    :cond_1
    # getter for: Lcom/samsung/android/providers/context/server/LogSender;->mRstMsgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/LogSender;->access$100(Lcom/samsung/android/providers/context/server/LogSender;)Landroid/os/Handler;

    move-result-object v1

    if-nez v1, :cond_2

    .line 162
    # getter for: Lcom/samsung/android/providers/context/server/LogSender;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/LogSender;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Result Message Handler is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 166
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 168
    :pswitch_0
    # getter for: Lcom/samsung/android/providers/context/server/LogSender;->mRstMsgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/LogSender;->access$100(Lcom/samsung/android/providers/context/server/LogSender;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 169
    # invokes: Lcom/samsung/android/providers/context/server/LogSender;->sendAll()V
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/LogSender;->access$200(Lcom/samsung/android/providers/context/server/LogSender;)V

    goto :goto_0

    .line 173
    :pswitch_1
    # getter for: Lcom/samsung/android/providers/context/server/LogSender;->mRstMsgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/LogSender;->access$100(Lcom/samsung/android/providers/context/server/LogSender;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
