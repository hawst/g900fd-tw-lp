.class public Lcom/samsung/android/providers/context/log/ExchangeCallLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "ExchangeCallLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 38
    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "app_id"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string v3, "uri"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "contact_address"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "call_type"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "start_time"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "stop_time"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "starttime"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "stoptime"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "duration"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/ExchangeCallLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 46
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "exchange_call"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_EXCHANGE_CALL:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/ExchangeCallLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 50
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    .line 52
    new-instance v1, Lcom/samsung/android/providers/context/log/ExchangeCallLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/ExchangeCallLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/ExchangeCallLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 99
    sget-object v1, Lcom/samsung/android/providers/context/log/ExchangeCallLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 100
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/samsung/android/providers/context/log/ExchangeCallLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 182
    return-void
.end method


# virtual methods
.method protected Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 12
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v10, 0x0

    .line 104
    const-string v0, "custom_id"

    .line 106
    .local v0, "deprecatedKey":Ljava/lang/String;
    const-string v7, "custom_id"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 107
    const-string v7, "ContextFramework"

    const-string v8, "FITERLOG : EXCHANGECALL:: Have to remove = custom_id"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const-string v7, "ContextFramework"

    const-string v8, "FITERLOG : EXCHANGECALL:: patch to URI = custom_id"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const-string v7, "uri"

    const-string v8, "custom_id"

    invoke-virtual {p1, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_0
    const-string v7, "start_time"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 114
    const-string v7, "starttime"

    const-string v8, "start_time"

    invoke-virtual {p1, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/providers/context/util/ContextLogger;->convertStringToTime(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 129
    :cond_1
    :goto_0
    const-string v7, "stop_time"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 130
    const-string v7, "stoptime"

    const-string v8, "stop_time"

    invoke-virtual {p1, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/providers/context/util/ContextLogger;->convertStringToTime(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 145
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;->Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v7

    return-object v7

    .line 115
    :cond_3
    const-string v7, "starttime"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 118
    const-string v7, "starttime"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 119
    .local v1, "startTime":Ljava/lang/Long;
    if-eqz v1, :cond_4

    .line 120
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v2, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 125
    .local v2, "startTimeDate":Ljava/util/Date;
    :goto_2
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 126
    .local v3, "startTimeString":Ljava/lang/String;
    const-string v7, "start_time"

    invoke-virtual {p1, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 122
    .end local v2    # "startTimeDate":Ljava/util/Date;
    .end local v3    # "startTimeString":Ljava/lang/String;
    :cond_4
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v10, v11}, Ljava/util/Date;-><init>(J)V

    .restart local v2    # "startTimeDate":Ljava/util/Date;
    goto :goto_2

    .line 131
    .end local v1    # "startTime":Ljava/lang/Long;
    .end local v2    # "startTimeDate":Ljava/util/Date;
    :cond_5
    const-string v7, "stoptime"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 134
    const-string v7, "stoptime"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    .line 135
    .local v4, "stopTime":Ljava/lang/Long;
    if-eqz v4, :cond_6

    .line 136
    new-instance v5, Ljava/util/Date;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v5, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 141
    .local v5, "stopTimeDate":Ljava/util/Date;
    :goto_3
    new-instance v6, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 142
    .local v6, "stopTimeString":Ljava/lang/String;
    const-string v7, "stop_time"

    invoke-virtual {p1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 138
    .end local v5    # "stopTimeDate":Ljava/util/Date;
    .end local v6    # "stopTimeString":Ljava/lang/String;
    :cond_6
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v10, v11}, Ljava/util/Date;-><init>(J)V

    .restart local v5    # "stopTimeDate":Ljava/util/Date;
    goto :goto_3
.end method

.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 150
    const/4 v0, 0x0

    .line 151
    .local v0, "app_id":Ljava/lang/String;
    const/4 v3, 0x0

    .line 152
    .local v3, "type":I
    const/4 v1, 0x0

    .line 155
    .local v1, "call_type":I
    :try_start_0
    const-string v4, "app_id"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    const-string v4, "type"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 157
    const-string v4, "call_type"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 163
    :goto_0
    if-eqz v0, :cond_0

    const-string v4, ""

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 164
    :cond_0
    const-string v4, "ContextFramework"

    const-string v5, "ExchangeCall : app_id is invalid"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_1
    if-lez v3, :cond_2

    const/4 v4, 0x6

    if-le v3, v4, :cond_3

    .line 167
    :cond_2
    const-string v4, "ContextFramework"

    const-string v5, "ExchangeCall : type is invalid"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :cond_3
    if-lez v1, :cond_4

    const/4 v4, 0x3

    if-le v1, v4, :cond_5

    .line 170
    :cond_4
    const-string v4, "ContextFramework"

    const-string v5, "ExchangeCall : call_type is invalid"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_5
    const/4 v4, 0x1

    return v4

    .line 158
    :catch_0
    move-exception v2

    .line 159
    .local v2, "ex":Ljava/lang/NullPointerException;
    const-string v4, "ContextFramework"

    const-string v5, "ExchangeCall : ValidBoundaryCheck - NullPointerException."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
