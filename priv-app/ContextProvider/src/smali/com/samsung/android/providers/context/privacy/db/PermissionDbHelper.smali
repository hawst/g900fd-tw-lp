.class public Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PermissionDbHelper.java"


# static fields
.field private static mDbName:Ljava/lang/String;

.field private static sDatabaseHandlerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDBFileDeleted:Z

.field private mDbFilesObserver:Landroid/os/FileObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "PrivacyLog.db"

    sput-object v0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDbName:Ljava/lang/String;

    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->sDatabaseHandlerList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    sget-object v0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDbName:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDBFileDeleted:Z

    .line 71
    const-string v0, "ContextFramework"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PermissionDatabase: using "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDbName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->startWatchDBFilesObserver(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDBFileDeleted:Z

    return p1
.end method

.method public static declared-synchronized addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V
    .locals 2
    .param p0, "handler"    # Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .prologue
    .line 54
    const-class v1, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->sDatabaseHandlerList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->sDatabaseHandlerList:Ljava/util/ArrayList;

    .line 58
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->sDatabaseHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    monitor-exit v1

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private startWatchDBFilesObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDbFilesObserver:Landroid/os/FileObserver;

    if-eqz v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDbFilesObserver:Landroid/os/FileObserver;

    invoke-virtual {v2}, Landroid/os/FileObserver;->startWatching()V

    .line 147
    :goto_0
    return-void

    .line 124
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDBFileDeleted:Z

    .line 126
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 127
    .local v0, "fileDir":Ljava/io/File;
    if-nez v0, :cond_1

    .line 128
    const-string v2, "ContextFramework"

    const-string v3, "LogDatabase: file directory not existing"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 132
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, "watchPath":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper$1;

    const/16 v3, 0x200

    invoke-direct {v2, p0, v1, v3}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper$1;-><init>(Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDbFilesObserver:Landroid/os/FileObserver;

    .line 144
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDbFilesObserver:Landroid/os/FileObserver;

    invoke-virtual {v2}, Landroid/os/FileObserver;->startWatching()V

    .line 146
    const-string v2, "ContextFramework"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LogDatabase: watching "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public isDeleted()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->mDBFileDeleted:Z

    return v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 77
    sget-object v3, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->sDatabaseHandlerList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 78
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 81
    :try_start_0
    sget-object v3, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->sDatabaseHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 82
    .local v1, "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    invoke-virtual {v1, p1}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 84
    .end local v1    # "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v3, :cond_0

    .line 86
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error executing SQL "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    :goto_1
    return-void

    .line 91
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 92
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 98
    sget-object v3, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->sDatabaseHandlerList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 99
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 102
    :try_start_0
    sget-object v3, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->sDatabaseHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 103
    .local v1, "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    invoke-virtual {v1, p1, p2, p3}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->upgradeTable(Landroid/database/sqlite/SQLiteDatabase;II)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    .end local v1    # "handler":Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v3, :cond_0

    .line 107
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error executing SQL "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    :goto_1
    return-void

    .line 112
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 113
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1
.end method
