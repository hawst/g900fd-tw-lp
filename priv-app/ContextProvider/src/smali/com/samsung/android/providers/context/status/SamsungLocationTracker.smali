.class public Lcom/samsung/android/providers/context/status/SamsungLocationTracker;
.super Lcom/samsung/android/providers/context/status/LocationTracker;
.source "SamsungLocationTracker.java"

# interfaces
.implements Lcom/samsung/location/SLocationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;,
        Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;
    }
.end annotation


# instance fields
.field private final mAirPressureMonitor:Lcom/samsung/android/providers/context/status/AirPressureMonitor;

.field private final mContext:Landroid/content/Context;

.field private mLastBatchingLocationTimeStamp:J

.field private mLatestLocation:Lcom/samsung/android/providers/context/status/LocationInfo;

.field private mLocationManager:Landroid/location/LocationManager;

.field private final mNetworkListener:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

.field private final mPassiveListener:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

.field private mPendingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/samsung/android/providers/context/status/LocationInfo;",
            "Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mReadyReceiver:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;

.field private mRequestId:I

.field private mSLocationManager:Lcom/samsung/location/SLocationManager;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/LocationTracker;-><init>()V

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLastBatchingLocationTimeStamp:J

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mRequestId:I

    .line 74
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mContext:Landroid/content/Context;

    .line 75
    new-instance v0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

    const-string v1, "Passive"

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;-><init>(Lcom/samsung/android/providers/context/status/SamsungLocationTracker;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPassiveListener:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

    .line 76
    new-instance v0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

    const-string v1, "Network"

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;-><init>(Lcom/samsung/android/providers/context/status/SamsungLocationTracker;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mNetworkListener:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

    .line 77
    new-instance v0, Lcom/samsung/android/providers/context/status/AirPressureMonitor;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/AirPressureMonitor;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mAirPressureMonitor:Lcom/samsung/android/providers/context/status/AirPressureMonitor;

    .line 78
    new-instance v0, Lcom/samsung/android/providers/context/status/LocationInfo;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/LocationInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLatestLocation:Lcom/samsung/android/providers/context/status/LocationInfo;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPendingList:Ljava/util/ArrayList;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/providers/context/status/SamsungLocationTracker;Landroid/location/Location;)Lcom/samsung/android/providers/context/status/LocationInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/SamsungLocationTracker;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->convertLocationInfo(Landroid/location/Location;)Lcom/samsung/android/providers/context/status/LocationInfo;

    move-result-object v0

    return-object v0
.end method

.method private convertLocationInfo(Landroid/location/Location;)Lcom/samsung/android/providers/context/status/LocationInfo;
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 297
    new-instance v0, Lcom/samsung/android/providers/context/status/LocationInfo;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/LocationInfo;-><init>()V

    .line 298
    .local v0, "locationInfo":Lcom/samsung/android/providers/context/status/LocationInfo;
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/providers/context/status/LocationInfo;->setTimestamp(J)V

    .line 299
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/providers/context/status/LocationInfo;->setLongitude(D)V

    .line 300
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/providers/context/status/LocationInfo;->setLatitude(D)V

    .line 301
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/status/LocationInfo;->setAccuracy(F)V

    .line 303
    return-object v0
.end method

.method private saveLocationLog(Lcom/samsung/android/providers/context/status/LocationInfo;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;)V
    .locals 12
    .param p1, "locationInfo"    # Lcom/samsung/android/providers/context/status/LocationInfo;
    .param p2, "placeInfo"    # Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    .prologue
    .line 260
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/LocationInfo;->getTimestamp()J

    move-result-wide v6

    iget-object v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLatestLocation:Lcom/samsung/android/providers/context/status/LocationInfo;

    invoke-virtual {v8}, Lcom/samsung/android/providers/context/status/LocationInfo;->getTimestamp()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/32 v8, 0xea60

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 262
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLatestLocation:Lcom/samsung/android/providers/context/status/LocationInfo;

    .line 265
    iget-object v6, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 266
    .local v1, "cr":Landroid/content/ContentResolver;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 268
    .local v5, "row":Landroid/content/ContentValues;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v2

    .line 270
    .local v2, "dateFormat":Ljava/text/DateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 271
    .local v4, "logTime":Ljava/util/Date;
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/LocationInfo;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/Date;->setTime(J)V

    .line 274
    iget-object v6, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mAirPressureMonitor:Lcom/samsung/android/providers/context/status/AirPressureMonitor;

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/LocationInfo;->getTimestamp()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->getLastestAirPressure(J)F

    move-result v0

    .line 276
    .local v0, "airPressure":F
    const-string v6, "timestamp_utc"

    invoke-virtual {v2, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v6, "timestamp"

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 278
    const-string v6, "time_zone"

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/providers/context/GlobalStatus;->timeZone:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v6, "longitude"

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/LocationInfo;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 280
    const-string v6, "latitude"

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/LocationInfo;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 281
    const-string v6, "place_name"

    invoke-virtual {p2}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v6, "place_category"

    invoke-virtual {p2}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->getCategory()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 283
    const-string v6, "air_pressure"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 284
    const-string v6, "accuracy"

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/LocationInfo;->getAccuracy()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 287
    :try_start_0
    sget-object v6, Lcom/samsung/android/providers/context/log/ContextLogContract$MoveLocation;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    .end local v0    # "airPressure":F
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    .end local v4    # "logTime":Ljava/util/Date;
    .end local v5    # "row":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 288
    .restart local v0    # "airPressure":F
    .restart local v1    # "cr":Landroid/content/ContentResolver;
    .restart local v2    # "dateFormat":Ljava/text/DateFormat;
    .restart local v4    # "logTime":Ljava/util/Date;
    .restart local v5    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v3

    .line 289
    .local v3, "ex":Ljava/lang/Exception;
    const-string v6, "ContextFramework.SamsungLocationTracker"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 292
    .end local v0    # "airPressure":F
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    .end local v3    # "ex":Ljava/lang/Exception;
    .end local v4    # "logTime":Ljava/util/Date;
    .end local v5    # "row":Landroid/content/ContentValues;
    :cond_0
    const-string v6, "ContextFramework.SamsungLocationTracker"

    const-string v7, "Location logging is canceled to avoid redundancy"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized disable()V
    .locals 2

    .prologue
    .line 141
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    iget v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mRequestId:I

    invoke-virtual {v0, v1, p0}, Lcom/samsung/location/SLocationManager;->stopBatching(ILcom/samsung/location/SLocationListener;)I

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    .line 144
    const-string v0, "ContextFramework.SamsungLocationTracker"

    const-string v1, "GPS batching is stopped"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPassiveListener:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 149
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mNetworkListener:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    .line 151
    const-string v0, "ContextFramework.SamsungLocationTracker"

    const-string v1, "Location update is stopped"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_3

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mAirPressureMonitor:Lcom/samsung/android/providers/context/status/AirPressureMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->disable()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    :cond_3
    monitor-exit p0

    return-void

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized enable()V
    .locals 7

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_5

    .line 88
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mContext:Landroid/content/Context;

    const-string v1, "sec_location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/SLocationManager;

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    .line 91
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p0}, Lcom/samsung/location/SLocationManager;->startBatching(ILcom/samsung/location/SLocationListener;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mRequestId:I

    .line 92
    iget v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mRequestId:I

    if-ltz v0, :cond_4

    .line 93
    const-string v0, "ContextFramework.SamsungLocationTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting GPS batching (SUCCESS), ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_1

    .line 112
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_6

    .line 114
    :try_start_3
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mContext:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    .line 117
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "passive"

    const-wide/32 v2, 0x1d4c0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPassiveListener:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 119
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/32 v2, 0x6ddd00

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mNetworkListener:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationManagerListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 122
    const-string v0, "ContextFramework.SamsungLocationTracker"

    const-string v1, "Starting LocationManager (passive & network) (SUCCESS)"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 134
    :cond_1
    :goto_1
    :try_start_4
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_3

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mAirPressureMonitor:Lcom/samsung/android/providers/context/status/AirPressureMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->enable()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 137
    :cond_3
    monitor-exit p0

    return-void

    .line 95
    :cond_4
    :try_start_5
    const-string v0, "ContextFramework.SamsungLocationTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting GPS batching (FAIL), Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v6

    .line 100
    .local v6, "e":Ljava/lang/NullPointerException;
    :try_start_6
    invoke-virtual {v6}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 101
    const-string v0, "ContextFramework.SamsungLocationTracker"

    const-string v1, "SLocation service is not ready. Wait..."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;-><init>(Lcom/samsung/android/providers/context/status/SamsungLocationTracker;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mReadyReceiver:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;

    .line 104
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mReadyReceiver:Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker$LocationReadyBroadcastReceiver;->register()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 85
    .end local v6    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 107
    :cond_5
    :try_start_7
    const-string v0, "ContextFramework.SamsungLocationTracker"

    const-string v1, "SLocation is not supported"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 124
    :catch_1
    move-exception v6

    .line 125
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 126
    const-string v0, "ContextFramework.SamsungLocationTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Location update request fails: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 129
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :cond_6
    const-string v0, "ContextFramework.SamsungLocationTracker"

    const-string v1, "LocationManager is not supported"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1
.end method

.method public getLatestLocation()Lcom/samsung/android/providers/context/status/LocationInfo;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLatestLocation:Lcom/samsung/android/providers/context/status/LocationInfo;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/LocationInfo;->clone()Lcom/samsung/android/providers/context/status/LocationInfo;

    move-result-object v0

    return-object v0
.end method

.method public insertRealtimeLocation(Lcom/samsung/android/providers/context/status/LocationInfo;)V
    .locals 3
    .param p1, "location"    # Lcom/samsung/android/providers/context/status/LocationInfo;

    .prologue
    .line 226
    invoke-static {}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getCurrentMyPlaceInfo()Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    move-result-object v0

    .line 228
    .local v0, "place":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mAirPressureMonitor:Lcom/samsung/android/providers/context/status/AirPressureMonitor;

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/AirPressureMonitor;->flush()Z

    .line 231
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    if-eqz v1, :cond_0

    .line 232
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPendingList:Ljava/util/ArrayList;

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mSLocationManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v1}, Lcom/samsung/location/SLocationManager;->requestBatchOfLocations()I

    .line 238
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->saveLocationLog(Lcom/samsung/android/providers/context/status/LocationInfo;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;)V

    goto :goto_0
.end method

.method public onLocationAvailable([Landroid/location/Location;)V
    .locals 12
    .param p1, "batchingList"    # [Landroid/location/Location;

    .prologue
    .line 169
    const/4 v3, 0x0

    .line 170
    .local v3, "i":I
    const/4 v4, 0x0

    .line 171
    .local v4, "j":I
    const/4 v2, 0x0

    .line 172
    .local v2, "batchingSize":I
    iget-object v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPendingList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 174
    .local v7, "pendingSize":I
    if-eqz p1, :cond_0

    .line 175
    array-length v2, p1

    .line 179
    :cond_0
    :goto_0
    if-ge v3, v2, :cond_1

    .line 180
    iget-wide v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLastBatchingLocationTimeStamp:J

    aget-object v10, p1, v3

    invoke-virtual {v10}, Landroid/location/Location;->getTime()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-gez v8, :cond_3

    .line 182
    add-int/lit8 v8, v2, -0x1

    aget-object v8, p1, v8

    invoke-virtual {v8}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mLastBatchingLocationTimeStamp:J

    .line 188
    :cond_1
    const-string v8, "ContextFramework.SamsungLocationTracker"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " GPS batching logs and "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " pending logs"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :goto_1
    if-lt v3, v2, :cond_2

    if-ge v4, v7, :cond_7

    .line 195
    :cond_2
    if-ne v3, v2, :cond_4

    .line 196
    iget-object v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPendingList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/util/Pair;

    iget-object v5, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/samsung/android/providers/context/status/LocationInfo;

    .line 197
    .local v5, "pendingLocation":Lcom/samsung/android/providers/context/status/LocationInfo;
    iget-object v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPendingList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/util/Pair;

    iget-object v6, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    .line 198
    .local v6, "pendingPlace":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    invoke-direct {p0, v5, v6}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->saveLocationLog(Lcom/samsung/android/providers/context/status/LocationInfo;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;)V

    .line 199
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 185
    .end local v5    # "pendingLocation":Lcom/samsung/android/providers/context/status/LocationInfo;
    .end local v6    # "pendingPlace":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 200
    :cond_4
    if-ne v4, v7, :cond_5

    .line 201
    aget-object v1, p1, v3

    .line 202
    .local v1, "batchingLocation":Landroid/location/Location;
    invoke-direct {p0, v1}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->convertLocationInfo(Landroid/location/Location;)Lcom/samsung/android/providers/context/status/LocationInfo;

    move-result-object v0

    .line 203
    .local v0, "batchingInfo":Lcom/samsung/android/providers/context/status/LocationInfo;
    sget-object v8, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->UNKNOWN_PLACE:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    invoke-direct {p0, v0, v8}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->saveLocationLog(Lcom/samsung/android/providers/context/status/LocationInfo;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;)V

    .line 204
    add-int/lit8 v3, v3, 0x1

    .line 205
    goto :goto_1

    .line 206
    .end local v0    # "batchingInfo":Lcom/samsung/android/providers/context/status/LocationInfo;
    .end local v1    # "batchingLocation":Landroid/location/Location;
    :cond_5
    aget-object v1, p1, v3

    .line 207
    .restart local v1    # "batchingLocation":Landroid/location/Location;
    iget-object v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPendingList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/util/Pair;

    iget-object v5, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/samsung/android/providers/context/status/LocationInfo;

    .line 209
    .restart local v5    # "pendingLocation":Lcom/samsung/android/providers/context/status/LocationInfo;
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/status/LocationInfo;->getTimestamp()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-gtz v8, :cond_6

    .line 210
    invoke-direct {p0, v1}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->convertLocationInfo(Landroid/location/Location;)Lcom/samsung/android/providers/context/status/LocationInfo;

    move-result-object v0

    .line 211
    .restart local v0    # "batchingInfo":Lcom/samsung/android/providers/context/status/LocationInfo;
    sget-object v8, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->UNKNOWN_PLACE:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    invoke-direct {p0, v0, v8}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->saveLocationLog(Lcom/samsung/android/providers/context/status/LocationInfo;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;)V

    .line 212
    add-int/lit8 v3, v3, 0x1

    .line 213
    goto :goto_1

    .line 214
    .end local v0    # "batchingInfo":Lcom/samsung/android/providers/context/status/LocationInfo;
    :cond_6
    iget-object v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPendingList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/util/Pair;

    iget-object v6, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    .line 215
    .restart local v6    # "pendingPlace":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    invoke-direct {p0, v5, v6}, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->saveLocationLog(Lcom/samsung/android/providers/context/status/LocationInfo;Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;)V

    .line 216
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 221
    .end local v1    # "batchingLocation":Landroid/location/Location;
    .end local v5    # "pendingLocation":Lcom/samsung/android/providers/context/status/LocationInfo;
    .end local v6    # "pendingPlace":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    :cond_7
    iget-object v8, p0, Lcom/samsung/android/providers/context/status/SamsungLocationTracker;->mPendingList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 222
    return-void
.end method
