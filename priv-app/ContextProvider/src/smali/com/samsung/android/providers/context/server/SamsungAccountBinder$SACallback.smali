.class public Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;
.super Lcom/msc/sa/aidl/ISACallback$Stub;
.source "SamsungAccountBinder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SACallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;


# direct methods
.method public constructor <init>(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    invoke-direct {p0}, Lcom/msc/sa/aidl/ISACallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveAccessToken(IZLandroid/os/Bundle;)V
    .locals 11
    .param p1, "requestID"    # I
    .param p2, "isSuccessAT"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 182
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/ContextApplication;->getSamsungAccountInfo()Lcom/samsung/android/providers/context/server/SamsungAccountInfo;

    move-result-object v0

    .line 185
    .local v0, "accountInfo":Lcom/samsung/android/providers/context/server/SamsungAccountInfo;
    if-eqz p3, :cond_2

    .line 186
    if-eqz p2, :cond_1

    .line 187
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Success receive AccessToken"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    invoke-virtual {v0, v10}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setInitedSA(I)V

    .line 191
    const-string v7, "user_id"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 192
    .local v6, "userId":Ljava/lang/String;
    const-string v7, "login_id"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 194
    .local v3, "logInId":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceiveAccessToken requestID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isSuccessAccessToken : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "AccessToken : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "access_token"

    invoke-virtual {p3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "User id : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Login id : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 197
    .local v5, "result":Ljava/lang/String;
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/ContextApplication;->getStorage()Lcom/samsung/android/providers/context/ContextPreference;

    move-result-object v4

    .line 200
    .local v4, "pref":Lcom/samsung/android/providers/context/ContextPreference;
    const-string v7, "login_id"

    invoke-virtual {v4, v7, v3}, Lcom/samsung/android/providers/context/ContextPreference;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v7, "user_id"

    invoke-virtual {v4, v7, v6}, Lcom/samsung/android/providers/context/ContextPreference;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {v0, v10}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setIsUserId(Z)V

    .line 203
    invoke-virtual {v0, v6}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setUserId(Ljava/lang/String;)V

    .line 229
    .end local v3    # "logInId":Ljava/lang/String;
    .end local v4    # "pref":Lcom/samsung/android/providers/context/ContextPreference;
    .end local v5    # "result":Ljava/lang/String;
    .end local v6    # "userId":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->onDestroy()V

    .line 230
    :goto_1
    return-void

    .line 205
    :cond_1
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Failed receive AccessToken"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {v0, v9}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setInitedSA(I)V

    .line 209
    const-string v7, "error_code"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 210
    .local v1, "errorCode":Ljava/lang/String;
    const-string v7, "error_message"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 212
    .local v2, "errorMessage":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceiveAccessToken requestID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isSuccessAccessToken : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "error_code : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "error_message : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 214
    .restart local v5    # "result":Ljava/lang/String;
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-virtual {v0, v9}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setIsUserId(Z)V

    .line 217
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lcom/samsung/android/providers/context/server/SamsungAccountInfo;->setUserId(Ljava/lang/String;)V

    .line 218
    iget-object v7, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    # operator++ for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestCount:I
    invoke-static {v7}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$508(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)I

    .line 219
    iget-object v7, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestCount:I
    invoke-static {v7}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$500(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)I

    move-result v7

    const/4 v8, 0x3

    if-ge v7, v8, :cond_0

    .line 220
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Try again request AccessToken"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-object v7, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    # invokes: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestAccessToken()V
    invoke-static {v7}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$400(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)V

    goto/16 :goto_1

    .line 226
    .end local v1    # "errorCode":Ljava/lang/String;
    .end local v2    # "errorMessage":Ljava/lang/String;
    .end local v5    # "result":Ljava/lang/String;
    :cond_2
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ResultData is null"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onReceiveAuthCode(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 245
    return-void
.end method

.method public onReceiveChecklistValidation(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 235
    return-void
.end method

.method public onReceiveDisclaimerAgreement(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 240
    return-void
.end method

.method public onReceivePasswordConfirmation(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 255
    return-void
.end method

.method public onReceiveSCloudAccessToken(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 250
    return-void
.end method
