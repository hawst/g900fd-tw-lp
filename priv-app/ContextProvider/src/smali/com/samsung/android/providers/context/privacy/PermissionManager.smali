.class public Lcom/samsung/android/providers/context/privacy/PermissionManager;
.super Ljava/lang/Object;
.source "PermissionManager.java"


# static fields
.field private static isInit:Z

.field private static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/providers/context/privacy/PermissionManager;->isInit:Z

    .line 40
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PermissionManager$1;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/android/providers/context/privacy/PermissionManager$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PermissionManager;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 76
    sget-object v0, Lcom/samsung/android/providers/context/privacy/PermissionManager;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v0}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 77
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v0, :cond_0

    .line 78
    const-string v0, "ContextFramework.PermissionManager"

    const-string v1, "DatabaseHandler is initialized"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/samsung/android/providers/context/privacy/PermissionManager;->mContext:Landroid/content/Context;

    .line 84
    return-void
.end method

.method private static disableDependentFeatures(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)V
    .locals 4
    .param p0, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .prologue
    .line 345
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v0

    .line 348
    .local v0, "accessControl":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-ne p0, v2, :cond_0

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 351
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 352
    .local v1, "msg":Landroid/os/Message;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 356
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-ne p0, v2, :cond_1

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 359
    invoke-static {}, Lcom/samsung/android/providers/context/status/AreaTracker;->getInstance()Lcom/samsung/android/providers/context/status/AreaTracker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/status/AreaTracker;->disable()V

    .line 363
    :cond_1
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-ne p0, v2, :cond_2

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 366
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 367
    .restart local v1    # "msg":Landroid/os/Message;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 371
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-eq p0, v2, :cond_3

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-ne p0, v2, :cond_4

    :cond_3
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 375
    invoke-static {}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->disable()V

    .line 378
    :cond_4
    return-void
.end method

.method private static enableDependentFeatures(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)V
    .locals 4
    .param p0, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .prologue
    .line 308
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v0

    .line 311
    .local v0, "accessControl":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-ne p0, v2, :cond_0

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 314
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 315
    .local v1, "msg":Landroid/os/Message;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 319
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-ne p0, v2, :cond_1

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 322
    invoke-static {}, Lcom/samsung/android/providers/context/status/AreaTracker;->getInstance()Lcom/samsung/android/providers/context/status/AreaTracker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/status/AreaTracker;->enable()V

    .line 326
    :cond_1
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-ne p0, v2, :cond_2

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 329
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 330
    .restart local v1    # "msg":Landroid/os/Message;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 334
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-eq p0, v2, :cond_3

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    if-ne p0, v2, :cond_4

    :cond_3
    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 338
    invoke-static {}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->enable()V

    .line 341
    :cond_4
    return-void
.end method

.method public static finalizeUninstalledAppConsent(Ljava/lang/String;)V
    .locals 6
    .param p0, "appId"    # Ljava/lang/String;

    .prologue
    .line 291
    sget-boolean v4, Lcom/samsung/android/providers/context/privacy/PermissionManager;->isInit:Z

    if-nez v4, :cond_1

    .line 292
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v4

    iget-boolean v4, v4, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v4, :cond_0

    .line 293
    const-string v4, "ContextFramework.PermissionManager"

    const-string v5, "PermissionManager is not initialized"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_0
    return-void

    .line 298
    :cond_1
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v0

    .line 299
    .local v0, "accessControl":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    invoke-virtual {v0, p0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getPermittedTypeList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 301
    .local v3, "permittedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 302
    .local v2, "item":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    invoke-virtual {v2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v4

    invoke-static {p0, v4}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->unsetPrivacyItem(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static getLastItem(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Landroid/os/Bundle;
    .locals 15
    .param p0, "privacyItem"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .prologue
    .line 444
    const/4 v13, 0x0

    .line 446
    .local v13, "returnData":Landroid/os/Bundle;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 448
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 449
    const-string v1, "ContextFramework.PermissionManager"

    const-string v2, "Privacy DB is not created"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    const/4 v1, 0x0

    .line 494
    :goto_0
    return-object v1

    .line 454
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "permission_type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 457
    .local v3, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v1, :cond_1

    .line 458
    const-string v1, "ContextFramework.PermissionManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Selection = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    :cond_1
    :try_start_0
    const-string v1, "privacy"

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 463
    .local v10, "cr":Landroid/database/Cursor;
    if-nez v10, :cond_2

    .line 464
    const-string v1, "ContextFramework.PermissionManager"

    const-string v2, "Cursor is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    const/4 v1, 0x0

    goto :goto_0

    .line 468
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_3

    .line 469
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 470
    const-string v1, "ContextFramework.PermissionManager"

    const-string v2, "Cursor is Empty"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    const/4 v1, 0x0

    goto :goto_0

    .line 474
    :cond_3
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v12

    .line 475
    .local v12, "privacyAV":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    if-nez v12, :cond_4

    .line 476
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 477
    const/4 v1, 0x0

    goto :goto_0

    .line 480
    :cond_4
    invoke-interface {v10}, Landroid/database/Cursor;->moveToLast()Z

    .line 482
    new-instance v14, Landroid/os/Bundle;

    invoke-direct {v14}, Landroid/os/Bundle;-><init>()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 483
    .end local v13    # "returnData":Landroid/os/Bundle;
    .local v14, "returnData":Landroid/os/Bundle;
    :try_start_1
    const-string v1, "app_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 484
    .local v9, "appID":Ljava/lang/String;
    const-string v1, "ONOFF"

    invoke-virtual {v12, v9, p0}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentNow(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    invoke-virtual {v14, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 485
    const-string v1, "APPID"

    invoke-virtual {v14, v1, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v13, v14

    .end local v9    # "appID":Ljava/lang/String;
    .end local v10    # "cr":Landroid/database/Cursor;
    .end local v12    # "privacyAV":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    .end local v14    # "returnData":Landroid/os/Bundle;
    .restart local v13    # "returnData":Landroid/os/Bundle;
    :goto_1
    move-object v1, v13

    .line 494
    goto/16 :goto_0

    .line 488
    :catch_0
    move-exception v11

    .line 489
    .local v11, "ex":Landroid/database/sqlite/SQLiteException;
    :goto_2
    const-string v1, "ContextFramework.PermissionManager"

    const-string v2, "no databases"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 490
    .end local v11    # "ex":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v11

    .line 491
    .local v11, "ex":Ljava/lang/IllegalStateException;
    :goto_3
    const-string v1, "ContextFramework.PermissionManager"

    const-string v2, "illegal state exception"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 490
    .end local v11    # "ex":Ljava/lang/IllegalStateException;
    .end local v13    # "returnData":Landroid/os/Bundle;
    .restart local v10    # "cr":Landroid/database/Cursor;
    .restart local v12    # "privacyAV":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    .restart local v14    # "returnData":Landroid/os/Bundle;
    :catch_2
    move-exception v11

    move-object v13, v14

    .end local v14    # "returnData":Landroid/os/Bundle;
    .restart local v13    # "returnData":Landroid/os/Bundle;
    goto :goto_3

    .line 488
    .end local v13    # "returnData":Landroid/os/Bundle;
    .restart local v14    # "returnData":Landroid/os/Bundle;
    :catch_3
    move-exception v11

    move-object v13, v14

    .end local v14    # "returnData":Landroid/os/Bundle;
    .restart local v13    # "returnData":Landroid/os/Bundle;
    goto :goto_2
.end method

.method private loadPrivacyConsentHistory()V
    .locals 19

    .prologue
    .line 382
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 384
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v2, :cond_0

    .line 385
    const-string v10, "ContextFramework.PermissionManager"

    const-string v11, "Privacy DB is not created"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :goto_0
    return-void

    .line 389
    :cond_0
    const-string v3, "privacy"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 391
    .local v12, "cr":Landroid/database/Cursor;
    if-nez v12, :cond_1

    .line 392
    const-string v10, "ContextFramework.PermissionManager"

    const-string v11, "Cursor of Privacy DB query is null"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 396
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-nez v10, :cond_2

    .line 397
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 398
    const-string v10, "ContextFramework.PermissionManager"

    const-string v11, "Cursor of Privacy DB query is empty"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 402
    :cond_2
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v3

    .line 404
    .local v3, "accessControl":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 406
    const-string v10, "start_time"

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 407
    .local v17, "startTime":Ljava/lang/String;
    const-string v10, "end_time"

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 408
    .local v14, "endTime":Ljava/lang/String;
    const-string v10, "app_id"

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 409
    .local v4, "appId":Ljava/lang/String;
    const-string v10, "permission_type"

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 410
    .local v16, "permissionType":I
    const-string v10, "_id"

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v12, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 411
    .local v15, "id":I
    const-wide/16 v6, -0x1

    .line 412
    .local v6, "startSysTime":J
    const-wide/16 v8, -0x1

    .line 414
    .local v8, "endSysTime":J
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getPrivacyItem(I)Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    move-result-object v5

    .line 417
    .local v5, "privacyItem":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v10

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 418
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_3

    .line 419
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v10

    invoke-virtual {v10, v14}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 425
    :cond_3
    :goto_2
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v10

    iget-boolean v10, v10, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v10, :cond_4

    .line 426
    const-string v10, "ContextFramework.PermissionManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Privacy: time ("

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v18, ", "

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v18, "), type "

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v18, ", "

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    :cond_4
    const-wide/16 v10, -0x1

    cmp-long v10, v8, v10

    if-nez v10, :cond_5

    .line 432
    invoke-static {v5}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->enableDependentFeatures(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)V

    .line 435
    :cond_5
    int-to-long v10, v15

    invoke-virtual/range {v3 .. v11}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->addConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;JJJ)V

    goto/16 :goto_1

    .line 421
    :catch_0
    move-exception v13

    .line 422
    .local v13, "e":Ljava/text/ParseException;
    invoke-virtual {v13}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_2

    .line 438
    .end local v4    # "appId":Ljava/lang/String;
    .end local v5    # "privacyItem":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .end local v6    # "startSysTime":J
    .end local v8    # "endSysTime":J
    .end local v13    # "e":Ljava/text/ParseException;
    .end local v14    # "endTime":Ljava/lang/String;
    .end local v15    # "id":I
    .end local v16    # "permissionType":I
    .end local v17    # "startTime":Ljava/lang/String;
    :cond_6
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public static setPrivacyItem(Ljava/lang/String;I)V
    .locals 1
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "privacyItem"    # I

    .prologue
    .line 156
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getInitedPermissionManager()Lcom/samsung/android/providers/context/privacy/PermissionManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->setPrivacyItemInternal(Ljava/lang/String;I)V

    .line 157
    return-void
.end method

.method public static unsetPrivacyItem(Ljava/lang/String;I)V
    .locals 1
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "privacyItem"    # I

    .prologue
    .line 223
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getInitedPermissionManager()Lcom/samsung/android/providers/context/privacy/PermissionManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->unsetPrivacyItemInternal(Ljava/lang/String;I)V

    .line 224
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/samsung/android/providers/context/status/AreaTracker;->getInstance()Lcom/samsung/android/providers/context/status/AreaTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/AreaTracker;->disable()V

    .line 145
    invoke-static {}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->disable()V

    .line 147
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v0

    .line 148
    .local v0, "permissionDb":Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;
    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {v0}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->close()V

    .line 152
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/android/providers/context/privacy/PermissionManager;->isInit:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    monitor-exit p0

    return-void

    .line 144
    .end local v0    # "permissionDb":Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized init()V
    .locals 8

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    sget-boolean v6, Lcom/samsung/android/providers/context/privacy/PermissionManager;->isInit:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_1

    .line 140
    :cond_0
    monitor-exit p0

    return-void

    .line 93
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 94
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 97
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->loadPrivacyConsentHistory()V

    .line 99
    const/4 v6, 0x1

    sput-boolean v6, Lcom/samsung/android/providers/context/privacy/PermissionManager;->isInit:Z

    .line 101
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_3

    .line 102
    const-string v6, "ContextFramework.PermissionManager"

    const-string v7, "User privacy consents are loaded from Privacy DB"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_3
    const/4 v6, 0x1

    new-array v1, v6, [I

    const/4 v6, 0x0

    sget-object v7, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getValue()I

    move-result v7

    aput v7, v1, v6

    .line 135
    .local v1, "defaultEnabledPrivacyItemList":[I
    iget-object v6, p0, Lcom/samsung/android/providers/context/privacy/PermissionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 136
    .local v5, "thisAppId":Ljava/lang/String;
    move-object v0, v1

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget v3, v0, v2

    .line 137
    .local v3, "item":I
    invoke-virtual {p0, v5, v3}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->setPrivacyItemInternal(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 88
    .end local v0    # "arr$":[I
    .end local v1    # "defaultEnabledPrivacyItemList":[I
    .end local v2    # "i$":I
    .end local v3    # "item":I
    .end local v4    # "len$":I
    .end local v5    # "thisAppId":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public setPrivacyItemInternal(Ljava/lang/String;I)V
    .locals 13
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "privacyItem"    # I

    .prologue
    .line 160
    sget-boolean v2, Lcom/samsung/android/providers/context/privacy/PermissionManager;->isInit:Z

    if-nez v2, :cond_1

    .line 161
    const-string v2, "ContextFramework.PermissionManager"

    const-string v6, "PermissionManager not yet initialized"

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v1

    .line 166
    .local v1, "accessControl":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 169
    .local v4, "currentUtcTime":J
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v12

    .line 170
    .local v12, "timeFormatUtc":Ljava/text/DateFormat;
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "currentUtcTimeString":Ljava/lang/String;
    invoke-static {p2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getPrivacyItem(I)Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    move-result-object v3

    .line 173
    .local v3, "recvItem":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    if-nez v3, :cond_2

    .line 174
    const-string v2, "ContextFramework.PermissionManager"

    const-string v6, "Wrong Privacy Item"

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 178
    :cond_2
    invoke-virtual {v1, p1, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentNow(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 179
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 183
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 184
    .local v10, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v10, :cond_0

    .line 188
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 190
    .local v11, "rows":Landroid/content/ContentValues;
    const-string v2, "start_time"

    invoke-virtual {v11, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v2, "end_time"

    const-string v6, ""

    invoke-virtual {v11, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v2, "permission_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v11, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 193
    const-string v2, "app_id"

    invoke-virtual {v11, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v2, "privacy"

    const/4 v6, 0x0

    invoke-virtual {v10, v2, v6, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 198
    .local v8, "dbID":J
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_3

    .line 199
    const-string v2, "ContextFramework.PermissionManager"

    const-string v6, "Privacy Item is set (SUCCESS)"

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const-string v2, "ContextFramework.PermissionManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "       - Start Time = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const-string v2, "ContextFramework.PermissionManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "       - Privacy Item = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const-string v2, "ContextFramework.PermissionManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "       - App ID = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_3
    invoke-static {v3}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->enableDependentFeatures(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)V

    .line 209
    const-wide/16 v6, -0x1

    move-object v2, p1

    invoke-virtual/range {v1 .. v9}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->addConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;JJJ)V

    goto/16 :goto_0

    .line 213
    .end local v8    # "dbID":J
    .end local v10    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v11    # "rows":Landroid/content/ContentValues;
    :cond_4
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_0

    .line 214
    const-string v2, "ContextFramework.PermissionManager"

    const-string v6, "Privacy Item is set (FAIL) - Already Registered"

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const-string v2, "ContextFramework.PermissionManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "       - Privacy Item = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    const-string v2, "ContextFramework.PermissionManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "       - App ID = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public unsetPrivacyItemInternal(Ljava/lang/String;I)V
    .locals 13
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "privacyItem"    # I

    .prologue
    .line 228
    sget-boolean v10, Lcom/samsung/android/providers/context/privacy/PermissionManager;->isInit:Z

    if-nez v10, :cond_1

    .line 229
    const-string v10, "ContextFramework.PermissionManager"

    const-string v11, "PermissionManager not yet initialized"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v0

    .line 234
    .local v0, "accessControl":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 237
    .local v2, "currentUtcTime":J
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v9

    .line 238
    .local v9, "timeFormatUtc":Ljava/text/DateFormat;
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 240
    .local v1, "currentUtcTimeString":Ljava/lang/String;
    invoke-static {p2}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getPrivacyItem(I)Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    move-result-object v5

    .line 241
    .local v5, "recvItem":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    if-nez v5, :cond_2

    .line 242
    const-string v10, "ContextFramework.PermissionManager"

    const-string v11, "Wrong Privacy Item"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 246
    :cond_2
    invoke-virtual {v0, p1, v5}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentNow(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    .line 247
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 251
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/ContextApplication;->getPermissionDatabase()Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/providers/context/privacy/db/PermissionDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 252
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v4, :cond_0

    .line 257
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 259
    .local v8, "rows":Landroid/content/ContentValues;
    const-string v10, "end_time"

    invoke-virtual {v8, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-virtual {v0, p1, v5}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getLastConsentId(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)J

    move-result-wide v6

    .line 261
    .local v6, "lastItemID":J
    const-string v10, "privacy"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "_id = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v4, v10, v8, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 264
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v10

    iget-boolean v10, v10, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v10, :cond_3

    .line 265
    const-string v10, "ContextFramework.PermissionManager"

    const-string v11, "Privacy Item is unset (SUCCESS)"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    const-string v10, "ContextFramework.PermissionManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "       - Update Time = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const-string v10, "ContextFramework.PermissionManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "       - Update ID = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const-string v10, "ContextFramework.PermissionManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "       - Privacy Item = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const-string v10, "ContextFramework.PermissionManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "       - App ID = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_3
    invoke-virtual {v0, p1, v5, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->updateLastConsentTime(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;J)V

    .line 276
    invoke-static {v5}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->disableDependentFeatures(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)V

    goto/16 :goto_0

    .line 280
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "lastItemID":J
    .end local v8    # "rows":Landroid/content/ContentValues;
    :cond_4
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v10

    iget-boolean v10, v10, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v10, :cond_0

    .line 281
    const-string v10, "ContextFramework.PermissionManager"

    const-string v11, "Privacy Item is unset (FAIL) - Not Registered"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    const-string v10, "ContextFramework.PermissionManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "       - Privacy Item = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    const-string v10, "ContextFramework.PermissionManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "       - App ID = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
