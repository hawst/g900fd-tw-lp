.class abstract Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;
.super Ljava/lang/Object;
.source "DeviceStatusMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "InternalReceiver"
.end annotation


# instance fields
.field private mIsReceiverRegistered:Z

.field protected mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;->mIsReceiverRegistered:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$1;

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method protected registerReceiver(Landroid/content/Context;Landroid/content/IntentFilter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filter"    # Landroid/content/IntentFilter;

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;->mIsReceiverRegistered:Z

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0, p2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;->mIsReceiverRegistered:Z

    .line 211
    :cond_0
    return-void
.end method

.method protected unregisterReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;->mIsReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$InternalReceiver;->mIsReceiverRegistered:Z

    .line 218
    :cond_0
    return-void
.end method
