.class public Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "ChangeDeviceStatusLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

.field static final sStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "device_status"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "changed_status"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "change_device_status"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_CHANGE_DEVICE_STATUS:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider;->sStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    .line 48
    new-instance v0, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider$1;

    sget-object v1, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider;->sStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    invoke-direct {v0, v1}, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v0, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 87
    sget-object v0, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v0}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 88
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/samsung/android/providers/context/log/ChangeDeviceStatusLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 119
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 5
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 92
    const/4 v1, -0x1

    .line 93
    .local v1, "device_status":I
    const/4 v0, -0x1

    .line 96
    .local v0, "changed_status":I
    :try_start_0
    const-string v3, "device_status"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 97
    const-string v3, "changed_status"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 103
    :goto_0
    if-gez v1, :cond_0

    .line 104
    const-string v3, "ContextFramework"

    const-string v4, "ChangeDeviceStatus : device_status is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_0
    if-gtz v0, :cond_1

    .line 107
    const-string v3, "ContextFramework"

    const-string v4, "ChangeDeviceStatus : changed_status is invalid"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    const/4 v3, 0x1

    return v3

    .line 98
    :catch_0
    move-exception v2

    .line 99
    .local v2, "ex":Ljava/lang/NullPointerException;
    const-string v3, "ContextFramework"

    const-string v4, "ChangeDeviceStatus : ValidBoundaryCheck - NullPointerException."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
