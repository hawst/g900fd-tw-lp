.class public Lcom/samsung/android/providers/context/ContextService;
.super Landroid/app/Service;
.source "ContextService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/ContextService$ServiceHandler;
    }
.end annotation


# static fields
.field private static sVideoRecordingWatcher:Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;


# instance fields
.field private final mAppErrorReceiver:Landroid/content/BroadcastReceiver;

.field private mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

.field private mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

.field private final mHandler:Lcom/samsung/android/providers/context/ContextService$ServiceHandler;

.field private final mPkgReceiver:Landroid/content/BroadcastReceiver;

.field private mRunning:Z

.field private mScreenShotMonitor:Lcom/samsung/android/providers/context/status/ScreenShotMonitor;

.field private final mUserReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 63
    new-instance v0, Lcom/samsung/android/providers/context/ContextService$ServiceHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/ContextService$ServiceHandler;-><init>(Lcom/samsung/android/providers/context/ContextService;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mHandler:Lcom/samsung/android/providers/context/ContextService$ServiceHandler;

    .line 73
    new-instance v0, Lcom/samsung/android/providers/context/ContextService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/ContextService$1;-><init>(Lcom/samsung/android/providers/context/ContextService;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mAppErrorReceiver:Landroid/content/BroadcastReceiver;

    .line 93
    new-instance v0, Lcom/samsung/android/providers/context/ContextService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/ContextService$2;-><init>(Lcom/samsung/android/providers/context/ContextService;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    .line 117
    new-instance v0, Lcom/samsung/android/providers/context/ContextService$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/ContextService$3;-><init>(Lcom/samsung/android/providers/context/ContextService;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mPkgReceiver:Landroid/content/BroadcastReceiver;

    .line 235
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-static {p0}, Lcom/samsung/android/providers/context/ContextService;->logAppError(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/providers/context/ContextService;)Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/ContextService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 51
    invoke-static {p0, p1, p2}, Lcom/samsung/android/providers/context/ContextService;->logManageApp(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/providers/context/ContextService;)Lcom/samsung/android/providers/context/status/AppUsageMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/ContextService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    return-object v0
.end method

.method static declared-synchronized getVideoRecordingWatcher()Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;
    .locals 2

    .prologue
    .line 273
    const-class v1, Lcom/samsung/android/providers/context/ContextService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/ContextService;->sVideoRecordingWatcher:Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/ContextService;->sVideoRecordingWatcher:Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;

    .line 276
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/ContextService;->sVideoRecordingWatcher:Lcom/samsung/android/providers/context/status/VideoRecordingWatcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static logAppError(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 286
    const-string v6, "activity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 287
    .local v0, "act":Landroid/app/ActivityManager;
    if-nez v0, :cond_1

    .line 309
    :cond_0
    return-void

    .line 291
    :cond_1
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getProcessesInErrorState()Ljava/util/List;

    move-result-object v3

    .line 292
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$ProcessErrorStateInfo;>;"
    if-eqz v3, :cond_0

    .line 296
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 298
    .local v4, "row":Landroid/content/ContentValues;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$ProcessErrorStateInfo;

    .line 299
    .local v2, "info":Landroid/app/ActivityManager$ProcessErrorStateInfo;
    const-string v6, "ContextFramework"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "condition : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Landroid/app/ActivityManager$ProcessErrorStateInfo;->condition:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", processName : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Landroid/app/ActivityManager$ProcessErrorStateInfo;->processName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iget v6, v2, Landroid/app/ActivityManager$ProcessErrorStateInfo;->condition:I

    const/4 v7, 0x2

    if-le v6, v7, :cond_2

    const/4 v5, 0x0

    .line 302
    .local v5, "type":I
    :goto_1
    const-string v6, "type"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 303
    const-string v6, "app_id"

    iget-object v7, v2, Landroid/app/ActivityManager$ProcessErrorStateInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/providers/context/log/ContextLogContract$ReportAppErrorSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6, v7, v4}, Lcom/samsung/android/providers/context/util/ContextLogger;->logLocalProvider(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Z

    .line 307
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    goto :goto_0

    .line 301
    .end local v5    # "type":I
    :cond_2
    iget v5, v2, Landroid/app/ActivityManager$ProcessErrorStateInfo;->condition:I

    goto :goto_1
.end method

.method private static logManageApp(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 320
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 322
    .local v0, "row":Landroid/content/ContentValues;
    const-string v1, "type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 323
    const-string v1, "installed_app_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/providers/context/log/ContextLogContract$ManageApp;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v0}, Lcom/samsung/android/providers/context/util/ContextLogger;->logLocalProvider(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Z

    .line 327
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 330
    const-string v1, "type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 331
    const-string v1, "installed_app_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/providers/context/log/ContextLogContract$ManageAppSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v0}, Lcom/samsung/android/providers/context/util/ContextLogger;->logLocalProvider(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Z

    .line 336
    :cond_0
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v1, :cond_1

    .line 337
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContextService: install log inserted. action["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], package["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :cond_1
    return-void
.end method

.method private registerAppErrorReceiver()V
    .locals 2

    .prologue
    .line 280
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 281
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.DROPBOX_ENTRY_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 282
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextService;->mAppErrorReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/providers/context/ContextService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 283
    return-void
.end method

.method private registerPackageReceiver()V
    .locals 2

    .prologue
    .line 312
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 313
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 314
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 315
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 316
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextService;->mPkgReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/providers/context/ContextService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 317
    return-void
.end method

.method private registerUserReceiver()V
    .locals 2

    .prologue
    .line 342
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 343
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 344
    const-string v1, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 345
    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/providers/context/ContextService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 346
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 232
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 147
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 148
    const-string v0, "ContextFramework"

    const-string v1, "ContextService: onCreate() start"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getInitedPermissionManager()Lcom/samsung/android/providers/context/privacy/PermissionManager;

    .line 151
    invoke-static {}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->getInstance()Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    .line 152
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->enable()V

    .line 155
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->onUserForeground()V

    .line 156
    invoke-static {}, Lcom/samsung/android/providers/context/status/WifiMonitor;->getInstance()Lcom/samsung/android/providers/context/status/WifiMonitor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/providers/context/status/WifiMonitor;->enable(Landroid/content/Context;)V

    .line 157
    invoke-static {}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->getInstance()Lcom/samsung/android/providers/context/status/RoamingMonitor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->enable(Landroid/content/Context;)V

    .line 159
    new-instance v0, Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    iget-object v1, p0, Lcom/samsung/android/providers/context/ContextService;->mHandler:Lcom/samsung/android/providers/context/ContextService$ServiceHandler;

    iget-object v2, p0, Lcom/samsung/android/providers/context/ContextService;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    invoke-direct {v0, p0, v1, v2}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    .line 161
    invoke-direct {p0}, Lcom/samsung/android/providers/context/ContextService;->registerPackageReceiver()V

    .line 163
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-direct {p0}, Lcom/samsung/android/providers/context/ContextService;->registerAppErrorReceiver()V

    .line 167
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/providers/context/ContextService;->registerUserReceiver()V

    .line 169
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->checkSurveyLogSetting()V

    .line 171
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    new-instance v0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mScreenShotMonitor:Lcom/samsung/android/providers/context/status/ScreenShotMonitor;

    .line 173
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mScreenShotMonitor:Lcom/samsung/android/providers/context/status/ScreenShotMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->startScreenShotObserver()V

    .line 176
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/providers/context/ContextApplication;->notifyAppCreated(Landroid/content/Context;)V

    .line 177
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mDeviceStatus:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;->disable()V

    .line 182
    invoke-static {}, Lcom/samsung/android/providers/context/status/WifiMonitor;->getInstance()Lcom/samsung/android/providers/context/status/WifiMonitor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/providers/context/status/WifiMonitor;->disable(Landroid/content/Context;)V

    .line 183
    invoke-static {}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->getInstance()Lcom/samsung/android/providers/context/status/RoamingMonitor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/providers/context/status/RoamingMonitor;->disable(Landroid/content/Context;)V

    .line 185
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->destroy()V

    .line 187
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mPkgReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/providers/context/ContextService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 188
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/providers/context/ContextService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 189
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mAppErrorReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/providers/context/ContextService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 191
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 192
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x3

    .line 196
    iget-boolean v2, p0, Lcom/samsung/android/providers/context/ContextService;->mRunning:Z

    if-eqz v2, :cond_1

    .line 197
    const-string v2, "ContextFramework"

    const-string v3, "ContextService: service already started"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_0
    :goto_0
    return v4

    .line 201
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/providers/context/ContextService;->mRunning:Z

    .line 203
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 205
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_2

    .line 206
    const-string v2, "ContextFramework"

    const-string v3, "ContextService: onStartCommand() called"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_2
    if-eqz p1, :cond_0

    .line 211
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "com.samsung.android.providers.context.action.RESTART"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    :cond_3
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_4

    .line 214
    const-string v2, "ContextFramework"

    const-string v3, "ContextService: Started"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_4
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->init()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    :catch_0
    move-exception v1

    .line 221
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 222
    const-string v2, "ContextFramework"

    const-string v3, "ContextService: Failed to connect usage stats watcher"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 257
    sparse-switch p1, :sswitch_data_0

    .line 269
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onTrimMemory(I)V

    .line 270
    return-void

    .line 261
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/samsung/android/providers/context/ContextService;->mAppUsageMonitor:Lcom/samsung/android/providers/context/status/AppUsageMonitor;

    invoke-virtual {v0, p1}, Lcom/samsung/android/providers/context/status/AppUsageMonitor;->onTrimMemory(I)V

    goto :goto_0

    .line 257
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
    .end sparse-switch
.end method
