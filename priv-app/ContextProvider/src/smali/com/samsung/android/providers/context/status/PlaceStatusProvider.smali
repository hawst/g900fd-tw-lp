.class public Lcom/samsung/android/providers/context/status/PlaceStatusProvider;
.super Landroid/content/ContentProvider;
.source "PlaceStatusProvider.java"


# static fields
.field private static final mColumnNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "category"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "method"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceStatusProvider;->mColumnNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projections"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 65
    const-string v7, "ContextFramework:PlaceStatusProvider"

    const-string v8, "A query is received (status.place CP)"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const/4 v4, 0x0

    .line 69
    .local v4, "needToUpdateName":Z
    if-nez p2, :cond_3

    .line 70
    sget-object p2, Lcom/samsung/android/providers/context/status/PlaceStatusProvider;->mColumnNames:[Ljava/lang/String;

    .line 71
    const/4 v4, 0x1

    .line 81
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->getInstance()Lcom/samsung/android/providers/context/status/PlaceMonitor;

    move-result-object v5

    .line 84
    .local v5, "placeMonitor":Lcom/samsung/android/providers/context/status/PlaceMonitor;
    if-eqz v4, :cond_1

    .line 85
    invoke-virtual {v5}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->updateAllPlaceNames()V

    .line 88
    :cond_1
    new-instance v3, Landroid/database/MatrixCursor;

    invoke-direct {v3, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 89
    .local v3, "matrixCursor":Landroid/database/MatrixCursor;
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/status/PlaceStatusProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentNow(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 90
    invoke-virtual {v5, v3, p2}, Lcom/samsung/android/providers/context/status/PlaceMonitor;->fillCursor(Landroid/database/MatrixCursor;[Ljava/lang/String;)V

    .line 93
    :cond_2
    return-object v3

    .line 73
    .end local v3    # "matrixCursor":Landroid/database/MatrixCursor;
    .end local v5    # "placeMonitor":Lcom/samsung/android/providers/context/status/PlaceMonitor;
    :cond_3
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v6, v0, v1

    .line 74
    .local v6, "property":Ljava/lang/String;
    const-string v7, "name"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 75
    const/4 v4, 0x1

    .line 76
    goto :goto_0

    .line 73
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method
