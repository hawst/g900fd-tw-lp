.class public Lcom/samsung/android/providers/context/security/SecureDbManager;
.super Ljava/lang/Object;
.source "SecureDbManager.java"


# static fields
.field private static isFirstTrial:Z

.field private static mPassword:[B

.field private static mReInitCount:I

.field private static mSecureDbManager:Lcom/samsung/android/providers/context/security/SecureDbManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    sput-object v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B

    .line 42
    sput-object v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->mSecureDbManager:Lcom/samsung/android/providers/context/security/SecureDbManager;

    .line 44
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->isFirstTrial:Z

    .line 46
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->mReInitCount:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    sget-object v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B

    if-nez v0, :cond_0

    .line 50
    invoke-static {}, Lcom/samsung/android/providers/context/security/SecureDbManager;->initialize()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B

    .line 54
    :cond_0
    return-void
.end method

.method private static createNewKey()[B
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 86
    const/4 v3, 0x0

    .line 87
    .local v3, "newKey":[B
    new-instance v1, Lcom/samsung/android/providers/context/security/AesEncryption;

    invoke-direct {v1}, Lcom/samsung/android/providers/context/security/AesEncryption;-><init>()V

    .line 91
    .local v1, "enc":Lcom/samsung/android/providers/context/security/AesEncryption;
    :try_start_0
    const-string v7, "SHA1PRNG"

    invoke-static {v7}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 98
    .local v5, "sr":Ljava/security/SecureRandom;
    const/16 v7, 0x100

    new-array v4, v7, [B

    .line 99
    .local v4, "output":[B
    invoke-virtual {v5, v4}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 101
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([B)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/providers/context/security/AesEncryption;->getSecretKey(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v2

    .line 102
    .local v2, "key":Ljavax/crypto/SecretKey;
    if-eqz v2, :cond_0

    .line 103
    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v3

    .line 108
    const-string v6, "ContextFramework"

    const-string v7, "Key for secure DB is newly created"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    .end local v2    # "key":Ljavax/crypto/SecretKey;
    .end local v3    # "newKey":[B
    .end local v4    # "output":[B
    .end local v5    # "sr":Ljava/security/SecureRandom;
    :goto_0
    return-object v3

    .line 92
    .restart local v3    # "newKey":[B
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 94
    const-string v7, "ContextFramework"

    const-string v8, "Fails to create new key for secure DB"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v6

    .line 95
    goto :goto_0

    .line 105
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v2    # "key":Ljavax/crypto/SecretKey;
    .restart local v4    # "output":[B
    .restart local v5    # "sr":Ljava/security/SecureRandom;
    :cond_0
    const-string v7, "ContextFramework"

    const-string v8, "Fails to create new key for secure DB"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v6

    .line 106
    goto :goto_0
.end method

.method public static declared-synchronized getPassword()[B
    .locals 2

    .prologue
    .line 57
    const-class v1, Lcom/samsung/android/providers/context/security/SecureDbManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B

    if-nez v0, :cond_0

    .line 58
    invoke-static {}, Lcom/samsung/android/providers/context/security/SecureDbManager;->initialize()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B

    .line 62
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static initSecureStorage()Z
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 121
    const/4 v1, 0x0

    .line 122
    .local v1, "passwd":[B
    const/4 v8, 0x0

    sput-object v8, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B

    .line 123
    const/4 v3, 0x0

    .line 124
    .local v3, "retry_count":I
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v8

    iget-boolean v8, v8, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v8, :cond_0

    .line 125
    const-string v8, "ContextFramework"

    const-string v9, "Check whether supporting secure storage or not. It may takes several seconds"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_0
    invoke-static {}, Lcom/sec/android/securestorage/SecureStorage;->isSupported()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 130
    new-instance v4, Lcom/sec/android/securestorage/SecureStorage;

    invoke-direct {v4}, Lcom/sec/android/securestorage/SecureStorage;-><init>()V

    .line 132
    .local v4, "ss":Lcom/sec/android/securestorage/SecureStorage;
    :cond_1
    const/4 v5, 0x0

    .line 134
    .local v5, "storedKey":[B
    :try_start_0
    const-string v8, "CF_logDBKey"

    invoke-virtual {v4, v8}, Lcom/sec/android/securestorage/SecureStorage;->getByteArray(Ljava/lang/String;)[B
    :try_end_0
    .catch Lcom/sec/android/securestorage/SecureStorage$SecureStorageException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 146
    :cond_2
    :goto_0
    if-eqz v5, :cond_4

    .line 148
    sput-object v5, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B

    .line 149
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v7

    iget-boolean v7, v7, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v7, :cond_3

    .line 150
    const-string v7, "ContextFramework"

    const-string v8, "Key gotten from Secure Storage"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    .end local v4    # "ss":Lcom/sec/android/securestorage/SecureStorage;
    .end local v5    # "storedKey":[B
    :cond_3
    :goto_1
    return v6

    .line 135
    .restart local v4    # "ss":Lcom/sec/android/securestorage/SecureStorage;
    .restart local v5    # "storedKey":[B
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    invoke-static {}, Lcom/samsung/android/providers/context/security/SecureDbManager;->isVirgin()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 142
    invoke-static {}, Lcom/samsung/android/providers/context/security/SecureDbManager;->createNewKey()[B

    move-result-object v1

    goto :goto_0

    .line 154
    .end local v0    # "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    :cond_4
    if-eqz v1, :cond_5

    array-length v8, v1

    if-lez v8, :cond_5

    .line 155
    const/4 v2, 0x0

    .line 157
    .local v2, "putSuccess":Z
    :try_start_1
    const-string v8, "CF_logDBKey"

    invoke-virtual {v4, v8, v1}, Lcom/sec/android/securestorage/SecureStorage;->put(Ljava/lang/String;[B)Z
    :try_end_1
    .catch Lcom/sec/android/securestorage/SecureStorage$SecureStorageException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 162
    :goto_2
    if-eqz v2, :cond_5

    .line 163
    sput-object v1, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B

    .line 164
    const-string v7, "ContextFramework"

    const-string v8, "Key newly created for Secure Log DB"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 158
    :catch_1
    move-exception v0

    .line 160
    .restart local v0    # "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    invoke-virtual {v0}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->printStackTrace()V

    goto :goto_2

    .line 170
    .end local v0    # "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .end local v2    # "putSuccess":Z
    :cond_5
    const-wide/16 v8, 0x3e8

    :try_start_2
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 171
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v8

    iget-boolean v8, v8, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v8, :cond_6

    .line 172
    const-string v8, "ContextFramework"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Try again to get in touch with secure storage ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 178
    :cond_6
    :goto_3
    add-int/lit8 v3, v3, 0x1

    const/4 v8, 0x3

    if-lt v3, v8, :cond_7

    .line 183
    :goto_4
    sput-boolean v7, Lcom/samsung/android/providers/context/security/SecureDbManager;->isFirstTrial:Z

    .line 191
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->isEngineerBinary()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 192
    sget v8, Lcom/samsung/android/providers/context/security/SecureDbManager;->mReInitCount:I

    add-int/lit8 v8, v8, 0x1

    sput v8, Lcom/samsung/android/providers/context/security/SecureDbManager;->mReInitCount:I

    const/4 v9, 0x2

    if-le v8, v9, :cond_a

    .line 194
    :try_start_3
    const-string v7, "cf.secure.db.key"

    const-string v8, "UTF-8"

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    sput-object v7, Lcom/samsung/android/providers/context/security/SecureDbManager;->mPassword:[B
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_3

    .line 198
    :goto_5
    const-string v7, "ContextFramework"

    const-string v8, "(ENG) From now, predefined secure key"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 174
    :catch_2
    move-exception v0

    .line 175
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 181
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_7
    sget-boolean v8, Lcom/samsung/android/providers/context/security/SecureDbManager;->isFirstTrial:Z

    if-nez v8, :cond_1

    goto :goto_4

    .line 185
    .end local v4    # "ss":Lcom/sec/android/securestorage/SecureStorage;
    .end local v5    # "storedKey":[B
    :cond_8
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v6

    iget-boolean v6, v6, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v6, :cond_9

    .line 186
    const-string v6, "ContextFramework"

    const-string v8, "Secure Storage is not suppported"

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    move v6, v7

    .line 188
    goto/16 :goto_1

    .line 195
    .restart local v4    # "ss":Lcom/sec/android/securestorage/SecureStorage;
    .restart local v5    # "storedKey":[B
    :catch_3
    move-exception v0

    .line 196
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_5

    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_a
    move v6, v7

    .line 203
    goto/16 :goto_1
.end method

.method private static declared-synchronized initialize()Z
    .locals 3

    .prologue
    .line 77
    const-class v1, Lcom/samsung/android/providers/context/security/SecureDbManager;

    monitor-enter v1

    :try_start_0
    const-string v0, "ContextFramework"

    const-string v2, "SecureDbManager initialization"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/providers/context/FeatureManager;->isSupportedFeature(S)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {}, Lcom/samsung/android/providers/context/security/SecureDbManager;->initSecureStorage()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 82
    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static isVirgin()Z
    .locals 3

    .prologue
    .line 113
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 114
    .local v0, "context":Landroid/content/Context;
    const-string v2, "SecureContextLog.db"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 115
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
