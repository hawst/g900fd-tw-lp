.class public Lcom/samsung/android/providers/context/ContextInitTask;
.super Landroid/os/AsyncTask;
.source "ContextInitTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 5
    .param p1, "ignores"    # [Ljava/lang/String;

    .prologue
    .line 33
    const/4 v1, 0x0

    .line 34
    .local v1, "ldb":Lcom/samsung/android/providers/context/log/db/LogDbHelper;
    const/4 v2, 0x0

    .line 36
    .local v2, "sldb":Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    :try_start_0
    const-string v3, "ContextFramework"

    const-string v4, "Start initialization for secure DB ( Log DB )"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getLogDatabase()Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    move-result-object v1

    .line 39
    const-string v3, "ContextFramework"

    const-string v4, "Start initialization for secure DB ( Survey Log DB )"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getSurveyLogDatabase()Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

    move-result-object v2

    .line 42
    const-string v3, "ContextFramework"

    const-string v4, "ContextApplication: Start initialization for SamsungAccountInfo"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->initSamsungAccountInfo()V

    .line 45
    const-string v3, "ContextFramework"

    const-string v4, "ContextApplication: parallel initialization finished"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :cond_0
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_0
    return-object v3

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 48
    instance-of v3, v0, Landroid/database/sqlite/SQLiteDatabaseCorruptException;

    if-eqz v3, :cond_0

    .line 49
    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 53
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/ContextInitTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 60
    const-string v0, "ContextFramework"

    const-string v1, "All init tasks are successfully completed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 62
    const-string v0, "ContextFramework"

    const-string v1, "Init tasks are not successfully completed (secure key acquisition failure)"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 64
    const-string v0, "ContextFramework"

    const-string v1, "Init tasks are not successfully completed (DB corruption)"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/ContextInitTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
