.class public Lcom/samsung/android/providers/context/log/RecordVideoLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "RecordVideoLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 35
    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "app_id"

    aput-object v3, v1, v2

    const-string v2, "pixel_height"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "pixel_width"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "uri"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "start_time"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "stop_time"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "starttime"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "stoptime"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "duration"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/RecordVideoLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "record_video"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/RecordVideoLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 47
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    .line 48
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->supportUpdate:Z

    .line 49
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useUpdateBroadcast:Z

    .line 51
    new-instance v1, Lcom/samsung/android/providers/context/log/RecordVideoLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/RecordVideoLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/RecordVideoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 97
    sget-object v1, Lcom/samsung/android/providers/context/log/RecordVideoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 98
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/samsung/android/providers/context/log/RecordVideoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 140
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 7
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 103
    .local v0, "app_id":Ljava/lang/String;
    const/4 v4, 0x0

    .line 104
    .local v4, "uri":Ljava/lang/String;
    const/4 v3, 0x0

    .line 105
    .local v3, "pixel_width":I
    const/4 v2, 0x0

    .line 108
    .local v2, "pixel_height":I
    :try_start_0
    const-string v5, "app_id"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    const-string v5, "uri"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 110
    const-string v5, "pixel_width"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 111
    const-string v5, "pixel_height"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 117
    :goto_0
    if-eqz v0, :cond_0

    const-string v5, ""

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 118
    :cond_0
    const-string v5, "ContextFramework"

    const-string v6, "RecordVideo : app_id is invalid"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_1
    if-eqz v4, :cond_2

    const-string v5, ""

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 121
    :cond_2
    const-string v5, "ContextFramework"

    const-string v6, "RecordVideo : uri is invalid"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_3
    if-gtz v3, :cond_4

    .line 124
    const-string v5, "ContextFramework"

    const-string v6, "RecordVideo : pixel_width is invalid"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_4
    if-gtz v2, :cond_5

    .line 127
    const-string v5, "ContextFramework"

    const-string v6, "RecordVideo : pixel_height is invalid"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_5
    const/4 v5, 0x1

    return v5

    .line 112
    :catch_0
    move-exception v1

    .line 113
    .local v1, "ex":Ljava/lang/NullPointerException;
    const-string v5, "ContextFramework"

    const-string v6, "RecordVideo : ValidBoundaryCheck - NullPointerException."

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
