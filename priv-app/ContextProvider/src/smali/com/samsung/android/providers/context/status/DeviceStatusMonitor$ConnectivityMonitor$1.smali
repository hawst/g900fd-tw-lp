.class Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "DeviceStatusMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;-><init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;

.field final synthetic val$this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;

    iput-object p2, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;->val$this$0:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 393
    const-string v0, "networkType"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 401
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkRoamingStatus(Landroid/content/Context;)V
    invoke-static {v0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->access$600(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;Landroid/content/Context;)V

    .line 402
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkWifiStatus(Landroid/content/Context;)V
    invoke-static {v0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->access$700(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;Landroid/content/Context;)V

    .line 405
    :goto_0
    return-void

    .line 395
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkRoamingStatus(Landroid/content/Context;)V
    invoke-static {v0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->access$600(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;Landroid/content/Context;)V

    goto :goto_0

    .line 398
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor$1;->this$1:Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;

    # invokes: Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->checkWifiStatus(Landroid/content/Context;)V
    invoke-static {v0, p1}, Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;->access$700(Lcom/samsung/android/providers/context/status/DeviceStatusMonitor$ConnectivityMonitor;Landroid/content/Context;)V

    goto :goto_0

    .line 393
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
