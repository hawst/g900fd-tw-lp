.class public final Lcom/samsung/android/providers/context/log/ContextLogContract$TrackRoaming;
.super Ljava/lang/Object;
.source "ContextLogContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/samsung/android/providers/context/log/ContextLogContract$BaseContextColumns;
.implements Lcom/samsung/android/providers/context/log/ContextLogContract$TrackRoamingColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/log/ContextLogContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TrackRoaming"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1737
    const-string v0, "content://com.samsung.android.providers.context.log.track_roaming"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/log/ContextLogContract$TrackRoaming;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732
    return-void
.end method
