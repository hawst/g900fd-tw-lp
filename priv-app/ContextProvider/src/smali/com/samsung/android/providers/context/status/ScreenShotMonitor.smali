.class public Lcom/samsung/android/providers/context/status/ScreenShotMonitor;
.super Ljava/lang/Object;
.source "ScreenShotMonitor.java"


# instance fields
.field private final mScreenShotObserver:Landroid/os/FileObserver;

.field private final mWatchDir:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v1, Ljava/io/File;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    const-string v3, "Screenshots"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->mWatchDir:Ljava/io/File;

    .line 46
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->mWatchDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 47
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Lcom/samsung/android/providers/context/status/ScreenShotMonitor$1;

    const/16 v2, 0x100

    invoke-direct {v1, p0, v0, v2, p1}, Lcom/samsung/android/providers/context/status/ScreenShotMonitor$1;-><init>(Lcom/samsung/android/providers/context/status/ScreenShotMonitor;Ljava/lang/String;ILandroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->mScreenShotObserver:Landroid/os/FileObserver;

    .line 69
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ScreenShotMonitor: watching "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/providers/context/status/ScreenShotMonitor;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/status/ScreenShotMonitor;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->mWatchDir:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/ContentResolver;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-static {p0, p1}, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->logScreenShot(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static logScreenShot(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 3
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 81
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 82
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "type"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 83
    const-string v1, "uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$CaptureContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v1, v0}, Lcom/samsung/android/providers/context/util/ContextLogger;->logLocalProvider(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public startScreenShotObserver()V
    .locals 4

    .prologue
    .line 73
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->mWatchDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->mWatchDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    .line 75
    .local v0, "b":Z
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ScreenShotMonitor: watch directory created with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    .end local v0    # "b":Z
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/status/ScreenShotMonitor;->mScreenShotObserver:Landroid/os/FileObserver;

    invoke-virtual {v1}, Landroid/os/FileObserver;->startWatching()V

    .line 78
    return-void
.end method
