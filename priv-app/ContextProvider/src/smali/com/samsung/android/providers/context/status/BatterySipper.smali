.class public Lcom/samsung/android/providers/context/status/BatterySipper;
.super Ljava/lang/Object;
.source "BatterySipper.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/samsung/android/providers/context/status/BatterySipper;",
        ">;"
    }
.end annotation


# instance fields
.field value:D


# virtual methods
.method public compareTo(Lcom/samsung/android/providers/context/status/BatterySipper;)I
    .locals 4
    .param p1, "other"    # Lcom/samsung/android/providers/context/status/BatterySipper;

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/samsung/android/providers/context/status/BatterySipper;->getSortValue()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/samsung/android/providers/context/status/BatterySipper;->getSortValue()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, Lcom/samsung/android/providers/context/status/BatterySipper;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/providers/context/status/BatterySipper;->compareTo(Lcom/samsung/android/providers/context/status/BatterySipper;)I

    move-result v0

    return v0
.end method

.method public getSortValue()D
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/samsung/android/providers/context/status/BatterySipper;->value:D

    return-wide v0
.end method
