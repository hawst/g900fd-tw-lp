.class public Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;
.super Lcom/samsung/android/providers/context/privacy/IPrivacyManager$Stub;
.source "PrivacyManagerStub.java"


# static fields
.field private static mInstance:Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;

.field private static final permissionArray:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NOTDEFINED"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.samsung.android.providers.context.permission.READ_USE_APP"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.samsung.android.providers.context.permission.READ_MANAGE_APP"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.samsung.android.providers.context.permission.READ_TAKE_PHOTO"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.samsung.android.providers.context.permission.READ_RECORD_VIDEO"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.samsung.android.providers.context.permission.READ_PLAY_MUSIC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.samsung.android.providers.context.permission.READ_PLAY_VIDEO"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.samsung.android.providers.context.permission.READ_EXCHANGE_CALL"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.samsung.android.providers.context.permission.READ_EXCHANGE_MESSAGE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.samsung.android.providers.context.permission.READ_EXCHANGE_EMAIL"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.samsung.android.providers.context.permission.READ_BROWSE_WEB"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.samsung.android.providers.context.permission.READ_SEARCH_KEYWORD"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.samsung.android.providers.context.permission.READ_TRACK_ROAMING"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.samsung.android.providers.context.permission.READ_CHANGE_DEVICE_STATUS"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.samsung.android.providers.context.permission.READ_USE_WIFI"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "com.samsung.android.providers.context.permission.READ_MOVE_PLACE"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "com.samsung.android.providers.context.permission.READ_RECORD_AUDIO"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "com.samsung.android.providers.context.permission.READ_WRITE_DOCUMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "com.samsung.android.providers.context.permission.READ_CAPTURE_CONTENT"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "com.samsung.android.providers.context.permission.READ_MOVE_LOCATION"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "com.samsung.android.providers.context.permission.READ_CHANGE_ACTIVITY"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "com.samsung.android.providers.context.permission.READ_MOVE_AREA"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "com.samsung.android.providers.context.permission.STATUS_PLACE"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "com.samsung.android.providers.context.permission.UPLOAD_APP_FEATURE_SURVEY"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "com.samsung.android.providers.context.permission.UPLOAD_ALWAYS_SERVICE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->permissionArray:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/samsung/android/providers/context/privacy/IPrivacyManager$Stub;-><init>()V

    .line 72
    return-void
.end method

.method private getAppName(I)Ljava/lang/String;
    .locals 7
    .param p1, "pID"    # I

    .prologue
    .line 169
    const-string v4, ""

    .line 170
    .local v4, "processName":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v5

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Lcom/samsung/android/providers/context/ContextApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 171
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    .line 173
    .local v3, "l":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 174
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 175
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 176
    .local v2, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, p1, :cond_0

    .line 177
    iget-object v4, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    goto :goto_0

    .line 181
    .end local v2    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    return-object v4
.end method

.method public static getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->mInstance:Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;

    invoke-direct {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->mInstance:Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;

    .line 78
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->mInstance:Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;

    return-object v0
.end method

.method private isClientPermitted(I)Z
    .locals 4
    .param p1, "privacyItem"    # I

    .prologue
    .line 156
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 157
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getCallingPid()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getAppName(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "packageName":Ljava/lang/String;
    const/4 v2, -0x1

    .line 160
    .local v2, "result":I
    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->permissionArray:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v1, v3, v0}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 161
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 162
    const/4 v3, 0x0

    .line 165
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isClientPermitted([I)Z
    .locals 8
    .param p1, "privacyItem"    # [I

    .prologue
    .line 141
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/providers/context/ContextApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 142
    .local v5, "pm":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getCallingPid()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getAppName(I)Ljava/lang/String;

    move-result-object v4

    .line 143
    .local v4, "packageName":Ljava/lang/String;
    const/4 v6, -0x1

    .line 145
    .local v6, "result":I
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget v2, v0, v1

    .line 146
    .local v2, "item":I
    sget-object v7, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->permissionArray:[Ljava/lang/String;

    aget-object v7, v7, v2

    invoke-virtual {v5, v7, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 147
    const/4 v7, -0x1

    if-ne v6, v7, :cond_0

    .line 148
    const/4 v7, 0x0

    .line 152
    .end local v2    # "item":I
    :goto_1
    return v7

    .line 145
    .restart local v2    # "item":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 152
    .end local v2    # "item":I
    :cond_1
    const/4 v7, 0x1

    goto :goto_1
.end method


# virtual methods
.method public isPrivacyConsentAcquired(I)Z
    .locals 5
    .param p1, "privacyItem"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->isClientPermitted(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 85
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1}, Ljava/lang/SecurityException;-><init>()V

    .line 87
    .local v1, "e":Ljava/lang/SecurityException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_0

    .line 88
    const-string v2, "ContextFramewor.PrivacyManagerStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Security Exception, check = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->permissionArray:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const-string v2, "ContextFramewor.PrivacyManagerStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Security Exception, caller = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getCallingPid()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getAppName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    throw v1

    .line 94
    .end local v1    # "e":Ljava/lang/SecurityException;
    :cond_1
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v0

    .line 95
    .local v0, "accessControl":Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getCallingPid()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getAppName(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->getPrivacyItem(I)Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentNow(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    return v2
.end method

.method public setPrivacyConsent([I)V
    .locals 8
    .param p1, "privacyItem"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->isClientPermitted([I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 102
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1}, Ljava/lang/SecurityException;-><init>()V

    .line 104
    .local v1, "e":Ljava/lang/SecurityException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v5

    iget-boolean v5, v5, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v5, :cond_0

    .line 105
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget v3, v0, v2

    .line 106
    .local v3, "item":I
    const-string v5, "ContextFramewor.PrivacyManagerStub"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Security Exception, set = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->permissionArray:[Ljava/lang/String;

    aget-object v7, v7, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v5, "ContextFramewor.PrivacyManagerStub"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Security Exception, caller = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getCallingPid()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getAppName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111
    .end local v0    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "item":I
    .end local v4    # "len$":I
    :cond_0
    throw v1

    .line 114
    .end local v1    # "e":Ljava/lang/SecurityException;
    :cond_1
    move-object v0, p1

    .restart local v0    # "arr$":[I
    array-length v4, v0

    .restart local v4    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v4, :cond_2

    aget v3, v0, v2

    .line 115
    .restart local v3    # "item":I
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getCallingPid()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getAppName(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->setPrivacyItem(Ljava/lang/String;I)V

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 117
    .end local v3    # "item":I
    :cond_2
    return-void
.end method

.method public unsetPrivacyConsent([I)V
    .locals 8
    .param p1, "privacyItem"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->isClientPermitted([I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 123
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1}, Ljava/lang/SecurityException;-><init>()V

    .line 125
    .local v1, "e":Ljava/lang/SecurityException;
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v5

    iget-boolean v5, v5, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v5, :cond_0

    .line 126
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget v3, v0, v2

    .line 127
    .local v3, "item":I
    const-string v5, "ContextFramewor.PrivacyManagerStub"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Security Exception, unset = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->permissionArray:[Ljava/lang/String;

    aget-object v7, v7, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const-string v5, "ContextFramewor.PrivacyManagerStub"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Security Exception, caller = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getCallingPid()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getAppName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 132
    .end local v0    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "item":I
    .end local v4    # "len$":I
    :cond_0
    throw v1

    .line 135
    .end local v1    # "e":Ljava/lang/SecurityException;
    :cond_1
    move-object v0, p1

    .restart local v0    # "arr$":[I
    array-length v4, v0

    .restart local v4    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v4, :cond_2

    aget v3, v0, v2

    .line 136
    .restart local v3    # "item":I
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getCallingPid()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/samsung/android/providers/context/privacy/PrivacyManagerStub;->getAppName(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/samsung/android/providers/context/privacy/PermissionManager;->unsetPrivacyItem(Ljava/lang/String;I)V

    .line 135
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 138
    .end local v3    # "item":I
    :cond_2
    return-void
.end method
