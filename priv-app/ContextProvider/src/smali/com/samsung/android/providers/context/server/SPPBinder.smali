.class public Lcom/samsung/android/providers/context/server/SPPBinder;
.super Ljava/lang/Object;
.source "SPPBinder.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mConMsgHandler:Landroid/os/Handler;

.field private final mConnection:Landroid/content/ServiceConnection;

.field private mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/android/providers/context/server/ServerUploader;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/providers/context/server/SPPBinder;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;)V
    .locals 2
    .param p1, "mConMsgHandler"    # Lcom/samsung/android/providers/context/server/LogSender$ConnectionMsgHandler;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 72
    new-instance v0, Lcom/samsung/android/providers/context/server/SPPBinder$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/server/SPPBinder$1;-><init>(Lcom/samsung/android/providers/context/server/SPPBinder;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mConnection:Landroid/content/ServiceConnection;

    .line 47
    sget-object v0, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v1, "Construct SPPBinder"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mConMsgHandler:Landroid/os/Handler;

    .line 49
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/providers/context/server/SPPBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SPPBinder;
    .param p1, "x1"    # Lcom/sec/spp/push/dlc/api/IDlcService;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/providers/context/server/SPPBinder;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/providers/context/server/SPPBinder;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mConMsgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private isServiceConnected()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public bindService(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    sget-object v1, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v2, "Bind Service"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 54
    .local v0, "service":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.spp.push"

    const-string v3, "com.sec.spp.push.dlc.writer.WriterService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 55
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 56
    return-void
.end method

.method public sendLog(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "svcCode"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J
    .param p5, "activity"    # Ljava/lang/String;
    .param p6, "deviceId"    # Ljava/lang/String;
    .param p7, "userId"    # Ljava/lang/String;
    .param p8, "appVersion"    # Ljava/lang/String;
    .param p9, "body"    # Ljava/lang/String;

    .prologue
    .line 106
    sget-object v1, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v2, "Send Log"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-direct {p0}, Lcom/samsung/android/providers/context/server/SPPBinder;->isServiceConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    sget-object v1, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v2, "IDlc service is already connected"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :try_start_0
    sget-object v1, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v2, "Request send by IDlc service"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-nez v1, :cond_0

    .line 113
    sget-object v1, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v2, "IDlcService is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v1, 0x0

    .line 122
    :goto_0
    return v1

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-interface/range {v1 .. v10}, Lcom/sec/spp/push/dlc/api/IDlcService;->requestSend(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 119
    sget-object v1, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RemoteException Error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const/16 v1, -0x3e8

    goto :goto_0
.end method

.method public unbindService(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    sget-object v0, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v1, "Unbind Service"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 63
    sget-object v0, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v1, "SPPBinder is disconnected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mConMsgHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 65
    sget-object v0, Lcom/samsung/android/providers/context/server/SPPBinder;->TAG:Ljava/lang/String;

    const-string v1, "ConMsgHandler is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SPPBinder;->mConMsgHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
