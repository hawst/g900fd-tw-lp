.class public Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ContextLogBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 70
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 73
    .local v0, "cr":Landroid/content/ContentResolver;
    :try_start_0
    const-string v3, "data"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 74
    .local v2, "row":Landroid/content/ContentValues;
    invoke-virtual {v0, p1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 80
    .end local v2    # "row":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v1

    .line 76
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No \"data\" bundle."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 77
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 78
    .local v1, "ex":Landroid/database/sqlite/SQLiteException;
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Database operation failure:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$UseAppFeatureSurvey;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    const-string v1, "com.samsung.android.providers.context.log.action.EXCHANGE_MESSAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeMessage;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    goto :goto_0

    .line 52
    :cond_2
    const-string v1, "com.samsung.android.providers.context.log.action.EXCHANGE_EMAIL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 53
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeEmail;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    goto :goto_0

    .line 54
    :cond_3
    const-string v1, "com.samsung.android.providers.context.log.action.EXCHANGE_CALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 55
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$ExchangeCall;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    goto :goto_0

    .line 56
    :cond_4
    const-string v1, "com.samsung.android.providers.context.log.action.BROWSE_WEB"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 57
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$BrowseWeb;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    goto :goto_0

    .line 58
    :cond_5
    const-string v1, "com.samsung.android.providers.context.log.action.SEARCH_KEYWORD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 59
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$SearchKeyword;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    goto :goto_0

    .line 60
    :cond_6
    const-string v1, "com.samsung.android.providers.context.log.action.RECORD_AUDIO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 61
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$RecordAudio;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    goto :goto_0

    .line 62
    :cond_7
    const-string v1, "com.samsung.android.providers.context.log.action.WRITE_DOCUMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 63
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$WriteDocument;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    goto :goto_0

    .line 64
    :cond_8
    const-string v1, "com.samsung.android.providers.context.log.action.CAPTURE_CONTENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    sget-object v1, Lcom/samsung/android/providers/context/log/ContextLogContract$CaptureContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, v1, p2}, Lcom/samsung/android/providers/context/ContextLogBroadcastReceiver;->insertLog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;)V

    goto :goto_0
.end method
