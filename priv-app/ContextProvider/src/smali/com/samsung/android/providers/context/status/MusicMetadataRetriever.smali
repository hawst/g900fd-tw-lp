.class public Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;
.super Ljava/lang/Object;
.source "MusicMetadataRetriever.java"


# instance fields
.field private mAlbum:Ljava/lang/String;

.field private mArtists:Ljava/lang/String;

.field private mContentUri:Ljava/lang/String;

.field private mGenres:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mYear:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mContentUri:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mTitle:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mArtists:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mAlbum:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mGenres:Ljava/lang/String;

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mYear:I

    .line 45
    if-nez p1, :cond_0

    .line 46
    const-string v0, "ContextFramework:MusicMetadataRetriever"

    const-string v1, "Music content URI is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :goto_0
    return-void

    .line 48
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mContentUri:Ljava/lang/String;

    .line 49
    const-string v0, "ContextFramework:MusicMetadataRetriever"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Music content URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->retrieveMusicGenre()V

    .line 52
    invoke-direct {p0}, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->retrieveMusicMetadata()V

    goto :goto_0
.end method

.method private retrieveMusicGenre()V
    .locals 13

    .prologue
    .line 65
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 66
    .local v11, "genreIdMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 67
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "name"

    aput-object v3, v2, v1

    .line 72
    .local v2, "projection":[Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 74
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_2

    .line 75
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 84
    :cond_2
    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 86
    .local v10, "genreId":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x2f

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x2f

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "members"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 89
    .local v4, "genreUri":Landroid/net/Uri;
    const-string v6, "_data = ?"

    .line 90
    .local v6, "selectionClause":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mContentUri:Ljava/lang/String;

    aput-object v3, v7, v1

    .line 94
    .local v7, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v3, v0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 96
    if-eqz v9, :cond_3

    .line 97
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 98
    invoke-virtual {v11, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mGenres:Ljava/lang/String;

    .line 99
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 106
    .end local v4    # "genreUri":Landroid/net/Uri;
    .end local v6    # "selectionClause":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .end local v10    # "genreId":Ljava/lang/String;
    :cond_4
    return-void

    .line 102
    .restart local v4    # "genreUri":Landroid/net/Uri;
    .restart local v6    # "selectionClause":Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    .restart local v10    # "genreId":Ljava/lang/String;
    :cond_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private retrieveMusicMetadata()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 110
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 111
    .local v0, "cr":Landroid/content/ContentResolver;
    iget-object v5, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mContentUri:Ljava/lang/String;

    invoke-static {v5}, Landroid/provider/MediaStore$Audio$Media;->getContentUriForPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 112
    .local v1, "uri":Landroid/net/Uri;
    const/4 v5, 0x6

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v8

    const-string v5, "title"

    aput-object v5, v2, v9

    const/4 v5, 0x2

    const-string v7, "artist"

    aput-object v7, v2, v5

    const/4 v5, 0x3

    const-string v7, "album"

    aput-object v7, v2, v5

    const/4 v5, 0x4

    const-string v7, "year"

    aput-object v7, v2, v5

    const/4 v5, 0x5

    const-string v7, "_data"

    aput-object v7, v2, v5

    .line 117
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "_data = ?"

    .line 118
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mContentUri:Ljava/lang/String;

    aput-object v5, v4, v8

    .line 122
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 124
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 125
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_0

    .line 127
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 129
    const-string v5, "title"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mTitle:Ljava/lang/String;

    .line 130
    const-string v5, "artist"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mArtists:Ljava/lang/String;

    .line 131
    const-string v5, "album"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mAlbum:Ljava/lang/String;

    .line 132
    const-string v5, "year"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mYear:I

    .line 135
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 137
    :cond_1
    return-void
.end method


# virtual methods
.method public getAlbum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mAlbum:Ljava/lang/String;

    return-object v0
.end method

.method public getArtists()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mArtists:Ljava/lang/String;

    return-object v0
.end method

.method public getCotentUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mContentUri:Ljava/lang/String;

    return-object v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mGenres:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getYear()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/android/providers/context/status/MusicMetadataRetriever;->mYear:I

    return v0
.end method
