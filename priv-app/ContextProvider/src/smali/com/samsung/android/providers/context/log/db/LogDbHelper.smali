.class public Lcom/samsung/android/providers/context/log/db/LogDbHelper;
.super Lcom/samsung/android/providers/context/log/db/SecureDbHelper;
.source "LogDbHelper.java"


# static fields
.field private static mDatabaseHandlerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->mDatabaseHandlerList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/providers/context/security/NoKeyException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 39
    const-string v0, "ContextLog.db"

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/providers/context/log/db/SecureDbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 40
    return-void
.end method

.method public static declared-synchronized addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V
    .locals 2
    .param p0, "handler"    # Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .prologue
    .line 43
    const-class v1, Lcom/samsung/android/providers/context/log/db/LogDbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->mDatabaseHandlerList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->mDatabaseHandlerList:Ljava/util/ArrayList;

    .line 47
    :cond_0
    sget-object v0, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->mDatabaseHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    monitor-exit v1

    return-void

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected getDatabaseHandlerList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->mDatabaseHandlerList:Ljava/util/ArrayList;

    return-object v0
.end method
