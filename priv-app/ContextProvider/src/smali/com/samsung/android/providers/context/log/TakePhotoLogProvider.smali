.class public Lcom/samsung/android/providers/context/log/TakePhotoLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "TakePhotoLogProvider.java"


# static fields
.field private static final VALID_LIST:[Ljava/lang/String;

.field static final sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 35
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "app_id"

    aput-object v3, v1, v2

    const-string v2, "orientation"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "pixel_height"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "pixel_width"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "uri"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "burst_shot_count"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/providers/context/log/TakePhotoLogProvider;->VALID_LIST:[Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/providers/context/log/LogTableProperty;

    const-string v1, "take_photo"

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    sget-object v3, Lcom/samsung/android/providers/context/log/TakePhotoLogProvider;->VALID_LIST:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/log/LogTableProperty;-><init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V

    .line 47
    .local v0, "staticProperties":Lcom/samsung/android/providers/context/log/LogTableProperty;
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    .line 48
    iput-boolean v4, v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useBroadcast:Z

    .line 50
    new-instance v1, Lcom/samsung/android/providers/context/log/TakePhotoLogProvider$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/providers/context/log/TakePhotoLogProvider$1;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    sput-object v1, Lcom/samsung/android/providers/context/log/TakePhotoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    .line 94
    sget-object v1, Lcom/samsung/android/providers/context/log/TakePhotoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-static {v1}, Lcom/samsung/android/providers/context/log/db/LogDbHelper;->addDatabaseHandler(Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;)V

    .line 95
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/samsung/android/providers/context/log/TakePhotoLogProvider;->sDatabaseHandler:Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/log/db/AbstractDatabaseHandler;->getProviderStaticProperties()Lcom/samsung/android/providers/context/log/LogTableProperty;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 146
    return-void
.end method


# virtual methods
.method public checkBoundaryValidity(Landroid/content/ContentValues;)Z
    .locals 10
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v9, 0x1

    .line 99
    const/4 v0, 0x0

    .line 100
    .local v0, "app_id":Ljava/lang/String;
    const/4 v6, 0x0

    .line 101
    .local v6, "uri":Ljava/lang/String;
    const/4 v2, -0x1

    .line 102
    .local v2, "orientation":I
    const/4 v4, 0x0

    .line 103
    .local v4, "pixel_width":I
    const/4 v3, 0x0

    .line 104
    .local v3, "pixel_height":I
    const/4 v5, -0x1

    .line 107
    .local v5, "type":I
    :try_start_0
    const-string v7, "app_id"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    const-string v7, "uri"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 109
    const-string v7, "orientation"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 110
    const-string v7, "pixel_width"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 111
    const-string v7, "pixel_height"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 112
    const-string v7, "type"

    invoke-virtual {p1, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 118
    :goto_0
    if-eqz v0, :cond_0

    const-string v7, ""

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 119
    :cond_0
    const-string v7, "ContextFramework"

    const-string v8, "TakePhoto : app_id is invalid"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_1
    if-eqz v6, :cond_2

    const-string v7, ""

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 122
    :cond_2
    const-string v7, "ContextFramework"

    const-string v8, "TakePhoto : uri is invalid"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_3
    if-eqz v2, :cond_4

    if-eq v2, v9, :cond_4

    .line 125
    const-string v7, "ContextFramework"

    const-string v8, "TakePhoto : orientation is invalid"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_4
    if-gtz v4, :cond_5

    .line 128
    const-string v7, "ContextFramework"

    const-string v8, "TakePhoto : pixel_width is invalid"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_5
    if-gtz v3, :cond_6

    .line 131
    const-string v7, "ContextFramework"

    const-string v8, "TakePhoto : pixel_height is invalid"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_6
    if-ltz v5, :cond_7

    const/16 v7, 0x11

    if-le v5, v7, :cond_8

    .line 134
    :cond_7
    const-string v7, "ContextFramework"

    const-string v8, "TakePhoto : type is invalid"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_8
    return v9

    .line 113
    :catch_0
    move-exception v1

    .line 114
    .local v1, "ex":Ljava/lang/NullPointerException;
    const-string v7, "ContextFramework"

    const-string v8, "TakePhoto : ValidBoundaryCheck - NullPointerException."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
