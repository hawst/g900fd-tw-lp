.class public Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
.super Ljava/lang/Object;
.source "PlaceMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/PlaceMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyPlaceInfo"
.end annotation


# static fields
.field public static final UNKNOWN_PLACE:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;


# instance fields
.field private mCategory:I

.field private mMethodType:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

.field private mName:Ljava/lang/String;

.field private final mUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 388
    new-instance v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->UNKNOWN_PLACE:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mCategory:I

    .line 391
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mName:Ljava/lang/String;

    .line 393
    sget-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_UNKNOWN:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mMethodType:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 396
    if-eqz p1, :cond_0

    .line 397
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mUri:Ljava/lang/String;

    .line 402
    :goto_0
    return-void

    .line 399
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mUri:Ljava/lang/String;

    .line 400
    const-string v0, "ContextFramework.PlaceMonitor"

    const-string v1, "place uri is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 456
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 457
    check-cast v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;

    .line 458
    .local v0, "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    iget-object v2, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mUri:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mMethodType:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    iget-object v3, v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mMethodType:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 461
    .end local v0    # "placeInfo":Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;
    :cond_0
    return v1
.end method

.method public getCategory()I
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mCategory:I

    return v0
.end method

.method public getMethodType()Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mMethodType:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    return-object v0
.end method

.method public getMethodTypeAsInt()I
    .locals 3

    .prologue
    .line 421
    const/4 v0, 0x0

    .line 422
    .local v0, "method":I
    sget-object v1, Lcom/samsung/android/providers/context/status/PlaceMonitor$1;->$SwitchMap$com$samsung$android$providers$context$status$PlaceMonitor$MyPlaceMethodType:[I

    iget-object v2, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mMethodType:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 436
    :goto_0
    return v0

    .line 424
    :pswitch_0
    const/4 v0, 0x1

    .line 425
    goto :goto_0

    .line 427
    :pswitch_1
    const/4 v0, 0x2

    .line 428
    goto :goto_0

    .line 430
    :pswitch_2
    const/4 v0, 0x3

    .line 431
    goto :goto_0

    .line 422
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mUri:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public setCategory(I)V
    .locals 0
    .param p1, "category"    # I

    .prologue
    .line 440
    iput p1, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mCategory:I

    .line 441
    return-void
.end method

.method public setMethodType(Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;)V
    .locals 0
    .param p1, "type"    # Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .prologue
    .line 450
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mMethodType:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 451
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 444
    if-eqz p1, :cond_0

    .line 445
    iput-object p1, p0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceInfo;->mName:Ljava/lang/String;

    .line 447
    :cond_0
    return-void
.end method
