.class public final enum Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
.super Ljava/lang/Enum;
.source "PlaceMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/status/PlaceMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MyPlaceMethodType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

.field public static final enum MYPLACE_METHOD_TYPE_BT:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

.field public static final enum MYPLACE_METHOD_TYPE_MAP:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

.field public static final enum MYPLACE_METHOD_TYPE_UNKNOWN:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

.field public static final enum MYPLACE_METHOD_TYPE_WIFI:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 378
    new-instance v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    const-string v1, "MYPLACE_METHOD_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_UNKNOWN:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 379
    new-instance v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    const-string v1, "MYPLACE_METHOD_TYPE_MAP"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_MAP:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 380
    new-instance v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    const-string v1, "MYPLACE_METHOD_TYPE_WIFI"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_WIFI:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 381
    new-instance v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    const-string v1, "MYPLACE_METHOD_TYPE_BT"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_BT:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    .line 377
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    sget-object v1, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_UNKNOWN:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_MAP:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_WIFI:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->MYPLACE_METHOD_TYPE_BT:Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->$VALUES:[Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 377
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 377
    const-class v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;
    .locals 1

    .prologue
    .line 377
    sget-object v0, Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->$VALUES:[Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    invoke-virtual {v0}, [Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/providers/context/status/PlaceMonitor$MyPlaceMethodType;

    return-object v0
.end method
