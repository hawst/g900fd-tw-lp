.class Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;
.super Ljava/lang/Object;
.source "SamsungAccountBinder.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/server/SamsungAccountBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 97
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Account Service Connected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v1

    # setter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mISAService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v0, v1}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$102(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;

    .line 99
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    new-instance v1, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;

    iget-object v2, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    invoke-direct {v1, v2}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;-><init>(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)V

    # setter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mSACallback:Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;
    invoke-static {v0, v1}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$202(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;)Lcom/samsung/android/providers/context/server/SamsungAccountBinder$SACallback;

    .line 101
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    # invokes: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->registerCallback()Z
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$300(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 102
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Request access token"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    # invokes: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->requestAccessToken()V
    invoke-static {v0}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$400(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;)V

    .line 105
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 109
    # getter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Account Service DisConnected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, Lcom/samsung/android/providers/context/server/SamsungAccountBinder$1;->this$0:Lcom/samsung/android/providers/context/server/SamsungAccountBinder;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->mISAService:Lcom/msc/sa/aidl/ISAService;
    invoke-static {v0, v1}, Lcom/samsung/android/providers/context/server/SamsungAccountBinder;->access$102(Lcom/samsung/android/providers/context/server/SamsungAccountBinder;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;

    .line 111
    return-void
.end method
