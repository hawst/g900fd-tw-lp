.class public Lcom/samsung/android/providers/context/security/AesEncryption;
.super Ljava/lang/Object;
.source "AesEncryption.java"


# static fields
.field public static final LOGTAG:Ljava/lang/String;


# instance fields
.field private cipher:Ljavax/crypto/Cipher;

.field private final iv:[B

.field private final mSalt:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/samsung/android/providers/context/security/AesEncryption;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/providers/context/security/AesEncryption;->LOGTAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lcom/samsung/android/providers/context/security/AesEncryption;->generateIV()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/providers/context/security/AesEncryption;->iv:[B

    .line 53
    const-string v1, "_cf_salt"

    iput-object v1, p0, Lcom/samsung/android/providers/context/security/AesEncryption;->mSalt:Ljava/lang/String;

    .line 56
    :try_start_0
    const-string v1, "AES/CBC/PKCS7Padding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/providers/context/security/AesEncryption;->cipher:Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 62
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    iput-object v2, p0, Lcom/samsung/android/providers/context/security/AesEncryption;->cipher:Ljavax/crypto/Cipher;

    goto :goto_0

    .line 59
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 60
    .local v0, "e":Ljavax/crypto/NoSuchPaddingException;
    iput-object v2, p0, Lcom/samsung/android/providers/context/security/AesEncryption;->cipher:Ljavax/crypto/Cipher;

    goto :goto_0
.end method

.method private static generateIV()[B
    .locals 2

    .prologue
    .line 135
    const/16 v1, 0x80

    :try_start_0
    new-array v1, v1, [B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    return-object v1

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getSecretKey(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 8
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "salt"    # Ljava/lang/String;

    .prologue
    .line 67
    :try_start_0
    new-instance v2, Ljavax/crypto/spec/PBEKeySpec;

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-virtual {p2, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    const/16 v6, 0x64

    const/16 v7, 0x400

    invoke-direct {v2, v4, v5, v6, v7}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    .line 69
    .local v2, "pbeKeySpec":Ljavax/crypto/spec/PBEKeySpec;
    const-string v4, "PBKDF2WithHmacSHA1"

    invoke-static {v4}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v1

    .line 70
    .local v1, "factory":Ljavax/crypto/SecretKeyFactory;
    invoke-virtual {v1, v2}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 71
    .local v3, "tmp":Ljavax/crypto/SecretKey;
    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {v3}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v5

    const-string v6, "AES"

    invoke-direct {v4, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .end local v1    # "factory":Ljavax/crypto/SecretKeyFactory;
    .end local v2    # "pbeKeySpec":Ljavax/crypto/spec/PBEKeySpec;
    .end local v3    # "tmp":Ljavax/crypto/SecretKey;
    :goto_0
    return-object v4

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    goto :goto_0
.end method
