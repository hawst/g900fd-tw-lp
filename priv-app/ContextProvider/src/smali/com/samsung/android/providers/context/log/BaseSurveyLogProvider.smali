.class public Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;
.super Lcom/samsung/android/providers/context/log/BaseLogProvider;
.source "BaseSurveyLogProvider.java"


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V
    .locals 0
    .param p1, "staticProperties"    # Lcom/samsung/android/providers/context/log/LogTableProperty;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/providers/context/log/BaseLogProvider;-><init>(Lcom/samsung/android/providers/context/log/LogTableProperty;)V

    .line 44
    return-void
.end method

.method private static getSurveyDbHelper()Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    .locals 1

    .prologue
    .line 183
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/providers/context/ContextApplication;->getSurveyLogDatabase()Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

    move-result-object v0

    return-object v0
.end method

.method public static insertSurveyLog(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZII)Landroid/net/Uri;
    .locals 10
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "useNotify"    # Z
    .param p4, "recordsCountPerDeletion"    # I
    .param p5, "maxRecordsCount"    # I

    .prologue
    .line 188
    const-wide/16 v6, -0x1

    .line 189
    .local v6, "rowId":J
    const/4 v5, 0x0

    .line 191
    .local v5, "newUri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 193
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->getSurveyDbHelper()Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

    move-result-object v4

    .line 194
    .local v4, "helper":Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    if-nez v4, :cond_0

    .line 195
    const-string v8, "ContextFramework"

    const-string v9, "database not initialized properly"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const/4 v8, 0x0

    .line 226
    :goto_0
    return-object v8

    .line 199
    :cond_0
    invoke-virtual {v4}, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 200
    if-nez v0, :cond_1

    .line 201
    const-string v8, "ContextFramework"

    const-string v9, "database not initialized properly"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/4 v8, 0x0

    goto :goto_0

    .line 206
    :cond_1
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v0, p2, v8, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    .line 207
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_2

    .line 208
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "content://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p2}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->getProviderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 209
    if-eqz p3, :cond_2

    .line 210
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/providers/context/ContextApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v5, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    :cond_2
    :goto_1
    if-lez p4, :cond_3

    .line 220
    invoke-static {v0, p2}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->getTotalCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v2

    .line 221
    .local v2, "dbCount":J
    int-to-long v8, p5

    cmp-long v8, v8, v2

    if-gez v8, :cond_3

    .line 222
    invoke-static {v0, p2, p4}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->deleteOldRecords(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)I

    .end local v2    # "dbCount":J
    :cond_3
    move-object v8, v5

    .line 226
    goto :goto_0

    .line 213
    :catch_0
    move-exception v1

    .line 214
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 21
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 51
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->hasPermittedApp(Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 52
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_0

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v16

    .line 54
    .local v16, "caller":Ljava/lang/String;
    const-string v3, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SurveyLog insertion denied by privacy for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v16, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "]"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    .end local v16    # "caller":Ljava/lang/String;
    :cond_0
    const/16 v19, 0x0

    .line 107
    :goto_1
    return-object v19

    .line 54
    .restart local v16    # "caller":Ljava/lang/String;
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 62
    .end local v16    # "caller":Ljava/lang/String;
    :cond_2
    const/16 v19, 0x0

    .line 65
    .local v19, "newUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->getUriType(Landroid/net/Uri;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    .line 66
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_3

    .line 67
    const-string v2, "ContextFramework"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Called insert() with invalid URI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->Filter(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object p2

    .line 73
    if-nez p2, :cond_5

    .line 75
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v2, :cond_4

    .line 76
    const-string v2, "ContextFramework"

    const-string v3, "Filetered value is NULL."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_4
    const/16 v19, 0x0

    goto :goto_1

    .line 82
    :cond_5
    new-instance v17, Ljava/util/Date;

    invoke-direct/range {v17 .. v17}, Ljava/util/Date;-><init>()V

    .line 83
    .local v17, "currentTime":Ljava/util/Date;
    new-instance v20, Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->createLogTimeFormat()Ljava/text/DateFormat;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 85
    .local v20, "timestampUtc":Ljava/lang/String;
    const-string v2, "timestamp_utc"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v2, "time_zone"

    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/providers/context/GlobalStatus;->timeZone:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/log/LogTableProperty;->needToCheckMaxRecords()I

    move-result v6

    .line 90
    .local v6, "needToCheckMaxRecords":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v2, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    if-eqz v2, :cond_6

    .line 93
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v4, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v5, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget v7, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v7}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->insertSurveyLog(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZII)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    goto/16 :goto_1

    .line 95
    :catch_0
    move-exception v18

    .line 96
    .local v18, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto/16 :goto_1

    .line 100
    .end local v18    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_6
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/ContextApplication;->getHandler()Landroid/os/Handler;

    move-result-object v7

    .line 103
    .local v7, "logHandler":Landroid/os/Handler;
    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v11, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-boolean v12, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget v15, v2, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v14, v6

    invoke-static/range {v7 .. v15}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->insertLogWithHandler(Landroid/os/Handler;ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;ZZII)V

    goto/16 :goto_1
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v11

    .line 115
    .local v11, "callingPackage":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/providers/context/ContextApplication;->getInstance()Lcom/samsung/android/providers/context/ContextApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/providers/context/ContextApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 116
    invoke-static {}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->getInstance()Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;->PRIVACY_ITEM_TYPE_UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    invoke-virtual {v3, v11, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl;->isConsentNow(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 118
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v3, :cond_0

    .line 119
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "(Always) SurveyLog query for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not available"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_0
    const/4 v10, 0x0

    .line 179
    :goto_0
    return-object v10

    .line 126
    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->getUriType(Landroid/net/Uri;)I

    move-result v15

    .line 127
    .local v15, "type":I
    const/4 v3, 0x2

    if-ne v15, v3, :cond_4

    .line 128
    if-eqz p3, :cond_3

    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 139
    :cond_2
    :goto_1
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 140
    .local v1, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->mStaticProperties:Lcom/samsung/android/providers/context/log/LogTableProperty;

    iget-object v3, v3, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 142
    const/4 v6, 0x0

    .line 143
    .local v6, "groupby":Ljava/lang/String;
    const/4 v7, 0x0

    .line 144
    .local v7, "having":Ljava/lang/String;
    const/4 v10, 0x0

    .line 146
    .local v10, "c":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 149
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/providers/context/log/BaseSurveyLogProvider;->getSurveyDbHelper()Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;

    move-result-object v13

    .line 150
    .local v13, "helper":Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    if-nez v13, :cond_5

    .line 151
    const-string v3, "ContextFramework"

    const-string v4, "database not initialized properly"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 152
    const/4 v10, 0x0

    goto :goto_0

    .line 131
    .end local v1    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "groupby":Ljava/lang/String;
    .end local v7    # "having":Ljava/lang/String;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v13    # "helper":Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 133
    :cond_4
    const/4 v3, 0x1

    if-eq v15, v3, :cond_2

    .line 134
    invoke-static {}, Lcom/samsung/android/providers/context/GlobalStatus;->getInstance()Lcom/samsung/android/providers/context/GlobalStatus;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/providers/context/GlobalStatus;->logVisible:Z

    if-eqz v3, :cond_2

    .line 135
    const-string v3, "ContextFramework"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Called query() with invalid URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 155
    .restart local v1    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v6    # "groupby":Ljava/lang/String;
    .restart local v7    # "having":Ljava/lang/String;
    .restart local v10    # "c":Landroid/database/Cursor;
    .restart local v13    # "helper":Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    :cond_5
    :try_start_1
    invoke-virtual {v13}, Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 156
    if-nez v2, :cond_6

    .line 157
    const-string v3, "ContextFramework"

    const-string v4, "database not initialized properly"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 161
    :cond_6
    const/4 v9, 0x0

    .line 162
    .local v9, "limit":Ljava/lang/String;
    const-string v3, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v14

    .line 163
    .local v14, "limitParam":Ljava/lang/String;
    if-eqz v14, :cond_7

    .line 165
    :try_start_2
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 167
    .local v16, "userLimit":I
    const/4 v3, 0x1

    move/from16 v0, v16

    if-lt v0, v3, :cond_7

    .line 168
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v9

    .end local v16    # "userLimit":I
    :cond_7
    :goto_2
    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    .line 174
    :try_start_3
    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto/16 :goto_0

    .line 170
    :catch_0
    move-exception v12

    .line 171
    .local v12, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v12}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 175
    .end local v9    # "limit":Ljava/lang/String;
    .end local v12    # "e":Ljava/lang/NumberFormatException;
    .end local v13    # "helper":Lcom/samsung/android/providers/context/log/db/SurveyLogDbHelper;
    .end local v14    # "limitParam":Ljava/lang/String;
    :catch_1
    move-exception v12

    .line 176
    .local v12, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto/16 :goto_0
.end method
