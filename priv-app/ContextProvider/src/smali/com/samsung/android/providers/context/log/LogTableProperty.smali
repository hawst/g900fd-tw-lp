.class public final Lcom/samsung/android/providers/context/log/LogTableProperty;
.super Ljava/lang/Object;
.source "LogTableProperty.java"


# static fields
.field private static final EMPTY_FIELD_LIST:[Ljava/lang/String;


# instance fields
.field fillCommonColumnsManually:Z

.field insertImmediately:Z

.field private mInsertCount:I

.field maxRecordsCount:I

.field final privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

.field public recordLifeTime:I

.field recordsCountPerDeletion:I

.field supportUpdate:Z

.field public final tableName:Ljava/lang/String;

.field useBroadcast:Z

.field useNotify:Z

.field useUpdateBroadcast:Z

.field final validFieldList:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->EMPTY_FIELD_LIST:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    .line 44
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useBroadcast:Z

    .line 45
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useUpdateBroadcast:Z

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    .line 50
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->supportUpdate:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->fillCommonColumnsManually:Z

    .line 54
    const/16 v0, 0x1388

    iput v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    .line 57
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    .line 60
    const/16 v0, 0x1e

    iput v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordLifeTime:I

    .line 62
    iput v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->mInsertCount:I

    .line 65
    iput-object p1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 67
    sget-object v0, Lcom/samsung/android/providers/context/log/LogTableProperty;->EMPTY_FIELD_LIST:[Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->validFieldList:[Ljava/lang/String;

    .line 68
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;[Ljava/lang/String;)V
    .locals 2
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "privacyItemType"    # Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;
    .param p3, "validFieldList"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useNotify:Z

    .line 44
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useBroadcast:Z

    .line 45
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->useUpdateBroadcast:Z

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->insertImmediately:Z

    .line 50
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->supportUpdate:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->fillCommonColumnsManually:Z

    .line 54
    const/16 v0, 0x1388

    iput v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->maxRecordsCount:I

    .line 57
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    .line 60
    const/16 v0, 0x1e

    iput v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordLifeTime:I

    .line 62
    iput v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->mInsertCount:I

    .line 71
    iput-object p1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->privacyItemType:Lcom/samsung/android/providers/context/privacy/PrivacyAccessControl$PrivacyItemType;

    .line 73
    iput-object p3, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->validFieldList:[Ljava/lang/String;

    .line 74
    return-void
.end method

.method private declared-synchronized increaseInsertCount()I
    .locals 2

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->mInsertCount:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->mInsertCount:I

    .line 78
    iget v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->mInsertCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public loadInsertCount(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 82
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->mInsertCount:I

    .line 84
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 89
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iget v2, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    rem-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->mInsertCount:I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "ContextFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BaseLogProvider: failed to get record number for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->tableName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method needToCheckMaxRecords()I
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/samsung/android/providers/context/log/LogTableProperty;->increaseInsertCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 102
    iget v0, p0, Lcom/samsung/android/providers/context/log/LogTableProperty;->recordsCountPerDeletion:I

    .line 105
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
