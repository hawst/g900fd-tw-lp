.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Sleep"
.end annotation


# instance fields
.field public efficiency:D

.field public endTime:J

.field public startTime:J

.field public status:[I

.field public time:[J


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide v2, 0x7fffffffffffffffL

    .line 1757
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1760
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->startTime:J

    .line 1762
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->endTime:J

    .line 1764
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->efficiency:D

    .line 1766
    new-array v0, v5, [I

    const v1, 0x7fffffff

    aput v1, v0, v4

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->status:[I

    .line 1768
    new-array v0, v5, [J

    aput-wide v2, v0, v4

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->time:[J

    .line 1757
    return-void
.end method
