.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_CoachingResult"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2088
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 2099
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2039
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;-><init>()V

    .line 2041
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 2043
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;-><init>()V

    .line 2045
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->readFromParcel(Landroid/os/Parcel;)V

    .line 2046
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 2081
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->endTime:J

    .line 2082
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->distance:D

    .line 2083
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteTrainingLoadPeak:I

    .line 2084
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteMaxMET:I

    .line 2085
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteResourceRecovery:I

    .line 2086
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 2052
    const/4 v0, 0x0

    return v0
.end method

.method public getDataType()I
    .locals 1

    .prologue
    .line 2068
    const/16 v0, 0x11

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2074
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[COACHINGRESULT]\n endTime ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->endTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2075
    const-string v1, ", distance = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->distance:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eteTrainingLoadPeak ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteTrainingLoadPeak:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2076
    const-string v1, ", eteMaxMET = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteMaxMET:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eteResourceRecovery ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteResourceRecovery:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2074
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 2058
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->endTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2059
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->distance:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2060
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteTrainingLoadPeak:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2061
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteMaxMET:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2062
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;->eteResourceRecovery:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2063
    return-void
.end method
