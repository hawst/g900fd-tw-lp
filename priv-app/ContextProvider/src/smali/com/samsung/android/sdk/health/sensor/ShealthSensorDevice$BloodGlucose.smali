.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodGlucose"
.end annotation


# instance fields
.field public glucose:F

.field public glucoseUnit:I

.field public mealTime:J

.field public mealType:I

.field public mean:F

.field public sampleSource:I

.field public sampleType:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    const v2, 0x7fffffff

    .line 1516
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1520
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->sampleSource:I

    .line 1522
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->sampleType:I

    .line 1524
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->glucose:F

    .line 1526
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->glucoseUnit:I

    .line 1528
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->mealTime:J

    .line 1530
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->mealType:I

    .line 1532
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->mean:F

    .line 1516
    return-void
.end method
