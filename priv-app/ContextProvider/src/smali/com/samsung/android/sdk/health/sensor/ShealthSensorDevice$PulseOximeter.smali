.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PulseOximeter"
.end annotation


# instance fields
.field public heartRate:I

.field public saturationOfOxygen:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1725
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1728
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->heartRate:I

    .line 1731
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->saturationOfOxygen:F

    .line 1725
    return-void
.end method
