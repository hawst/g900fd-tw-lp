.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Temperature"
.end annotation


# instance fields
.field public accuracy:I

.field public temperature:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1747
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1749
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->accuracy:I

    .line 1751
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->temperature:F

    .line 1747
    return-void
.end method
