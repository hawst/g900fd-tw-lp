.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Pedometer"
.end annotation


# instance fields
.field public cadence:F

.field public calories:F

.field public caloriesUnit:I

.field public distance:F

.field public distanceUnit:I

.field public runStep:I

.field public samplePosition:I

.field public speed:F

.field public speedUnit:I

.field public stepType:I

.field public totalStep:I

.field public updownStep:I

.field public walkStep:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    const v1, 0x7fffffff

    .line 1546
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1568
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->distance:F

    .line 1570
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->distanceUnit:I

    .line 1572
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->calories:F

    .line 1574
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->caloriesUnit:I

    .line 1576
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->speed:F

    .line 1578
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->speedUnit:I

    .line 1580
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->stepType:I

    .line 1582
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->totalStep:I

    .line 1584
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->runStep:I

    .line 1586
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->walkStep:I

    .line 1588
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->updownStep:I

    .line 1590
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->samplePosition:I

    .line 1592
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->cadence:F

    .line 1546
    return-void
.end method
