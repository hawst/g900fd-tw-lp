.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BodyTemperature"
.end annotation


# instance fields
.field public temperature:F

.field public temperatureType:I

.field public temperatureUnit:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    .line 1616
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1619
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;->temperature:F

    .line 1622
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;->temperatureUnit:I

    .line 1625
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;->temperatureType:I

    .line 1616
    return-void
.end method
