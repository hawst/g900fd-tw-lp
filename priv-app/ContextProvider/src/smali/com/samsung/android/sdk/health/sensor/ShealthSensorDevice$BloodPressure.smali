.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodPressure"
.end annotation


# instance fields
.field public diastolic:F

.field public mean:F

.field public pulse:I

.field public systolic:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 1502
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1504
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;->systolic:F

    .line 1506
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;->diastolic:F

    .line 1508
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;->pulse:I

    .line 1510
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;->mean:F

    .line 1502
    return-void
.end method
