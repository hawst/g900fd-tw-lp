.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Coaching"
.end annotation


# instance fields
.field public ac:C

.field public lastTrainingLevelUpdate:J

.field public latestExerciseTime:J

.field public latestFeedbackPhraseNumber:I

.field public maxHeartRate:C

.field public maxMET:J

.field public previousToPreviousTrainingLevel:I

.field public previousTrainingLevel:I

.field public recourceRecovery:I

.field public startDate:J

.field public trainingLevel:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v1, 0xffff

    const-wide v2, 0x7fffffffffffffffL

    const v0, 0x7fffffff

    .line 1782
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1784
    iput-char v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->ac:C

    .line 1786
    iput-char v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->maxHeartRate:C

    .line 1788
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->maxMET:J

    .line 1790
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->recourceRecovery:I

    .line 1792
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->startDate:J

    .line 1794
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->trainingLevel:I

    .line 1796
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->lastTrainingLevelUpdate:J

    .line 1798
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->previousTrainingLevel:I

    .line 1800
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->previousToPreviousTrainingLevel:I

    .line 1802
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->latestFeedbackPhraseNumber:I

    .line 1804
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->latestExerciseTime:J

    .line 1782
    return-void
.end method
