.class public Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;
.super Ljava/lang/Object;
.source "ShealthState.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;


# instance fields
.field private initialized:Z

.field private mDiscoveredDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->TAG:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    .line 13
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;
    .locals 2

    .prologue
    .line 35
    const-class v1, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->instance:Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->instance:Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    .line 37
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->instance:Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public isInitialized()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->initialized:Z

    return v0
.end method
