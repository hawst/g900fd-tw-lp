.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_BodyTemperature"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1436
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1447
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1409
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;-><init>()V

    .line 1411
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1413
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;-><init>()V

    .line 1415
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->readFromParcel(Landroid/os/Parcel;)V

    .line 1416
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1451
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->time:J

    .line 1452
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperature:F

    .line 1453
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureUnit:I

    .line 1454
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureType:I

    .line 1455
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1421
    const/4 v0, 0x0

    return v0
.end method

.method public getDataType()I
    .locals 1

    .prologue
    .line 1460
    const/4 v0, 0x7

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1427
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1428
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperature:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1429
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureUnit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1430
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1431
    return-void
.end method
