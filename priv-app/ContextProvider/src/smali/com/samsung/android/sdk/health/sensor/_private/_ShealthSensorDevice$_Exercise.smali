.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_Exercise"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2179
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 2190
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2104
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;-><init>()V

    .line 2106
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 2108
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;-><init>()V

    .line 2110
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->readFromParcel(Landroid/os/Parcel;)V

    .line 2111
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 2164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    .line 2165
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    .line 2166
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    .line 2167
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    .line 2168
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    .line 2169
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->inclineDistance:D

    .line 2170
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->declineDistance:D

    .line 2171
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    .line 2172
    const-class v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    .line 2173
    const-class v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 2174
    .local v1, "temp":[Landroid/os/Parcelable;
    array-length v3, v1

    new-array v2, v3, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 2175
    .local v2, "tmp":[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-lt v0, v3, :cond_0

    .line 2176
    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 2177
    return-void

    .line 2175
    :cond_0
    aget-object v3, v1, v0

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 2116
    const/4 v0, 0x0

    return v0
.end method

.method public getDataType()I
    .locals 1

    .prologue
    .line 2140
    const/16 v0, 0xd

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2145
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2147
    .local v1, "loctionRes":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    if-eqz v2, :cond_0

    .line 2148
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 2154
    .end local v0    # "i":I
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[EXERCISE] type="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " time="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2155
    const-string v3, ", calorie ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", heartRate = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2156
    const-string v3, ", distance ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", inclineDistance ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->inclineDistance:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2157
    const-string v3, ", declineDistance ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->declineDistance:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", fitnessLevel = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2158
    iget v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", coachingResult ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2159
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", GPS = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2154
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 2149
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    .line 2150
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 2122
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2123
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 2124
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2125
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2126
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2127
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->inclineDistance:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2128
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->declineDistance:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2129
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2130
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2131
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    array-length v2, v2

    new-array v1, v2, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    .line 2132
    .local v1, "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 2134
    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 2135
    return-void

    .line 2133
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v2, v2, v0

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    aput-object v2, v1, v0

    .line 2132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
