.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Fitness"
.end annotation


# instance fields
.field public cadence:J

.field public cadencePerMinute:F

.field public calorieBurnRate:F

.field public cumulativeAltitudeGain:D

.field public cumulativeAltitudeLoss:D

.field public cumulativeCalories:F

.field public cumulativeCaloriesUnit:I

.field public cumulativeDistance:F

.field public cumulativeDistanceUnit:I

.field public cycleLength:F

.field public duration:J

.field public equipmentType:I

.field public inclinePercent:F

.field public instantaneousHeartRate:I

.field public instantaneousSpeed:F

.field public intantaneousMet:F

.field public maxCadence:J

.field public maxHeartRate:I

.field public maxSpeed:F

.field public meanCadence:F

.field public meanHeartRate:F

.field public meanSpeed:F

.field public power:F

.field public resistanceLevel:I

.field public speedUnit:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7fffffff

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 1631
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1634
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->equipmentType:I

    .line 1637
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->duration:J

    .line 1640
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeDistance:F

    .line 1644
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeDistanceUnit:I

    .line 1647
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->instantaneousSpeed:F

    .line 1650
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->speedUnit:I

    .line 1653
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->instantaneousHeartRate:I

    .line 1656
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cycleLength:F

    .line 1659
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->inclinePercent:F

    .line 1662
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->resistanceLevel:I

    .line 1665
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->intantaneousMet:F

    .line 1668
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->calorieBurnRate:F

    .line 1671
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeCalories:F

    .line 1674
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeCaloriesUnit:I

    .line 1677
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cadencePerMinute:F

    .line 1680
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cadence:J

    .line 1683
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeAltitudeGain:D

    .line 1686
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeAltitudeLoss:D

    .line 1689
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->meanSpeed:F

    .line 1692
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->maxSpeed:F

    .line 1695
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->meanHeartRate:F

    .line 1698
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->maxHeartRate:I

    .line 1701
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->meanCadence:F

    .line 1704
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->maxCadence:J

    .line 1707
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->power:F

    .line 1631
    return-void
.end method
