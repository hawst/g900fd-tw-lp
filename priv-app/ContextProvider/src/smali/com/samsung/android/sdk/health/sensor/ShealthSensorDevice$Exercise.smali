.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Exercise"
.end annotation


# instance fields
.field public calorie:D

.field public coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

.field public declineDistance:D

.field public distance:D

.field public duration:J

.field public fitnessLevel:I

.field public heartRate:D

.field public inclineDistance:D

.field public location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1827
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1830
    const/16 v0, 0x2712

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->type:I

    .line 1832
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->duration:J

    .line 1834
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->calorie:D

    .line 1836
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->heartRate:D

    .line 1838
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->distance:D

    .line 1840
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->inclineDistance:D

    .line 1842
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->declineDistance:D

    .line 1844
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->fitnessLevel:I

    .line 1846
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    .line 1848
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 1827
    return-void
.end method
