.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_DefaultHealth;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Profile;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Temperature;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private connectivityType:I

.field private dataTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private deviceId:Ljava/lang/String;

.field private deviceName:Ljava/lang/String;

.field private deviceSubType:I

.field private deviceType:I

.field private devices:Landroid/os/Bundle;

.field private manufacturer:Ljava/lang/String;

.field private originalDeviceName:Ljava/lang/String;

.field private protocol:Ljava/lang/String;

.field private serialNumber:Ljava/lang/String;

.field private serviceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 835
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 154
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 131
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->readFromParcel(Landroid/os/Parcel;)V

    .line 132
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->manufacturer:Ljava/lang/String;

    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->serialNumber:Ljava/lang/String;

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceSubType:I

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    .line 146
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    const-class v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 147
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public getServiceBinder()Lcom/samsung/android/sdk/health/sensor/_SensorService;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->serviceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->manufacturer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->serialNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceSubType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 106
    return-void
.end method
