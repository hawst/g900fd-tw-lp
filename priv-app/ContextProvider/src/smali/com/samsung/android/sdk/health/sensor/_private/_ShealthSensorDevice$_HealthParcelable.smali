.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;
.super Ljava/lang/Object;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_HealthParcelable"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private _data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 881
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 892
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 854
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->readFromParcel(Landroid/os/Parcel;)V

    .line 855
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 896
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 897
    .local v0, "dataType":I
    packed-switch v0, :pswitch_data_0

    .line 972
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_DefaultHealth;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_DefaultHealth;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .line 975
    :goto_0
    return-void

    .line 900
    :pswitch_0
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 904
    :pswitch_1
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 908
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 912
    :pswitch_3
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 916
    :pswitch_4
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 920
    :pswitch_5
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 924
    :pswitch_6
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 928
    :pswitch_7
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 932
    :pswitch_8
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 936
    :pswitch_9
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 940
    :pswitch_a
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 944
    :pswitch_b
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Temperature;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Temperature;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 948
    :pswitch_c
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 952
    :pswitch_d
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 956
    :pswitch_e
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 960
    :pswitch_f
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 964
    :pswitch_10
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto/16 :goto_0

    .line 968
    :pswitch_11
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Profile;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Profile;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto/16 :goto_0

    .line 897
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 865
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 871
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    if-eqz v0, :cond_0

    .line 873
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 874
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->writeToParcel(Landroid/os/Parcel;I)V

    .line 876
    :cond_0
    return-void
.end method
