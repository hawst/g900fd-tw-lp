.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Profile"
.end annotation


# instance fields
.field public activityClass:I

.field public age:I

.field public distanceUnit:I

.field public gender:I

.field public height:F

.field public heightUnit:I

.field public weight:F

.field public weightUnit:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const v0, 0x7fffffff

    .line 1854
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1856
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->height:F

    .line 1858
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->weight:F

    .line 1860
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->gender:I

    .line 1862
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->age:I

    .line 1864
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->distanceUnit:I

    .line 1866
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->weightUnit:I

    .line 1868
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->heightUnit:I

    .line 1870
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->activityClass:I

    .line 1854
    return-void
.end method
