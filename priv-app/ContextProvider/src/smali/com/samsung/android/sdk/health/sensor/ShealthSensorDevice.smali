.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Electrocardiogram;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Stress;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private _device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private isCanceled:Z

.field private mCallBackHandler:Landroid/os/Handler;

.field private final mDataCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;

.field private mExerciseId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    .line 1433
    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    .line 1038
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mDataCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;

    .line 631
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 633
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 635
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isCanceled:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)J
    .locals 2

    .prologue
    .line 1032
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mExerciseId:J

    return-wide v0
.end method
