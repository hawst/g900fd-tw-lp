.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Weight"
.end annotation


# instance fields
.field public activityMetabolicRate:F

.field public bodyAge:I

.field public bodyFat:F

.field public bodyFatUnit:I

.field public bodyMassIndex:F

.field public bodyMetabolicRate:F

.field public bodyWater:F

.field public bodyWaterUnit:I

.field public boneMass:F

.field public boneMassUnit:I

.field public height:F

.field public heightUnit:I

.field public muscleMass:F

.field public muscleMassUnit:I

.field public skeletalMuscle:F

.field public skeletalMuscleUnit:I

.field public visceralFat:F

.field public visceralFatUnit:I

.field public weight:F

.field public weightUnit:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 1456
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1458
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->weight:F

    .line 1460
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->weightUnit:I

    .line 1462
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->height:F

    .line 1464
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->heightUnit:I

    .line 1466
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyFat:F

    .line 1468
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyFatUnit:I

    .line 1470
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->visceralFat:F

    .line 1472
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->visceralFatUnit:I

    .line 1474
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->skeletalMuscle:F

    .line 1476
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->skeletalMuscleUnit:I

    .line 1478
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyWater:F

    .line 1480
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyWaterUnit:I

    .line 1482
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->muscleMass:F

    .line 1484
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->muscleMassUnit:I

    .line 1486
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->boneMass:F

    .line 1488
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->boneMassUnit:I

    .line 1490
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyMassIndex:F

    .line 1492
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyMetabolicRate:F

    .line 1494
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->activityMetabolicRate:F

    .line 1496
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyAge:I

    .line 1456
    return-void
.end method
