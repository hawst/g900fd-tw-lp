.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Response"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private commandId:Ljava/lang/String;

.field private device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private errorCode:I

.field private errorDescription:Ljava/lang/String;

.field private event:I

.field private response:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 678
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 689
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 694
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;-><init>()V

    .line 696
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 714
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;-><init>()V

    .line 716
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->readFromParcel(Landroid/os/Parcel;)V

    .line 717
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 721
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorCode:I

    .line 722
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorDescription:Ljava/lang/String;

    .line 723
    const-class v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    .line 724
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->commandId:Ljava/lang/String;

    .line 725
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->event:I

    .line 726
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    .line 727
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 730
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    const-class v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 733
    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 655
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 667
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 669
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 670
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->commandId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 671
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->event:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 672
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 673
    return-void
.end method
