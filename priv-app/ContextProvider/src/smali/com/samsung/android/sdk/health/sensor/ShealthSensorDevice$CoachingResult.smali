.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CoachingResult"
.end annotation


# instance fields
.field public distance:D

.field public endTime:J

.field public eteMaxMET:I

.field public eteResourceRecovery:I

.field public eteTrainingLoadPeak:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7fffffff

    .line 1810
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 1812
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->endTime:J

    .line 1814
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->distance:D

    .line 1816
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteTrainingLoadPeak:I

    .line 1818
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteMaxMET:I

    .line 1820
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteResourceRecovery:I

    .line 1810
    return-void
.end method
