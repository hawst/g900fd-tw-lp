.class public Lcom/android/phasebeam/ScriptField_VpConsts;
.super Landroid/renderscript/Script$FieldBase;
.source "ScriptField_VpConsts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phasebeam/ScriptField_VpConsts$Item;
    }
.end annotation


# static fields
.field private static mElementCache:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/renderscript/Element;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIOBuffer:Landroid/renderscript/FieldPacker;

.field private mItemArray:[Lcom/android/phasebeam/ScriptField_VpConsts$Item;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/phasebeam/ScriptField_VpConsts;->mElementCache:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScript;II)V
    .locals 1
    .param p1, "rs"    # Landroid/renderscript/RenderScript;
    .param p2, "count"    # I
    .param p3, "usages"    # I

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Landroid/renderscript/Script$FieldBase;-><init>()V

    .line 67
    iput-object v0, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mItemArray:[Lcom/android/phasebeam/ScriptField_VpConsts$Item;

    .line 68
    iput-object v0, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    .line 69
    invoke-static {p1}, Lcom/android/phasebeam/ScriptField_VpConsts;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mElement:Landroid/renderscript/Element;

    .line 70
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/phasebeam/ScriptField_VpConsts;->init(Landroid/renderscript/RenderScript;II)V

    .line 71
    return-void
.end method

.method private copyToArray(Lcom/android/phasebeam/ScriptField_VpConsts$Item;I)V
    .locals 3
    .param p1, "i"    # Lcom/android/phasebeam/ScriptField_VpConsts$Item;
    .param p2, "index"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v0, :cond_0

    new-instance v0, Landroid/renderscript/FieldPacker;

    iget-object v1, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mElement:Landroid/renderscript/Element;

    invoke-virtual {v1}, Landroid/renderscript/Element;->getBytesSize()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/phasebeam/ScriptField_VpConsts;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v0, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    iget-object v1, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mElement:Landroid/renderscript/Element;

    invoke-virtual {v1}, Landroid/renderscript/Element;->getBytesSize()I

    move-result v1

    mul-int/2addr v1, p2

    invoke-virtual {v0, v1}, Landroid/renderscript/FieldPacker;->reset(I)V

    .line 120
    iget-object v0, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-direct {p0, p1, v0}, Lcom/android/phasebeam/ScriptField_VpConsts;->copyToArrayLocal(Lcom/android/phasebeam/ScriptField_VpConsts$Item;Landroid/renderscript/FieldPacker;)V

    .line 121
    return-void
.end method

.method private copyToArrayLocal(Lcom/android/phasebeam/ScriptField_VpConsts$Item;Landroid/renderscript/FieldPacker;)V
    .locals 1
    .param p1, "i"    # Lcom/android/phasebeam/ScriptField_VpConsts$Item;
    .param p2, "fp"    # Landroid/renderscript/FieldPacker;

    .prologue
    .line 113
    iget-object v0, p1, Lcom/android/phasebeam/ScriptField_VpConsts$Item;->MVP:Landroid/renderscript/Matrix4f;

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addMatrix(Landroid/renderscript/Matrix4f;)V

    .line 114
    iget v0, p1, Lcom/android/phasebeam/ScriptField_VpConsts$Item;->scaleSize:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    .line 115
    return-void
.end method

.method public static createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .locals 3
    .param p0, "rs"    # Landroid/renderscript/RenderScript;

    .prologue
    .line 47
    new-instance v0, Landroid/renderscript/Element$Builder;

    invoke-direct {v0, p0}, Landroid/renderscript/Element$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    .line 48
    .local v0, "eb":Landroid/renderscript/Element$Builder;
    invoke-static {p0}, Landroid/renderscript/Element;->MATRIX_4X4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "MVP"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    .line 49
    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "scaleSize"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    .line 50
    invoke-virtual {v0}, Landroid/renderscript/Element$Builder;->create()Landroid/renderscript/Element;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public declared-synchronized set(Lcom/android/phasebeam/ScriptField_VpConsts$Item;IZ)V
    .locals 2
    .param p1, "i"    # Lcom/android/phasebeam/ScriptField_VpConsts$Item;
    .param p2, "index"    # I
    .param p3, "copyNow"    # Z

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mItemArray:[Lcom/android/phasebeam/ScriptField_VpConsts$Item;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/phasebeam/ScriptField_VpConsts;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/phasebeam/ScriptField_VpConsts$Item;

    iput-object v1, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mItemArray:[Lcom/android/phasebeam/ScriptField_VpConsts$Item;

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mItemArray:[Lcom/android/phasebeam/ScriptField_VpConsts$Item;

    aput-object p1, v1, p2

    .line 126
    if-eqz p3, :cond_1

    .line 127
    invoke-direct {p0, p1, p2}, Lcom/android/phasebeam/ScriptField_VpConsts;->copyToArray(Lcom/android/phasebeam/ScriptField_VpConsts$Item;I)V

    .line 128
    new-instance v0, Landroid/renderscript/FieldPacker;

    iget-object v1, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mElement:Landroid/renderscript/Element;

    invoke-virtual {v1}, Landroid/renderscript/Element;->getBytesSize()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    .line 129
    .local v0, "fp":Landroid/renderscript/FieldPacker;
    invoke-direct {p0, p1, v0}, Lcom/android/phasebeam/ScriptField_VpConsts;->copyToArrayLocal(Lcom/android/phasebeam/ScriptField_VpConsts$Item;Landroid/renderscript/FieldPacker;)V

    .line 130
    iget-object v1, p0, Lcom/android/phasebeam/ScriptField_VpConsts;->mAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v1, p2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    .end local v0    # "fp":Landroid/renderscript/FieldPacker;
    :cond_1
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
