.class public Lcom/android/phasebeam/ScriptField_Particle;
.super Landroid/renderscript/Script$FieldBase;
.source "ScriptField_Particle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phasebeam/ScriptField_Particle$Item;
    }
.end annotation


# static fields
.field private static mElementCache:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/renderscript/Element;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIOBuffer:Landroid/renderscript/FieldPacker;

.field private mItemArray:[Lcom/android/phasebeam/ScriptField_Particle$Item;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/phasebeam/ScriptField_Particle;->mElementCache:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScript;II)V
    .locals 1
    .param p1, "rs"    # Landroid/renderscript/RenderScript;
    .param p2, "count"    # I
    .param p3, "usages"    # I

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Landroid/renderscript/Script$FieldBase;-><init>()V

    .line 67
    iput-object v0, p0, Lcom/android/phasebeam/ScriptField_Particle;->mItemArray:[Lcom/android/phasebeam/ScriptField_Particle$Item;

    .line 68
    iput-object v0, p0, Lcom/android/phasebeam/ScriptField_Particle;->mIOBuffer:Landroid/renderscript/FieldPacker;

    .line 69
    invoke-static {p1}, Lcom/android/phasebeam/ScriptField_Particle;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phasebeam/ScriptField_Particle;->mElement:Landroid/renderscript/Element;

    .line 70
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/phasebeam/ScriptField_Particle;->init(Landroid/renderscript/RenderScript;II)V

    .line 71
    return-void
.end method

.method public static createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .locals 3
    .param p0, "rs"    # Landroid/renderscript/RenderScript;

    .prologue
    .line 47
    new-instance v0, Landroid/renderscript/Element$Builder;

    invoke-direct {v0, p0}, Landroid/renderscript/Element$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    .line 48
    .local v0, "eb":Landroid/renderscript/Element$Builder;
    invoke-static {p0}, Landroid/renderscript/Element;->F32_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "position"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    .line 49
    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "offsetX"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    .line 50
    invoke-virtual {v0}, Landroid/renderscript/Element$Builder;->create()Landroid/renderscript/Element;

    move-result-object v1

    return-object v1
.end method
