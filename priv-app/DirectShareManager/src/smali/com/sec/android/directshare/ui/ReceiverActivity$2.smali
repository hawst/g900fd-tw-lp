.class Lcom/sec/android/directshare/ui/ReceiverActivity$2;
.super Ljava/lang/Object;
.source "ReceiverActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/directshare/ui/ReceiverActivity;->initUi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

.field final synthetic val$btnCancel:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/ui/ReceiverActivity;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity$2;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    iput-object p2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity$2;->val$btnCancel:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v1, 0x42

    .line 232
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 249
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 234
    :pswitch_0
    if-ne p2, v1, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity$2;->val$btnCancel:Landroid/widget/Button;

    const v1, 0x7f020012

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 239
    :cond_1
    :pswitch_1
    if-ne p2, v1, :cond_0

    .line 240
    const/4 v0, -0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static {v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 241
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity$2;->val$btnCancel:Landroid/widget/Button;

    const v1, 0x7f020010

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity$2;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->stopThread()V
    invoke-static {v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$100(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity$2;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->finish()V

    .line 244
    const-string v0, "[SBeam]"

    const-string v1, "ReceiverActivity: ----> finish..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    const/4 v0, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z
    invoke-static {v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$202(Z)Z

    goto :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
