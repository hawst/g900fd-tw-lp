.class public Lcom/sec/android/directshare/DirectShareService;
.super Landroid/app/Service;
.source "DirectShareService.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;
.implements Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/directshare/DirectShareService$FileDownThread;,
        Lcom/sec/android/directshare/DirectShareService$ControlThread;,
        Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;,
        Lcom/sec/android/directshare/DirectShareService$WorkerThread;,
        Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;,
        Lcom/sec/android/directshare/DirectShareService$DownloadListener;,
        Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;,
        Lcom/sec/android/directshare/DirectShareService$DownloadInfo;,
        Lcom/sec/android/directshare/DirectShareService$DownloadFile;,
        Lcom/sec/android/directshare/DirectShareService$HouseKeeper;,
        Lcom/sec/android/directshare/DirectShareService$DoAction;
    }
.end annotation


# static fields
.field private static mNfcState:I

.field private static mSocketList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/Socket;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final ACTION_DOWNLOAD:I

.field private final ACTION_START:I

.field private final ACTION_UNKNOWN:I

.field private final ACTION_WAIT:I

.field private final CONNECT_TIMEOUT:I

.field private final DISCOVER_PEERS_TIMEOUT:I

.field private final DO_DISCOVER_PEERS:I

.field private final DO_DOWNLOAD:I

.field private final DO_ENABLE_WIFI_DIRECT:I

.field private final DO_END_SESSION:I

.field private final DO_NFC_DISCONNECT:I

.field private final DO_REMOVE_GROUP:I

.field private final DO_REQUEST_GROUP_INFO:I

.field private final DO_REQUEST_NFC_CONNECT:I

.field private final DO_START_CONTROL_THREAD:I

.field private final DO_UNKNOWN:I

.field private final MODE_DOWNLOAD:I

.field private final MODE_UPLOAD:I

.field private final REQUEST_NFC_CONNECT_TIMEOUT:I

.field private final SAME_TOUCH_TIME:I

.field private final SOCKET_CONNECTION_TIMEOUT:I

.field private final SUB_PATH:Ljava/lang/String;

.field private final TEST:Z

.field private mAction:I

.field private mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mCheckWifiBroadcast:Z

.field private mClientList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectTime:J

.field private mConnectedMac:Ljava/lang/String;

.field private mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

.field private mCurrentMac:Ljava/lang/String;

.field private mDetailedState:Landroid/net/NetworkInfo$DetailedState;

.field private mDeviceMode:I

.field private mDeviceName:Ljava/lang/String;

.field private mDiscoverPeersTime:J

.field mDoActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/directshare/DirectShareService$DoAction;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadAddress:Ljava/lang/String;

.field private mDownloadCount:I

.field private mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

.field private mDownloadLock:Ljava/util/concurrent/Semaphore;

.field private mDownloadedCount:I

.field private mFirstNfcDisconnectTime:J

.field private mHandler:Landroid/os/Handler;

.field private mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

.field private mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

.field private mInitWifiP2pConnection:I

.field private mInitWifiP2pState:I

.field private mInitWifiTime:J

.field private mIsGroupOwner:Z

.field private mIsNfcStartAction:Z

.field private mIsSocketConnected:Z

.field private mIsStartAction:Z

.field private mLastAction:I

.field private mLastActionTime:J

.field private mLastDownloadTime:J

.field private mLastPercent:I

.field private mManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field mMaxCount:I

.field mMaxFileSize:J

.field private mMediaScanCount:I

.field private mNextDownloadStateTime:J

.field private mNextState:I

.field private mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

.field private mOwnerAddress:Ljava/lang/String;

.field private mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private mP2pSendCompleteTime:J

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRequestNfcConnectRetry:I

.field private mRequestNfcConnectTime:J

.field private mScanActionCount:I

.field private mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

.field private mSessionInfoForSender:Lcom/sec/android/directshare/utils/SessionInfo;

.field private mSnbFolderName:Ljava/lang/String;

.field private mStartTime:J

.field private mState:I

.field private mStateLock:Ljava/util/concurrent/Semaphore;

.field private mStateTime:J

.field mSumReadByte:J

.field private mThemeContext:Landroid/view/ContextThemeWrapper;

.field private mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

.field private mVideoFileName:Ljava/lang/String;

.field private mVideoUri:Landroid/net/Uri;

.field private mWaitActionTime:J

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWifiApState:I

.field private mWifiDirectConnect:Z

.field private mWifiDirectInit:Z

.field private mWifiDisplayCheck:Z

.field private mWifiHotspotCheck:Z

.field private mWifiP2pState:I

.field private threadName:Ljava/lang/String;

.field private updateDownloadCount:I

.field private updateDownloadFileName:Ljava/lang/String;

.field private updateDownloadPercent:I

.field private updateUploadCount:I

.field private updateUploadFileName:Ljava/lang/String;

.field private updateUploadPercent:I

.field private updateUploadTotalCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/directshare/DirectShareService;->mSocketList:Ljava/util/ArrayList;

    .line 338
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/16 v6, 0x4e20

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 132
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 134
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->TEST:Z

    .line 215
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 216
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 217
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 219
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiP2pState:I

    .line 220
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pState:I

    .line 221
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pConnection:I

    .line 224
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 225
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    .line 227
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerAddress:Ljava/lang/String;

    .line 228
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    .line 229
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;

    .line 230
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    .line 234
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    .line 235
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v3, v3}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;

    .line 236
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceMode:I

    .line 237
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I

    .line 238
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadedCount:I

    .line 239
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadAddress:Ljava/lang/String;

    .line 240
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I

    .line 241
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mScanActionCount:I

    .line 242
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mNextDownloadStateTime:J

    .line 243
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mLastPercent:I

    .line 244
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    .line 245
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    .line 246
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    .line 247
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mConnectedMac:Ljava/lang/String;

    .line 270
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    .line 271
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mNextState:I

    .line 272
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mStateTime:J

    .line 273
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v3, v3}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    .line 274
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_UNKNOWN:I

    .line 275
    const/16 v0, 0x1f5

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_REQUEST_NFC_CONNECT:I

    .line 276
    const/16 v0, 0x1f6

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_DISCOVER_PEERS:I

    .line 277
    const/16 v0, 0x1f7

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_ENABLE_WIFI_DIRECT:I

    .line 278
    const/16 v0, 0x1f8

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_REQUEST_GROUP_INFO:I

    .line 279
    const/16 v0, 0x1f9

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_REMOVE_GROUP:I

    .line 280
    const/16 v0, 0x1fa

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_END_SESSION:I

    .line 281
    const/16 v0, 0x1fb

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_DOWNLOAD:I

    .line 282
    const/16 v0, 0x1fc

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_NFC_DISCONNECT:I

    .line 283
    const/16 v0, 0x1fd

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->DO_START_CONTROL_THREAD:I

    .line 284
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->MODE_UPLOAD:I

    .line 285
    const/16 v0, 0x259

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->MODE_DOWNLOAD:I

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDoActionList:Ljava/util/ArrayList;

    .line 294
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->ACTION_UNKNOWN:I

    .line 295
    iput v3, p0, Lcom/sec/android/directshare/DirectShareService;->ACTION_START:I

    .line 296
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->ACTION_DOWNLOAD:I

    .line 297
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->ACTION_WAIT:I

    .line 298
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    .line 299
    iput v6, p0, Lcom/sec/android/directshare/DirectShareService;->CONNECT_TIMEOUT:I

    .line 300
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->REQUEST_NFC_CONNECT_TIMEOUT:I

    .line 301
    iput v6, p0, Lcom/sec/android/directshare/DirectShareService;->DISCOVER_PEERS_TIMEOUT:I

    .line 302
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->SAME_TOUCH_TIME:I

    .line 303
    const/16 v0, 0x7530

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->SOCKET_CONNECTION_TIMEOUT:I

    .line 308
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    .line 309
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    .line 310
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    .line 312
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadFileName:Ljava/lang/String;

    .line 313
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadPercent:I

    .line 314
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadCount:I

    .line 315
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadTotalCount:I

    .line 316
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadFileName:Ljava/lang/String;

    .line 317
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadPercent:I

    .line 318
    iput v3, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I

    .line 320
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    .line 321
    const-string v0, "threadName"

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->threadName:Ljava/lang/String;

    .line 322
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    .line 324
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiTime:J

    .line 325
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mStartTime:J

    .line 326
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mLastActionTime:J

    .line 327
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mLastAction:I

    .line 328
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mDiscoverPeersTime:J

    .line 329
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mConnectTime:J

    .line 330
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J

    .line 331
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mLastDownloadTime:J

    .line 332
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mFirstNfcDisconnectTime:J

    .line 333
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mP2pSendCompleteTime:J

    .line 334
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectRetry:I

    .line 339
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mIsStartAction:Z

    .line 340
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mIsNfcStartAction:Z

    .line 341
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectInit:Z

    .line 342
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    .line 343
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiApState:I

    .line 344
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDisplayCheck:Z

    .line 345
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiHotspotCheck:Z

    .line 346
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mIsSocketConnected:Z

    .line 347
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mCheckWifiBroadcast:Z

    .line 348
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectConnect:Z

    .line 349
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mWaitActionTime:J

    .line 350
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSnbFolderName:Ljava/lang/String;

    .line 351
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    .line 352
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfoForSender:Lcom/sec/android/directshare/utils/SessionInfo;

    .line 353
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mVideoFileName:Ljava/lang/String;

    .line 354
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mVideoUri:Landroid/net/Uri;

    .line 355
    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;

    .line 3910
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$11;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectShareService$11;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private DownLoadStartNotificationInit(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "bDownloadCount"    # Z

    .prologue
    .line 938
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    if-nez v0, :cond_0

    .line 942
    :goto_0
    return-void

    .line 940
    :cond_0
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService:  DownLoadStartNotificationInit ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/NotiManager;->addDownload()V

    goto :goto_0
.end method

.method private DownLoadstateNotification(Ljava/lang/String;J)V
    .locals 8
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "size"    # J

    .prologue
    .line 945
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    if-nez v0, :cond_0

    .line 950
    :goto_0
    return-void

    .line 947
    :cond_0
    const-wide/16 v0, 0x64

    mul-long/2addr v0, p2

    iget-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    div-long/2addr v0, v4

    long-to-int v3, v0

    .line 948
    .local v3, "progress":I
    iput v3, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadPercent:I

    .line 949
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    iget-wide v6, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    move-object v2, p1

    move-wide v4, p2

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/directshare/ui/NotiManager;->updateDownload(Ljava/lang/String;IJJ)V

    goto :goto_0
.end method

.method private DownLoadstopNotification()V
    .locals 2

    .prologue
    .line 953
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    if-nez v0, :cond_0

    .line 957
    :goto_0
    return-void

    .line 955
    :cond_0
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService:  DownLoadstopNotification"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/NotiManager;->cancelDownload()V

    goto :goto_0
.end method

.method private StartWebServer(Ljava/lang/String;)Z
    .locals 9
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 2636
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    if-nez v0, :cond_0

    .line 2637
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->fileScan()V

    .line 2638
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    const/16 v4, 0x3a98

    const-string v5, "/"

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;-><init>(Lcom/sec/android/directshare/DirectShareService;Landroid/app/Service;Ljava/lang/String;ILjava/lang/String;Lcom/sec/android/directshare/DirectShareService$DownloadListener;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    .line 2639
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->setDaemon(Z)V

    .line 2640
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v8

    .line 2646
    :goto_0
    return v0

    .line 2643
    :catch_0
    move-exception v7

    .line 2644
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: StartWebServer: errmsg=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2646
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private StopWebServer()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2650
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    if-eqz v0, :cond_0

    .line 2651
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->fileScan()V

    .line 2652
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->stopThread()V

    .line 2654
    :cond_0
    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mHttpServerThread:Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;

    .line 2655
    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfoForSender:Lcom/sec/android/directshare/utils/SessionInfo;

    .line 2656
    const/4 v0, 0x1

    return v0
.end method

.method private UpLoadStartNotification(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "fileCount"    # I

    .prologue
    .line 908
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    if-nez v0, :cond_0

    .line 913
    :goto_0
    return-void

    .line 910
    :cond_0
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: UpLoadStartNotification : file["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] dev["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " count ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/NotiManager;->addUpload()V

    goto :goto_0
.end method

.method private UpLoadstateNotification(I)V
    .locals 1
    .param p1, "progressCount"    # I

    .prologue
    .line 924
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    if-nez v0, :cond_0

    .line 928
    :goto_0
    return-void

    .line 926
    :cond_0
    iput p1, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadPercent:I

    .line 927
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/directshare/ui/NotiManager;->updateUploadProgress(I)V

    goto :goto_0
.end method

.method private UpLoadstateNotification(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "fileCount"    # I

    .prologue
    .line 916
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    if-nez v0, :cond_0

    .line 921
    :goto_0
    return-void

    .line 918
    :cond_0
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService:  UpLoadstateNotification : file["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " dev["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " count["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/directshare/ui/NotiManager;->updateUpload(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private UpLoadstopNotification()V
    .locals 2

    .prologue
    .line 931
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    if-nez v0, :cond_0

    .line 935
    :goto_0
    return-void

    .line 933
    :cond_0
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService:  UpLoadstopNotification "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/NotiManager;->cancelUpload()V

    goto :goto_0
.end method

.method private WfdDisconnect()V
    .locals 3

    .prologue
    .line 973
    const-string v1, "[SBeam]"

    const-string v2, "DirectShareService:  WfdDisconnect "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    const-string v1, "display"

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 976
    .local v0, "mDisplayManager":Landroid/hardware/display/DisplayManager;
    if-eqz v0, :cond_0

    .line 977
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 979
    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/directshare/DirectShareService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsStartAction:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/directshare/DirectShareService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsNfcStartAction:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mP2pSendCompleteTime:J

    return-wide v0
.end method

.method static synthetic access$1202(Lcom/sec/android/directshare/DirectShareService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # J

    .prologue
    .line 132
    iput-wide p1, p0, Lcom/sec/android/directshare/DirectShareService;->mP2pSendCompleteTime:J

    return-wide p1
.end method

.method static synthetic access$1300(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/directshare/DirectShareService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDisplayCheck:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/directshare/DirectShareService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiHotspotCheck:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$1602(Lcom/sec/android/directshare/DirectShareService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # J

    .prologue
    .line 132
    iput-wide p1, p0, Lcom/sec/android/directshare/DirectShareService;->mStartTime:J

    return-wide p1
.end method

.method static synthetic access$1700(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/NetworkInfo$DetailedState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/directshare/DirectShareService;Landroid/net/NetworkInfo$DetailedState;)Landroid/net/NetworkInfo$DetailedState;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Landroid/net/NetworkInfo$DetailedState;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/directshare/DirectShareService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/directshare/DirectShareService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J

    return-wide v0
.end method

.method static synthetic access$202(Lcom/sec/android/directshare/DirectShareService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiApState:I

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectRetry:I

    return v0
.end method

.method static synthetic access$2108(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectRetry:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectRetry:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/directshare/DirectShareService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestNfcConnect(Z)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_stopPeerDiscovery()V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/directshare/DirectShareService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateTime:J

    return-wide v0
.end method

.method static synthetic access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNextState:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->getWifiApState()I

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopControlThread()V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDiscoverPeersTime:J

    return-wide v0
.end method

.method static synthetic access$3400(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mConnectTime:J

    return-wide v0
.end method

.method static synthetic access$3500(Lcom/sec/android/directshare/DirectShareService;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/sec/android/directshare/DirectShareService;->auditLog(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastDownloadTime:J

    return-wide v0
.end method

.method static synthetic access$3800(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadAddress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->downFiles(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_init()V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_Listen()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiP2pState:I

    return v0
.end method

.method static synthetic access$4302(Lcom/sec/android/directshare/DirectShareService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiP2pState:I

    return p1
.end method

.method static synthetic access$4400(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestGroupInfo()V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/directshare/DirectShareService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->stopDisconnectUi(I)V

    return-void
.end method

.method static synthetic access$4600(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startControlThread()V

    return-void
.end method

.method static synthetic access$4800(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$ControlThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/directshare/DirectShareService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    return-void
.end method

.method static synthetic access$5000(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/sec/android/directshare/DirectShareService;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    return-object p1
.end method

.method static synthetic access$5100(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/directshare/DirectShareService;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->getDeviceStatus(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestPeer()V

    return-void
.end method

.method static synthetic access$5400(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5402(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerAddress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5500(Lcom/sec/android/directshare/DirectShareService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    return v0
.end method

.method static synthetic access$5502(Lcom/sec/android/directshare/DirectShareService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    return p1
.end method

.method static synthetic access$5600(Lcom/sec/android/directshare/DirectShareService;Landroid/net/NetworkInfo$DetailedState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Landroid/net/NetworkInfo$DetailedState;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->getDetailStateString(Landroid/net/NetworkInfo$DetailedState;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceMode:I

    return v0
.end method

.method static synthetic access$5800(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->StartWebServer(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5900()I
    .locals 1

    .prologue
    .line 132
    sget v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/directshare/DirectShareService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mCheckWifiBroadcast:Z

    return v0
.end method

.method static synthetic access$6000(Lcom/sec/android/directshare/DirectShareService;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/sec/android/directshare/DirectShareService;->setState(II)V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/directshare/DirectShareService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/sec/android/directshare/DirectShareService;->mCheckWifiBroadcast:Z

    return p1
.end method

.method static synthetic access$6100(Lcom/sec/android/directshare/DirectShareService;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->isExistDoAction(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$6200(Lcom/sec/android/directshare/DirectShareService;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6300()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/sec/android/directshare/DirectShareService;->mSocketList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6400(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/utils/SessionInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfoForSender:Lcom/sec/android/directshare/utils/SessionInfo;

    return-object v0
.end method

.method static synthetic access$6502(Lcom/sec/android/directshare/DirectShareService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Z

    .prologue
    .line 132
    iput-boolean p1, p0, Lcom/sec/android/directshare/DirectShareService;->mIsSocketConnected:Z

    return p1
.end method

.method static synthetic access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    return-object v0
.end method

.method static synthetic access$6800(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->sendMsgSendingToFail()V

    return-void
.end method

.method static synthetic access$6900(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->UpLoadstopNotification()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiTime:J

    return-wide v0
.end method

.method static synthetic access$7000(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->uploaded()V

    return-void
.end method

.method static synthetic access$7100(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->setDeviceName(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$7200(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/directshare/DirectShareService;->startUpload(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$7300(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mConnectedMac:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7302(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->mConnectedMac:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7400(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->getDeviceAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7500(Lcom/sec/android/directshare/DirectShareService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->updateUploadPrgress(I)V

    return-void
.end method

.method static synthetic access$7600(Lcom/sec/android/directshare/DirectShareService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->UpLoadstateNotification(I)V

    return-void
.end method

.method static synthetic access$7700(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadCount:I

    return v0
.end method

.method static synthetic access$7708(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadCount:I

    return v0
.end method

.method static synthetic access$7800(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadTotalCount:I

    return v0
.end method

.method static synthetic access$7900(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/directshare/DirectShareService;->updateUploadFileCount(Ljava/lang/String;II)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/directshare/DirectShareService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->getWifiOnOffState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8000(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8100(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->getSnbFolderName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8200(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadedCount:I

    return v0
.end method

.method static synthetic access$8208(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadedCount:I

    return v0
.end method

.method static synthetic access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I

    return v0
.end method

.method static synthetic access$8408(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I

    return v0
.end method

.method static synthetic access$8410(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I

    return v0
.end method

.method static synthetic access$8500(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->removeDownloadList(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8700(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Landroid/net/Uri;

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/directshare/DirectShareService;->downloadFileScanned(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$8800(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->subtitleFormat(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8900(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mVideoFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8902(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->mVideoFileName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/directshare/DirectShareService;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Lcom/sec/android/directshare/DirectShareService;->setWifiState(II)V

    return-void
.end method

.method static synthetic access$9000(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mVideoUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$9002(Lcom/sec/android/directshare/DirectShareService;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->mVideoUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$9100(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$FileDownThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    return-object v0
.end method

.method static synthetic access$9200(Lcom/sec/android/directshare/DirectShareService;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # J
    .param p3, "x2"    # J
    .param p5, "x3"    # Ljava/lang/String;
    .param p6, "x4"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/directshare/DirectShareService;->startDownload(JJLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$9300(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastPercent:I

    return v0
.end method

.method static synthetic access$9302(Lcom/sec/android/directshare/DirectShareService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/directshare/DirectShareService;->mLastPercent:I

    return p1
.end method

.method static synthetic access$9400(Lcom/sec/android/directshare/DirectShareService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNextDownloadStateTime:J

    return-wide v0
.end method

.method static synthetic access$9402(Lcom/sec/android/directshare/DirectShareService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # J

    .prologue
    .line 132
    iput-wide p1, p0, Lcom/sec/android/directshare/DirectShareService;->mNextDownloadStateTime:J

    return-wide p1
.end method

.method static synthetic access$9500(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I

    return v0
.end method

.method static synthetic access$9508(Lcom/sec/android/directshare/DirectShareService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I

    return v0
.end method

.method static synthetic access$9600(Lcom/sec/android/directshare/DirectShareService;JLjava/lang/String;II)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # I
    .param p5, "x4"    # I

    .prologue
    .line 132
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/directshare/DirectShareService;->updateDownloadProgress(JLjava/lang/String;II)V

    return-void
.end method

.method static synthetic access$9700(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/directshare/DirectShareService;->DownLoadstateNotification(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$9800(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->sendDownloadEndOnefile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$9900(Lcom/sec/android/directshare/DirectShareService;)Landroid/view/ContextThemeWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectShareService;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method

.method private addDownloadList()V
    .locals 6

    .prologue
    .line 1614
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 1615
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v4}, Lcom/sec/android/directshare/utils/SessionInfo;->getMacAddr()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mMac:Ljava/lang/String;

    .line 1616
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v4}, Lcom/sec/android/directshare/utils/SessionInfo;->getMimeType()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mimeType:Ljava/lang/String;

    .line 1617
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1618
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: addDownloadList: Mime=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v5, v5, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1619
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v3}, Lcom/sec/android/directshare/utils/SessionInfo;->getFileCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1620
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectShareService$DownloadFile;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1621
    .local v0, "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v3, v1}, Lcom/sec/android/directshare/utils/SessionInfo;->getFileInfo(I)Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;

    move-result-object v2

    .line 1622
    .local v2, "sFile":Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->getFileName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    .line 1623
    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->getFilePath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->subPath:Ljava/lang/String;

    .line 1624
    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->getFilelen()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->length:Ljava/lang/String;

    .line 1625
    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->isDownloading:Z

    .line 1626
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: addDownloadList: fileName=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1627
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1619
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1629
    .end local v0    # "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    .end local v2    # "sFile":Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v4, v4, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iput v4, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mTotalFile:I

    .line 1632
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 1633
    return-void
.end method

.method private auditLog(ZLjava/lang/String;)V
    .locals 6
    .param p1, "flag"    # Z
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 4120
    :try_start_0
    const-string v3, "content://com.sec.knox.provider/AuditLog"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 4121
    .local v1, "auditUri":Landroid/net/Uri;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4122
    .local v0, "auditCv":Landroid/content/ContentValues;
    const-string v3, "severity"

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4123
    const-string v3, "group"

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4124
    const-string v3, "outcome"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 4125
    const-string v3, "uid"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4126
    const-string v3, "component"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4127
    const-string v3, "message"

    invoke-virtual {v0, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4128
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4133
    .end local v0    # "auditCv":Landroid/content/ContentValues;
    .end local v1    # "auditUri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 4129
    :catch_0
    move-exception v2

    .line 4130
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: auditLog: errmsg=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private doAction(IJ)V
    .locals 4
    .param p1, "action"    # I
    .param p2, "delay"    # J

    .prologue
    .line 1747
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$DoAction;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectShareService$DoAction;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1748
    .local v0, "da":Lcom/sec/android/directshare/DirectShareService$DoAction;
    # setter for: Lcom/sec/android/directshare/DirectShareService$DoAction;->mDoAction:I
    invoke-static {v0, p1}, Lcom/sec/android/directshare/DirectShareService$DoAction;->access$4002(Lcom/sec/android/directshare/DirectShareService$DoAction;I)I

    .line 1749
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p2

    # setter for: Lcom/sec/android/directshare/DirectShareService$DoAction;->mDoActionTime:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService$DoAction;->access$4102(Lcom/sec/android/directshare/DirectShareService$DoAction;J)J

    .line 1750
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mDoActionList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1751
    return-void
.end method

.method private downFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V
    .locals 10
    .param p1, "serverIp"    # Ljava/lang/String;
    .param p2, "subPath"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "total"    # J
    .param p6, "mimeType"    # Ljava/lang/String;
    .param p7, "maxfilesize"    # J

    .prologue
    .line 1411
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;-><init>(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    .line 1412
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->threadName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->setName(Ljava/lang/String;)V

    .line 1413
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->setFlag(Z)V

    .line 1414
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->start()V

    .line 1415
    return-void
.end method

.method private downFiles(Ljava/lang/String;)V
    .locals 11
    .param p1, "serverAddress"    # Ljava/lang/String;

    .prologue
    .line 1389
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 1390
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v10, v0, :cond_0

    .line 1391
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/directshare/DirectShareService$DownloadFile;

    .line 1392
    .local v9, "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    iget-boolean v0, v9, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->isDownloading:Z

    if-nez v0, :cond_1

    .line 1393
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: downFiles: i=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], fileName=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], length=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->length:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], mDownloadCount=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1396
    iget-object v0, v9, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->length:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1397
    .local v4, "length":J
    iget-object v2, v9, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->subPath:Ljava/lang/String;

    iget-object v3, v9, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mimeType:Ljava/lang/String;

    iget-wide v7, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/directshare/DirectShareService;->downFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    .line 1399
    const/4 v0, 0x1

    iput-boolean v0, v9, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->isDownloading:Z

    .line 1400
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v0, v10, v9}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1401
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I

    .line 1402
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastDownloadTime:J

    .line 1406
    .end local v4    # "length":J
    .end local v9    # "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 1407
    return-void

    .line 1390
    .restart local v9    # "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0
.end method

.method private downloadFileScanned(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;
    .param p4, "uri"    # Landroid/net/Uri;

    .prologue
    .line 4243
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->mScanActionCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/directshare/DirectShareService;->mScanActionCount:I

    .line 4244
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->mScanActionCount:I

    iget v4, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    if-lt v3, v4, :cond_0

    .line 4245
    new-instance v1, Lcom/sec/android/directshare/utils/Launcher;

    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-direct {v1, v3, p2, p4, p1}, Lcom/sec/android/directshare/utils/Launcher;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 4246
    .local v1, "launcher":Lcom/sec/android/directshare/utils/Launcher;
    invoke-virtual {v1}, Lcom/sec/android/directshare/utils/Launcher;->build()Landroid/content/Intent;

    move-result-object v2

    .line 4247
    .local v2, "viewIntent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v3, v2, p3}, Lcom/sec/android/directshare/ui/NotiManager;->addDownloaded(Landroid/content/Intent;Ljava/lang/String;)V

    .line 4249
    .end local v1    # "launcher":Lcom/sec/android/directshare/utils/Launcher;
    .end local v2    # "viewIntent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.android.directshare.DOWNLOAD_SCANNER_ACTION"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4250
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "com.sec.android.directshare"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4251
    const-string v3, "filepath"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4252
    const-string v3, "mimetype"

    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4253
    const-string v3, "device_name"

    invoke-virtual {v0, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4254
    const-string v3, "uri"

    invoke-virtual {v0, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4255
    const-string v3, "scancount"

    iget v4, p0, Lcom/sec/android/directshare/DirectShareService;->mScanActionCount:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4256
    const-string v3, "totalCount"

    iget v4, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4257
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4258
    return-void
.end method

.method private downloaded(Ljava/lang/String;II)V
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "count"    # I
    .param p3, "totalCount"    # I

    .prologue
    .line 4234
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_COMPLETED_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4235
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4236
    const-string v1, "filename"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4237
    const-string v1, "count"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4238
    const-string v1, "totalCount"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4239
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4240
    return-void
.end method

.method private fileFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1645
    const-string v0, ""

    .line 1646
    .local v0, "fileExt":Ljava/lang/String;
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1647
    return-object v0
.end method

.method private fileScan()V
    .locals 7

    .prologue
    .line 1350
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/TMP"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1351
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1352
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 1353
    .local v0, "children":[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1354
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v0

    if-ge v4, v5, :cond_1

    .line 1355
    aget-object v3, v0, v4

    .line 1356
    .local v3, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/TMP/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1358
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1359
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1354
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1363
    .end local v1    # "f":Ljava/io/File;
    .end local v3    # "filename":Ljava/lang/String;
    .end local v4    # "i":I
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1365
    .end local v0    # "children":[Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private getClientDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    .line 1045
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1046
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 1047
    .local v0, "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1048
    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    .line 1050
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :goto_1
    return-object v2

    .line 1045
    .restart local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1050
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private getDetailStateString(Landroid/net/NetworkInfo$DetailedState;)Ljava/lang/String;
    .locals 1
    .param p1, "detailState"    # Landroid/net/NetworkInfo$DetailedState;

    .prologue
    .line 1545
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_0

    .line 1546
    const-string v0, "AUTHENTICATING"

    .line 1568
    :goto_0
    return-object v0

    .line 1547
    :cond_0
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->BLOCKED:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_1

    .line 1548
    const-string v0, "BLOCKED"

    goto :goto_0

    .line 1549
    :cond_1
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_2

    .line 1550
    const-string v0, "CONNECTED"

    goto :goto_0

    .line 1551
    :cond_2
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_3

    .line 1552
    const-string v0, "CONNECTING]"

    goto :goto_0

    .line 1553
    :cond_3
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_4

    .line 1554
    const-string v0, "DISCONNECTED"

    goto :goto_0

    .line 1555
    :cond_4
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTING:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_5

    .line 1556
    const-string v0, "DISCONNECTING"

    goto :goto_0

    .line 1557
    :cond_5
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_6

    .line 1558
    const-string v0, "FAILED"

    goto :goto_0

    .line 1559
    :cond_6
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_7

    .line 1560
    const-string v0, "IDLE"

    goto :goto_0

    .line 1561
    :cond_7
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_8

    .line 1562
    const-string v0, "OBTAINING_IPADDR"

    goto :goto_0

    .line 1563
    :cond_8
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->SCANNING:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_9

    .line 1564
    const-string v0, "SCANNING"

    goto :goto_0

    .line 1565
    :cond_9
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->SUSPENDED:Landroid/net/NetworkInfo$DetailedState;

    if-ne p1, v0, :cond_a

    .line 1566
    const-string v0, "SUSPENDED"

    goto :goto_0

    .line 1568
    :cond_a
    const-string v0, ""

    goto :goto_0
.end method

.method private getDeviceAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 1034
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1035
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 1038
    .local v0, "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1039
    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    .line 1041
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :goto_1
    return-object v2

    .line 1034
    .restart local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1041
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private getDeviceName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 4047
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    if-eqz v1, :cond_0

    .line 4048
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    .line 4056
    :goto_0
    return-object v0

    .line 4050
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4051
    .local v0, "ssid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4052
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_p2p_device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4055
    :cond_1
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: getDeviceName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getDeviceStatus(I)Ljava/lang/String;
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 1572
    if-nez p1, :cond_0

    .line 1573
    const-string v0, "CONNECTED"

    .line 1582
    :goto_0
    return-object v0

    .line 1574
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1575
    const-string v0, "INVITED"

    goto :goto_0

    .line 1576
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1577
    const-string v0, "FAILED"

    goto :goto_0

    .line 1578
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 1579
    const-string v0, "AVAILABLE"

    goto :goto_0

    .line 1580
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 1581
    const-string v0, "UNAVAILABLE"

    goto :goto_0

    .line 1582
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method private getErrorString(I)Ljava/lang/String;
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 1526
    packed-switch p1, :pswitch_data_0

    .line 1540
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "STATE=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1528
    :pswitch_0
    const-string v0, "ERROR_CONNECT_FAILED"

    goto :goto_0

    .line 1530
    :pswitch_1
    const-string v0, "ERROR_CONNECT_TIMEOUT"

    goto :goto_0

    .line 1532
    :pswitch_2
    const-string v0, "ERROR_DEVICE_FAILED"

    goto :goto_0

    .line 1534
    :pswitch_3
    const-string v0, "ERROR_FINISH_ACTIVITY"

    goto :goto_0

    .line 1536
    :pswitch_4
    const-string v0, "ERROR_DOWNLOAD_FAILED"

    goto :goto_0

    .line 1538
    :pswitch_5
    const-string v0, "ERROR_DOWNLOADING"

    goto :goto_0

    .line 1526
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getMac(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    .line 1059
    if-eqz p1, :cond_3

    .line 1060
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1061
    .local v1, "buf":Ljava/lang/StringBuffer;
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1062
    .local v0, "a":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_2

    .line 1063
    if-eqz v2, :cond_0

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 1064
    :cond_0
    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1062
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1066
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1068
    .end local v0    # "a":[Ljava/lang/String;
    .end local v1    # "buf":Ljava/lang/StringBuffer;
    .end local v2    # "i":I
    :goto_1
    return-object v3

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 3851
    const-string v1, "s_beam_setting"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 3853
    .local v0, "settings":Landroid/content/SharedPreferences;
    const-string v1, ""

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getSnbFolderName()Ljava/lang/String;
    .locals 14

    .prologue
    .line 3991
    const-string v9, "S Note"

    .line 3992
    .local v9, "nameSnote":Ljava/lang/String;
    const-string v8, "SMemo"

    .line 3993
    .local v8, "nameSmemo":Ljava/lang/String;
    const-string v11, "TMemo"

    .line 3994
    .local v11, "nameTmemo":Ljava/lang/String;
    const-string v12, "TextMemo"

    .line 3995
    .local v12, "nameTxtmemo":Ljava/lang/String;
    const-string v10, "SnoteData"

    .line 3996
    .local v10, "nameSnoteData":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSnbFolderName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3997
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: getSnbFolderName : saved name "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mSnbFolderName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3998
    iget-object v13, p0, Lcom/sec/android/directshare/DirectShareService;->mSnbFolderName:Ljava/lang/String;

    .line 4043
    :goto_0
    return-object v13

    .line 4000
    :cond_0
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->isSupportMenuTreeForK()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4001
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: getSnbFolderName : K "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4002
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSnbFolderName:Ljava/lang/String;

    .line 4003
    iget-object v13, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    goto :goto_0

    .line 4005
    :cond_1
    iget-object v13, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    .line 4007
    .local v13, "retName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.samsung.android.snote"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 4008
    const-string v13, "SnoteData"
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4009
    :catch_0
    move-exception v7

    .line 4010
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: getSnbFolderName : there is no provider "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4013
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.android.provider.snote"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 4019
    const/4 v6, 0x0

    .line 4021
    .local v6, "cur":Landroid/database/Cursor;
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.infraware.provider.SNoteProvider/appInfo"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "AppName"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4026
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4027
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 4028
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: getSnbFolderName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4029
    const-string v0, "TextMemo"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4030
    const-string v13, "TMemo"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4038
    :cond_2
    :goto_1
    if-eqz v6, :cond_3

    .line 4039
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 4042
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    :goto_2
    iput-object v13, p0, Lcom/sec/android/directshare/DirectShareService;->mSnbFolderName:Ljava/lang/String;

    goto/16 :goto_0

    .line 4014
    .end local v6    # "cur":Landroid/database/Cursor;
    .restart local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v7

    .line 4015
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: getSnbFolderName : there is no provider "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4016
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSnbFolderName:Ljava/lang/String;

    .line 4017
    iget-object v13, p0, Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;

    goto/16 :goto_0

    .line 4031
    .restart local v6    # "cur":Landroid/database/Cursor;
    :cond_4
    :try_start_3
    const-string v0, "SMemo"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "TMemo"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4032
    const-string v13, "S Note"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 4035
    :catch_2
    move-exception v7

    .line 4036
    .local v7, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: getSnbFolderName : query "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4038
    if-eqz v6, :cond_3

    .line 4039
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 4038
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 4039
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private getStateString(I)Ljava/lang/String;
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 1481
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    sparse-switch v0, :sswitch_data_0

    .line 1521
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "STATE=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1483
    :sswitch_0
    const-string v0, "STATE_UNKNOWN"

    goto :goto_0

    .line 1485
    :sswitch_1
    const-string v0, "STATE_ENABLE_WIFI"

    goto :goto_0

    .line 1487
    :sswitch_2
    const-string v0, "STATE_DISCOVER_PEERS"

    goto :goto_0

    .line 1489
    :sswitch_3
    const-string v0, "STATE_REQUEST_NFC_CONNECT"

    goto :goto_0

    .line 1491
    :sswitch_4
    const-string v0, "STATE_UPLOAD"

    goto :goto_0

    .line 1493
    :sswitch_5
    const-string v0, "STATE_CONNECT"

    goto :goto_0

    .line 1495
    :sswitch_6
    const-string v0, "STATE_DOWNLOAD"

    goto :goto_0

    .line 1497
    :sswitch_7
    const-string v0, "STATE_DOWNLOAD_END"

    goto :goto_0

    .line 1499
    :sswitch_8
    const-string v0, "STATE_REQUEST_GROUP_INFO"

    goto :goto_0

    .line 1501
    :sswitch_9
    const-string v0, "STATE_REMOVE_GROUP"

    goto :goto_0

    .line 1503
    :sswitch_a
    const-string v0, "STATE_WAIT_CLIENT"

    goto :goto_0

    .line 1505
    :sswitch_b
    const-string v0, "STATE_CONNECT_OWNER"

    goto :goto_0

    .line 1507
    :sswitch_c
    const-string v0, "STATE_STOP_SERVICE"

    goto :goto_0

    .line 1509
    :sswitch_d
    const-string v0, "STATE_CANCEL_OR_BACK"

    goto :goto_0

    .line 1511
    :sswitch_e
    const-string v0, "STATE_WAIT_NFC_DISCONNECT"

    goto :goto_0

    .line 1513
    :sswitch_f
    const-string v0, "STATE_WAIT_WIFI_DISPLAY_POPUP_RESULT"

    goto :goto_0

    .line 1515
    :sswitch_10
    const-string v0, "STATE_WAIT_WIFI_HOTSPOT_POPUP_RESULT"

    goto :goto_0

    .line 1517
    :sswitch_11
    const-string v0, "STATE_INIT_WIFI"

    goto :goto_0

    .line 1519
    :sswitch_12
    const-string v0, "STATE_WAIT_ACTION"

    goto :goto_0

    .line 1481
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x67 -> :sswitch_4
        0x68 -> :sswitch_5
        0x69 -> :sswitch_6
        0x6a -> :sswitch_8
        0x6b -> :sswitch_9
        0x6c -> :sswitch_a
        0x6d -> :sswitch_b
        0x6e -> :sswitch_c
        0x6f -> :sswitch_d
        0x70 -> :sswitch_e
        0x71 -> :sswitch_11
        0x72 -> :sswitch_f
        0x74 -> :sswitch_10
        0x76 -> :sswitch_12
        0x77 -> :sswitch_7
    .end sparse-switch
.end method

.method private getWifiApState()I
    .locals 7

    .prologue
    .line 2023
    const/16 v2, 0xb

    .line 2024
    .local v2, "state":I
    const-string v4, "wifi"

    invoke-virtual {p0, v4}, Lcom/sec/android/directshare/DirectShareService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 2026
    .local v3, "wifi":Landroid/net/wifi/WifiManager;
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "getWifiApState"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2027
    .local v1, "method":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2031
    .end local v1    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 2028
    :catch_0
    move-exception v0

    .line 2029
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "[SBeam]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DirectShareService: getWifiApState: errmsg=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getWifiOnOffState()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1109
    const-string v2, "wifi"

    invoke-virtual {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1110
    .local v0, "wManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-ne v2, v1, :cond_0

    .line 1111
    const-string v2, "[SBeam]"

    const-string v3, "DirectShareService: wifi is Enabled"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    :goto_0
    return v1

    .line 1114
    :cond_0
    const-string v1, "[SBeam]"

    const-string v2, "DirectShareService: wifi is disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isClientList(Ljava/lang/String;)Z
    .locals 5
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    .line 1019
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: isClientList: da=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], size=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1022
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 1023
    .local v0, "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: isClientList: i=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], da=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1026
    const/4 v2, 0x1

    .line 1028
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :goto_1
    return v2

    .line 1021
    .restart local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1028
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isExistDoAction(I)Z
    .locals 5
    .param p1, "action"    # I

    .prologue
    .line 1754
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mDoActionList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1755
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mDoActionList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/directshare/DirectShareService$DoAction;

    .line 1756
    .local v0, "da":Lcom/sec/android/directshare/DirectShareService$DoAction;
    # getter for: Lcom/sec/android/directshare/DirectShareService$DoAction;->mDoAction:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService$DoAction;->access$4000(Lcom/sec/android/directshare/DirectShareService$DoAction;)I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 1757
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: isExistDoAction : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1758
    const/4 v2, 0x1

    .line 1761
    .end local v0    # "da":Lcom/sec/android/directshare/DirectShareService$DoAction;
    :goto_1
    return v2

    .line 1754
    .restart local v0    # "da":Lcom/sec/android/directshare/DirectShareService$DoAction;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1761
    .end local v0    # "da":Lcom/sec/android/directshare/DirectShareService$DoAction;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isProgress(II)Z
    .locals 5
    .param p1, "lastAction"    # I
    .param p2, "newAction"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4068
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: isProgress: ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]=>["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] state["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4069
    if-nez p1, :cond_0

    .line 4114
    :goto_0
    return v0

    .line 4075
    :cond_0
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 4076
    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    packed-switch v2, :pswitch_data_0

    .line 4110
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto :goto_0

    .line 4095
    :pswitch_1
    if-eq p1, p2, :cond_1

    .line 4096
    if-ne p2, v1, :cond_2

    .line 4098
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->notifyStopService()V

    .line 4104
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    move v0, v1

    .line 4105
    goto :goto_0

    .line 4099
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 4101
    const/16 v0, 0xcc

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    goto :goto_1

    .line 4076
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isSupportMenuTreeForK()Z
    .locals 1

    .prologue
    .line 4061
    const/4 v0, 0x1

    return v0
.end method

.method private lock(Ljava/util/concurrent/Semaphore;)V
    .locals 7
    .param p1, "semaphore"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 3867
    :try_start_0
    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v1

    .line 3869
    .local v1, "permits":I
    if-nez v1, :cond_1

    .line 3870
    const/4 v3, 0x1

    const-wide/16 v4, 0xc8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v3, v4, v5, v6}, Ljava/util/concurrent/Semaphore;->tryAcquire(IJLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    .line 3875
    .local v2, "ret":Z
    :goto_0
    if-nez v2, :cond_0

    .line 3876
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: ----------> lock: tryAcquire() return false, permits=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3882
    .end local v1    # "permits":I
    .end local v2    # "ret":Z
    :cond_0
    :goto_1
    return-void

    .line 3871
    .restart local v1    # "permits":I
    :cond_1
    if-lez v1, :cond_0

    .line 3872
    const-wide/16 v4, 0xc8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1, v4, v5, v3}, Ljava/util/concurrent/Semaphore;->tryAcquire(IJLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .restart local v2    # "ret":Z
    goto :goto_0

    .line 3879
    .end local v1    # "permits":I
    .end local v2    # "ret":Z
    :catch_0
    move-exception v0

    .line 3880
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: lock: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private notifyDownloadError(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    .line 4261
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: notifyDownloadError: errcode=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->getErrorString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4263
    packed-switch p1, :pswitch_data_0

    .line 4276
    :goto_0
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_ERROR_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4277
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4278
    const-string v1, "errorcode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4279
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4280
    return-void

    .line 4267
    .end local v0    # "i":Landroid/content/Intent;
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x66

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 4270
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 4263
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private notifyStopService()V
    .locals 2

    .prologue
    .line 4201
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.STOP_SERVICE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4202
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4203
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4204
    return-void
.end method

.method private removeDownloadList(Ljava/lang/String;)V
    .locals 6
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1714
    const/4 v1, 0x0

    .line 1715
    .local v1, "fileCount":I
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 1716
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1717
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;

    .line 1718
    .local v0, "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1719
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1720
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I

    .line 1721
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: removeDownloadList: fileName=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], mDownloadCount=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], mMediaScanCount=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1727
    .end local v0    # "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1728
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 1729
    if-nez v1, :cond_2

    .line 1730
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    packed-switch v3, :pswitch_data_0

    .line 1744
    :goto_1
    return-void

    .line 1716
    .restart local v0    # "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1732
    .end local v0    # "file":Lcom/sec/android/directshare/DirectShareService$DownloadFile;
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 1733
    const/16 v3, 0x77

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 1734
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 1735
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->sendDownloadEnd()V

    goto :goto_1

    .line 1741
    :cond_2
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadFileName:Ljava/lang/String;

    .line 1742
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I

    iget v4, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    invoke-direct {p0, p1, v3, v4}, Lcom/sec/android/directshare/DirectShareService;->downloaded(Ljava/lang/String;II)V

    goto :goto_1

    .line 1730
    nop

    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_0
    .end packed-switch
.end method

.method public static removeSharedPreferences(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 3857
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: remove shared preferences, name=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3858
    const-string v2, "s_beam_setting"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 3860
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3861
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 3862
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3863
    return-void
.end method

.method private sendBroadcastServiceStart()V
    .locals 2

    .prologue
    .line 1418
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.CLIENT_SERVICE_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1419
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1420
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1421
    return-void
.end method

.method private sendBroadcastServiceStop()V
    .locals 2

    .prologue
    .line 1424
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.CLIENT_SERVICE_STOP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1425
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1426
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1427
    return-void
.end method

.method private sendCancel()V
    .locals 1

    .prologue
    .line 1686
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectShareService$2;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$2;->start()V

    .line 1697
    return-void
.end method

.method private sendDownloadEnd()V
    .locals 1

    .prologue
    .line 1673
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectShareService$1;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$1;->start()V

    .line 1683
    return-void
.end method

.method private sendDownloadEndOnefile(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1700
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$3;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/directshare/DirectShareService$3;-><init>(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$3;->start()V

    .line 1711
    return-void
.end method

.method private sendDownloadFileInfo(ZIIILjava/lang/String;)V
    .locals 3
    .param p1, "bReady"    # Z
    .param p2, "progress"    # I
    .param p3, "count"    # I
    .param p4, "totalCount"    # I
    .param p5, "fileName"    # Ljava/lang/String;

    .prologue
    .line 4283
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_INFO_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4284
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4285
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4286
    if-eqz p1, :cond_0

    .line 4287
    const-string v1, "session"

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4288
    const-string v1, "progress"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4289
    const-string v1, "count"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4290
    const-string v1, "totalCount"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4291
    const-string v1, "filename"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4292
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4294
    :cond_0
    return-void
.end method

.method private sendMsgSendingToFail()V
    .locals 2

    .prologue
    .line 3973
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: sendMsgSendingToFail"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3974
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 3975
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 3976
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v1, 0x70

    if-ne v0, v1, :cond_0

    .line 3977
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 3978
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3988
    :goto_0
    return-void

    .line 3979
    :cond_0
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v1, 0x67

    if-ne v0, v1, :cond_1

    .line 3980
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 3981
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    goto :goto_0

    .line 3983
    :cond_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto :goto_0

    .line 3986
    :cond_2
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto :goto_0
.end method

.method private setDeviceName(Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 3894
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3895
    :cond_0
    const-string v0, "[SBeam]"

    const-string v1, "setDeviceName : invalid string"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3902
    :cond_1
    :goto_0
    return-void

    .line 3898
    :cond_2
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3899
    :cond_3
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;

    .line 3900
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDeviceName = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setSharedPreferences(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 3843
    const-string v2, "s_beam_setting"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 3845
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3846
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 3847
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3848
    return-void
.end method

.method private declared-synchronized setState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 1430
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    .line 1431
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateTime:J

    .line 1432
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNextState:I

    .line 1433
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: ----# state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1434
    monitor-exit p0

    return-void

    .line 1430
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setState(II)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "nextState"    # I

    .prologue
    .line 1437
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    .line 1438
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateTime:J

    .line 1439
    iput p2, p0, Lcom/sec/android/directshare/DirectShareService;->mNextState:I

    .line 1440
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: ----# state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], next state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mNextState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1442
    monitor-exit p0

    return-void

    .line 1437
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setWifiApEnabled()V
    .locals 5

    .prologue
    .line 2035
    const-string v2, "[SBeam]"

    const-string v3, "DirectShareService:  setWifiApEnabled "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2036
    const-string v2, "wifi"

    invoke-virtual {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 2038
    .local v1, "wifi":Landroid/net/wifi/WifiManager;
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2042
    :goto_0
    return-void

    .line 2039
    :catch_0
    move-exception v0

    .line 2040
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: setWifiApEnabled: errmsg=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setWifiState(II)V
    .locals 5
    .param p1, "wifiState"    # I
    .param p2, "connection"    # I

    .prologue
    .line 3807
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "WifiInitState"

    invoke-static {v2, v3}, Lcom/sec/android/directshare/DirectShareService;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3809
    .local v0, "ret":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "WifiConnection"

    invoke-static {v2, v3}, Lcom/sec/android/directshare/DirectShareService;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3811
    .local v1, "ret1":Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3812
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "WifiInitState"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/directshare/DirectShareService;->setSharedPreferences(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3814
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "WifiConnection"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/directshare/DirectShareService;->setSharedPreferences(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3816
    iput p1, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pState:I

    .line 3817
    iput p2, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pConnection:I

    .line 3818
    const-string v2, "[SBeam]"

    const-string v3, "DirectShareService: Set Shared Preferences"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3819
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 3820
    if-nez p2, :cond_1

    .line 3821
    const-string v2, "[SBeam]"

    const-string v3, "    WiFi State=[WIFI_P2P_STATE_ENABLED]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3822
    const-string v2, "[SBeam]"

    const-string v3, "    WiFi Connectione=[DISCONNECTED]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3840
    :goto_0
    return-void

    .line 3824
    :cond_1
    const-string v2, "[SBeam]"

    const-string v3, "    WiFi State=[WIFI_P2P_STATE_ENABLED]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3825
    const-string v2, "[SBeam]"

    const-string v3, "    WiFi Connectione=[CONNECTED]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3828
    :cond_2
    if-nez p2, :cond_3

    .line 3829
    const-string v2, "[SBeam]"

    const-string v3, "    WiFi State=[WIFI_P2P_STATE_DISABLED]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3830
    const-string v2, "[SBeam]"

    const-string v3, "    WiFi Connectione=[DISCONNECTED]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3832
    :cond_3
    const-string v2, "[SBeam]"

    const-string v3, "    WiFi State=[WIFI_P2P_STATE_DISABLED]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3833
    const-string v2, "[SBeam]"

    const-string v3, "    WiFi Connectione=[CONNECTED]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3837
    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pState:I

    .line 3838
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pConnection:I

    goto :goto_0
.end method

.method private startAction()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 985
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startHouseKeeper()V

    .line 986
    iput v2, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    .line 987
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStartTime:J

    .line 988
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J

    .line 989
    iput-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mFirstNfcDisconnectTime:J

    .line 990
    iput v3, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectRetry:I

    .line 991
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsStartAction:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    if-ne v0, v2, :cond_1

    .line 992
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startDisconnectUi()V

    .line 993
    sput v2, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    .line 995
    :cond_1
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    sparse-switch v0, :sswitch_data_0

    .line 1010
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onStartCommand: invalid state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    iput v3, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    .line 1013
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    .line 1016
    :goto_0
    return-void

    .line 999
    :sswitch_0
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiApState:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_2

    .line 1000
    const/16 v0, 0x74

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    goto :goto_0

    .line 1001
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDisplayCheck:Z

    if-eqz v0, :cond_3

    .line 1002
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_init()V

    .line 1003
    const/16 v0, 0x72

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    goto :goto_0

    .line 1005
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_init()V

    .line 1006
    const/16 v0, 0x71

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    goto :goto_0

    .line 995
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x76 -> :sswitch_0
    .end sparse-switch
.end method

.method private startControlThread()V
    .locals 3

    .prologue
    .line 1651
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    if-nez v0, :cond_0

    .line 1652
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: startControlThread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1653
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;

    iget-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerAddress:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/directshare/DirectShareService$ControlThread;-><init>(Lcom/sec/android/directshare/DirectShareService;ZLjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    .line 1654
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->start()V

    .line 1656
    :cond_0
    return-void
.end method

.method private startDisconnectUi()V
    .locals 2

    .prologue
    .line 4300
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4301
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4302
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4303
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4304
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->startActivity(Landroid/content/Intent;)V

    .line 4305
    return-void
.end method

.method private startDownload(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "downloadSize"    # J
    .param p3, "fileSize"    # J
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 4210
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4211
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4212
    const-string v1, "filesize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4213
    const-string v1, "filename"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4214
    const-string v1, "totalsize"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 4215
    const-string v1, "device_name"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4216
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 4217
    return-void
.end method

.method private startHouseKeeper()V
    .locals 2

    .prologue
    .line 1331
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    if-nez v0, :cond_0

    .line 1332
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;-><init>(Lcom/sec/android/directshare/DirectShareService;Lcom/sec/android/directshare/DirectShareService$1;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    .line 1333
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->start()V

    .line 1335
    :cond_0
    return-void
.end method

.method private startPowerManager()V
    .locals 3

    .prologue
    .line 960
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 961
    .local v0, "powerManager":Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string v2, "SBeam"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 962
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 963
    return-void
.end method

.method private startSenderActivity()V
    .locals 4

    .prologue
    .line 4139
    const-string v1, ""

    const-string v2, ""

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->UpLoadStartNotification(Ljava/lang/String;Ljava/lang/String;I)V

    .line 4141
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/directshare/ui/SenderActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4142
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4143
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4144
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4145
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->startActivity(Landroid/content/Intent;)V

    .line 4146
    return-void
.end method

.method private startUpload(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "totalCount"    # I

    .prologue
    .line 4150
    iput p3, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadTotalCount:I

    .line 4151
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadFileName:Ljava/lang/String;

    .line 4152
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/directshare/DirectShareService;->UpLoadstateNotification(Ljava/lang/String;Ljava/lang/String;I)V

    .line 4154
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.UPLOAD_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4155
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4156
    const-string v1, "filename"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4157
    const-string v1, "device_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4158
    const-string v1, "count"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4159
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 4160
    const-string v1, "[SBeam]"

    const-string v2, "DirectShareService: startUpload: sendStickyBroadcast"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4161
    return-void
.end method

.method private stopControlThread()V
    .locals 4

    .prologue
    .line 1659
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    if-eqz v1, :cond_0

    .line 1660
    const-string v1, "[SBeam]"

    const-string v2, "DirectShareService: stopControlThread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1661
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    invoke-virtual {v1}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->interrupt()V

    .line 1663
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->join(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1669
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;

    .line 1670
    return-void

    .line 1664
    :catch_0
    move-exception v0

    .line 1665
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: stopControlThread : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopDisconnectUi(I)V
    .locals 4
    .param p1, "opt"    # I

    .prologue
    .line 4308
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.STOP_DISCONNECT_UI_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4309
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "opt"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4310
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4311
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: stopDisconnectUi, opt=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4312
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 4313
    return-void
.end method

.method private stopDownload(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1473
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    if-eqz v0, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1475
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->setFlag(Z)V

    .line 1478
    :cond_0
    return-void
.end method

.method private stopHouseKeeper()V
    .locals 4

    .prologue
    .line 1338
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    if-eqz v1, :cond_0

    .line 1339
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    invoke-virtual {v1}, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->interrupt()V

    .line 1341
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1345
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mHouseKeeper:Lcom/sec/android/directshare/DirectShareService$HouseKeeper;

    .line 1347
    :cond_0
    return-void

    .line 1342
    :catch_0
    move-exception v0

    .line 1343
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: stopHouseKeeper: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopPowerManager()V
    .locals 1

    .prologue
    .line 966
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 967
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 968
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 970
    :cond_0
    return-void
.end method

.method private declared-synchronized stopService()V
    .locals 6

    .prologue
    const/16 v5, 0x6e

    .line 1445
    monitor-enter p0

    :try_start_0
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: stopService["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-direct {p0, v4}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1446
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.directshare.UPLOAD_START_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1447
    .local v1, "removedI_upload":Landroid/content/Intent;
    const-string v2, "com.sec.android.directshare"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1448
    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->removeStickyBroadcast(Landroid/content/Intent;)V

    .line 1449
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.directshare.DOWNLOAD_START_ACTION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1450
    .local v0, "removedI_download":Landroid/content/Intent;
    const-string v2, "com.sec.android.directshare"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1451
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->removeStickyBroadcast(Landroid/content/Intent;)V

    .line 1452
    const-string v2, "[SBeam]"

    const-string v3, "DirectShareService: stopService: removeStickyBroadcast"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1453
    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v3, 0x68

    if-ne v2, v3, :cond_2

    .line 1454
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_cancelConnect()V

    .line 1459
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 1460
    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    if-eq v2, v5, :cond_1

    .line 1461
    const/16 v2, 0x6e

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 1463
    :cond_1
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 1464
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->notifyStopService()V

    .line 1465
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->DownLoadstopNotification()V

    .line 1466
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->UpLoadstopNotification()V

    .line 1467
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->threadName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->stopDownload(Ljava/lang/String;)V

    .line 1468
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopControlThread()V

    .line 1469
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->stopSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1470
    monitor-exit p0

    return-void

    .line 1455
    :cond_2
    :try_start_1
    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v3, 0x65

    if-eq v2, v3, :cond_3

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v3, 0x66

    if-ne v2, v3, :cond_0

    .line 1456
    :cond_3
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestNfcConnect(Z)V

    .line 1457
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_stopPeerDiscovery()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1445
    .end local v0    # "removedI_download":Landroid/content/Intent;
    .end local v1    # "removedI_upload":Landroid/content/Intent;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private subtitleFormat(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1636
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->fileFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "smi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->fileFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "srt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->fileFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "sub"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->fileFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "vtt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->fileFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1639
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1641
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private unlock(Ljava/util/concurrent/Semaphore;)V
    .locals 4
    .param p1, "semaphore"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 3885
    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    .line 3886
    .local v0, "permits":I
    if-nez v0, :cond_0

    .line 3887
    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 3891
    :goto_0
    return-void

    .line 3889
    :cond_0
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: ----------> unlock: invalid permits=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateDownloadProgress(JLjava/lang/String;II)V
    .locals 7
    .param p1, "downloadSize"    # J
    .param p3, "deviceName"    # Ljava/lang/String;
    .param p4, "count"    # I
    .param p5, "totalCount"    # I

    .prologue
    .line 4222
    const-wide/16 v2, 0x64

    mul-long/2addr v2, p1

    iget-wide v4, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 4223
    .local v1, "progress":I
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.directshare.DOWNLOAD_STATE_ACTION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4224
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "com.sec.android.directshare"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4225
    const-string v2, "device_name"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4226
    const-string v2, "progress"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4227
    const-string v2, "count"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4228
    const-string v2, "totalCount"

    invoke-virtual {v0, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4229
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4230
    return-void
.end method

.method private updateUploadFileCount(Ljava/lang/String;II)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "count"    # I
    .param p3, "totalCount"    # I

    .prologue
    .line 4192
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.UPLOAD_COMPLETED_ONEFILE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4193
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4194
    const-string v1, "filepath"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4195
    const-string v1, "count"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4196
    const-string v1, "totalCount"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4197
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4198
    return-void
.end method

.method private updateUploadFileInfo(Ljava/lang/String;III)V
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "progress"    # I
    .param p3, "count"    # I
    .param p4, "totalCount"    # I

    .prologue
    .line 4182
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.UPLOAD_FILEINFO_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4183
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4184
    const-string v1, "filename"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4185
    const-string v1, "progress"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4186
    const-string v1, "count"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4187
    const-string v1, "totalCount"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4188
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4189
    return-void
.end method

.method private updateUploadPrgress(I)V
    .locals 2
    .param p1, "percent"    # I

    .prologue
    .line 4165
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.UPLOAD_STATE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4166
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4167
    const-string v1, "progress"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4168
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4169
    return-void
.end method

.method private uploaded()V
    .locals 3

    .prologue
    .line 4173
    new-instance v1, Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/directshare/ui/NotiManager;-><init>(Landroid/content/Context;)V

    .line 4174
    .local v1, "notiManager":Lcom/sec/android/directshare/ui/NotiManager;
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/directshare/ui/NotiManager;->addUploaded(Ljava/lang/String;)V

    .line 4176
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.directshare.UPLOAD_COMPLETED_ACTION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4177
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "com.sec.android.directshare"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4178
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4179
    return-void
.end method

.method private wifiDirectCancelByCondition()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1804
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: wifiDirectCancelByCondition : enabled("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") connected("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pConnection:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1806
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pState:I

    if-ne v3, v0, :cond_1

    .line 1807
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_disable()V

    .line 1813
    :cond_0
    :goto_0
    return-void

    .line 1810
    :cond_1
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pConnection:I

    if-ne v3, v0, :cond_0

    .line 1811
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V

    goto :goto_0
.end method

.method private wifiDirect_Listen()V
    .locals 3

    .prologue
    .line 1835
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_1

    .line 1836
    :cond_0
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: wifiDirect_Listen : no manager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1850
    :goto_0
    return-void

    .line 1839
    :cond_1
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> wifiDirect_Listen()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1840
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directshare/DirectShareService$5;

    invoke-direct {v2, p0}, Lcom/sec/android/directshare/DirectShareService$5;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestP2pListen(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 1849
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J

    goto :goto_0
.end method

.method private wifiDirect_cancelConnect()V
    .locals 3

    .prologue
    .line 1970
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 1971
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> cancelConnect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1972
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directshare/DirectShareService$9;

    invoke-direct {v2, p0}, Lcom/sec/android/directshare/DirectShareService$9;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 1984
    :cond_0
    return-void
.end method

.method private wifiDirect_connect(Ljava/lang/String;)Z
    .locals 4
    .param p1, "deviceAddress"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1893
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v2, :cond_0

    .line 1894
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    .line 1895
    .local v0, "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    iput-object p1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 1896
    iput v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    .line 1897
    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v1, v2, Landroid/net/wifi/WpsInfo;->setup:I

    .line 1898
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: ----> connect(): ma=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1899
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/sec/android/directshare/DirectShareService$6;

    invoke-direct {v3, p0}, Lcom/sec/android/directshare/DirectShareService$6;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 1909
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/directshare/DirectShareService;->mConnectTime:J

    .line 1910
    const/4 v1, 0x1

    .line 1912
    .end local v0    # "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    :cond_0
    return v1
.end method

.method private wifiDirect_init()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/directshare/utils/Utils;->isEdmSbeamAllowed(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 428
    const/16 v1, 0xc9

    invoke-direct {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    .line 429
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_cancelConnect()V

    .line 430
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_disable()V

    .line 432
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectInit:Z

    .line 433
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    .line 434
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->onDestroy()V

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectInit:Z

    if-nez v1, :cond_0

    .line 439
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 440
    .local v0, "i":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 441
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 442
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 443
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 444
    const-string v1, "com.sec.android.directshare.UPLOAD_CANCEL_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 445
    const-string v1, "com.sec.android.directshare.REQ_DOWNLOAD_CANCEL_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 446
    const-string v1, "wifip2p"

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 447
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 448
    new-instance v1, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;-><init>(Lcom/sec/android/directshare/DirectShareService;Lcom/sec/android/directshare/DirectShareService$1;)V

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 449
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/directshare/DirectShareService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 450
    iput-boolean v4, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectInit:Z

    .line 451
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiTime:J

    .line 452
    iput-boolean v4, p0, Lcom/sec/android/directshare/DirectShareService;->mCheckWifiBroadcast:Z

    goto :goto_0
.end method

.method private wifiDirect_removeGroup()V
    .locals 3

    .prologue
    .line 1953
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 1954
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> removeGroup"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directshare/DirectShareService$8;

    invoke-direct {v2, p0}, Lcom/sec/android/directshare/DirectShareService$8;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 1967
    :cond_0
    return-void
.end method

.method private wifiDirect_requestGroupInfo()V
    .locals 2

    .prologue
    .line 1945
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 1946
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1947
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> requestGroupInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1948
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, p0}, Landroid/net/wifi/p2p/WifiP2pManager;->requestGroupInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 1950
    :cond_0
    return-void
.end method

.method private wifiDirect_requestNfcConnect(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1916
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 1917
    if-eqz p1, :cond_1

    .line 1918
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: ----> requestNfcConnect ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1919
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directshare/DirectShareService$7;

    invoke-direct {v2, p0}, Lcom/sec/android/directshare/DirectShareService$7;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v0, v1, p1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestNfcConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ZLandroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 1930
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J

    .line 1936
    :cond_0
    :goto_0
    return-void

    .line 1932
    :cond_1
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: ----> requestNfcConnect ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1933
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestNfcConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ZLandroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0
.end method

.method private wifiDirect_requestPeer()V
    .locals 2

    .prologue
    .line 1853
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 1857
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, p0}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 1860
    :cond_0
    return-void
.end method

.method private wifiDirect_stopPeerDiscovery()V
    .locals 3

    .prologue
    .line 1987
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_1

    .line 1988
    :cond_0
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: wifiDirect_stopPeerDiscovery : invalid condition"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2003
    :goto_0
    return-void

    .line 1991
    :cond_1
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> stopPeerDiscovery"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1992
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directshare/DirectShareService$10;

    invoke-direct {v2, p0}, Lcom/sec/android/directshare/DirectShareService$10;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 402
    const/4 v0, 0x0

    return-object v0
.end method

.method public onChannelDisconnected()V
    .locals 2

    .prologue
    .line 2628
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> onChannelDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2629
    return-void
.end method

.method public onConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V
    .locals 0
    .param p1, "arg0"    # Landroid/net/wifi/p2p/WifiP2pInfo;

    .prologue
    .line 2517
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 407
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 408
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ****************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: *  DirectShare : VER=[2.5.0]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ****************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x7f060000

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;

    .line 418
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->fileScan()V

    .line 419
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcastServiceStart()V

    .line 421
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startPowerManager()V

    .line 422
    new-instance v0, Lcom/sec/android/directshare/ui/NotiManager;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/ui/NotiManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    .line 423
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1073
    const/16 v0, 0xcc

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    .line 1074
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->threadName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->stopDownload(Ljava/lang/String;)V

    .line 1076
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopControlThread()V

    .line 1077
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopHouseKeeper()V

    .line 1078
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1080
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1082
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->StopWebServer()Z

    .line 1083
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcastServiceStop()V

    .line 1086
    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->stopForeground(Z)V

    .line 1088
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    if-eqz v0, :cond_2

    .line 1089
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onDestroy : init["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] connect["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pConnection:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1091
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pState:I

    if-ne v0, v3, :cond_3

    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1094
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_disable()V

    .line 1100
    :cond_1
    :goto_0
    const-string v0, "WifiInitState"

    invoke-static {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->removeSharedPreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 1103
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopPowerManager()V

    .line 1104
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 1106
    return-void

    .line 1096
    :cond_3
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mInitWifiP2pConnection:I

    if-nez v0, :cond_1

    .line 1097
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V

    goto :goto_0
.end method

.method public onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 11
    .param p1, "arg0"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    const/16 v10, 0x6c

    const/16 v9, 0x6b

    const/4 v8, 0x2

    const-wide/16 v6, 0x0

    const/16 v5, 0x1fd

    .line 2521
    const-string v2, "[SBeam]"

    const-string v3, "DirectShareService: ----> onGroupInfoAvailable"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2522
    if-nez p1, :cond_0

    .line 2523
    const-string v2, "[SBeam]"

    const-string v3, "onGroupInfoAvailable: arg0 is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2624
    :goto_0
    return-void

    .line 2526
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    if-eqz v2, :cond_1

    .line 2527
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 2531
    :goto_1
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    if-nez v2, :cond_2

    .line 2532
    const-string v2, "[SBeam]"

    const-string v3, "onGroupInfoAvailable: mOwnerDevice is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2529
    :cond_1
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    goto :goto_1

    .line 2535
    :cond_2
    const-string v2, "[SBeam]"

    const-string v3, "Owner:"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2536
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    da=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2537
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    deviceName=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2538
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    primaryDeviceType=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2539
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    secondaryDeviceType=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->secondaryDeviceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2540
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    status=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-direct {p0, v4}, Lcom/sec/android/directshare/DirectShareService;->getDeviceStatus(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2541
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2542
    const-string v2, "[SBeam]"

    const-string v3, "Client:"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2543
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2544
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2545
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 2546
    .local v0, "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    da=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2547
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    deviceName=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2548
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    primaryDeviceType=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2549
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    secondaryDeviceType=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->secondaryDeviceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2550
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    status=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-direct {p0, v4}, Lcom/sec/android/directshare/DirectShareService;->getDeviceStatus(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2551
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mClientList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2554
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    :cond_3
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 2555
    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    packed-switch v2, :pswitch_data_0

    .line 2622
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 2557
    :pswitch_0
    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    if-ne v2, v8, :cond_b

    .line 2558
    iget-boolean v2, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    if-eqz v2, :cond_7

    .line 2560
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->isClientList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2562
    sget v2, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    if-ne v2, v8, :cond_4

    .line 2563
    invoke-direct {p0, v10}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2564
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2565
    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    .line 2571
    :goto_3
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getClientDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->setDeviceName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2567
    :cond_4
    const/16 v2, 0x70

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2568
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2569
    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    goto :goto_3

    .line 2574
    :cond_5
    iget-boolean v2, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDisplayCheck:Z

    if-eqz v2, :cond_6

    .line 2575
    const/16 v2, 0x72

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2576
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 2578
    :cond_6
    invoke-direct {p0, v9}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2579
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2580
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V

    goto/16 :goto_0

    .line 2585
    :cond_7
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2587
    sget v2, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    if-ne v2, v8, :cond_8

    .line 2588
    const/16 v2, 0x6d

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2589
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2590
    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    .line 2596
    :goto_4
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mOwnerDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->setDeviceName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2592
    :cond_8
    const/16 v2, 0x70

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2593
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2594
    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    goto :goto_4

    .line 2597
    :cond_9
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->isClientList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2599
    invoke-direct {p0, v9}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2600
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2601
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V

    goto/16 :goto_0

    .line 2604
    :cond_a
    invoke-direct {p0, v9}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2605
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2606
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V

    goto/16 :goto_0

    .line 2609
    :cond_b
    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_d

    .line 2610
    iget-boolean v2, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    if-eqz v2, :cond_c

    .line 2611
    invoke-direct {p0, v10}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 2612
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2613
    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    goto/16 :goto_0

    .line 2615
    :cond_c
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 2618
    :cond_d
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 2555
    :pswitch_data_0
    .packed-switch 0x6a
        :pswitch_0
    .end packed-switch
.end method

.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 8
    .param p1, "peers"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 2448
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "----> onPeersAvailable ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2451
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v0

    .line 2452
    .local v0, "devList":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2454
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2455
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 2457
    .local v1, "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    if-ne v3, v6, :cond_1

    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2458
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "----> da=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2459
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "      deviceName=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2460
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "      status=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-direct {p0, v5}, Lcom/sec/android/directshare/DirectShareService;->getDeviceStatus(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2461
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "      isGroupOwner=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2462
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "      isGroupClient=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupClient()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2463
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "      GOdeviceName=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->GOdeviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2464
    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->setDeviceName(Ljava/lang/String;)V

    .line 2466
    :cond_1
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 2467
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    packed-switch v3, :pswitch_data_0

    .line 2509
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 2469
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2470
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    if-ne v3, v6, :cond_0

    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2471
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupClient()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->GOdeviceName:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 2473
    iput-boolean v7, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectConnect:Z

    .line 2478
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_connect(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2479
    const/16 v3, 0x68

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    goto/16 :goto_0

    .line 2481
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_discoverPeers()V

    goto/16 :goto_0

    .line 2487
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2488
    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    if-ne v3, v6, :cond_3

    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2489
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "----> da=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], deviceName=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], status=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-direct {p0, v5}, Lcom/sec/android/directshare/DirectShareService;->getDeviceStatus(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2493
    :cond_3
    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-ne v3, v6, :cond_0

    .line 2495
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V

    .line 2496
    const/16 v3, 0xcb

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    .line 2497
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    goto/16 :goto_0

    .line 2501
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v3}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 2502
    iget v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-ne v3, v7, :cond_0

    .line 2503
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "----> da=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], deviceName=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], status=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-direct {p0, v5}, Lcom/sec/android/directshare/DirectShareService;->getDeviceStatus(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2513
    .end local v1    # "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_4
    return-void

    .line 2467
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 458
    if-nez p1, :cond_0

    .line 459
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: onStartCommand : null intent"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    const/4 v0, 0x2

    .line 904
    :goto_0
    return v0

    .line 462
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 463
    .local v7, "action":Ljava/lang/String;
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onStartCommand: action=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    if-nez v7, :cond_1

    .line 465
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    .line 466
    const/4 v0, 0x2

    goto :goto_0

    .line 467
    :cond_1
    const-string v0, "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 468
    sget v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 469
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: nfc state is disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_2
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceMode:I

    .line 473
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->stopDisconnectUi(I)V

    .line 475
    const-string v0, "com.sec.android.directshare.extra_from"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 477
    .local v8, "from":I
    if-nez v8, :cond_3

    .line 478
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: from=[APP]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsStartAction:Z

    .line 485
    :goto_1
    if-nez v8, :cond_5

    .line 486
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 487
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;

    const v1, 0x7f050019

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 488
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    .line 489
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsStartAction:Z

    .line 490
    const/4 v0, 0x2

    goto :goto_0

    .line 481
    :cond_3
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: from=[NFC:SEND_COMPLETE]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsNfcStartAction:Z

    goto :goto_1

    .line 493
    :cond_4
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastAction:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/directshare/DirectShareService;->isProgress(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 494
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 495
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 499
    :cond_5
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 500
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: onStartCommand: in ACTION_DOWNLOAD"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/directshare/DirectShareService;->mLastActionTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-gez v0, :cond_6

    .line 502
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: onStartCommand: same touch"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_init()V

    .line 504
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V

    .line 505
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    .line 506
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 508
    :cond_6
    const/16 v0, 0x1f5

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    .line 509
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v1, :cond_7

    .line 510
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startDisconnectUi()V

    .line 512
    :cond_7
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 515
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastActionTime:J

    .line 516
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastAction:I

    .line 517
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsStartAction:Z

    if-nez v0, :cond_a

    .line 518
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsNfcStartAction:Z

    .line 519
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mP2pSendCompleteTime:J

    .line 520
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    .line 521
    const/16 v0, 0x1f5

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    .line 522
    :cond_9
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 524
    :cond_a
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isAllowWifiByDevicePolicy(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 525
    new-instance v9, Landroid/content/Intent;

    const-string v0, "com.android.settings.action.SBEAM_STATE_UPDATED"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 526
    .local v9, "i":Landroid/content/Intent;
    const-string v0, "turn_on"

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 527
    invoke-virtual {p0, v9}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 528
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;

    const v1, 0x7f050023

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 530
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->stopSelf()V

    .line 531
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 533
    .end local v9    # "i":Landroid/content/Intent;
    :cond_b
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isAllowSBeamOnOperatorAirplaneMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 534
    new-instance v9, Landroid/content/Intent;

    const-string v0, "com.android.settings.action.SBEAM_STATE_UPDATED"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 535
    .restart local v9    # "i":Landroid/content/Intent;
    const-string v0, "turn_on"

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 536
    invoke-virtual {p0, v9}, Lcom/sec/android/directshare/DirectShareService;->sendBroadcast(Landroid/content/Intent;)V

    .line 537
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;

    const v1, 0x7f050024

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/DirectShareService;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 539
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->stopSelf()V

    .line 540
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 542
    .end local v9    # "i":Landroid/content/Intent;
    :cond_c
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_e

    .line 543
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onStartCommand: in ACTION_START, state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    if-nez v8, :cond_d

    .line 546
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v1, :cond_d

    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v1, 0x66

    if-ne v0, v1, :cond_d

    .line 548
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStartTime:J

    .line 549
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J

    .line 550
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mFirstNfcDisconnectTime:J

    .line 551
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectRetry:I

    .line 552
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 553
    const/16 v0, 0x71

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 554
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 555
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startDisconnectUi()V

    .line 556
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V

    .line 559
    :cond_d
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 561
    :cond_e
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->UpLoadstopNotification()V

    .line 562
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startSenderActivity()V

    .line 563
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startAction()V

    .line 564
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/NotiManager;->getUploadId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v1}, Lcom/sec/android/directshare/ui/NotiManager;->getUploadNoti()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directshare/DirectShareService;->startForeground(ILandroid/app/Notification;)V

    .line 904
    .end local v8    # "from":I
    :cond_f
    :goto_2
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 565
    :cond_10
    const-string v0, "com.sec.android.directshare.DIRECT_SHARE_DOWNLOAD_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 571
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastAction:I

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/sec/android/directshare/DirectShareService;->isProgress(II)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 572
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 573
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 575
    :cond_11
    const/16 v0, 0x259

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDeviceMode:I

    .line 576
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->stopDisconnectUi(I)V

    .line 577
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 578
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startDisconnectUi()V

    .line 579
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->stopSelf()V

    .line 580
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 583
    :cond_12
    const-string v0, "session"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/directshare/utils/SessionInfo;

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    .line 596
    const-string v0, "WiFiDisplayChack"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDisplayCheck:Z

    .line 597
    const-string v0, "WiFiHotspotChack"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiHotspotCheck:Z

    .line 598
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: Wifi display=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDisplayCheck:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: Wifi hotspot=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiHotspotCheck:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_15

    .line 601
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/directshare/DirectShareService;->mLastActionTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-gez v0, :cond_13

    .line 602
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: onStartCommand: same touch"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastActionTime:J

    .line 604
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastAction:I

    .line 605
    const/16 v0, 0xcc

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    .line 606
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_init()V

    .line 607
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V

    .line 608
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    .line 609
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 611
    :cond_13
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v0, v1, :cond_14

    .line 612
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onStartCommand: in ACTION_DOWNLOAD, state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    const/16 v0, 0xcc

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    .line 615
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 617
    :cond_14
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->UpLoadstopNotification()V

    .line 619
    :cond_15
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0}, Lcom/sec/android/directshare/utils/SessionInfo;->getMacAddr()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_16

    .line 620
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: onStartCommand: da is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 623
    :cond_16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastActionTime:J

    .line 624
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastAction:I

    .line 625
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1a

    .line 626
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onStartCommand: in ACTION_DOWNLOAD, state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v1, :cond_17

    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectConnect:Z

    if-nez v0, :cond_17

    .line 629
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0}, Lcom/sec/android/directshare/utils/SessionInfo;->getMacAddr()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    .line 630
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStartTime:J

    .line 631
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDiscoverPeersTime:J

    .line 632
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->addDownloadList()V

    .line 634
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0}, Lcom/sec/android/directshare/utils/SessionInfo;->getTotalSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    .line 635
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0}, Lcom/sec/android/directshare/utils/SessionInfo;->getFileCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    .line 637
    :cond_17
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v1, 0x69

    if-ne v0, v1, :cond_18

    .line 638
    const/16 v0, 0xce

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    .line 640
    :cond_18
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v1, :cond_19

    sget v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_19

    .line 641
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startDisconnectUi()V

    .line 643
    :cond_19
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 645
    :cond_1a
    sget v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    if-eqz v0, :cond_1b

    sget v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1c

    .line 646
    :cond_1b
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startDisconnectUi()V

    .line 647
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    .line 652
    :cond_1c
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->startHouseKeeper()V

    .line 653
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->addDownloadList()V

    .line 654
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0}, Lcom/sec/android/directshare/utils/SessionInfo;->getMacAddr()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    .line 655
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0}, Lcom/sec/android/directshare/utils/SessionInfo;->getTotalSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    .line 656
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0}, Lcom/sec/android/directshare/utils/SessionInfo;->getFileCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    .line 657
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    .line 658
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDiscoverPeersTime:J

    .line 659
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I

    .line 660
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadedCount:I

    .line 661
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNextDownloadStateTime:J

    .line 662
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mLastPercent:I

    .line 663
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStartTime:J

    .line 664
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J

    .line 665
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mFirstNfcDisconnectTime:J

    .line 666
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectConnect:Z

    .line 667
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: da=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService;->mCurrentMac:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], MaxFileSize=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], MaxCount=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    .line 670
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 671
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    sparse-switch v0, :sswitch_data_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 707
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onStartCommand: invalid state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    .line 710
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 711
    const/16 v0, 0xcc

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    .line 712
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    .line 715
    :goto_3
    const/4 v6, 0x0

    .line 716
    .local v6, "DownLoadCount":Z
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    iget v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mTotalFile:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1e

    .line 717
    const/4 v6, 0x0

    .line 721
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->UpLoadstopNotification()V

    .line 722
    const-string v0, ""

    invoke-direct {p0, v0, v6}, Lcom/sec/android/directshare/DirectShareService;->DownLoadStartNotificationInit(Ljava/lang/String;Z)V

    .line 723
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/NotiManager;->getDownloadId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v1}, Lcom/sec/android/directshare/ui/NotiManager;->getDownloadNoti()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directshare/DirectShareService;->startForeground(ILandroid/app/Notification;)V

    goto/16 :goto_2

    .line 676
    .end local v6    # "DownLoadCount":Z
    :sswitch_0
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiHotspotCheck:Z

    if-eqz v0, :cond_1d

    .line 677
    const/16 v0, 0x74

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto :goto_3

    .line 680
    :cond_1d
    const/16 v0, 0x71

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 681
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 682
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_init()V

    goto :goto_3

    .line 686
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto :goto_3

    .line 689
    :sswitch_2
    const/16 v0, 0x70

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 690
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto :goto_3

    .line 693
    :sswitch_3
    const/16 v0, 0x65

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 694
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 695
    const/16 v0, 0x1f6

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    goto :goto_3

    .line 698
    :sswitch_4
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 699
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto :goto_3

    .line 703
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto :goto_3

    .line 719
    .restart local v6    # "DownLoadCount":Z
    :cond_1e
    const/4 v6, 0x1

    goto :goto_4

    .line 724
    .end local v6    # "DownLoadCount":Z
    :cond_1f
    const-string v0, "com.sec.android.directshare.WFD_POPUP_RESULT_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 725
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: action = WFD_POPUP_RESULT_ACTION "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    const-string v0, "result"

    const/16 v1, 0xc9

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 727
    .local v11, "result":I
    const/16 v0, 0xc9

    if-ne v11, v0, :cond_20

    .line 728
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    goto/16 :goto_2

    .line 729
    :cond_20
    const/16 v0, 0xc8

    if-ne v11, v0, :cond_f

    .line 730
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 731
    const/16 v0, 0x73

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 732
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 733
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->WfdDisconnect()V

    goto/16 :goto_2

    .line 735
    .end local v11    # "result":I
    :cond_21
    const-string v0, "com.sec.android.directshare.MOBILEAP_POPUP_RESULT_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 736
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: action = MOBILEAP_POPUP_RESULT_ACTION "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    const-string v0, "result"

    const/16 v1, 0xc9

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 738
    .restart local v11    # "result":I
    const/16 v0, 0xc9

    if-ne v11, v0, :cond_22

    .line 739
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    goto/16 :goto_2

    .line 740
    :cond_22
    const/16 v0, 0xc8

    if-ne v11, v0, :cond_f

    .line 741
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 742
    const/16 v0, 0x75

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 743
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 744
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->setWifiApEnabled()V

    goto/16 :goto_2

    .line 746
    .end local v11    # "result":I
    :cond_23
    const-string v0, "com.sec.android.directshare.REQ_DOWNLOAD_INFO_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 747
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 748
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v1, 0x69

    if-ne v0, v1, :cond_24

    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_24

    .line 749
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 750
    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadPercent:I

    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I

    iget v4, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadFileName:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/directshare/DirectShareService;->sendDownloadFileInfo(ZIIILjava/lang/String;)V

    goto/16 :goto_2

    .line 752
    :cond_24
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 753
    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadPercent:I

    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I

    iget v4, p0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService;->updateDownloadFileName:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/directshare/DirectShareService;->sendDownloadFileInfo(ZIIILjava/lang/String;)V

    goto/16 :goto_2

    .line 755
    :cond_25
    const-string v0, "com.sec.android.directshare.STOP_THREAD_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 756
    const-string v0, "REQ"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 757
    .local v10, "mReq":Ljava/lang/String;
    if-eqz v10, :cond_26

    .line 758
    const-string v0, "com.sec.android.directshare.UPLOAD_CANCEL_ACTION"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 759
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: REQ_UPLOAD_CANCEL_ACTION"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->StopWebServer()Z

    .line 763
    :cond_26
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 764
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    sparse-switch v0, :sswitch_data_1

    .line 793
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 766
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 767
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/NotiManager;->cancelUpload()V

    .line 768
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mNotiManager:Lcom/sec/android/directshare/ui/NotiManager;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/NotiManager;->cancelDownload()V

    .line 769
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    goto/16 :goto_2

    .line 777
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 779
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V

    .line 780
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    goto/16 :goto_2

    .line 783
    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 784
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->sendCancel()V

    goto/16 :goto_2

    .line 787
    :sswitch_9
    const/16 v0, 0x6f

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 788
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 789
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->sendCancel()V

    .line 790
    const-string v0, "threadName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->stopDownload(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 795
    .end local v10    # "mReq":Ljava/lang/String;
    :cond_27
    const-string v0, "com.sec.android.directshare.NFC_CONNECT_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 796
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: DIRECT_SHARE_P2P_CONNECT_ACTION"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    .line 798
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStartTime:J

    .line 799
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 800
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 808
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 802
    :pswitch_0
    const/16 v0, 0x76

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 803
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 805
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWaitActionTime:J

    goto/16 :goto_2

    .line 810
    :cond_28
    const-string v0, "com.sec.android.directshare.NFC_DISCONNECT_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 811
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: DIRECT_SHARE_P2P_DISCONNECT_ACTION"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    const/16 v0, 0x1fc

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    .line 813
    iget-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mFirstNfcDisconnectTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_29

    .line 814
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mFirstNfcDisconnectTime:J

    .line 815
    :cond_29
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/directshare/DirectShareService;->mNfcState:I

    .line 816
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 817
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    sparse-switch v0, :sswitch_data_2

    .line 885
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 819
    :sswitch_a
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v0, v1, :cond_31

    .line 820
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2d

    .line 821
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    if-eqz v0, :cond_2b

    .line 822
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsSocketConnected:Z

    if-eqz v0, :cond_2a

    .line 823
    const/16 v0, 0x67

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 824
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 826
    :cond_2a
    const/16 v0, 0x6c

    const/16 v1, 0x6b

    invoke-direct {p0, v0, v1}, Lcom/sec/android/directshare/DirectShareService;->setState(II)V

    .line 827
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 830
    :cond_2b
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsSocketConnected:Z

    if-eqz v0, :cond_2c

    .line 831
    const/16 v0, 0x67

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 832
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 834
    :cond_2c
    const/16 v0, 0x6d

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 835
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 838
    :cond_2d
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_f

    .line 839
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z

    if-eqz v0, :cond_2f

    .line 841
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsSocketConnected:Z

    if-eqz v0, :cond_2e

    .line 842
    const/16 v0, 0x69

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 843
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 844
    const/16 v0, 0x1fb

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    goto/16 :goto_2

    .line 846
    :cond_2e
    const/16 v0, 0x6c

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 847
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 850
    :cond_2f
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsSocketConnected:Z

    if-eqz v0, :cond_30

    .line 851
    const/16 v0, 0x69

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 852
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 853
    const/16 v0, 0x1fb

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    goto/16 :goto_2

    .line 855
    :cond_30
    const/16 v0, 0x6d

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 856
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_2

    .line 861
    :cond_31
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_32

    .line 862
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 863
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 865
    const/16 v0, 0x1f5

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    goto/16 :goto_2

    .line 866
    :cond_32
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_f

    .line 867
    const/16 v0, 0x65

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 868
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 869
    const/16 v0, 0x1f6

    const-wide/16 v2, 0x64

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V

    goto/16 :goto_2

    .line 874
    :sswitch_b
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 875
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mIsNfcStartAction:Z

    if-nez v0, :cond_f

    .line 876
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2

    .line 880
    :sswitch_c
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 881
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2

    .line 887
    :cond_33
    const-string v0, "com.sec.android.directshare.UPLOAD_CANCEL_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 888
    new-instance v9, Landroid/content/Intent;

    const-string v0, "com.sec.android.directshare.STOP_THREAD_ACTION"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 889
    .restart local v9    # "i":Landroid/content/Intent;
    const-string v0, "threadName"

    const-string v1, "threadName"

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 890
    const-string v0, "com.sec.android.directshare"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 891
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V

    .line 892
    iget v0, p0, Lcom/sec/android/directshare/DirectShareService;->mState:I

    const/16 v1, 0x67

    if-ne v0, v1, :cond_34

    .line 893
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 894
    :cond_34
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V

    .line 895
    const/16 v0, 0x68

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->setState(I)V

    .line 896
    invoke-virtual {p0, p1}, Lcom/sec/android/directshare/DirectShareService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 897
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->StopWebServer()Z

    goto/16 :goto_2

    .line 898
    .end local v9    # "i":Landroid/content/Intent;
    :cond_35
    const-string v0, "com.sec.android.directshare.REQ_UPLOAD_INFO_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 899
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadFileName:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadPercent:I

    iget v2, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadCount:I

    iget v3, p0, Lcom/sec/android/directshare/DirectShareService;->updateUploadTotalCount:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/directshare/DirectShareService;->updateUploadFileInfo(Ljava/lang/String;III)V

    goto/16 :goto_2

    .line 900
    :cond_36
    const-string v0, "com.sec.android.directshare.SBEAM_RECORD_DETECTED_ACTION"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 902
    invoke-static {p1}, Lcom/sec/android/directshare/utils/NdefParser;->getSessionInfo(Landroid/content/Intent;)Lcom/sec/android/directshare/utils/SessionInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mSessionInfoForSender:Lcom/sec/android/directshare/utils/SessionInfo;

    goto/16 :goto_2

    .line 671
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x6a -> :sswitch_5
        0x6c -> :sswitch_5
        0x6e -> :sswitch_0
        0x71 -> :sswitch_4
        0x76 -> :sswitch_0
    .end sparse-switch

    .line 764
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_6
        0x64 -> :sswitch_7
        0x65 -> :sswitch_7
        0x66 -> :sswitch_7
        0x67 -> :sswitch_8
        0x68 -> :sswitch_7
        0x69 -> :sswitch_9
        0x70 -> :sswitch_7
        0x71 -> :sswitch_7
    .end sparse-switch

    .line 800
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 817
    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_b
        0x70 -> :sswitch_a
        0x76 -> :sswitch_c
    .end sparse-switch
.end method

.method public wifiDirect_disable()V
    .locals 2

    .prologue
    .line 1797
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 1798
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> diableP2p()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1799
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->disableP2p(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)V

    .line 1801
    :cond_0
    return-void
.end method

.method public wifiDirect_discoverPeers()V
    .locals 4

    .prologue
    .line 1816
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 1817
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> discoverPeers()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1818
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v2, 0x1

    new-instance v3, Lcom/sec/android/directshare/DirectShareService$4;

    invoke-direct {v3, p0}, Lcom/sec/android/directshare/DirectShareService$4;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ILandroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 1827
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/DirectShareService;->mDiscoverPeersTime:J

    .line 1832
    :cond_0
    return-void
.end method

.method public wifiDirect_enable()V
    .locals 2

    .prologue
    .line 1779
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/directshare/utils/Utils;->isEdmSbeamAllowed(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1780
    const/16 v0, 0xc9

    invoke-direct {p0, v0}, Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V

    .line 1781
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_cancelConnect()V

    .line 1782
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V

    .line 1783
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_disable()V

    .line 1784
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService;->mWifiDirectInit:Z

    .line 1785
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService;->stopService()V

    .line 1786
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService;->onDestroy()V

    .line 1794
    :cond_0
    :goto_0
    return-void

    .line 1790
    :cond_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 1791
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ----> enableP2p()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1792
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->enableP2p(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)V

    goto :goto_0
.end method
