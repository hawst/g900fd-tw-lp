.class Lcom/sec/android/directshare/DirectShareService$ControlThread;
.super Ljava/lang/Thread;
.source "DirectShareService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ControlThread"
.end annotation


# instance fields
.field final CLIENT_STATE_CONNECTED:I

.field final CLIENT_STATE_INIT:I

.field final CLIENT_STATE_REQ_CONNECT:I

.field mClient:Ljava/nio/channels/SocketChannel;

.field mClientState:I

.field mHostAddr:Ljava/lang/String;

.field mSelector:Ljava/nio/channels/Selector;

.field mServer:Ljava/nio/channels/ServerSocketChannel;

.field mbCanceled:Z

.field mbServer:Z

.field final synthetic this$0:Lcom/sec/android/directshare/DirectShareService;


# direct methods
.method public constructor <init>(Lcom/sec/android/directshare/DirectShareService;ZLjava/lang/String;)V
    .locals 3
    .param p2, "bOwner"    # Z
    .param p3, "hostAddr"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 3224
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2913
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbServer:Z

    .line 2914
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mHostAddr:Ljava/lang/String;

    .line 2915
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mServer:Ljava/nio/channels/ServerSocketChannel;

    .line 2916
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    .line 2917
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    .line 2918
    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbCanceled:Z

    .line 2920
    iput v2, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->CLIENT_STATE_INIT:I

    .line 2921
    iput v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->CLIENT_STATE_REQ_CONNECT:I

    .line 2922
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->CLIENT_STATE_CONNECTED:I

    .line 2923
    iput v2, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClientState:I

    .line 3225
    iput-boolean p2, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbServer:Z

    .line 3226
    iput-object p3, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mHostAddr:Ljava/lang/String;

    .line 3227
    return-void
.end method

.method private bind()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3249
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->close()V

    .line 3250
    invoke-static {}, Ljava/nio/channels/ServerSocketChannel;->open()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mServer:Ljava/nio/channels/ServerSocketChannel;

    .line 3251
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mServer:Ljava/nio/channels/ServerSocketChannel;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/channels/ServerSocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 3252
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    .line 3253
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mServer:Ljava/nio/channels/ServerSocketChannel;

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    const/16 v3, 0x10

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/ServerSocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    .line 3254
    new-instance v0, Ljava/net/InetSocketAddress;

    const/16 v1, 0x2726

    invoke-direct {v0, v1}, Ljava/net/InetSocketAddress;-><init>(I)V

    .line 3255
    .local v0, "addr":Ljava/net/SocketAddress;
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mServer:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    .line 3256
    return-void
.end method

.method private close()V
    .locals 4

    .prologue
    .line 3272
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    if-eqz v1, :cond_0

    .line 3274
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3280
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    if-eqz v1, :cond_1

    .line 3282
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 3288
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mServer:Ljava/nio/channels/ServerSocketChannel;

    if-eqz v1, :cond_2

    .line 3290
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mServer:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/ServerSocketChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 3295
    :cond_2
    :goto_2
    return-void

    .line 3275
    :catch_0
    move-exception v0

    .line 3276
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: ControlThread : Selector "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3283
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 3284
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: ControlThread : Client "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3291
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 3292
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: ControlThread : Server "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private connect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/nio/channels/ClosedSelectorException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3259
    iget v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClientState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 3260
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    .line 3261
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1, v4}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 3262
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    .line 3263
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    .line 3264
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mHostAddr:Ljava/lang/String;

    const/16 v2, 0x2726

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 3265
    .local v0, "addr":Ljava/net/InetSocketAddress;
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1, v0}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    .line 3266
    iput v4, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClientState:I

    .line 3268
    .end local v0    # "addr":Ljava/net/InetSocketAddress;
    :cond_0
    return-void
.end method

.method private read(Ljava/nio/channels/SelectionKey;Ljava/nio/ByteBuffer;)I
    .locals 5
    .param p1, "key"    # Ljava/nio/channels/SelectionKey;
    .param p2, "buf"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 3298
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/SocketChannel;

    .line 3299
    .local v0, "c":Ljava/nio/channels/SocketChannel;
    const/4 v2, 0x0

    .line 3300
    .local v2, "offset":I
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    .line 3301
    .local v1, "len":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 3302
    invoke-virtual {v0, p2}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v3

    .line 3303
    .local v3, "ret":I
    if-ne v3, v4, :cond_0

    move v2, v4

    .line 3311
    .end local v2    # "offset":I
    .end local v3    # "ret":I
    :goto_1
    return v2

    .line 3307
    .restart local v2    # "offset":I
    .restart local v3    # "ret":I
    :cond_0
    add-int/2addr v2, v3

    .line 3308
    goto :goto_0

    .line 3310
    .end local v3    # "ret":I
    :cond_1
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 32

    .prologue
    .line 2927
    invoke-super/range {p0 .. p0}, Ljava/lang/Thread;->run()V

    .line 2929
    const-wide/16 v10, 0x0

    .line 2930
    .local v10, "curTime":J
    const-wide/16 v20, 0x0

    .line 2933
    .local v20, "nextTtlTime":J
    const-wide/16 v22, 0x0

    .line 2936
    .local v22, "notiNextTime":J
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbServer:Z

    move/from16 v28, v0

    if-eqz v28, :cond_0

    .line 2937
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->bind()V

    .line 2940
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->isInterrupted()Z

    move-result v28

    if-nez v28, :cond_16

    .line 2941
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 2942
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbServer:Z

    move/from16 v28, v0

    if-nez v28, :cond_1

    .line 2943
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->connect()V

    .line 2946
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v28, v0

    const-wide/16 v30, 0x64

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/nio/channels/Selector;->select(J)I

    move-result v25

    .line 2947
    .local v25, "ret":I
    if-nez v25, :cond_3

    .line 2948
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    if-eqz v28, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbServer:Z

    move/from16 v28, v0

    if-nez v28, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClientState:I

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_0

    :cond_2
    cmp-long v28, v20, v10

    if-gez v28, :cond_0

    .line 2951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareMessage;->sendTtl(Ljava/nio/channels/SocketChannel;)V

    .line 2952
    const-wide/16 v28, 0x7d0

    add-long v20, v10, v28

    goto :goto_0

    .line 2957
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 2958
    .local v17, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    :cond_4
    :goto_1
    :pswitch_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_0

    .line 2960
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/nio/channels/SelectionKey;

    .line 2961
    .local v18, "key":Ljava/nio/channels/SelectionKey;
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    .line 2963
    invoke-virtual/range {v18 .. v18}, Ljava/nio/channels/SelectionKey;->isAcceptable()Z

    move-result v28

    if-eqz v28, :cond_8

    .line 2964
    invoke-virtual/range {v18 .. v18}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v27

    check-cast v27, Ljava/nio/channels/ServerSocketChannel;

    .line 2965
    .local v27, "serverSock":Ljava/nio/channels/ServerSocketChannel;
    invoke-virtual/range {v27 .. v27}, Ljava/nio/channels/ServerSocketChannel;->accept()Ljava/nio/channels/SocketChannel;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    .line 2966
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    if-eqz v28, :cond_4

    .line 2970
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {v28 .. v29}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 2971
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v29}, Ljava/nio/channels/ServerSocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    .line 2972
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v29, v0

    const/16 v30, 0x1

    invoke-virtual/range {v28 .. v30}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    .line 2973
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v28

    check-cast v28, Ljava/net/InetSocketAddress;

    invoke-virtual/range {v28 .. v28}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadAddress:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3802(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;

    .line 2974
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mIsSocketConnected:Z
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$6502(Lcom/sec/android/directshare/DirectShareService;Z)Z

    .line 2976
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2977
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    sparse-switch v28, :sswitch_data_0

    .line 3007
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 3213
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v18    # "key":Ljava/nio/channels/SelectionKey;
    .end local v25    # "ret":I
    .end local v27    # "serverSock":Ljava/nio/channels/ServerSocketChannel;
    :catch_0
    move-exception v13

    .line 3214
    .local v13, "e":Ljava/io/IOException;
    :try_start_1
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlThread : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3218
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->close()V

    .line 3221
    .end local v13    # "e":Ljava/io/IOException;
    :goto_2
    const-string v28, "[SBeam]"

    const-string v29, "DirectShareService: ControlThread : End of Thread."

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3222
    return-void

    .line 2979
    .restart local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v18    # "key":Ljava/nio/channels/SelectionKey;
    .restart local v25    # "ret":I
    .restart local v27    # "serverSock":Ljava/nio/channels/ServerSocketChannel;
    :sswitch_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2980
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_5

    .line 2981
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareMessage;->sendDeviceName(Ljava/nio/channels/SocketChannel;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 3215
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v18    # "key":Ljava/nio/channels/SelectionKey;
    .end local v25    # "ret":I
    .end local v27    # "serverSock":Ljava/nio/channels/ServerSocketChannel;
    :catch_1
    move-exception v13

    .line 3216
    .local v13, "e":Ljava/nio/channels/ClosedSelectorException;
    :try_start_3
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlThread : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3218
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->close()V

    goto :goto_2

    .line 2982
    .end local v13    # "e":Ljava/nio/channels/ClosedSelectorException;
    .restart local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v18    # "key":Ljava/nio/channels/SelectionKey;
    .restart local v25    # "ret":I
    .restart local v27    # "serverSock":Ljava/nio/channels/ServerSocketChannel;
    :cond_5
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_4

    .line 2983
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v28

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mTotalFile:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v28

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    const/16 v31, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/sec/android/directshare/DirectShareService$DownloadFile;

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v31, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v29

    move/from16 v1, v30

    move-object/from16 v2, v28

    move-object/from16 v3, v31

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/directshare/DirectShareMessage;->sendDownloadInfo(Ljava/nio/channels/SocketChannel;ILjava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 3218
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v18    # "key":Ljava/nio/channels/SelectionKey;
    .end local v25    # "ret":I
    .end local v27    # "serverSock":Ljava/nio/channels/ServerSocketChannel;
    :catchall_0
    move-exception v28

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->close()V

    throw v28

    .line 2990
    .restart local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v18    # "key":Ljava/nio/channels/SelectionKey;
    .restart local v25    # "ret":I
    .restart local v27    # "serverSock":Ljava/nio/channels/ServerSocketChannel;
    :sswitch_1
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_6

    .line 2991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x69

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2992
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2993
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x1fb

    const-wide/16 v30, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static/range {v28 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 2994
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v28

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mTotalFile:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v28

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    const/16 v31, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/sec/android/directshare/DirectShareService$DownloadFile;

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v31, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v29

    move/from16 v1, v30

    move-object/from16 v2, v28

    move-object/from16 v3, v31

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/directshare/DirectShareMessage;->sendDownloadInfo(Ljava/nio/channels/SocketChannel;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2998
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_7

    .line 2999
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x67

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 3000
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3001
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareMessage;->sendDeviceName(Ljava/nio/channels/SocketChannel;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3003
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_1

    .line 3010
    .end local v27    # "serverSock":Ljava/nio/channels/ServerSocketChannel;
    :cond_8
    invoke-virtual/range {v18 .. v18}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    move-result v28

    if-eqz v28, :cond_d

    .line 3011
    invoke-virtual/range {v18 .. v18}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v4

    check-cast v4, Ljava/nio/channels/SocketChannel;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3013
    .local v4, "c":Ljava/nio/channels/SocketChannel;
    :try_start_6
    invoke-virtual {v4}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    .line 3014
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClientState:I

    .line 3015
    const-string v28, "[SBeam]"

    const-string v29, "DirectShareService: ControlClientThread: connected!"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3016
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mIsSocketConnected:Z
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$6502(Lcom/sec/android/directshare/DirectShareService;Z)Z

    .line 3017
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    packed-switch v28, :pswitch_data_0

    .line 3050
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3051
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlClientThread: invalid state=["

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v31, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static/range {v31 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v31

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;
    invoke-static/range {v30 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$6200(Lcom/sec/android/directshare/DirectShareService;I)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "]"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3056
    :cond_9
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    if-eqz v28, :cond_4

    .line 3057
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v29, v0

    const/16 v30, 0x1

    invoke-virtual/range {v28 .. v30}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;
    :try_end_6
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/net/ConnectException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 3059
    :catch_2
    move-exception v6

    .line 3060
    .local v6, "cie":Ljava/nio/channels/ClosedByInterruptException;
    :try_start_7
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlThread 1: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 3020
    .end local v6    # "cie":Ljava/nio/channels/ClosedByInterruptException;
    :pswitch_2
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_a

    .line 3022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareMessage;->sendDeviceName(Ljava/nio/channels/SocketChannel;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/net/ConnectException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/net/SocketException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 3062
    :catch_3
    move-exception v5

    .line 3063
    .local v5, "ce":Ljava/net/ConnectException;
    :try_start_9
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlThread 2: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3064
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->close()V

    .line 3065
    const/16 v28, -0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClientState:I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 3024
    .end local v5    # "ce":Ljava/net/ConnectException;
    :cond_a
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_9

    .line 3025
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v28

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mTotalFile:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v28

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    const/16 v31, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/sec/android/directshare/DirectShareService$DownloadFile;

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v31, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v29

    move/from16 v1, v30

    move-object/from16 v2, v28

    move-object/from16 v3, v31

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/directshare/DirectShareMessage;->sendDownloadInfo(Ljava/nio/channels/SocketChannel;ILjava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/net/ConnectException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/net/SocketException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_3

    .line 3067
    :catch_4
    move-exception v26

    .line 3068
    .local v26, "se":Ljava/net/SocketException;
    :try_start_b
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlThread 3: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3069
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->close()V

    .line 3070
    const/16 v28, -0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClientState:I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 3032
    .end local v26    # "se":Ljava/net/SocketException;
    :pswitch_3
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_b

    .line 3033
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x69

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 3034
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3035
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x1fb

    const-wide/16 v30, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static/range {v28 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 3036
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v28

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mTotalFile:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v28

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    const/16 v31, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/sec/android/directshare/DirectShareService$DownloadFile;

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadFile;->fileName:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v31, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v29

    move/from16 v1, v30

    move-object/from16 v2, v28

    move-object/from16 v3, v31

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/directshare/DirectShareMessage;->sendDownloadInfo(Ljava/nio/channels/SocketChannel;ILjava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/net/ConnectException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/net/SocketException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_3

    .line 3072
    :catch_5
    move-exception v13

    .line 3073
    .local v13, "e":Ljava/io/IOException;
    :try_start_d
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlThread 4: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3074
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->close()V

    .line 3075
    const/16 v28, -0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClientState:I
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0

    .line 3040
    .end local v13    # "e":Ljava/io/IOException;
    :cond_b
    :try_start_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_c

    .line 3041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x67

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 3042
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3043
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceName()Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$6600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareMessage;->sendDeviceName(Ljava/nio/channels/SocketChannel;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3046
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V
    :try_end_e
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/net/ConnectException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/net/SocketException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_3

    .line 3078
    .end local v4    # "c":Ljava/nio/channels/SocketChannel;
    :cond_d
    :try_start_f
    invoke-virtual/range {v18 .. v18}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v28

    if-eqz v28, :cond_4

    .line 3079
    const/16 v28, 0x8

    invoke-static/range {v28 .. v28}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v16

    .line 3080
    .local v16, "head":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->read(Ljava/nio/channels/SelectionKey;Ljava/nio/ByteBuffer;)I

    move-result v28

    const/16 v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_f

    .line 3082
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlThread : read -1 ("

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbCanceled:Z

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ")"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3083
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbCanceled:Z

    move/from16 v28, v0

    if-nez v28, :cond_e

    .line 3084
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->sendMsgSendingToFail()V
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6800(Lcom/sec/android/directshare/DirectShareService;)V

    .line 3087
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->interrupt()V

    goto/16 :goto_0

    .line 3091
    :cond_f
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 3092
    .local v7, "cmd":I
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v19

    .line 3093
    .local v19, "len":I
    const/4 v9, 0x0

    .line 3094
    .local v9, "data":Ljava/nio/ByteBuffer;
    if-lez v19, :cond_13

    .line 3095
    invoke-static/range {v19 .. v19}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 3096
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v9}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->read(Ljava/nio/channels/SelectionKey;Ljava/nio/ByteBuffer;)I

    move-result v28

    const/16 v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_13

    .line 3097
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3098
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_12

    .line 3099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x67

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x70

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_11

    .line 3101
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->sendMsgSendingToFail()V
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6800(Lcom/sec/android/directshare/DirectShareService;)V

    .line 3107
    :cond_11
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->interrupt()V

    goto/16 :goto_0

    .line 3105
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_4

    .line 3113
    :cond_13
    packed-switch v7, :pswitch_data_1

    .line 3205
    :pswitch_4
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlServerThread : Unhandle command "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 3115
    :pswitch_5
    const-string v28, "[SBeam]"

    const-string v29, "DirectShareService: ----#cmd=[CMD_DOWNLOAD_END]"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->UpLoadstopNotification()V
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$6900(Lcom/sec/android/directshare/DirectShareService;)V

    .line 3119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    sparse-switch v28, :sswitch_data_1

    .line 3133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3134
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ControlServerThread: invalid state=["

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v31, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static/range {v31 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v31

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;
    invoke-static/range {v30 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$6200(Lcom/sec/android/directshare/DirectShareService;I)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "]"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 3122
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->uploaded()V
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$7000(Lcom/sec/android/directshare/DirectShareService;)V

    .line 3124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x1fa

    const-wide/16 v30, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static/range {v28 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 3125
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->interrupt()V

    goto/16 :goto_1

    .line 3128
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x1fa

    const-wide/16 v30, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static/range {v28 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 3130
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->interrupt()V

    goto/16 :goto_1

    .line 3141
    :pswitch_6
    if-eqz v9, :cond_4

    .line 3144
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v8

    .line 3145
    .local v8, "count":I
    const/16 v28, 0x100

    move/from16 v0, v28

    invoke-static {v9, v0}, Lcom/sec/android/directshare/DirectShareMessage;->getString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v14

    .line 3147
    .local v14, "fileName":Ljava/lang/String;
    const/16 v28, 0x40

    move/from16 v0, v28

    invoke-static {v9, v0}, Lcom/sec/android/directshare/DirectShareMessage;->getString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v12

    .line 3149
    .local v12, "deviceName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setDeviceName(Ljava/lang/String;)V
    invoke-static {v0, v12}, Lcom/sec/android/directshare/DirectShareService;->access$7100(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    .line 3150
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ----#cmd=[CMD_DOWNLOAD_INFO] fileName=["

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "], deviceName=["

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "]"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_4

    .line 3153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    # invokes: Lcom/sec/android/directshare/DirectShareService;->startUpload(Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v0, v14, v12, v8}, Lcom/sec/android/directshare/DirectShareService;->access$7200(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;I)V

    .line 3154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceAddress(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v12}, Lcom/sec/android/directshare/DirectShareService;->access$7400(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mConnectedMac:Ljava/lang/String;
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$7302(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;

    .line 3155
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ****> Connected da=["

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v31, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mConnectedMac:Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$7300(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v31

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v30 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$5100(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "]"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 3160
    .end local v8    # "count":I
    .end local v12    # "deviceName":Ljava/lang/String;
    .end local v14    # "fileName":Ljava/lang/String;
    :pswitch_7
    if-eqz v9, :cond_4

    .line 3163
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v24

    .line 3164
    .local v24, "progress":I
    const-string v28, "[SBeam]"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "DirectShareService: ----#cmd=[CMD_DOWNLOAD_STATE] "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v24

    # invokes: Lcom/sec/android/directshare/DirectShareService;->updateUploadPrgress(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$7500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 3167
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    cmp-long v28, v28, v22

    if-ltz v28, :cond_4

    .line 3169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v24

    # invokes: Lcom/sec/android/directshare/DirectShareService;->UpLoadstateNotification(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$7600(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 3170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    const-wide/16 v30, 0x12c

    add-long v22, v28, v30

    goto/16 :goto_1

    .line 3174
    .end local v24    # "progress":I
    :pswitch_8
    if-eqz v9, :cond_4

    .line 3177
    const-string v28, "[SBeam]"

    const-string v29, "DirectShareService: ----#cmd=[CMD_DEVICE_NAME]"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_14

    .line 3179
    const/16 v28, 0x40

    move/from16 v0, v28

    invoke-static {v9, v0}, Lcom/sec/android/directshare/DirectShareMessage;->getString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v12

    .line 3181
    .restart local v12    # "deviceName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setDeviceName(Ljava/lang/String;)V
    invoke-static {v0, v12}, Lcom/sec/android/directshare/DirectShareService;->access$7100(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3182
    .end local v12    # "deviceName":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_4

    .line 3183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x1f9

    const-wide/16 v30, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static/range {v28 .. v31}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 3184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    const/16 v29, 0x6b

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static/range {v28 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    goto/16 :goto_1

    .line 3188
    :pswitch_9
    const-string v28, "[SBeam]"

    const-string v29, "DirectShareService: ----#cmd=[CMD_USER_CANCEL]"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$2500(Lcom/sec/android/directshare/DirectShareService;)Landroid/os/Handler;

    move-result-object v28

    const/16 v29, 0x67

    invoke-virtual/range {v28 .. v29}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3190
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->interrupt()V

    goto/16 :goto_1

    .line 3193
    :pswitch_a
    if-eqz v9, :cond_4

    .line 3196
    const-string v28, "[SBeam]"

    const-string v29, "DirectShareService: ----#cmd=[CMD_DOWNLOAD_END_ONEFILE]"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3197
    move/from16 v0, v19

    invoke-static {v9, v0}, Lcom/sec/android/directshare/DirectShareMessage;->getString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v15

    .line 3198
    .local v15, "filePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->updateUploadCount:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$7700(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->updateUploadTotalCount:I
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$7800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_15

    .line 3199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    # operator++ for: Lcom/sec/android/directshare/DirectShareService;->updateUploadCount:I
    invoke-static/range {v28 .. v28}, Lcom/sec/android/directshare/DirectShareService;->access$7708(Lcom/sec/android/directshare/DirectShareService;)I

    .line 3200
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v29, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->updateUploadCount:I
    invoke-static/range {v29 .. v29}, Lcom/sec/android/directshare/DirectShareService;->access$7700(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v30, v0

    # getter for: Lcom/sec/android/directshare/DirectShareService;->updateUploadTotalCount:I
    invoke-static/range {v30 .. v30}, Lcom/sec/android/directshare/DirectShareService;->access$7800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v30

    move-object/from16 v0, v28

    move/from16 v1, v29

    move/from16 v2, v30

    # invokes: Lcom/sec/android/directshare/DirectShareService;->updateUploadFileCount(Ljava/lang/String;II)V
    invoke-static {v0, v15, v1, v2}, Lcom/sec/android/directshare/DirectShareService;->access$7900(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;II)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_1

    .line 3218
    .end local v7    # "cmd":I
    .end local v9    # "data":Ljava/nio/ByteBuffer;
    .end local v15    # "filePath":Ljava/lang/String;
    .end local v16    # "head":Ljava/nio/ByteBuffer;
    .end local v17    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v18    # "key":Ljava/nio/channels/SelectionKey;
    .end local v19    # "len":I
    .end local v25    # "ret":I
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->close()V

    goto/16 :goto_2

    .line 2977
    nop

    :sswitch_data_0
    .sparse-switch
        0x6c -> :sswitch_1
        0x70 -> :sswitch_0
    .end sparse-switch

    .line 3018
    :pswitch_data_0
    .packed-switch 0x6d
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 3113
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_4
        :pswitch_9
        :pswitch_4
        :pswitch_a
    .end packed-switch

    .line 3120
    :sswitch_data_1
    .sparse-switch
        0x67 -> :sswitch_2
        0x70 -> :sswitch_3
    .end sparse-switch
.end method

.method public sendCancel()V
    .locals 2

    .prologue
    .line 3234
    const-string v0, "[SBeam]"

    const-string v1, "DirectShareService: ControlThread : sendCancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mbCanceled:Z

    .line 3236
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareMessage;->sendCancel(Ljava/nio/channels/SocketChannel;)V

    .line 3237
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->sendMsgSendingToFail()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$6800(Lcom/sec/android/directshare/DirectShareService;)V

    .line 3238
    return-void
.end method

.method public sendDownloadEnd()V
    .locals 1

    .prologue
    .line 3230
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareMessage;->sendDownloadEnd(Ljava/nio/channels/SocketChannel;)V

    .line 3231
    return-void
.end method

.method public sendDownloadEndOnefile(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 3241
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    invoke-static {v0, p1}, Lcom/sec/android/directshare/DirectShareMessage;->sendDownloadEndOnefile(Ljava/nio/channels/SocketChannel;Ljava/lang/String;)V

    .line 3242
    return-void
.end method

.method public sendDownloadState(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 3245
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$ControlThread;->mClient:Ljava/nio/channels/SocketChannel;

    invoke-static {v0, p1}, Lcom/sec/android/directshare/DirectShareMessage;->sendDownloadState(Ljava/nio/channels/SocketChannel;I)V

    .line 3246
    return-void
.end method
