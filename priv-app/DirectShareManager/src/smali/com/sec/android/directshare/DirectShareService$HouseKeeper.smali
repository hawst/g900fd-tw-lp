.class Lcom/sec/android/directshare/DirectShareService$HouseKeeper;
.super Ljava/lang/Thread;
.source "DirectShareService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HouseKeeper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/DirectShareService;


# direct methods
.method private constructor <init>(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0

    .prologue
    .line 1119
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/directshare/DirectShareService;Lcom/sec/android/directshare/DirectShareService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p2, "x1"    # Lcom/sec/android/directshare/DirectShareService$1;

    .prologue
    .line 1119
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 1122
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 1124
    const-wide/16 v6, 0x0

    .line 1125
    .local v6, "nextTtl":J
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper start."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->isInterrupted()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1127
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1128
    .local v0, "curTime":J
    cmp-long v8, v6, v0

    if-gez v8, :cond_0

    .line 1130
    const-wide/16 v8, 0x7d0

    add-long v6, v0, v8

    .line 1132
    :cond_0
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x75

    if-ne v8, v9, :cond_1

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v9, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getWifiApState()I
    invoke-static {v9}, Lcom/sec/android/directshare/DirectShareService;->access$300(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v9

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mWifiApState:I
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$202(Lcom/sec/android/directshare/DirectShareService;I)I

    move-result v8

    const/16 v9, 0xb

    if-ne v8, v9, :cond_1

    .line 1134
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService:  mState == STATE_DISCONNECT_WIFI_HOTSPOT "

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1135
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService:  getWifiApState() == WifiManager.WIFI_AP_STATE_DISABLED "

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1137
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_init()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$400(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1138
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-virtual {v8}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_enable()V

    .line 1139
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1141
    :cond_1
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x71

    if-ne v8, v9, :cond_2

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mCheckWifiBroadcast:Z
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$600(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mInitWifiTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$700(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mInitWifiTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$700(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_2

    .line 1143
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: enable wifi direct"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    iget-object v9, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getWifiOnOffState()Z
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$800(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v8, 0x2

    :goto_1
    const/4 v10, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setWifiState(II)V
    invoke-static {v9, v8, v10}, Lcom/sec/android/directshare/DirectShareService;->access$900(Lcom/sec/android/directshare/DirectShareService;II)V

    .line 1146
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-virtual {v8}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_enable()V

    .line 1147
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1148
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mCheckWifiBroadcast:Z
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$602(Lcom/sec/android/directshare/DirectShareService;Z)Z

    .line 1150
    :cond_2
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mIsStartAction:Z
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1000(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mIsNfcStartAction:Z
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1100(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mP2pSendCompleteTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1200(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mP2pSendCompleteTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1200(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x7d0

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_3

    .line 1152
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: lost complete"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1153
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const-wide/16 v10, 0x0

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mP2pSendCompleteTime:J
    invoke-static {v8, v10, v11}, Lcom/sec/android/directshare/DirectShareService;->access$1202(Lcom/sec/android/directshare/DirectShareService;J)J

    .line 1154
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    if-nez v8, :cond_3

    .line 1155
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1158
    :cond_3
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mWifiDisplayCheck:Z
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1400(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mWifiHotspotCheck:Z
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1500(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStartTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1600(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1700(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/NetworkInfo$DetailedState;

    move-result-object v8

    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v8, v9, :cond_7

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStartTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1600(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/32 v10, 0xea60

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_7

    .line 1161
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: connect timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1162
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_4

    .line 1163
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0xcc

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1164
    :cond_4
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1326
    .end local v0    # "curTime":J
    :cond_5
    :goto_2
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper end."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    return-void

    .line 1144
    .restart local v0    # "curTime":J
    :cond_6
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 1168
    :cond_7
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_8

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x66

    if-ne v8, v9, :cond_8

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2000(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2000(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x3a98

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_8

    .line 1171
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectRetry:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    if-nez v8, :cond_b

    .line 1172
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # operator++ for: Lcom/sec/android/directshare/DirectShareService;->mRequestNfcConnectRetry:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2108(Lcom/sec/android/directshare/DirectShareService;)I

    .line 1173
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: request nfc connect retry"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v9, 0x1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestNfcConnect(Z)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$2200(Lcom/sec/android/directshare/DirectShareService;Z)V

    .line 1184
    :cond_8
    :goto_3
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_9

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x6c

    if-ne v8, v9, :cond_9

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2600(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x2710

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_9

    .line 1187
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: wait client timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v9, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v9}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v9

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 1189
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mNextState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2900(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 1200
    :pswitch_0
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v9, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v9}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v9

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 1210
    :cond_9
    :goto_4
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x6d

    if-ne v8, v9, :cond_a

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2600(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x1388

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_a

    .line 1212
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: connect owner timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1213
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopControlThread()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3200(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1214
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3100(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1215
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0x6b

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1217
    :cond_a
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_c

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x65

    if-ne v8, v9, :cond_c

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDiscoverPeersTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3300(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDiscoverPeersTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3300(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x4e20

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_c

    .line 1220
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: discoverpeers timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0xca

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1222
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_2

    .line 1176
    :cond_b
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: request nfc connect timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v9, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestNfcConnect(Z)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$2200(Lcom/sec/android/directshare/DirectShareService;Z)V

    .line 1178
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_stopPeerDiscovery()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2300(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1179
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2400(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1180
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2500(Lcom/sec/android/directshare/DirectShareService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x66

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_3

    .line 1191
    :pswitch_1
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v9, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v9}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v9

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 1192
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2500(Lcom/sec/android/directshare/DirectShareService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x66

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_4

    .line 1195
    :pswitch_2
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0x6b

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1196
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v9, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v9}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v9

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 1197
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3100(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_4

    .line 1225
    :cond_c
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_d

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x68

    if-ne v8, v9, :cond_d

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mConnectTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3400(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mConnectTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3400(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x4e20

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_d

    .line 1228
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v9, 0x0

    const-string v10, "Downloading file  using NFC failed - timeout"

    # invokes: Lcom/sec/android/directshare/DirectShareService;->auditLog(ZLjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/sec/android/directshare/DirectShareService;->access$3500(Lcom/sec/android/directshare/DirectShareService;ZLjava/lang/String;)V

    .line 1231
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: download connect timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1232
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0xca

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1233
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2400(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1234
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_2

    .line 1237
    :cond_d
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_e

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x69

    if-ne v8, v9, :cond_e

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3600(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0xa

    if-ge v8, v9, :cond_e

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mLastDownloadTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3700(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_e

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mLastDownloadTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3700(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0xc8

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_e

    .line 1240
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v9, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadAddress:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/directshare/DirectShareService;->access$3800(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/sec/android/directshare/DirectShareService;->downFiles(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$3900(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    .line 1242
    :cond_e
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_f

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x6d

    if-ne v8, v9, :cond_f

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2600(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x1388

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_f

    .line 1244
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: connect owner timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0xc9

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1246
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2400(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1247
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_2

    .line 1250
    :cond_f
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_10

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x6c

    if-ne v8, v9, :cond_10

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2600(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x2710

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_10

    .line 1252
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: wait client timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v9, 0xc9

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 1254
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirectCancelByCondition()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2400(Lcom/sec/android/directshare/DirectShareService;)V

    .line 1255
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_2

    .line 1258
    :cond_10
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_11

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/16 v9, 0x6f

    if-ne v8, v9, :cond_11

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateTime:J
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$2600(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    add-long/2addr v8, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_11

    .line 1260
    const-string v8, "[SBeam]"

    const-string v9, "DirectShareService: HouseKeeper: cancel or back timeout"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1261
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_2

    .line 1273
    :cond_11
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_5
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v8, v8, Lcom/sec/android/directshare/DirectShareService;->mDoActionList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v4, v8, :cond_14

    .line 1274
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v8, v8, Lcom/sec/android/directshare/DirectShareService;->mDoActionList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/directshare/DirectShareService$DoAction;

    .line 1275
    .local v2, "da":Lcom/sec/android/directshare/DirectShareService$DoAction;
    # getter for: Lcom/sec/android/directshare/DirectShareService$DoAction;->mDoAction:I
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService$DoAction;->access$4000(Lcom/sec/android/directshare/DirectShareService$DoAction;)I

    move-result v8

    const/16 v9, 0x1f4

    if-eq v8, v9, :cond_13

    # getter for: Lcom/sec/android/directshare/DirectShareService$DoAction;->mDoAction:I
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService$DoAction;->access$4000(Lcom/sec/android/directshare/DirectShareService$DoAction;)I

    move-result v8

    if-eqz v8, :cond_13

    # getter for: Lcom/sec/android/directshare/DirectShareService$DoAction;->mDoActionTime:J
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService$DoAction;->access$4100(Lcom/sec/android/directshare/DirectShareService$DoAction;)J

    move-result-wide v8

    cmp-long v8, v8, v0

    if-gez v8, :cond_13

    .line 1277
    const/4 v5, 0x1

    .line 1278
    .local v5, "remove":Z
    # getter for: Lcom/sec/android/directshare/DirectShareService$DoAction;->mDoAction:I
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService$DoAction;->access$4000(Lcom/sec/android/directshare/DirectShareService$DoAction;)I

    move-result v8

    packed-switch v8, :pswitch_data_1

    .line 1310
    const-string v8, "[SBeam]"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DirectShareService: HouseKeeper: unknown action "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    # getter for: Lcom/sec/android/directshare/DirectShareService$DoAction;->mDoAction:I
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService$DoAction;->access$4000(Lcom/sec/android/directshare/DirectShareService$DoAction;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    :cond_12
    :goto_6
    const/4 v8, 0x1

    if-ne v5, v8, :cond_13

    .line 1314
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v8, v8, Lcom/sec/android/directshare/DirectShareService;->mDoActionList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_5

    .line 1280
    :pswitch_3
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v9, 0x1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestNfcConnect(Z)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$2200(Lcom/sec/android/directshare/DirectShareService;Z)V

    .line 1281
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_Listen()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$4200(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_6

    .line 1284
    :pswitch_4
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-virtual {v8}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_discoverPeers()V

    goto :goto_6

    .line 1287
    :pswitch_5
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mWifiP2pState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$4300(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    const/4 v9, 0x2

    if-eq v8, v9, :cond_12

    .line 1288
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-virtual {v8}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_enable()V

    goto :goto_6

    .line 1292
    :pswitch_6
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestGroupInfo()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$4400(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_6

    .line 1295
    :pswitch_7
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_removeGroup()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$3100(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_6

    .line 1298
    :pswitch_8
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_6

    .line 1301
    :pswitch_9
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v9, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadAddress:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/directshare/DirectShareService;->access$3800(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/sec/android/directshare/DirectShareService;->downFiles(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$3900(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    goto :goto_6

    .line 1304
    :pswitch_a
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v9, 0x1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopDisconnectUi(I)V
    invoke-static {v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4500(Lcom/sec/android/directshare/DirectShareService;I)V

    goto :goto_6

    .line 1307
    :pswitch_b
    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$HouseKeeper;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->startControlThread()V
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$4600(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_6

    .line 1318
    .end local v5    # "remove":Z
    :cond_13
    add-int/lit8 v4, v4, 0x1

    .line 1319
    goto/16 :goto_5

    .line 1321
    .end local v2    # "da":Lcom/sec/android/directshare/DirectShareService$DoAction;
    :cond_14
    const-wide/16 v8, 0x64

    const/4 v10, 0x0

    :try_start_0
    invoke-static {v8, v9, v10}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1322
    :catch_0
    move-exception v3

    .line 1323
    .local v3, "e":Ljava/lang/InterruptedException;
    goto/16 :goto_2

    .line 1189
    :pswitch_data_0
    .packed-switch 0x6b
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1278
    :pswitch_data_1
    .packed-switch 0x1f5
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
