.class Lcom/sec/android/directshare/DirectShareService$11;
.super Landroid/os/Handler;
.source "DirectShareService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/DirectShareService;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0

    .prologue
    .line 3910
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v1, 0x7f050021

    const v0, 0x7f050020

    const/4 v5, 0x1

    .line 3914
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 3915
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: handleMessage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3916
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 3969
    :goto_0
    return-void

    .line 3918
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$9900(Lcom/sec/android/directshare/DirectShareService;)Landroid/view/ContextThemeWrapper;

    move-result-object v1

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3920
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_0

    .line 3923
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$9900(Lcom/sec/android/directshare/DirectShareService;)Landroid/view/ContextThemeWrapper;

    move-result-object v0

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 3927
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService;->access$9900(Lcom/sec/android/directshare/DirectShareService;)Landroid/view/ContextThemeWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v3

    if-ne v3, v5, :cond_0

    :goto_1
    invoke-static {v2, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3929
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 3927
    goto :goto_1

    .line 3932
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v3

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v2, v3}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3933
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 3945
    :goto_2
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3946
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_0

    .line 3935
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$9900(Lcom/sec/android/directshare/DirectShareService;)Landroid/view/ContextThemeWrapper;

    move-result-object v0

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 3939
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$9900(Lcom/sec/android/directshare/DirectShareService;)Landroid/view/ContextThemeWrapper;

    move-result-object v1

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 3949
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThemeContext:Landroid/view/ContextThemeWrapper;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$9900(Lcom/sec/android/directshare/DirectShareService;)Landroid/view/ContextThemeWrapper;

    move-result-object v0

    const v1, 0x7f050029

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 3953
    :pswitch_8
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: handleMessage - MSG_STOP_SERVICE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3954
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3955
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 3962
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 3958
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3959
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$11;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_0

    .line 3916
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 3933
    :pswitch_data_1
    .packed-switch 0x67
        :pswitch_6
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 3955
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x76 -> :sswitch_0
    .end sparse-switch
.end method
