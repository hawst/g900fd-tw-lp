.class public Lcom/sec/android/directshare/utils/NdefParser;
.super Ljava/lang/Object;
.source "NdefParser.java"


# direct methods
.method private static getNdefMessages(Landroid/content/Intent;)[Landroid/nfc/NdefMessage;
    .locals 9
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 65
    const/4 v3, 0x0

    .line 66
    .local v3, "msgs":[Landroid/nfc/NdefMessage;
    const-string v6, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p0, v6}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 67
    .local v4, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v4, :cond_0

    .line 68
    array-length v6, v4

    new-array v3, v6, [Landroid/nfc/NdefMessage;

    .line 69
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v6, v4

    if-ge v1, v6, :cond_1

    .line 70
    aget-object v6, v4, v1

    check-cast v6, Landroid/nfc/NdefMessage;

    aput-object v6, v3, v1

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    .end local v1    # "j":I
    :cond_0
    new-array v0, v7, [B

    .line 74
    .local v0, "empty":[B
    new-instance v5, Landroid/nfc/NdefRecord;

    const/4 v6, 0x5

    invoke-direct {v5, v6, v0, v0, v0}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    .line 75
    .local v5, "record":Landroid/nfc/NdefRecord;
    new-instance v2, Landroid/nfc/NdefMessage;

    new-array v6, v8, [Landroid/nfc/NdefRecord;

    aput-object v5, v6, v7

    invoke-direct {v2, v6}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    .line 78
    .local v2, "msg":Landroid/nfc/NdefMessage;
    new-array v3, v8, [Landroid/nfc/NdefMessage;

    .end local v3    # "msgs":[Landroid/nfc/NdefMessage;
    aput-object v2, v3, v7

    .line 82
    .end local v0    # "empty":[B
    .end local v2    # "msg":Landroid/nfc/NdefMessage;
    .end local v5    # "record":Landroid/nfc/NdefRecord;
    .restart local v3    # "msgs":[Landroid/nfc/NdefMessage;
    :cond_1
    return-object v3
.end method

.method private static getPayload([Landroid/nfc/NdefMessage;)Ljava/lang/String;
    .locals 4
    .param p0, "msgs"    # [Landroid/nfc/NdefMessage;

    .prologue
    const/4 v3, 0x0

    .line 56
    if-eqz p0, :cond_0

    .line 57
    aget-object v1, p0, v3

    .line 58
    .local v1, "msg":Landroid/nfc/NdefMessage;
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    .line 61
    .end local v1    # "msg":Landroid/nfc/NdefMessage;
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static getPopupMode(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    .line 22
    invoke-static {p0}, Lcom/sec/android/directshare/utils/NdefParser;->getNdefMessages(Landroid/content/Intent;)[Landroid/nfc/NdefMessage;

    move-result-object v0

    .line 23
    .local v0, "msgs":[Landroid/nfc/NdefMessage;
    invoke-static {v0}, Lcom/sec/android/directshare/utils/NdefParser;->getPayload([Landroid/nfc/NdefMessage;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getSessionInfo(Landroid/content/Intent;)Lcom/sec/android/directshare/utils/SessionInfo;
    .locals 14
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    .line 27
    invoke-static {p0}, Lcom/sec/android/directshare/utils/NdefParser;->getNdefMessages(Landroid/content/Intent;)[Landroid/nfc/NdefMessage;

    move-result-object v5

    .line 28
    .local v5, "msgs":[Landroid/nfc/NdefMessage;
    invoke-static {v5}, Lcom/sec/android/directshare/utils/NdefParser;->getPayload([Landroid/nfc/NdefMessage;)Ljava/lang/String;

    move-result-object v4

    .line 29
    .local v4, "json":Ljava/lang/String;
    new-instance v8, Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-direct {v8}, Lcom/sec/android/directshare/utils/SessionInfo;-><init>()V

    .line 30
    .local v8, "retInfo":Lcom/sec/android/directshare/utils/SessionInfo;
    const-wide/16 v10, 0x0

    .line 33
    .local v10, "totalSize":J
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 34
    .local v6, "obj":Lorg/json/JSONObject;
    const-string v9, "mac"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/directshare/utils/SessionInfo;->setMacAddr(Ljava/lang/String;)V

    .line 35
    const-string v9, "mimeType"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/directshare/utils/SessionInfo;->setMimeType(Ljava/lang/String;)V

    .line 36
    const-string v9, "list"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 37
    .local v1, "_list":Lorg/json/JSONArray;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-eqz v9, :cond_0

    .line 38
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v3, v9, :cond_0

    .line 39
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 40
    .local v7, "obj2":Lorg/json/JSONObject;
    new-instance v0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;

    invoke-direct {v0}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;-><init>()V

    .line 41
    .local v0, "_f":Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    const-string v9, "fileName"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->setFileName(Ljava/lang/String;)V

    .line 42
    const-string v9, "fileLen"

    const-wide/16 v12, -0x1

    invoke-virtual {v7, v9, v12, v13}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v12

    invoke-virtual {v0, v12, v13}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->setFilelen(J)V

    .line 43
    invoke-virtual {v0}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->getFilelen()J

    move-result-wide v12

    add-long/2addr v10, v12

    .line 44
    const-string v9, "filepath"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->setFilePath(Ljava/lang/String;)V

    .line 45
    invoke-virtual {v8, v0}, Lcom/sec/android/directshare/utils/SessionInfo;->addFileInfo(Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;)V

    .line 38
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 48
    .end local v0    # "_f":Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    .end local v3    # "j":I
    .end local v7    # "obj2":Lorg/json/JSONObject;
    :cond_0
    invoke-virtual {v8, v10, v11}, Lcom/sec/android/directshare/utils/SessionInfo;->setTotalSize(J)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .end local v1    # "_list":Lorg/json/JSONArray;
    .end local v6    # "obj":Lorg/json/JSONObject;
    :goto_1
    return-object v8

    .line 49
    :catch_0
    move-exception v2

    .line 50
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method public static isNdef(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    .line 16
    const-string v0, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    .line 18
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
