.class Lcom/sec/android/directshare/DirectShareService$WorkerThread;
.super Ljava/lang/Thread;
.source "DirectShareService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WorkerThread"
.end annotation


# instance fields
.field private final conn:Lorg/apache/http/HttpServerConnection;

.field private final httpservice:Lorg/apache/http/protocol/HttpService;

.field final synthetic this$0:Lcom/sec/android/directshare/DirectShareService;


# direct methods
.method public constructor <init>(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/ThreadGroup;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;)V
    .locals 1
    .param p2, "group"    # Ljava/lang/ThreadGroup;
    .param p3, "httpservice"    # Lorg/apache/http/protocol/HttpService;
    .param p4, "conn"    # Lorg/apache/http/HttpServerConnection;

    .prologue
    .line 2764
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    .line 2765
    const-string v0, "WorkerThread"

    invoke-direct {p0, p2, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/String;)V

    .line 2766
    iput-object p3, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->httpservice:Lorg/apache/http/protocol/HttpService;

    .line 2767
    iput-object p4, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    .line 2768
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 2771
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lorg/apache/http/protocol/BasicHttpContext;-><init>(Lorg/apache/http/protocol/HttpContext;)V

    .line 2773
    .local v0, "context":Lorg/apache/http/protocol/HttpContext;
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v3}, Lorg/apache/http/HttpServerConnection;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2774
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->httpservice:Lorg/apache/http/protocol/HttpService;

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-virtual {v3, v4, v0}, Lorg/apache/http/protocol/HttpService;->handleRequest(Lorg/apache/http/HttpServerConnection;Lorg/apache/http/protocol/HttpContext;)V
    :try_end_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2776
    :catch_0
    move-exception v1

    .line 2777
    .local v1, "ex":Lorg/apache/http/ConnectionClosedException;
    :try_start_1
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: WorkerThread 1: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2784
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v3}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2789
    .end local v1    # "ex":Lorg/apache/http/ConnectionClosedException;
    :goto_1
    const-string v3, "[SBeam]"

    const-string v4, "DirectShareService: WorkThread end."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2790
    return-void

    .line 2784
    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v3}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 2785
    :catch_1
    move-exception v2

    .line 2786
    .local v2, "ignore":Ljava/lang/Exception;
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: WorkerThread: shutdown"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2785
    .end local v2    # "ignore":Ljava/lang/Exception;
    .restart local v1    # "ex":Lorg/apache/http/ConnectionClosedException;
    :catch_2
    move-exception v2

    .line 2786
    .restart local v2    # "ignore":Ljava/lang/Exception;
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: WorkerThread: shutdown"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2778
    .end local v1    # "ex":Lorg/apache/http/ConnectionClosedException;
    .end local v2    # "ignore":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 2779
    .local v1, "ex":Ljava/io/IOException;
    :try_start_4
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: WorkerThread 2: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2784
    :try_start_5
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v3}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 2785
    :catch_4
    move-exception v2

    .line 2786
    .restart local v2    # "ignore":Ljava/lang/Exception;
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: WorkerThread: shutdown"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2780
    .end local v1    # "ex":Ljava/io/IOException;
    .end local v2    # "ignore":Ljava/lang/Exception;
    :catch_5
    move-exception v1

    .line 2781
    .local v1, "ex":Lorg/apache/http/HttpException;
    :try_start_6
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: WorkerThread 3: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2784
    :try_start_7
    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v3}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_1

    .line 2785
    :catch_6
    move-exception v2

    .line 2786
    .restart local v2    # "ignore":Ljava/lang/Exception;
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: WorkerThread: shutdown"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2783
    .end local v1    # "ex":Lorg/apache/http/HttpException;
    .end local v2    # "ignore":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 2784
    :try_start_8
    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v4}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    .line 2787
    :goto_2
    throw v3

    .line 2785
    :catch_7
    move-exception v2

    .line 2786
    .restart local v2    # "ignore":Ljava/lang/Exception;
    const-string v4, "[SBeam]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DirectShareService: WorkerThread: shutdown"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
