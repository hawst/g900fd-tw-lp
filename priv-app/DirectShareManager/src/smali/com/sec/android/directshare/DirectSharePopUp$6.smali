.class Lcom/sec/android/directshare/DirectSharePopUp$6;
.super Landroid/content/BroadcastReceiver;
.source "DirectSharePopUp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectSharePopUp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/DirectSharePopUp;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/DirectSharePopUp;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const-wide/16 v4, 0x3e8

    .line 185
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "action":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/directshare/DirectSharePopUp;->access$100(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/app/AlertDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/directshare/DirectSharePopUp;->access$100(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    const-string v2, "android.nfc.action.P2P_DISCONNECT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 190
    const-string v2, "allshare_disconnect"

    iget-object v3, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectSharePopUp;->access$200(Lcom/sec/android/directshare/DirectSharePopUp;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 191
    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/directshare/DirectSharePopUp;->access$300(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 192
    :cond_2
    const-string v2, "hotspot_disconnect"

    iget-object v3, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectSharePopUp;->access$200(Lcom/sec/android/directshare/DirectSharePopUp;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/directshare/DirectSharePopUp;->access$300(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v7, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 195
    :cond_3
    const-string v2, "com.sec.android.directshare.STOP_DISCONNECT_UI_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 196
    const-string v2, "opt"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 197
    .local v1, "check":I
    if-ne v1, v6, :cond_0

    .line 198
    const-string v2, "allshare_disconnect"

    iget-object v3, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectSharePopUp;->access$200(Lcom/sec/android/directshare/DirectSharePopUp;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 199
    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/directshare/DirectSharePopUp;->access$300(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 200
    :cond_4
    const-string v2, "hotspot_disconnect"

    iget-object v3, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectSharePopUp;->access$200(Lcom/sec/android/directshare/DirectSharePopUp;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp$6;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/directshare/DirectSharePopUp;->access$300(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v7, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
