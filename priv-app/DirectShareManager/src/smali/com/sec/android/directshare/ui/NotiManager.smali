.class public Lcom/sec/android/directshare/ui/NotiManager;
.super Ljava/lang/Object;
.source "NotiManager.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDownloadBuilder:Landroid/app/Notification$Builder;

.field private mManager:Landroid/app/NotificationManager;

.field private mUploadBuilder:Landroid/app/Notification$Builder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iput-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    .line 190
    iput-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    .line 191
    iput-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    .line 192
    iput-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    .line 19
    iput-object p1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    .line 20
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    .line 21
    return-void
.end method


# virtual methods
.method public addDownload()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 103
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v3, :cond_0

    .line 104
    const-string v3, "[SBeam]"

    const-string v4, "NotiManager : addDownload : Manager is null!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :goto_0
    return-void

    .line 107
    :cond_0
    const-string v3, "[SBeam]"

    const-string v4, "NotiManager : addDownload"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/directshare/ui/ReceiverActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "com.sec.android.directshare.ui"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    const/high16 v3, 0x20000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 111
    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 112
    const-string v3, "fromNoti"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 114
    .local v1, "actionView":Landroid/app/PendingIntent;
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.directshare.REQ_DOWNLOAD_CANCEL_ACTION"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x8000000

    invoke-static {v3, v6, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 117
    .local v0, "actionCancel":Landroid/app/PendingIntent;
    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    .line 118
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    .line 119
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    const v4, 0x1080081

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 120
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 121
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const v5, 0x7f05001b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 122
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    const/16 v4, 0x64

    invoke-virtual {v3, v4, v6, v7}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 123
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 124
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 125
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    const v4, 0x7f020002

    iget-object v5, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const/high16 v6, 0x1040000

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v0}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 126
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v4, 0xcda88

    iget-object v5, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method public addDownloaded(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 7
    .param p1, "viewIntent"    # Landroid/content/Intent;
    .param p2, "deviceName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 159
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    .line 160
    const-string v2, "[SBeam]"

    const-string v3, "NotiManager : addDownloaded : Manager is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NotiManager : addDownloaded : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const/high16 v3, 0x8000000

    invoke-static {v2, v5, p1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 166
    .local v0, "actionView":Landroid/app/PendingIntent;
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 167
    .local v1, "builder":Landroid/app/Notification$Builder;
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const v3, 0x7f05001d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 168
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const v3, 0x7f050015

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p2, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 169
    const v2, 0x1080082

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 171
    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 172
    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 173
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v3, 0x1d977

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public addUpload()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 23
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v3, :cond_0

    .line 24
    const-string v3, "[SBeam]"

    const-string v4, "NotiManager : addUpload : Manager is null!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :goto_0
    return-void

    .line 27
    :cond_0
    const-string v3, "[SBeam]"

    const-string v4, "NotiManager : addUpload"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/directshare/ui/SenderActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 29
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "com.sec.android.directshare.ui"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 31
    const-string v3, "fromNoti"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 32
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 33
    .local v1, "actionView":Landroid/app/PendingIntent;
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.directshare.UPLOAD_CANCEL_ACTION"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x8000000

    invoke-static {v3, v6, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 36
    .local v0, "actionCancel":Landroid/app/PendingIntent;
    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    .line 37
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    .line 38
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    const v4, 0x1080088

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 39
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 40
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const v5, 0x7f05001b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 41
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    const/16 v4, 0x64

    invoke-virtual {v3, v4, v6, v7}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 42
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 43
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 44
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    const v4, 0x7f020002

    iget-object v5, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const/high16 v6, 0x1040000

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v0}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 45
    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v4, 0xcda87

    iget-object v5, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method public addUploaded(Ljava/lang/String;)V
    .locals 5
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 89
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v1, :cond_0

    .line 90
    const-string v1, "[SBeam]"

    const-string v2, "NotiManager : addUploaded : Manager is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :goto_0
    return-void

    .line 93
    :cond_0
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NotiManager : addUploaded : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 95
    .local v0, "builder":Landroid/app/Notification$Builder;
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const v2, 0x7f05001c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 96
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const v2, 0x7f050028

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 97
    const v1, 0x1080089

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 98
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 99
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 100
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v2, 0x1d978

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public cancelDownload()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 152
    const-string v0, "[SBeam]"

    const-string v1, "NotiManager : cancelDownload : Manager is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :goto_0
    return-void

    .line 155
    :cond_0
    const-string v0, "[SBeam]"

    const-string v1, "NotiManager : cancelDownload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v1, 0xcda88

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public cancelUpload()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 82
    const-string v0, "[SBeam]"

    const-string v1, "NotiManager : cancelUpload : Manager is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :goto_0
    return-void

    .line 85
    :cond_0
    const-string v0, "[SBeam]"

    const-string v1, "NotiManager : cancelUpload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v1, 0xcda87

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public getDownloadId()I
    .locals 1

    .prologue
    .line 179
    const v0, 0xcda88

    return v0
.end method

.method public getDownloadNoti()Landroid/app/Notification;
    .locals 2

    .prologue
    .line 186
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 187
    .local v0, "noti":Landroid/app/Notification;
    return-object v0
.end method

.method public getUploadId()I
    .locals 1

    .prologue
    .line 176
    const v0, 0xcda87

    return v0
.end method

.method public getUploadNoti()Landroid/app/Notification;
    .locals 2

    .prologue
    .line 182
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 183
    .local v0, "noti":Landroid/app/Notification;
    return-object v0
.end method

.method public updateDownload(Ljava/lang/String;IJJ)V
    .locals 7
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "progress"    # I
    .param p3, "downloadSize"    # J
    .param p5, "totalFileSize"    # J

    .prologue
    const/4 v5, 0x0

    .line 130
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    .line 131
    const-string v2, "[SBeam]"

    const-string v3, "NotiManager : updateDownload : Manager is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    if-nez v2, :cond_1

    .line 134
    const-string v2, "[SBeam]"

    const-string v3, "NotiManager : updateDownload : DownloadNoti is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 137
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p5, v2

    if-gtz v2, :cond_2

    .line 138
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NotiManager : updateDownload : invalid totalFileSize! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 143
    :cond_2
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p3, p4}, Lcom/sec/android/directshare/utils/Utils;->getFileSizeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "size":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p5, p6}, Lcom/sec/android/directshare/utils/Utils;->getFileSizeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "totalSize":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const v4, 0x7f050014

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 146
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 147
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    const/16 v3, 0x64

    invoke-virtual {v2, v3, p2, v5}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 148
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v3, 0xcda88

    iget-object v4, p0, Lcom/sec/android/directshare/ui/NotiManager;->mDownloadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public updateUpload(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "fileCount"    # I

    .prologue
    const v3, 0x7f050016

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 48
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 49
    const-string v0, "[SBeam]"

    const-string v1, "NotiManager : updateUpload : Manager is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    if-nez v0, :cond_1

    .line 52
    const-string v0, "[SBeam]"

    const-string v1, "NotiManager : updateUpload : UploadNoti is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 55
    :cond_1
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NotiManager : updateUpload : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 58
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v5, v5}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 59
    if-ne p3, v4, :cond_2

    .line 60
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 67
    :goto_1
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v1, 0xcda87

    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 62
    :cond_2
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p2, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mContext:Landroid/content/Context;

    const v3, 0x7f050017

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    add-int/lit8 v4, p3, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_1
.end method

.method public updateUploadProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 71
    const-string v0, "[SBeam]"

    const-string v1, "NotiManager : updateUploadProgress : Manager is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    if-nez v0, :cond_1

    .line 74
    const-string v0, "[SBeam]"

    const-string v1, "NotiManager : updateUploadProgress : UploadNoti is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    const/16 v1, 0x64

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 78
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NotiManager;->mManager:Landroid/app/NotificationManager;

    const v1, 0xcda87

    iget-object v2, p0, Lcom/sec/android/directshare/ui/NotiManager;->mUploadBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method
