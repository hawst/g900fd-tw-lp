.class Lcom/sec/android/directshare/ui/SenderActivity$3;
.super Landroid/content/BroadcastReceiver;
.source "SenderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/ui/SenderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/ui/SenderActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/ui/SenderActivity;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 311
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "action":Ljava/lang/String;
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SenderActivity: onReceive > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const-string v3, "com.sec.android.directshare.UPLOAD_START_ACTION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 321
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->stopAniPreparing()V
    invoke-static {v3}, Lcom/sec/android/directshare/ui/SenderActivity;->access$100(Lcom/sec/android/directshare/ui/SenderActivity;)V

    .line 322
    # setter for: Lcom/sec/android/directshare/ui/SenderActivity;->mUploadState:I
    invoke-static {v6}, Lcom/sec/android/directshare/ui/SenderActivity;->access$202(I)I

    .line 328
    const-string v3, "filename"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "fileName":Ljava/lang/String;
    const-string v3, "count"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 330
    .local v2, "totalCount":I
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->updateFileName(Ljava/lang/String;)V
    invoke-static {v3, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->access$300(Lcom/sec/android/directshare/ui/SenderActivity;Ljava/lang/String;)V

    .line 331
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->updateFileMore(I)V
    invoke-static {v3, v2}, Lcom/sec/android/directshare/ui/SenderActivity;->access$400(Lcom/sec/android/directshare/ui/SenderActivity;I)V

    .line 332
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->startUploadUi()V
    invoke-static {v3}, Lcom/sec/android/directshare/ui/SenderActivity;->access$500(Lcom/sec/android/directshare/ui/SenderActivity;)V

    .line 352
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "totalCount":I
    :cond_0
    :goto_0
    return-void

    .line 333
    :cond_1
    const-string v3, "com.sec.android.directshare.UPLOAD_STATE_ACTION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 334
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    const-string v4, "progress"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->updateProgress(I)V
    invoke-static {v3, v4}, Lcom/sec/android/directshare/ui/SenderActivity;->access$600(Lcom/sec/android/directshare/ui/SenderActivity;I)V

    goto :goto_0

    .line 335
    :cond_2
    const-string v3, "com.sec.android.directshare.UPLOAD_COMPLETED_ACTION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 336
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    const/16 v4, 0x64

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->updateProgress(I)V
    invoke-static {v3, v4}, Lcom/sec/android/directshare/ui/SenderActivity;->access$600(Lcom/sec/android/directshare/ui/SenderActivity;I)V

    .line 337
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    invoke-virtual {v3}, Lcom/sec/android/directshare/ui/SenderActivity;->finish()V

    goto :goto_0

    .line 338
    :cond_3
    const-string v3, "com.sec.android.directshare.STOP_SERVICE_ACTION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 339
    const/4 v3, -0x1

    # setter for: Lcom/sec/android/directshare/ui/SenderActivity;->mUploadState:I
    invoke-static {v3}, Lcom/sec/android/directshare/ui/SenderActivity;->access$202(I)I

    .line 340
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    invoke-virtual {v3}, Lcom/sec/android/directshare/ui/SenderActivity;->finish()V

    goto :goto_0

    .line 341
    :cond_4
    const-string v3, "com.sec.android.directshare.UPLOAD_FILEINFO_ACTION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 342
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    const-string v4, "progress"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->updateProgress(I)V
    invoke-static {v3, v4}, Lcom/sec/android/directshare/ui/SenderActivity;->access$600(Lcom/sec/android/directshare/ui/SenderActivity;I)V

    .line 343
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    const-string v4, "filename"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->updateFileName(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/directshare/ui/SenderActivity;->access$300(Lcom/sec/android/directshare/ui/SenderActivity;Ljava/lang/String;)V

    .line 344
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity$3;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    const-string v4, "totalCount"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->updateFileMore(I)V
    invoke-static {v3, v4}, Lcom/sec/android/directshare/ui/SenderActivity;->access$400(Lcom/sec/android/directshare/ui/SenderActivity;I)V

    goto :goto_0
.end method
