.class Lcom/sec/android/directshare/DirectSharePopUp$7;
.super Landroid/os/Handler;
.source "DirectSharePopUp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectSharePopUp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/DirectSharePopUp;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/DirectSharePopUp;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0x3e8

    const/16 v1, 0xc9

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 210
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$100(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$100(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v4, :cond_3

    .line 215
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$400(Lcom/sec/android/directshare/DirectSharePopUp;)I

    move-result v0

    if-le v0, v4, :cond_2

    .line 216
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # operator-- for: Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$410(Lcom/sec/android/directshare/DirectSharePopUp;)I

    .line 217
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$100(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    const v2, 0x7f050012

    invoke-virtual {v1, v2}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectSharePopUp;->access$400(Lcom/sec/android/directshare/DirectSharePopUp;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$300(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # invokes: Lcom/sec/android/directshare/DirectSharePopUp;->resultAction(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectSharePopUp;->access$000(Lcom/sec/android/directshare/DirectSharePopUp;I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->finish()V

    goto :goto_0

    .line 224
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v6, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$400(Lcom/sec/android/directshare/DirectSharePopUp;)I

    move-result v0

    if-le v0, v4, :cond_4

    .line 226
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # operator-- for: Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$410(Lcom/sec/android/directshare/DirectSharePopUp;)I

    .line 227
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$100(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    const v2, 0x7f050013

    invoke-virtual {v1, v2}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectSharePopUp;->access$400(Lcom/sec/android/directshare/DirectSharePopUp;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # getter for: Lcom/sec/android/directshare/DirectSharePopUp;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->access$300(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 231
    :cond_4
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    # invokes: Lcom/sec/android/directshare/DirectSharePopUp;->resultAction(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectSharePopUp;->access$000(Lcom/sec/android/directshare/DirectSharePopUp;I)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp$7;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectSharePopUp;->finish()V

    goto/16 :goto_0
.end method
