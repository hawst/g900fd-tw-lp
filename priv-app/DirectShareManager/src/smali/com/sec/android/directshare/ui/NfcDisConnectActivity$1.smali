.class Lcom/sec/android/directshare/ui/NfcDisConnectActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "NfcDisConnectActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/ui/NfcDisConnectActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/ui/NfcDisConnectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/ui/NfcDisConnectActivity;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity$1;->this$0:Lcom/sec/android/directshare/ui/NfcDisConnectActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 70
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.sec.android.directshare.STOP_DISCONNECT_UI_ACTION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    const-string v2, "opt"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 73
    .local v1, "opt":I
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareNfcDisConnect: onReceive: FINISH_NFC_DISCONNECT_ACTIVITY, opt=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 76
    const-string v2, "[SBeam]"

    const-string v3, "DirectShareNfcDisConnect: ----> onReceive: finish"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity$1;->this$0:Lcom/sec/android/directshare/ui/NfcDisConnectActivity;

    invoke-virtual {v2}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->finish()V

    .line 84
    .end local v1    # "opt":I
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    const-string v2, "com.sec.android.directshare.CLIENT_SERVICE_STOP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 80
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity$1;->this$0:Lcom/sec/android/directshare/ui/NfcDisConnectActivity;

    invoke-virtual {v2}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->finish()V

    goto :goto_0

    .line 81
    :cond_2
    const-string v2, "android.nfc.action.P2P_DISCONNECT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    iget-object v2, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity$1;->this$0:Lcom/sec/android/directshare/ui/NfcDisConnectActivity;

    invoke-virtual {v2}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->finish()V

    goto :goto_0
.end method
