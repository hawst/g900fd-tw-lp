.class Lcom/sec/android/directshare/DirectShareService$FileDownThread;
.super Ljava/lang/Thread;
.source "DirectShareService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FileDownThread"
.end annotation


# instance fields
.field mDownThreadFlag:Z

.field mFileLength:J

.field mFileName:Ljava/lang/String;

.field mMaxfilesize:J

.field mMimeType:Ljava/lang/String;

.field mOldFileName:Ljava/lang/String;

.field private mScanClient:Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;

.field mServerIp:Ljava/lang/String;

.field mSubPath:Ljava/lang/String;

.field mTotalReadBytes:J

.field msc:Landroid/media/MediaScannerConnection;

.field final synthetic this$0:Lcom/sec/android/directshare/DirectShareService;


# direct methods
.method public constructor <init>(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V
    .locals 4
    .param p2, "serverIp"    # Ljava/lang/String;
    .param p3, "subPath"    # Ljava/lang/String;
    .param p4, "fileName"    # Ljava/lang/String;
    .param p5, "total"    # J
    .param p7, "mimeType"    # Ljava/lang/String;
    .param p8, "maxfilesize"    # J

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 3329
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 3317
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mServerIp:Ljava/lang/String;

    .line 3318
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mSubPath:Ljava/lang/String;

    .line 3319
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    .line 3320
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mOldFileName:Ljava/lang/String;

    .line 3321
    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    .line 3322
    iput-wide v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMaxfilesize:J

    .line 3323
    iput-wide v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileLength:J

    .line 3324
    iput-wide v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mTotalReadBytes:J

    .line 3326
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mDownThreadFlag:Z

    .line 3552
    new-instance v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;-><init>(Lcom/sec/android/directshare/DirectShareService$FileDownThread;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mScanClient:Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;

    .line 3330
    iput-object p2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mServerIp:Ljava/lang/String;

    .line 3331
    iput-object p4, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    .line 3332
    iput-object p4, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mOldFileName:Ljava/lang/String;

    .line 3333
    iput-object p3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mSubPath:Ljava/lang/String;

    .line 3334
    iput-wide p5, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileLength:J

    .line 3335
    iput-object p7, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    .line 3336
    iput-wide p8, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMaxfilesize:J

    .line 3337
    iput-wide v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mTotalReadBytes:J

    .line 3338
    return-void
.end method

.method private DelNoMedia()V
    .locals 3

    .prologue
    .line 3382
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService;->access$8000(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/.nomedia"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3384
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3386
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3390
    :cond_0
    :goto_0
    return-void

    .line 3387
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private downloadFile(Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Ljava/lang/String;J)Z
    .locals 31
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "total"    # J
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "oldFileName"    # Ljava/lang/String;
    .param p7, "maxfilesize"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 3654
    const/4 v2, 0x0

    .line 3655
    .local v2, "con":Ljava/net/HttpURLConnection;
    const/4 v10, 0x0

    .line 3657
    .local v10, "connectUrl":Ljava/net/URL;
    const v3, 0x8000

    new-array v13, v3, [B

    .line 3658
    .local v13, "downloadBuffer":[B
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 3659
    .local v14, "curTime":J
    move-wide/from16 v28, v14

    .line 3661
    .local v28, "start":J
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 3662
    const-wide/16 v4, 0x7530

    add-long v4, v4, v28

    cmp-long v3, v4, v14

    if-gez v3, :cond_0

    .line 3663
    const-string v3, "[SBeam]"

    const-string v4, "DirectShareService: downloadFile: connect timeout"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3664
    const/4 v3, 0x0

    .line 3798
    :goto_1
    return v3

    .line 3666
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$9100(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->getFlag()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3668
    if-eqz v2, :cond_1

    .line 3669
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 3670
    const/4 v2, 0x0

    .line 3672
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 3675
    :cond_2
    :try_start_0
    new-instance v11, Ljava/net/URL;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 3676
    .end local v10    # "connectUrl":Ljava/net/URL;
    .local v11, "connectUrl":Ljava/net/URL;
    :try_start_1
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: downloadFile : ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3678
    sget-object v3, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual {v11, v3}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v2, v0

    .line 3679
    const-string v3, "GET"

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 3680
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 3681
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDefaultUseCaches(Z)V

    .line 3682
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 3683
    const/16 v3, 0x7530

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 3684
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 3685
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v25

    .local v25, "responseCode":I
    const/16 v3, 0xc8

    move/from16 v0, v25

    if-eq v0, v3, :cond_6

    .line 3686
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: downloadFile: invalid response code=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3688
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 3689
    const/4 v3, 0x0

    move-object v10, v11

    .end local v11    # "connectUrl":Ljava/net/URL;
    .restart local v10    # "connectUrl":Ljava/net/URL;
    goto/16 :goto_1

    .line 3692
    .end local v25    # "responseCode":I
    :catch_0
    move-exception v27

    .line 3693
    .local v27, "ste":Ljava/net/SocketTimeoutException;
    :goto_2
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: downloadFile: ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3694
    if-eqz v2, :cond_3

    .line 3695
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 3696
    const/4 v2, 0x0

    .line 3698
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 3699
    .end local v27    # "ste":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v26

    .line 3700
    .local v26, "se":Ljava/net/SocketException;
    :goto_3
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: downloadFile: ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3701
    if-eqz v2, :cond_4

    .line 3702
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 3703
    const/4 v2, 0x0

    .line 3705
    :cond_4
    const/4 v10, 0x0

    .line 3706
    const-wide/16 v4, 0x64

    invoke-static {v4, v5}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->sleep(J)V

    goto/16 :goto_0

    .line 3707
    .end local v26    # "se":Ljava/net/SocketException;
    :catch_2
    move-exception v19

    .line 3708
    .local v19, "ie":Ljava/io/IOException;
    :goto_4
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: downloadFile: ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3709
    if-eqz v2, :cond_5

    .line 3710
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 3711
    const/4 v2, 0x0

    .line 3713
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 3716
    .end local v10    # "connectUrl":Ljava/net/URL;
    .end local v19    # "ie":Ljava/io/IOException;
    .restart local v11    # "connectUrl":Ljava/net/URL;
    .restart local v25    # "responseCode":I
    :cond_6
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v20

    .line 3717
    .local v20, "inputStream":Ljava/io/InputStream;
    new-instance v18, Ljava/io/FileOutputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 3718
    .local v18, "fos":Ljava/io/FileOutputStream;
    const/16 v24, 0x0

    .line 3719
    .local v24, "readBytes":I
    const/4 v12, 0x0

    .line 3720
    .local v12, "count":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v9

    move-wide/from16 v4, p3

    move-wide/from16 v6, p7

    move-object/from16 v8, p6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->startDownload(JJLjava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v3 .. v9}, Lcom/sec/android/directshare/DirectShareService;->access$9200(Lcom/sec/android/directshare/DirectShareService;JJLjava/lang/String;Ljava/lang/String;)V

    .line 3722
    const-wide/16 v22, 0x0

    .line 3724
    .local v22, "notiNextTime":J
    :cond_7
    :goto_5
    :try_start_2
    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/io/InputStream;->read([B)I

    move-result v24

    const/4 v3, -0x1

    move/from16 v0, v24

    if-eq v0, v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$9100(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->getFlag()Z

    move-result v3

    if-nez v3, :cond_a

    .line 3725
    const/4 v3, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v13, v3, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 3726
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v4}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v4

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v3, v4}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3727
    add-int/lit8 v12, v12, 0x1

    .line 3728
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mTotalReadBytes:J

    move/from16 v0, v24

    int-to-long v6, v0

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mTotalReadBytes:J

    .line 3729
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v4, v3, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    move/from16 v0, v24

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    .line 3730
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v4, v3, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v6, v3, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    div-long/2addr v4, v6

    long-to-int v0, v4

    move/from16 v21, v0

    .line 3731
    .local v21, "p":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 3732
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mLastPercent:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$9300(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v3

    move/from16 v0, v21

    if-ge v3, v0, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mNextDownloadStateTime:J
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$9400(Lcom/sec/android/directshare/DirectShareService;)J

    move-result-wide v4

    cmp-long v3, v4, v14

    if-gez v3, :cond_9

    .line 3733
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move/from16 v0, v21

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mLastPercent:I
    invoke-static {v3, v0}, Lcom/sec/android/directshare/DirectShareService;->access$9302(Lcom/sec/android/directshare/DirectShareService;I)I

    .line 3737
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const-wide/16 v4, 0x64

    add-long/2addr v4, v14

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mNextDownloadStateTime:J
    invoke-static {v3, v4, v5}, Lcom/sec/android/directshare/DirectShareService;->access$9402(Lcom/sec/android/directshare/DirectShareService;J)J

    .line 3738
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v4}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v4

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v3, v4}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3740
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$4800(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$ControlThread;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 3741
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mControlThread:Lcom/sec/android/directshare/DirectShareService$ControlThread;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$4800(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$ControlThread;

    move-result-object v3

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/sec/android/directshare/DirectShareService$ControlThread;->sendDownloadState(I)V

    .line 3744
    :cond_8
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: ----#cmd= send[CMD_DOWNLOAD_STATE] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3745
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v4, v4, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I
    invoke-static {v7}, Lcom/sec/android/directshare/DirectShareService;->access$9500(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget v8, v8, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    # invokes: Lcom/sec/android/directshare/DirectShareService;->updateDownloadProgress(JLjava/lang/String;II)V
    invoke-static/range {v3 .. v8}, Lcom/sec/android/directshare/DirectShareService;->access$9600(Lcom/sec/android/directshare/DirectShareService;JLjava/lang/String;II)V

    .line 3747
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v3, v4, v22

    if-ltz v3, :cond_7

    .line 3749
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v6, v5, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    # invokes: Lcom/sec/android/directshare/DirectShareService;->DownLoadstateNotification(Ljava/lang/String;J)V
    invoke-static {v3, v4, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$9700(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;J)V

    .line 3750
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x12c

    add-long v22, v4, v6

    goto/16 :goto_5

    .line 3754
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v4}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v4

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v3, v4}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_5

    .line 3766
    .end local v21    # "p":I
    :catch_3
    move-exception v16

    .line 3767
    .local v16, "e":Ljava/io/IOException;
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectShareService: downloadFile: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3768
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 3769
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V

    .line 3770
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 3771
    const/4 v3, 0x0

    move-object v10, v11

    .end local v11    # "connectUrl":Ljava/net/URL;
    .restart local v10    # "connectUrl":Ljava/net/URL;
    goto/16 :goto_1

    .line 3773
    .end local v10    # "connectUrl":Ljava/net/URL;
    .end local v16    # "e":Ljava/io/IOException;
    .restart local v11    # "connectUrl":Ljava/net/URL;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThread:Lcom/sec/android/directshare/DirectShareService$FileDownThread;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$9100(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->getFlag()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 3775
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 3776
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V

    .line 3777
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 3778
    const/4 v3, 0x0

    move-object v10, v11

    .end local v11    # "connectUrl":Ljava/net/URL;
    .restart local v10    # "connectUrl":Ljava/net/URL;
    goto/16 :goto_1

    .line 3780
    .end local v10    # "connectUrl":Ljava/net/URL;
    .restart local v11    # "connectUrl":Ljava/net/URL;
    :cond_b
    rem-int/lit8 v3, v12, 0xa

    if-eqz v3, :cond_c

    .line 3781
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v4, v4, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I
    invoke-static {v7}, Lcom/sec/android/directshare/DirectShareService;->access$9500(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget v8, v8, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    # invokes: Lcom/sec/android/directshare/DirectShareService;->updateDownloadProgress(JLjava/lang/String;II)V
    invoke-static/range {v3 .. v8}, Lcom/sec/android/directshare/DirectShareService;->access$9600(Lcom/sec/android/directshare/DirectShareService;JLjava/lang/String;II)V

    .line 3783
    :cond_c
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    .line 3784
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V

    .line 3785
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 3786
    const/16 v17, 0x0

    .line 3787
    .local v17, "filePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    const-string v4, "text/DirectShareSNote"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 3788
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getSnbFolderName()Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/directshare/DirectShareService;->access$8100(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 3794
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$9500(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget v4, v4, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    if-ge v3, v4, :cond_d

    .line 3795
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # operator++ for: Lcom/sec/android/directshare/DirectShareService;->updateDownloadCount:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$9508(Lcom/sec/android/directshare/DirectShareService;)I

    .line 3797
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, v17

    # invokes: Lcom/sec/android/directshare/DirectShareService;->sendDownloadEndOnefile(Ljava/lang/String;)V
    invoke-static {v3, v0}, Lcom/sec/android/directshare/DirectShareService;->access$9800(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    .line 3798
    const/4 v3, 0x1

    move-object v10, v11

    .end local v11    # "connectUrl":Ljava/net/URL;
    .restart local v10    # "connectUrl":Ljava/net/URL;
    goto/16 :goto_1

    .line 3791
    .end local v10    # "connectUrl":Ljava/net/URL;
    .restart local v11    # "connectUrl":Ljava/net/URL;
    :cond_e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/directshare/DirectShareService;->access$8000(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto :goto_6

    .line 3707
    .end local v12    # "count":I
    .end local v17    # "filePath":Ljava/lang/String;
    .end local v18    # "fos":Ljava/io/FileOutputStream;
    .end local v20    # "inputStream":Ljava/io/InputStream;
    .end local v22    # "notiNextTime":J
    .end local v24    # "readBytes":I
    .end local v25    # "responseCode":I
    :catch_4
    move-exception v19

    move-object v10, v11

    .end local v11    # "connectUrl":Ljava/net/URL;
    .restart local v10    # "connectUrl":Ljava/net/URL;
    goto/16 :goto_4

    .line 3699
    .end local v10    # "connectUrl":Ljava/net/URL;
    .restart local v11    # "connectUrl":Ljava/net/URL;
    :catch_5
    move-exception v26

    move-object v10, v11

    .end local v11    # "connectUrl":Ljava/net/URL;
    .restart local v10    # "connectUrl":Ljava/net/URL;
    goto/16 :goto_3

    .line 3692
    .end local v10    # "connectUrl":Ljava/net/URL;
    .restart local v11    # "connectUrl":Ljava/net/URL;
    :catch_6
    move-exception v27

    move-object v10, v11

    .end local v11    # "connectUrl":Ljava/net/URL;
    .restart local v10    # "connectUrl":Ljava/net/URL;
    goto/16 :goto_2
.end method

.method private onError(ZJ)V
    .locals 6
    .param p1, "flag"    # Z
    .param p2, "fileLength"    # J

    .prologue
    const/16 v2, 0xcd

    .line 3508
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3509
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 3546
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3547
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onError: invalid state=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v3

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/directshare/DirectShareService;->access$6200(Lcom/sec/android/directshare/DirectShareService;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3550
    :cond_0
    :goto_0
    return-void

    .line 3511
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3512
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mOldFileName:Ljava/lang/String;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->removeDownloadList(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$8500(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    .line 3513
    if-nez p1, :cond_0

    .line 3514
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->mTotalFile:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 3515
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v0, v2}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 3516
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_0

    .line 3518
    :cond_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3519
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 3520
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget v1, v0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    .line 3521
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v2, v0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    iget-wide v4, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileLength:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    .line 3522
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v2, v0, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    iget-wide v4, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mTotalReadBytes:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    .line 3524
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onError: mMaxCount=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget v2, v2, Lcom/sec/android/directshare/DirectShareService;->mMaxCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3525
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onError: mMaxFileSize=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v2, v2, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], mSumReadByte=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v2, v2, Lcom/sec/android/directshare/DirectShareService;->mSumReadByte:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3534
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 3527
    :cond_3
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 3528
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadedCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$8200(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v0

    if-nez v0, :cond_4

    .line 3529
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v0, v2}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    goto :goto_1

    .line 3530
    :cond_4
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$8400(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v0

    if-nez v0, :cond_2

    .line 3531
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_1

    .line 3539
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3540
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mOldFileName:Ljava/lang/String;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->removeDownloadList(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$8500(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    .line 3541
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$3600(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v0

    if-nez v0, :cond_0

    .line 3542
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_0

    .line 3509
    nop

    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_0
        0x6f -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public FileCheck(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 3351
    invoke-direct {p0}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->DelNoMedia()V

    .line 3352
    const/4 v1, 0x0

    .line 3353
    .local v1, "count":I
    move-object v7, p1

    .line 3354
    .local v7, "realFile":Ljava/lang/String;
    const-string v5, ""

    .line 3355
    .local v5, "fileNm":Ljava/lang/String;
    const-string v3, ""

    .line 3357
    .local v3, "fileExt":Ljava/lang/String;
    :try_start_0
    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 3358
    const/4 v8, 0x0

    const-string v9, "."

    invoke-virtual {v7, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 3359
    const-string v8, "("

    invoke-virtual {v5, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    const-string v9, ")"

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 3361
    .local v0, "bracket":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    const-string v9, ")"

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    if-ne v8, v9, :cond_1

    .line 3362
    const/4 v8, 0x0

    const-string v9, "("

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 3363
    .local v4, "fileName":Ljava/lang/String;
    const-string v8, "[0-9]+"

    invoke-virtual {v0, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    .line 3364
    .local v6, "isNumber":Z
    if-eqz v6, :cond_0

    .line 3365
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 3366
    add-int/lit8 v1, v1, 0x1

    .line 3367
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3368
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .line 3378
    .end local v0    # "bracket":Ljava/lang/String;
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v6    # "isNumber":Z
    :goto_0
    return-object v8

    .line 3370
    .restart local v0    # "bracket":Ljava/lang/String;
    .restart local v4    # "fileName":Ljava/lang/String;
    .restart local v6    # "isNumber":Z
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "(1)."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 3373
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v6    # "isNumber":Z
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "(1)."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_0

    .line 3375
    .end local v0    # "bracket":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 3376
    .local v2, "ex":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "(1)."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method public getFlag()Z
    .locals 1

    .prologue
    .line 3347
    iget-boolean v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mDownThreadFlag:Z

    return v0
.end method

.method public run()V
    .locals 22

    .prologue
    .line 3394
    new-instance v14, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$8000(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v14, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3395
    .local v14, "exPath":Ljava/io/File;
    new-instance v21, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/directshare/DirectShareService;->access$8000(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/TMP"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-direct {v0, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3396
    .local v21, "tmpPath":Ljava/io/File;
    const/16 v19, 0x0

    .line 3397
    .local v19, "s_notePath":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3398
    invoke-virtual {v14}, Ljava/io/File;->mkdir()Z

    .line 3400
    :cond_0
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3401
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->mkdir()Z

    .line 3404
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    const-string v6, "text/DirectShareSNote"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3405
    new-instance v19, Ljava/io/File;

    .end local v19    # "s_notePath":Ljava/io/File;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getSnbFolderName()Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$8100(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-direct {v0, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3406
    .restart local v19    # "s_notePath":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 3407
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->mkdir()Z

    .line 3410
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mDownThreadFlag:Z

    if-nez v3, :cond_5

    .line 3412
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mSubPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v20

    .line 3413
    .local v20, "subPath":Ljava/lang/String;
    const-string v15, ""

    .line 3414
    .local v15, "extraSubPath":Ljava/lang/String;
    const-string v3, "/"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3415
    .local v12, "arrSubPath":[Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    array-length v3, v12

    move/from16 v0, v16

    if-ge v0, v3, :cond_3

    .line 3416
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v6, v12, v16

    const-string v7, "UTF-8"

    invoke-static {v6, v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 3415
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 3419
    :cond_3
    move-object/from16 v20, v15

    .line 3420
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-static {v3, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3421
    .local v2, "FileName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 3423
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "http://"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mServerIp:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ":15000"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3424
    .local v4, "url":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ".tmp"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-direct {v5, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3425
    .local v5, "f":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_6

    .line 3426
    invoke-virtual {v5, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3434
    :goto_1
    const/16 v17, 0x0

    .line 3436
    .local v17, "isOK":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileLength:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mOldFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-wide v10, v3, Lcom/sec/android/directshare/DirectShareService;->mMaxFileSize:J

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v11}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->downloadFile(Ljava/lang/String;Ljava/io/File;JLjava/lang/String;Ljava/lang/String;J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v17

    .line 3452
    if-nez v17, :cond_7

    .line 3453
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mDownThreadFlag:Z

    if-nez v3, :cond_4

    .line 3454
    const-string v3, "[SBeam]"

    const-string v6, "DirectShareService: FileDownThread: downloadFile() failed"

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3456
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Downloading file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " from device "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mServerIp:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " using NFC"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " failed"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/sec/android/directshare/DirectShareService;->auditLog(ZLjava/lang/String;)V
    invoke-static {v3, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$3500(Lcom/sec/android/directshare/DirectShareService;ZLjava/lang/String;)V

    .line 3460
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mDownThreadFlag:Z

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileLength:J

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v7}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->onError(ZJ)V

    .line 3505
    .end local v2    # "FileName":Ljava/lang/String;
    .end local v4    # "url":Ljava/lang/String;
    .end local v5    # "f":Ljava/io/File;
    .end local v12    # "arrSubPath":[Ljava/lang/String;
    .end local v15    # "extraSubPath":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v17    # "isOK":Z
    .end local v20    # "subPath":Ljava/lang/String;
    :cond_5
    :goto_2
    return-void

    .line 3429
    .restart local v2    # "FileName":Ljava/lang/String;
    .restart local v4    # "url":Ljava/lang/String;
    .restart local v5    # "f":Ljava/io/File;
    .restart local v12    # "arrSubPath":[Ljava/lang/String;
    .restart local v15    # "extraSubPath":Ljava/lang/String;
    .restart local v16    # "i":I
    .restart local v20    # "subPath":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->FileCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    .line 3430
    new-instance v5, Ljava/io/File;

    .end local v5    # "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    invoke-direct {v5, v14, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3431
    .restart local v5    # "f":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_6

    .line 3432
    invoke-virtual {v5, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 3499
    .end local v2    # "FileName":Ljava/lang/String;
    .end local v4    # "url":Ljava/lang/String;
    .end local v5    # "f":Ljava/io/File;
    .end local v12    # "arrSubPath":[Ljava/lang/String;
    .end local v15    # "extraSubPath":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v20    # "subPath":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 3500
    .local v13, "e":Ljava/lang/Exception;
    const-string v3, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DirectShareService: FileDownThread: errmsg=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v13}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3501
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mDownThreadFlag:Z

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileLength:J

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v7}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->onError(ZJ)V

    goto :goto_2

    .line 3438
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v2    # "FileName":Ljava/lang/String;
    .restart local v4    # "url":Ljava/lang/String;
    .restart local v5    # "f":Ljava/io/File;
    .restart local v12    # "arrSubPath":[Ljava/lang/String;
    .restart local v15    # "extraSubPath":Ljava/lang/String;
    .restart local v16    # "i":I
    .restart local v17    # "isOK":Z
    .restart local v20    # "subPath":Ljava/lang/String;
    :catch_1
    move-exception v13

    .line 3440
    .restart local v13    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string v3, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DirectShareService: FileDownThread: downloadFile() failed, errmsg=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v13}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3444
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Downloading file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " from device "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mServerIp:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " using NFC"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " failed"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/sec/android/directshare/DirectShareService;->auditLog(ZLjava/lang/String;)V
    invoke-static {v3, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$3500(Lcom/sec/android/directshare/DirectShareService;ZLjava/lang/String;)V

    .line 3448
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mDownThreadFlag:Z

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileLength:J

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v7}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->onError(ZJ)V

    goto/16 :goto_2

    .line 3464
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_7
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v14, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3465
    .local v18, "newFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    const-string v6, "text/DirectShareSNote"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 3466
    new-instance v18, Ljava/io/File;

    .end local v18    # "newFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3468
    .restart local v18    # "newFile":Ljava/io/File;
    :cond_8
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_9

    .line 3469
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 3482
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # operator++ for: Lcom/sec/android/directshare/DirectShareService;->mDownloadedCount:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8208(Lcom/sec/android/directshare/DirectShareService;)I

    .line 3483
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v3, v6}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3484
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # operator++ for: Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8408(Lcom/sec/android/directshare/DirectShareService;)I

    .line 3485
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v3, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3489
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mOldFileName:Ljava/lang/String;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->removeDownloadList(Ljava/lang/String;)V
    invoke-static {v3, v6}, Lcom/sec/android/directshare/DirectShareService;->access$8500(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)V

    .line 3491
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Downloading file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " from device "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mServerIp:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " using NFC"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " succeeded"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/sec/android/directshare/DirectShareService;->auditLog(ZLjava/lang/String;)V
    invoke-static {v3, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$3500(Lcom/sec/android/directshare/DirectShareService;ZLjava/lang/String;)V

    .line 3495
    new-instance v3, Landroid/media/MediaScannerConnection;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mScanClient:Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;

    invoke-direct {v3, v6, v7}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->msc:Landroid/media/MediaScannerConnection;

    .line 3496
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v3}, Landroid/media/MediaScannerConnection;->connect()V

    goto/16 :goto_2

    .line 3473
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->FileCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    .line 3474
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    const-string v6, "text/DirectShareSNote"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 3475
    new-instance v18, Ljava/io/File;

    .end local v18    # "newFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3479
    .restart local v18    # "newFile":Ljava/io/File;
    :goto_4
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_9

    .line 3480
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto/16 :goto_3

    .line 3477
    :cond_a
    new-instance v18, Ljava/io/File;

    .end local v18    # "newFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v14, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .restart local v18    # "newFile":Ljava/io/File;
    goto :goto_4
.end method

.method public setFlag(Z)V
    .locals 0
    .param p1, "mDownThreadFlag"    # Z

    .prologue
    .line 3343
    iput-boolean p1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mDownThreadFlag:Z

    .line 3344
    return-void
.end method
