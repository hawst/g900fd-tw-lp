.class Lcom/sec/android/directshare/ui/SenderActivity$2;
.super Ljava/lang/Object;
.source "SenderActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/directshare/ui/SenderActivity;->initUi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/ui/SenderActivity;

.field final synthetic val$btnCancel:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/ui/SenderActivity;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/directshare/ui/SenderActivity$2;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    iput-object p2, p0, Lcom/sec/android/directshare/ui/SenderActivity$2;->val$btnCancel:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v1, 0x42

    .line 177
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 192
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 179
    :pswitch_0
    if-ne p2, v1, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity$2;->val$btnCancel:Landroid/widget/Button;

    const v1, 0x7f020012

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 184
    :cond_1
    :pswitch_1
    if-ne p2, v1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity$2;->val$btnCancel:Landroid/widget/Button;

    const v1, 0x7f020010

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity$2;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    # invokes: Lcom/sec/android/directshare/ui/SenderActivity;->cancel()V
    invoke-static {v0}, Lcom/sec/android/directshare/ui/SenderActivity;->access$000(Lcom/sec/android/directshare/ui/SenderActivity;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity$2;->this$0:Lcom/sec/android/directshare/ui/SenderActivity;

    invoke-virtual {v0}, Lcom/sec/android/directshare/ui/SenderActivity;->finish()V

    .line 188
    const-string v0, "[SBeam]"

    const-string v1, "SenderActivity: ----> finish..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 177
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
