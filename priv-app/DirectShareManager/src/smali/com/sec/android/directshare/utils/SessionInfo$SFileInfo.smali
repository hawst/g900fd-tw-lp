.class public Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
.super Ljava/lang/Object;
.source "SessionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/utils/SessionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SFileInfo"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private fileName:Ljava/lang/String;

.field private filePath:Ljava/lang/String;

.field private filelen:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo$1;

    invoke-direct {v0}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->filePath:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->fileName:Ljava/lang/String;

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->filelen:J

    .line 111
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/directshare/utils/SessionInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/directshare/utils/SessionInfo$1;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFilelen()J
    .locals 2

    .prologue
    .line 156
    iget-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->filelen:J

    return-wide v0
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->fileName:Ljava/lang/String;

    .line 161
    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->filePath:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public setFilelen(J)V
    .locals 1
    .param p1, "filelen"    # J

    .prologue
    .line 152
    iput-wide p1, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->filelen:J

    .line 153
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->filePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->fileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->filelen:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 123
    return-void
.end method
