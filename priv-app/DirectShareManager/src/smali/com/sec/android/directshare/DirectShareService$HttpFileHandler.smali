.class Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;
.super Ljava/lang/Object;
.source "DirectShareService.java"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HttpFileHandler"
.end annotation


# instance fields
.field private final docRoot:Ljava/lang/String;

.field downloadListener:Lcom/sec/android/directshare/DirectShareService$DownloadListener;

.field final synthetic this$0:Lcom/sec/android/directshare/DirectShareService;


# direct methods
.method public constructor <init>(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Lcom/sec/android/directshare/DirectShareService$DownloadListener;)V
    .locals 0
    .param p2, "docRoot"    # Ljava/lang/String;
    .param p3, "downloadListener"    # Lcom/sec/android/directshare/DirectShareService$DownloadListener;

    .prologue
    .line 2801
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2802
    iput-object p2, p0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;->docRoot:Ljava/lang/String;

    .line 2803
    iput-object p3, p0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;->downloadListener:Lcom/sec/android/directshare/DirectShareService$DownloadListener;

    .line 2804
    return-void
.end method


# virtual methods
.method public handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 11
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2808
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 2809
    .local v8, "method":Ljava/lang/String;
    const-string v0, "GET"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "HEAD"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "POST"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2810
    new-instance v0, Lorg/apache/http/MethodNotSupportedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " method not supported"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2812
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v10

    .line 2822
    .local v10, "target":Ljava/lang/String;
    const/4 v9, 0x0

    .line 2823
    .local v9, "reqAbsentFile":Z
    new-instance v7, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;->docRoot:Ljava/lang/String;

    invoke-static {v10}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v7, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2824
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 2825
    .local v2, "fileSize":J
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;->downloadListener:Lcom/sec/android/directshare/DirectShareService$DownloadListener;

    if-eqz v0, :cond_1

    .line 2826
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;->downloadListener:Lcom/sec/android/directshare/DirectShareService$DownloadListener;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v4, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/directshare/DirectShareService$DownloadListener;->onChange(Ljava/lang/String;JJ)V

    .line 2830
    :cond_1
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mSessionInfoForSender:Lcom/sec/android/directshare/utils/SessionInfo;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$6400(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/utils/SessionInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2831
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mSessionInfoForSender:Lcom/sec/android/directshare/utils/SessionInfo;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$6400(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/utils/SessionInfo;

    move-result-object v0

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/directshare/utils/SessionInfo;->exist(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v9, 0x1

    .line 2834
    :cond_2
    :goto_0
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v9, :cond_5

    .line 2835
    :cond_3
    const/16 v0, 0x194

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 2836
    new-instance v6, Lorg/apache/http/entity/EntityTemplate;

    new-instance v0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler$1;

    invoke-direct {v0, p0, v7}, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler$1;-><init>(Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;Ljava/io/File;)V

    invoke-direct {v6, v0}, Lorg/apache/http/entity/EntityTemplate;-><init>(Lorg/apache/http/entity/ContentProducer;)V

    .line 2847
    .local v6, "body":Lorg/apache/http/entity/EntityTemplate;
    const-string v0, "text/html; charset=UTF-8"

    invoke-virtual {v6, v0}, Lorg/apache/http/entity/EntityTemplate;->setContentType(Ljava/lang/String;)V

    .line 2848
    invoke-interface {p2, v6}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2849
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: File "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " not found"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2875
    .end local v6    # "body":Lorg/apache/http/entity/EntityTemplate;
    :goto_1
    return-void

    .line 2831
    :cond_4
    const/4 v9, 0x0

    goto :goto_0

    .line 2852
    :cond_5
    invoke-virtual {v7}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2853
    :cond_6
    const/16 v0, 0x193

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 2854
    new-instance v6, Lorg/apache/http/entity/EntityTemplate;

    new-instance v0, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler$2;-><init>(Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;)V

    invoke-direct {v6, v0}, Lorg/apache/http/entity/EntityTemplate;-><init>(Lorg/apache/http/entity/ContentProducer;)V

    .line 2863
    .restart local v6    # "body":Lorg/apache/http/entity/EntityTemplate;
    const-string v0, "text/html; charset=UTF-8"

    invoke-virtual {v6, v0}, Lorg/apache/http/entity/EntityTemplate;->setContentType(Ljava/lang/String;)V

    .line 2864
    invoke-interface {p2, v6}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2865
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: Cannot read file "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2869
    .end local v6    # "body":Lorg/apache/http/entity/EntityTemplate;
    :cond_7
    const/16 v0, 0xc8

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 2870
    new-instance v6, Lorg/apache/http/entity/FileEntity;

    const-string v0, "text/html"

    invoke-direct {v6, v7, v0}, Lorg/apache/http/entity/FileEntity;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2871
    .local v6, "body":Lorg/apache/http/entity/FileEntity;
    invoke-interface {p2, v6}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2872
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DirectShareService: Serving file "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
