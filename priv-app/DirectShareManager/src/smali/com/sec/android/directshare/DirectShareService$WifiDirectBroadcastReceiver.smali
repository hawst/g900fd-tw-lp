.class Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DirectShareService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WifiDirectBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/DirectShareService;


# direct methods
.method private constructor <init>(Lcom/sec/android/directshare/DirectShareService;)V
    .locals 0

    .prologue
    .line 2044
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/directshare/DirectShareService;Lcom/sec/android/directshare/DirectShareService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/directshare/DirectShareService;
    .param p2, "x1"    # Lcom/sec/android/directshare/DirectShareService$1;

    .prologue
    .line 2044
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;-><init>(Lcom/sec/android/directshare/DirectShareService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 2047
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2048
    .local v0, "action":Ljava/lang/String;
    const-string v5, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2049
    const-string v5, "[SBeam]"

    const-string v6, "DirectShareService: WIFI_P2P_STATE_CHANGED_ACTION"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2050
    const-string v5, "wifi_p2p_state"

    const/4 v6, -0x1

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 2056
    .local v4, "wifiP2pState":I
    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 2057
    const-string v5, "[SBeam]"

    const-string v6, "DirectShareService: WIFI_P2P_STATE_ENABLED"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2058
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2059
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 2065
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2086
    :goto_0
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mWifiP2pState:I
    invoke-static {v5, v4}, Lcom/sec/android/directshare/DirectShareService;->access$4302(Lcom/sec/android/directshare/DirectShareService;I)I

    .line 2443
    .end local v4    # "wifiP2pState":I
    :cond_0
    :goto_1
    return-void

    .line 2061
    .restart local v4    # "wifiP2pState":I
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2062
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_0

    .line 2068
    :cond_1
    const-string v5, "[SBeam]"

    const-string v6, "DirectShareService: WIFI_P2P_STATE_DISABLED"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2069
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2070
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    packed-switch v5, :pswitch_data_1

    .line 2083
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_0

    .line 2072
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2073
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2074
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-virtual {v5}, Lcom/sec/android/directshare/DirectShareService;->wifiDirect_enable()V

    goto :goto_0

    .line 2077
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2078
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2079
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mStartTime:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$1602(Lcom/sec/android/directshare/DirectShareService;J)J

    .line 2080
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f7

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_0

    .line 2087
    .end local v4    # "wifiP2pState":I
    :cond_2
    const-string v5, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2088
    const-string v5, "[SBeam]"

    const-string v6, "DirectShareService: WIFI_P2P_THIS_DEVICE_CHANGED_ACTION"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2089
    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const-string v5, "wifiP2pDevice"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/p2p/WifiP2pDevice;

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v6, v5}, Lcom/sec/android/directshare/DirectShareService;->access$5002(Lcom/sec/android/directshare/DirectShareService;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 2090
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    da=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$5000(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v8

    iget-object v8, v8, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getMac(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/directshare/DirectShareService;->access$5100(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2091
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    deviceName=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v7}, Lcom/sec/android/directshare/DirectShareService;->access$5000(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v7

    iget-object v7, v7, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2092
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    primaryDeviceType=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v7}, Lcom/sec/android/directshare/DirectShareService;->access$5000(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v7

    iget-object v7, v7, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2093
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    secondaryDeviceType=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v7}, Lcom/sec/android/directshare/DirectShareService;->access$5000(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v7

    iget-object v7, v7, Landroid/net/wifi/p2p/WifiP2pDevice;->secondaryDeviceType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2094
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    status=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$5000(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v8

    iget v8, v8, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDeviceStatus(I)Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/directshare/DirectShareService;->access$5200(Lcom/sec/android/directshare/DirectShareService;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2095
    :cond_3
    const-string v5, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2097
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->wifiDirect_requestPeer()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$5300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_1

    .line 2098
    :cond_4
    const-string v5, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2099
    const-string v5, "[SBeam]"

    const-string v6, "DirectShareService: WIFI_P2P_CONNECTION_CHANGED_ACTION"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2100
    const-string v5, "wifiP2pInfo"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 2102
    .local v3, "wifiP2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    const-string v6, "[SBeam]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    groupFormed=["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v5, v3, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    if-eqz v5, :cond_9

    const-string v5, "true"

    :goto_2
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "]"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2104
    iget-boolean v5, v3, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    if-eqz v5, :cond_5

    .line 2105
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    groupOwnerAddress=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2108
    const-string v6, "[SBeam]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    isGroupOwner=["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v5, v3, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    if-eqz v5, :cond_a

    const-string v5, "true"

    :goto_3
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "]"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2110
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, v3, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mOwnerAddress:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$5402(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;

    .line 2111
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-boolean v6, v3, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$5502(Lcom/sec/android/directshare/DirectShareService;Z)Z

    .line 2112
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$5500(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2113
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mOwnerAddress:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$5400(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadAddress:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3802(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;

    .line 2116
    :cond_5
    const-string v5, "networkInfo"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/NetworkInfo;

    .line 2118
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    .line 2119
    .local v1, "detailState":Landroid/net/NetworkInfo$DetailedState;
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    DetailState=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getDetailStateString(Landroid/net/NetworkInfo$DetailedState;)Ljava/lang/String;
    invoke-static {v7, v1}, Lcom/sec/android/directshare/DirectShareService;->access$5600(Lcom/sec/android/directshare/DirectShareService;Landroid/net/NetworkInfo$DetailedState;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2120
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceMode:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$5700(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/16 v6, 0x258

    if-ne v5, v6, :cond_6

    .line 2121
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->StartWebServer(Ljava/lang/String;)Z
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$5800(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Z

    .line 2123
    :cond_6
    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mWifiP2pState:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$4300(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v7

    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_b

    const/4 v5, 0x1

    :goto_4
    # invokes: Lcom/sec/android/directshare/DirectShareService;->setWifiState(II)V
    invoke-static {v6, v7, v5}, Lcom/sec/android/directshare/DirectShareService;->access$900(Lcom/sec/android/directshare/DirectShareService;II)V

    .line 2124
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2125
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 2433
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2434
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    invalid state=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v8, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v8}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v8

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getStateString(I)Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/directshare/DirectShareService;->access$6200(Lcom/sec/android/directshare/DirectShareService;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2435
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_7

    .line 2436
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0xcc

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2438
    :cond_7
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    .line 2441
    :cond_8
    :goto_5
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;
    invoke-static {v5, v1}, Lcom/sec/android/directshare/DirectShareService;->access$1702(Lcom/sec/android/directshare/DirectShareService;Landroid/net/NetworkInfo$DetailedState;)Landroid/net/NetworkInfo$DetailedState;

    goto/16 :goto_1

    .line 2102
    .end local v1    # "detailState":Landroid/net/NetworkInfo$DetailedState;
    .end local v2    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_9
    const-string v5, "false"

    goto/16 :goto_2

    .line 2108
    :cond_a
    const-string v5, "false"

    goto/16 :goto_3

    .line 2123
    .restart local v1    # "detailState":Landroid/net/NetworkInfo$DetailedState;
    .restart local v2    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_b
    const/4 v5, 0x0

    goto :goto_4

    .line 2127
    :sswitch_0
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_5

    .line 2130
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_5

    .line 2133
    :sswitch_2
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_e

    .line 2134
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_d

    .line 2135
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$5500(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 2136
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6a

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2137
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2138
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f8

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_5

    .line 2140
    :cond_c
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2141
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto :goto_5

    .line 2144
    :cond_d
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_5

    .line 2147
    :cond_e
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2148
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_5

    .line 2152
    :sswitch_3
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_f

    .line 2153
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2158
    :cond_f
    :sswitch_4
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_15

    .line 2159
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_13

    .line 2160
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$5500(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 2162
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mNfcState:I
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$5900()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_10

    .line 2163
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6c

    const/16 v7, 0x6b

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(II)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$6000(Lcom/sec/android/directshare/DirectShareService;II)V

    .line 2164
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2165
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 2183
    :goto_6
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f8

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2167
    :cond_10
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x70

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2168
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2169
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_6

    .line 2173
    :cond_11
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mNfcState:I
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$5900()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_12

    .line 2174
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6d

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2175
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2176
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_6

    .line 2178
    :cond_12
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x70

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2179
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2180
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_6

    .line 2184
    :cond_13
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_14

    .line 2186
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6a

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2187
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2188
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f8

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2190
    :cond_14
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2194
    :cond_15
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_17

    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mWifiP2pState:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$4300(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_17

    .line 2197
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mNfcState:I
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$5900()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_16

    .line 2198
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x66

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2199
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2200
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fc

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 2202
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f5

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2204
    :cond_16
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x70

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2205
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2207
    :cond_17
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_19

    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mWifiP2pState:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$4300(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_19

    .line 2210
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mNfcState:I
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$5900()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_18

    .line 2211
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x65

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2212
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2213
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f6

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2215
    :cond_18
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x70

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2216
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2219
    :cond_19
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2225
    :sswitch_5
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_1c

    .line 2227
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1b

    .line 2228
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$5500(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 2229
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6c

    const/16 v7, 0x6e

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(II)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$6000(Lcom/sec/android/directshare/DirectShareService;II)V

    .line 2230
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2231
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 2237
    :goto_7
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f8

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2233
    :cond_1a
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6d

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2234
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2235
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_7

    .line 2239
    :cond_1b
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2242
    :cond_1c
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2243
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_5

    .line 2248
    :sswitch_6
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_1d

    .line 2249
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2250
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fa

    # invokes: Lcom/sec/android/directshare/DirectShareService;->isExistDoAction(I)Z
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$6100(Lcom/sec/android/directshare/DirectShareService;I)Z

    move-result v5

    if-nez v5, :cond_8

    .line 2251
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$2500(Lcom/sec/android/directshare/DirectShareService;)Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x64

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_5

    .line 2254
    :cond_1d
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2259
    :sswitch_7
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_1f

    .line 2260
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$5500(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 2261
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6c

    const/16 v7, 0x6e

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(II)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$6000(Lcom/sec/android/directshare/DirectShareService;II)V

    .line 2262
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2263
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0xcc

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2264
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$1802(Lcom/sec/android/directshare/DirectShareService;I)I

    .line 2265
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2267
    :cond_1e
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2270
    :cond_1f
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2271
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_20

    .line 2272
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0xcc

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2273
    :cond_20
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_5

    .line 2278
    :sswitch_8
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_22

    .line 2283
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mIsGroupOwner:Z
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$5500(Lcom/sec/android/directshare/DirectShareService;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 2284
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6c

    const/16 v7, 0x6e

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(II)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/directshare/DirectShareService;->access$6000(Lcom/sec/android/directshare/DirectShareService;II)V

    .line 2285
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2286
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 2292
    :goto_8
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f8

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2288
    :cond_21
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x6d

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2289
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2290
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fd

    const-wide/16 v8, 0x0

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_8

    .line 2294
    :cond_22
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2299
    :sswitch_9
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_24

    .line 2300
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2301
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1fa

    # invokes: Lcom/sec/android/directshare/DirectShareService;->isExistDoAction(I)Z
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$6100(Lcom/sec/android/directshare/DirectShareService;I)Z

    move-result v5

    if-nez v5, :cond_23

    .line 2302
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0xcd

    # invokes: Lcom/sec/android/directshare/DirectShareService;->notifyDownloadError(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$1900(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2304
    :cond_23
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_5

    .line 2306
    :cond_24
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2310
    :sswitch_a
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2320
    :sswitch_b
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v5, :cond_26

    .line 2321
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_25

    .line 2322
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f6

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 2323
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x65

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2324
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2326
    :cond_25
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2330
    :cond_26
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2334
    :sswitch_c
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v5, :cond_2b

    .line 2335
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_28

    .line 2337
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mNfcState:I
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$5900()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_27

    .line 2338
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x66

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2339
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2341
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f5

    const-wide/16 v8, 0xc8

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 2361
    :goto_9
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopControlThread()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$3200(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_5

    .line 2345
    :cond_27
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x70

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2346
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_9

    .line 2348
    :cond_28
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2a

    .line 2349
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mNfcState:I
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$5900()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_29

    .line 2350
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x65

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2351
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2352
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f6

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_9

    .line 2354
    :cond_29
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x70

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2355
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_9

    .line 2358
    :cond_2a
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_9

    .line 2363
    :cond_2b
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2367
    :sswitch_d
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2370
    :sswitch_e
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2373
    :sswitch_f
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2376
    :sswitch_10
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v5, :cond_2e

    .line 2377
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopControlThread()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$3200(Lcom/sec/android/directshare/DirectShareService;)V

    .line 2378
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2c

    .line 2379
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x65

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2380
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2381
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f6

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2382
    :cond_2c
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2d

    .line 2383
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x66

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2384
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2386
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f5

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2388
    :cond_2d
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2391
    :cond_2e
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2395
    :sswitch_11
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v5, :cond_31

    .line 2396
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2f

    .line 2397
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x65

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2398
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2399
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f6

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    .line 2408
    :goto_a
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopControlThread()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$3200(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_5

    .line 2400
    :cond_2f
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_30

    .line 2401
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x66

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2402
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2404
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f5

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto :goto_a

    .line 2406
    :cond_30
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto :goto_a

    .line 2410
    :cond_31
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2414
    :sswitch_12
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v5, :cond_34

    .line 2415
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopControlThread()V
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$3200(Lcom/sec/android/directshare/DirectShareService;)V

    .line 2416
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_32

    .line 2417
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x65

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2418
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2419
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f6

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2420
    :cond_32
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mAction:I
    invoke-static {v5}, Lcom/sec/android/directshare/DirectShareService;->access$1800(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_33

    .line 2421
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x66

    # invokes: Lcom/sec/android/directshare/DirectShareService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$500(Lcom/sec/android/directshare/DirectShareService;I)V

    .line 2422
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 2424
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    const/16 v6, 0x1f5

    const-wide/16 v8, 0x64

    # invokes: Lcom/sec/android/directshare/DirectShareService;->doAction(IJ)V
    invoke-static {v5, v6, v8, v9}, Lcom/sec/android/directshare/DirectShareService;->access$4900(Lcom/sec/android/directshare/DirectShareService;IJ)V

    goto/16 :goto_5

    .line 2426
    :cond_33
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2429
    :cond_34
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v5, v6}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_5

    .line 2059
    :pswitch_data_0
    .packed-switch 0x71
        :pswitch_0
    .end packed-switch

    .line 2070
    :pswitch_data_1
    .packed-switch 0x71
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 2125
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_e
        0x64 -> :sswitch_4
        0x65 -> :sswitch_7
        0x66 -> :sswitch_5
        0x67 -> :sswitch_6
        0x68 -> :sswitch_8
        0x69 -> :sswitch_9
        0x6a -> :sswitch_b
        0x6b -> :sswitch_c
        0x6c -> :sswitch_12
        0x6d -> :sswitch_10
        0x6e -> :sswitch_f
        0x6f -> :sswitch_d
        0x70 -> :sswitch_11
        0x71 -> :sswitch_2
        0x72 -> :sswitch_0
        0x73 -> :sswitch_3
        0x75 -> :sswitch_1
        0x77 -> :sswitch_a
    .end sparse-switch
.end method
