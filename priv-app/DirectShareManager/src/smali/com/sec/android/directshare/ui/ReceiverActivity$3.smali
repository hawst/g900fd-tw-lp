.class Lcom/sec/android/directshare/ui/ReceiverActivity$3;
.super Landroid/content/BroadcastReceiver;
.source "ReceiverActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/ui/ReceiverActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/ui/ReceiverActivity;)V
    .locals 0

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 441
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 442
    .local v3, "action":Ljava/lang/String;
    const-string v16, "[SBeam]"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "ReceiverActivity: onReceive > "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    const-string v16, "com.sec.android.directshare.DOWNLOAD_START_ACTION"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 454
    const/16 v16, 0x0

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "device_name"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$302(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$300(Lcom/sec/android/directshare/ui/ReceiverActivity;)Ljava/lang/String;

    move-result-object v16

    if-nez v16, :cond_0

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "android_23a8"

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$302(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 459
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$400(Lcom/sec/android/directshare/ui/ReceiverActivity;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 460
    const-string v16, "[SBeam]"

    const-string v17, "ReceiverActivity: mSessionInfo is null"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->requestFileInfo()V
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$500(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    .line 463
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->startDownloadUi()V
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$600(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    .line 582
    :cond_2
    :goto_0
    return-void

    .line 464
    :cond_3
    const-string v16, "com.sec.android.directshare.DOWNLOAD_INFO_ACTION"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 465
    const-string v16, "state"

    const/16 v17, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 467
    .local v4, "bReady":Z
    if-nez v4, :cond_4

    .line 468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mInitCheck:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$700(Lcom/sec/android/directshare/ui/ReceiverActivity;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 470
    const/16 v16, -0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$800(Lcom/sec/android/directshare/ui/ReceiverActivity;)Landroid/app/Activity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->finish()V

    .line 472
    const/16 v16, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$202(Z)Z

    goto :goto_0

    .line 477
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "count"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadCount:I
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$902(Lcom/sec/android/directshare/ui/ReceiverActivity;I)I

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "totalCount"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadTotalCount:I
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1002(Lcom/sec/android/directshare/ui/ReceiverActivity;I)I

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v17, v0

    const-string v16, "session"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Lcom/sec/android/directshare/utils/SessionInfo;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;
    invoke-static {v0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1102(Lcom/sec/android/directshare/ui/ReceiverActivity;Lcom/sec/android/directshare/utils/SessionInfo;)Lcom/sec/android/directshare/utils/SessionInfo;

    .line 480
    const-string v16, "filename"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 481
    .local v7, "fileName":Ljava/lang/String;
    if-nez v7, :cond_5

    .line 482
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1100(Lcom/sec/android/directshare/ui/ReceiverActivity;)Lcom/sec/android/directshare/utils/SessionInfo;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/directshare/utils/SessionInfo;->getFileInfo(I)Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->getFileName()Ljava/lang/String;

    move-result-object v7

    .line 486
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$402(Lcom/sec/android/directshare/ui/ReceiverActivity;Z)Z

    .line 489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadCount:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$900(Lcom/sec/android/directshare/ui/ReceiverActivity;)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadTotalCount:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1000(Lcom/sec/android/directshare/ui/ReceiverActivity;)I

    move-result v18

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->udpateFileCount(II)V
    invoke-static/range {v16 .. v18}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1200(Lcom/sec/android/directshare/ui/ReceiverActivity;II)V

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->updateFileName(Ljava/lang/String;)V
    invoke-static {v0, v7}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1300(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)V

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "progress"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->updateProgress(I)V
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1400(Lcom/sec/android/directshare/ui/ReceiverActivity;I)V

    goto/16 :goto_0

    .line 484
    :cond_5
    const-string v16, "filename"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 492
    .end local v4    # "bReady":Z
    .end local v7    # "fileName":Ljava/lang/String;
    :cond_6
    const-string v16, "com.sec.android.directshare.DOWNLOAD_COMPLETED_ACTION"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 493
    const-string v16, "filename"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 494
    .restart local v7    # "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "count"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadCount:I
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$902(Lcom/sec/android/directshare/ui/ReceiverActivity;I)I

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "totalCount"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadTotalCount:I
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1002(Lcom/sec/android/directshare/ui/ReceiverActivity;I)I

    .line 496
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadCount:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$900(Lcom/sec/android/directshare/ui/ReceiverActivity;)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadTotalCount:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1000(Lcom/sec/android/directshare/ui/ReceiverActivity;)I

    move-result v18

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->udpateFileCount(II)V
    invoke-static/range {v16 .. v18}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1200(Lcom/sec/android/directshare/ui/ReceiverActivity;II)V

    .line 497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->updateFileName(Ljava/lang/String;)V
    invoke-static {v0, v7}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1300(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 499
    .end local v7    # "fileName":Ljava/lang/String;
    :cond_7
    const-string v16, "com.sec.android.directshare.DOWNLOAD_SCANNER_ACTION"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_c

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$400(Lcom/sec/android/directshare/ui/ReceiverActivity;)Z

    move-result v16

    if-nez v16, :cond_8

    .line 501
    const-string v16, "[SBeam]"

    const-string v17, "ReceiverActivity: mSessionInfo is null"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->requestFileInfo()V
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$500(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    goto/16 :goto_0

    .line 505
    :cond_8
    const-string v16, "filepath"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 506
    .local v8, "filePath":Ljava/lang/String;
    const-string v16, "mimetype"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 507
    .local v10, "mimetype":Ljava/lang/String;
    const-string v16, "uri"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v14

    check-cast v14, Landroid/net/Uri;

    .line 508
    .local v14, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "device_name"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$302(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 509
    const-string v16, "scancount"

    const/16 v17, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 510
    .local v12, "scanCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "totalCount"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadTotalCount:I
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1002(Lcom/sec/android/directshare/ui/ReceiverActivity;I)I

    move-result v13

    .line 511
    .local v13, "totolCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$300(Lcom/sec/android/directshare/ui/ReceiverActivity;)Ljava/lang/String;

    move-result-object v16

    if-nez v16, :cond_9

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "android_23a8"

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$302(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 514
    :cond_9
    if-lt v12, v13, :cond_2

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1100(Lcom/sec/android/directshare/ui/ReceiverActivity;)Lcom/sec/android/directshare/utils/SessionInfo;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/directshare/utils/SessionInfo;->getMimeType()Ljava/lang/String;

    move-result-object v16

    const-string v17, "text/DirectShareSNote"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->viewSNote(Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1500(Lcom/sec/android/directshare/ui/ReceiverActivity;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 519
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->isHome:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1600(Lcom/sec/android/directshare/ui/ReceiverActivity;)Z

    move-result v16

    if-nez v16, :cond_b

    .line 520
    new-instance v9, Lcom/sec/android/directshare/utils/Launcher;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$800(Lcom/sec/android/directshare/ui/ReceiverActivity;)Landroid/app/Activity;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v9, v0, v10, v14, v8}, Lcom/sec/android/directshare/utils/Launcher;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 521
    .local v9, "launcher":Lcom/sec/android/directshare/utils/Launcher;
    invoke-virtual {v9}, Lcom/sec/android/directshare/utils/Launcher;->build()Landroid/content/Intent;

    move-result-object v15

    .line 523
    .local v15, "viewIntent":Landroid/content/Intent;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 528
    .end local v9    # "launcher":Lcom/sec/android/directshare/utils/Launcher;
    .end local v15    # "viewIntent":Landroid/content/Intent;
    :cond_b
    :goto_2
    const/16 v16, -0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->finish()V

    .line 530
    const/16 v16, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$202(Z)Z

    goto/16 :goto_0

    .line 524
    .restart local v9    # "launcher":Lcom/sec/android/directshare/utils/Launcher;
    .restart local v15    # "viewIntent":Landroid/content/Intent;
    :catch_0
    move-exception v5

    .line 525
    .local v5, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v9}, Lcom/sec/android/directshare/utils/Launcher;->showActivityNotFound()V

    goto :goto_2

    .line 532
    .end local v5    # "e":Landroid/content/ActivityNotFoundException;
    .end local v8    # "filePath":Ljava/lang/String;
    .end local v9    # "launcher":Lcom/sec/android/directshare/utils/Launcher;
    .end local v10    # "mimetype":Ljava/lang/String;
    .end local v12    # "scanCount":I
    .end local v13    # "totolCount":I
    .end local v14    # "uri":Landroid/net/Uri;
    .end local v15    # "viewIntent":Landroid/content/Intent;
    :cond_c
    const-string v16, "com.sec.android.directshare.DOWNLOAD_STATE_ACTION"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_f

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$400(Lcom/sec/android/directshare/ui/ReceiverActivity;)Z

    move-result v16

    if-nez v16, :cond_d

    .line 534
    const-string v16, "[SBeam]"

    const-string v17, "ReceiverActivity: mSessionInfo is null"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->requestFileInfo()V
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$500(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    goto/16 :goto_0

    .line 538
    :cond_d
    const-string v16, "progress"

    const/16 v17, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 539
    .local v11, "progress":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "device_name"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$302(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$300(Lcom/sec/android/directshare/ui/ReceiverActivity;)Ljava/lang/String;

    move-result-object v16

    if-nez v16, :cond_e

    .line 541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    const-string v17, "android_23a8"

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$302(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 543
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->updateProgress(I)V
    invoke-static {v0, v11}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$1400(Lcom/sec/android/directshare/ui/ReceiverActivity;I)V

    goto/16 :goto_0

    .line 544
    .end local v11    # "progress":I
    :cond_f
    const-string v16, "com.sec.android.directshare.CLIENT_SERVICE_STOP"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_10

    .line 545
    const/16 v16, -0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 546
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->finish()V

    .line 547
    const/16 v16, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$202(Z)Z

    goto/16 :goto_0

    .line 548
    :cond_10
    const-string v16, "com.sec.android.directshare.DOWNLOAD_CANCELED_ACTION"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_11

    .line 549
    const-string v16, "[SBeam]"

    const-string v17, "DIRECT_SHARE_RECEIVER_TRANSFER_CANCEL"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const/16 v16, -0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->stopThread()V
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$100(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    .line 552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->finish()V

    .line 553
    const-string v16, "[SBeam]"

    const-string v17, "ReceiverActivity: ----> finish..."

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    const/16 v16, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$202(Z)Z

    goto/16 :goto_0

    .line 555
    :cond_11
    const-string v16, "com.sec.android.directshare.DOWNLOAD_ERROR_ACTION"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 556
    const-string v16, "errorcode"

    const/16 v17, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 557
    .local v6, "errcode":I
    const-string v16, "[SBeam]"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "ReceiverActivity:   DIRECT_SHARE_ERROR_ACTION > "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    packed-switch v6, :pswitch_data_0

    goto/16 :goto_0

    .line 562
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$800(Lcom/sec/android/directshare/ui/ReceiverActivity;)Landroid/app/Activity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->finish()V

    .line 563
    const/16 v16, -0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 564
    const/16 v16, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$202(Z)Z

    goto/16 :goto_0

    .line 567
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$800(Lcom/sec/android/directshare/ui/ReceiverActivity;)Landroid/app/Activity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->finish()V

    .line 568
    const/16 v16, -0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 569
    const/16 v16, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$202(Z)Z

    goto/16 :goto_0

    .line 572
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$800(Lcom/sec/android/directshare/ui/ReceiverActivity;)Landroid/app/Activity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->finish()V

    .line 573
    const/16 v16, -0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$002(I)I

    .line 574
    const/16 v16, 0x1

    # setter for: Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$202(Z)Z

    goto/16 :goto_0

    .line 577
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->requestFileInfo()V
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$500(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    .line 578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;->this$0:Lcom/sec/android/directshare/ui/ReceiverActivity;

    move-object/from16 v16, v0

    # invokes: Lcom/sec/android/directshare/ui/ReceiverActivity;->startDownloadUi()V
    invoke-static/range {v16 .. v16}, Lcom/sec/android/directshare/ui/ReceiverActivity;->access$600(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    goto/16 :goto_0

    .line 558
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
