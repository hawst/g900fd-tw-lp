.class public Lcom/sec/android/directshare/ui/SenderActivity;
.super Landroid/app/Activity;
.source "SenderActivity.java"


# static fields
.field private static mUploadState:I


# instance fields
.field private mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

.field private mFileName:Ljava/lang/String;

.field private mImgPreparing:Landroid/widget/ImageView;

.field private mImgSend:Landroid/widget/ImageView;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mLayoutFileInfo:Landroid/widget/LinearLayout;

.field private mLayoutPreparingTxt:Landroid/widget/LinearLayout;

.field private mProgress:I

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mTotalCount:I

.field private mTxtFileMore:Landroid/widget/TextView;

.field private mTxtFileName:Landroid/widget/TextView;

.field private mTxtPercent:Landroid/widget/TextView;

.field private mTxtSharing:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/directshare/ui/SenderActivity;->mUploadState:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mIntentFilter:Landroid/content/IntentFilter;

    .line 29
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgPreparing:Landroid/widget/ImageView;

    .line 30
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    .line 31
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mLayoutPreparingTxt:Landroid/widget/LinearLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgSend:Landroid/widget/ImageView;

    .line 33
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileName:Landroid/widget/TextView;

    .line 34
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtPercent:Landroid/widget/TextView;

    .line 35
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtSharing:Landroid/widget/TextView;

    .line 36
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    .line 37
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mLayoutFileInfo:Landroid/widget/LinearLayout;

    .line 38
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 42
    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mFileName:Ljava/lang/String;

    .line 43
    iput v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgress:I

    .line 44
    iput v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTotalCount:I

    .line 308
    new-instance v0, Lcom/sec/android/directshare/ui/SenderActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/ui/SenderActivity$3;-><init>(Lcom/sec/android/directshare/ui/SenderActivity;)V

    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/directshare/ui/SenderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/SenderActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->cancel()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/directshare/ui/SenderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/SenderActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->stopAniPreparing()V

    return-void
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 25
    sput p0, Lcom/sec/android/directshare/ui/SenderActivity;->mUploadState:I

    return p0
.end method

.method static synthetic access$300(Lcom/sec/android/directshare/ui/SenderActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/SenderActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/ui/SenderActivity;->updateFileName(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/directshare/ui/SenderActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/SenderActivity;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/ui/SenderActivity;->updateFileMore(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/directshare/ui/SenderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/SenderActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->startUploadUi()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/directshare/ui/SenderActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/SenderActivity;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/ui/SenderActivity;->updateProgress(I)V

    return-void
.end method

.method private cancel()V
    .locals 3

    .prologue
    .line 271
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.STOP_THREAD_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 272
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "REQ"

    const-string v2, "com.sec.android.directshare.UPLOAD_CANCEL_ACTION"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/SenderActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 275
    return-void
.end method

.method private initIntentFilter()V
    .locals 2

    .prologue
    .line 259
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mIntentFilter:Landroid/content/IntentFilter;

    .line 260
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.UPLOAD_START_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.UPLOAD_STATE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.UPLOAD_COMPLETED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.UPLOAD_FILEINFO_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.STOP_SERVICE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method private initUi()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 136
    const v1, 0x7f070003

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgPreparing:Landroid/widget/ImageView;

    .line 137
    iget-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgPreparing:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgPreparing:Landroid/widget/ImageView;

    const v2, 0x7f020003

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 139
    iget-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgPreparing:Landroid/widget/ImageView;

    const v2, 0x7f040001

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgPreparing:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    .line 142
    const v1, 0x7f070006

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mLayoutPreparingTxt:Landroid/widget/LinearLayout;

    .line 150
    const v1, 0x7f07000e

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgSend:Landroid/widget/ImageView;

    .line 152
    const v1, 0x7f070005

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtSharing:Landroid/widget/TextView;

    .line 154
    const v1, 0x7f070009

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mLayoutFileInfo:Landroid/widget/LinearLayout;

    .line 155
    const v1, 0x7f07000a

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileName:Landroid/widget/TextView;

    .line 156
    const v1, 0x7f07000f

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    .line 157
    const v1, 0x7f07000b

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtPercent:Landroid/widget/TextView;

    .line 158
    iget-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtPercent:Landroid/widget/TextView;

    const-string v2, "%d%%"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    const v1, 0x7f07000c

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 161
    iget-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 162
    iget-object v1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 164
    const v1, 0x7f07000d

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 165
    .local v0, "btnCancel":Landroid/widget/Button;
    new-instance v1, Lcom/sec/android/directshare/ui/SenderActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/directshare/ui/SenderActivity$1;-><init>(Lcom/sec/android/directshare/ui/SenderActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    new-instance v1, Lcom/sec/android/directshare/ui/SenderActivity$2;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/directshare/ui/SenderActivity$2;-><init>(Lcom/sec/android/directshare/ui/SenderActivity;Landroid/widget/Button;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 195
    return-void
.end method

.method private reqUpdateFileInfo()V
    .locals 2

    .prologue
    .line 277
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.REQ_UPLOAD_INFO_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 278
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/SenderActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 280
    return-void
.end method

.method private resultHotspotPopup(I)V
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 282
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.MOBILEAP_POPUP_RESULT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 283
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 285
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/SenderActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 286
    return-void
.end method

.method private resultWfdPopup(I)V
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 288
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.WFD_POPUP_RESULT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 289
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 291
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/SenderActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 292
    return-void
.end method

.method private showHotspotPopup()V
    .locals 3

    .prologue
    .line 297
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 298
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "POPUP_MODE"

    const-string v2, "hotspot_disconnect"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 301
    return-void
.end method

.method private showWfdPopup()V
    .locals 3

    .prologue
    .line 303
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 304
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "POPUP_MODE"

    const-string v2, "allshare_disconnect"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    const/16 v1, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directshare/ui/SenderActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 307
    return-void
.end method

.method private startUploadUi()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 219
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->stopAniPreparing()V

    .line 220
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgPreparing:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mImgSend:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mLayoutPreparingTxt:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtSharing:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mLayoutFileInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 226
    return-void
.end method

.method private stopAniPreparing()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 257
    :cond_0
    return-void
.end method

.method private updateFileMore(I)V
    .locals 8
    .param p1, "TotalCount"    # I

    .prologue
    const v6, 0x7f050017

    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 237
    iput p1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTotalCount:I

    .line 238
    if-le p1, v5, :cond_1

    .line 239
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 240
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 241
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v6}, Lcom/sec/android/directshare/ui/SenderActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, p1, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileName:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileName:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v6}, Lcom/sec/android/directshare/ui/SenderActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, p1, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 246
    :cond_1
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 248
    .local v0, "MoreParams":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v5, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v1, v2

    .line 249
    .local v1, "width":I
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 250
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private updateFileName(Ljava/lang/String;)V
    .locals 1
    .param p1, "FileName"    # Ljava/lang/String;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mFileName:Ljava/lang/String;

    .line 234
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    return-void
.end method

.method private updateProgress(I)V
    .locals 5
    .param p1, "progress"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgress:I

    .line 229
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtPercent:Landroid/widget/TextView;

    const-string v1, "%d%%"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    return-void
.end method

.method private updateUi()V
    .locals 9

    .prologue
    const v8, 0x7f050017

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 197
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgressBar:Landroid/widget/ProgressBar;

    iget v3, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgress:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 198
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtPercent:Landroid/widget/TextView;

    const-string v3, "%d%%"

    new-array v4, v6, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mProgress:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileName:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    sget v2, Lcom/sec/android/directshare/ui/SenderActivity;->mUploadState:I

    if-nez v2, :cond_0

    .line 201
    iget v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTotalCount:I

    if-le v2, v6, :cond_2

    .line 202
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 203
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v8}, Lcom/sec/android/directshare/ui/SenderActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTotalCount:I

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileName:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileName:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v8}, Lcom/sec/android/directshare/ui/SenderActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTotalCount:I

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 209
    :cond_2
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 210
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 211
    .local v0, "MoreParams":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v1, v2

    .line 212
    .local v1, "width":I
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 213
    iget-object v2, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mTxtFileMore:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 103
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 104
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SenderActivity:  requestCode = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], resultCode = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 107
    invoke-direct {p0, p2}, Lcom/sec/android/directshare/ui/SenderActivity;->resultHotspotPopup(I)V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    .line 109
    invoke-direct {p0, p2}, Lcom/sec/android/directshare/ui/SenderActivity;->resultWfdPopup(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 115
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 118
    :pswitch_0
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/SenderActivity;->setContentView(I)V

    .line 119
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->initUi()V

    .line 120
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->updateUi()V

    .line 121
    sget v0, Lcom/sec/android/directshare/ui/SenderActivity;->mUploadState:I

    if-nez v0, :cond_0

    .line 122
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->startUploadUi()V

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v3, 0x7f030002

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/ui/SenderActivity;->setContentView(I)V

    .line 55
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->initUi()V

    .line 56
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->initIntentFilter()V

    .line 57
    iget-object v3, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v4, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/directshare/ui/SenderActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 59
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "complete"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 60
    .local v2, "isComplete":Z
    const-string v3, "fromNoti"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 61
    .local v0, "bFromNoti":Z
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SenderActivity: onCreate : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    if-nez v2, :cond_0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    sget v3, Lcom/sec/android/directshare/ui/SenderActivity;->mUploadState:I

    if-nez v3, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->reqUpdateFileInfo()V

    .line 66
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->startUploadUi()V

    .line 70
    :cond_0
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isMobileAPEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 71
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->showHotspotPopup()V

    .line 73
    :cond_1
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isWfdEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 74
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->showWfdPopup()V

    .line 76
    :cond_2
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 90
    const-string v0, "[SBeam]"

    const-string v1, "SenderActivity:  onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/SenderActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 92
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/SenderActivity;->stopAniPreparing()V

    .line 93
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 85
    const-string v0, "[SBeam]"

    const-string v1, "SenderActivity: [onNewIntent]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 80
    const-string v0, "[SBeam]"

    const-string v1, "SenderActivity: [onUserLeaveHint]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/directshare/ui/SenderActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 100
    :cond_0
    return-void
.end method
