.class public Lcom/sec/android/directshare/DirectShareMessage;
.super Ljava/lang/Object;
.source "DirectShareMessage.java"


# direct methods
.method private static addString(Ljava/nio/ByteBuffer;Ljava/lang/String;I)V
    .locals 4
    .param p0, "buf"    # Ljava/nio/ByteBuffer;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "len"    # I

    .prologue
    .line 138
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int v1, v3, p2

    .line 139
    .local v1, "newPosition":I
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 140
    .local v2, "temp":[B
    array-length v3, v2

    if-lt v3, p2, :cond_0

    .line 141
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 148
    :goto_0
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    .line 144
    .local v0, "b":B
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 145
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 146
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static getString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
    .locals 6
    .param p0, "src"    # Ljava/nio/ByteBuffer;
    .param p1, "len"    # I

    .prologue
    .line 197
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int v2, v5, p1

    .line 198
    .local v2, "newPosition":I
    new-array v4, p1, [B

    .line 199
    .local v4, "temp":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 200
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    .line 201
    .local v0, "b":B
    if-nez v0, :cond_1

    .line 205
    .end local v0    # "b":B
    :cond_0
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 206
    new-instance v3, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5, v1}, Ljava/lang/String;-><init>([BII)V

    .line 207
    .local v3, "str":Ljava/lang/String;
    return-object v3

    .line 203
    .end local v3    # "str":Ljava/lang/String;
    .restart local v0    # "b":B
    :cond_1
    aput-byte v0, v4, v1

    .line 199
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static makeCancel()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 130
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 131
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 132
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 133
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 134
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 135
    return-object v0
.end method

.method private static makeDeviceName(Ljava/lang/String;)Ljava/nio/ByteBuffer;
    .locals 6
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x48

    const/16 v4, 0x40

    .line 150
    if-nez p0, :cond_0

    .line 151
    const/4 v1, 0x0

    .line 166
    :goto_0
    return-object v1

    .line 152
    :cond_0
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 153
    .local v1, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 154
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 155
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 156
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 157
    .local v2, "temp":[B
    array-length v3, v2

    if-lt v3, v4, :cond_1

    .line 158
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 165
    :goto_1
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0

    .line 160
    :cond_1
    const/4 v0, 0x0

    .line 161
    .local v0, "b":B
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 162
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 163
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_1
.end method

.method private static makeDownloadEnd()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 83
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 84
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 85
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 86
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 87
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 88
    return-object v0
.end method

.method private static makeDownloadEndOnefile(Ljava/lang/String;)Ljava/nio/ByteBuffer;
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, 0x1

    .line 92
    .local v0, "filePathLeng":I
    add-int/lit8 v2, v0, 0x8

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 93
    .local v1, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 94
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 95
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 96
    invoke-static {v1, p0, v0}, Lcom/sec/android/directshare/DirectShareMessage;->addString(Ljava/nio/ByteBuffer;Ljava/lang/String;I)V

    .line 97
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 98
    return-object v1
.end method

.method private static makeDownloadInfo(ILjava/lang/String;Ljava/lang/String;)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0, "fileCount"    # I
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 117
    if-nez p1, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 127
    :goto_0
    return-object v0

    .line 119
    :cond_0
    const/16 v1, 0x14c

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 120
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 121
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 122
    const/16 v1, 0x144

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 123
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 124
    const/16 v1, 0x100

    invoke-static {v0, p1, v1}, Lcom/sec/android/directshare/DirectShareMessage;->addString(Ljava/nio/ByteBuffer;Ljava/lang/String;I)V

    .line 125
    const/16 v1, 0x40

    invoke-static {v0, p2, v1}, Lcom/sec/android/directshare/DirectShareMessage;->addString(Ljava/nio/ByteBuffer;Ljava/lang/String;I)V

    .line 126
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method private static makeDownloadState(I)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0, "percent"    # I

    .prologue
    .line 169
    const/16 v1, 0xc

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 170
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 171
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 172
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 173
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 174
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 175
    return-object v0
.end method

.method private static makeTtl()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 109
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 110
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 111
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 112
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 113
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 114
    return-object v0
.end method

.method public static sendCancel(Ljava/nio/channels/SocketChannel;)V
    .locals 3
    .param p0, "channel"    # Ljava/nio/channels/SocketChannel;

    .prologue
    .line 69
    const-string v1, "[SBeam]"

    const-string v2, "DirectShareMessage: ----#cmd= send[CMD_CANCEL]"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-static {}, Lcom/sec/android/directshare/DirectShareMessage;->makeCancel()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 71
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Lcom/sec/android/directshare/DirectShareMessage;->write(Ljava/nio/channels/SocketChannel;Ljava/nio/ByteBuffer;)V

    .line 72
    return-void
.end method

.method public static sendDeviceName(Ljava/nio/channels/SocketChannel;Ljava/lang/String;)V
    .locals 4
    .param p0, "channel"    # Ljava/nio/channels/SocketChannel;
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 78
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareMessage: ----#cmd= send[CMD_DEVICE_NAME] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-static {p1}, Lcom/sec/android/directshare/DirectShareMessage;->makeDeviceName(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 80
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Lcom/sec/android/directshare/DirectShareMessage;->write(Ljava/nio/channels/SocketChannel;Ljava/nio/ByteBuffer;)V

    .line 81
    return-void
.end method

.method public static sendDownloadEnd(Ljava/nio/channels/SocketChannel;)V
    .locals 3
    .param p0, "channel"    # Ljava/nio/channels/SocketChannel;

    .prologue
    .line 44
    const-string v1, "[SBeam]"

    const-string v2, "DirectShareMessage: ----#cmd= send[CMD_DOWNLOAD_END]"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-static {}, Lcom/sec/android/directshare/DirectShareMessage;->makeDownloadEnd()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 46
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Lcom/sec/android/directshare/DirectShareMessage;->write(Ljava/nio/channels/SocketChannel;Ljava/nio/ByteBuffer;)V

    .line 47
    return-void
.end method

.method public static sendDownloadEndOnefile(Ljava/nio/channels/SocketChannel;Ljava/lang/String;)V
    .locals 3
    .param p0, "channel"    # Ljava/nio/channels/SocketChannel;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 49
    const-string v1, "[SBeam]"

    const-string v2, "DirectShareMessage: ----#cmd= send[CMD_DOWNLOAD_END_ONEFILE]"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-static {p1}, Lcom/sec/android/directshare/DirectShareMessage;->makeDownloadEndOnefile(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 51
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Lcom/sec/android/directshare/DirectShareMessage;->write(Ljava/nio/channels/SocketChannel;Ljava/nio/ByteBuffer;)V

    .line 52
    return-void
.end method

.method public static sendDownloadInfo(Ljava/nio/channels/SocketChannel;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "channel"    # Ljava/nio/channels/SocketChannel;
    .param p1, "fileCount"    # I
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareMessage: ----#cmd= send[CMD_DOWNLOAD_INFO]{ #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-static {p1, p2, p3}, Lcom/sec/android/directshare/DirectShareMessage;->makeDownloadInfo(ILjava/lang/String;Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 66
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Lcom/sec/android/directshare/DirectShareMessage;->write(Ljava/nio/channels/SocketChannel;Ljava/nio/ByteBuffer;)V

    .line 67
    return-void
.end method

.method public static sendDownloadState(Ljava/nio/channels/SocketChannel;I)V
    .locals 1
    .param p0, "channel"    # Ljava/nio/channels/SocketChannel;
    .param p1, "percent"    # I

    .prologue
    .line 74
    invoke-static {p1}, Lcom/sec/android/directshare/DirectShareMessage;->makeDownloadState(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 75
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Lcom/sec/android/directshare/DirectShareMessage;->write(Ljava/nio/channels/SocketChannel;Ljava/nio/ByteBuffer;)V

    .line 76
    return-void
.end method

.method public static sendTtl(Ljava/nio/channels/SocketChannel;)V
    .locals 1
    .param p0, "channel"    # Ljava/nio/channels/SocketChannel;

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/android/directshare/DirectShareMessage;->makeTtl()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 59
    .local v0, "sendbuf":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Lcom/sec/android/directshare/DirectShareMessage;->write(Ljava/nio/channels/SocketChannel;Ljava/nio/ByteBuffer;)V

    .line 60
    return-void
.end method

.method private static write(Ljava/nio/channels/SocketChannel;Ljava/nio/ByteBuffer;)V
    .locals 4
    .param p0, "channel"    # Ljava/nio/channels/SocketChannel;
    .param p1, "sendbuf"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 178
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    const/4 v2, 0x0

    .line 182
    .local v2, "offset":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    .line 186
    .local v1, "len":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 187
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 188
    .local v3, "ret":I
    add-int/2addr v2, v3

    goto :goto_1

    .line 190
    .end local v3    # "ret":I
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
