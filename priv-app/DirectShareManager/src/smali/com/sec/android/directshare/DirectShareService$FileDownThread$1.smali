.class Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;
.super Ljava/lang/Object;
.source "DirectShareService.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService$FileDownThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/DirectShareService$FileDownThread;)V
    .locals 0

    .prologue
    .line 3552
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .locals 5

    .prologue
    .line 3557
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->SUB_PATH:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8000(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3558
    .local v0, "exPath":Ljava/io/File;
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v2, v2, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    const-string v3, "text/DirectShareSNote"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3559
    new-instance v0, Ljava/io/File;

    .end local v0    # "exPath":Ljava/io/File;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->getSnbFolderName()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8100(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3561
    .restart local v0    # "exPath":Ljava/io/File;
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v2, v2, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3562
    .local v1, "newFile":Ljava/io/File;
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v2, v2, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 3563
    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 3567
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectShareService: onScanCompleted: filePath=["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] uri =["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3569
    const-string v0, "text/DirectShareGallery"

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3570
    const/4 v9, 0x0

    .line 3571
    .local v9, "isImage":Z
    const/4 v10, 0x0

    .line 3572
    .local v10, "isVideo":Z
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 3573
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3576
    .local v6, "c":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    .line 3577
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3578
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 3581
    .local v7, "c1":Landroid/database/Cursor;
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    .line 3582
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 3583
    if-nez v9, :cond_0

    if-eqz v10, :cond_1

    .line 3584
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    const-string v2, "text/DirectShareGallery"

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/directshare/DirectShareService;->downloadFileScanned(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    invoke-static {v0, v1, v2, v3, p2}, Lcom/sec/android/directshare/DirectShareService;->access$8700(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 3609
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "c1":Landroid/database/Cursor;
    .end local v9    # "isImage":Z
    .end local v10    # "isVideo":Z
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    .line 3610
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3611
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # operator-- for: Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$8410(Lcom/sec/android/directshare/DirectShareService;)I

    .line 3612
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadInfo:Lcom/sec/android/directshare/DirectShareService$DownloadInfo;
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$6700(Lcom/sec/android/directshare/DirectShareService;)Lcom/sec/android/directshare/DirectShareService$DownloadInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$DownloadInfo;->file:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 3613
    .local v12, "size":I
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareService: onScanCompleted: mMediaScanCount=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v2, v2, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I
    invoke-static {v2}, Lcom/sec/android/directshare/DirectShareService;->access$8400(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], file size=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3615
    if-nez v12, :cond_6

    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mMediaScanCount:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$8400(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v0

    if-nez v0, :cond_6

    .line 3616
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3617
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->lock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$2800(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3618
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mState:I
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$100(Lcom/sec/android/directshare/DirectShareService;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3624
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3630
    :goto_1
    return-void

    .line 3586
    .end local v12    # "size":I
    :cond_2
    const-string v0, "text/DirectShareVideos"

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3587
    const/4 v10, 0x0

    .line 3588
    .restart local v10    # "isVideo":Z
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 3589
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-virtual {v0}, Lcom/sec/android/directshare/DirectShareService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3592
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    .line 3593
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3594
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->subtitleFormat(Ljava/lang/String;)Ljava/lang/Boolean;
    invoke-static {v0, p1}, Lcom/sec/android/directshare/DirectShareService;->access$8800(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3595
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mVideoFileName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$8900(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "text/DirectShareVideos"

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v4, v4, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mVideoUri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/sec/android/directshare/DirectShareService;->access$9000(Lcom/sec/android/directshare/DirectShareService;)Landroid/net/Uri;

    move-result-object v4

    # invokes: Lcom/sec/android/directshare/DirectShareService;->downloadFileScanned(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/directshare/DirectShareService;->access$8700(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 3597
    :cond_3
    const/4 v0, 0x1

    if-ne v10, v0, :cond_1

    .line 3598
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mVideoFileName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$8902(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;)Ljava/lang/String;

    .line 3599
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # setter for: Lcom/sec/android/directshare/DirectShareService;->mVideoUri:Landroid/net/Uri;
    invoke-static {v0, p2}, Lcom/sec/android/directshare/DirectShareService;->access$9002(Lcom/sec/android/directshare/DirectShareService;Landroid/net/Uri;)Landroid/net/Uri;

    .line 3600
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    const-string v2, "text/DirectShareVideos"

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/directshare/DirectShareService;->downloadFileScanned(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    invoke-static {v0, v1, v2, v3, p2}, Lcom/sec/android/directshare/DirectShareService;->access$8700(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 3602
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "isVideo":Z
    :cond_4
    const-string v0, "text/DirectShareSNote"

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3604
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v11

    .line 3605
    .local v11, "noteUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    const-string v2, "text/DirectShareSNote"

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/directshare/DirectShareService;->downloadFileScanned(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    invoke-static {v0, v1, v2, v3, v11}, Lcom/sec/android/directshare/DirectShareService;->access$8700(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 3607
    .end local v11    # "noteUri":Landroid/net/Uri;
    :cond_5
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mFileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v2, v2, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->mMimeType:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v3, v3, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDeviceName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directshare/DirectShareService;->access$8600(Lcom/sec/android/directshare/DirectShareService;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/directshare/DirectShareService;->downloadFileScanned(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    invoke-static {v0, v1, v2, v3, p2}, Lcom/sec/android/directshare/DirectShareService;->access$8700(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 3620
    .restart local v12    # "size":I
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mStateLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$2700(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    .line 3621
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # invokes: Lcom/sec/android/directshare/DirectShareService;->stopService()V
    invoke-static {v0}, Lcom/sec/android/directshare/DirectShareService;->access$1300(Lcom/sec/android/directshare/DirectShareService;)V

    goto/16 :goto_1

    .line 3627
    :cond_6
    iget-object v0, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v0, v0, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v1, p0, Lcom/sec/android/directshare/DirectShareService$FileDownThread$1;->this$1:Lcom/sec/android/directshare/DirectShareService$FileDownThread;

    iget-object v1, v1, Lcom/sec/android/directshare/DirectShareService$FileDownThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    # getter for: Lcom/sec/android/directshare/DirectShareService;->mDownloadLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/android/directshare/DirectShareService;->access$8300(Lcom/sec/android/directshare/DirectShareService;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    # invokes: Lcom/sec/android/directshare/DirectShareService;->unlock(Ljava/util/concurrent/Semaphore;)V
    invoke-static {v0, v1}, Lcom/sec/android/directshare/DirectShareService;->access$3000(Lcom/sec/android/directshare/DirectShareService;Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_1

    .line 3618
    :pswitch_data_0
    .packed-switch 0x77
        :pswitch_0
    .end packed-switch
.end method
