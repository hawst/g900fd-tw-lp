.class public Lcom/sec/android/directshare/ui/NfcDisConnectActivity;
.super Landroid/app/Activity;
.source "NfcDisConnectActivity.java"


# instance fields
.field private mAnimation:Landroid/graphics/drawable/AnimationDrawable;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 67
    new-instance v0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity$1;-><init>(Lcom/sec/android/directshare/ui/NfcDisConnectActivity;)V

    iput-object v0, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const-string v1, "[SBeam]"

    const-string v2, "DirectShareNfcDisConnect:  onCreate "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->setContentView(I)V

    .line 27
    const/high16 v1, 0x7f070000

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 28
    .local v0, "imgAnim":Landroid/widget/ImageView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 29
    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 30
    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 31
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mIntentFilter:Landroid/content/IntentFilter;

    .line 32
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.directshare.STOP_DISCONNECT_UI_ACTION"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 33
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v2, "android.nfc.action.P2P_DISCONNECT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 34
    iget-object v1, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.directshare.CLIENT_SERVICE_STOP"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 58
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DirectShareNfcDisConnect:  onDestroy : isFinishing() = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 65
    :cond_1
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 53
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 40
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 41
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/directshare/ui/NfcDisConnectActivity;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 48
    :cond_0
    return-void
.end method
