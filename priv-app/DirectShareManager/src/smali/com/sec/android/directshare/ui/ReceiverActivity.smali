.class public Lcom/sec/android/directshare/ui/ReceiverActivity;
.super Landroid/app/Activity;
.source "ReceiverActivity.java"


# static fields
.field private static mDownloadState:I

.field private static service_start_check:Z


# instance fields
.field private WiFiDisplayChack:Z

.field private WiFiHotspotCheck:Z

.field private deviceName:Ljava/lang/String;

.field private isHome:Z

.field private mActivity:Landroid/app/Activity;

.field private mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

.field private mCount:I

.field private mDownloadCount:I

.field private mDownloadTotalCount:I

.field private mFileName:Ljava/lang/String;

.field private mImgPreparing:Landroid/widget/ImageView;

.field private mImgReceive:Landroid/widget/ImageView;

.field private mInitCheck:Z

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mLayoutFileInfo:Landroid/widget/LinearLayout;

.field private mLayoutPreparingTxt:Landroid/widget/LinearLayout;

.field private mProgress:I

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRegisterCheck:Z

.field private mSessionCheck:Z

.field private mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

.field private mSnbTypePath:Ljava/lang/String;

.field private mTotalCount:I

.field private mTxtFileCount:Landroid/widget/TextView;

.field private mTxtFileName:Landroid/widget/TextView;

.field private mTxtPercent:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    .line 56
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    .line 36
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;

    .line 37
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgPreparing:Landroid/widget/ImageView;

    .line 38
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    .line 39
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mLayoutPreparingTxt:Landroid/widget/LinearLayout;

    .line 40
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgReceive:Landroid/widget/ImageView;

    .line 41
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileCount:Landroid/widget/TextView;

    .line 42
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileName:Landroid/widget/TextView;

    .line 43
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtPercent:Landroid/widget/TextView;

    .line 44
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mLayoutFileInfo:Landroid/widget/LinearLayout;

    .line 45
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mInitCheck:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->isHome:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->WiFiHotspotCheck:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->WiFiDisplayChack:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mRegisterCheck:Z

    .line 57
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSnbTypePath:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    .line 59
    iput v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadCount:I

    .line 60
    iput v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadTotalCount:I

    .line 61
    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mFileName:Ljava/lang/String;

    .line 62
    iput v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgress:I

    .line 63
    iput v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mCount:I

    .line 64
    iput v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTotalCount:I

    .line 438
    new-instance v0, Lcom/sec/android/directshare/ui/ReceiverActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/ui/ReceiverActivity$3;-><init>(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 30
    sput p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    return p0
.end method

.method static synthetic access$100(Lcom/sec/android/directshare/ui/ReceiverActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->stopThread()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/directshare/ui/ReceiverActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadTotalCount:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/directshare/ui/ReceiverActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadTotalCount:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/directshare/ui/ReceiverActivity;)Lcom/sec/android/directshare/utils/SessionInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/directshare/ui/ReceiverActivity;Lcom/sec/android/directshare/utils/SessionInfo;)Lcom/sec/android/directshare/utils/SessionInfo;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # Lcom/sec/android/directshare/utils/SessionInfo;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/directshare/ui/ReceiverActivity;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/android/directshare/ui/ReceiverActivity;->udpateFileCount(II)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->updateFileName(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/directshare/ui/ReceiverActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->updateProgress(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/directshare/ui/ReceiverActivity;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->viewSNote(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/directshare/ui/ReceiverActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->isHome:Z

    return v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 30
    sput-boolean p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    return p0
.end method

.method static synthetic access$300(Lcom/sec/android/directshare/ui/ReceiverActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/directshare/ui/ReceiverActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/directshare/ui/ReceiverActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/directshare/ui/ReceiverActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/directshare/ui/ReceiverActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->requestFileInfo()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/directshare/ui/ReceiverActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startDownloadUi()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/directshare/ui/ReceiverActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mInitCheck:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/directshare/ui/ReceiverActivity;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/directshare/ui/ReceiverActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadCount:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/directshare/ui/ReceiverActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/ui/ReceiverActivity;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadCount:I

    return p1
.end method

.method private getSnbType(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 427
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReceiverActivity: getSnbType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 429
    .local v0, "fileExt":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "snb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "spd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 430
    :cond_0
    iput-object p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSnbTypePath:Ljava/lang/String;

    .line 434
    :goto_0
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSnbTypePath:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 435
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSnbTypePath:Ljava/lang/String;

    .line 436
    :goto_1
    return-object v1

    .line 432
    :cond_1
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReceiverActivity: checkSnbType : exception! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 436
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private initIntentFilter()V
    .locals 2

    .prologue
    .line 288
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    .line 289
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_INFO_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_CANCELED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_START_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_STATE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_COMPLETED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_SCANNER_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.directshare.DOWNLOAD_ERROR_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 296
    return-void
.end method

.method private initUi()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 189
    const v1, 0x7f070003

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgPreparing:Landroid/widget/ImageView;

    .line 190
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgPreparing:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 191
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgPreparing:Landroid/widget/ImageView;

    const v2, 0x7f020003

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 192
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgPreparing:Landroid/widget/ImageView;

    const v2, 0x7f040001

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 193
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgPreparing:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    .line 195
    const v1, 0x7f070006

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mLayoutPreparingTxt:Landroid/widget/LinearLayout;

    .line 203
    const v1, 0x7f070004

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgReceive:Landroid/widget/ImageView;

    .line 205
    const v1, 0x7f070005

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileCount:Landroid/widget/TextView;

    .line 207
    const v1, 0x7f070009

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mLayoutFileInfo:Landroid/widget/LinearLayout;

    .line 208
    const v1, 0x7f07000a

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileName:Landroid/widget/TextView;

    .line 209
    const v1, 0x7f07000b

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtPercent:Landroid/widget/TextView;

    .line 210
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtPercent:Landroid/widget/TextView;

    const-string v2, "%d%%"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    const v1, 0x7f07000c

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 213
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 214
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 217
    const v1, 0x7f07000d

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 218
    .local v0, "btnCancel":Landroid/widget/Button;
    new-instance v1, Lcom/sec/android/directshare/ui/ReceiverActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/directshare/ui/ReceiverActivity$1;-><init>(Lcom/sec/android/directshare/ui/ReceiverActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    new-instance v1, Lcom/sec/android/directshare/ui/ReceiverActivity$2;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity$2;-><init>(Lcom/sec/android/directshare/ui/ReceiverActivity;Landroid/widget/Button;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 252
    return-void
.end method

.method private requestFileInfo()V
    .locals 2

    .prologue
    .line 307
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.REQ_DOWNLOAD_INFO_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 308
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 310
    return-void
.end method

.method private resultHotspotPopup(I)V
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 312
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.MOBILEAP_POPUP_RESULT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 313
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 314
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 315
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 316
    return-void
.end method

.method private resultWfdPopup(I)V
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 318
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.WFD_POPUP_RESULT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 319
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 321
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 322
    return-void
.end method

.method private showHotspotPopup()V
    .locals 3

    .prologue
    .line 327
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 328
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "POPUP_MODE"

    const-string v2, "hotspot_disconnect"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 331
    return-void
.end method

.method private showStorageFullPopup()V
    .locals 3

    .prologue
    .line 339
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 340
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "POPUP_MODE"

    const-string v2, "disk_full"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startActivity(Landroid/content/Intent;)V

    .line 343
    return-void
.end method

.method private showWfdPopup()V
    .locals 3

    .prologue
    .line 333
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 334
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "POPUP_MODE"

    const-string v2, "allshare_disconnect"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 335
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    const/16 v1, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 337
    return-void
.end method

.method private startDownloadUi()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 260
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->stopAniPreparing()V

    .line 261
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgPreparing:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mImgReceive:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mLayoutPreparingTxt:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mLayoutFileInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileCount:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 267
    return-void
.end method

.method private stopAniPreparing()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 286
    :cond_0
    return-void
.end method

.method private stopThread()V
    .locals 3

    .prologue
    .line 301
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.STOP_THREAD_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 302
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "threadName"

    const-string v2, "threadName"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 305
    return-void
.end method

.method private udpateFileCount(II)V
    .locals 5
    .param p1, "index"    # I
    .param p2, "totalCount"    # I

    .prologue
    .line 278
    iput p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mCount:I

    .line 279
    iput p2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTotalCount:I

    .line 280
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileCount:Landroid/widget/TextView;

    const v1, 0x7f050001

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    return-void
.end method

.method private updateFileName(Ljava/lang/String;)V
    .locals 1
    .param p1, "FileName"    # Ljava/lang/String;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mFileName:Ljava/lang/String;

    .line 275
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    return-void
.end method

.method private updateProgress(I)V
    .locals 5
    .param p1, "progress"    # I

    .prologue
    .line 269
    iput p1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgress:I

    .line 270
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtPercent:Landroid/widget/TextView;

    const-string v1, "%d%%"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    return-void
.end method

.method private updateUi()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 254
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgressBar:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgress:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtPercent:Landroid/widget/TextView;

    const-string v1, "%d%%"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mProgress:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTxtFileCount:Landroid/widget/TextView;

    const v1, 0x7f050001

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mTotalCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    return-void
.end method

.method private viewSNote(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 408
    const-string v6, "filepath"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getSnbType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 409
    .local v1, "filePath":Ljava/lang/String;
    const-string v6, "mimetype"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 410
    .local v3, "mimetype":Ljava/lang/String;
    const-string v6, "uri"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 411
    .local v4, "uri":Landroid/net/Uri;
    const-string v6, "device_name"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->deviceName:Ljava/lang/String;

    .line 412
    iget-boolean v6, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->isHome:Z

    if-nez v6, :cond_0

    .line 413
    new-instance v2, Lcom/sec/android/directshare/utils/Launcher;

    iget-object v6, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v6, v3, v4, v1}, Lcom/sec/android/directshare/utils/Launcher;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 414
    .local v2, "launcher":Lcom/sec/android/directshare/utils/Launcher;
    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/Launcher;->build()Landroid/content/Intent;

    move-result-object v5

    .line 416
    .local v5, "viewIntent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v5}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    .end local v2    # "launcher":Lcom/sec/android/directshare/utils/Launcher;
    .end local v5    # "viewIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    const/4 v6, -0x1

    sput v6, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    .line 423
    iget-object v6, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    .line 424
    const/4 v6, 0x1

    sput-boolean v6, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    .line 425
    return-void

    .line 417
    .restart local v2    # "launcher":Lcom/sec/android/directshare/utils/Launcher;
    .restart local v5    # "viewIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 418
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/Launcher;->showActivityNotFound()V

    goto :goto_0
.end method


# virtual methods
.method public NdefMessageDistinction(Landroid/content/Intent;Z)Ljava/lang/Boolean;
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "check"    # Z

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 348
    invoke-static {p1}, Lcom/sec/android/directshare/utils/NdefParser;->isNdef(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 349
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 405
    :goto_0
    return-object v2

    .line 351
    :cond_0
    if-eqz p2, :cond_1

    .line 352
    sput-boolean v5, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    .line 354
    :cond_1
    const-string v2, "[SBeam]"

    const-string v3, "ReceiverActivity:  NdefMessageDistinction "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isAllowWifiByDevicePolicy(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 356
    sput v4, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    .line 357
    invoke-static {p0, v6}, Lcom/sec/android/directshare/utils/Utils;->turnOn(Landroid/content/Context;Z)V

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f050023

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->finish()V

    .line 361
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 363
    :cond_2
    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isAllowSBeamOnOperatorAirplaneMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 364
    sput v4, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    .line 365
    invoke-static {p0, v6}, Lcom/sec/android/directshare/utils/Utils;->turnOn(Landroid/content/Context;Z)V

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f050024

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 368
    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->finish()V

    .line 369
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 371
    :cond_3
    invoke-static {p1}, Lcom/sec/android/directshare/utils/NdefParser;->getSessionInfo(Landroid/content/Intent;)Lcom/sec/android/directshare/utils/SessionInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    .line 372
    iput-boolean v5, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z

    .line 373
    iget-object v2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/SessionInfo;->getTotalSize()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/directshare/utils/Utils;->isAvailableStorage(J)Z

    move-result v1

    .line 374
    .local v1, "isAvailable":Z
    if-nez v1, :cond_4

    .line 375
    sput v4, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    .line 376
    const-string v2, "[SBeam]"

    const-string v3, "ReceiverActivity: NdefMessageDistinction: size_check is false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->showStorageFullPopup()V

    .line 378
    iget-object v2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 379
    sput-boolean v5, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    .line 380
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 382
    :cond_4
    const-string v2, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ReceiverActivity: NdefMessageDistinction: service_start_check=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    sget-boolean v2, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    if-eqz v2, :cond_7

    .line 385
    iget-object v2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/directshare/utils/Utils;->isMobileAPEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 386
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->showHotspotPopup()V

    .line 387
    iput-boolean v5, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->WiFiHotspotCheck:Z

    .line 389
    :cond_5
    iget-object v2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/directshare/utils/Utils;->isWfdEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 390
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->showWfdPopup()V

    .line 391
    iput-boolean v5, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->WiFiDisplayChack:Z

    .line 393
    :cond_6
    iget-object v2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v2, v6}, Lcom/sec/android/directshare/utils/SessionInfo;->getFileInfo(I)Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/directshare/ui/ReceiverActivity;->updateFileName(Ljava/lang/String;)V

    .line 394
    iget-object v2, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/SessionInfo;->getFileCount()I

    move-result v2

    invoke-direct {p0, v5, v2}, Lcom/sec/android/directshare/ui/ReceiverActivity;->udpateFileCount(II)V

    .line 395
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.directshare.DIRECT_SHARE_DOWNLOAD_ACTION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 396
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "session"

    iget-object v3, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionInfo:Lcom/sec/android/directshare/utils/SessionInfo;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 397
    const-string v2, "WiFiDisplayChack"

    iget-boolean v3, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->WiFiDisplayChack:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 398
    const-string v2, "WiFiHotspotCheck"

    iget-boolean v3, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->WiFiHotspotCheck:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 399
    const-string v2, "com.sec.android.directshare"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 400
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 402
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->requestFileInfo()V

    .line 403
    sput-boolean v6, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    .line 405
    .end local v0    # "i":Landroid/content/Intent;
    :cond_7
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 156
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 157
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReceiverActivity:  requestCode = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], resultCode = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 160
    invoke-direct {p0, p2}, Lcom/sec/android/directshare/ui/ReceiverActivity;->resultHotspotPopup(I)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    .line 162
    invoke-direct {p0, p2}, Lcom/sec/android/directshare/ui/ReceiverActivity;->resultWfdPopup(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 124
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 127
    :pswitch_0
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->setContentView(I)V

    .line 128
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->initUi()V

    .line 129
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->updateUi()V

    .line 130
    sget v0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    if-nez v0, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startDownloadUi()V

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const v3, 0x7f030001

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/ui/ReceiverActivity;->setContentView(I)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 70
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "fromNoti"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 71
    .local v0, "bFromNoti":Z
    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/directshare/utils/Utils;->isCalling(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f050019

    invoke-virtual {p0, v4}, Lcom/sec/android/directshare/ui/ReceiverActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 73
    sput v7, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->finish()V

    .line 75
    sput-boolean v6, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->initUi()V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->initIntentFilter()V

    .line 80
    iget-object v3, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v4, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/directshare/ui/ReceiverActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 81
    iput-boolean v6, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mRegisterCheck:Z

    .line 82
    iput-object p0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mActivity:Landroid/app/Activity;

    .line 83
    iput-boolean v5, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->isHome:Z

    .line 84
    iput-boolean v5, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mSessionCheck:Z

    .line 86
    if-eqz v0, :cond_3

    .line 87
    const-string v3, "complete"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 88
    .local v2, "isComplete":Z
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ReceiverActivity: onCreate : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    if-nez v2, :cond_0

    .line 90
    sget v3, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    if-nez v3, :cond_2

    .line 91
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->requestFileInfo()V

    .line 92
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->startDownloadUi()V

    .line 94
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mInitCheck:Z

    goto :goto_0

    .line 97
    .end local v2    # "isComplete":Z
    :cond_3
    sput v7, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    .line 98
    invoke-virtual {p0, v1, v5}, Lcom/sec/android/directshare/ui/ReceiverActivity;->NdefMessageDistinction(Landroid/content/Intent;Z)Ljava/lang/Boolean;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 109
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 110
    const-string v1, "[SBeam]"

    const-string v2, "ReceiverActivity:  onDestroy "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-boolean v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mRegisterCheck:Z

    if-eqz v1, :cond_0

    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/ui/ReceiverActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->stopAniPreparing()V

    .line 119
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/directshare/ui/ReceiverActivity;->service_start_check:Z

    .line 120
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReceiverActivity: onDestroy : IllegalArgumentException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 148
    const-string v0, "[SBeam]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReceiverActivity:  onNewIntent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    sget v0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mDownloadState:I

    if-eqz v0, :cond_0

    .line 151
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/directshare/ui/ReceiverActivity;->NdefMessageDistinction(Landroid/content/Intent;Z)Ljava/lang/Boolean;

    .line 152
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->isHome:Z

    .line 153
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 143
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 144
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->isHome:Z

    .line 140
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 104
    const-string v0, "[SBeam]"

    const-string v1, "ReceiverActivity: [ onUserLeaveHint]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->isHome:Z

    .line 106
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 167
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/directshare/ui/ReceiverActivity;->mAniPreparing:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 171
    :cond_0
    return-void
.end method
