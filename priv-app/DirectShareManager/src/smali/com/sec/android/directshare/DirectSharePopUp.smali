.class public Lcom/sec/android/directshare/DirectSharePopUp;
.super Landroid/app/Activity;
.source "DirectSharePopUp.java"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mFromService:Z

.field private mHandler:Landroid/os/Handler;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mPopupMode:Ljava/lang/String;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mTimeoutCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 182
    new-instance v0, Lcom/sec/android/directshare/DirectSharePopUp$6;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectSharePopUp$6;-><init>(Lcom/sec/android/directshare/DirectSharePopUp;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 207
    new-instance v0, Lcom/sec/android/directshare/DirectSharePopUp$7;

    invoke-direct {v0, p0}, Lcom/sec/android/directshare/DirectSharePopUp$7;-><init>(Lcom/sec/android/directshare/DirectSharePopUp;)V

    iput-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mHandler:Landroid/os/Handler;

    .line 242
    iput-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mIntentFilter:Landroid/content/IntentFilter;

    .line 243
    iput-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    .line 244
    iput-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    .line 245
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mFromService:Z

    .line 246
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/directshare/DirectSharePopUp;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectSharePopUp;
    .param p1, "x1"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/DirectSharePopUp;->resultAction(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectSharePopUp;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/directshare/DirectSharePopUp;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectSharePopUp;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/directshare/DirectSharePopUp;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectSharePopUp;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/directshare/DirectSharePopUp;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectSharePopUp;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I

    return v0
.end method

.method static synthetic access$410(Lcom/sec/android/directshare/DirectSharePopUp;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directshare/DirectSharePopUp;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I

    return v0
.end method

.method private createDialog(Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 10
    .param p1, "popupMode"    # Ljava/lang/String;

    .prologue
    const v9, 0x7f05001a

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x7f050009

    .line 92
    const-string v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectSharePopUp: createDialog : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const v3, 0x7f050006

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "lablel":Ljava/lang/String;
    const/4 v2, 0x0

    .line 96
    .local v2, "message":Ljava/lang/String;
    const-string v3, "s_beam_off"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 97
    const v3, 0x7f050007

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 124
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 125
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 126
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 128
    const-string v3, "allshare_disconnect"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "hotspot_disconnect"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 129
    :cond_0
    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/android/directshare/DirectSharePopUp$2;

    invoke-direct {v4, p0}, Lcom/sec/android/directshare/DirectSharePopUp$2;-><init>(Lcom/sec/android/directshare/DirectSharePopUp;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 136
    const v3, 0x104000a

    new-instance v4, Lcom/sec/android/directshare/DirectSharePopUp$3;

    invoke-direct {v4, p0}, Lcom/sec/android/directshare/DirectSharePopUp$3;-><init>(Lcom/sec/android/directshare/DirectSharePopUp;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 160
    :goto_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :goto_2
    return-object v3

    .line 98
    :cond_1
    const-string v3, "no_file_selected"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 99
    const v3, 0x7f05000b

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 100
    const v3, 0x7f050008

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 101
    :cond_2
    const-string v3, "failed"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 102
    const v3, 0x7f05000c

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 103
    :cond_3
    const-string v3, "does_not_saved"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 104
    const v3, 0x7f05000d

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-virtual {p0, v6}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 106
    :cond_4
    const-string v3, "disk_full"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 107
    const v3, 0x7f05000e

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 108
    const v3, 0x7f05000a

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 109
    :cond_5
    const-string v3, "from_cloud_file"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 110
    const v3, 0x7f05000f

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 111
    invoke-virtual {p0, v6}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 112
    :cond_6
    const-string v3, "from_drm_file"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 113
    const v3, 0x7f050010

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 114
    invoke-virtual {p0, v6}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 115
    :cond_7
    const-string v3, "allshare_disconnect"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 116
    const v3, 0x7f050012

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 117
    :cond_8
    const-string v3, "hotspot_disconnect"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 118
    const v3, 0x7f050013

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mTimeoutCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 119
    :cond_9
    const-string v3, "no_application"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 120
    const v3, 0x7f05001f

    invoke-virtual {p0, v3}, Lcom/sec/android/directshare/DirectSharePopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 122
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 142
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_b
    const-string v3, "s_beam_off"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 143
    new-instance v3, Lcom/sec/android/directshare/DirectSharePopUp$4;

    invoke-direct {v3, p0}, Lcom/sec/android/directshare/DirectSharePopUp$4;-><init>(Lcom/sec/android/directshare/DirectSharePopUp;)V

    invoke-virtual {v0, v9, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 154
    :cond_c
    new-instance v3, Lcom/sec/android/directshare/DirectSharePopUp$5;

    invoke-direct {v3, p0}, Lcom/sec/android/directshare/DirectSharePopUp$5;-><init>(Lcom/sec/android/directshare/DirectSharePopUp;)V

    invoke-virtual {v0, v9, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1
.end method

.method private resultAction(I)V
    .locals 3
    .param p1, "result"    # I

    .prologue
    .line 164
    iget-boolean v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mFromService:Z

    if-eqz v1, :cond_2

    .line 165
    const-string v1, "allshare_disconnect"

    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.WFD_POPUP_RESULT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 169
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectSharePopUp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 179
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    const-string v1, "hotspot_disconnect"

    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.MOBILEAP_POPUP_RESULT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 172
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectSharePopUp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 177
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/directshare/DirectSharePopUp;->setResult(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 36
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/directshare/DirectSharePopUp;->requestWindowFeature(I)Z

    .line 38
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mIntentFilter:Landroid/content/IntentFilter;

    .line 39
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v2, "android.nfc.action.P2P_DISCONNECT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 40
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.directshare.STOP_DISCONNECT_UI_ACTION"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 41
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/directshare/DirectSharePopUp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectSharePopUp;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/directshare/utils/NdefParser;->isNdef(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectSharePopUp;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/directshare/utils/NdefParser;->getPopupMode(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    .line 48
    :goto_0
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectSharePopUp: onCreate : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectSharePopUp;->finish()V

    .line 77
    :goto_1
    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectSharePopUp;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "POPUP_MODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectSharePopUp;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "from_service"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mFromService:Z

    goto :goto_0

    .line 53
    :cond_1
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mPopupMode:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/directshare/DirectSharePopUp;->createDialog(Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    .line 54
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    if-nez v1, :cond_2

    .line 55
    const-string v1, "[SBeam]"

    const-string v2, "DirectSharePopUp: onCreate : fail to createDialog"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/directshare/DirectSharePopUp;->finish()V

    goto :goto_1

    .line 59
    :cond_2
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 60
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/directshare/DirectSharePopUp$1;

    invoke-direct {v2, p0}, Lcom/sec/android/directshare/DirectSharePopUp$1;-><init>(Lcom/sec/android/directshare/DirectSharePopUp;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 72
    :try_start_0
    const-string v1, "[SBeam]"

    const-string v2, "DirectSharePopUp: show"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectSharePopUp: onCreate : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 82
    const-string v0, "[SBeam]"

    const-string v1, "DirectSharePopUp:  onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/directshare/DirectSharePopUp;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/sec/android/directshare/DirectSharePopUp;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 89
    :cond_1
    return-void
.end method
