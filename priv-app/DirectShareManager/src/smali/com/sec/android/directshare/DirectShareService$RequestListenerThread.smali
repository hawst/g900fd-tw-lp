.class public Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;
.super Ljava/lang/Thread;
.source "DirectShareService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directshare/DirectShareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RequestListenerThread"
.end annotation


# instance fields
.field private final httpService:Lorg/apache/http/protocol/HttpService;

.field private final params:Lorg/apache/http/params/HttpParams;

.field private final serversocket:Ljava/net/ServerSocket;

.field final synthetic this$0:Lcom/sec/android/directshare/DirectShareService;

.field private workers:Ljava/lang/ThreadGroup;


# direct methods
.method public constructor <init>(Lcom/sec/android/directshare/DirectShareService;Landroid/app/Service;Ljava/lang/String;ILjava/lang/String;Lcom/sec/android/directshare/DirectShareService$DownloadListener;)V
    .locals 6
    .param p2, "service"    # Landroid/app/Service;
    .param p3, "ipaddr"    # Ljava/lang/String;
    .param p4, "port"    # I
    .param p5, "docroot"    # Ljava/lang/String;
    .param p6, "downloadListener"    # Lcom/sec/android/directshare/DirectShareService$DownloadListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2670
    iput-object p1, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2667
    new-instance v2, Ljava/lang/ThreadGroup;

    const-string v3, "WorKerGroup"

    invoke-direct {v2, v3}, Ljava/lang/ThreadGroup;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->workers:Ljava/lang/ThreadGroup;

    .line 2674
    new-instance v2, Ljava/net/ServerSocket;

    invoke-direct {v2, p4}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    .line 2675
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->params:Lorg/apache/http/params/HttpParams;

    .line 2676
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->params:Lorg/apache/http/params/HttpParams;

    const-string v3, "http.socket.timeout"

    const/16 v4, 0x7530

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.socket.buffer-size"

    const/high16 v4, 0x10000

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.connection.stalecheck"

    invoke-interface {v2, v3, v5}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.tcp.nodelay"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.origin-server"

    const-string v4, "HttpComponents/1.1"

    invoke-interface {v2, v3, v4}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v3, "http.socket.linger"

    invoke-interface {v2, v3, v5}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 2687
    new-instance v0, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    .line 2688
    .local v0, "httpproc":Lorg/apache/http/protocol/BasicHttpProcessor;
    new-instance v2, Lorg/apache/http/protocol/ResponseDate;

    invoke-direct {v2}, Lorg/apache/http/protocol/ResponseDate;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 2689
    new-instance v2, Lorg/apache/http/protocol/ResponseServer;

    invoke-direct {v2}, Lorg/apache/http/protocol/ResponseServer;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 2690
    new-instance v2, Lorg/apache/http/protocol/ResponseContent;

    invoke-direct {v2}, Lorg/apache/http/protocol/ResponseContent;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 2691
    new-instance v2, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v2}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 2693
    new-instance v1, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v1}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    .line 2694
    .local v1, "reqistry":Lorg/apache/http/protocol/HttpRequestHandlerRegistry;
    const-string v2, "*"

    new-instance v3, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;

    invoke-direct {v3, p1, p5, p6}, Lcom/sec/android/directshare/DirectShareService$HttpFileHandler;-><init>(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/String;Lcom/sec/android/directshare/DirectShareService$DownloadListener;)V

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 2696
    new-instance v2, Lorg/apache/http/protocol/HttpService;

    new-instance v3, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v3}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v4, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v4}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v2, v0, v3, v4}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    iput-object v2, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->httpService:Lorg/apache/http/protocol/HttpService;

    .line 2698
    iget-object v2, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->httpService:Lorg/apache/http/protocol/HttpService;

    invoke-virtual {v2, v1}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 2702
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 2727
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DirectShareService: RequestListenerThread start. port=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    invoke-virtual {v7}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2731
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/net/ServerSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2735
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2738
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    invoke-virtual {v5}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    .line 2739
    .local v3, "socket":Ljava/net/Socket;
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mSocketList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$6300()Ljava/util/ArrayList;

    move-result-object v6

    monitor-enter v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2740
    :try_start_2
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mSocketList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$6300()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2741
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2742
    :try_start_3
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DirectShareService: RequestListenerThread: Incoming connection from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v7

    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2744
    new-instance v0, Lorg/apache/http/impl/DefaultHttpServerConnection;

    invoke-direct {v0}, Lorg/apache/http/impl/DefaultHttpServerConnection;-><init>()V

    .line 2745
    .local v0, "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->params:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v0, v3, v5}, Lorg/apache/http/impl/DefaultHttpServerConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    .line 2747
    new-instance v4, Lcom/sec/android/directshare/DirectShareService$WorkerThread;

    iget-object v5, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->this$0:Lcom/sec/android/directshare/DirectShareService;

    iget-object v6, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->workers:Ljava/lang/ThreadGroup;

    iget-object v7, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->httpService:Lorg/apache/http/protocol/HttpService;

    invoke-direct {v4, v5, v6, v7, v0}, Lcom/sec/android/directshare/DirectShareService$WorkerThread;-><init>(Lcom/sec/android/directshare/DirectShareService;Ljava/lang/ThreadGroup;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;)V

    .line 2748
    .local v4, "t":Ljava/lang/Thread;
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 2749
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 2750
    .end local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v3    # "socket":Ljava/net/Socket;
    .end local v4    # "t":Ljava/lang/Thread;
    :catch_0
    move-exception v1

    .line 2755
    :cond_0
    const-string v5, "[SBeam]"

    const-string v6, "DirectShareService: RequestListenerThread end."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2756
    return-void

    .line 2732
    :catch_1
    move-exception v2

    .line 2733
    .local v2, "e1":Ljava/lang/Exception;
    const-string v5, "[SBeam]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DirectShareService: RequestListenerThread: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2741
    .end local v2    # "e1":Ljava/lang/Exception;
    .restart local v3    # "socket":Ljava/net/Socket;
    :catchall_0
    move-exception v5

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v5
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
.end method

.method public stopThread()V
    .locals 7

    .prologue
    .line 2705
    const-string v4, "[SBeam]"

    const-string v5, "DirectShareService: RequestListenerThread : stopThread"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2706
    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->httpService:Lorg/apache/http/protocol/HttpService;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 2709
    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->workers:Ljava/lang/ThreadGroup;

    invoke-virtual {v4}, Ljava/lang/ThreadGroup;->interrupt()V

    .line 2711
    const/4 v3, 0x0

    .line 2712
    .local v3, "socket":Ljava/net/Socket;
    :try_start_0
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mSocketList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$6300()Ljava/util/ArrayList;

    move-result-object v5

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2713
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_1
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mSocketList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$6300()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 2714
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mSocketList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$6300()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/net/Socket;

    move-object v3, v0

    .line 2715
    invoke-virtual {v3}, Ljava/net/Socket;->close()V

    .line 2713
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2717
    :cond_0
    # getter for: Lcom/sec/android/directshare/DirectShareService;->mSocketList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/directshare/DirectShareService;->access$6300()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 2718
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2719
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    if-eqz v4, :cond_1

    .line 2720
    iget-object v4, p0, Lcom/sec/android/directshare/DirectShareService$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2724
    .end local v2    # "i":I
    :cond_1
    :goto_1
    return-void

    .line 2718
    .restart local v2    # "i":I
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 2721
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 2722
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "[SBeam]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DirectShareService: RequestListenerThread : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
