.class Lcom/sec/android/directshare/DirectSharePopUp$4;
.super Ljava/lang/Object;
.source "DirectSharePopUp.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/directshare/DirectSharePopUp;->createDialog(Ljava/lang/String;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directshare/DirectSharePopUp;


# direct methods
.method constructor <init>(Lcom/sec/android/directshare/DirectSharePopUp;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/android/directshare/DirectSharePopUp$4;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 146
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp$4;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.settings.SBEAM_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/directshare/DirectSharePopUp;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_0
    iget-object v1, p0, Lcom/sec/android/directshare/DirectSharePopUp$4;->this$0:Lcom/sec/android/directshare/DirectSharePopUp;

    invoke-virtual {v1}, Lcom/sec/android/directshare/DirectSharePopUp;->finish()V

    .line 151
    return-void

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DirectSharePopUp: createDialog : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
