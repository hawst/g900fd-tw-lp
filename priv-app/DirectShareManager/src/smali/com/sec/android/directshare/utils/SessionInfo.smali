.class public Lcom/sec/android/directshare/utils/SessionInfo;
.super Ljava/lang/Object;
.source "SessionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/directshare/utils/SessionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMacAddr:Ljava/lang/String;

.field private mMimeType:Ljava/lang/String;

.field private mTotalSize:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/sec/android/directshare/utils/SessionInfo$1;

    invoke-direct {v0}, Lcom/sec/android/directshare/utils/SessionInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/directshare/utils/SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMacAddr:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMimeType:Ljava/lang/String;

    .line 12
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mTotalSize:J

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    .line 16
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMacAddr:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMimeType:Ljava/lang/String;

    .line 12
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mTotalSize:J

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMacAddr:Ljava/lang/String;

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMimeType:Ljava/lang/String;

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mTotalSize:J

    .line 34
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/directshare/utils/SessionInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/directshare/utils/SessionInfo$1;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/sec/android/directshare/utils/SessionInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public addFileInfo(Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;)V
    .locals 4
    .param p1, "f"    # Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mTotalSize:J

    invoke-virtual {p1}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->getFilelen()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mTotalSize:J

    .line 81
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public exist(Ljava/lang/String;)Z
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 19
    const/4 v0, 0x0

    .line 20
    .local v0, "flag":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 21
    iget-object v3, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;

    .line 22
    .local v2, "sFile":Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    invoke-virtual {v2}, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 23
    const/4 v0, 0x1

    .line 27
    .end local v2    # "sFile":Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    :cond_0
    return v0

    .line 20
    .restart local v2    # "sFile":Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getFileCount()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFileInfo(I)Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/directshare/utils/SessionInfo$SFileInfo;

    return-object v0
.end method

.method public getMacAddr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMacAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalSize()J
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mTotalSize:J

    return-wide v0
.end method

.method public setMacAddr(Ljava/lang/String;)V
    .locals 0
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMacAddr:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMimeType:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setTotalSize(J)V
    .locals 1
    .param p1, "totalSize"    # J

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mTotalSize:J

    .line 93
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMacAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mMimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    iget-wide v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mTotalSize:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/directshare/utils/SessionInfo;->mList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 48
    return-void
.end method
