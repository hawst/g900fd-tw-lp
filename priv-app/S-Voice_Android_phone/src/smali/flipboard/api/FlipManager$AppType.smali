.class final enum Lflipboard/api/FlipManager$AppType;
.super Ljava/lang/Enum;
.source "FlipManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflipboard/api/FlipManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AppType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/api/FlipManager$AppType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lflipboard/api/FlipManager$AppType;

.field public static final enum CHINA:Lflipboard/api/FlipManager$AppType;

.field public static final enum DEBUG:Lflipboard/api/FlipManager$AppType;

.field public static final enum NONE:Lflipboard/api/FlipManager$AppType;

.field public static final enum REGULAR:Lflipboard/api/FlipManager$AppType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lflipboard/api/FlipManager$AppType;

    const-string/jumbo v1, "REGULAR"

    invoke-direct {v0, v1, v2}, Lflipboard/api/FlipManager$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$AppType;->REGULAR:Lflipboard/api/FlipManager$AppType;

    .line 31
    new-instance v0, Lflipboard/api/FlipManager$AppType;

    const-string/jumbo v1, "CHINA"

    invoke-direct {v0, v1, v3}, Lflipboard/api/FlipManager$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$AppType;->CHINA:Lflipboard/api/FlipManager$AppType;

    .line 32
    new-instance v0, Lflipboard/api/FlipManager$AppType;

    const-string/jumbo v1, "DEBUG"

    invoke-direct {v0, v1, v4}, Lflipboard/api/FlipManager$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$AppType;->DEBUG:Lflipboard/api/FlipManager$AppType;

    .line 33
    new-instance v0, Lflipboard/api/FlipManager$AppType;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v5}, Lflipboard/api/FlipManager$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$AppType;->NONE:Lflipboard/api/FlipManager$AppType;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/api/FlipManager$AppType;

    sget-object v1, Lflipboard/api/FlipManager$AppType;->REGULAR:Lflipboard/api/FlipManager$AppType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/api/FlipManager$AppType;->CHINA:Lflipboard/api/FlipManager$AppType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/api/FlipManager$AppType;->DEBUG:Lflipboard/api/FlipManager$AppType;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/api/FlipManager$AppType;->NONE:Lflipboard/api/FlipManager$AppType;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/api/FlipManager$AppType;->$VALUES:[Lflipboard/api/FlipManager$AppType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/api/FlipManager$AppType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lflipboard/api/FlipManager$AppType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/api/FlipManager$AppType;

    return-object v0
.end method

.method public static values()[Lflipboard/api/FlipManager$AppType;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lflipboard/api/FlipManager$AppType;->$VALUES:[Lflipboard/api/FlipManager$AppType;

    invoke-virtual {v0}, [Lflipboard/api/FlipManager$AppType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/api/FlipManager$AppType;

    return-object v0
.end method


# virtual methods
.method getContentUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    sget-object v0, Lflipboard/api/FlipManager$1;->$SwitchMap$flipboard$api$FlipManager$AppType:[I

    invoke-virtual {p0}, Lflipboard/api/FlipManager$AppType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 41
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 38
    :pswitch_0
    const-string/jumbo v0, "content://flipboard.remoteservice.feeds/feeditems"

    goto :goto_0

    .line 39
    :pswitch_1
    const-string/jumbo v0, "content://flipboard.remoteservice.feeds.china/feeditems"

    goto :goto_0

    .line 40
    :pswitch_2
    const-string/jumbo v0, "content://flipboard.remoteservice.feeds.debug/feeditems"

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
