.class public final enum Lflipboard/api/FlipManager$ErrorMessage;
.super Ljava/lang/Enum;
.source "FlipManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflipboard/api/FlipManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/api/FlipManager$ErrorMessage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lflipboard/api/FlipManager$ErrorMessage;

.field public static final enum DISCONNECTED_FROM_FLIPBOARD:Lflipboard/api/FlipManager$ErrorMessage;

.field public static final enum DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

.field public static final enum INVALID_ARGUMENTS:Lflipboard/api/FlipManager$ErrorMessage;

.field public static final enum NO_NETWORK_CONNECTIVITY:Lflipboard/api/FlipManager$ErrorMessage;

.field public static final enum TIMEOUT:Lflipboard/api/FlipManager$ErrorMessage;

.field public static final enum UNKNOWN:Lflipboard/api/FlipManager$ErrorMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    new-instance v0, Lflipboard/api/FlipManager$ErrorMessage;

    const-string/jumbo v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lflipboard/api/FlipManager$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$ErrorMessage;->UNKNOWN:Lflipboard/api/FlipManager$ErrorMessage;

    .line 65
    new-instance v0, Lflipboard/api/FlipManager$ErrorMessage;

    const-string/jumbo v1, "DUPLICATE_REQUEST"

    invoke-direct {v0, v1, v4}, Lflipboard/api/FlipManager$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$ErrorMessage;->DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

    .line 66
    new-instance v0, Lflipboard/api/FlipManager$ErrorMessage;

    const-string/jumbo v1, "INVALID_ARGUMENTS"

    invoke-direct {v0, v1, v5}, Lflipboard/api/FlipManager$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$ErrorMessage;->INVALID_ARGUMENTS:Lflipboard/api/FlipManager$ErrorMessage;

    .line 67
    new-instance v0, Lflipboard/api/FlipManager$ErrorMessage;

    const-string/jumbo v1, "NO_NETWORK_CONNECTIVITY"

    invoke-direct {v0, v1, v6}, Lflipboard/api/FlipManager$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$ErrorMessage;->NO_NETWORK_CONNECTIVITY:Lflipboard/api/FlipManager$ErrorMessage;

    .line 71
    new-instance v0, Lflipboard/api/FlipManager$ErrorMessage;

    const-string/jumbo v1, "DISCONNECTED_FROM_FLIPBOARD"

    invoke-direct {v0, v1, v7}, Lflipboard/api/FlipManager$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$ErrorMessage;->DISCONNECTED_FROM_FLIPBOARD:Lflipboard/api/FlipManager$ErrorMessage;

    .line 72
    new-instance v0, Lflipboard/api/FlipManager$ErrorMessage;

    const-string/jumbo v1, "TIMEOUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/api/FlipManager$ErrorMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$ErrorMessage;->TIMEOUT:Lflipboard/api/FlipManager$ErrorMessage;

    .line 63
    const/4 v0, 0x6

    new-array v0, v0, [Lflipboard/api/FlipManager$ErrorMessage;

    sget-object v1, Lflipboard/api/FlipManager$ErrorMessage;->UNKNOWN:Lflipboard/api/FlipManager$ErrorMessage;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/api/FlipManager$ErrorMessage;->DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/api/FlipManager$ErrorMessage;->INVALID_ARGUMENTS:Lflipboard/api/FlipManager$ErrorMessage;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/api/FlipManager$ErrorMessage;->NO_NETWORK_CONNECTIVITY:Lflipboard/api/FlipManager$ErrorMessage;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/api/FlipManager$ErrorMessage;->DISCONNECTED_FROM_FLIPBOARD:Lflipboard/api/FlipManager$ErrorMessage;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/api/FlipManager$ErrorMessage;->TIMEOUT:Lflipboard/api/FlipManager$ErrorMessage;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/api/FlipManager$ErrorMessage;->$VALUES:[Lflipboard/api/FlipManager$ErrorMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/api/FlipManager$ErrorMessage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const-class v0, Lflipboard/api/FlipManager$ErrorMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/api/FlipManager$ErrorMessage;

    return-object v0
.end method

.method public static values()[Lflipboard/api/FlipManager$ErrorMessage;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lflipboard/api/FlipManager$ErrorMessage;->$VALUES:[Lflipboard/api/FlipManager$ErrorMessage;

    invoke-virtual {v0}, [Lflipboard/api/FlipManager$ErrorMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/api/FlipManager$ErrorMessage;

    return-object v0
.end method
