.class public final enum Lflipboard/api/FlipManager$ContentType;
.super Ljava/lang/Enum;
.source "FlipManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflipboard/api/FlipManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/api/FlipManager$ContentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lflipboard/api/FlipManager$ContentType;

.field public static final enum AUDIO:Lflipboard/api/FlipManager$ContentType;

.field public static final enum TEXT:Lflipboard/api/FlipManager$ContentType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lflipboard/api/FlipManager$ContentType;

    const-string/jumbo v1, "TEXT"

    invoke-direct {v0, v1, v2}, Lflipboard/api/FlipManager$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$ContentType;->TEXT:Lflipboard/api/FlipManager$ContentType;

    new-instance v0, Lflipboard/api/FlipManager$ContentType;

    const-string/jumbo v1, "AUDIO"

    invoke-direct {v0, v1, v3}, Lflipboard/api/FlipManager$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/api/FlipManager$ContentType;->AUDIO:Lflipboard/api/FlipManager$ContentType;

    .line 49
    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/api/FlipManager$ContentType;

    sget-object v1, Lflipboard/api/FlipManager$ContentType;->TEXT:Lflipboard/api/FlipManager$ContentType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/api/FlipManager$ContentType;->AUDIO:Lflipboard/api/FlipManager$ContentType;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/api/FlipManager$ContentType;->$VALUES:[Lflipboard/api/FlipManager$ContentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/api/FlipManager$ContentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lflipboard/api/FlipManager$ContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/api/FlipManager$ContentType;

    return-object v0
.end method

.method public static values()[Lflipboard/api/FlipManager$ContentType;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lflipboard/api/FlipManager$ContentType;->$VALUES:[Lflipboard/api/FlipManager$ContentType;

    invoke-virtual {v0}, [Lflipboard/api/FlipManager$ContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/api/FlipManager$ContentType;

    return-object v0
.end method
