.class Lflipboard/api/MessengerProvider$1;
.super Ljava/lang/Object;
.source "MessengerProvider.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflipboard/api/MessengerProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lflipboard/api/MessengerProvider;


# direct methods
.method constructor <init>(Lflipboard/api/MessengerProvider;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 144
    iget-object v3, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    iget-object v4, v3, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    monitor-enter v4

    .line 148
    :try_start_0
    iget-object v3, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    new-instance v5, Landroid/os/Messenger;

    invoke-direct {v5, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v5, v3, Lflipboard/api/MessengerProvider;->mService:Landroid/os/Messenger;

    .line 149
    const-string/jumbo v3, "FlipboardLibrary"

    const-string/jumbo v5, "service attached "

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 153
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "LibraryVersion"

    const-string/jumbo v5, "1.861"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string/jumbo v3, "AppPackageName"

    iget-object v5, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    iget-object v5, v5, Lflipboard/api/MessengerProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v3, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    const/4 v5, 0x1

    # invokes: Lflipboard/api/MessengerProvider;->sendMessage(ILandroid/os/Bundle;)V
    invoke-static {v3, v5, v0}, Lflipboard/api/MessengerProvider;->access$000(Lflipboard/api/MessengerProvider;ILandroid/os/Bundle;)V

    .line 156
    iget-object v3, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    iget-object v3, v3, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/api/MessengerProvider$Request;

    .line 157
    .local v2, "req":Lflipboard/api/MessengerProvider$Request;
    iget-boolean v3, v2, Lflipboard/api/MessengerProvider$Request;->mIsSent:Z

    if-nez v3, :cond_0

    .line 158
    iget-object v3, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    const/4 v5, 0x3

    iget-object v6, v2, Lflipboard/api/MessengerProvider$Request;->mBundle:Landroid/os/Bundle;

    # invokes: Lflipboard/api/MessengerProvider;->sendMessage(ILandroid/os/Bundle;)V
    invoke-static {v3, v5, v6}, Lflipboard/api/MessengerProvider;->access$000(Lflipboard/api/MessengerProvider;ILandroid/os/Bundle;)V

    .line 159
    const/4 v3, 0x1

    iput-boolean v3, v2, Lflipboard/api/MessengerProvider$Request;->mIsSent:Z

    goto :goto_0

    .line 162
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "req":Lflipboard/api/MessengerProvider$Request;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 169
    iget-object v0, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/api/MessengerProvider;->mService:Landroid/os/Messenger;

    .line 170
    const-string/jumbo v0, "FlipboardLibrary"

    const-string/jumbo v1, "Disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v0, p0, Lflipboard/api/MessengerProvider$1;->this$0:Lflipboard/api/MessengerProvider;

    invoke-virtual {v0}, Lflipboard/api/MessengerProvider;->disconnectServer()Z

    .line 172
    return-void
.end method
