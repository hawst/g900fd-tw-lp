.class public Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;
.super Loauth/signpost/AbstractOAuthProvider;
.source "CommonsHttpOAuthProvider.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private httpClient:Lorg/apache/http/client/HttpClient;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestTokenEndpointUrl"    # Ljava/lang/String;
    .param p2, "accessTokenEndpointUrl"    # Ljava/lang/String;
    .param p3, "authorizationWebsiteUrl"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Loauth/signpost/AbstractOAuthProvider;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v0, p0, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 53
    return-void
.end method


# virtual methods
.method protected retrieveToken(Loauth/signpost/OAuthConsumer;Ljava/lang/String;)V
    .locals 13
    .param p1, "consumer"    # Loauth/signpost/OAuthConsumer;
    .param p2, "endpointUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Loauth/signpost/exception/OAuthMessageSignerException;,
            Loauth/signpost/exception/OAuthCommunicationException;,
            Loauth/signpost/exception/OAuthNotAuthorizedException;,
            Loauth/signpost/exception/OAuthExpectationFailedException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0}, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;->getRequestHeaders()Ljava/util/Map;

    move-result-object v0

    .line 61
    .local v0, "defaultHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p1}, Loauth/signpost/OAuthConsumer;->getConsumerKey()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-interface {p1}, Loauth/signpost/OAuthConsumer;->getConsumerSecret()Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_1

    .line 62
    :cond_0
    new-instance v11, Loauth/signpost/exception/OAuthExpectationFailedException;

    const-string/jumbo v12, "Consumer key or secret not set"

    invoke-direct {v11, v12}, Loauth/signpost/exception/OAuthExpectationFailedException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 65
    :cond_1
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 66
    .local v5, "request":Lorg/apache/http/client/methods/HttpGet;
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 67
    .local v3, "header":Ljava/lang/String;
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v5, v3, v11}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    .end local v3    # "header":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    .line 73
    .local v6, "response":Lorg/apache/http/HttpResponse;
    :try_start_0
    new-instance v11, Loauth/signpost/commonshttp/HttpRequestAdapter;

    invoke-direct {v11, v5}, Loauth/signpost/commonshttp/HttpRequestAdapter;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;)V

    invoke-interface {p1, v11}, Loauth/signpost/OAuthConsumer;->sign(Loauth/signpost/http/HttpRequest;)Loauth/signpost/http/HttpRequest;

    .line 75
    iget-object v11, p0, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v11, v5}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 77
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    .line 79
    .local v9, "statusCode":I
    const/16 v11, 0x191

    if-ne v9, v11, :cond_4

    .line 80
    new-instance v11, Loauth/signpost/exception/OAuthNotAuthorizedException;

    invoke-direct {v11}, Loauth/signpost/exception/OAuthNotAuthorizedException;-><init>()V

    throw v11
    :try_end_0
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    .end local v9    # "statusCode":I
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Loauth/signpost/exception/OAuthNotAuthorizedException;
    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    .end local v1    # "e":Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catchall_0
    move-exception v11

    if-eqz v6, :cond_3

    .line 109
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 110
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_3

    .line 113
    :try_start_2
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 119
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    :cond_3
    :goto_1
    throw v11

    .line 83
    .restart local v9    # "statusCode":I
    :cond_4
    :try_start_3
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v11

    invoke-static {v11}, Loauth/signpost/OAuth;->decodeForm(Ljava/io/InputStream;)Ljava/util/Map;

    move-result-object v7

    .line 86
    .local v7, "responseParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v11, "oauth_token"

    invoke-interface {v7, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 87
    .local v10, "token":Ljava/lang/String;
    const-string/jumbo v11, "oauth_token_secret"

    invoke-interface {v7, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 88
    .local v8, "secret":Ljava/lang/String;
    const-string/jumbo v11, "oauth_token"

    invoke-interface {v7, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    const-string/jumbo v11, "oauth_token_secret"

    invoke-interface {v7, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-virtual {p0, v7}, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;->setResponseParameters(Ljava/util/Map;)V

    .line 93
    if-eqz v10, :cond_5

    if-nez v8, :cond_6

    .line 94
    :cond_5
    new-instance v11, Loauth/signpost/exception/OAuthExpectationFailedException;

    const-string/jumbo v12, "Request token or token secret not set in server reply. The service provider you use is probably buggy."

    invoke-direct {v11, v12}, Loauth/signpost/exception/OAuthExpectationFailedException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_3
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    .end local v7    # "responseParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v8    # "secret":Ljava/lang/String;
    .end local v9    # "statusCode":I
    .end local v10    # "token":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 104
    .local v1, "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 99
    .end local v1    # "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    .restart local v7    # "responseParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v8    # "secret":Ljava/lang/String;
    .restart local v9    # "statusCode":I
    .restart local v10    # "token":Ljava/lang/String;
    :cond_6
    :try_start_5
    invoke-interface {p1, v10, v8}, Loauth/signpost/OAuthConsumer;->setTokenWithSecret(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 108
    if-eqz v6, :cond_7

    .line 109
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 110
    .restart local v2    # "entity":Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_7

    .line 113
    :try_start_6
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 121
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    :cond_7
    :goto_2
    return-void

    .line 114
    .restart local v2    # "entity":Lorg/apache/http/HttpEntity;
    :catch_2
    move-exception v1

    .line 116
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 105
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    .end local v7    # "responseParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v8    # "secret":Ljava/lang/String;
    .end local v9    # "statusCode":I
    .end local v10    # "token":Ljava/lang/String;
    :catch_3
    move-exception v1

    .line 106
    .local v1, "e":Ljava/lang/Exception;
    :try_start_7
    new-instance v11, Loauth/signpost/exception/OAuthCommunicationException;

    invoke-direct {v11, v1}, Loauth/signpost/exception/OAuthCommunicationException;-><init>(Ljava/lang/Exception;)V

    throw v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 114
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "entity":Lorg/apache/http/HttpEntity;
    :catch_4
    move-exception v1

    .line 116
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method setHttpClient(Lorg/apache/http/client/HttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lorg/apache/http/client/HttpClient;

    .prologue
    .line 124
    iput-object p1, p0, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 125
    return-void
.end method
