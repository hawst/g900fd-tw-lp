.class public abstract enum Lorg/apache/log4j/Logger$Level;
.super Ljava/lang/Enum;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/log4j/Logger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "Level"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/log4j/Logger$Level;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/log4j/Logger$Level;

.field public static final enum DEBUG:Lorg/apache/log4j/Logger$Level;

.field public static final enum ERROR:Lorg/apache/log4j/Logger$Level;

.field public static final enum INFO:Lorg/apache/log4j/Logger$Level;

.field public static final enum WARN:Lorg/apache/log4j/Logger$Level;


# instance fields
.field private level:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lorg/apache/log4j/Logger$Level$1;

    const-string/jumbo v1, "ERROR"

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/log4j/Logger$Level$1;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    .line 31
    new-instance v0, Lorg/apache/log4j/Logger$Level$2;

    const-string/jumbo v1, "WARN"

    invoke-direct {v0, v1, v3, v3}, Lorg/apache/log4j/Logger$Level$2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/log4j/Logger$Level;->WARN:Lorg/apache/log4j/Logger$Level;

    .line 42
    new-instance v0, Lorg/apache/log4j/Logger$Level$3;

    const-string/jumbo v1, "INFO"

    invoke-direct {v0, v1, v4, v4}, Lorg/apache/log4j/Logger$Level$3;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    .line 53
    new-instance v0, Lorg/apache/log4j/Logger$Level$4;

    const-string/jumbo v1, "DEBUG"

    invoke-direct {v0, v1, v5, v5}, Lorg/apache/log4j/Logger$Level$4;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    .line 19
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/log4j/Logger$Level;

    sget-object v1, Lorg/apache/log4j/Logger$Level;->ERROR:Lorg/apache/log4j/Logger$Level;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/log4j/Logger$Level;->WARN:Lorg/apache/log4j/Logger$Level;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/log4j/Logger$Level;->INFO:Lorg/apache/log4j/Logger$Level;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/log4j/Logger$Level;->DEBUG:Lorg/apache/log4j/Logger$Level;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/log4j/Logger$Level;->$VALUES:[Lorg/apache/log4j/Logger$Level;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput p3, p0, Lorg/apache/log4j/Logger$Level;->level:I

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILorg/apache/log4j/Logger$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # Lorg/apache/log4j/Logger$1;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/log4j/Logger$Level;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/log4j/Logger$Level;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lorg/apache/log4j/Logger$Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/log4j/Logger$Level;

    return-object v0
.end method

.method public static values()[Lorg/apache/log4j/Logger$Level;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lorg/apache/log4j/Logger$Level;->$VALUES:[Lorg/apache/log4j/Logger$Level;

    invoke-virtual {v0}, [Lorg/apache/log4j/Logger$Level;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/log4j/Logger$Level;

    return-object v0
.end method


# virtual methods
.method public compare(Lorg/apache/log4j/Logger$Level;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/log4j/Logger$Level;

    .prologue
    .line 76
    iget v0, p0, Lorg/apache/log4j/Logger$Level;->level:I

    iget v1, p1, Lorg/apache/log4j/Logger$Level;->level:I

    sub-int/2addr v0, v1

    return v0
.end method

.method abstract log(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method abstract log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public toInt()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/log4j/Logger$Level;->level:I

    return v0
.end method
