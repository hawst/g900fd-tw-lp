.class public Lorg/apache/commons/jexl2/Interpreter;
.super Ljava/lang/Object;
.source "Interpreter.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/ParserVisitor;


# static fields
.field protected static final EMPTY_PARAMS:[Ljava/lang/Object;


# instance fields
.field protected final arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

.field protected final cache:Z

.field protected final context:Lorg/apache/commons/jexl2/JexlContext;

.field protected final functions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected functors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected final logger:Lorg/apache/commons/logging/Log;

.field protected registers:[Ljava/lang/Object;

.field protected silent:Z

.field protected final strict:Z

.field protected final uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lorg/apache/commons/jexl2/Interpreter;->EMPTY_PARAMS:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;Lorg/apache/commons/jexl2/JexlContext;)V
    .locals 4
    .param p1, "jexl"    # Lorg/apache/commons/jexl2/JexlEngine;
    .param p2, "aContext"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    .line 123
    iget-object v0, p1, Lorg/apache/commons/jexl2/JexlEngine;->logger:Lorg/apache/commons/logging/Log;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    .line 124
    iget-object v0, p1, Lorg/apache/commons/jexl2/JexlEngine;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    .line 125
    iget-object v0, p1, Lorg/apache/commons/jexl2/JexlEngine;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    .line 126
    iget-object v0, p1, Lorg/apache/commons/jexl2/JexlEngine;->functions:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->functions:Ljava/util/Map;

    .line 127
    iget-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->isLenient()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    .line 128
    iget-boolean v0, p1, Lorg/apache/commons/jexl2/JexlEngine;->silent:Z

    iput-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    .line 129
    iget-object v0, p1, Lorg/apache/commons/jexl2/JexlEngine;->cache:Lorg/apache/commons/jexl2/JexlEngine$SoftCache;

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    .line 130
    iput-object p2, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    .line 131
    iput-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 132
    return-void

    :cond_0
    move v0, v2

    .line 127
    goto :goto_0

    :cond_1
    move v1, v2

    .line 129
    goto :goto_1
.end method

.method private sizeOf(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)I
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p2, "val"    # Ljava/lang/Object;

    .prologue
    .line 1170
    instance-of v4, p2, Ljava/util/Collection;

    if-eqz v4, :cond_0

    .line 1171
    check-cast p2, Ljava/util/Collection;

    .end local p2    # "val":Ljava/lang/Object;
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v4

    .line 1190
    :goto_0
    return v4

    .line 1172
    .restart local p2    # "val":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1173
    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v4

    goto :goto_0

    .line 1174
    :cond_1
    instance-of v4, p2, Ljava/util/Map;

    if-eqz v4, :cond_2

    .line 1175
    check-cast p2, Ljava/util/Map;

    .end local p2    # "val":Ljava/lang/Object;
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v4

    goto :goto_0

    .line 1176
    .restart local p2    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v4, p2, Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 1177
    check-cast p2, Ljava/lang/String;

    .end local p2    # "val":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_0

    .line 1181
    .restart local p2    # "val":Ljava/lang/Object;
    :cond_3
    const/4 v4, 0x0

    new-array v1, v4, [Ljava/lang/Object;

    .line 1182
    .local v1, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    const-string/jumbo v5, "size"

    sget-object v6, Lorg/apache/commons/jexl2/Interpreter;->EMPTY_PARAMS:[Ljava/lang/Object;

    invoke-interface {v4, p2, v5, v6, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v3

    .line 1183
    .local v3, "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-eqz v3, :cond_4

    invoke-interface {v3}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->getReturnType()Ljava/lang/Class;

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v4, v5, :cond_4

    .line 1186
    :try_start_0
    invoke-interface {v3, p2, v1}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1190
    .local v2, "result":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_0

    .line 1187
    .end local v2    # "result":Ljava/lang/Integer;
    :catch_0
    move-exception v0

    .line 1188
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v5, "size() : error executing"

    invoke-direct {v4, p1, v5, v0}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 1192
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "size() : unsupported type : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, p1, v5, v6}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method


# virtual methods
.method protected findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;
    .locals 2
    .param p1, "xrt"    # Ljava/lang/RuntimeException;
    .param p2, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;
    .param p3, "left"    # Ljava/lang/Object;
    .param p4, "right"    # Ljava/lang/Object;

    .prologue
    .line 197
    instance-of v0, p1, Ljava/lang/NullPointerException;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "jexl.null"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 199
    if-nez p3, :cond_1

    .line 200
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object p2

    .line 206
    .end local p2    # "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_0
    :goto_0
    return-object p2

    .line 202
    .restart local p2    # "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_1
    if-nez p4, :cond_0

    .line 203
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object p2

    goto :goto_0
.end method

.method protected getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;
    .locals 7
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "attribute"    # Ljava/lang/Object;
    .param p3, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    .line 1218
    if-nez p1, :cond_0

    .line 1219
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v6, "object is null"

    invoke-direct {v5, p3, v6}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v5

    .line 1222
    :cond_0
    if-eqz p3, :cond_2

    iget-boolean v5, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v5, :cond_2

    .line 1223
    invoke-virtual {p3}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetValue()Ljava/lang/Object;

    move-result-object v0

    .line 1224
    .local v0, "cached":Ljava/lang/Object;
    instance-of v5, v0, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    if-eqz v5, :cond_2

    move-object v2, v0

    .line 1225
    check-cast v2, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    .line 1226
    .local v2, "vg":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    invoke-interface {v2, p1, p2}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->tryInvoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1227
    .local v1, "value":Ljava/lang/Object;
    invoke-interface {v2, v1}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->tryFailed(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1256
    .end local v0    # "cached":Ljava/lang/Object;
    .end local v1    # "value":Ljava/lang/Object;
    :cond_1
    :goto_0
    return-object v1

    .line 1232
    .end local v2    # "vg":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    :cond_2
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v5, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;

    move-result-object v2

    .line 1233
    .restart local v2    # "vg":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    if-eqz v2, :cond_5

    .line 1235
    :try_start_0
    invoke-interface {v2, p1}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1237
    .restart local v1    # "value":Ljava/lang/Object;
    if-eqz p3, :cond_1

    iget-boolean v5, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v5, :cond_1

    invoke-interface {v2}, Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;->isCacheable()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1238
    invoke-virtual {p3, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtSetValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1241
    .end local v1    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v3

    .line 1242
    .local v3, "xany":Ljava/lang/Exception;
    if-nez p3, :cond_3

    .line 1243
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 1245
    :cond_3
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v5, "get object property error"

    invoke-direct {v4, p3, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1246
    .local v4, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    iget-boolean v5, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-eqz v5, :cond_4

    .line 1247
    throw v4

    .line 1249
    :cond_4
    iget-boolean v5, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-nez v5, :cond_5

    .line 1250
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v4}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    .line 1256
    .end local v3    # "xany":Ljava/lang/Exception;
    .end local v4    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getUberspect()Lorg/apache/commons/jexl2/introspection/Uberspect;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    return-object v0
.end method

.method public interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    const/4 v1, 0x0

    .line 162
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, p0, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 166
    :goto_0
    return-object v1

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    iget-boolean v2, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-eqz v2, :cond_0

    .line 165
    iget-object v2, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 168
    :cond_0
    throw v0
.end method

.method protected invocationFailed(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;
    .locals 3
    .param p1, "xjexl"    # Lorg/apache/commons/jexl2/JexlException;

    .prologue
    .line 230
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-eqz v0, :cond_0

    .line 231
    throw p1

    .line 233
    :cond_0
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-nez v0, :cond_1

    .line 234
    iget-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 236
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected resolveNamespace(Ljava/lang/String;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;
    .locals 8
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    .line 250
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    if-eqz v5, :cond_0

    .line 251
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 252
    .local v2, "namespace":Ljava/lang/Object;
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 276
    .end local v2    # "namespace":Ljava/lang/Object;
    .local v3, "namespace":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 256
    .end local v3    # "namespace":Ljava/lang/Object;
    :cond_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->functions:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 257
    .restart local v2    # "namespace":Ljava/lang/Object;
    if-nez v2, :cond_1

    .line 258
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "no such function namespace "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v5

    .line 261
    :cond_1
    instance-of v5, v2, Ljava/lang/Class;

    if-eqz v5, :cond_3

    .line 262
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    aput-object v6, v0, v5

    .line 263
    .local v0, "args":[Ljava/lang/Object;
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v5, v2, v0, p2}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getConstructor(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 264
    .local v1, "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    if-eqz v1, :cond_3

    .line 266
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 267
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    if-nez v5, :cond_2

    .line 268
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    .line 270
    :cond_2
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->functors:Ljava/util/Map;

    invoke-interface {v5, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "args":[Ljava/lang/Object;
    .end local v1    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :cond_3
    move-object v3, v2

    .line 276
    .end local v2    # "namespace":Ljava/lang/Object;
    .restart local v3    # "namespace":Ljava/lang/Object;
    goto :goto_0

    .line 271
    .end local v3    # "namespace":Ljava/lang/Object;
    .restart local v0    # "args":[Ljava/lang/Object;
    .restart local v1    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    .restart local v2    # "namespace":Ljava/lang/Object;
    :catch_0
    move-exception v4

    .line 272
    .local v4, "xinst":Ljava/lang/Exception;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "unable to instantiate namespace "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p2, v6, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method protected setAttribute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)V
    .locals 10
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "attribute"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;
    .param p4, "node"    # Lorg/apache/commons/jexl2/parser/JexlNode;

    .prologue
    .line 1282
    if-eqz p4, :cond_1

    iget-boolean v8, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v8, :cond_1

    .line 1283
    invoke-virtual {p4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetValue()Ljava/lang/Object;

    move-result-object v0

    .line 1284
    .local v0, "cached":Ljava/lang/Object;
    instance-of v8, v0, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;

    if-eqz v8, :cond_1

    move-object v3, v0

    .line 1285
    check-cast v3, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;

    .line 1286
    .local v3, "setter":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    invoke-interface {v3, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;->tryInvoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1287
    .local v2, "eval":Ljava/lang/Object;
    invoke-interface {v3, v2}, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;->tryFailed(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1329
    .end local v0    # "cached":Ljava/lang/Object;
    .end local v2    # "eval":Ljava/lang/Object;
    .end local v3    # "setter":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    :cond_0
    :goto_0
    return-void

    .line 1292
    :cond_1
    const/4 v6, 0x0

    .line 1293
    .local v6, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v8, p1, p2, p3, p4}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getPropertySet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertySet;

    move-result-object v4

    .line 1294
    .local v4, "vs":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    if-eqz v4, :cond_3

    .line 1297
    :try_start_0
    invoke-interface {v4, p1, p3}, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1298
    if-eqz p4, :cond_0

    iget-boolean v8, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v8, :cond_0

    invoke-interface {v4}, Lorg/apache/commons/jexl2/introspection/JexlPropertySet;->isCacheable()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1299
    invoke-virtual {p4, v4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtSetValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1302
    :catch_0
    move-exception v7

    .line 1303
    .local v7, "xrt":Ljava/lang/RuntimeException;
    if-nez p4, :cond_2

    .line 1304
    throw v7

    .line 1306
    :cond_2
    new-instance v6, Lorg/apache/commons/jexl2/JexlException;

    .end local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v8, "set object property error"

    invoke-direct {v6, p4, v8, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1314
    .end local v7    # "xrt":Ljava/lang/RuntimeException;
    .restart local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_3
    :goto_1
    if-nez v6, :cond_6

    .line 1315
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "unable to set object property, class: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", property: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1318
    .local v1, "error":Ljava/lang/String;
    if-nez p4, :cond_5

    .line 1319
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v8, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1307
    .end local v1    # "error":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 1308
    .local v5, "xany":Ljava/lang/Exception;
    if-nez p4, :cond_4

    .line 1309
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 1311
    :cond_4
    new-instance v6, Lorg/apache/commons/jexl2/JexlException;

    .end local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v8, "set object property error"

    invoke-direct {v6, p4, v8, v5}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .restart local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_1

    .line 1321
    .end local v5    # "xany":Ljava/lang/Exception;
    .restart local v1    # "error":Ljava/lang/String;
    :cond_5
    new-instance v6, Lorg/apache/commons/jexl2/JexlException;

    .end local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const/4 v8, 0x0

    invoke-direct {v6, p4, v1, v8}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1323
    .end local v1    # "error":Ljava/lang/String;
    .restart local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_6
    iget-boolean v8, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-eqz v8, :cond_7

    .line 1324
    throw v6

    .line 1326
    :cond_7
    iget-boolean v8, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-nez v8, :cond_0

    .line 1327
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {v6}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method protected unknownVariable(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;
    .locals 2
    .param p1, "xjexl"    # Lorg/apache/commons/jexl2/JexlException;

    .prologue
    .line 215
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-eqz v0, :cond_0

    .line 216
    throw p1

    .line 218
    :cond_0
    iget-boolean v0, p0, Lorg/apache/commons/jexl2/Interpreter;->silent:Z

    if-nez v0, :cond_1

    .line 219
    iget-object v0, p0, Lorg/apache/commons/jexl2/Interpreter;->logger:Lorg/apache/commons/logging/Log;

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/JexlException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    .line 221
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 288
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 289
    .local v1, "left":Ljava/lang/Object;
    const/4 v0, 0x2

    .local v0, "c":I
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetNumChildren()I

    move-result v4

    .local v4, "size":I
    :goto_0
    if-ge v0, v4, :cond_3

    .line 290
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 292
    .local v3, "right":Ljava/lang/Object;
    add-int/lit8 v8, v0, -0x1

    :try_start_0
    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTAdditiveNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 293
    .local v2, "op":Lorg/apache/commons/jexl2/parser/JexlNode;
    instance-of v8, v2, Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;

    if-eqz v8, :cond_2

    .line 294
    check-cast v2, Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;

    .end local v2    # "op":Lorg/apache/commons/jexl2/parser/JexlNode;
    iget-object v5, v2, Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;->image:Ljava/lang/String;

    .line 295
    .local v5, "which":Ljava/lang/String;
    const-string/jumbo v8, "+"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 296
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v1, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 289
    :goto_1
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 299
    :cond_0
    const-string/jumbo v8, "-"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 300
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v1, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;->subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_1

    .line 303
    :cond_1
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "unknown operator "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    .end local v5    # "which":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 307
    .local v7, "xrt":Ljava/lang/RuntimeException;
    invoke-virtual {p0, v7, p1, v1, v3}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    .line 308
    .local v6, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v9, "+/- error"

    invoke-direct {v8, v6, v9, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 305
    .end local v6    # "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v7    # "xrt":Ljava/lang/RuntimeException;
    .restart local v2    # "op":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_2
    :try_start_1
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "unknown operator "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 311
    .end local v2    # "op":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v3    # "right":Ljava/lang/Object;
    :cond_3
    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAdditiveOperator;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 316
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Shoud not be called."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAmbiguous;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAmbiguous;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1348
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "unexpected type of node"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAndNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAndNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 321
    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 323
    .local v0, "left":Ljava/lang/Object;
    :try_start_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    .line 324
    .local v1, "leftValue":Z
    if-nez v1, :cond_0

    .line 325
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_0
    return-object v5

    .line 327
    .end local v1    # "leftValue":Z
    :catch_0
    move-exception v4

    .line 328
    .local v4, "xrt":Ljava/lang/RuntimeException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string/jumbo v7, "boolean coercion error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 330
    .end local v4    # "xrt":Ljava/lang/RuntimeException;
    .restart local v1    # "leftValue":Z
    :cond_0
    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 332
    .local v2, "right":Ljava/lang/Object;
    :try_start_1
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v3

    .line 333
    .local v3, "rightValue":Z
    if-nez v3, :cond_1

    .line 334
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 336
    .end local v3    # "rightValue":Z
    :catch_1
    move-exception v4

    .line 337
    .restart local v4    # "xrt":Ljava/lang/RuntimeException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string/jumbo v7, "boolean coercion error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 339
    .end local v4    # "xrt":Ljava/lang/RuntimeException;
    .restart local v3    # "rightValue":Z
    :cond_1
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTArrayAccess;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 345
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 348
    .local v4, "object":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetNumChildren()I

    move-result v3

    .line 349
    .local v3, "numChildren":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 350
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 351
    .local v2, "nindex":Lorg/apache/commons/jexl2/parser/JexlNode;
    instance-of v5, v2, Lorg/apache/commons/jexl2/parser/JexlNode$Literal;

    if-eqz v5, :cond_0

    .line 352
    invoke-virtual {v2, p0, v4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 349
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 354
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v2, p0, v5}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 355
    .local v1, "index":Ljava/lang/Object;
    invoke-virtual {p0, v4, v1, v2}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_1

    .line 359
    .end local v1    # "index":Ljava/lang/Object;
    .end local v2    # "nindex":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_1
    return-object v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 364
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->getLiteral()Ljava/lang/Object;

    move-result-object v4

    .line 365
    .local v4, "literal":Ljava/lang/Object;
    if-nez v4, :cond_1

    .line 366
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetNumChildren()I

    move-result v1

    .line 367
    .local v1, "childCount":I
    new-array v0, v1, [Ljava/lang/Object;

    .line 368
    .local v0, "array":[Ljava/lang/Object;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 369
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 370
    .local v2, "entry":Ljava/lang/Object;
    aput-object v2, v0, v3

    .line 368
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 372
    .end local v2    # "entry":Ljava/lang/Object;
    :cond_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArrayType([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 373
    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->setLiteral(Ljava/lang/Object;)V

    .line 375
    .end local v0    # "array":[Ljava/lang/Object;
    .end local v1    # "childCount":I
    .end local v3    # "i":I
    :cond_1
    return-object v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTAssignment;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 21
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTAssignment;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 381
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/parser/ASTAssignment;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    .line 382
    .local v9, "left":Lorg/apache/commons/jexl2/parser/JexlNode;
    instance-of v0, v9, Lorg/apache/commons/jexl2/parser/ASTReference;

    move/from16 v19, v0

    if-nez v19, :cond_0

    .line 383
    new-instance v19, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v20, "illegal assignment form"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v9, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v19

    .line 386
    :cond_0
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/parser/ASTAssignment;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    .line 389
    .local v16, "right":Ljava/lang/Object;
    const/4 v13, 0x0

    .line 390
    .local v13, "objectNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    const/4 v12, 0x0

    .line 391
    .local v12, "object":Ljava/lang/Object;
    const/4 v15, 0x0

    .line 392
    .local v15, "propertyNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    const/4 v14, 0x0

    .line 393
    .local v14, "property":Ljava/lang/Object;
    const/4 v7, 0x1

    .line 394
    .local v7, "isVariable":Z
    const/16 v17, 0x0

    .line 395
    .local v17, "v":I
    const/16 v18, 0x0

    .line 397
    .local v18, "variableName":Ljava/lang/StringBuilder;
    invoke-virtual {v9}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetNumChildren()I

    move-result v19

    add-int/lit8 v8, v19, -0x1

    .line 398
    .local v8, "last":I
    const/4 v4, 0x0

    .end local v12    # "object":Ljava/lang/Object;
    .local v4, "c":I
    :goto_0
    if-ge v4, v8, :cond_6

    .line 399
    invoke-virtual {v9, v4}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v13

    .line 401
    move-object/from16 v0, p0

    invoke-virtual {v13, v0, v12}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .line 402
    .restart local v12    # "object":Ljava/lang/Object;
    if-eqz v12, :cond_2

    .line 398
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 405
    :cond_2
    instance-of v0, v13, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    move/from16 v19, v0

    and-int v7, v7, v19

    .line 407
    if-eqz v7, :cond_5

    .line 408
    if-nez v17, :cond_3

    .line 409
    new-instance v18, Ljava/lang/StringBuilder;

    .end local v18    # "variableName":Ljava/lang/StringBuilder;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 410
    .restart local v18    # "variableName":Ljava/lang/StringBuilder;
    const/16 v17, 0x1

    .line 412
    :cond_3
    :goto_2
    move/from16 v0, v17

    if-gt v0, v4, :cond_4

    .line 413
    const/16 v19, 0x2e

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 414
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 416
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    .line 418
    if-eqz v12, :cond_1

    .line 419
    const/4 v7, 0x0

    goto :goto_1

    .line 422
    :cond_5
    new-instance v19, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v20, "illegal assignment form"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v13, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v19

    .line 426
    .end local v12    # "object":Ljava/lang/Object;
    :cond_6
    invoke-virtual {v9, v8}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v15

    .line 427
    const/4 v3, 0x0

    .line 428
    .local v3, "antVar":Z
    instance-of v0, v15, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    move/from16 v19, v0

    if-eqz v19, :cond_9

    move-object/from16 v19, v15

    .line 429
    check-cast v19, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    move-object/from16 v0, v19

    iget-object v14, v0, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    .line 430
    .local v14, "property":Ljava/lang/String;
    const/4 v3, 0x1

    .line 461
    .end local v14    # "property":Ljava/lang/String;
    :goto_3
    if-eqz v3, :cond_f

    .line 462
    if-eqz v7, :cond_f

    if-nez v12, :cond_f

    .line 463
    if-eqz v18, :cond_8

    .line 464
    if-lez v8, :cond_7

    .line 465
    const/16 v19, 0x2e

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 467
    :cond_7
    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 468
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 470
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    move-object/from16 v19, v0

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v16

    invoke-interface {v0, v1, v2}, Lorg/apache/commons/jexl2/JexlContext;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 484
    :goto_4
    return-object v16

    .line 431
    .local v14, "property":Ljava/lang/Object;
    :cond_9
    instance-of v0, v15, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;

    move/from16 v19, v0

    if-eqz v19, :cond_a

    move-object/from16 v19, v15

    .line 432
    check-cast v19, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;

    invoke-virtual/range {v19 .. v19}, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;->getLiteral()Ljava/lang/Integer;

    move-result-object v14

    .line 433
    .local v14, "property":Ljava/lang/Integer;
    const/4 v3, 0x1

    goto :goto_3

    .line 434
    .local v14, "property":Ljava/lang/Object;
    :cond_a
    instance-of v0, v15, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;

    move/from16 v19, v0

    if-eqz v19, :cond_e

    .line 436
    move-object v13, v15

    move-object v10, v13

    .line 437
    check-cast v10, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;

    .line 438
    .local v10, "narray":Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v12}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .line 439
    .local v11, "nobject":Ljava/lang/Object;
    if-nez v11, :cond_b

    .line 440
    new-instance v19, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v20, "array element is null"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v13, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v19

    .line 442
    :cond_b
    move-object v12, v11

    .line 446
    .restart local v12    # "object":Ljava/lang/Object;
    invoke-virtual {v10}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetNumChildren()I

    move-result v19

    add-int/lit8 v8, v19, -0x1

    .line 447
    const/4 v5, 0x1

    .local v5, "i":I
    :goto_5
    if-ge v5, v8, :cond_d

    .line 448
    invoke-virtual {v10, v5}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v13

    .line 449
    instance-of v0, v13, Lorg/apache/commons/jexl2/parser/JexlNode$Literal;

    move/from16 v19, v0

    if-eqz v19, :cond_c

    .line 450
    move-object/from16 v0, p0

    invoke-virtual {v13, v0, v12}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .line 447
    :goto_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 452
    :cond_c
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 453
    .local v6, "index":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v6, v13}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v12

    goto :goto_6

    .line 456
    .end local v6    # "index":Ljava/lang/Object;
    :cond_d
    invoke-virtual {v10, v8}, Lorg/apache/commons/jexl2/parser/ASTArrayAccess;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .line 457
    goto/16 :goto_3

    .line 458
    .end local v5    # "i":I
    .end local v10    # "narray":Lorg/apache/commons/jexl2/parser/ASTArrayAccess;
    .end local v11    # "nobject":Ljava/lang/Object;
    .end local v12    # "object":Ljava/lang/Object;
    :cond_e
    new-instance v19, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v20, "illegal assignment form"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v13, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v19

    .line 474
    .end local v14    # "property":Ljava/lang/Object;
    :cond_f
    if-nez v14, :cond_10

    .line 476
    new-instance v19, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v20, "property is null"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v19

    .line 478
    :cond_10
    if-nez v12, :cond_11

    .line 480
    new-instance v19, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v20, "bean is null"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v13, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v19

    .line 483
    :cond_11
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v12, v14, v1, v15}, Lorg/apache/commons/jexl2/Interpreter;->setAttribute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)V

    goto/16 :goto_4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 489
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 490
    .local v2, "left":Ljava/lang/Object;
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 491
    .local v6, "right":Ljava/lang/Object;
    const/4 v3, 0x0

    .line 494
    .local v3, "n":I
    :try_start_0
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 495
    .local v0, "l":J
    const/4 v3, 0x1

    .line 496
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v6}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v4

    .line 497
    .local v4, "r":J
    and-long v8, v0, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    return-object v8

    .line 498
    .end local v0    # "l":J
    .end local v4    # "r":J
    :catch_0
    move-exception v7

    .line 499
    .local v7, "xrt":Ljava/lang/RuntimeException;
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseAndNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    const-string/jumbo v10, "long coercion error"

    invoke-direct {v8, v9, v10, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 505
    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 507
    .local v2, "left":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 508
    .local v0, "l":J
    const-wide/16 v4, -0x1

    xor-long/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 509
    .end local v0    # "l":J
    :catch_0
    move-exception v3

    .line 510
    .local v3, "xrt":Ljava/lang/RuntimeException;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTBitwiseComplNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    const-string/jumbo v6, "long coercion error"

    invoke-direct {v4, v5, v6, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 516
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 517
    .local v2, "left":Ljava/lang/Object;
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 518
    .local v6, "right":Ljava/lang/Object;
    const/4 v3, 0x0

    .line 521
    .local v3, "n":I
    :try_start_0
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 522
    .local v0, "l":J
    const/4 v3, 0x1

    .line 523
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v6}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v4

    .line 524
    .local v4, "r":J
    or-long v8, v0, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    return-object v8

    .line 525
    .end local v0    # "l":J
    .end local v4    # "r":J
    :catch_0
    move-exception v7

    .line 526
    .local v7, "xrt":Ljava/lang/RuntimeException;
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    const-string/jumbo v10, "long coercion error"

    invoke-direct {v8, v9, v10, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 532
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 533
    .local v2, "left":Ljava/lang/Object;
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 534
    .local v6, "right":Ljava/lang/Object;
    const/4 v3, 0x0

    .line 537
    .local v3, "n":I
    :try_start_0
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v0

    .line 538
    .local v0, "l":J
    const/4 v3, 0x1

    .line 539
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v6}, Lorg/apache/commons/jexl2/JexlArithmetic;->toLong(Ljava/lang/Object;)J

    move-result-wide v4

    .line 540
    .local v4, "r":J
    xor-long v8, v0, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    return-object v8

    .line 541
    .end local v0    # "l":J
    .end local v4    # "r":J
    :catch_0
    move-exception v7

    .line 542
    .local v7, "xrt":Ljava/lang/RuntimeException;
    new-instance v8, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTBitwiseXorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v9

    const-string/jumbo v10, "long coercion error"

    invoke-direct {v8, v9, v10, v7}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTBlock;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTBlock;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 548
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTBlock;->jjtGetNumChildren()I

    move-result v1

    .line 549
    .local v1, "numChildren":I
    const/4 v2, 0x0

    .line 550
    .local v2, "result":Ljava/lang/Object;
    const/4 v0, 0x0

    .end local v2    # "result":Ljava/lang/Object;
    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 551
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTBlock;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 550
    .restart local v2    # "result":Ljava/lang/Object;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 553
    .end local v2    # "result":Ljava/lang/Object;
    :cond_0
    return-object v2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTConstructorNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTConstructorNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x0

    .line 853
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 855
    .local v2, "cobject":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetNumChildren()I

    move-result v8

    add-int/lit8 v0, v8, -0x1

    .line 856
    .local v0, "argc":I
    new-array v1, v0, [Ljava/lang/Object;

    .line 857
    .local v1, "argv":[Ljava/lang/Object;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v0, :cond_0

    .line 858
    add-int/lit8 v8, v5, 0x1

    invoke-virtual {p1, v8}, Lorg/apache/commons/jexl2/parser/ASTConstructorNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    invoke-virtual {v8, p0, v9}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v1, v5

    .line 857
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 861
    :cond_0
    const/4 v6, 0x0

    .line 863
    .local v6, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_0
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v8, v2, v1, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getConstructor(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    .line 866
    .local v3, "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    if-nez v3, :cond_2

    .line 867
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v8, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 868
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    invoke-interface {v8, v2, v1, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getConstructor(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    .line 870
    :cond_1
    if-nez v3, :cond_2

    .line 871
    new-instance v7, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v8, "unknown constructor"

    const/4 v9, 0x0

    invoke-direct {v7, p1, v8, v9}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .local v7, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object v6, v7

    .line 874
    .end local v7    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_2
    if-nez v6, :cond_3

    .line 875
    invoke-virtual {v3, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 882
    .end local v3    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :goto_1
    return-object v8

    .line 877
    :catch_0
    move-exception v4

    .line 878
    .local v4, "e":Ljava/lang/reflect/InvocationTargetException;
    new-instance v6, Lorg/apache/commons/jexl2/JexlException;

    .end local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v8, "constructor invocation error"

    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-direct {v6, p1, v8, v9}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 882
    .end local v4    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_3
    :goto_2
    invoke-virtual {p0, v6}, Lorg/apache/commons/jexl2/Interpreter;->invocationFailed(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v8

    goto :goto_1

    .line 879
    :catch_1
    move-exception v4

    .line 880
    .local v4, "e":Ljava/lang/Exception;
    new-instance v6, Lorg/apache/commons/jexl2/JexlException;

    .end local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v8, "constructor error"

    invoke-direct {v6, p1, v8, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .restart local v6    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTDivNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTDivNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 558
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTDivNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 559
    .local v0, "left":Ljava/lang/Object;
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTDivNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 561
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->divide(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 564
    :goto_0
    return-object v4

    .line 562
    :catch_0
    move-exception v3

    .line 563
    .local v3, "xrt":Ljava/lang/RuntimeException;
    iget-boolean v4, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-nez v4, :cond_0

    instance-of v4, v3, Ljava/lang/ArithmeticException;

    if-eqz v4, :cond_0

    .line 564
    new-instance v4, Ljava/lang/Double;

    const-wide/16 v5, 0x0

    invoke-direct {v4, v5, v6}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 566
    :cond_0
    invoke-virtual {p0, v3, p1, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 567
    .local v2, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v5, "divide error"

    invoke-direct {v4, v2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTEQNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTEQNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 595
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTEQNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 596
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTEQNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 598
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 599
    :catch_0
    move-exception v2

    .line 600
    .local v2, "xrt":Ljava/lang/RuntimeException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v4, "== error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTERNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTERNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 669
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTERNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 670
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTERNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 672
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->matches(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 673
    :catch_0
    move-exception v2

    .line 674
    .local v2, "xrt":Ljava/lang/RuntimeException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v4, "=~ error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 573
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTEmptyFunction;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 574
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 575
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 590
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 577
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string/jumbo v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 578
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 580
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    array-length v1, v1

    if-nez v1, :cond_2

    .line 581
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 583
    :cond_2
    instance-of v1, v0, Ljava/util/Collection;

    if-eqz v1, :cond_4

    .line 584
    check-cast v0, Ljava/util/Collection;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_3
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 587
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_4
    instance-of v1, v0, Ljava/util/Map;

    if-eqz v1, :cond_6

    .line 588
    check-cast v0, Ljava/util/Map;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_5
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 590
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_6
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFalseNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFalseNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 606
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 611
    if-eqz p2, :cond_0

    .line 612
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;->getLiteral()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {p0, p2, v0, p1}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v0

    .line 614
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTFloatLiteral;->getLiteral()Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTForeachStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTForeachStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    .line 619
    const/4 v4, 0x0

    .line 621
    .local v4, "result":Ljava/lang/Object;
    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    check-cast v2, Lorg/apache/commons/jexl2/parser/ASTReference;

    .line 622
    .local v2, "loopReference":Lorg/apache/commons/jexl2/parser/ASTReference;
    invoke-virtual {v2, v7}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    check-cast v3, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    .line 624
    .local v3, "loopVariable":Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v7

    invoke-virtual {v7, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 626
    .local v1, "iterableValue":Ljava/lang/Object;
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetNumChildren()I

    move-result v7

    const/4 v8, 0x3

    if-lt v7, v8, :cond_0

    .line 628
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTForeachStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    .line 631
    .local v5, "statement":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/Interpreter;->getUberspect()Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-result-object v7

    invoke-interface {v7, v1, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getIterator(Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/util/Iterator;

    move-result-object v0

    .line 632
    .local v0, "itemsIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    if-eqz v0, :cond_0

    .line 633
    .end local v4    # "result":Ljava/lang/Object;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 635
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 636
    .local v6, "value":Ljava/lang/Object;
    iget-object v7, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    iget-object v8, v3, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    invoke-interface {v7, v8, v6}, Lorg/apache/commons/jexl2/JexlContext;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 638
    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 639
    .restart local v4    # "result":Ljava/lang/Object;
    goto :goto_0

    .line 642
    .end local v0    # "itemsIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    .end local v4    # "result":Ljava/lang/Object;
    .end local v5    # "statement":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v6    # "value":Ljava/lang/Object;
    :cond_0
    return-object v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTFunctionNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 17
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTFunctionNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 888
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v15

    check-cast v15, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    iget-object v11, v15, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    .line 889
    .local v11, "prefix":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v11, v1}, Lorg/apache/commons/jexl2/Interpreter;->resolveNamespace(Ljava/lang/String;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v10

    .line 891
    .local v10, "namespace":Ljava/lang/Object;
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v15

    check-cast v15, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    iget-object v7, v15, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    .line 894
    .local v7, "function":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetNumChildren()I

    move-result v15

    add-int/lit8 v2, v15, -0x2

    .line 895
    .local v2, "argc":I
    new-array v3, v2, [Ljava/lang/Object;

    .line 896
    .local v3, "argv":[Ljava/lang/Object;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v2, :cond_0

    .line 897
    add-int/lit8 v15, v8, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v15, v0, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    aput-object v15, v3, v8

    .line 896
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 900
    :cond_0
    const/4 v13, 0x0

    .line 903
    .local v13, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v15, :cond_2

    .line 904
    invoke-virtual/range {p1 .. p1}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtGetValue()Ljava/lang/Object;

    move-result-object v4

    .line 905
    .local v4, "cached":Ljava/lang/Object;
    instance-of v15, v4, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    if-eqz v15, :cond_2

    .line 906
    move-object v0, v4

    check-cast v0, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-object v9, v0

    .line 907
    .local v9, "me":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    invoke-interface {v9, v7, v10, v3}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->tryInvoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 908
    .local v6, "eval":Ljava/lang/Object;
    invoke-interface {v9, v6}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->tryFailed(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 938
    .end local v4    # "cached":Ljava/lang/Object;
    .end local v6    # "eval":Ljava/lang/Object;
    .end local v9    # "me":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :cond_1
    :goto_1
    return-object v6

    .line 913
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v0, p1

    invoke-interface {v15, v10, v7, v3, v0}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v12

    .line 916
    .local v12, "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-nez v12, :cond_4

    .line 918
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v15, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 919
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v0, p1

    invoke-interface {v15, v10, v7, v3, v0}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v12

    .line 921
    :cond_3
    if-nez v12, :cond_4

    .line 922
    new-instance v14, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v15, "unknown function"

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v14, v0, v15, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v13    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .local v14, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object v13, v14

    .line 925
    .end local v14    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v13    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_4
    if-nez v13, :cond_5

    .line 926
    invoke-interface {v12, v10, v3}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 928
    .restart local v6    # "eval":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v15, :cond_1

    invoke-interface {v12}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->isCacheable()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 929
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/commons/jexl2/parser/ASTFunctionNode;->jjtSetValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 933
    .end local v6    # "eval":Ljava/lang/Object;
    .end local v12    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :catch_0
    move-exception v5

    .line 934
    .local v5, "e":Ljava/lang/reflect/InvocationTargetException;
    new-instance v13, Lorg/apache/commons/jexl2/JexlException;

    .end local v13    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v15, "function invocation error"

    invoke-virtual {v5}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v13, v0, v15, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 938
    .end local v5    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v13    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/apache/commons/jexl2/Interpreter;->invocationFailed(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v6

    goto :goto_1

    .line 935
    :catch_1
    move-exception v5

    .line 936
    .local v5, "e":Ljava/lang/Exception;
    new-instance v13, Lorg/apache/commons/jexl2/JexlException;

    .end local v13    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v15, "function error"

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v15, v5}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .restart local v13    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTGENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTGENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 647
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTGENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 648
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTGENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 650
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->greaterThanOrEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 651
    :catch_0
    move-exception v2

    .line 652
    .local v2, "xrt":Ljava/lang/RuntimeException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v4, ">= error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTGTNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTGTNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 658
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTGTNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 659
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTGTNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 661
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->greaterThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 662
    :catch_0
    move-exception v2

    .line 663
    .local v2, "xrt":Ljava/lang/RuntimeException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v4, "> error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIdentifier;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIdentifier;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 680
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    .line 681
    .local v0, "name":Ljava/lang/String;
    if-nez p2, :cond_2

    .line 682
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    if-eqz v3, :cond_1

    .line 683
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->registers:[Ljava/lang/Object;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v4, v4, -0x30

    aget-object v1, v3, v4

    .line 694
    :cond_0
    :goto_0
    return-object v1

    .line 685
    :cond_1
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v3, v0}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 686
    .local v1, "value":Ljava/lang/Object;
    if-nez v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    instance-of v3, v3, Lorg/apache/commons/jexl2/parser/ASTReference;

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    invoke-interface {v3, v0}, Lorg/apache/commons/jexl2/JexlContext;->has(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 689
    new-instance v2, Lorg/apache/commons/jexl2/JexlException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "undefined variable "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    .line 690
    .local v2, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    invoke-virtual {p0, v2}, Lorg/apache/commons/jexl2/Interpreter;->unknownVariable(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 694
    .end local v1    # "value":Ljava/lang/Object;
    .end local v2    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_2
    invoke-virtual {p0, p2, v0, p1}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIfStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIfStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 700
    const/4 v2, 0x0

    .line 702
    .local v2, "n":I
    const/4 v3, 0x0

    .line 704
    .local v3, "result":Ljava/lang/Object;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 705
    .local v1, "expression":Ljava/lang/Object;
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 707
    const/4 v2, 0x1

    .line 708
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 717
    .end local v3    # "result":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v3

    .line 712
    .restart local v3    # "result":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetNumChildren()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 713
    const/4 v2, 0x2

    .line 714
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    goto :goto_0

    .line 718
    .end local v1    # "expression":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 719
    .local v0, "error":Lorg/apache/commons/jexl2/JexlException;
    throw v0

    .line 720
    .end local v0    # "error":Lorg/apache/commons/jexl2/JexlException;
    :catch_1
    move-exception v4

    .line 721
    .local v4, "xrt":Ljava/lang/RuntimeException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTIfStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string/jumbo v7, "if error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 727
    if-eqz p2, :cond_0

    .line 728
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;->getLiteral()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p2, v0, p1}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v0

    .line 730
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;->getLiteral()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTJexlScript;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 735
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetNumChildren()I

    move-result v2

    .line 736
    .local v2, "numChildren":I
    const/4 v3, 0x0

    .line 737
    .local v3, "result":Ljava/lang/Object;
    const/4 v1, 0x0

    .end local v3    # "result":Ljava/lang/Object;
    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 738
    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 739
    .local v0, "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    invoke-virtual {v0, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 737
    .restart local v3    # "result":Ljava/lang/Object;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 741
    .end local v0    # "child":Lorg/apache/commons/jexl2/parser/JexlNode;
    .end local v3    # "result":Ljava/lang/Object;
    :cond_0
    return-object v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTLENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTLENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 746
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTLENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 747
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTLENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 749
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->lessThanOrEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 750
    :catch_0
    move-exception v2

    .line 751
    .local v2, "xrt":Ljava/lang/RuntimeException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v4, "<= error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTLTNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTLTNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 757
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTLTNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 758
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTLTNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 760
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 761
    :catch_0
    move-exception v2

    .line 762
    .local v2, "xrt":Ljava/lang/RuntimeException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v4, "< error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMapEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMapEntry;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 768
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTMapEntry;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 769
    .local v0, "key":Ljava/lang/Object;
    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTMapEntry;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 770
    .local v1, "value":Ljava/lang/Object;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    aput-object v1, v2, v4

    return-object v2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMapLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMapLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 775
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetNumChildren()I

    move-result v0

    .line 776
    .local v0, "childCount":I
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 778
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 779
    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTMapLiteral;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v1, v4

    check-cast v1, [Ljava/lang/Object;

    .line 780
    .local v1, "entry":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v4, v1, v4

    const/4 v5, 0x1

    aget-object v5, v1, v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 778
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 783
    .end local v1    # "entry":[Ljava/lang/Object;
    :cond_0
    return-object v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMethodNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMethodNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 789
    if-nez p2, :cond_1

    .line 792
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v12

    if-ne v12, p1, :cond_0

    .line 793
    const/4 v12, 0x0

    invoke-virtual {p0, v12, p1}, Lorg/apache/commons/jexl2/Interpreter;->resolveNamespace(Ljava/lang/String;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object p2

    .line 794
    if-nez p2, :cond_1

    .line 795
    new-instance v12, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v13, "no default function namespace"

    invoke-direct {v12, p1, v13}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v12

    .line 798
    :cond_0
    new-instance v12, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v13, "attempting to call method on null"

    invoke-direct {v12, p1, v13}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v12

    .line 802
    :cond_1
    const/4 v12, 0x0

    invoke-virtual {p1, v12}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v12

    check-cast v12, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    iget-object v8, v12, Lorg/apache/commons/jexl2/parser/ASTIdentifier;->image:Ljava/lang/String;

    .line 805
    .local v8, "methodName":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetNumChildren()I

    move-result v12

    add-int/lit8 v1, v12, -0x1

    .line 806
    .local v1, "argc":I
    new-array v2, v1, [Ljava/lang/Object;

    .line 807
    .local v2, "argv":[Ljava/lang/Object;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v1, :cond_2

    .line 808
    add-int/lit8 v12, v6, 0x1

    invoke-virtual {p1, v12}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, p0, v13}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v2, v6

    .line 807
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 811
    :cond_2
    const/4 v10, 0x0

    .line 814
    .local v10, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :try_start_0
    iget-boolean v12, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v12, :cond_4

    .line 815
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtGetValue()Ljava/lang/Object;

    move-result-object v3

    .line 816
    .local v3, "cached":Ljava/lang/Object;
    instance-of v12, v3, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    if-eqz v12, :cond_4

    .line 817
    move-object v0, v3

    check-cast v0, Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-object v7, v0

    .line 818
    .local v7, "me":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    move-object/from16 v0, p2

    invoke-interface {v7, v8, v0, v2}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->tryInvoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 819
    .local v5, "eval":Ljava/lang/Object;
    invoke-interface {v7, v5}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->tryFailed(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 847
    .end local v3    # "cached":Ljava/lang/Object;
    .end local v5    # "eval":Ljava/lang/Object;
    .end local v7    # "me":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :cond_3
    :goto_1
    return-object v5

    .line 824
    :cond_4
    iget-object v12, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v0, p2

    invoke-interface {v12, v0, v8, v2, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v9

    .line 826
    .local v9, "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    if-nez v9, :cond_6

    .line 827
    iget-object v12, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v12, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->narrowArguments([Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 828
    iget-object v12, p0, Lorg/apache/commons/jexl2/Interpreter;->uberspect:Lorg/apache/commons/jexl2/introspection/Uberspect;

    move-object/from16 v0, p2

    invoke-interface {v12, v0, v8, v2, p1}, Lorg/apache/commons/jexl2/introspection/Uberspect;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;

    move-result-object v9

    .line 830
    :cond_5
    if-nez v9, :cond_6

    .line 831
    new-instance v11, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v12, "unknown or ambiguous method"

    const/4 v13, 0x0

    invoke-direct {v11, p1, v12, v13}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v10    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .local v11, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    move-object v10, v11

    .line 834
    .end local v11    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    .restart local v10    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_6
    if-nez v10, :cond_7

    .line 835
    move-object/from16 v0, p2

    invoke-interface {v9, v0, v2}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 837
    .restart local v5    # "eval":Ljava/lang/Object;
    iget-boolean v12, p0, Lorg/apache/commons/jexl2/Interpreter;->cache:Z

    if-eqz v12, :cond_3

    invoke-interface {v9}, Lorg/apache/commons/jexl2/introspection/JexlMethod;->isCacheable()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 838
    invoke-virtual {p1, v9}, Lorg/apache/commons/jexl2/parser/ASTMethodNode;->jjtSetValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 842
    .end local v5    # "eval":Ljava/lang/Object;
    .end local v9    # "vm":Lorg/apache/commons/jexl2/introspection/JexlMethod;
    :catch_0
    move-exception v4

    .line 843
    .local v4, "e":Ljava/lang/reflect/InvocationTargetException;
    new-instance v10, Lorg/apache/commons/jexl2/JexlException;

    .end local v10    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v12, "method invocation error"

    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v13

    invoke-direct {v10, p1, v12, v13}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 847
    .end local v4    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v10    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :cond_7
    :goto_2
    invoke-virtual {p0, v10}, Lorg/apache/commons/jexl2/Interpreter;->invocationFailed(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v5

    goto :goto_1

    .line 844
    :catch_1
    move-exception v4

    .line 845
    .local v4, "e":Ljava/lang/Exception;
    new-instance v10, Lorg/apache/commons/jexl2/JexlException;

    .end local v10    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    const-string/jumbo v12, "method error"

    invoke-direct {v10, p1, v12, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    .restart local v10    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    goto :goto_2
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTModNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTModNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 943
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTModNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 944
    .local v0, "left":Ljava/lang/Object;
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTModNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 946
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->mod(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 949
    :goto_0
    return-object v4

    .line 947
    :catch_0
    move-exception v3

    .line 948
    .local v3, "xrt":Ljava/lang/RuntimeException;
    iget-boolean v4, p0, Lorg/apache/commons/jexl2/Interpreter;->strict:Z

    if-nez v4, :cond_0

    instance-of v4, v3, Ljava/lang/ArithmeticException;

    if-eqz v4, :cond_0

    .line 949
    new-instance v4, Ljava/lang/Double;

    const-wide/16 v5, 0x0

    invoke-direct {v4, v5, v6}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 951
    :cond_0
    invoke-virtual {p0, v3, p1, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 952
    .local v2, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v5, "% error"

    invoke-direct {v4, v2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTMulNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTMulNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 958
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTMulNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 959
    .local v0, "left":Ljava/lang/Object;
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTMulNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 961
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->multiply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 962
    :catch_0
    move-exception v3

    .line 963
    .local v3, "xrt":Ljava/lang/RuntimeException;
    invoke-virtual {p0, v3, p1, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 964
    .local v2, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v5, "* error"

    invoke-direct {v4, v2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNENode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNENode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 970
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTNENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 971
    .local v0, "left":Ljava/lang/Object;
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/commons/jexl2/parser/ASTNENode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 973
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v4, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_0
    return-object v4

    :cond_0
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 974
    :catch_0
    move-exception v3

    .line 975
    .local v3, "xrt":Ljava/lang/RuntimeException;
    invoke-virtual {p0, v3, p1, v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->findNullOperand(Ljava/lang/RuntimeException;Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    .line 976
    .local v2, "xnode":Lorg/apache/commons/jexl2/parser/JexlNode;
    new-instance v4, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v5, "!= error"

    invoke-direct {v4, v2, v5, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNRNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNRNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 982
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTNRNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 983
    .local v0, "left":Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTNRNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 985
    .local v1, "right":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/JexlArithmetic;->matches(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_0
    return-object v3

    :cond_0
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 986
    :catch_0
    move-exception v2

    .line 987
    .local v2, "xrt":Ljava/lang/RuntimeException;
    new-instance v3, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v4, "!~ error"

    invoke-direct {v3, p1, v4, v2}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNotNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNotNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 993
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTNotNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 994
    .local v0, "val":Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v1, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTNullLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTNullLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 999
    const/4 v0, 0x0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTOrNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTOrNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1004
    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1006
    .local v0, "left":Ljava/lang/Object;
    :try_start_0
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    .line 1007
    .local v1, "leftValue":Z
    if-eqz v1, :cond_0

    .line 1008
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1022
    :goto_0
    return-object v5

    .line 1010
    .end local v1    # "leftValue":Z
    :catch_0
    move-exception v4

    .line 1011
    .local v4, "xrt":Ljava/lang/RuntimeException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v6}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string/jumbo v7, "boolean coercion error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1013
    .end local v4    # "xrt":Ljava/lang/RuntimeException;
    .restart local v1    # "leftValue":Z
    :cond_0
    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v5

    invoke-virtual {v5, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1015
    .local v2, "right":Ljava/lang/Object;
    :try_start_1
    iget-object v5, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v5, v2}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v3

    .line 1016
    .local v3, "rightValue":Z
    if-eqz v3, :cond_1

    .line 1017
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1019
    .end local v3    # "rightValue":Z
    :catch_1
    move-exception v4

    .line 1020
    .restart local v4    # "xrt":Ljava/lang/RuntimeException;
    new-instance v5, Lorg/apache/commons/jexl2/JexlException;

    invoke-virtual {p1, v7}, Lorg/apache/commons/jexl2/parser/ASTOrNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v6

    const-string/jumbo v7, "boolean coercion error"

    invoke-direct {v5, v6, v7, v4}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1022
    .end local v4    # "xrt":Ljava/lang/RuntimeException;
    .restart local v3    # "rightValue":Z
    :cond_1
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTReference;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTReference;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x0

    .line 1031
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetNumChildren()I

    move-result v2

    .line 1034
    .local v2, "numChildren":I
    const/4 v3, 0x0

    .line 1035
    .local v3, "result":Ljava/lang/Object;
    const/4 v6, 0x0

    .line 1036
    .local v6, "variableName":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    .line 1037
    .local v1, "isVariable":Z
    const/4 v5, 0x0

    .line 1038
    .local v5, "v":I
    const/4 v0, 0x0

    .local v0, "c":I
    move-object v10, v3

    .end local v3    # "result":Ljava/lang/Object;
    :goto_0
    if-ge v0, v2, :cond_5

    .line 1039
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v4

    .line 1041
    .local v4, "theNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    if-nez v10, :cond_2

    instance-of v8, v4, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;

    if-eqz v8, :cond_2

    .line 1042
    if-lez v5, :cond_1

    const/4 v8, 0x1

    :goto_1
    and-int/2addr v1, v8

    move-object v3, v10

    .line 1048
    :goto_2
    if-nez v3, :cond_4

    if-eqz v1, :cond_4

    .line 1049
    if-nez v5, :cond_0

    .line 1050
    new-instance v6, Ljava/lang/StringBuilder;

    .end local v6    # "variableName":Ljava/lang/StringBuilder;
    invoke-virtual {p1, v9}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    iget-object v8, v8, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1051
    .restart local v6    # "variableName":Ljava/lang/StringBuilder;
    const/4 v5, 0x1

    .line 1053
    :cond_0
    :goto_3
    if-gt v5, v0, :cond_3

    .line 1054
    const/16 v8, 0x2e

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1055
    invoke-virtual {p1, v5}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    iget-object v8, v8, Lorg/apache/commons/jexl2/parser/JexlNode;->image:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1053
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_1
    move v8, v9

    .line 1042
    goto :goto_1

    .line 1044
    :cond_2
    instance-of v8, v4, Lorg/apache/commons/jexl2/parser/ASTIdentifier;

    and-int/2addr v1, v8

    .line 1045
    invoke-virtual {v4, p0, v10}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .restart local v3    # "result":Ljava/lang/Object;
    goto :goto_2

    .line 1057
    .end local v3    # "result":Ljava/lang/Object;
    :cond_3
    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v10}, Lorg/apache/commons/jexl2/JexlContext;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 1038
    :cond_4
    add-int/lit8 v0, v0, 0x1

    move-object v10, v3

    goto :goto_0

    .line 1060
    .end local v4    # "theNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    :cond_5
    if-nez v10, :cond_6

    .line 1061
    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTReference;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v8

    instance-of v8, v8, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;

    if-nez v8, :cond_6

    iget-object v8, p0, Lorg/apache/commons/jexl2/Interpreter;->context:Lorg/apache/commons/jexl2/JexlContext;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/apache/commons/jexl2/JexlContext;->has(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 1064
    new-instance v7, Lorg/apache/commons/jexl2/JexlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "undefined variable "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, p1, v8}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    .line 1065
    .local v7, "xjexl":Lorg/apache/commons/jexl2/JexlException;
    invoke-virtual {p0, v7}, Lorg/apache/commons/jexl2/Interpreter;->unknownVariable(Lorg/apache/commons/jexl2/JexlException;)Ljava/lang/Object;

    move-result-object v8

    .line 1068
    .end local v7    # "xjexl":Lorg/apache/commons/jexl2/JexlException;
    :goto_4
    return-object v8

    :cond_6
    move-object v8, v10

    goto :goto_4
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTSizeFunction;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTSizeFunction;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1073
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTSizeFunction;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1075
    .local v0, "val":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 1076
    new-instance v1, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v2, "size() : argument is null"

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1079
    :cond_0
    invoke-direct {p0, p1, v0}, Lorg/apache/commons/jexl2/Interpreter;->sizeOf(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTSizeMethod;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTSizeMethod;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1084
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/Interpreter;->sizeOf(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTStringLiteral;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTStringLiteral;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1089
    if-eqz p2, :cond_0

    .line 1090
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTStringLiteral;->getLiteral()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0, p1}, Lorg/apache/commons/jexl2/Interpreter;->getAttribute(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v0

    .line 1092
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lorg/apache/commons/jexl2/parser/ASTStringLiteral;->image:Ljava/lang/String;

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTTernaryNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTTernaryNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    .line 1097
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1098
    .local v0, "condition":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetNumChildren()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 1099
    if-eqz v0, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-virtual {v1, v0}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1100
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1108
    .end local v0    # "condition":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v0

    .line 1102
    .restart local v0    # "condition":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1105
    :cond_2
    if-eqz v0, :cond_3

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1108
    :cond_3
    invoke-virtual {p1, v3}, Lorg/apache/commons/jexl2/parser/ASTTernaryNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v1, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTTrueNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTTrueNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1114
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 17
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1119
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/commons/jexl2/parser/ASTUnaryMinusNode;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v3

    .line 1120
    .local v3, "valNode":Lorg/apache/commons/jexl2/parser/JexlNode;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1121
    .local v2, "val":Ljava/lang/Object;
    instance-of v14, v2, Ljava/lang/Byte;

    if-eqz v14, :cond_0

    .line 1122
    check-cast v2, Ljava/lang/Byte;

    .end local v2    # "val":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    .line 1123
    .local v6, "valueAsByte":B
    neg-int v14, v6

    int-to-byte v14, v14

    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v14

    .line 1144
    .end local v6    # "valueAsByte":B
    :goto_0
    return-object v14

    .line 1124
    .restart local v2    # "val":Ljava/lang/Object;
    :cond_0
    instance-of v14, v2, Ljava/lang/Short;

    if-eqz v14, :cond_1

    .line 1125
    check-cast v2, Ljava/lang/Short;

    .end local v2    # "val":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Short;->shortValue()S

    move-result v13

    .line 1126
    .local v13, "valueAsShort":S
    neg-int v14, v13

    int-to-short v14, v14

    invoke-static {v14}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v14

    goto :goto_0

    .line 1127
    .end local v13    # "valueAsShort":S
    .restart local v2    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v14, v2, Ljava/lang/Integer;

    if-eqz v14, :cond_2

    .line 1128
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "val":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 1129
    .local v10, "valueAsInt":I
    neg-int v14, v10

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    goto :goto_0

    .line 1130
    .end local v10    # "valueAsInt":I
    .restart local v2    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v14, v2, Ljava/lang/Long;

    if-eqz v14, :cond_3

    .line 1131
    check-cast v2, Ljava/lang/Long;

    .end local v2    # "val":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    .line 1132
    .local v11, "valueAsLong":J
    neg-long v14, v11

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    goto :goto_0

    .line 1133
    .end local v11    # "valueAsLong":J
    .restart local v2    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v14, v2, Ljava/lang/Float;

    if-eqz v14, :cond_4

    .line 1134
    check-cast v2, Ljava/lang/Float;

    .end local v2    # "val":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 1135
    .local v9, "valueAsFloat":F
    new-instance v14, Ljava/lang/Float;

    neg-float v15, v9

    invoke-direct {v14, v15}, Ljava/lang/Float;-><init>(F)V

    goto :goto_0

    .line 1136
    .end local v9    # "valueAsFloat":F
    .restart local v2    # "val":Ljava/lang/Object;
    :cond_4
    instance-of v14, v2, Ljava/lang/Double;

    if-eqz v14, :cond_5

    .line 1137
    check-cast v2, Ljava/lang/Double;

    .end local v2    # "val":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    .line 1138
    .local v7, "valueAsDouble":D
    new-instance v14, Ljava/lang/Double;

    neg-double v15, v7

    invoke-direct/range {v14 .. v16}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0

    .line 1139
    .end local v7    # "valueAsDouble":D
    .restart local v2    # "val":Ljava/lang/Object;
    :cond_5
    instance-of v14, v2, Ljava/math/BigDecimal;

    if-eqz v14, :cond_6

    move-object v4, v2

    .line 1140
    check-cast v4, Ljava/math/BigDecimal;

    .line 1141
    .local v4, "valueAsBigD":Ljava/math/BigDecimal;
    invoke-virtual {v4}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v14

    goto :goto_0

    .line 1142
    .end local v4    # "valueAsBigD":Ljava/math/BigDecimal;
    :cond_6
    instance-of v14, v2, Ljava/math/BigInteger;

    if-eqz v14, :cond_7

    move-object v5, v2

    .line 1143
    check-cast v5, Ljava/math/BigInteger;

    .line 1144
    .local v5, "valueAsBigI":Ljava/math/BigInteger;
    invoke-virtual {v5}, Ljava/math/BigInteger;->negate()Ljava/math/BigInteger;

    move-result-object v14

    goto :goto_0

    .line 1146
    .end local v5    # "valueAsBigI":Ljava/math/BigInteger;
    :cond_7
    new-instance v14, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v15, "not a number"

    invoke-direct {v14, v3, v15}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/parser/JexlNode;Ljava/lang/String;)V

    throw v14
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/ASTWhileStatement;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/ASTWhileStatement;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1151
    const/4 v1, 0x0

    .line 1153
    .local v1, "result":Ljava/lang/Object;
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    .line 1154
    .end local v1    # "result":Ljava/lang/Object;
    .local v0, "expressionNode":Lorg/apache/commons/jexl2/parser/Node;
    :goto_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/Interpreter;->arithmetic:Lorg/apache/commons/jexl2/JexlArithmetic;

    invoke-interface {v0, p0, p2}, Lorg/apache/commons/jexl2/parser/Node;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/commons/jexl2/JexlArithmetic;->toBoolean(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1156
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lorg/apache/commons/jexl2/parser/ASTWhileStatement;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v2

    invoke-virtual {v2, p0, p2}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .restart local v1    # "result":Ljava/lang/Object;
    goto :goto_0

    .line 1159
    .end local v1    # "result":Ljava/lang/Object;
    :cond_0
    return-object v1
.end method

.method public visit(Lorg/apache/commons/jexl2/parser/SimpleNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "node"    # Lorg/apache/commons/jexl2/parser/SimpleNode;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1338
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
