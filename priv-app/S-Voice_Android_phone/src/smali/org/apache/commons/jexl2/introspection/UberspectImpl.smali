.class public Lorg/apache/commons/jexl2/introspection/UberspectImpl;
.super Lorg/apache/commons/jexl2/internal/Introspector;
.source "UberspectImpl.java"

# interfaces
.implements Lorg/apache/commons/jexl2/introspection/Uberspect;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;,
        Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;
    }
.end annotation


# static fields
.field public static final TRY_FAILED:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lorg/apache/commons/jexl2/internal/AbstractExecutor;->TRY_FAILED:Ljava/lang/Object;

    sput-object v0, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->TRY_FAILED:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/logging/Log;)V
    .locals 0
    .param p1, "runtimeLogger"    # Lorg/apache/commons/logging/Log;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/internal/Introspector;-><init>(Lorg/apache/commons/logging/Log;)V

    .line 57
    return-void
.end method


# virtual methods
.method public getConstructor(Ljava/lang/Object;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Constructor;
    .locals 1
    .param p1, "ctorHandle"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "[",
            "Ljava/lang/Object;",
            "Lorg/apache/commons/jexl2/JexlInfo;",
            ")",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getConstructor(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    return-object v0
.end method

.method public getField(Ljava/lang/Object;Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Field;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 101
    instance-of v1, p1, Ljava/lang/Class;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/lang/Class;

    .end local p1    # "obj":Ljava/lang/Object;
    move-object v0, p1

    .line 102
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    return-object v1

    .line 101
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public getIterator(Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/util/Iterator;
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "info"    # Lorg/apache/commons/jexl2/JexlInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lorg/apache/commons/jexl2/JexlInfo;",
            ")",
            "Ljava/util/Iterator",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 64
    instance-of v3, p1, Ljava/util/Iterator;

    if-eqz v3, :cond_0

    .line 65
    check-cast p1, Ljava/util/Iterator;

    .line 90
    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p1

    .line 67
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 68
    new-instance v2, Lorg/apache/commons/jexl2/internal/ArrayIterator;

    invoke-direct {v2, p1}, Lorg/apache/commons/jexl2/internal/ArrayIterator;-><init>(Ljava/lang/Object;)V

    move-object p1, v2

    goto :goto_0

    .line 70
    :cond_1
    instance-of v3, p1, Ljava/util/Map;

    if-eqz v3, :cond_2

    .line 71
    check-cast p1, Ljava/util/Map;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    goto :goto_0

    .line 73
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_2
    instance-of v3, p1, Ljava/util/Enumeration;

    if-eqz v3, :cond_3

    .line 74
    new-instance v2, Lorg/apache/commons/jexl2/internal/EnumerationIterator;

    check-cast p1, Ljava/util/Enumeration;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-direct {v2, p1}, Lorg/apache/commons/jexl2/internal/EnumerationIterator;-><init>(Ljava/util/Enumeration;)V

    move-object p1, v2

    goto :goto_0

    .line 76
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_3
    instance-of v3, p1, Ljava/lang/Iterable;

    if-eqz v3, :cond_4

    .line 77
    check-cast p1, Ljava/lang/Iterable;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    goto :goto_0

    .line 83
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_4
    :try_start_0
    const-string/jumbo v3, "iterator"

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v3, v4}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getMethodExecutor(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;

    move-result-object v0

    .line 84
    .local v0, "it":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    if-eqz v0, :cond_5

    const-class v3, Ljava/util/Iterator;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->getReturnType()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 85
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;->execute(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v2

    goto :goto_0

    .line 87
    .end local v0    # "it":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    :catch_0
    move-exception v1

    .line 88
    .local v1, "xany":Ljava/lang/Exception;
    new-instance v2, Lorg/apache/commons/jexl2/JexlException;

    const-string/jumbo v3, "unable to generate iterator()"

    invoke-direct {v2, p2, v3, v1}, Lorg/apache/commons/jexl2/JexlException;-><init>(Lorg/apache/commons/jexl2/JexlInfo;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .end local v1    # "xany":Ljava/lang/Exception;
    .restart local v0    # "it":Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;
    :cond_5
    move-object p1, v2

    .line 90
    goto :goto_0
.end method

.method public getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlMethod;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;
    .param p4, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 116
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getMethodExecutor(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Method;

    move-result-object v0

    return-object v0
.end method

.method public getPropertyGet(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;
    .param p3, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 176
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getGetExecutor(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;

    move-result-object v1

    .line 177
    .local v1, "get":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 178
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getField(Ljava/lang/Object;Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 179
    .local v0, "field":Ljava/lang/reflect/Field;
    if-eqz v0, :cond_0

    .line 180
    new-instance v1, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;

    .end local v1    # "get":Lorg/apache/commons/jexl2/introspection/JexlPropertyGet;
    invoke-direct {v1, v0}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertyGet;-><init>(Ljava/lang/reflect/Field;)V

    .line 183
    .end local v0    # "field":Ljava/lang/reflect/Field;
    :cond_0
    return-object v1
.end method

.method public getPropertySet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/commons/jexl2/JexlInfo;)Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "identifier"    # Ljava/lang/Object;
    .param p3, "arg"    # Ljava/lang/Object;
    .param p4, "info"    # Lorg/apache/commons/jexl2/JexlInfo;

    .prologue
    .line 247
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getSetExecutor(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;

    move-result-object v1

    .line 248
    .local v1, "set":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 249
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2, p4}, Lorg/apache/commons/jexl2/introspection/UberspectImpl;->getField(Ljava/lang/Object;Ljava/lang/String;Lorg/apache/commons/jexl2/JexlInfo;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 250
    .local v0, "field":Ljava/lang/reflect/Field;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p3, :cond_0

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->isInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 253
    :cond_0
    new-instance v1, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;

    .end local v1    # "set":Lorg/apache/commons/jexl2/introspection/JexlPropertySet;
    invoke-direct {v1, v0}, Lorg/apache/commons/jexl2/introspection/UberspectImpl$FieldPropertySet;-><init>(Ljava/lang/reflect/Field;)V

    .line 256
    .end local v0    # "field":Ljava/lang/reflect/Field;
    :cond_1
    return-object v1
.end method
