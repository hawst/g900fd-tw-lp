.class public Lorg/apache/commons/jexl2/DebugInfo;
.super Ljava/lang/Object;
.source "DebugInfo.java"

# interfaces
.implements Lorg/apache/commons/jexl2/JexlInfo;


# instance fields
.field private final column:I

.field private final line:I

.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "tn"    # Ljava/lang/String;
    .param p2, "l"    # I
    .param p3, "c"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lorg/apache/commons/jexl2/DebugInfo;->name:Ljava/lang/String;

    .line 40
    iput p2, p0, Lorg/apache/commons/jexl2/DebugInfo;->line:I

    .line 41
    iput p3, p0, Lorg/apache/commons/jexl2/DebugInfo;->column:I

    .line 42
    return-void
.end method


# virtual methods
.method public debugString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/DebugInfo;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->name:Ljava/lang/String;

    :goto_0
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 51
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->line:I

    if-lez v1, :cond_0

    .line 52
    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    iget v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->line:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    iget v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->column:I

    if-lez v1, :cond_0

    .line 55
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    iget v1, p0, Lorg/apache/commons/jexl2/DebugInfo;->column:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 59
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 50
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    const-string/jumbo v1, ""

    goto :goto_0
.end method
