.class public Lorg/apache/commons/jexl2/ExpressionImpl;
.super Ljava/lang/Object;
.source "ExpressionImpl.java"

# interfaces
.implements Lorg/apache/commons/jexl2/Expression;


# instance fields
.field protected final expression:Ljava/lang/String;

.field protected final jexl:Lorg/apache/commons/jexl2/JexlEngine;

.field protected final script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;


# direct methods
.method protected constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;Ljava/lang/String;Lorg/apache/commons/jexl2/parser/ASTJexlScript;)V
    .locals 0
    .param p1, "engine"    # Lorg/apache/commons/jexl2/JexlEngine;
    .param p2, "expr"    # Ljava/lang/String;
    .param p3, "ref"    # Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    .line 52
    iput-object p2, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->expression:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    .line 54
    return-void
.end method


# virtual methods
.method public evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;
    .locals 3
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 60
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetNumChildren()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 61
    const/4 v1, 0x0

    .line 64
    :goto_0
    return-object v1

    .line 63
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->jexl:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v1, p1}, Lorg/apache/commons/jexl2/JexlEngine;->createInterpreter(Lorg/apache/commons/jexl2/JexlContext;)Lorg/apache/commons/jexl2/Interpreter;

    move-result-object v0

    .line 64
    .local v0, "interpreter":Lorg/apache/commons/jexl2/Interpreter;
    iget-object v1, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->script:Lorg/apache/commons/jexl2/parser/ASTJexlScript;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/commons/jexl2/parser/ASTJexlScript;->jjtGetChild(I)Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/Interpreter;->interpret(Lorg/apache/commons/jexl2/parser/JexlNode;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getExpression()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/commons/jexl2/ExpressionImpl;->expression:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/ExpressionImpl;->getExpression()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "expr":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    .end local v0    # "expr":Ljava/lang/String;
    :cond_0
    return-object v0
.end method
