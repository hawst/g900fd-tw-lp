.class public final Lorg/apache/commons/jexl2/internal/ListSetExecutor;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;
.source "ListSetExecutor.java"


# static fields
.field private static final ARRAY_SET:Ljava/lang/reflect/Method;

.field private static final LIST_SET:Ljava/lang/reflect/Method;


# instance fields
.field private final property:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    const-class v0, Ljava/lang/reflect/Array;

    const-string/jumbo v1, "set"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/Object;

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    const-class v3, Ljava/lang/Object;

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->initMarker(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->ARRAY_SET:Ljava/lang/reflect/Method;

    .line 30
    const-class v0, Ljava/util/List;

    const-string/jumbo v1, "set"

    new-array v2, v6, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/Object;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->initMarker(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->LIST_SET:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/Integer;Ljava/lang/Object;)V
    .locals 1
    .param p1, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p3, "key"    # Ljava/lang/Integer;
    .param p4, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p2}, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->discover(Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Set;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 44
    iput-object p3, p0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->property:Ljava/lang/Integer;

    .line 45
    return-void
.end method

.method static discover(Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->ARRAY_SET:Ljava/lang/reflect/Method;

    .line 103
    :goto_0
    return-object v0

    .line 100
    :cond_0
    const-class v0, Ljava/util/List;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    sget-object v0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->LIST_SET:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public execute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 56
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->method:Ljava/lang/reflect/Method;

    sget-object v2, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->ARRAY_SET:Ljava/lang/reflect/Method;

    if-ne v1, v2, :cond_0

    .line 57
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->property:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p1, v1, p2}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 63
    :goto_0
    return-object p2

    :cond_0
    move-object v0, p1

    .line 60
    check-cast v0, Ljava/util/List;

    .line 61
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->property:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->property:Ljava/lang/Integer;

    return-object v0
.end method

.method public tryExecute(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 69
    if-eqz p1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->objectClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 72
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->method:Ljava/lang/reflect/Method;

    sget-object v2, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->ARRAY_SET:Ljava/lang/reflect/Method;

    if-ne v1, v2, :cond_0

    .line 73
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "key":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p1, v1, p3}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 81
    .end local p3    # "value":Ljava/lang/Object;
    :goto_0
    return-object p3

    .restart local p2    # "key":Ljava/lang/Object;
    .restart local p3    # "value":Ljava/lang/Object;
    :cond_0
    move-object v0, p1

    .line 76
    check-cast v0, Ljava/util/List;

    .line 77
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "key":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1, p3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 81
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .restart local p2    # "key":Ljava/lang/Object;
    :cond_1
    sget-object p3, Lorg/apache/commons/jexl2/internal/ListSetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
