.class public Lorg/apache/commons/jexl2/JexlEngine$SoftCache;
.super Ljava/lang/Object;
.source "JexlEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/JexlEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SoftCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private ref:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/util/Map",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field private final size:I

.field final synthetic this$0:Lorg/apache/commons/jexl2/JexlEngine;


# direct methods
.method constructor <init>(Lorg/apache/commons/jexl2/JexlEngine;I)V
    .locals 1
    .param p2, "theSize"    # I

    .prologue
    .line 719
    .local p0, "this":Lorg/apache/commons/jexl2/JexlEngine$SoftCache;, "Lorg/apache/commons/jexl2/JexlEngine$SoftCache<TK;TV;>;"
    iput-object p1, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->this$0:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 713
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->ref:Ljava/lang/ref/SoftReference;

    .line 720
    iput p2, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->size:I

    .line 721
    return-void
.end method


# virtual methods
.method get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/commons/jexl2/JexlEngine$SoftCache;, "Lorg/apache/commons/jexl2/JexlEngine$SoftCache<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    const/4 v2, 0x0

    .line 746
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->ref:Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->ref:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    move-object v0, v1

    .line 747
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    :goto_1
    return-object v1

    .end local v0    # "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :cond_0
    move-object v0, v2

    .line 746
    goto :goto_0

    .restart local v0    # "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :cond_1
    move-object v1, v2

    .line 747
    goto :goto_1
.end method

.method put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 756
    .local p0, "this":Lorg/apache/commons/jexl2/JexlEngine$SoftCache;, "Lorg/apache/commons/jexl2/JexlEngine$SoftCache<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "script":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->ref:Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->ref:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    move-object v0, v1

    .line 757
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :goto_0
    if-nez v0, :cond_0

    .line 758
    iget-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->this$0:Lorg/apache/commons/jexl2/JexlEngine;

    iget v2, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->size:I

    invoke-virtual {v1, v2}, Lorg/apache/commons/jexl2/JexlEngine;->createCache(I)Ljava/util/Map;

    move-result-object v0

    .line 759
    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->ref:Ljava/lang/ref/SoftReference;

    .line 761
    :cond_0
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 762
    return-void

    .line 756
    .end local v0    # "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method size()I
    .locals 1

    .prologue
    .line 728
    .local p0, "this":Lorg/apache/commons/jexl2/JexlEngine$SoftCache;, "Lorg/apache/commons/jexl2/JexlEngine$SoftCache<TK;TV;>;"
    iget v0, p0, Lorg/apache/commons/jexl2/JexlEngine$SoftCache;->size:I

    return v0
.end method
