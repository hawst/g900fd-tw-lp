.class public abstract Lorg/apache/commons/jexl2/parser/JexlNode;
.super Lorg/apache/commons/jexl2/parser/SimpleNode;
.source "JexlNode.java"

# interfaces
.implements Lorg/apache/commons/jexl2/JexlInfo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/jexl2/parser/JexlNode$Literal;
    }
.end annotation


# instance fields
.field public image:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/SimpleNode;-><init>(I)V

    .line 39
    return-void
.end method


# virtual methods
.method public debugString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/parser/JexlNode;->getInfo()Lorg/apache/commons/jexl2/JexlInfo;

    move-result-object v0

    .line 59
    .local v0, "info":Lorg/apache/commons/jexl2/JexlInfo;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/commons/jexl2/JexlInfo;->debugString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string/jumbo v1, ""

    goto :goto_0
.end method

.method public getInfo()Lorg/apache/commons/jexl2/JexlInfo;
    .locals 2

    .prologue
    .line 46
    move-object v0, p0

    .line 47
    .local v0, "node":Lorg/apache/commons/jexl2/parser/JexlNode;
    :goto_0
    if-eqz v0, :cond_1

    .line 48
    iget-object v1, v0, Lorg/apache/commons/jexl2/parser/JexlNode;->value:Ljava/lang/Object;

    instance-of v1, v1, Lorg/apache/commons/jexl2/JexlInfo;

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, v0, Lorg/apache/commons/jexl2/parser/JexlNode;->value:Ljava/lang/Object;

    check-cast v1, Lorg/apache/commons/jexl2/JexlInfo;

    .line 53
    :goto_1
    return-object v1

    .line 51
    :cond_0
    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/JexlNode;->jjtGetParent()Lorg/apache/commons/jexl2/parser/JexlNode;

    move-result-object v0

    goto :goto_0

    .line 53
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
