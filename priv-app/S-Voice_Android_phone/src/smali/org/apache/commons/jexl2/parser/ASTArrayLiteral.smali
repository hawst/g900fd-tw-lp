.class public final Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;
.super Lorg/apache/commons/jexl2/parser/JexlNode;
.source "ASTArrayLiteral.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/JexlNode$Literal;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/commons/jexl2/parser/JexlNode;",
        "Lorg/apache/commons/jexl2/parser/JexlNode$Literal",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field array:Ljava/lang/Object;


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/JexlNode;-><init>(I)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->array:Ljava/lang/Object;

    .line 25
    return-void
.end method


# virtual methods
.method public getLiteral()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->array:Ljava/lang/Object;

    return-object v0
.end method

.method public jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "visitor"    # Lorg/apache/commons/jexl2/parser/ParserVisitor;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 54
    invoke-interface {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/ParserVisitor;->visit(Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setLiteral(Ljava/lang/Object;)V
    .locals 3
    .param p1, "literal"    # Ljava/lang/Object;

    .prologue
    .line 45
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not an array"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/ASTArrayLiteral;->array:Ljava/lang/Object;

    .line 49
    return-void
.end method
