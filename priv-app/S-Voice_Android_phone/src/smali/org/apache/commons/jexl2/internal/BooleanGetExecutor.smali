.class public final Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;
.super Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;
.source "BooleanGetExecutor.java"


# instance fields
.field private final property:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1
    .param p1, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p3, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p1, p2, p3}, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/apache/commons/jexl2/internal/AbstractExecutor$Get;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 35
    iput-object p3, p0, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->property:Ljava/lang/String;

    .line 36
    return-void
.end method

.method static discover(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 3
    .param p0, "is"    # Lorg/apache/commons/jexl2/internal/Introspector;
    .param p2, "property"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/internal/Introspector;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v1, "is"

    invoke-static {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/internal/PropertyGetExecutor;->discoverGet(Lorg/apache/commons/jexl2/internal/Introspector;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 79
    .local v0, "m":Ljava/lang/reflect/Method;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v1, v2, :cond_0

    .end local v0    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-object v0

    .restart local v0    # "m":Ljava/lang/reflect/Method;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public execute(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 48
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->method:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->method:Ljava/lang/reflect/Method;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getTargetProperty()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->property:Ljava/lang/String;

    return-object v0
.end method

.method public tryExecute(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 54
    if-eqz p1, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->method:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->property:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->objectClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    :try_start_0
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->method:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v3, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 66
    :goto_0
    return-object v2

    .line 60
    :catch_0
    move-exception v1

    .line 61
    .local v1, "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    sget-object v2, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 62
    .end local v1    # "xinvoke":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v0

    .line 63
    .local v0, "xill":Ljava/lang/IllegalAccessException;
    sget-object v2, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0

    .line 66
    .end local v0    # "xill":Ljava/lang/IllegalAccessException;
    :cond_0
    sget-object v2, Lorg/apache/commons/jexl2/internal/BooleanGetExecutor;->TRY_FAILED:Ljava/lang/Object;

    goto :goto_0
.end method
