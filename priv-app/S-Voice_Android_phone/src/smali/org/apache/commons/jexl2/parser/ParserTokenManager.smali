.class public Lorg/apache/commons/jexl2/parser/ParserTokenManager;
.super Ljava/lang/Object;
.source "ParserTokenManager.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/ParserConstants;


# static fields
.field static final jjbitVec0:[J

.field static final jjbitVec2:[J

.field static final jjbitVec3:[J

.field static final jjbitVec4:[J

.field public static final jjnewLexState:[I

.field static final jjnextStates:[I

.field public static final jjstrLiteralImages:[Ljava/lang/String;

.field static final jjtoSkip:[J

.field static final jjtoToken:[J

.field public static final lexStateNames:[Ljava/lang/String;


# instance fields
.field protected curChar:C

.field curLexState:I

.field public debugStream:Ljava/io/PrintStream;

.field defaultLexState:I

.field protected input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

.field jjmatchedKind:I

.field jjmatchedPos:I

.field jjnewStateCnt:I

.field jjround:I

.field private final jjrounds:[I

.field private final jjstateSet:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 372
    new-array v0, v2, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec0:[J

    .line 375
    new-array v0, v2, [J

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec2:[J

    .line 378
    new-array v0, v2, [J

    fill-array-data v0, :array_2

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec3:[J

    .line 381
    new-array v0, v2, [J

    fill-array-data v0, :array_3

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec4:[J

    .line 2821
    const/16 v0, 0x31

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnextStates:[I

    .line 2855
    const/16 v0, 0x3d

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v4

    aput-object v3, v0, v5

    aput-object v3, v0, v6

    const/4 v1, 0x3

    aput-object v3, v0, v1

    aput-object v3, v0, v2

    const/4 v1, 0x5

    aput-object v3, v0, v1

    const/4 v1, 0x6

    aput-object v3, v0, v1

    const/4 v1, 0x7

    aput-object v3, v0, v1

    const/16 v1, 0x8

    aput-object v3, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "if"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "else"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "for"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "foreach"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "while"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "new"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "empty"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "size"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "true"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "false"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "in"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "{"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "}"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "["

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "]"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, ";"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "?"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "?:"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    aput-object v3, v0, v1

    const/16 v1, 0x22

    aput-object v3, v0, v1

    const/16 v1, 0x23

    aput-object v3, v0, v1

    const/16 v1, 0x24

    aput-object v3, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "=~"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "!~"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    aput-object v3, v0, v1

    const/16 v1, 0x28

    aput-object v3, v0, v1

    const/16 v1, 0x29

    aput-object v3, v0, v1

    const/16 v1, 0x2a

    aput-object v3, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    aput-object v3, v0, v1

    const/16 v1, 0x2d

    aput-object v3, v0, v1

    const/16 v1, 0x2e

    aput-object v3, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "-"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "~"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "&"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "^"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    aput-object v3, v0, v1

    const/16 v1, 0x37

    aput-object v3, v0, v1

    const/16 v1, 0x38

    aput-object v3, v0, v1

    const/16 v1, 0x39

    aput-object v3, v0, v1

    const/16 v1, 0x3a

    aput-object v3, v0, v1

    const/16 v1, 0x3b

    aput-object v3, v0, v1

    const/16 v1, 0x3c

    aput-object v3, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    .line 2865
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "REGISTERS"

    aput-object v1, v0, v4

    const-string/jumbo v1, "FOR_EACH_IN"

    aput-object v1, v0, v5

    const-string/jumbo v1, "DEFAULT"

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->lexStateNames:[Ljava/lang/String;

    .line 2872
    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    .line 2877
    new-array v0, v5, [J

    const-wide v1, 0x1e7ffffffffffe01L    # 8.891034997939806E-162

    aput-wide v1, v0, v4

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjtoToken:[J

    .line 2880
    new-array v0, v5, [J

    const-wide/16 v1, 0x1fe

    aput-wide v1, v0, v4

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjtoSkip:[J

    return-void

    .line 372
    :array_0
    .array-data 8
        -0x2
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 375
    :array_1
    .array-data 8
        0x0
        0x0
        -0x1
        -0x1
    .end array-data

    .line 378
    :array_2
    .array-data 8
        -0x100000002L
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 381
    :array_3
    .array-data 8
        -0x30000000001L
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 2821
    :array_4
    .array-data 4
        0x47
        0x48
        0x49
        0x31
        0x37
        0x2c
        0x2d
        0x2f
        0x27
        0x28
        0x2a
        0x1
        0x2
        0x4
        0x33
        0x34
        0x36
        0x38
        0x39
        0x3b
        0x44
        0x45
        0x41
        0x42
        0x3d
        0x3f
        0x49
        0x4a
        0x4b
        0x33
        0x39
        0x2e
        0x2f
        0x31
        0x29
        0x2a
        0x2c
        0x35
        0x36
        0x38
        0x3a
        0x3b
        0x3d
        0x46
        0x47
        0x43
        0x44
        0x3f
        0x41
    .end array-data

    .line 2872
    :array_5
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x2
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/commons/jexl2/parser/SimpleCharStream;)V
    .locals 2
    .param p1, "stream"    # Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    .prologue
    const/4 v1, 0x2

    .line 2888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->debugStream:Ljava/io/PrintStream;

    .line 2884
    const/16 v0, 0x4d

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjrounds:[I

    .line 2885
    const/16 v0, 0x9a

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    .line 2956
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    .line 2957
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->defaultLexState:I

    .line 2891
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    .line 2892
    return-void
.end method

.method private ReInitRounds()V
    .locals 4

    .prologue
    .line 2911
    const v2, -0x7fffffff

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    .line 2912
    const/16 v0, 0x4d

    .local v0, "i":I
    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    if-lez v1, :cond_0

    .line 2913
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjrounds:[I

    const/high16 v3, -0x80000000

    aput v3, v2, v0

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 2914
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_0
    return-void
.end method

.method private jjAddStates(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 3068
    :goto_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    sget-object v3, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnextStates:[I

    aget v3, v3, p1

    aput v3, v1, v2

    .line 3069
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 3070
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private static final jjCanMove_0(IIIJJ)Z
    .locals 6
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2829
    packed-switch p0, :pswitch_data_0

    .line 2834
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec0:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2836
    :cond_0
    :goto_0
    return v0

    .line 2832
    :pswitch_0
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec2:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2836
    goto :goto_0

    .line 2829
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static final jjCanMove_1(IIIJJ)Z
    .locals 6
    .param p0, "hiByte"    # I
    .param p1, "i1"    # I
    .param p2, "i2"    # I
    .param p3, "l1"    # J
    .param p5, "l2"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2841
    sparse-switch p0, :sswitch_data_0

    .line 2848
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec3:[J

    aget-wide v2, v2, p1

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2850
    :cond_0
    :goto_0
    return v0

    .line 2844
    :sswitch_0
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec2:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2846
    :sswitch_1
    sget-object v2, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjbitVec4:[J

    aget-wide v2, v2, p2

    and-long/2addr v2, p5

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2850
    goto :goto_0

    .line 2841
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch
.end method

.method private jjCheckNAdd(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 3059
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjrounds:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    if-eq v0, v1, :cond_0

    .line 3061
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    aput p1, v0, v1

    .line 3062
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjrounds:[I

    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    aput v1, v0, p1

    .line 3064
    :cond_0
    return-void
.end method

.method private jjCheckNAddStates(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 3080
    :goto_0
    sget-object v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnextStates:[I

    aget v1, v1, p1

    invoke-direct {p0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 3081
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "start":I
    .local v0, "start":I
    if-ne p1, p2, :cond_0

    .line 3082
    return-void

    :cond_0
    move p1, v0

    .end local v0    # "start":I
    .restart local p1    # "start":I
    goto :goto_0
.end method

.method private jjCheckNAddTwoStates(II)V
    .locals 0
    .param p1, "state1"    # I
    .param p2, "state2"    # I

    .prologue
    .line 3073
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 3074
    invoke-direct {p0, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 3075
    return-void
.end method

.method private jjMoveNfa_0(II)I
    .locals 19
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 2240
    const/4 v14, 0x0

    .line 2241
    .local v14, "startsAt":I
    const/16 v15, 0x4d

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    .line 2242
    const/4 v10, 0x1

    .line 2243
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 2244
    const v11, 0x7fffffff

    .line 2247
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 2248
    invoke-direct/range {p0 .. p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInitRounds()V

    .line 2249
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_20

    .line 2251
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    shl-long v12, v15, v17

    .line 2254
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 2548
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 2808
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 2810
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2811
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 2812
    const v11, 0x7fffffff

    .line 2814
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 2815
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x4d

    if-ne v10, v14, :cond_3b

    .line 2818
    :goto_3
    return p2

    .line 2257
    .restart local v12    # "l":J
    :pswitch_1
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_6

    .line 2259
    const/16 v15, 0x3a

    if-le v11, v15, :cond_4

    .line 2260
    const/16 v11, 0x3a

    .line 2261
    :cond_4
    const/16 v15, 0x1a

    const/16 v16, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    .line 2295
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_12

    .line 2297
    const/16 v15, 0x2d

    if-le v11, v15, :cond_2

    .line 2298
    const/16 v11, 0x2d

    goto :goto_1

    .line 2263
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 2264
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 2265
    :cond_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 2266
    const/16 v15, 0x1f

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 2267
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 2268
    const/16 v15, 0x22

    const/16 v16, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 2269
    :cond_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 2270
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x27

    aput v17, v15, v16

    goto :goto_4

    .line 2271
    :cond_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_c

    .line 2273
    const/16 v15, 0x36

    if-le v11, v15, :cond_b

    .line 2274
    const/16 v11, 0x36

    .line 2275
    :cond_b
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_4

    .line 2277
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    .line 2279
    const/16 v15, 0x2e

    if-le v11, v15, :cond_5

    .line 2280
    const/16 v11, 0x2e

    goto/16 :goto_4

    .line 2282
    :cond_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 2284
    const/16 v15, 0x2c

    if-le v11, v15, :cond_5

    .line 2285
    const/16 v11, 0x2c

    goto/16 :goto_4

    .line 2287
    :cond_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 2288
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_4

    .line 2289
    :cond_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_10

    .line 2290
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_4

    .line 2291
    :cond_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 2292
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_4

    .line 2293
    :cond_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 2294
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_4

    .line 2300
    :cond_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_13

    .line 2302
    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 2303
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 2305
    :cond_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_14

    .line 2307
    const/16 v15, 0x27

    if-le v11, v15, :cond_2

    .line 2308
    const/16 v11, 0x27

    goto/16 :goto_1

    .line 2310
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_15

    .line 2311
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2312
    :cond_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2313
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x0

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2317
    :pswitch_2
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2319
    const/16 v15, 0x36

    if-le v11, v15, :cond_16

    .line 2320
    const/16 v11, 0x36

    .line 2321
    :cond_16
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2324
    :pswitch_3
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2326
    const/16 v15, 0x36

    if-le v11, v15, :cond_17

    .line 2327
    const/16 v11, 0x36

    .line 2328
    :cond_17
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2331
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2333
    const/4 v15, 0x1

    if-le v11, v15, :cond_18

    .line 2334
    const/4 v11, 0x1

    .line 2335
    :cond_18
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2338
    :pswitch_5
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2340
    const/4 v15, 0x1

    if-le v11, v15, :cond_19

    .line 2341
    const/4 v11, 0x1

    .line 2342
    :cond_19
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2345
    :pswitch_6
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 2346
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 2349
    :pswitch_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 2350
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 2353
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2354
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2357
    :pswitch_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x21

    if-le v11, v15, :cond_2

    .line 2358
    const/16 v11, 0x21

    goto/16 :goto_1

    .line 2361
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2362
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2365
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x23

    if-le v11, v15, :cond_2

    .line 2366
    const/16 v11, 0x23

    goto/16 :goto_1

    .line 2369
    :pswitch_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2370
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2373
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x24

    if-le v11, v15, :cond_2

    .line 2374
    const/16 v11, 0x24

    goto/16 :goto_1

    .line 2377
    :pswitch_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2378
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2381
    :pswitch_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x27

    if-le v11, v15, :cond_2

    .line 2382
    const/16 v11, 0x27

    goto/16 :goto_1

    .line 2385
    :pswitch_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x28

    if-le v11, v15, :cond_2

    .line 2386
    const/16 v11, 0x28

    goto/16 :goto_1

    .line 2389
    :pswitch_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2390
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2393
    :pswitch_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 2394
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 2397
    :pswitch_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2a

    if-le v11, v15, :cond_2

    .line 2398
    const/16 v11, 0x2a

    goto/16 :goto_1

    .line 2401
    :pswitch_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2402
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2405
    :pswitch_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2c

    if-le v11, v15, :cond_2

    .line 2406
    const/16 v11, 0x2c

    goto/16 :goto_1

    .line 2409
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2d

    if-le v11, v15, :cond_2

    .line 2410
    const/16 v11, 0x2d

    goto/16 :goto_1

    .line 2413
    :pswitch_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2e

    if-le v11, v15, :cond_2

    .line 2414
    const/16 v11, 0x2e

    goto/16 :goto_1

    .line 2417
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2419
    const/16 v15, 0x36

    if-le v11, v15, :cond_1a

    .line 2420
    const/16 v11, 0x36

    .line 2421
    :cond_1a
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2424
    :pswitch_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2425
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x27

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2428
    :pswitch_1a
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/16 v15, 0x39

    if-le v11, v15, :cond_2

    .line 2429
    const/16 v11, 0x39

    goto/16 :goto_1

    .line 2432
    :pswitch_1b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2433
    const/16 v15, 0x22

    const/16 v16, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2436
    :pswitch_1c
    const-wide v15, -0x400002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2437
    const/16 v15, 0x22

    const/16 v16, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2440
    :pswitch_1d
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2441
    const/16 v15, 0x22

    const/16 v16, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2444
    :pswitch_1e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3c

    if-le v11, v15, :cond_2

    .line 2445
    const/16 v11, 0x3c

    goto/16 :goto_1

    .line 2448
    :pswitch_1f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2449
    const/16 v15, 0x1f

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2452
    :pswitch_20
    const-wide v15, -0x8000002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2453
    const/16 v15, 0x1f

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2456
    :pswitch_21
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2457
    const/16 v15, 0x1f

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2460
    :pswitch_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3c

    if-le v11, v15, :cond_2

    .line 2461
    const/16 v11, 0x3c

    goto/16 :goto_1

    .line 2464
    :pswitch_23
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2465
    const/16 v15, 0x1d

    const/16 v16, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_1

    .line 2468
    :pswitch_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2469
    const/16 v15, 0x34

    const/16 v16, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2472
    :pswitch_25
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2473
    const/16 v15, 0x34

    const/16 v16, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2476
    :pswitch_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2477
    const/16 v15, 0x25

    const/16 v16, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2480
    :pswitch_27
    const-wide v15, -0x840000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2481
    const/16 v15, 0x37

    const/16 v16, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2484
    :pswitch_28
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2485
    const/16 v15, 0x37

    const/16 v16, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2488
    :pswitch_29
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x2

    if-le v11, v15, :cond_2

    .line 2489
    const/4 v11, 0x2

    goto/16 :goto_1

    .line 2492
    :pswitch_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2494
    const/4 v15, 0x3

    if-le v11, v15, :cond_1b

    .line 2495
    const/4 v11, 0x3

    .line 2496
    :cond_1b
    const/16 v15, 0x28

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2499
    :pswitch_2b
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2501
    const/4 v15, 0x3

    if-le v11, v15, :cond_1c

    .line 2502
    const/4 v11, 0x3

    .line 2503
    :cond_1c
    const/16 v15, 0x28

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2506
    :pswitch_2c
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 2507
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 2510
    :pswitch_2d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 2511
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 2514
    :pswitch_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2515
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3c

    aput v17, v15, v16

    goto/16 :goto_1

    .line 2518
    :pswitch_2f
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2520
    const/16 v15, 0x3a

    if-le v11, v15, :cond_1d

    .line 2521
    const/16 v11, 0x3a

    .line 2522
    :cond_1d
    const/16 v15, 0x1a

    const/16 v16, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 2525
    :pswitch_30
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2527
    const/16 v15, 0x3a

    if-le v11, v15, :cond_1e

    .line 2528
    const/16 v11, 0x3a

    .line 2529
    :cond_1e
    const/16 v15, 0x49

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2532
    :pswitch_31
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2533
    const/16 v15, 0x4a

    const/16 v16, 0x4b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 2536
    :pswitch_32
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 2537
    const/16 v15, 0x4c

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2540
    :pswitch_33
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 2542
    const/16 v15, 0x3b

    if-le v11, v15, :cond_1f

    .line 2543
    const/16 v11, 0x3b

    .line 2544
    :cond_1f
    const/16 v15, 0x4c

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 2550
    .end local v12    # "l":J
    :cond_20
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_36

    .line 2552
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v12, v15, v17

    .line 2555
    .restart local v12    # "l":J
    :cond_21
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 2758
    :cond_22
    :goto_5
    :pswitch_34
    if-ne v10, v14, :cond_21

    goto/16 :goto_2

    .line 2558
    :pswitch_35
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_25

    .line 2560
    const/16 v15, 0x36

    if-le v11, v15, :cond_23

    .line 2561
    const/16 v11, 0x36

    .line 2562
    :cond_23
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 2566
    :cond_24
    :goto_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_26

    .line 2567
    const/16 v15, 0x2b

    const/16 v16, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 2564
    :cond_25
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_24

    .line 2565
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto :goto_6

    .line 2568
    :cond_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_27

    .line 2569
    const/16 v15, 0x2d

    const/16 v16, 0x2e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 2570
    :cond_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 2571
    const/16 v15, 0x2f

    const/16 v16, 0x30

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 2572
    :cond_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_29

    .line 2573
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2574
    :cond_29
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 2575
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2576
    :cond_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_2b

    .line 2577
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2578
    :cond_2b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2c

    .line 2579
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2580
    :cond_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2581
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2584
    :pswitch_36
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2e

    .line 2586
    const/16 v15, 0x36

    if-le v11, v15, :cond_2d

    .line 2587
    const/16 v11, 0x36

    .line 2588
    :cond_2d
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 2590
    :cond_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2f

    .line 2591
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x40

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2592
    :cond_2f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2594
    const/16 v15, 0x24

    if-le v11, v15, :cond_22

    .line 2595
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 2599
    :pswitch_37
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_31

    .line 2601
    const/16 v15, 0x36

    if-le v11, v15, :cond_30

    .line 2602
    const/16 v11, 0x36

    .line 2603
    :cond_30
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 2605
    :cond_31
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x71

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2607
    const/16 v15, 0x23

    if-le v11, v15, :cond_22

    .line 2608
    const/16 v11, 0x23

    goto/16 :goto_5

    .line 2612
    :pswitch_38
    const/4 v15, 0x1

    if-le v11, v15, :cond_32

    .line 2613
    const/4 v11, 0x1

    .line 2614
    :cond_32
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2617
    :pswitch_39
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x21

    if-le v11, v15, :cond_22

    .line 2618
    const/16 v11, 0x21

    goto/16 :goto_5

    .line 2621
    :pswitch_3a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2622
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2625
    :pswitch_3b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2626
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2629
    :pswitch_3c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x22

    if-le v11, v15, :cond_22

    .line 2630
    const/16 v11, 0x22

    goto/16 :goto_5

    .line 2633
    :pswitch_3d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2634
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2637
    :pswitch_3e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x72

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x22

    if-le v11, v15, :cond_22

    .line 2638
    const/16 v11, 0x22

    goto/16 :goto_5

    .line 2641
    :pswitch_3f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2642
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2645
    :pswitch_40
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2646
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2649
    :pswitch_41
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x2c

    if-le v11, v15, :cond_22

    .line 2650
    const/16 v11, 0x2c

    goto/16 :goto_5

    .line 2653
    :pswitch_42
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2654
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1c

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2657
    :pswitch_43
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2658
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2661
    :pswitch_44
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x76

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x2d

    if-le v11, v15, :cond_22

    .line 2662
    const/16 v11, 0x2d

    goto/16 :goto_5

    .line 2665
    :pswitch_45
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x69

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2666
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x20

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2669
    :pswitch_46
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2670
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2673
    :pswitch_47
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_22

    .line 2675
    const/16 v15, 0x36

    if-le v11, v15, :cond_33

    .line 2676
    const/16 v11, 0x36

    .line 2677
    :cond_33
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 2680
    :pswitch_48
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_22

    .line 2682
    const/16 v15, 0x36

    if-le v11, v15, :cond_34

    .line 2683
    const/16 v11, 0x36

    .line 2684
    :cond_34
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 2687
    :pswitch_49
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_22

    .line 2688
    const/16 v15, 0x22

    const/16 v16, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 2691
    :pswitch_4a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2692
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2b

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2695
    :pswitch_4b
    const/16 v15, 0x22

    const/16 v16, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 2698
    :pswitch_4c
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_22

    .line 2699
    const/16 v15, 0x1f

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 2702
    :pswitch_4d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2703
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x30

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2706
    :pswitch_4e
    const/16 v15, 0x1f

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 2709
    :pswitch_4f
    const/16 v15, 0x34

    const/16 v16, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 2713
    :pswitch_50
    const/16 v15, 0x37

    const/16 v16, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 2716
    :pswitch_51
    const/4 v15, 0x3

    if-le v11, v15, :cond_35

    .line 2717
    const/4 v11, 0x3

    .line 2718
    :cond_35
    const/16 v15, 0x28

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2721
    :pswitch_52
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2722
    const/16 v15, 0x2f

    const/16 v16, 0x30

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2725
    :pswitch_53
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x2e

    if-le v11, v15, :cond_22

    .line 2726
    const/16 v11, 0x2e

    goto/16 :goto_5

    .line 2729
    :pswitch_54
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2730
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x40

    aput v17, v15, v16

    goto/16 :goto_5

    .line 2733
    :pswitch_55
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2734
    const/16 v15, 0x2d

    const/16 v16, 0x2e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2737
    :pswitch_56
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x27

    if-le v11, v15, :cond_22

    .line 2738
    const/16 v11, 0x27

    goto/16 :goto_5

    .line 2741
    :pswitch_57
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x28

    if-le v11, v15, :cond_22

    .line 2742
    const/16 v11, 0x28

    goto/16 :goto_5

    .line 2745
    :pswitch_58
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    .line 2746
    const/16 v15, 0x2b

    const/16 v16, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 2749
    :pswitch_59
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x29

    if-le v11, v15, :cond_22

    .line 2750
    const/16 v11, 0x29

    goto/16 :goto_5

    .line 2753
    :pswitch_5a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_22

    const/16 v15, 0x2a

    if-le v11, v15, :cond_22

    .line 2754
    const/16 v11, 0x2a

    goto/16 :goto_5

    .line 2762
    .end local v12    # "l":J
    :cond_36
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shr-int/lit8 v2, v15, 0x8

    .line 2763
    .local v2, "hiByte":I
    shr-int/lit8 v3, v2, 0x6

    .line 2764
    .local v3, "i1":I
    const-wide/16 v15, 0x1

    and-int/lit8 v17, v2, 0x3f

    shl-long v5, v15, v17

    .line 2765
    .local v5, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v4, v15, 0x6

    .line 2766
    .local v4, "i2":I
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v7, v15, v17

    .line 2769
    .local v7, "l2":J
    :cond_37
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 2806
    :cond_38
    :goto_7
    if-ne v10, v14, :cond_37

    goto/16 :goto_2

    .line 2772
    :sswitch_0
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_38

    .line 2774
    const/4 v15, 0x1

    if-le v11, v15, :cond_39

    .line 2775
    const/4 v11, 0x1

    .line 2776
    :cond_39
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 2780
    :sswitch_1
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_38

    .line 2781
    const/16 v15, 0x22

    const/16 v16, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 2785
    :sswitch_2
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_38

    .line 2786
    const/16 v15, 0x1f

    const/16 v16, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 2789
    :sswitch_3
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_38

    .line 2790
    const/16 v15, 0x34

    const/16 v16, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 2794
    :sswitch_4
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_38

    .line 2795
    const/16 v15, 0x37

    const/16 v16, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 2798
    :sswitch_5
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_38

    .line 2800
    const/4 v15, 0x3

    if-le v11, v15, :cond_3a

    .line 2801
    const/4 v11, 0x3

    .line 2802
    :cond_3a
    const/16 v15, 0x28

    const/16 v16, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 2817
    .end local v2    # "hiByte":I
    .end local v3    # "i1":I
    .end local v4    # "i2":I
    .end local v5    # "l1":J
    .end local v7    # "l2":J
    :cond_3b
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v15}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2818
    :catch_0
    move-exception v9

    .local v9, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 2254
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_3
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_2
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
    .end packed-switch

    .line 2555
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_38
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_35
        :pswitch_34
        :pswitch_34
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_34
        :pswitch_34
        :pswitch_37
        :pswitch_40
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_34
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_34
        :pswitch_47
        :pswitch_48
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_34
        :pswitch_34
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_4f
        :pswitch_34
        :pswitch_50
        :pswitch_50
        :pswitch_34
        :pswitch_34
        :pswitch_51
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_52
        :pswitch_36
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
    .end packed-switch

    .line 2769
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2b -> :sswitch_1
        0x2e -> :sswitch_2
        0x30 -> :sswitch_2
        0x34 -> :sswitch_3
        0x36 -> :sswitch_4
        0x37 -> :sswitch_4
        0x3a -> :sswitch_5
    .end sparse-switch
.end method

.method private jjMoveNfa_1(II)I
    .locals 19
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 1315
    const/4 v14, 0x0

    .line 1316
    .local v14, "startsAt":I
    const/16 v15, 0x4b

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    .line 1317
    const/4 v10, 0x1

    .line 1318
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 1319
    const v11, 0x7fffffff

    .line 1322
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 1323
    invoke-direct/range {p0 .. p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInitRounds()V

    .line 1324
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_1f

    .line 1326
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    shl-long v12, v15, v17

    .line 1329
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 1613
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 1873
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 1875
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1876
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 1877
    const v11, 0x7fffffff

    .line 1879
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 1880
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x4b

    if-ne v10, v14, :cond_3a

    .line 1883
    :goto_3
    return p2

    .line 1332
    .restart local v12    # "l":J
    :pswitch_1
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_6

    .line 1334
    const/16 v15, 0x3a

    if-le v11, v15, :cond_4

    .line 1335
    const/16 v11, 0x3a

    .line 1336
    :cond_4
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    .line 1370
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_12

    .line 1372
    const/16 v15, 0x2d

    if-le v11, v15, :cond_2

    .line 1373
    const/16 v11, 0x2d

    goto :goto_1

    .line 1338
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 1339
    const/4 v15, 0x3

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 1340
    :cond_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 1341
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 1342
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 1343
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 1344
    :cond_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    .line 1346
    const/16 v15, 0x36

    if-le v11, v15, :cond_a

    .line 1347
    const/16 v11, 0x36

    .line 1348
    :cond_a
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 1350
    :cond_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_c

    .line 1352
    const/16 v15, 0x2e

    if-le v11, v15, :cond_5

    .line 1353
    const/16 v11, 0x2e

    goto :goto_4

    .line 1355
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    .line 1357
    const/16 v15, 0x2c

    if-le v11, v15, :cond_5

    .line 1358
    const/16 v11, 0x2c

    goto/16 :goto_4

    .line 1360
    :cond_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 1361
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1362
    :cond_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 1363
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1364
    :cond_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_10

    .line 1365
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1366
    :cond_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 1367
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1368
    :cond_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 1369
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x0

    aput v17, v15, v16

    goto/16 :goto_4

    .line 1375
    :cond_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_13

    .line 1377
    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 1378
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 1380
    :cond_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_14

    .line 1382
    const/16 v15, 0x27

    if-le v11, v15, :cond_2

    .line 1383
    const/16 v11, 0x27

    goto/16 :goto_1

    .line 1385
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1386
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1390
    :pswitch_2
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1392
    const/16 v15, 0x36

    if-le v11, v15, :cond_15

    .line 1393
    const/16 v11, 0x36

    .line 1394
    :cond_15
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1397
    :pswitch_3
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1399
    const/16 v15, 0x36

    if-le v11, v15, :cond_16

    .line 1400
    const/16 v11, 0x36

    .line 1401
    :cond_16
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1404
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1406
    const/4 v15, 0x1

    if-le v11, v15, :cond_17

    .line 1407
    const/4 v11, 0x1

    .line 1408
    :cond_17
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1411
    :pswitch_5
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1413
    const/4 v15, 0x1

    if-le v11, v15, :cond_18

    .line 1414
    const/4 v11, 0x1

    .line 1415
    :cond_18
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1418
    :pswitch_6
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 1419
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 1422
    :pswitch_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 1423
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 1426
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1427
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1430
    :pswitch_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x21

    if-le v11, v15, :cond_2

    .line 1431
    const/16 v11, 0x21

    goto/16 :goto_1

    .line 1434
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1435
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1438
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x23

    if-le v11, v15, :cond_2

    .line 1439
    const/16 v11, 0x23

    goto/16 :goto_1

    .line 1442
    :pswitch_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1443
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1446
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x24

    if-le v11, v15, :cond_2

    .line 1447
    const/16 v11, 0x24

    goto/16 :goto_1

    .line 1450
    :pswitch_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1451
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1454
    :pswitch_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x27

    if-le v11, v15, :cond_2

    .line 1455
    const/16 v11, 0x27

    goto/16 :goto_1

    .line 1458
    :pswitch_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x28

    if-le v11, v15, :cond_2

    .line 1459
    const/16 v11, 0x28

    goto/16 :goto_1

    .line 1462
    :pswitch_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1463
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1466
    :pswitch_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 1467
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 1470
    :pswitch_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2a

    if-le v11, v15, :cond_2

    .line 1471
    const/16 v11, 0x2a

    goto/16 :goto_1

    .line 1474
    :pswitch_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1475
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1478
    :pswitch_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2c

    if-le v11, v15, :cond_2

    .line 1479
    const/16 v11, 0x2c

    goto/16 :goto_1

    .line 1482
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2d

    if-le v11, v15, :cond_2

    .line 1483
    const/16 v11, 0x2d

    goto/16 :goto_1

    .line 1486
    :pswitch_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2e

    if-le v11, v15, :cond_2

    .line 1487
    const/16 v11, 0x2e

    goto/16 :goto_1

    .line 1490
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1492
    const/16 v15, 0x36

    if-le v11, v15, :cond_19

    .line 1493
    const/16 v11, 0x36

    .line 1494
    :cond_19
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1497
    :pswitch_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1498
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1501
    :pswitch_1a
    const-wide v15, -0x400002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1502
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1505
    :pswitch_1b
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1506
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1509
    :pswitch_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3c

    if-le v11, v15, :cond_2

    .line 1510
    const/16 v11, 0x3c

    goto/16 :goto_1

    .line 1513
    :pswitch_1d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1514
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1517
    :pswitch_1e
    const-wide v15, -0x8000002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1518
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1521
    :pswitch_1f
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1522
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1525
    :pswitch_20
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3c

    if-le v11, v15, :cond_2

    .line 1526
    const/16 v11, 0x3c

    goto/16 :goto_1

    .line 1529
    :pswitch_21
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1530
    const/4 v15, 0x3

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_1

    .line 1533
    :pswitch_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1534
    const/16 v15, 0x32

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1537
    :pswitch_23
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1538
    const/16 v15, 0x32

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1541
    :pswitch_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1542
    const/16 v15, 0xe

    const/16 v16, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1545
    :pswitch_25
    const-wide v15, -0x840000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1546
    const/16 v15, 0x35

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1549
    :pswitch_26
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1550
    const/16 v15, 0x35

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1553
    :pswitch_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x2

    if-le v11, v15, :cond_2

    .line 1554
    const/4 v11, 0x2

    goto/16 :goto_1

    .line 1557
    :pswitch_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1559
    const/4 v15, 0x3

    if-le v11, v15, :cond_1a

    .line 1560
    const/4 v11, 0x3

    .line 1561
    :cond_1a
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1564
    :pswitch_29
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1566
    const/4 v15, 0x3

    if-le v11, v15, :cond_1b

    .line 1567
    const/4 v11, 0x3

    .line 1568
    :cond_1b
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1571
    :pswitch_2a
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 1572
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 1575
    :pswitch_2b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 1576
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 1579
    :pswitch_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1580
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3a

    aput v17, v15, v16

    goto/16 :goto_1

    .line 1583
    :pswitch_2d
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1585
    const/16 v15, 0x3a

    if-le v11, v15, :cond_1c

    .line 1586
    const/16 v11, 0x3a

    .line 1587
    :cond_1c
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 1590
    :pswitch_2e
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1592
    const/16 v15, 0x3a

    if-le v11, v15, :cond_1d

    .line 1593
    const/16 v11, 0x3a

    .line 1594
    :cond_1d
    const/16 v15, 0x47

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1597
    :pswitch_2f
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1598
    const/16 v15, 0x48

    const/16 v16, 0x49

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 1601
    :pswitch_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1602
    const/16 v15, 0x4a

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1605
    :pswitch_31
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 1607
    const/16 v15, 0x3b

    if-le v11, v15, :cond_1e

    .line 1608
    const/16 v11, 0x3b

    .line 1609
    :cond_1e
    const/16 v15, 0x4a

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 1615
    .end local v12    # "l":J
    :cond_1f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_35

    .line 1617
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v12, v15, v17

    .line 1620
    .restart local v12    # "l":J
    :cond_20
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 1823
    :cond_21
    :goto_5
    :pswitch_32
    if-ne v10, v14, :cond_20

    goto/16 :goto_2

    .line 1623
    :pswitch_33
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_24

    .line 1625
    const/16 v15, 0x36

    if-le v11, v15, :cond_22

    .line 1626
    const/16 v11, 0x36

    .line 1627
    :cond_22
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 1631
    :cond_23
    :goto_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_25

    .line 1632
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 1629
    :cond_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_23

    .line 1630
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto :goto_6

    .line 1633
    :cond_25
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_26

    .line 1634
    const/16 v15, 0x16

    const/16 v16, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 1635
    :cond_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_27

    .line 1636
    const/16 v15, 0x18

    const/16 v16, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 1637
    :cond_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 1638
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1639
    :cond_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_29

    .line 1640
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1641
    :cond_29
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 1642
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1643
    :cond_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2b

    .line 1644
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1645
    :cond_2b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1646
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1649
    :pswitch_34
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2d

    .line 1651
    const/16 v15, 0x36

    if-le v11, v15, :cond_2c

    .line 1652
    const/16 v11, 0x36

    .line 1653
    :cond_2c
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 1655
    :cond_2d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2e

    .line 1656
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1657
    :cond_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1659
    const/16 v15, 0x24

    if-le v11, v15, :cond_21

    .line 1660
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 1664
    :pswitch_35
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_30

    .line 1666
    const/16 v15, 0x36

    if-le v11, v15, :cond_2f

    .line 1667
    const/16 v11, 0x36

    .line 1668
    :cond_2f
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 1670
    :cond_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x71

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1672
    const/16 v15, 0x23

    if-le v11, v15, :cond_21

    .line 1673
    const/16 v11, 0x23

    goto/16 :goto_5

    .line 1677
    :pswitch_36
    const/4 v15, 0x1

    if-le v11, v15, :cond_31

    .line 1678
    const/4 v11, 0x1

    .line 1679
    :cond_31
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1682
    :pswitch_37
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x21

    if-le v11, v15, :cond_21

    .line 1683
    const/16 v11, 0x21

    goto/16 :goto_5

    .line 1686
    :pswitch_38
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1687
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1690
    :pswitch_39
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1691
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1694
    :pswitch_3a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x22

    if-le v11, v15, :cond_21

    .line 1695
    const/16 v11, 0x22

    goto/16 :goto_5

    .line 1698
    :pswitch_3b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1699
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1702
    :pswitch_3c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x72

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x22

    if-le v11, v15, :cond_21

    .line 1703
    const/16 v11, 0x22

    goto/16 :goto_5

    .line 1706
    :pswitch_3d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1707
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1710
    :pswitch_3e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1711
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1714
    :pswitch_3f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x2c

    if-le v11, v15, :cond_21

    .line 1715
    const/16 v11, 0x2c

    goto/16 :goto_5

    .line 1718
    :pswitch_40
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1719
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1c

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1722
    :pswitch_41
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1723
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1726
    :pswitch_42
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x76

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x2d

    if-le v11, v15, :cond_21

    .line 1727
    const/16 v11, 0x2d

    goto/16 :goto_5

    .line 1730
    :pswitch_43
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x69

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1731
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x20

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1734
    :pswitch_44
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1735
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1738
    :pswitch_45
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_21

    .line 1740
    const/16 v15, 0x36

    if-le v11, v15, :cond_32

    .line 1741
    const/16 v11, 0x36

    .line 1742
    :cond_32
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 1745
    :pswitch_46
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_21

    .line 1747
    const/16 v15, 0x36

    if-le v11, v15, :cond_33

    .line 1748
    const/16 v11, 0x36

    .line 1749
    :cond_33
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 1752
    :pswitch_47
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_21

    .line 1753
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 1756
    :pswitch_48
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1757
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x29

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1760
    :pswitch_49
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 1763
    :pswitch_4a
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_21

    .line 1764
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 1767
    :pswitch_4b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1768
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1771
    :pswitch_4c
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 1774
    :pswitch_4d
    const/16 v15, 0x32

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 1778
    :pswitch_4e
    const/16 v15, 0x35

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 1781
    :pswitch_4f
    const/4 v15, 0x3

    if-le v11, v15, :cond_34

    .line 1782
    const/4 v11, 0x3

    .line 1783
    :cond_34
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1786
    :pswitch_50
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1787
    const/16 v15, 0x18

    const/16 v16, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1790
    :pswitch_51
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x2e

    if-le v11, v15, :cond_21

    .line 1791
    const/16 v11, 0x2e

    goto/16 :goto_5

    .line 1794
    :pswitch_52
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1795
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 1798
    :pswitch_53
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1799
    const/16 v15, 0x16

    const/16 v16, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1802
    :pswitch_54
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x27

    if-le v11, v15, :cond_21

    .line 1803
    const/16 v11, 0x27

    goto/16 :goto_5

    .line 1806
    :pswitch_55
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x28

    if-le v11, v15, :cond_21

    .line 1807
    const/16 v11, 0x28

    goto/16 :goto_5

    .line 1810
    :pswitch_56
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 1811
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 1814
    :pswitch_57
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x29

    if-le v11, v15, :cond_21

    .line 1815
    const/16 v11, 0x29

    goto/16 :goto_5

    .line 1818
    :pswitch_58
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x2a

    if-le v11, v15, :cond_21

    .line 1819
    const/16 v11, 0x2a

    goto/16 :goto_5

    .line 1827
    .end local v12    # "l":J
    :cond_35
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shr-int/lit8 v2, v15, 0x8

    .line 1828
    .local v2, "hiByte":I
    shr-int/lit8 v3, v2, 0x6

    .line 1829
    .local v3, "i1":I
    const-wide/16 v15, 0x1

    and-int/lit8 v17, v2, 0x3f

    shl-long v5, v15, v17

    .line 1830
    .local v5, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v4, v15, 0x6

    .line 1831
    .local v4, "i2":I
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v7, v15, v17

    .line 1834
    .local v7, "l2":J
    :cond_36
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 1871
    :cond_37
    :goto_7
    if-ne v10, v14, :cond_36

    goto/16 :goto_2

    .line 1837
    :sswitch_0
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 1839
    const/4 v15, 0x1

    if-le v11, v15, :cond_38

    .line 1840
    const/4 v11, 0x1

    .line 1841
    :cond_38
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 1845
    :sswitch_1
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 1846
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 1850
    :sswitch_2
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 1851
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 1854
    :sswitch_3
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 1855
    const/16 v15, 0x32

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 1859
    :sswitch_4
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 1860
    const/16 v15, 0x35

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 1863
    :sswitch_5
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 1865
    const/4 v15, 0x3

    if-le v11, v15, :cond_39

    .line 1866
    const/4 v11, 0x3

    .line 1867
    :cond_39
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 1882
    .end local v2    # "hiByte":I
    .end local v3    # "i1":I
    .end local v4    # "i2":I
    .end local v5    # "l1":J
    .end local v7    # "l2":J
    :cond_3a
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v15}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1883
    :catch_0
    move-exception v9

    .local v9, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 1329
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_3
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_2
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
    .end packed-switch

    .line 1620
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_36
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_33
        :pswitch_32
        :pswitch_32
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_32
        :pswitch_32
        :pswitch_35
        :pswitch_3e
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_32
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_32
        :pswitch_45
        :pswitch_46
        :pswitch_32
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_32
        :pswitch_32
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_4d
        :pswitch_32
        :pswitch_4e
        :pswitch_4e
        :pswitch_32
        :pswitch_32
        :pswitch_4f
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_50
        :pswitch_34
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
    .end packed-switch

    .line 1834
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x27 -> :sswitch_1
        0x29 -> :sswitch_1
        0x2c -> :sswitch_2
        0x2e -> :sswitch_2
        0x32 -> :sswitch_3
        0x34 -> :sswitch_4
        0x35 -> :sswitch_4
        0x38 -> :sswitch_5
    .end sparse-switch
.end method

.method private jjMoveNfa_2(II)I
    .locals 19
    .param p1, "startState"    # I
    .param p2, "curPos"    # I

    .prologue
    .line 386
    const/4 v14, 0x0

    .line 387
    .local v14, "startsAt":I
    const/16 v15, 0x4b

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    .line 388
    const/4 v10, 0x1

    .line 389
    .local v10, "i":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    const/16 v16, 0x0

    aput p1, v15, v16

    .line 390
    const v11, 0x7fffffff

    .line 393
    .local v11, "kind":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjround:I

    const v16, 0x7fffffff

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 394
    invoke-direct/range {p0 .. p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInitRounds()V

    .line 395
    :cond_0
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v15, v0, :cond_1f

    .line 397
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    shl-long v12, v15, v17

    .line 400
    .local v12, "l":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_0

    .line 684
    :cond_2
    :goto_1
    :pswitch_0
    if-ne v10, v14, :cond_1

    .line 944
    .end local v12    # "l":J
    :goto_2
    const v15, 0x7fffffff

    if-eq v11, v15, :cond_3

    .line 946
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 947
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 948
    const v11, 0x7fffffff

    .line 950
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 951
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    rsub-int/lit8 v14, v14, 0x4b

    if-ne v10, v14, :cond_3a

    .line 954
    :goto_3
    return p2

    .line 403
    .restart local v12    # "l":J
    :pswitch_1
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_6

    .line 405
    const/16 v15, 0x3a

    if-le v11, v15, :cond_4

    .line 406
    const/16 v11, 0x3a

    .line 407
    :cond_4
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    .line 441
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_12

    .line 443
    const/16 v15, 0x2d

    if-le v11, v15, :cond_2

    .line 444
    const/16 v11, 0x2d

    goto :goto_1

    .line 409
    :cond_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 410
    const/4 v15, 0x3

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_4

    .line 411
    :cond_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 412
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 413
    :cond_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 414
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_4

    .line 415
    :cond_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    .line 417
    const/16 v15, 0x36

    if-le v11, v15, :cond_a

    .line 418
    const/16 v11, 0x36

    .line 419
    :cond_a
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto :goto_4

    .line 421
    :cond_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_c

    .line 423
    const/16 v15, 0x2e

    if-le v11, v15, :cond_5

    .line 424
    const/16 v11, 0x2e

    goto :goto_4

    .line 426
    :cond_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    .line 428
    const/16 v15, 0x2c

    if-le v11, v15, :cond_5

    .line 429
    const/16 v11, 0x2c

    goto/16 :goto_4

    .line 431
    :cond_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 432
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_4

    .line 433
    :cond_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 434
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_4

    .line 435
    :cond_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_10

    .line 436
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_4

    .line 437
    :cond_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 438
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_4

    .line 439
    :cond_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 440
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x0

    aput v17, v15, v16

    goto/16 :goto_4

    .line 446
    :cond_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_13

    .line 448
    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 449
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 451
    :cond_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_14

    .line 453
    const/16 v15, 0x27

    if-le v11, v15, :cond_2

    .line 454
    const/16 v11, 0x27

    goto/16 :goto_1

    .line 456
    :cond_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 457
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 461
    :pswitch_2
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 463
    const/16 v15, 0x36

    if-le v11, v15, :cond_15

    .line 464
    const/16 v11, 0x36

    .line 465
    :cond_15
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 468
    :pswitch_3
    const-wide v15, 0x3ff001000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 470
    const/16 v15, 0x36

    if-le v11, v15, :cond_16

    .line 471
    const/16 v11, 0x36

    .line 472
    :cond_16
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 475
    :pswitch_4
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x23

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 477
    const/4 v15, 0x1

    if-le v11, v15, :cond_17

    .line 478
    const/4 v11, 0x1

    .line 479
    :cond_17
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 482
    :pswitch_5
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 484
    const/4 v15, 0x1

    if-le v11, v15, :cond_18

    .line 485
    const/4 v11, 0x1

    .line 486
    :cond_18
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 489
    :pswitch_6
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 490
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 493
    :pswitch_7
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x1

    if-le v11, v15, :cond_2

    .line 494
    const/4 v11, 0x1

    goto/16 :goto_1

    .line 497
    :pswitch_8
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 498
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3

    aput v17, v15, v16

    goto/16 :goto_1

    .line 501
    :pswitch_9
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x21

    if-le v11, v15, :cond_2

    .line 502
    const/16 v11, 0x21

    goto/16 :goto_1

    .line 505
    :pswitch_a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x26

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 506
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x6

    aput v17, v15, v16

    goto/16 :goto_1

    .line 509
    :pswitch_b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x23

    if-le v11, v15, :cond_2

    .line 510
    const/16 v11, 0x23

    goto/16 :goto_1

    .line 513
    :pswitch_c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 514
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xf

    aput v17, v15, v16

    goto/16 :goto_1

    .line 517
    :pswitch_d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x24

    if-le v11, v15, :cond_2

    .line 518
    const/16 v11, 0x24

    goto/16 :goto_1

    .line 521
    :pswitch_e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 522
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x13

    aput v17, v15, v16

    goto/16 :goto_1

    .line 525
    :pswitch_f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x27

    if-le v11, v15, :cond_2

    .line 526
    const/16 v11, 0x27

    goto/16 :goto_1

    .line 529
    :pswitch_10
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x28

    if-le v11, v15, :cond_2

    .line 530
    const/16 v11, 0x28

    goto/16 :goto_1

    .line 533
    :pswitch_11
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 534
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x16

    aput v17, v15, v16

    goto/16 :goto_1

    .line 537
    :pswitch_12
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x29

    if-le v11, v15, :cond_2

    .line 538
    const/16 v11, 0x29

    goto/16 :goto_1

    .line 541
    :pswitch_13
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3d

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2a

    if-le v11, v15, :cond_2

    .line 542
    const/16 v11, 0x2a

    goto/16 :goto_1

    .line 545
    :pswitch_14
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x3c

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 546
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x19

    aput v17, v15, v16

    goto/16 :goto_1

    .line 549
    :pswitch_15
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x25

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2c

    if-le v11, v15, :cond_2

    .line 550
    const/16 v11, 0x2c

    goto/16 :goto_1

    .line 553
    :pswitch_16
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2d

    if-le v11, v15, :cond_2

    .line 554
    const/16 v11, 0x2d

    goto/16 :goto_1

    .line 557
    :pswitch_17
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x21

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x2e

    if-le v11, v15, :cond_2

    .line 558
    const/16 v11, 0x2e

    goto/16 :goto_1

    .line 561
    :pswitch_18
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x24

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 563
    const/16 v15, 0x36

    if-le v11, v15, :cond_19

    .line 564
    const/16 v11, 0x36

    .line 565
    :cond_19
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 568
    :pswitch_19
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 569
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 572
    :pswitch_1a
    const-wide v15, -0x400002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 573
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 576
    :pswitch_1b
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 577
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 580
    :pswitch_1c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3c

    if-le v11, v15, :cond_2

    .line 581
    const/16 v11, 0x3c

    goto/16 :goto_1

    .line 584
    :pswitch_1d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 585
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 588
    :pswitch_1e
    const-wide v15, -0x8000002401L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 589
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 592
    :pswitch_1f
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 593
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 596
    :pswitch_20
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x27

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/16 v15, 0x3c

    if-le v11, v15, :cond_2

    .line 597
    const/16 v11, 0x3c

    goto/16 :goto_1

    .line 600
    :pswitch_21
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 601
    const/4 v15, 0x3

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_1

    .line 604
    :pswitch_22
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 605
    const/16 v15, 0x32

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 608
    :pswitch_23
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 609
    const/16 v15, 0x32

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 612
    :pswitch_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2a

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 613
    const/16 v15, 0xe

    const/16 v16, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 616
    :pswitch_25
    const-wide v15, -0x840000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 617
    const/16 v15, 0x35

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 620
    :pswitch_26
    const-wide v15, -0x40000000001L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 621
    const/16 v15, 0x35

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 624
    :pswitch_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x2

    if-le v11, v15, :cond_2

    .line 625
    const/4 v11, 0x2

    goto/16 :goto_1

    .line 628
    :pswitch_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 630
    const/4 v15, 0x3

    if-le v11, v15, :cond_1a

    .line 631
    const/4 v11, 0x3

    .line 632
    :cond_1a
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 635
    :pswitch_29
    const-wide/16 v15, -0x2401

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 637
    const/4 v15, 0x3

    if-le v11, v15, :cond_1b

    .line 638
    const/4 v11, 0x3

    .line 639
    :cond_1b
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 642
    :pswitch_2a
    const-wide/16 v15, 0x2400

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 643
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 646
    :pswitch_2b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xa

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v15, 0x3

    if-le v11, v15, :cond_2

    .line 647
    const/4 v11, 0x3

    goto/16 :goto_1

    .line 650
    :pswitch_2c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 651
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3a

    aput v17, v15, v16

    goto/16 :goto_1

    .line 654
    :pswitch_2d
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 656
    const/16 v15, 0x3a

    if-le v11, v15, :cond_1c

    .line 657
    const/16 v11, 0x3a

    .line 658
    :cond_1c
    const/4 v15, 0x0

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_1

    .line 661
    :pswitch_2e
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 663
    const/16 v15, 0x3a

    if-le v11, v15, :cond_1d

    .line 664
    const/16 v11, 0x3a

    .line 665
    :cond_1d
    const/16 v15, 0x47

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 668
    :pswitch_2f
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 669
    const/16 v15, 0x48

    const/16 v16, 0x49

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_1

    .line 672
    :pswitch_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 673
    const/16 v15, 0x4a

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 676
    :pswitch_31
    const-wide/high16 v15, 0x3ff000000000000L

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2

    .line 678
    const/16 v15, 0x3b

    if-le v11, v15, :cond_1e

    .line 679
    const/16 v11, 0x3b

    .line 680
    :cond_1e
    const/16 v15, 0x4a

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_1

    .line 686
    .end local v12    # "l":J
    :cond_1f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x80

    move/from16 v0, v16

    if-ge v15, v0, :cond_35

    .line 688
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v12, v15, v17

    .line 691
    .restart local v12    # "l":J
    :cond_20
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    packed-switch v15, :pswitch_data_1

    .line 894
    :cond_21
    :goto_5
    :pswitch_32
    if-ne v10, v14, :cond_20

    goto/16 :goto_2

    .line 694
    :pswitch_33
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_24

    .line 696
    const/16 v15, 0x36

    if-le v11, v15, :cond_22

    .line 697
    const/16 v11, 0x36

    .line 698
    :cond_22
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 702
    :cond_23
    :goto_6
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_25

    .line 703
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 700
    :cond_24
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_23

    .line 701
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto :goto_6

    .line 704
    :cond_25
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_26

    .line 705
    const/16 v15, 0x16

    const/16 v16, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 706
    :cond_26
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_27

    .line 707
    const/16 v15, 0x18

    const/16 v16, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_5

    .line 708
    :cond_27
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_28

    .line 709
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 710
    :cond_28
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_29

    .line 711
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 712
    :cond_29
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_2a

    .line 713
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 714
    :cond_2a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2b

    .line 715
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 716
    :cond_2b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 717
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 720
    :pswitch_34
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_2d

    .line 722
    const/16 v15, 0x36

    if-le v11, v15, :cond_2c

    .line 723
    const/16 v11, 0x36

    .line 724
    :cond_2c
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 726
    :cond_2d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_2e

    .line 727
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 728
    :cond_2e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 730
    const/16 v15, 0x24

    if-le v11, v15, :cond_21

    .line 731
    const/16 v11, 0x24

    goto/16 :goto_5

    .line 735
    :pswitch_35
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_30

    .line 737
    const/16 v15, 0x36

    if-le v11, v15, :cond_2f

    .line 738
    const/16 v11, 0x36

    .line 739
    :cond_2f
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    .line 741
    :cond_30
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x71

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 743
    const/16 v15, 0x23

    if-le v11, v15, :cond_21

    .line 744
    const/16 v11, 0x23

    goto/16 :goto_5

    .line 748
    :pswitch_36
    const/4 v15, 0x1

    if-le v11, v15, :cond_31

    .line 749
    const/4 v11, 0x1

    .line 750
    :cond_31
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 753
    :pswitch_37
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x21

    if-le v11, v15, :cond_21

    .line 754
    const/16 v11, 0x21

    goto/16 :goto_5

    .line 757
    :pswitch_38
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 758
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x8

    aput v17, v15, v16

    goto/16 :goto_5

    .line 761
    :pswitch_39
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x61

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 762
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x9

    aput v17, v15, v16

    goto/16 :goto_5

    .line 765
    :pswitch_3a
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x22

    if-le v11, v15, :cond_21

    .line 766
    const/16 v11, 0x22

    goto/16 :goto_5

    .line 769
    :pswitch_3b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x7c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 770
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xb

    aput v17, v15, v16

    goto/16 :goto_5

    .line 773
    :pswitch_3c
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x72

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x22

    if-le v11, v15, :cond_21

    .line 774
    const/16 v11, 0x22

    goto/16 :goto_5

    .line 777
    :pswitch_3d
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 778
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0xd

    aput v17, v15, v16

    goto/16 :goto_5

    .line 781
    :pswitch_3e
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 782
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x11

    aput v17, v15, v16

    goto/16 :goto_5

    .line 785
    :pswitch_3f
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x2c

    if-le v11, v15, :cond_21

    .line 786
    const/16 v11, 0x2c

    goto/16 :goto_5

    .line 789
    :pswitch_40
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 790
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1c

    aput v17, v15, v16

    goto/16 :goto_5

    .line 793
    :pswitch_41
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6d

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 794
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x1d

    aput v17, v15, v16

    goto/16 :goto_5

    .line 797
    :pswitch_42
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x76

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x2d

    if-le v11, v15, :cond_21

    .line 798
    const/16 v11, 0x2d

    goto/16 :goto_5

    .line 801
    :pswitch_43
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x69

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 802
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x20

    aput v17, v15, v16

    goto/16 :goto_5

    .line 805
    :pswitch_44
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x64

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 806
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x21

    aput v17, v15, v16

    goto/16 :goto_5

    .line 809
    :pswitch_45
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_21

    .line 811
    const/16 v15, 0x36

    if-le v11, v15, :cond_32

    .line 812
    const/16 v11, 0x36

    .line 813
    :cond_32
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 816
    :pswitch_46
    const-wide v15, 0x7fffffe87fffffeL

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_21

    .line 818
    const/16 v15, 0x36

    if-le v11, v15, :cond_33

    .line 819
    const/16 v11, 0x36

    .line 820
    :cond_33
    const/16 v15, 0x25

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAdd(I)V

    goto/16 :goto_5

    .line 823
    :pswitch_47
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_21

    .line 824
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 827
    :pswitch_48
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 828
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x29

    aput v17, v15, v16

    goto/16 :goto_5

    .line 831
    :pswitch_49
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 834
    :pswitch_4a
    const-wide/32 v15, -0x10000001

    and-long/2addr v15, v12

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_21

    .line 835
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 838
    :pswitch_4b
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x5c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 839
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x2e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 842
    :pswitch_4c
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto/16 :goto_5

    .line 845
    :pswitch_4d
    const/16 v15, 0x32

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 849
    :pswitch_4e
    const/16 v15, 0x35

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto/16 :goto_5

    .line 852
    :pswitch_4f
    const/4 v15, 0x3

    if-le v11, v15, :cond_34

    .line 853
    const/4 v11, 0x3

    .line 854
    :cond_34
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 857
    :pswitch_50
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6e

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 858
    const/16 v15, 0x18

    const/16 v16, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 861
    :pswitch_51
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x2e

    if-le v11, v15, :cond_21

    .line 862
    const/16 v11, 0x2e

    goto/16 :goto_5

    .line 865
    :pswitch_52
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6f

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 866
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    const/16 v17, 0x3e

    aput v17, v15, v16

    goto/16 :goto_5

    .line 869
    :pswitch_53
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x67

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 870
    const/16 v15, 0x16

    const/16 v16, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 873
    :pswitch_54
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x27

    if-le v11, v15, :cond_21

    .line 874
    const/16 v11, 0x27

    goto/16 :goto_5

    .line 877
    :pswitch_55
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x28

    if-le v11, v15, :cond_21

    .line 878
    const/16 v11, 0x28

    goto/16 :goto_5

    .line 881
    :pswitch_56
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x6c

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 882
    const/16 v15, 0x14

    const/16 v16, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto/16 :goto_5

    .line 885
    :pswitch_57
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x74

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x29

    if-le v11, v15, :cond_21

    .line 886
    const/16 v11, 0x29

    goto/16 :goto_5

    .line 889
    :pswitch_58
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v16, 0x65

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    const/16 v15, 0x2a

    if-le v11, v15, :cond_21

    .line 890
    const/16 v11, 0x2a

    goto/16 :goto_5

    .line 898
    .end local v12    # "l":J
    :cond_35
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shr-int/lit8 v2, v15, 0x8

    .line 899
    .local v2, "hiByte":I
    shr-int/lit8 v3, v2, 0x6

    .line 900
    .local v3, "i1":I
    const-wide/16 v15, 0x1

    and-int/lit8 v17, v2, 0x3f

    shl-long v5, v15, v17

    .line 901
    .local v5, "l1":J
    move-object/from16 v0, p0

    iget-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    and-int/lit16 v15, v15, 0xff

    shr-int/lit8 v4, v15, 0x6

    .line 902
    .local v4, "i2":I
    const-wide/16 v15, 0x1

    move-object/from16 v0, p0

    iget-char v0, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x3f

    shl-long v7, v15, v17

    .line 905
    .local v7, "l2":J
    :cond_36
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstateSet:[I

    add-int/lit8 v10, v10, -0x1

    aget v15, v15, v10

    sparse-switch v15, :sswitch_data_0

    .line 942
    :cond_37
    :goto_7
    if-ne v10, v14, :cond_36

    goto/16 :goto_2

    .line 908
    :sswitch_0
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 910
    const/4 v15, 0x1

    if-le v11, v15, :cond_38

    .line 911
    const/4 v11, 0x1

    .line 912
    :cond_38
    const/16 v15, 0xb

    const/16 v16, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 916
    :sswitch_1
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 917
    const/16 v15, 0x8

    const/16 v16, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 921
    :sswitch_2
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_1(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 922
    const/4 v15, 0x5

    const/16 v16, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddStates(II)V

    goto :goto_7

    .line 925
    :sswitch_3
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 926
    const/16 v15, 0x32

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 930
    :sswitch_4
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 931
    const/16 v15, 0x35

    const/16 v16, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCheckNAddTwoStates(II)V

    goto :goto_7

    .line 934
    :sswitch_5
    invoke-static/range {v2 .. v8}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjCanMove_0(IIIJJ)Z

    move-result v15

    if-eqz v15, :cond_37

    .line 936
    const/4 v15, 0x3

    if-le v11, v15, :cond_39

    .line 937
    const/4 v11, 0x3

    .line 938
    :cond_39
    const/16 v15, 0x11

    const/16 v16, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjAddStates(II)V

    goto :goto_7

    .line 953
    .end local v2    # "hiByte":I
    .end local v3    # "i1":I
    .end local v4    # "i2":I
    .end local v5    # "l1":J
    .end local v7    # "l2":J
    :cond_3a
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v15}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v15

    move-object/from16 v0, p0

    iput-char v15, v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 954
    :catch_0
    move-exception v9

    .local v9, "e":Ljava/io/IOException;
    goto/16 :goto_3

    .line 400
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_3
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_2
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
    .end packed-switch

    .line 691
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_36
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_33
        :pswitch_32
        :pswitch_32
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_32
        :pswitch_32
        :pswitch_35
        :pswitch_3e
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_32
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_32
        :pswitch_45
        :pswitch_46
        :pswitch_32
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_32
        :pswitch_32
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_4d
        :pswitch_32
        :pswitch_4e
        :pswitch_4e
        :pswitch_32
        :pswitch_32
        :pswitch_4f
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_50
        :pswitch_34
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
    .end packed-switch

    .line 905
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x27 -> :sswitch_1
        0x29 -> :sswitch_1
        0x2c -> :sswitch_2
        0x2e -> :sswitch_2
        0x32 -> :sswitch_3
        0x34 -> :sswitch_4
        0x35 -> :sswitch_4
        0x38 -> :sswitch_5
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_0()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1985
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 2044
    const/4 v0, 0x5

    invoke-direct {p0, v0, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_0(II)I

    move-result v0

    :goto_0
    return v0

    .line 1988
    :sswitch_0
    const-wide v0, 0x4000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto :goto_0

    .line 1990
    :sswitch_1
    const/16 v0, 0x33

    const/4 v1, 0x6

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v0

    goto :goto_0

    .line 1992
    :sswitch_2
    const/16 v0, 0x15

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1994
    :sswitch_3
    const/16 v0, 0x16

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1996
    :sswitch_4
    const/16 v0, 0x31

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1998
    :sswitch_5
    const/16 v0, 0x2f

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2000
    :sswitch_6
    const/16 v0, 0x1d

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2002
    :sswitch_7
    const/16 v0, 0x30

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2004
    :sswitch_8
    const/16 v0, 0x1e

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2006
    :sswitch_9
    const/16 v0, 0x1c

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2008
    :sswitch_a
    const/16 v0, 0x1b

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2010
    :sswitch_b
    const/16 v0, 0x2b

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2011
    const-wide v0, 0x2000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto :goto_0

    .line 2013
    :sswitch_c
    const/16 v0, 0x1f

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2014
    const-wide v0, 0x100000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto :goto_0

    .line 2016
    :sswitch_d
    const/16 v0, 0x19

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2018
    :sswitch_e
    const/16 v0, 0x1a

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 2020
    :sswitch_f
    const/16 v0, 0x35

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 2022
    :sswitch_10
    const-wide/32 v0, 0x8400

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2024
    :sswitch_11
    const-wide/32 v0, 0x81800

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2026
    :sswitch_12
    const-wide/16 v0, 0x200

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2028
    :sswitch_13
    const-wide/32 v0, 0x24000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2030
    :sswitch_14
    const-wide/32 v0, 0x10000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2032
    :sswitch_15
    const-wide/32 v0, 0x40000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2034
    :sswitch_16
    const-wide/16 v0, 0x2000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_0(J)I

    move-result v0

    goto/16 :goto_0

    .line 2036
    :sswitch_17
    const/16 v0, 0x17

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 2038
    :sswitch_18
    const/16 v0, 0x34

    const/16 v1, 0xb

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v0

    goto/16 :goto_0

    .line 2040
    :sswitch_19
    const/16 v0, 0x18

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 2042
    :sswitch_1a
    const/16 v0, 0x32

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1985
    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x26 -> :sswitch_1
        0x28 -> :sswitch_2
        0x29 -> :sswitch_3
        0x2a -> :sswitch_4
        0x2b -> :sswitch_5
        0x2c -> :sswitch_6
        0x2d -> :sswitch_7
        0x2e -> :sswitch_8
        0x3a -> :sswitch_9
        0x3b -> :sswitch_a
        0x3d -> :sswitch_b
        0x3f -> :sswitch_c
        0x5b -> :sswitch_d
        0x5d -> :sswitch_e
        0x5e -> :sswitch_f
        0x65 -> :sswitch_10
        0x66 -> :sswitch_11
        0x69 -> :sswitch_12
        0x6e -> :sswitch_13
        0x73 -> :sswitch_14
        0x74 -> :sswitch_15
        0x77 -> :sswitch_16
        0x7b -> :sswitch_17
        0x7c -> :sswitch_18
        0x7d -> :sswitch_19
        0x7e -> :sswitch_1a
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_1()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1056
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 1115
    const/4 v0, 0x5

    invoke-direct {p0, v0, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    :goto_0
    return v0

    .line 1059
    :sswitch_0
    const-wide v0, 0x4000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 1061
    :sswitch_1
    const/16 v0, 0x33

    const/4 v1, 0x6

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v0

    goto :goto_0

    .line 1063
    :sswitch_2
    const/16 v0, 0x15

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1065
    :sswitch_3
    const/16 v0, 0x16

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1067
    :sswitch_4
    const/16 v0, 0x31

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1069
    :sswitch_5
    const/16 v0, 0x2f

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1071
    :sswitch_6
    const/16 v0, 0x1d

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1073
    :sswitch_7
    const/16 v0, 0x30

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1075
    :sswitch_8
    const/16 v0, 0x1e

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1077
    :sswitch_9
    const/16 v0, 0x1c

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1079
    :sswitch_a
    const/16 v0, 0x1b

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1081
    :sswitch_b
    const/16 v0, 0x2b

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1082
    const-wide v0, 0x2000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 1084
    :sswitch_c
    const/16 v0, 0x1f

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1085
    const-wide v0, 0x100000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto :goto_0

    .line 1087
    :sswitch_d
    const/16 v0, 0x19

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1089
    :sswitch_e
    const/16 v0, 0x1a

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 1091
    :sswitch_f
    const/16 v0, 0x35

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1093
    :sswitch_10
    const-wide/32 v0, 0x8400

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1095
    :sswitch_11
    const-wide/32 v0, 0x81800

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1097
    :sswitch_12
    const-wide/32 v0, 0x100200

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1099
    :sswitch_13
    const-wide/32 v0, 0x24000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1101
    :sswitch_14
    const-wide/32 v0, 0x10000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1103
    :sswitch_15
    const-wide/32 v0, 0x40000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1105
    :sswitch_16
    const-wide/16 v0, 0x2000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_1(J)I

    move-result v0

    goto/16 :goto_0

    .line 1107
    :sswitch_17
    const/16 v0, 0x17

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1109
    :sswitch_18
    const/16 v0, 0x34

    const/16 v1, 0xb

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v0

    goto/16 :goto_0

    .line 1111
    :sswitch_19
    const/16 v0, 0x18

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1113
    :sswitch_1a
    const/16 v0, 0x32

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 1056
    nop

    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x26 -> :sswitch_1
        0x28 -> :sswitch_2
        0x29 -> :sswitch_3
        0x2a -> :sswitch_4
        0x2b -> :sswitch_5
        0x2c -> :sswitch_6
        0x2d -> :sswitch_7
        0x2e -> :sswitch_8
        0x3a -> :sswitch_9
        0x3b -> :sswitch_a
        0x3d -> :sswitch_b
        0x3f -> :sswitch_c
        0x5b -> :sswitch_d
        0x5d -> :sswitch_e
        0x5e -> :sswitch_f
        0x65 -> :sswitch_10
        0x66 -> :sswitch_11
        0x69 -> :sswitch_12
        0x6e -> :sswitch_13
        0x73 -> :sswitch_14
        0x74 -> :sswitch_15
        0x77 -> :sswitch_16
        0x7b -> :sswitch_17
        0x7c -> :sswitch_18
        0x7d -> :sswitch_19
        0x7e -> :sswitch_1a
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa0_2()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 119
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v0, :sswitch_data_0

    .line 178
    const/4 v0, 0x5

    invoke-direct {p0, v0, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    :goto_0
    return v0

    .line 122
    :sswitch_0
    const-wide v0, 0x4000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 124
    :sswitch_1
    const/16 v0, 0x33

    const/4 v1, 0x6

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v0

    goto :goto_0

    .line 126
    :sswitch_2
    const/16 v0, 0x15

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 128
    :sswitch_3
    const/16 v0, 0x16

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 130
    :sswitch_4
    const/16 v0, 0x31

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 132
    :sswitch_5
    const/16 v0, 0x2f

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 134
    :sswitch_6
    const/16 v0, 0x1d

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 136
    :sswitch_7
    const/16 v0, 0x30

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 138
    :sswitch_8
    const/16 v0, 0x1e

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 140
    :sswitch_9
    const/16 v0, 0x1c

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 142
    :sswitch_a
    const/16 v0, 0x1b

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 144
    :sswitch_b
    const/16 v0, 0x2b

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 145
    const-wide v0, 0x2000000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 147
    :sswitch_c
    const/16 v0, 0x1f

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 148
    const-wide v0, 0x100000000L

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto :goto_0

    .line 150
    :sswitch_d
    const/16 v0, 0x19

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 152
    :sswitch_e
    const/16 v0, 0x1a

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto :goto_0

    .line 154
    :sswitch_f
    const/16 v0, 0x35

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 156
    :sswitch_10
    const-wide/32 v0, 0x8400

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 158
    :sswitch_11
    const-wide/32 v0, 0x81800

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 160
    :sswitch_12
    const-wide/16 v0, 0x200

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 162
    :sswitch_13
    const-wide/32 v0, 0x24000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 164
    :sswitch_14
    const-wide/32 v0, 0x10000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 166
    :sswitch_15
    const-wide/32 v0, 0x40000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 168
    :sswitch_16
    const-wide/16 v0, 0x2000

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa1_2(J)I

    move-result v0

    goto/16 :goto_0

    .line 170
    :sswitch_17
    const/16 v0, 0x17

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 172
    :sswitch_18
    const/16 v0, 0x34

    const/16 v1, 0xb

    invoke-direct {p0, v2, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v0

    goto/16 :goto_0

    .line 174
    :sswitch_19
    const/16 v0, 0x18

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 176
    :sswitch_1a
    const/16 v0, 0x32

    invoke-direct {p0, v2, v0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v0

    goto/16 :goto_0

    .line 119
    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x26 -> :sswitch_1
        0x28 -> :sswitch_2
        0x29 -> :sswitch_3
        0x2a -> :sswitch_4
        0x2b -> :sswitch_5
        0x2c -> :sswitch_6
        0x2d -> :sswitch_7
        0x2e -> :sswitch_8
        0x3a -> :sswitch_9
        0x3b -> :sswitch_a
        0x3d -> :sswitch_b
        0x3f -> :sswitch_c
        0x5b -> :sswitch_d
        0x5d -> :sswitch_e
        0x5e -> :sswitch_f
        0x65 -> :sswitch_10
        0x66 -> :sswitch_11
        0x69 -> :sswitch_12
        0x6e -> :sswitch_13
        0x73 -> :sswitch_14
        0x74 -> :sswitch_15
        0x77 -> :sswitch_16
        0x7b -> :sswitch_17
        0x7c -> :sswitch_18
        0x7d -> :sswitch_19
        0x7e -> :sswitch_1a
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_0(J)I
    .locals 8
    .param p1, "active0"    # J

    .prologue
    const/16 v7, 0x25

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 2049
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2054
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 2091
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 2050
    :catch_0
    move-exception v0

    .line 2051
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2057
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide v2, 0x100000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2058
    const/16 v2, 0x20

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 2061
    :sswitch_1
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2063
    :sswitch_2
    const-wide/16 v1, 0x4000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2065
    :sswitch_3
    const-wide/16 v2, 0x200

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2066
    const/16 v2, 0x9

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2069
    :sswitch_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2071
    :sswitch_5
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2073
    :sswitch_6
    const-wide/16 v1, 0x400

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2075
    :sswitch_7
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2077
    :sswitch_8
    const-wide/16 v1, 0x1800

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2079
    :sswitch_9
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2081
    :sswitch_a
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2083
    :sswitch_b
    const-wide v2, 0x2000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2084
    invoke-direct {p0, v1, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 2085
    :cond_1
    const-wide v2, 0x4000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2086
    const/16 v2, 0x26

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto/16 :goto_0

    .line 2054
    nop

    :sswitch_data_0
    .sparse-switch
        0x3a -> :sswitch_0
        0x61 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x68 -> :sswitch_4
        0x69 -> :sswitch_5
        0x6c -> :sswitch_6
        0x6d -> :sswitch_7
        0x6f -> :sswitch_8
        0x72 -> :sswitch_9
        0x75 -> :sswitch_a
        0x7e -> :sswitch_b
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_1(J)I
    .locals 8
    .param p1, "active0"    # J

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 1120
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1125
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 1166
    :cond_0
    invoke-direct {p0, v7, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 1121
    :catch_0
    move-exception v0

    .line 1122
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v7, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1128
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide v2, 0x100000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1129
    const/16 v2, 0x20

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 1132
    :sswitch_1
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1134
    :sswitch_2
    const-wide/16 v1, 0x4000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1136
    :sswitch_3
    const-wide/16 v2, 0x200

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1137
    const/16 v2, 0x9

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1140
    :sswitch_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1142
    :sswitch_5
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1144
    :sswitch_6
    const-wide/16 v1, 0x400

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1146
    :sswitch_7
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1148
    :sswitch_8
    const-wide/32 v2, 0x100000

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1149
    const/16 v2, 0x14

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1152
    :sswitch_9
    const-wide/16 v1, 0x1800

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1154
    :sswitch_a
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1156
    :sswitch_b
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1158
    :sswitch_c
    const-wide v2, 0x2000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1159
    invoke-direct {p0, v1, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto/16 :goto_0

    .line 1160
    :cond_1
    const-wide v2, 0x4000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1161
    const/16 v2, 0x26

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto/16 :goto_0

    .line 1125
    nop

    :sswitch_data_0
    .sparse-switch
        0x3a -> :sswitch_0
        0x61 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x68 -> :sswitch_4
        0x69 -> :sswitch_5
        0x6c -> :sswitch_6
        0x6d -> :sswitch_7
        0x6e -> :sswitch_8
        0x6f -> :sswitch_9
        0x72 -> :sswitch_a
        0x75 -> :sswitch_b
        0x7e -> :sswitch_c
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa1_2(J)I
    .locals 8
    .param p1, "active0"    # J

    .prologue
    const/16 v7, 0x25

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 183
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 225
    :cond_0
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    :goto_0
    return v1

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 191
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide v2, 0x100000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 192
    const/16 v2, 0x20

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 195
    :sswitch_1
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 197
    :sswitch_2
    const-wide/16 v1, 0x4000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 199
    :sswitch_3
    const-wide/16 v2, 0x200

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 200
    const/16 v2, 0x9

    invoke-direct {p0, v1, v2, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 203
    :sswitch_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 205
    :sswitch_5
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 207
    :sswitch_6
    const-wide/16 v1, 0x400

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 209
    :sswitch_7
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 211
    :sswitch_8
    const-wide/16 v1, 0x1800

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 213
    :sswitch_9
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 215
    :sswitch_a
    const-wide/32 v1, 0x20000

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa2_2(JJ)I

    move-result v1

    goto :goto_0

    .line 217
    :sswitch_b
    const-wide v2, 0x2000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 218
    invoke-direct {p0, v1, v7}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto :goto_0

    .line 219
    :cond_1
    const-wide v2, 0x4000000000L

    and-long/2addr v2, p1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 220
    const/16 v2, 0x26

    invoke-direct {p0, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopAtPos(II)I

    move-result v1

    goto/16 :goto_0

    .line 188
    nop

    :sswitch_data_0
    .sparse-switch
        0x3a -> :sswitch_0
        0x61 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x68 -> :sswitch_4
        0x69 -> :sswitch_5
        0x6c -> :sswitch_6
        0x6d -> :sswitch_7
        0x6f -> :sswitch_8
        0x72 -> :sswitch_9
        0x75 -> :sswitch_a
        0x7e -> :sswitch_b
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa2_0(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x2

    .line 2095
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 2096
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2130
    :goto_0
    return v1

    .line 2097
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2102
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 2130
    :cond_1
    :pswitch_0
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2098
    :catch_0
    move-exception v0

    .line 2099
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2105
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2107
    :pswitch_2
    const-wide/32 v1, 0xa0000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2109
    :pswitch_3
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2111
    :pswitch_4
    const-wide/16 v2, 0x800

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 2113
    const/16 v2, 0xb

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2114
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 2116
    :cond_2
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2118
    :pswitch_5
    const-wide/16 v1, 0x400

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2120
    :pswitch_6
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2122
    :pswitch_7
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2123
    const/16 v2, 0xe

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2126
    :pswitch_8
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2102
    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa2_1(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x2

    .line 1170
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 1171
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1205
    :goto_0
    return v1

    .line 1172
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1177
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 1205
    :cond_1
    :pswitch_0
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1173
    :catch_0
    move-exception v0

    .line 1174
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1180
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1182
    :pswitch_2
    const-wide/32 v1, 0xa0000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1184
    :pswitch_3
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1186
    :pswitch_4
    const-wide/16 v2, 0x800

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1188
    const/16 v2, 0xb

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1189
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 1191
    :cond_2
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1193
    :pswitch_5
    const-wide/16 v1, 0x400

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1195
    :pswitch_6
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1197
    :pswitch_7
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1198
    const/16 v2, 0xe

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1201
    :pswitch_8
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1177
    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa2_2(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x2

    .line 229
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 230
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 264
    :goto_0
    return v1

    .line 231
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 264
    :cond_1
    :pswitch_0
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v6, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 239
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 241
    :pswitch_2
    const-wide/32 v1, 0xa0000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 243
    :pswitch_3
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 245
    :pswitch_4
    const-wide/16 v2, 0x800

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 247
    const/16 v2, 0xb

    iput v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 248
    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 250
    :cond_2
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 252
    :pswitch_5
    const-wide/16 v1, 0x400

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 254
    :pswitch_6
    const-wide/32 v1, 0x40000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 256
    :pswitch_7
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 257
    const/16 v2, 0xe

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 260
    :pswitch_8
    const-wide/32 v1, 0x10000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa3_2(JJ)I

    move-result v1

    goto :goto_0

    .line 236
    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa3_0(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x2

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x3

    .line 2134
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 2135
    const/4 v1, 0x1

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2162
    :goto_0
    return v1

    .line 2136
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2141
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 2162
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2137
    :catch_0
    move-exception v0

    .line 2138
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2144
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v2, 0x400

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2145
    const/16 v2, 0xa

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2146
    :cond_1
    const-wide/32 v2, 0x10000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 2147
    const/16 v2, 0x10

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2148
    :cond_2
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 2149
    const/16 v2, 0x12

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2150
    :cond_3
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2152
    :sswitch_1
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 2153
    const/16 v2, 0x11

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2154
    :cond_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2156
    :sswitch_2
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2158
    :sswitch_3
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2141
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x6c -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_3
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa3_1(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x2

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x3

    .line 1209
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 1210
    const/4 v1, 0x1

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1237
    :goto_0
    return v1

    .line 1211
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1216
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 1237
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1212
    :catch_0
    move-exception v0

    .line 1213
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1219
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v2, 0x400

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1220
    const/16 v2, 0xa

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1221
    :cond_1
    const-wide/32 v2, 0x10000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1222
    const/16 v2, 0x10

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1223
    :cond_2
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 1224
    const/16 v2, 0x12

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1225
    :cond_3
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1227
    :sswitch_1
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 1228
    const/16 v2, 0x11

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1229
    :cond_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1231
    :sswitch_2
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1233
    :sswitch_3
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1216
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x6c -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_3
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa3_2(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x2

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x3

    .line 268
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 269
    const/4 v1, 0x1

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 296
    :goto_0
    return v1

    .line 270
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 296
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 271
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 278
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v2, 0x400

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 279
    const/16 v2, 0xa

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 280
    :cond_1
    const-wide/32 v2, 0x10000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 281
    const/16 v2, 0x10

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 282
    :cond_2
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 283
    const/16 v2, 0x12

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 284
    :cond_3
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 286
    :sswitch_1
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 287
    const/16 v2, 0x11

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 288
    :cond_4
    const-wide/16 v1, 0x2000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 290
    :sswitch_2
    const-wide/32 v1, 0x80000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 292
    :sswitch_3
    const-wide/32 v1, 0x8000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa4_2(JJ)I

    move-result v1

    goto :goto_0

    .line 275
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x6c -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_3
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa4_0(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x4

    .line 2166
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 2167
    const/4 v1, 0x2

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2190
    :goto_0
    return v1

    .line 2168
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2173
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 2190
    :cond_1
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2169
    :catch_0
    move-exception v0

    .line 2170
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2176
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2178
    :sswitch_1
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 2179
    const/16 v2, 0xd

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2180
    :cond_2
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2181
    const/16 v2, 0x13

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2184
    :sswitch_2
    const-wide/32 v2, 0x8000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2185
    const/16 v2, 0xf

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2173
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_1
        0x79 -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa4_1(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x4

    .line 1241
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 1242
    const/4 v1, 0x2

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1265
    :goto_0
    return v1

    .line 1243
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1248
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 1265
    :cond_1
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1244
    :catch_0
    move-exception v0

    .line 1245
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1251
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1253
    :sswitch_1
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1254
    const/16 v2, 0xd

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1255
    :cond_2
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1256
    const/16 v2, 0x13

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1259
    :sswitch_2
    const-wide/32 v2, 0x8000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1260
    const/16 v2, 0xf

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1248
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_1
        0x79 -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa4_2(JJ)I
    .locals 8
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x25

    const-wide/16 v4, 0x0

    const/4 v1, 0x4

    .line 300
    and-long/2addr p3, p1

    cmp-long v2, p3, v4

    if-nez v2, :cond_0

    .line 301
    const/4 v1, 0x2

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 324
    :goto_0
    return v1

    .line 302
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    sparse-switch v2, :sswitch_data_0

    .line 324
    :cond_1
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v7, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 310
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa5_2(JJ)I

    move-result v1

    goto :goto_0

    .line 312
    :sswitch_1
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 313
    const/16 v2, 0xd

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 314
    :cond_2
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 315
    const/16 v2, 0x13

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 318
    :sswitch_2
    const-wide/32 v2, 0x8000

    and-long/2addr v2, p3

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 319
    const/16 v2, 0xf

    invoke-direct {p0, v1, v2, v6}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 307
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_1
        0x79 -> :sswitch_2
    .end sparse-switch
.end method

.method private jjMoveStringLiteralDfa5_0(JJ)I
    .locals 4
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x4

    .line 2194
    and-long/2addr p3, p1

    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-nez v1, :cond_0

    .line 2195
    const/4 v1, 0x3

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2208
    :goto_0
    return v1

    .line 2196
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2201
    iget-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v1, :pswitch_data_0

    .line 2208
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2197
    :catch_0
    move-exception v0

    .line 2198
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    .line 2199
    const/4 v1, 0x5

    goto :goto_0

    .line 2204
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa6_0(JJ)I

    move-result v1

    goto :goto_0

    .line 2201
    nop

    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa5_1(JJ)I
    .locals 4
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x4

    .line 1269
    and-long/2addr p3, p1

    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-nez v1, :cond_0

    .line 1270
    const/4 v1, 0x3

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1283
    :goto_0
    return v1

    .line 1271
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1276
    iget-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v1, :pswitch_data_0

    .line 1283
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1272
    :catch_0
    move-exception v0

    .line 1273
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    .line 1274
    const/4 v1, 0x5

    goto :goto_0

    .line 1279
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa6_1(JJ)I

    move-result v1

    goto :goto_0

    .line 1276
    nop

    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa5_2(JJ)I
    .locals 4
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const/4 v3, 0x4

    .line 328
    and-long/2addr p3, p1

    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-nez v1, :cond_0

    .line 329
    const/4 v1, 0x3

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 342
    :goto_0
    return v1

    .line 330
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    iget-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v1, :pswitch_data_0

    .line 342
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v3, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    .line 333
    const/4 v1, 0x5

    goto :goto_0

    .line 338
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v1, 0x1000

    invoke-direct {p0, p3, p4, v1, v2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa6_2(JJ)I

    move-result v1

    goto :goto_0

    .line 335
    nop

    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa6_0(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x6

    const/4 v4, 0x5

    .line 2212
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 2213
    const/4 v1, 0x4

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    .line 2228
    :goto_0
    return v1

    .line 2214
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2219
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 2228
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_0(IJ)I

    move-result v1

    goto :goto_0

    .line 2215
    :catch_0
    move-exception v0

    .line 2216
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    goto :goto_0

    .line 2222
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 2223
    const/16 v2, 0xc

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_0(III)I

    move-result v1

    goto :goto_0

    .line 2219
    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa6_1(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x6

    const/4 v4, 0x5

    .line 1287
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 1288
    const/4 v1, 0x4

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    .line 1303
    :goto_0
    return v1

    .line 1289
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1294
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 1303
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_1(IJ)I

    move-result v1

    goto :goto_0

    .line 1290
    :catch_0
    move-exception v0

    .line 1291
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    goto :goto_0

    .line 1297
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 1298
    const/16 v2, 0xc

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_1(III)I

    move-result v1

    goto :goto_0

    .line 1294
    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_0
    .end packed-switch
.end method

.method private jjMoveStringLiteralDfa6_2(JJ)I
    .locals 7
    .param p1, "old0"    # J
    .param p3, "active0"    # J

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x6

    const/4 v4, 0x5

    .line 346
    and-long/2addr p3, p1

    cmp-long v2, p3, v5

    if-nez v2, :cond_0

    .line 347
    const/4 v1, 0x4

    invoke-direct {p0, v1, p1, p2}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    .line 362
    :goto_0
    return v1

    .line 348
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v2

    iput-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    iget-char v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    packed-switch v2, :pswitch_data_0

    .line 362
    :cond_1
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfa_2(IJ)I

    move-result v1

    goto :goto_0

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v4, p3, p4}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    goto :goto_0

    .line 356
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_0
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p3

    cmp-long v2, v2, v5

    if-eqz v2, :cond_1

    .line 357
    const/16 v2, 0xc

    const/16 v3, 0x25

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStartNfaWithStates_2(III)I

    move-result v1

    goto :goto_0

    .line 353
    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_0
    .end packed-switch
.end method

.method private jjStartNfaWithStates_0(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 2232
    iput p2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2233
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 2234
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2236
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_0(II)I

    move-result v1

    :goto_0
    return v1

    .line 2235
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private jjStartNfaWithStates_1(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 1307
    iput p2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1308
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 1309
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1311
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_1(II)I

    move-result v1

    :goto_0
    return v1

    .line 1310
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private jjStartNfaWithStates_2(III)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "kind"    # I
    .param p3, "state"    # I

    .prologue
    .line 366
    iput p2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 367
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 368
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v1}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    move-result v1

    iput-char v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p3, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_2(II)I

    move-result v1

    :goto_0
    return v1

    .line 369
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    add-int/lit8 v1, p1, 0x1

    goto :goto_0
.end method

.method private final jjStartNfa_0(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 1981
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_0(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_0(II)I

    move-result v0

    return v0
.end method

.method private final jjStartNfa_1(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 1052
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_1(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_1(II)I

    move-result v0

    return v0
.end method

.method private final jjStartNfa_2(IJ)I
    .locals 2
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    .line 109
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjStopStringLiteralDfa_2(IJ)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveNfa_2(II)I

    move-result v0

    return v0
.end method

.method private jjStopAtPos(II)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "kind"    # I

    .prologue
    .line 113
    iput p2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 114
    iput p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 115
    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private final jjStopStringLiteralDfa_0(IJ)I
    .locals 8
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v1, -0x1

    const/16 v6, 0x36

    const/16 v0, 0x25

    const-wide/16 v4, 0x0

    .line 1888
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 1976
    :cond_0
    :goto_0
    return v0

    .line 1891
    :pswitch_0
    const-wide/32 v2, 0xd3a00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1893
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    goto :goto_0

    .line 1896
    :cond_1
    const-wide/high16 v2, 0x8000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 1897
    const/4 v0, 0x6

    goto :goto_0

    .line 1898
    :cond_2
    const-wide/high16 v2, 0x10000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 1899
    const/16 v0, 0xb

    goto :goto_0

    .line 1900
    :cond_3
    const-wide/32 v2, 0x8400

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 1902
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1903
    const/16 v0, 0x11

    goto :goto_0

    .line 1905
    :cond_4
    const-wide/32 v2, 0x24000

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    .line 1907
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1908
    const/16 v0, 0x3f

    goto :goto_0

    .line 1910
    :cond_5
    const-wide v2, 0x4000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 1912
    const/16 v0, 0x2e

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1913
    const/16 v0, 0x13

    goto :goto_0

    .line 1915
    :cond_6
    const-wide v2, 0x82000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7

    .line 1916
    const/16 v0, 0xf

    goto :goto_0

    :cond_7
    move v0, v1

    .line 1917
    goto :goto_0

    .line 1919
    :pswitch_1
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 1921
    const/16 v1, 0x24

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1922
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 1925
    :cond_8
    const-wide/32 v2, 0xfbc00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 1927
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1928
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 1931
    :cond_9
    const-wide/16 v2, 0x200

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1933
    goto :goto_0

    .line 1935
    :pswitch_2
    const-wide/16 v2, 0x5800

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1937
    const-wide/32 v2, 0xfa400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 1939
    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 1941
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1942
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 1946
    goto/16 :goto_0

    .line 1948
    :pswitch_3
    const-wide/32 v2, 0x8b000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 1950
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1951
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 1954
    :cond_b
    const-wide/32 v2, 0x70400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1956
    goto/16 :goto_0

    .line 1958
    :pswitch_4
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 1960
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1961
    const/4 v1, 0x4

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 1964
    :cond_c
    const-wide/32 v2, 0x8a000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1966
    goto/16 :goto_0

    .line 1968
    :pswitch_5
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 1970
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1971
    const/4 v1, 0x5

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 1974
    goto/16 :goto_0

    .line 1888
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private final jjStopStringLiteralDfa_1(IJ)I
    .locals 8
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v1, -0x1

    const/16 v6, 0x36

    const/16 v0, 0x25

    const-wide/16 v4, 0x0

    .line 959
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 1047
    :cond_0
    :goto_0
    return v0

    .line 962
    :pswitch_0
    const-wide/32 v2, 0x1d3a00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 964
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    goto :goto_0

    .line 967
    :cond_1
    const-wide/high16 v2, 0x8000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 968
    const/4 v0, 0x6

    goto :goto_0

    .line 969
    :cond_2
    const-wide/high16 v2, 0x10000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 970
    const/16 v0, 0xb

    goto :goto_0

    .line 971
    :cond_3
    const-wide/32 v2, 0x24000

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 973
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 974
    const/16 v0, 0x3d

    goto :goto_0

    .line 976
    :cond_4
    const-wide/32 v2, 0x8400

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    .line 978
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 979
    const/16 v0, 0x11

    goto :goto_0

    .line 981
    :cond_5
    const-wide v2, 0x4000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 983
    const/16 v0, 0x2e

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 984
    const/16 v0, 0x13

    goto :goto_0

    .line 986
    :cond_6
    const-wide v2, 0x82000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7

    .line 987
    const/16 v0, 0xf

    goto :goto_0

    :cond_7
    move v0, v1

    .line 988
    goto :goto_0

    .line 990
    :pswitch_1
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 992
    const/16 v1, 0x24

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 993
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 996
    :cond_8
    const-wide/32 v2, 0xfbc00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 998
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 999
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 1002
    :cond_9
    const-wide/32 v2, 0x100200

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1004
    goto :goto_0

    .line 1006
    :pswitch_2
    const-wide/16 v2, 0x5800

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1008
    const-wide/32 v2, 0xfa400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 1010
    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 1012
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1013
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 1017
    goto/16 :goto_0

    .line 1019
    :pswitch_3
    const-wide/32 v2, 0x8b000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 1021
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1022
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 1025
    :cond_b
    const-wide/32 v2, 0x70400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1027
    goto/16 :goto_0

    .line 1029
    :pswitch_4
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 1031
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1032
    const/4 v1, 0x4

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 1035
    :cond_c
    const-wide/32 v2, 0x8a000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 1037
    goto/16 :goto_0

    .line 1039
    :pswitch_5
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 1041
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 1042
    const/4 v1, 0x5

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 1045
    goto/16 :goto_0

    .line 959
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private final jjStopStringLiteralDfa_2(IJ)I
    .locals 8
    .param p1, "pos"    # I
    .param p2, "active0"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v1, -0x1

    const/16 v6, 0x36

    const/16 v0, 0x25

    const-wide/16 v4, 0x0

    .line 16
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 19
    :pswitch_0
    const-wide/32 v2, 0xd3a00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 21
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    goto :goto_0

    .line 24
    :cond_1
    const-wide/high16 v2, 0x8000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 25
    const/4 v0, 0x6

    goto :goto_0

    .line 26
    :cond_2
    const-wide/high16 v2, 0x10000000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 27
    const/16 v0, 0xb

    goto :goto_0

    .line 28
    :cond_3
    const-wide/32 v2, 0x24000

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 30
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 31
    const/16 v0, 0x3d

    goto :goto_0

    .line 33
    :cond_4
    const-wide/32 v2, 0x8400

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    .line 35
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 36
    const/16 v0, 0x11

    goto :goto_0

    .line 38
    :cond_5
    const-wide v2, 0x4000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 40
    const/16 v0, 0x2e

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 41
    const/16 v0, 0x13

    goto :goto_0

    .line 43
    :cond_6
    const-wide v2, 0x82000000000L

    and-long/2addr v2, p2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7

    .line 44
    const/16 v0, 0xf

    goto :goto_0

    :cond_7
    move v0, v1

    .line 45
    goto :goto_0

    .line 47
    :pswitch_1
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 49
    const/16 v1, 0x24

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 50
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 53
    :cond_8
    const-wide/32 v2, 0xfbc00

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 55
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 56
    iput v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto :goto_0

    .line 59
    :cond_9
    const-wide/16 v2, 0x200

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :pswitch_2
    const-wide/16 v2, 0x5800

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 65
    const-wide/32 v2, 0xfa400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 67
    iget v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 69
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 70
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 74
    goto/16 :goto_0

    .line 76
    :pswitch_3
    const-wide/32 v2, 0x8b000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 78
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 79
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 82
    :cond_b
    const-wide/32 v2, 0x70400

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 84
    goto/16 :goto_0

    .line 86
    :pswitch_4
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 88
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 89
    const/4 v1, 0x4

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    .line 92
    :cond_c
    const-wide/32 v2, 0x8a000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 94
    goto/16 :goto_0

    .line 96
    :pswitch_5
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 98
    iput v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 99
    const/4 v1, 0x5

    iput v1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 102
    goto/16 :goto_0

    .line 16
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public ReInit(Lorg/apache/commons/jexl2/parser/SimpleCharStream;)V
    .locals 1
    .param p1, "stream"    # Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    .prologue
    .line 2903
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewStateCnt:I

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 2904
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->defaultLexState:I

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    .line 2905
    iput-object p1, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    .line 2906
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->ReInitRounds()V

    .line 2907
    return-void
.end method

.method public getNextToken()Lorg/apache/commons/jexl2/parser/Token;
    .locals 15

    .prologue
    .line 2967
    const/4 v8, 0x0

    .line 2974
    .local v8, "curPos":I
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->BeginToken()C

    move-result v0

    iput-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2983
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    packed-switch v0, :pswitch_data_0

    .line 3016
    :goto_1
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_7

    .line 3018
    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    add-int/lit8 v0, v0, 0x1

    if-ge v0, v8, :cond_1

    .line 3019
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    sub-int v2, v8, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 3020
    :cond_1
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjtoToken:[J

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    shr-int/lit8 v2, v2, 0x6

    aget-wide v6, v0, v2

    const-wide/16 v13, 0x1

    iget v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    and-int/lit8 v0, v0, 0x3f

    shl-long/2addr v13, v0

    and-long/2addr v6, v13

    const-wide/16 v13, 0x0

    cmp-long v0, v6, v13

    if-eqz v0, :cond_6

    .line 3022
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjFillToken()Lorg/apache/commons/jexl2/parser/Token;

    move-result-object v11

    .line 3023
    .local v11, "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget v0, v0, v2

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 3024
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget v0, v0, v2

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    :cond_2
    move-object v12, v11

    .line 3025
    .end local v11    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    .local v12, "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    :goto_2
    return-object v12

    .line 2976
    .end local v12    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    :catch_0
    move-exception v9

    .line 2978
    .local v9, "e":Ljava/io/IOException;
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2979
    invoke-virtual {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjFillToken()Lorg/apache/commons/jexl2/parser/Token;

    move-result-object v11

    .restart local v11    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    move-object v12, v11

    .line 2980
    .end local v11    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    .restart local v12    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    goto :goto_2

    .line 2986
    .end local v9    # "e":Ljava/io/IOException;
    .end local v12    # "matchedToken":Lorg/apache/commons/jexl2/parser/Token;
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 2987
    :goto_3
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0x20

    if-gt v0, v2, :cond_3

    const-wide v6, 0x100003600L

    const-wide/16 v13, 0x1

    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shl-long/2addr v13, v0

    and-long/2addr v6, v13

    const-wide/16 v13, 0x0

    cmp-long v0, v6, v13

    if-eqz v0, :cond_3

    .line 2988
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->BeginToken()C

    move-result v0

    iput-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 2990
    :catch_1
    move-exception v10

    .local v10, "e1":Ljava/io/IOException;
    goto/16 :goto_0

    .line 2991
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_3
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 2992
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 2993
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa0_0()I

    move-result v8

    .line 2994
    goto/16 :goto_1

    .line 2996
    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 2997
    :goto_4
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0x20

    if-gt v0, v2, :cond_4

    const-wide v6, 0x100003600L

    const-wide/16 v13, 0x1

    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shl-long/2addr v13, v0

    and-long/2addr v6, v13

    const-wide/16 v13, 0x0

    cmp-long v0, v6, v13

    if-eqz v0, :cond_4

    .line 2998
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->BeginToken()C

    move-result v0

    iput-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    .line 3000
    :catch_2
    move-exception v10

    .restart local v10    # "e1":Ljava/io/IOException;
    goto/16 :goto_0

    .line 3001
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_4
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 3002
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 3003
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa0_1()I

    move-result v8

    .line 3004
    goto/16 :goto_1

    .line 3006
    :pswitch_2
    :try_start_3
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 3007
    :goto_5
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0x20

    if-gt v0, v2, :cond_5

    const-wide v6, 0x100003600L

    const-wide/16 v13, 0x1

    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    shl-long/2addr v13, v0

    and-long/2addr v6, v13

    const-wide/16 v13, 0x0

    cmp-long v0, v6, v13

    if-eqz v0, :cond_5

    .line 3008
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->BeginToken()C

    move-result v0

    iput-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_5

    .line 3010
    :catch_3
    move-exception v10

    .restart local v10    # "e1":Ljava/io/IOException;
    goto/16 :goto_0

    .line 3011
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_5
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    .line 3012
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedPos:I

    .line 3013
    invoke-direct {p0}, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjMoveStringLiteralDfa0_2()I

    move-result v8

    goto/16 :goto_1

    .line 3029
    :cond_6
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget v0, v0, v2

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3030
    sget-object v0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjnewLexState:[I

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget v0, v0, v2

    iput v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    goto/16 :goto_0

    .line 3034
    :cond_7
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getEndLine()I

    move-result v3

    .line 3035
    .local v3, "error_line":I
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getEndColumn()I

    move-result v4

    .line 3036
    .local v4, "error_column":I
    const/4 v5, 0x0

    .line 3037
    .local v5, "error_after":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3038
    .local v1, "EOFSeen":Z
    :try_start_4
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->readChar()C

    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 3049
    :goto_6
    if-nez v1, :cond_8

    .line 3050
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->backup(I)V

    .line 3051
    const/4 v0, 0x1

    if-gt v8, v0, :cond_c

    const-string/jumbo v5, ""

    .line 3053
    :cond_8
    :goto_7
    new-instance v0, Lorg/apache/commons/jexl2/parser/TokenMgrError;

    iget v2, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curLexState:I

    iget-char v6, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lorg/apache/commons/jexl2/parser/TokenMgrError;-><init>(ZIIILjava/lang/String;CI)V

    throw v0

    .line 3039
    :catch_4
    move-exception v10

    .line 3040
    .restart local v10    # "e1":Ljava/io/IOException;
    const/4 v1, 0x1

    .line 3041
    const/4 v0, 0x1

    if-gt v8, v0, :cond_a

    const-string/jumbo v5, ""

    .line 3042
    :goto_8
    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0xa

    if-eq v0, v2, :cond_9

    iget-char v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->curChar:C

    const/16 v2, 0xd

    if-ne v0, v2, :cond_b

    .line 3043
    :cond_9
    add-int/lit8 v3, v3, 0x1

    .line 3044
    const/4 v4, 0x0

    goto :goto_6

    .line 3041
    :cond_a
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->GetImage()Ljava/lang/String;

    move-result-object v5

    goto :goto_8

    .line 3047
    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 3051
    .end local v10    # "e1":Ljava/io/IOException;
    :cond_c
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v0}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->GetImage()Ljava/lang/String;

    move-result-object v5

    goto :goto_7

    .line 2983
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected jjFillToken()Lorg/apache/commons/jexl2/parser/Token;
    .locals 9

    .prologue
    .line 2940
    sget-object v7, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjstrLiteralImages:[Ljava/lang/String;

    iget v8, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    aget-object v5, v7, v8

    .line 2941
    .local v5, "im":Ljava/lang/String;
    if-nez v5, :cond_0

    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->GetImage()Ljava/lang/String;

    move-result-object v2

    .line 2942
    .local v2, "curTokenImage":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getBeginLine()I

    move-result v1

    .line 2943
    .local v1, "beginLine":I
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getBeginColumn()I

    move-result v0

    .line 2944
    .local v0, "beginColumn":I
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getEndLine()I

    move-result v4

    .line 2945
    .local v4, "endLine":I
    iget-object v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->input_stream:Lorg/apache/commons/jexl2/parser/SimpleCharStream;

    invoke-virtual {v7}, Lorg/apache/commons/jexl2/parser/SimpleCharStream;->getEndColumn()I

    move-result v3

    .line 2946
    .local v3, "endColumn":I
    iget v7, p0, Lorg/apache/commons/jexl2/parser/ParserTokenManager;->jjmatchedKind:I

    invoke-static {v7, v2}, Lorg/apache/commons/jexl2/parser/Token;->newToken(ILjava/lang/String;)Lorg/apache/commons/jexl2/parser/Token;

    move-result-object v6

    .line 2948
    .local v6, "t":Lorg/apache/commons/jexl2/parser/Token;
    iput v1, v6, Lorg/apache/commons/jexl2/parser/Token;->beginLine:I

    .line 2949
    iput v4, v6, Lorg/apache/commons/jexl2/parser/Token;->endLine:I

    .line 2950
    iput v0, v6, Lorg/apache/commons/jexl2/parser/Token;->beginColumn:I

    .line 2951
    iput v3, v6, Lorg/apache/commons/jexl2/parser/Token;->endColumn:I

    .line 2953
    return-object v6

    .end local v0    # "beginColumn":I
    .end local v1    # "beginLine":I
    .end local v2    # "curTokenImage":Ljava/lang/String;
    .end local v3    # "endColumn":I
    .end local v4    # "endLine":I
    .end local v6    # "t":Lorg/apache/commons/jexl2/parser/Token;
    :cond_0
    move-object v2, v5

    .line 2941
    goto :goto_0
.end method
