.class public interface abstract Lorg/apache/commons/jexl2/introspection/JexlMethod;
.super Ljava/lang/Object;
.source "JexlMethod.java"


# virtual methods
.method public abstract getReturnType()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract isCacheable()Z
.end method

.method public abstract tryFailed(Ljava/lang/Object;)Z
.end method

.method public abstract tryInvoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
.end method
