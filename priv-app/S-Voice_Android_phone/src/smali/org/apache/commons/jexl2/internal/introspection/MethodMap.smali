.class final Lorg/apache/commons/jexl2/internal/introspection/MethodMap;
.super Ljava/lang/Object;
.source "MethodMap.java"


# instance fields
.field private final methodByNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Method;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;->methodByNameMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public declared-synchronized add(Ljava/lang/reflect/Method;)V
    .locals 3
    .param p1, "method"    # Ljava/lang/reflect/Method;

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "methodName":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;->methodByNameMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 52
    .local v0, "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Method;>;"
    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Method;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .restart local v0    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Method;>;"
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;->methodByNameMap:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    .line 48
    .end local v0    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Method;>;"
    .end local v1    # "methodName":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method find(Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;
    .locals 2
    .param p1, "methodKey"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
        }
    .end annotation

    .prologue
    .line 119
    invoke-virtual {p1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMethod()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;->get(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 120
    .local v0, "methodList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Method;>;"
    if-nez v0, :cond_0

    .line 121
    const/4 v1, 0x0

    .line 123
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->getMostSpecificMethod(Ljava/util/List;)Ljava/lang/reflect/Method;

    move-result-object v1

    goto :goto_0
.end method

.method public declared-synchronized get(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;->methodByNameMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
