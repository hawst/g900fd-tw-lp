.class public final Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;
.super Lorg/apache/commons/jexl2/parser/JexlNode;
.source "ASTIntegerLiteral.java"

# interfaces
.implements Lorg/apache/commons/jexl2/parser/JexlNode$Literal;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/commons/jexl2/parser/JexlNode;",
        "Lorg/apache/commons/jexl2/parser/JexlNode$Literal",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field literal:Ljava/lang/Integer;


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lorg/apache/commons/jexl2/parser/JexlNode;-><init>(I)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;->literal:Ljava/lang/Integer;

    .line 25
    return-void
.end method


# virtual methods
.method public getLiteral()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;->literal:Ljava/lang/Integer;

    return-object v0
.end method

.method public jjtAccept(Lorg/apache/commons/jexl2/parser/ParserVisitor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "visitor"    # Lorg/apache/commons/jexl2/parser/ParserVisitor;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 42
    invoke-interface {p1, p0, p2}, Lorg/apache/commons/jexl2/parser/ParserVisitor;->visit(Lorg/apache/commons/jexl2/parser/ASTIntegerLiteral;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
