.class public interface abstract Lorg/apache/commons/jexl2/parser/ParserConstants;
.super Ljava/lang/Object;
.source "ParserConstants.java"


# static fields
.field public static final tokenImage:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 126
    const/16 v0, 0x3d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "<EOF>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "<token of kind 1>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "<token of kind 2>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "<token of kind 3>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "\" \""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "\"\\t\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\"\\n\""

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\"\\r\""

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\"\\f\""

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\"if\""

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "\"else\""

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "\"for\""

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "\"foreach\""

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "\"while\""

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "\"new\""

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "\"empty\""

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "\"size\""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "\"null\""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "\"true\""

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "\"false\""

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "\"in\""

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "\"(\""

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "\")\""

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "\"{\""

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "\"}\""

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "\"[\""

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "\"]\""

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "\";\""

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "\":\""

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "\",\""

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "\".\""

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "\"?\""

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "\"?:\""

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "<AND>"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "<OR>"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "<eq>"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "<ne>"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "\"=~\""

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "\"!~\""

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "<gt>"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "<ge>"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "<lt>"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "<le>"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "\"=\""

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "<mod>"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "<div>"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "<not>"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "\"+\""

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "\"-\""

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "\"*\""

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "\"~\""

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "\"&\""

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "\"|\""

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "\"^\""

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "<IDENTIFIER>"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string/jumbo v2, "<LETTER>"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string/jumbo v2, "<DIGIT>"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string/jumbo v2, "<REGISTER>"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string/jumbo v2, "<INTEGER_LITERAL>"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string/jumbo v2, "<FLOAT_LITERAL>"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string/jumbo v2, "<STRING_LITERAL>"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/commons/jexl2/parser/ParserConstants;->tokenImage:[Ljava/lang/String;

    return-void
.end method
