.class abstract Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;
.super Ljava/lang/Object;
.source "MethodKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Parameters"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 374
    .local p0, "this":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;, "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/jexl2/internal/introspection/MethodKey$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey$1;

    .prologue
    .line 374
    .local p0, "this":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;, "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters<TT;>;"
    invoke-direct {p0}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;Ljava/util/List;[Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # [Ljava/lang/Class;

    .prologue
    .line 374
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->getMostSpecific(Ljava/util/List;[Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private getApplicables(Ljava/util/List;[Ljava/lang/Class;)Ljava/util/LinkedList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;[",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/util/LinkedList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 523
    .local p0, "this":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;, "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters<TT;>;"
    .local p1, "methods":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p2, "classes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 525
    .local v1, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<TT;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "imethod":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 526
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 527
    .local v2, "method":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, v2, p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->isApplicable(Ljava/lang/Object;[Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 528
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 532
    .end local v2    # "method":Ljava/lang/Object;, "TT;"
    :cond_1
    return-object v1
.end method

.method private getMostSpecific(Ljava/util/List;[Ljava/lang/Class;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;[",
            "Ljava/lang/Class",
            "<*>;)TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;, "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters<TT;>;"
    .local p1, "methods":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p2, "classes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v9, 0x1

    .line 391
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->getApplicables(Ljava/util/List;[Ljava/lang/Class;)Ljava/util/LinkedList;

    move-result-object v3

    .line 393
    .local v3, "applicables":Ljava/util/LinkedList;, "Ljava/util/LinkedList<TT;>;"
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 394
    const/4 v8, 0x0

    .line 453
    :goto_0
    return-object v8

    .line 397
    :cond_0
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v8

    if-ne v8, v9, :cond_1

    .line 398
    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v8

    goto :goto_0

    .line 407
    :cond_1
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 409
    .local v7, "maximals":Ljava/util/LinkedList;, "Ljava/util/LinkedList<TT;>;"
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 410
    .local v2, "applicable":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 411
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 412
    .local v0, "app":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, v0}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->getParameterTypes(Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v1

    .line 414
    .local v1, "appArgs":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v4, 0x0

    .line 416
    .local v4, "lessSpecific":Z
    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 417
    .local v6, "maximal":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :goto_2
    if-nez v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 418
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 421
    .local v5, "max":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, v5}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->getParameterTypes(Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v8

    invoke-direct {p0, v1, v8}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->moreSpecific([Ljava/lang/Class;[Ljava/lang/Class;)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    goto :goto_2

    .line 427
    :pswitch_0
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 438
    :pswitch_1
    const/4 v4, 0x1

    goto :goto_2

    .line 443
    .end local v5    # "max":Ljava/lang/Object;, "TT;"
    :cond_3
    if-nez v4, :cond_2

    .line 444
    invoke-virtual {v7, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_1

    .line 448
    .end local v0    # "app":Ljava/lang/Object;, "TT;"
    .end local v1    # "appArgs":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v4    # "lessSpecific":Z
    .end local v6    # "maximal":Ljava/util/Iterator;, "Ljava/util/Iterator<TT;>;"
    :cond_4
    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v8

    if-le v8, v9, :cond_5

    .line 450
    new-instance v8, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;

    invoke-direct {v8}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;-><init>()V

    throw v8

    .line 453
    :cond_5
    invoke-virtual {v7}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v8

    goto :goto_0

    .line 421
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private isApplicable(Ljava/lang/Object;[Ljava/lang/Class;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;[",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;, "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters<TT;>;"
    .local p1, "method":Ljava/lang/Object;, "TT;"
    .local p2, "classes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 544
    invoke-virtual {p0, p1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->getParameterTypes(Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v2

    .line 546
    .local v2, "methodArgs":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    array-length v6, v2

    array-length v7, p2

    if-le v6, v7, :cond_2

    .line 549
    array-length v6, v2

    array-length v7, p2

    add-int/lit8 v7, v7, 0x1

    if-ne v6, v7, :cond_1

    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/Class;->isArray()Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_0
    move v5, v4

    .line 593
    :cond_0
    :goto_1
    return v5

    :cond_1
    move v4, v5

    .line 549
    goto :goto_0

    .line 551
    :cond_2
    array-length v6, v2

    array-length v7, p2

    if-ne v6, v7, :cond_5

    .line 555
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v6, p2

    if-ge v0, v6, :cond_4

    .line 556
    aget-object v6, v2, v0

    aget-object v7, p2, v0

    invoke-direct {p0, v6, v7, v5}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->isConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v6

    if-nez v6, :cond_3

    .line 558
    array-length v6, p2

    add-int/lit8 v6, v6, -0x1

    if-ne v0, v6, :cond_0

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/lang/Class;->isArray()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 561
    aget-object v5, v2, v0

    aget-object v6, p2, v0

    invoke-direct {p0, v5, v6, v4}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->isConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v5

    goto :goto_1

    .line 555
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v5, v4

    .line 566
    goto :goto_1

    .line 569
    .end local v0    # "i":I
    :cond_5
    array-length v6, v2

    if-lez v6, :cond_0

    .line 571
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    aget-object v1, v2, v6

    .line 572
    .local v1, "lastarg":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 577
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v0, v6, :cond_6

    .line 578
    aget-object v6, v2, v0

    aget-object v7, p2, v0

    invoke-direct {p0, v6, v7, v5}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->isConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 577
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 584
    :cond_6
    invoke-virtual {v1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v3

    .line 585
    .local v3, "vararg":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    array-length v6, v2

    add-int/lit8 v0, v6, -0x1

    :goto_4
    array-length v6, p2

    if-ge v0, v6, :cond_7

    .line 586
    aget-object v6, p2, v0

    invoke-direct {p0, v3, v6, v5}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->isConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 585
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    move v5, v4

    .line 590
    goto :goto_1
.end method

.method private isConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z
    .locals 1
    .param p3, "possibleVarArg"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;Z)Z"
        }
    .end annotation

    .prologue
    .line 608
    .local p0, "this":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;, "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters<TT;>;"
    .local p1, "formal":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p2, "actual":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v0, Ljava/lang/Void;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    .end local p2    # "actual":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    invoke-static {p1, p2, p3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->isInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v0

    return v0
.end method

.method private isStrictConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z
    .locals 1
    .param p3, "possibleVarArg"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;Z)Z"
        }
    .end annotation

    .prologue
    .line 623
    .local p0, "this":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;, "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters<TT;>;"
    .local p1, "formal":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p2, "actual":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v0, Ljava/lang/Void;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    .end local p2    # "actual":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    invoke-static {p1, p2, p3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;->isStrictInvocationConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v0

    return v0
.end method

.method private moreSpecific([Ljava/lang/Class;[Ljava/lang/Class;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/Class",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;, "Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters<TT;>;"
    .local p1, "c1":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .local p2, "c2":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 466
    const/4 v0, 0x0

    .line 467
    .local v0, "c1MoreSpecific":Z
    const/4 v1, 0x0

    .line 472
    .local v1, "c2MoreSpecific":Z
    array-length v7, p1

    array-length v8, p2

    if-le v7, v8, :cond_1

    .line 510
    :cond_0
    :goto_0
    return v4

    .line 475
    :cond_1
    array-length v7, p2

    array-length v8, p1

    if-le v7, v8, :cond_2

    move v4, v5

    .line 476
    goto :goto_0

    .line 480
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v7, p1

    if-ge v2, v7, :cond_9

    .line 481
    aget-object v7, p1, v2

    aget-object v8, p2, v2

    if-eq v7, v8, :cond_5

    .line 482
    array-length v7, p1

    add-int/lit8 v7, v7, -0x1

    if-ne v2, v7, :cond_6

    move v3, v5

    .line 483
    .local v3, "last":Z
    :goto_2
    if-nez v0, :cond_3

    aget-object v7, p2, v2

    aget-object v8, p1, v2

    invoke-direct {p0, v7, v8, v3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->isStrictConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v7

    if-eqz v7, :cond_7

    :cond_3
    move v0, v5

    .line 484
    :goto_3
    if-nez v1, :cond_4

    aget-object v7, p1, v2

    aget-object v8, p2, v2

    invoke-direct {p0, v7, v8, v3}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey$Parameters;->isStrictConvertible(Ljava/lang/Class;Ljava/lang/Class;Z)Z

    move-result v7

    if-eqz v7, :cond_8

    :cond_4
    move v1, v5

    .line 480
    .end local v3    # "last":Z
    :cond_5
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    move v3, v4

    .line 482
    goto :goto_2

    .restart local v3    # "last":Z
    :cond_7
    move v0, v4

    .line 483
    goto :goto_3

    :cond_8
    move v1, v4

    .line 484
    goto :goto_4

    .line 488
    .end local v3    # "last":Z
    :cond_9
    if-eqz v0, :cond_a

    .line 489
    if-eqz v1, :cond_0

    move v4, v6

    .line 495
    goto :goto_0

    .line 501
    :cond_a
    if-eqz v1, :cond_b

    move v4, v5

    .line 502
    goto :goto_0

    :cond_b
    move v4, v6

    .line 510
    goto :goto_0
.end method


# virtual methods
.method protected abstract getParameterTypes(Ljava/lang/Object;)[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method
