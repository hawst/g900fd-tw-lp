.class final Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;
.super Ljava/lang/Object;
.source "ClassMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/jexl2/internal/introspection/ClassMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "MethodCache"
.end annotation


# static fields
.field private static final CACHE_MISS:Ljava/lang/reflect/Method;

.field private static final PRIMITIVE_TYPES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final methodMap:Lorg/apache/commons/jexl2/internal/introspection/MethodMap;

.field private final methods:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/commons/jexl2/internal/introspection/MethodKey;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 228
    invoke-static {}, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->cacheMiss()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->CACHE_MISS:Ljava/lang/reflect/Method;

    .line 234
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    .line 235
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Byte;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Character;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Double;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Float;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Integer;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Long;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Short;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methods:Ljava/util/Map;

    .line 272
    new-instance v0, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methodMap:Lorg/apache/commons/jexl2/internal/introspection/MethodMap;

    return-void
.end method

.method public static cacheMiss()Ljava/lang/reflect/Method;
    .locals 4

    .prologue
    .line 221
    :try_start_0
    const-class v1, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;

    const-string/jumbo v2, "cacheMiss"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 224
    .local v0, "xio":Ljava/lang/Exception;
    :goto_0
    return-object v1

    .line 222
    .end local v0    # "xio":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 224
    .restart local v0    # "xio":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static primitiveClass(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 258
    .local p0, "parm":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v1, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->PRIMITIVE_TYPES:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 259
    .local v0, "prim":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez v0, :cond_0

    .end local p0    # "parm":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    return-object p0

    .restart local p0    # "parm":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method get(Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;
    .locals 5
    .param p1, "methodKey"    # Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
        }
    .end annotation

    .prologue
    .line 291
    iget-object v3, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methodMap:Lorg/apache/commons/jexl2/internal/introspection/MethodMap;

    monitor-enter v3

    .line 292
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methods:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Method;

    .line 294
    .local v1, "cacheEntry":Ljava/lang/reflect/Method;
    sget-object v2, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->CACHE_MISS:Ljava/lang/reflect/Method;

    if-ne v1, v2, :cond_0

    .line 295
    const/4 v2, 0x0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    :goto_0
    return-object v2

    .line 298
    :cond_0
    if-nez v1, :cond_1

    .line 301
    :try_start_1
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methodMap:Lorg/apache/commons/jexl2/internal/introspection/MethodMap;

    invoke-virtual {v2, p1}, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;->find(Lorg/apache/commons/jexl2/internal/introspection/MethodKey;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 302
    if-eqz v1, :cond_2

    .line 303
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methods:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v2, v1

    goto :goto_0

    .line 305
    :cond_2
    :try_start_3
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methods:Ljava/util/Map;

    sget-object v4, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->CACHE_MISS:Ljava/lang/reflect/Method;

    invoke-interface {v2, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 307
    :catch_0
    move-exception v0

    .line 309
    .local v0, "ae":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    :try_start_4
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methods:Ljava/util/Map;

    sget-object v4, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->CACHE_MISS:Ljava/lang/reflect/Method;

    invoke-interface {v2, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    throw v0

    .line 316
    .end local v0    # "ae":Lorg/apache/commons/jexl2/internal/introspection/MethodKey$AmbiguousException;
    .end local v1    # "cacheEntry":Ljava/lang/reflect/Method;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2
.end method

.method put(Ljava/lang/reflect/Method;)V
    .locals 3
    .param p1, "method"    # Ljava/lang/reflect/Method;

    .prologue
    .line 324
    iget-object v2, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methodMap:Lorg/apache/commons/jexl2/internal/introspection/MethodMap;

    monitor-enter v2

    .line 325
    :try_start_0
    new-instance v0, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;

    invoke-direct {v0, p1}, Lorg/apache/commons/jexl2/internal/introspection/MethodKey;-><init>(Ljava/lang/reflect/Method;)V

    .line 330
    .local v0, "methodKey":Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methods:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 331
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methods:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    iget-object v1, p0, Lorg/apache/commons/jexl2/internal/introspection/ClassMap$MethodCache;->methodMap:Lorg/apache/commons/jexl2/internal/introspection/MethodMap;

    invoke-virtual {v1, p1}, Lorg/apache/commons/jexl2/internal/introspection/MethodMap;->add(Ljava/lang/reflect/Method;)V

    .line 334
    :cond_0
    monitor-exit v2

    .line 335
    return-void

    .line 334
    .end local v0    # "methodKey":Lorg/apache/commons/jexl2/internal/introspection/MethodKey;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
