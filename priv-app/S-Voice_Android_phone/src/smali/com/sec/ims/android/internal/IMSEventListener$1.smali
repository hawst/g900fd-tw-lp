.class Lcom/sec/ims/android/internal/IMSEventListener$1;
.super Lcom/sec/ims/android/internal/IIMSEventListener$Stub;
.source "IMSEventListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/android/internal/IMSEventListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/ims/android/internal/IMSEventListener;


# direct methods
.method constructor <init>(Lcom/sec/ims/android/internal/IMSEventListener;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/ims/android/internal/IMSEventListener$1;->this$0:Lcom/sec/ims/android/internal/IMSEventListener;

    .line 54
    invoke-direct {p0}, Lcom/sec/ims/android/internal/IIMSEventListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public handleEvent(IIII[BLcom/sec/ims/android/internal/IIMSParams;)V
    .locals 7
    .param p1, "appType"    # I
    .param p2, "eventType"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "data"    # [B
    .param p6, "param"    # Lcom/sec/ims/android/internal/IIMSParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/ims/android/internal/IMSEventListener$1;->this$0:Lcom/sec/ims/android/internal/IMSEventListener;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/ims/android/internal/IMSEventListener;->handleEvent(IIII[BLcom/sec/ims/android/internal/IIMSParams;)V

    .line 63
    const-string/jumbo v0, "IIMSEventListener"

    const-string/jumbo v1, "Inside handleEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    return-void
.end method

.method public handleEventForNotify(IIII[I[Ljava/lang/String;)V
    .locals 7
    .param p1, "appType"    # I
    .param p2, "eventType"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "data1"    # [I
    .param p6, "data2"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/ims/android/internal/IMSEventListener$1;->this$0:Lcom/sec/ims/android/internal/IMSEventListener;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/ims/android/internal/IMSEventListener;->handleEventForNotify(IIII[I[Ljava/lang/String;)V

    .line 78
    const-string/jumbo v0, "IIMSEventListener handle event for notify"

    const-string/jumbo v1, "Inside handleEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-void
.end method

.method public notifyEvent(IIII[I[Ljava/lang/String;)V
    .locals 7
    .param p1, "appType"    # I
    .param p2, "eventType"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "participantList"    # [I
    .param p6, "uriList"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/ims/android/internal/IMSEventListener$1;->this$0:Lcom/sec/ims/android/internal/IMSEventListener;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/ims/android/internal/IMSEventListener;->notifyEvent(IIII[I[Ljava/lang/String;)V

    .line 70
    const-string/jumbo v0, "IIMSEventListener"

    const-string/jumbo v1, "Inside notifyEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    return-void
.end method
