.class public abstract Lcom/sec/ims/android/internal/IIMSEabService$Stub;
.super Landroid/os/Binder;
.source "IIMSEabService.java"

# interfaces
.implements Lcom/sec/ims/android/internal/IIMSEabService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/android/internal/IIMSEabService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/android/internal/IIMSEabService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.ims.android.internal.IIMSEabService"

.field static final TRANSACTION_contactSvcCallFunction:I = 0x7

.field static final TRANSACTION_getRemoteCapabilitiesAndAvailability:I = 0x2

.field static final TRANSACTION_publishCapabilitiesAndAvailability:I = 0x3

.field static final TRANSACTION_registerEabEventListener:I = 0x5

.field static final TRANSACTION_registerServiceCapabilityAvailabilityTemplate:I = 0x1

.field static final TRANSACTION_unRegisterEabEventListener:I = 0x6

.field static final TRANSACTION_unpublishCapabilitiesAndAvailability:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/ims/android/internal/IIMSEabService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IIMSEabService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string/jumbo v1, "com.sec.ims.android.internal.IIMSEabService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/ims/android/internal/IIMSEabService;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/sec/ims/android/internal/IIMSEabService;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/sec/ims/android/internal/IIMSEabService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/ims/android/internal/IIMSEabService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 124
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 45
    :sswitch_0
    const-string/jumbo v6, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string/jumbo v6, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/ims/android/internal/IIMSEabService$Stub;->registerServiceCapabilityAvailabilityTemplate(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    :sswitch_2
    const-string/jumbo v6, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 64
    .local v1, "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 65
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSEabService$Stub;->getRemoteCapabilitiesAndAvailability(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    .end local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "_arg1":Ljava/lang/String;
    :sswitch_3
    const-string/jumbo v6, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 74
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_0

    .line 75
    sget-object v6, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 80
    .local v2, "_arg1":Landroid/os/Bundle;
    :goto_1
    invoke-virtual {p0, v0, v2}, Lcom/sec/ims/android/internal/IIMSEabService$Stub;->publishCapabilitiesAndAvailability(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 78
    .end local v2    # "_arg1":Landroid/os/Bundle;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/os/Bundle;
    goto :goto_1

    .line 85
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Landroid/os/Bundle;
    :sswitch_4
    const-string/jumbo v6, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSEabService$Stub;->unpublishCapabilitiesAndAvailability()V

    goto :goto_0

    .line 91
    :sswitch_5
    const-string/jumbo v8, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/ims/android/internal/IIMSEabEventListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IIMSEabEventListener;

    move-result-object v0

    .line 94
    .local v0, "_arg0":Lcom/sec/ims/android/internal/IIMSEabEventListener;
    invoke-virtual {p0, v0}, Lcom/sec/ims/android/internal/IIMSEabService$Stub;->registerEabEventListener(Lcom/sec/ims/android/internal/IIMSEabEventListener;)Z

    move-result v5

    .line 95
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 96
    if-eqz v5, :cond_1

    move v6, v7

    :cond_1
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 101
    .end local v0    # "_arg0":Lcom/sec/ims/android/internal/IIMSEabEventListener;
    .end local v5    # "_result":Z
    :sswitch_6
    const-string/jumbo v8, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/ims/android/internal/IIMSEabEventListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IIMSEabEventListener;

    move-result-object v0

    .line 104
    .restart local v0    # "_arg0":Lcom/sec/ims/android/internal/IIMSEabEventListener;
    invoke-virtual {p0, v0}, Lcom/sec/ims/android/internal/IIMSEabService$Stub;->unRegisterEabEventListener(Lcom/sec/ims/android/internal/IIMSEabEventListener;)Z

    move-result v5

    .line 105
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 106
    if-eqz v5, :cond_2

    move v6, v7

    :cond_2
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 111
    .end local v0    # "_arg0":Lcom/sec/ims/android/internal/IIMSEabEventListener;
    .end local v5    # "_result":Z
    :sswitch_7
    const-string/jumbo v6, "com.sec.ims.android.internal.IIMSEabService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 115
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 117
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 119
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 120
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/sec/ims/android/internal/IIMSEabService$Stub;->contactSvcCallFunction(IIILjava/lang/String;)V

    goto/16 :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
