.class public abstract Lcom/sec/ims/android/internal/IMSEventListener;
.super Ljava/lang/Object;
.source "IMSEventListener.java"


# static fields
.field private static final HANDLE_EVENT:I = 0x0

.field public static final LISTEN_ENABLER_MESSAGE:I = 0x4

.field public static final LISTEN_ENABLER_VOIP:I = 0x2

.field public static final LISTEN_IMS_CORE:I = 0x1

.field public static final LISTEN_NONE:I = 0x0

.field public static final LISTEN_SIGNAL_STRENGTHS:I = 0x100


# instance fields
.field public callback:Lcom/sec/ims/android/internal/IIMSEventListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/sec/ims/android/internal/IMSEventListener$1;

    invoke-direct {v0, p0}, Lcom/sec/ims/android/internal/IMSEventListener$1;-><init>(Lcom/sec/ims/android/internal/IMSEventListener;)V

    iput-object v0, p0, Lcom/sec/ims/android/internal/IMSEventListener;->callback:Lcom/sec/ims/android/internal/IIMSEventListener;

    .line 13
    return-void
.end method


# virtual methods
.method public abstract handleEvent(IIII[BLcom/sec/ims/android/internal/IIMSParams;)V
.end method

.method public handleEventForNotify(IIII[I[Ljava/lang/String;)V
    .locals 0
    .param p1, "appType"    # I
    .param p2, "eventType"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "data1"    # [I
    .param p6, "data2"    # [Ljava/lang/String;

    .prologue
    .line 48
    return-void
.end method

.method public notifyEvent(IIII[I[Ljava/lang/String;)V
    .locals 0
    .param p1, "appType"    # I
    .param p2, "eventType"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "participantList"    # [I
    .param p6, "uriList"    # [Ljava/lang/String;

    .prologue
    .line 42
    return-void
.end method
