.class Lcom/sec/ims/android/internal/Utils$1;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/ims/android/internal/Utils;->updatePCSCFAddressToRouteTable(Ljava/util/List;[Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$gateWayAddress:Ljava/lang/String;

.field private final synthetic val$intfName:[Ljava/lang/String;

.field private final synthetic val$localIpAddressSet:Ljava/util/Set;

.field private final synthetic val$pcscfIpList:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/ims/android/internal/Utils$1;->val$pcscfIpList:Ljava/util/List;

    iput-object p2, p0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/ims/android/internal/Utils$1;->val$gateWayAddress:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/ims/android/internal/Utils$1;->val$localIpAddressSet:Ljava/util/Set;

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 37
    const-string/jumbo v8, "network_management"

    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v8

    .line 36
    invoke-static {v8}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    move-result-object v5

    .line 38
    .local v5, "networkManagementService":Landroid/os/INetworkManagementService;
    iget-object v8, p0, Lcom/sec/ims/android/internal/Utils$1;->val$pcscfIpList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 62
    iget-object v8, p0, Lcom/sec/ims/android/internal/Utils$1;->val$gateWayAddress:Ljava/lang/String;

    if-nez v8, :cond_5

    .line 63
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$0()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "updatePcscfAddressToRouteTable gateWayAddress passed is null, returning"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_1
    :goto_1
    return-void

    .line 38
    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 39
    .local v6, "pcscfAddress":Ljava/lang/String;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$0()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "updatePcscfAddressToRouteTable addRoute-pcscfAddress = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " IntfName = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    :try_start_0
    invoke-static {v6}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    .line 42
    .local v3, "inetAddress":Ljava/net/InetAddress;
    const/4 v7, 0x0

    .line 43
    .local v7, "routeInfo":Landroid/net/RouteInfo;
    instance-of v9, v3, Ljava/net/Inet4Address;

    if-eqz v9, :cond_4

    .line 44
    new-instance v4, Landroid/net/LinkAddress;

    const/16 v9, 0x20

    invoke-direct {v4, v3, v9}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 45
    .local v4, "linkAddress":Landroid/net/LinkAddress;
    new-instance v7, Landroid/net/RouteInfo;

    .end local v7    # "routeInfo":Landroid/net/RouteInfo;
    const/4 v9, 0x0

    invoke-direct {v7, v4, v9}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    .line 50
    .end local v4    # "linkAddress":Landroid/net/LinkAddress;
    .restart local v7    # "routeInfo":Landroid/net/RouteInfo;
    :cond_3
    :goto_2
    if-eqz v7, :cond_0

    .line 51
    iget-object v9, p0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    invoke-interface {v5, v9, v7}, Landroid/os/INetworkManagementService;->addRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 53
    .end local v3    # "inetAddress":Ljava/net/InetAddress;
    .end local v7    # "routeInfo":Landroid/net/RouteInfo;
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$0()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "IllegalArgumentException caught while addRoute for Emergency"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 46
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v3    # "inetAddress":Ljava/net/InetAddress;
    .restart local v7    # "routeInfo":Landroid/net/RouteInfo;
    :cond_4
    :try_start_1
    instance-of v9, v3, Ljava/net/Inet6Address;

    if-eqz v9, :cond_3

    .line 47
    new-instance v4, Landroid/net/LinkAddress;

    const/16 v9, 0x80

    invoke-direct {v4, v3, v9}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 48
    .restart local v4    # "linkAddress":Landroid/net/LinkAddress;
    new-instance v7, Landroid/net/RouteInfo;

    .end local v7    # "routeInfo":Landroid/net/RouteInfo;
    const/4 v9, 0x0

    invoke-direct {v7, v4, v9}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    .restart local v7    # "routeInfo":Landroid/net/RouteInfo;
    goto :goto_2

    .line 55
    .end local v3    # "inetAddress":Ljava/net/InetAddress;
    .end local v4    # "linkAddress":Landroid/net/LinkAddress;
    .end local v7    # "routeInfo":Landroid/net/RouteInfo;
    :catch_1
    move-exception v0

    .line 56
    .local v0, "e":Ljava/net/UnknownHostException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$0()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "UnknownHostException caught while addRoute for Emergency"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 57
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :catch_2
    move-exception v0

    .line 58
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$0()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "RemoteException caught while addRoute for Emergency"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 68
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v6    # "pcscfAddress":Ljava/lang/String;
    :cond_5
    :try_start_2
    iget-object v8, p0, Lcom/sec/ims/android/internal/Utils$1;->val$gateWayAddress:Ljava/lang/String;

    invoke-static {v8}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    .line 69
    .local v2, "gatewayInetAddress":Ljava/net/InetAddress;
    iget-object v8, p0, Lcom/sec/ims/android/internal/Utils$1;->val$localIpAddressSet:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/InetAddress;

    .line 70
    .local v1, "epdnLocalIp":Ljava/net/InetAddress;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$0()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "updatePcscfAddressToRouteTable replaceSrcRoute-epdnLocalIp = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " IntfName = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    const/4 v12, 0x0

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 71
    const-string/jumbo v11, "gateWayAddress = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/ims/android/internal/Utils$1;->val$gateWayAddress:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 70
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v9, p0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    .line 73
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v10

    .line 74
    invoke-virtual {v2}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v11

    .line 75
    const/16 v12, 0x5a

    .line 72
    invoke-interface {v5, v9, v10, v11, v12}, Landroid/os/INetworkManagementService;->replaceSrcRoute(Ljava/lang/String;[B[BI)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_3

    .line 77
    .end local v1    # "epdnLocalIp":Ljava/net/InetAddress;
    .end local v2    # "gatewayInetAddress":Ljava/net/InetAddress;
    :catch_3
    move-exception v0

    .line 78
    .restart local v0    # "e":Landroid/os/RemoteException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$0()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "RemoteException caught while replaceSrcRoute for Emergency"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 79
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_4
    move-exception v0

    .line 80
    .local v0, "e":Ljava/net/UnknownHostException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$0()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "UnknownHostException caught while replaceSrcRoute for Emergency"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
