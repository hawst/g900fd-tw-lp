.class public Lcom/sec/ims/android/internal/IIMSParams;
.super Ljava/lang/Object;
.source "IIMSParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/android/internal/IIMSParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAudioCodec:Ljava/lang/String;

.field private mCodecBitRate:I

.field private mFullModeRegistered:I

.field private mHistoryInfo:Ljava/lang/String;

.field private mIndicationFlag:I

.field private mIsConferenceSupported:Ljava/lang/String;

.field private mIsFocus:Ljava/lang/String;

.field private mIsVMSCall:Ljava/lang/String;

.field private mModifySupported:Ljava/lang/String;

.field private mNumberPlus:Ljava/lang/String;

.field private mPLettering:Ljava/lang/String;

.field private mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

.field private mReasonCode:Ljava/lang/String;

.field private mTTYModeType:Ljava/lang/String;

.field private mUSSDData:[B

.field private misHDIcon:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/sec/ims/android/internal/IIMSParams$1;

    invoke-direct {v0}, Lcom/sec/ims/android/internal/IIMSParams$1;-><init>()V

    sput-object v0, Lcom/sec/ims/android/internal/IIMSParams;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 47
    .local v0, "ussdDataLength":I
    if-lez v0, :cond_0

    .line 48
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    .line 49
    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 54
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    .line 58
    return-void

    .line 51
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public getAudioCodec()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    return-object v0
.end method

.method public getCodecBitRate()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    return v0
.end method

.method public getErrorReasonCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    return-object v0
.end method

.method public getHistoryInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getIndicationFlag()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    return v0
.end method

.method public getIsConferenceSupported()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    return-object v0
.end method

.method public getIsFocus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    return-object v0
.end method

.method public getIsVMSCall()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    return-object v0
.end method

.method public getModifyHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberPlus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    return-object v0
.end method

.method public getPLettering()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    return-object v0
.end method

.method public getParticipantChangedInNWayConferenceCall()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    return-object v0
.end method

.method public getUSSDData()[B
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    return-object v0
.end method

.method public getisFullModeRegistered()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    return v0
.end method

.method public getisHDIcon()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    return v0
.end method

.method public getttyType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    return-object v0
.end method

.method public setAudioCodec(Ljava/lang/String;)V
    .locals 0
    .param p1, "codec"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public setCodecBitRate(I)V
    .locals 0
    .param p1, "bitrate"    # I

    .prologue
    .line 220
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    .line 221
    return-void
.end method

.method public setErrorReasonCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public setHistoryInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public setIndicationFlag(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 195
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    .line 196
    return-void
.end method

.method public setIsConferenceSupported(Ljava/lang/String;)V
    .locals 0
    .param p1, "isConfSupported"    # Ljava/lang/String;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    .line 172
    return-void
.end method

.method public setIsFocus(Ljava/lang/String;)V
    .locals 0
    .param p1, "isFocus"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public setIsVMSCall(Ljava/lang/String;)V
    .locals 0
    .param p1, "isVMSCall"    # Ljava/lang/String;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    .line 230
    return-void
.end method

.method public setModifyHeader(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setNumberPlus(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setPLettering(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public setParticipantChangedInNWayConferenceCall(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    .line 164
    return-void
.end method

.method public setUSSDData([B)V
    .locals 0
    .param p1, "data"    # [B

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    .line 188
    return-void
.end method

.method public setisFullModeRegistered(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 211
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    .line 212
    return-void
.end method

.method public setisHDIcon(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 203
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    .line 204
    return-void
.end method

.method public setttyType(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    .line 156
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "IIMSParams [mPLettering="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mHistoryInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 235
    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mModifySupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 236
    const-string/jumbo v1, ", mAudioCodec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mNumberPlus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mReasonCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 238
    const-string/jumbo v1, ", mTTYModeType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 239
    const-string/jumbo v1, ", mParticipantChangedInNWayConferenceCall="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 241
    const-string/jumbo v1, ", mIsConferenceSupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 242
    const-string/jumbo v1, ", mIsFocus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mIsVMSCall="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mUSSDData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mIndicationFlag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 244
    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", misHDIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 245
    const-string/jumbo v1, ", mFullModeRegistered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 246
    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", misHDIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCodecBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    :goto_0
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 83
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0
.end method
