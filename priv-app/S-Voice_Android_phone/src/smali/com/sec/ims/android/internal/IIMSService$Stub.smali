.class public abstract Lcom/sec/ims/android/internal/IIMSService$Stub;
.super Landroid/os/Binder;
.source "IIMSService.java"

# interfaces
.implements Lcom/sec/ims/android/internal/IIMSService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/android/internal/IIMSService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/android/internal/IIMSService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.ims.android.internal.IIMSService"

.field static final TRANSACTION_captureSurfaceImage:I = 0x12

.field static final TRANSACTION_contactSvcCallFunction:I = 0x28

.field static final TRANSACTION_deinitSurface:I = 0x13

.field static final TRANSACTION_getCurrentLatchedNetwork:I = 0x2a

.field static final TRANSACTION_getImsRegStatus:I = 0x5

.field static final TRANSACTION_getMessageEnabler:I = 0x4

.field static final TRANSACTION_isFrontCamInUse:I = 0x9

.field static final TRANSACTION_isIMSEnabledOnWifi:I = 0x2b

.field static final TRANSACTION_isImsForbidden:I = 0xc

.field static final TRANSACTION_isOnEHRPD:I = 0xb

.field static final TRANSACTION_isOnLTE:I = 0xa

.field static final TRANSACTION_isRegistered:I = 0x1

.field static final TRANSACTION_mmSS_Svc_Api:I = 0x6

.field static final TRANSACTION_mmTelSvcCallFunc:I = 0x27

.field static final TRANSACTION_mmTelSvcCallFuncAsync:I = 0x25

.field static final TRANSACTION_register:I = 0x1f

.field static final TRANSACTION_registerApp:I = 0x2

.field static final TRANSACTION_registerListener:I = 0x23

.field static final TRANSACTION_registerSSApp:I = 0x7

.field static final TRANSACTION_registerWithISIMResponse:I = 0x21

.field static final TRANSACTION_resetCameraID:I = 0xf

.field static final TRANSACTION_sendDeregister:I = 0x2c

.field static final TRANSACTION_sendDeregisterForSimRefresh:I = 0x2d

.field static final TRANSACTION_sendLiveVideo:I = 0x11

.field static final TRANSACTION_sendStillImage:I = 0x10

.field static final TRANSACTION_sendTtyData:I = 0x26

.field static final TRANSACTION_setAudioTuningParameters:I = 0x29

.field static final TRANSACTION_setNearEndSurface:I = 0x1e

.field static final TRANSACTION_setOrientation:I = 0x1a

.field static final TRANSACTION_startAudio:I = 0x17

.field static final TRANSACTION_startCamera:I = 0x1c

.field static final TRANSACTION_startRender:I = 0xe

.field static final TRANSACTION_startVideo:I = 0x18

.field static final TRANSACTION_startVideoRenderer:I = 0x15

.field static final TRANSACTION_stopCamera:I = 0x1d

.field static final TRANSACTION_stopVideo:I = 0x19

.field static final TRANSACTION_stopVideoRenderer:I = 0x16

.field static final TRANSACTION_swapVideoSurface:I = 0x14

.field static final TRANSACTION_switchCamera:I = 0xd

.field static final TRANSACTION_unRegisterApp:I = 0x3

.field static final TRANSACTION_unRegisterSSApp:I = 0x8

.field static final TRANSACTION_unregister:I = 0x20

.field static final TRANSACTION_unregisterListener:I = 0x24

.field static final TRANSACTION_unregisterWithISIMResponse:I = 0x22

.field static final TRANSACTION_voiceRecord:I = 0x1b

.field static final TRANSACTION_writeErrorData:I = 0x2e


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IIMSService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string/jumbo v1, "com.sec.ims.android.internal.IIMSService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/ims/android/internal/IIMSService;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/sec/ims/android/internal/IIMSService;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/sec/ims/android/internal/IIMSService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/ims/android/internal/IIMSService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 507
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 45
    :sswitch_0
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->isRegistered()Z

    move-result v8

    .line 52
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 53
    if-eqz v8, :cond_0

    move v0, v9

    :cond_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 58
    .end local v8    # "_result":Z
    :sswitch_2
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 62
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/ims/android/internal/IImsServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IImsServiceCallback;

    move-result-object v3

    .line 65
    .local v3, "_arg2":Lcom/sec/ims/android/internal/IImsServiceCallback;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/ims/android/internal/IIMSService$Stub;->registerApp(ILjava/lang/String;Lcom/sec/ims/android/internal/IImsServiceCallback;)I

    move-result v8

    .line 66
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 67
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 72
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/sec/ims/android/internal/IImsServiceCallback;
    .end local v8    # "_result":I
    :sswitch_3
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 76
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 77
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSService$Stub;->unRegisterApp(II)V

    goto :goto_0

    .line 82
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_4
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->getMessageEnabler()Lcom/sec/ims/android/internal/IMessageEnabler;

    move-result-object v8

    .line 84
    .local v8, "_result":Lcom/sec/ims/android/internal/IMessageEnabler;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    if-eqz v8, :cond_1

    invoke-interface {v8}, Lcom/sec/ims/android/internal/IMessageEnabler;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 90
    .end local v8    # "_result":Lcom/sec/ims/android/internal/IMessageEnabler;
    :sswitch_5
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->getImsRegStatus()Z

    move-result v8

    .line 92
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 93
    if-eqz v8, :cond_2

    move v0, v9

    :cond_2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 98
    .end local v8    # "_result":Z
    :sswitch_6
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 102
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 104
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 106
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .local v5, "_arg4":Ljava/lang/String;
    move-object v0, p0

    .line 109
    invoke-virtual/range {v0 .. v5}, Lcom/sec/ims/android/internal/IIMSService$Stub;->mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 110
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 111
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 116
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_7
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 120
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 122
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/ims/android/internal/IImsServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IImsServiceCallback;

    move-result-object v3

    .line 123
    .local v3, "_arg2":Lcom/sec/ims/android/internal/IImsServiceCallback;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/ims/android/internal/IIMSService$Stub;->registerSSApp(ILjava/lang/String;Lcom/sec/ims/android/internal/IImsServiceCallback;)I

    move-result v8

    .line 124
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 125
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 130
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/sec/ims/android/internal/IImsServiceCallback;
    .end local v8    # "_result":I
    :sswitch_8
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 134
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 135
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSService$Stub;->unRegisterSSApp(II)V

    goto/16 :goto_0

    .line 140
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_9
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->isFrontCamInUse()I

    move-result v8

    .line 142
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 143
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 148
    .end local v8    # "_result":I
    :sswitch_a
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->isOnLTE()Z

    move-result v8

    .line 150
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 151
    if-eqz v8, :cond_3

    move v0, v9

    :cond_3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 156
    .end local v8    # "_result":Z
    :sswitch_b
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->isOnEHRPD()Z

    move-result v8

    .line 158
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 159
    if-eqz v8, :cond_4

    move v0, v9

    :cond_4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 164
    .end local v8    # "_result":Z
    :sswitch_c
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->isImsForbidden()Z

    move-result v8

    .line 166
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 167
    if-eqz v8, :cond_5

    move v0, v9

    :cond_5
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 172
    .end local v8    # "_result":Z
    :sswitch_d
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->switchCamera()V

    .line 174
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 179
    :sswitch_e
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_6

    move v1, v9

    .line 182
    .local v1, "_arg0":Z
    :goto_2
    invoke-virtual {p0, v1}, Lcom/sec/ims/android/internal/IIMSService$Stub;->startRender(Z)V

    .line 183
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_6
    move v1, v0

    .line 181
    goto :goto_2

    .line 188
    :sswitch_f
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 189
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->resetCameraID()V

    goto/16 :goto_0

    .line 194
    :sswitch_10
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 196
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 198
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 200
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 201
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/ims/android/internal/IIMSService$Stub;->sendStillImage(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 206
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_11
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->sendLiveVideo()V

    goto/16 :goto_0

    .line 212
    :sswitch_12
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_7

    move v1, v9

    .line 216
    .local v1, "_arg0":Z
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 218
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 219
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/ims/android/internal/IIMSService$Stub;->captureSurfaceImage(ZLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :cond_7
    move v1, v0

    .line 214
    goto :goto_3

    .line 224
    :sswitch_13
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 226
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_8

    move v1, v9

    .line 227
    .restart local v1    # "_arg0":Z
    :goto_4
    invoke-virtual {p0, v1}, Lcom/sec/ims/android/internal/IIMSService$Stub;->deinitSurface(Z)V

    .line 228
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_8
    move v1, v0

    .line 226
    goto :goto_4

    .line 233
    :sswitch_14
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 234
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->swapVideoSurface()V

    .line 235
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 240
    :sswitch_15
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 242
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 243
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 249
    .local v1, "_arg0":Landroid/view/Surface;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 251
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 253
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 254
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/ims/android/internal/IIMSService$Stub;->startVideoRenderer(Landroid/view/Surface;IILjava/lang/String;)V

    .line 255
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 246
    .end local v1    # "_arg0":Landroid/view/Surface;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    :cond_9
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_5

    .line 260
    .end local v1    # "_arg0":Landroid/view/Surface;
    :sswitch_16
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->stopVideoRenderer()V

    .line 262
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 267
    :sswitch_17
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->startAudio()V

    goto/16 :goto_0

    .line 273
    :sswitch_18
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->startVideo()V

    goto/16 :goto_0

    .line 279
    :sswitch_19
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 280
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->stopVideo()V

    goto/16 :goto_0

    .line 285
    :sswitch_1a
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 287
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 288
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/ims/android/internal/IIMSService$Stub;->setOrientation(I)V

    .line 289
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 294
    .end local v1    # "_arg0":I
    :sswitch_1b
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 296
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 298
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 299
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSService$Stub;->voiceRecord(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 304
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    :sswitch_1c
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_a

    .line 307
    sget-object v10, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 313
    .local v1, "_arg0":Landroid/view/Surface;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 315
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 317
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 319
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_b

    move v5, v9

    .line 321
    .local v5, "_arg4":Z
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_c

    move v6, v9

    .line 323
    .local v6, "_arg5":Z
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .local v7, "_arg6":Ljava/lang/String;
    move-object v0, p0

    .line 324
    invoke-virtual/range {v0 .. v7}, Lcom/sec/ims/android/internal/IIMSService$Stub;->startCamera(Landroid/view/Surface;IIIZZLjava/lang/String;)V

    .line 325
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 310
    .end local v1    # "_arg0":Landroid/view/Surface;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":Z
    .end local v6    # "_arg5":Z
    .end local v7    # "_arg6":Ljava/lang/String;
    :cond_a
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_6

    .restart local v2    # "_arg1":I
    .restart local v3    # "_arg2":I
    .restart local v4    # "_arg3":I
    :cond_b
    move v5, v0

    .line 319
    goto :goto_7

    .restart local v5    # "_arg4":Z
    :cond_c
    move v6, v0

    .line 321
    goto :goto_8

    .line 330
    .end local v1    # "_arg0":Landroid/view/Surface;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":Z
    :sswitch_1d
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 331
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->stopCamera()V

    .line 332
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 337
    :sswitch_1e
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    .line 340
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 345
    .restart local v1    # "_arg0":Landroid/view/Surface;
    :goto_9
    invoke-virtual {p0, v1}, Lcom/sec/ims/android/internal/IIMSService$Stub;->setNearEndSurface(Landroid/view/Surface;)V

    .line 346
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 343
    .end local v1    # "_arg0":Landroid/view/Surface;
    :cond_d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_9

    .line 351
    .end local v1    # "_arg0":Landroid/view/Surface;
    :sswitch_1f
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 353
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 355
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_e

    move v2, v9

    .line 356
    .local v2, "_arg1":Z
    :goto_a
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSService$Stub;->register(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    :cond_e
    move v2, v0

    .line 355
    goto :goto_a

    .line 361
    .end local v1    # "_arg0":Ljava/lang/String;
    :sswitch_20
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->unregister()V

    goto/16 :goto_0

    .line 367
    :sswitch_21
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 369
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 371
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 372
    .local v2, "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSService$Stub;->registerWithISIMResponse(Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 377
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":[B
    :sswitch_22
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 379
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 381
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 382
    .restart local v2    # "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSService$Stub;->unregisterWithISIMResponse(Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 387
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":[B
    :sswitch_23
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 389
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/ims/android/internal/IIMSEventListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IIMSEventListener;

    move-result-object v1

    .line 391
    .local v1, "_arg0":Lcom/sec/ims/android/internal/IIMSEventListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 392
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSService$Stub;->registerListener(Lcom/sec/ims/android/internal/IIMSEventListener;I)V

    goto/16 :goto_0

    .line 397
    .end local v1    # "_arg0":Lcom/sec/ims/android/internal/IIMSEventListener;
    .end local v2    # "_arg1":I
    :sswitch_24
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 399
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/ims/android/internal/IIMSEventListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IIMSEventListener;

    move-result-object v1

    .line 400
    .restart local v1    # "_arg0":Lcom/sec/ims/android/internal/IIMSEventListener;
    invoke-virtual {p0, v1}, Lcom/sec/ims/android/internal/IIMSService$Stub;->unregisterListener(Lcom/sec/ims/android/internal/IIMSEventListener;)V

    goto/16 :goto_0

    .line 405
    .end local v1    # "_arg0":Lcom/sec/ims/android/internal/IIMSEventListener;
    :sswitch_25
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 407
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 409
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 411
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 413
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 414
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/ims/android/internal/IIMSService$Stub;->mmTelSvcCallFuncAsync(IIILjava/lang/String;)V

    goto/16 :goto_0

    .line 419
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    :sswitch_26
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 421
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 423
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 425
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 426
    .local v3, "_arg2":[B
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/ims/android/internal/IIMSService$Stub;->sendTtyData(II[B)V

    goto/16 :goto_0

    .line 431
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":[B
    :sswitch_27
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 433
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 435
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 437
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 439
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 440
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/ims/android/internal/IIMSService$Stub;->mmTelSvcCallFunc(IIILjava/lang/String;)I

    move-result v8

    .line 441
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 442
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 447
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_28
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 449
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 451
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 453
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 455
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 456
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/ims/android/internal/IIMSService$Stub;->contactSvcCallFunction(IIILjava/lang/String;)V

    goto/16 :goto_0

    .line 461
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    :sswitch_29
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 463
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 464
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/ims/android/internal/IIMSService$Stub;->setAudioTuningParameters(I)V

    goto/16 :goto_0

    .line 469
    .end local v1    # "_arg0":I
    :sswitch_2a
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 470
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->getCurrentLatchedNetwork()Ljava/lang/String;

    move-result-object v8

    .line 471
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 472
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 477
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_2b
    const-string/jumbo v10, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 478
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->isIMSEnabledOnWifi()Z

    move-result v8

    .line 479
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 480
    if-eqz v8, :cond_f

    move v0, v9

    :cond_f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 485
    .end local v8    # "_result":Z
    :sswitch_2c
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 486
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->sendDeregister()V

    goto/16 :goto_0

    .line 491
    :sswitch_2d
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 492
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSService$Stub;->sendDeregisterForSimRefresh()V

    goto/16 :goto_0

    .line 497
    :sswitch_2e
    const-string/jumbo v0, "com.sec.ims.android.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 499
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 501
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 502
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/ims/android/internal/IIMSService$Stub;->writeErrorData(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
