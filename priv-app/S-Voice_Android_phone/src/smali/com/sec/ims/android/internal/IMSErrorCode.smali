.class public Lcom/sec/ims/android/internal/IMSErrorCode;
.super Ljava/lang/Object;
.source "IMSErrorCode.java"


# static fields
.field public static final BAD_REQUEST:I = 0x190

.field public static final CALL_HOLD_FAILED:I = 0x2be

.field public static final CALL_RESUME_FAILED:I = 0x2bf

.field public static final CALL_SWITCH_FAILURE:I = 0x2bc

.field public static final CALL_SWITCH_REJECTED:I = 0x2bd

.field public static final CALL_TEMP_UNAVAILABLE_415_CAUSE:I = -0x31

.field public static final CALL_TIMEOUT:I = -0x32

.field public static final CLIENT_ERROR:I = -0x4

.field public static final CONGESTION:I = -0xf

.field public static final CROSS_DOMAIN_AUTHENTICATION:I = -0xb

.field public static final DATA_CONNECTION_LOST:I = -0xa

.field public static final ERROR:I = -0xd

.field public static final IMS_CALL_ADDRESS_INCOMPLETE:I = -0x16

.field public static final IMS_CALL_ALTERNATIVE_SERVICES:I = -0x2c

.field public static final IMS_CALL_END_CALL_NW_HANDOVER:I = -0x30

.field public static final IMS_CALL_FAILED:I = -0x13

.field public static final IMS_CALL_FORBIDDEN:I = -0x19

.field public static final IMS_CALL_FORBIDDEN_RSN_EXPIRED:I = -0x1a

.field public static final IMS_CALL_FORBIDDEN_RSN_GROUP_CALL_SERVICE_UNAVAILABLE:I = -0x1b

.field public static final IMS_CALL_FORBIDDEN_RSN_OUTGOING_CALLS_IMPOSSIBLE:I = -0x1d

.field public static final IMS_CALL_FORBIDDEN_RSN_TEMPORARY_DISABILITY:I = -0x1c

.field public static final IMS_CALL_METHOD_NOT_ALLOWED:I = -0x18

.field public static final IMS_CALL_NOT_ACCEPTABLE:I = -0x17

.field public static final IMS_CALL_NOT_ACCEPTABLE_DIVERT:I = -0x29

.field public static final IMS_CALL_REJECTED:I = -0x12

.field public static final IMS_CALL_REQ_FAILED:I = -0x10

.field public static final IMS_CALL_REQ_TERMINATED:I = -0x14

.field public static final IMS_CALL_SERVER_INTERNAL_ERR:I = -0x24

.field public static final IMS_CALL_SERVICE_UNAVAILABLE:I = -0x25

.field public static final IMS_CALL_SESSION_TIMEOUT:I = -0x26

.field public static final IMS_CALL_STATUS_CONF_ADD_USER_TO_SESSION_FAILURE:I = -0x2e

.field public static final IMS_CALL_STATUS_CONF_REMOVE_USER_FROM_SESSION_FAILURE:I = -0x2f

.field public static final IMS_CALL_STATUS_CONF_START_SESSION_FAILURE:I = -0x2d

.field public static final IMS_FAILED_TO_GO_READY:I = -0x23

.field public static final IMS_PPP_OPEN_FAILURE:I = -0x2a

.field public static final IMS_PPP_STATUS_CLOSE_EVENT:I = -0x22

.field public static final IMS_QOS_FAILURE:I = -0x1e

.field public static final IMS_QOS_INCALL_SUSPEND:I = -0x20

.field public static final IMS_QOS_INCALL_UNAWARE:I = -0x21

.field public static final IMS_QOS_NW_UNAWARE:I = -0x1f

.field public static final IMS_REG_NOT_SUBSCRIBED:I = -0x34

.field public static final IMS_REG_REQ_FAILED:I = -0x28

.field public static final IMS_REG_TIMEOUT:I = -0x27

.field public static final IMS_SESSION_ABORT:I = -0x15

.field public static final IMS_SESSION_TERMINATED:I = -0x33

.field public static final IMS_SIP_REG_FAILURE:I = -0x11

.field public static final IMS_USER_REJECT_REASON_USR_BUSY_CS_CALL:I = -0x35

.field public static final INVALID_CREDENTIALS:I = -0x8

.field public static final INVALID_REMOTE_URI:I = -0x6

.field public static final IN_PROGRESS:I = -0x9

.field public static final NOTACCEPTABLE_AUTO_DIVERT:I = -0xe

.field public static final NO_ERROR:I = 0x0

.field public static final PEER_NOT_REACHABLE:I = -0x7

.field public static final RTP_TIME_OUT:I = -0x2b

.field public static final SERVER_ERROR:I = -0x2

.field public static final SERVER_UNREACHABLE:I = -0xc

.field public static final SOCKET_ERROR:I = -0x1

.field public static final TIME_OUT:I = -0x5

.field public static final TRANSACTION_TERMINTED:I = -0x3

.field public static final UNSUPPORTED_MEDIA:I = 0x19f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "errorCode"    # I

    .prologue
    .line 130
    sparse-switch p0, :sswitch_data_0

    .line 191
    const-string/jumbo v0, "Unknown"

    :goto_0
    return-object v0

    .line 131
    :sswitch_0
    const-string/jumbo v0, "No Error"

    goto :goto_0

    .line 132
    :sswitch_1
    const-string/jumbo v0, "Socket failure"

    goto :goto_0

    .line 133
    :sswitch_2
    const-string/jumbo v0, "Server error"

    goto :goto_0

    .line 134
    :sswitch_3
    const-string/jumbo v0, "Transaction terminated"

    goto :goto_0

    .line 135
    :sswitch_4
    const-string/jumbo v0, "Client error"

    goto :goto_0

    .line 136
    :sswitch_5
    const-string/jumbo v0, "Timeout"

    goto :goto_0

    .line 137
    :sswitch_6
    const-string/jumbo v0, "Invalid remote address"

    goto :goto_0

    .line 138
    :sswitch_7
    const-string/jumbo v0, "Unreachable"

    goto :goto_0

    .line 139
    :sswitch_8
    const-string/jumbo v0, "Authentication failed"

    goto :goto_0

    .line 140
    :sswitch_9
    const-string/jumbo v0, "In progress"

    goto :goto_0

    .line 141
    :sswitch_a
    const-string/jumbo v0, "Network disconnected"

    goto :goto_0

    .line 142
    :sswitch_b
    const-string/jumbo v0, "Invalid domain"

    goto :goto_0

    .line 143
    :sswitch_c
    const-string/jumbo v0, "Server not rechable"

    goto :goto_0

    .line 144
    :sswitch_d
    const-string/jumbo v0, "Error"

    goto :goto_0

    .line 145
    :sswitch_e
    const-string/jumbo v0, "Call not acceptable and auto divert"

    goto :goto_0

    .line 146
    :sswitch_f
    const-string/jumbo v0, "Network busy"

    goto :goto_0

    .line 147
    :sswitch_10
    const-string/jumbo v0, "Call request failed"

    goto :goto_0

    .line 148
    :sswitch_11
    const-string/jumbo v0, "Registration failed"

    goto :goto_0

    .line 149
    :sswitch_12
    const-string/jumbo v0, "Call rejected"

    goto :goto_0

    .line 150
    :sswitch_13
    const-string/jumbo v0, "Call failed"

    goto :goto_0

    .line 151
    :sswitch_14
    const-string/jumbo v0, "Request terminated"

    goto :goto_0

    .line 152
    :sswitch_15
    const-string/jumbo v0, "Session aborted"

    goto :goto_0

    .line 153
    :sswitch_16
    const-string/jumbo v0, "Invalid address"

    goto :goto_0

    .line 154
    :sswitch_17
    const-string/jumbo v0, "call not allowed"

    goto :goto_0

    .line 155
    :sswitch_18
    const-string/jumbo v0, "Request type not allowed"

    goto :goto_0

    .line 156
    :sswitch_19
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 157
    :sswitch_1a
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 158
    :sswitch_1b
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 159
    :sswitch_1c
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 160
    :sswitch_1d
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 161
    :sswitch_1e
    const-string/jumbo v0, "QOS failed"

    goto :goto_0

    .line 162
    :sswitch_1f
    const-string/jumbo v0, "QOS network unaware"

    goto :goto_0

    .line 163
    :sswitch_20
    const-string/jumbo v0, "QOS suspended"

    goto/16 :goto_0

    .line 164
    :sswitch_21
    const-string/jumbo v0, "QOS incall unaware"

    goto/16 :goto_0

    .line 165
    :sswitch_22
    const-string/jumbo v0, "PPP Closed"

    goto/16 :goto_0

    .line 166
    :sswitch_23
    const-string/jumbo v0, "Call failed"

    goto/16 :goto_0

    .line 167
    :sswitch_24
    const-string/jumbo v0, "Internal server error"

    goto/16 :goto_0

    .line 168
    :sswitch_25
    const-string/jumbo v0, "Service unavailable"

    goto/16 :goto_0

    .line 169
    :sswitch_26
    const-string/jumbo v0, "Session time out"

    goto/16 :goto_0

    .line 170
    :sswitch_27
    const-string/jumbo v0, "SIP Registration time out"

    goto/16 :goto_0

    .line 171
    :sswitch_28
    const-string/jumbo v0, "Registration request failed"

    goto/16 :goto_0

    .line 172
    :sswitch_29
    const-string/jumbo v0, "Call not acceptable divert"

    goto/16 :goto_0

    .line 173
    :sswitch_2a
    const-string/jumbo v0, "PPP Open Failed"

    goto/16 :goto_0

    .line 174
    :sswitch_2b
    const-string/jumbo v0, "RTP Timeout"

    goto/16 :goto_0

    .line 175
    :sswitch_2c
    const-string/jumbo v0, "Call alternative services"

    goto/16 :goto_0

    .line 176
    :sswitch_2d
    const-string/jumbo v0, "Start conference call failure"

    goto/16 :goto_0

    .line 177
    :sswitch_2e
    const-string/jumbo v0, "Add user to session failure"

    goto/16 :goto_0

    .line 178
    :sswitch_2f
    const-string/jumbo v0, "Remove user from session failure"

    goto/16 :goto_0

    .line 179
    :sswitch_30
    const-string/jumbo v0, "End call NW handover"

    goto/16 :goto_0

    .line 180
    :sswitch_31
    const-string/jumbo v0, "Cannot connect HD call"

    goto/16 :goto_0

    .line 181
    :sswitch_32
    const-string/jumbo v0, "ACK wait timer timeout"

    goto/16 :goto_0

    .line 182
    :sswitch_33
    const-string/jumbo v0, "ACK for 200 OK but call terminated"

    goto/16 :goto_0

    .line 183
    :sswitch_34
    const-string/jumbo v0, "403 response for registering"

    goto/16 :goto_0

    .line 184
    :sswitch_35
    const-string/jumbo v0, "Call rejected due to active CS Call"

    goto/16 :goto_0

    .line 185
    :sswitch_36
    const-string/jumbo v0, "Bad request"

    goto/16 :goto_0

    .line 186
    :sswitch_37
    const-string/jumbo v0, "Media not supported"

    goto/16 :goto_0

    .line 187
    :sswitch_38
    const-string/jumbo v0, "Call switch failure"

    goto/16 :goto_0

    .line 188
    :sswitch_39
    const-string/jumbo v0, "Call switch rejected"

    goto/16 :goto_0

    .line 189
    :sswitch_3a
    const-string/jumbo v0, "Call hold failed"

    goto/16 :goto_0

    .line 190
    :sswitch_3b
    const-string/jumbo v0, "Call resume failed"

    goto/16 :goto_0

    .line 130
    nop

    :sswitch_data_0
    .sparse-switch
        -0x35 -> :sswitch_35
        -0x34 -> :sswitch_34
        -0x33 -> :sswitch_33
        -0x32 -> :sswitch_32
        -0x31 -> :sswitch_31
        -0x30 -> :sswitch_30
        -0x2f -> :sswitch_2f
        -0x2e -> :sswitch_2e
        -0x2d -> :sswitch_2d
        -0x2c -> :sswitch_2c
        -0x2b -> :sswitch_2b
        -0x2a -> :sswitch_2a
        -0x29 -> :sswitch_29
        -0x28 -> :sswitch_28
        -0x27 -> :sswitch_27
        -0x26 -> :sswitch_26
        -0x25 -> :sswitch_25
        -0x24 -> :sswitch_24
        -0x23 -> :sswitch_23
        -0x22 -> :sswitch_22
        -0x21 -> :sswitch_21
        -0x20 -> :sswitch_20
        -0x1f -> :sswitch_1f
        -0x1e -> :sswitch_1e
        -0x1d -> :sswitch_1d
        -0x1c -> :sswitch_1c
        -0x1b -> :sswitch_1b
        -0x1a -> :sswitch_1a
        -0x19 -> :sswitch_19
        -0x18 -> :sswitch_18
        -0x17 -> :sswitch_17
        -0x16 -> :sswitch_16
        -0x15 -> :sswitch_15
        -0x14 -> :sswitch_14
        -0x13 -> :sswitch_13
        -0x12 -> :sswitch_12
        -0x11 -> :sswitch_11
        -0x10 -> :sswitch_10
        -0xf -> :sswitch_f
        -0xe -> :sswitch_e
        -0xd -> :sswitch_d
        -0xc -> :sswitch_c
        -0xb -> :sswitch_b
        -0xa -> :sswitch_a
        -0x9 -> :sswitch_9
        -0x8 -> :sswitch_8
        -0x7 -> :sswitch_7
        -0x6 -> :sswitch_6
        -0x5 -> :sswitch_5
        -0x4 -> :sswitch_4
        -0x3 -> :sswitch_3
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_1
        0x0 -> :sswitch_0
        0x190 -> :sswitch_36
        0x19f -> :sswitch_37
        0x2bc -> :sswitch_38
        0x2bd -> :sswitch_39
        0x2be -> :sswitch_3a
        0x2bf -> :sswitch_3b
    .end sparse-switch
.end method
