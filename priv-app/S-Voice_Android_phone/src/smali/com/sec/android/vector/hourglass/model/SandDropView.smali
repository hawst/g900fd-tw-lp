.class public Lcom/sec/android/vector/hourglass/model/SandDropView;
.super Lcom/sec/android/vector/hourglass/model/AbstractView;
.source "SandDropView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;FF)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentWidth"    # F
    .param p3, "contentHeight"    # F

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/vector/hourglass/model/AbstractView;-><init>(Landroid/content/Context;FF)V

    .line 16
    invoke-direct {p0}, Lcom/sec/android/vector/hourglass/model/SandDropView;->init()V

    .line 17
    return-void
.end method

.method private getPaint(I)Landroid/graphics/Paint;
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 57
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 58
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 60
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 62
    return-object v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/vector/hourglass/model/SandDropView;->setAlpha(F)V

    .line 21
    return-void
.end method

.method private makePathInfo()Lcom/sec/android/vector/hourglass/type/PathInfo;
    .locals 6

    .prologue
    const/high16 v5, 0x42040000    # 33.0f

    const/high16 v4, 0x418c0000    # 17.5f

    const/16 v3, 0xfe

    const/high16 v2, 0x41840000    # 16.5f

    const v1, 0x417e6666    # 15.9f

    .line 44
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 46
    .local v0, "path":Landroid/graphics/Path;
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 47
    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 48
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 49
    invoke-virtual {v0, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 50
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 52
    new-instance v1, Lcom/sec/android/vector/hourglass/type/PathInfo;

    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/vector/hourglass/model/SandDropView;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/sec/android/vector/hourglass/type/PathInfo;-><init>(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-object v1
.end method


# virtual methods
.method protected createPathInfos()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/vector/hourglass/type/PathInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 27
    .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/vector/hourglass/type/PathInfo;>;"
    invoke-direct {p0}, Lcom/sec/android/vector/hourglass/model/SandDropView;->makePathInfo()Lcom/sec/android/vector/hourglass/type/PathInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    return-object v0
.end method

.method protected modifyPathInfo(Lcom/sec/android/vector/hourglass/type/PathInfo;F)V
    .locals 0
    .param p1, "info"    # Lcom/sec/android/vector/hourglass/type/PathInfo;
    .param p2, "ratio"    # F

    .prologue
    .line 35
    return-void
.end method

.method public render(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/android/vector/hourglass/model/SandDropView;->setAlpha(F)V

    .line 40
    return-void
.end method
