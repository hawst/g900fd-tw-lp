.class public Lcom/sec/android/vector/hourglass/control/Controller;
.super Ljava/lang/Object;
.source "Controller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;,
        Lcom/sec/android/vector/hourglass/control/Controller$RotateAnimateUpdater;
    }
.end annotation


# instance fields
.field private final DROP_DURATION:I

.field private final QUADEASEOUT:Landroid/animation/TimeInterpolator;

.field private final QUINTEASEIN:Landroid/animation/TimeInterpolator;

.field private final ROTATE_DELAY:I

.field private final ROTATE_DURATION:I

.field private mDropAnimator:Landroid/animation/ValueAnimator;

.field private final mDropSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

.field private final mDropUpdateListener:Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;

.field private final mLayout:Landroid/view/View;

.field private final mLowerSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

.field private mRotateAnimator:Landroid/animation/ObjectAnimator;

.field private final mRotateUpdateListener:Lcom/sec/android/vector/hourglass/control/Controller$RotateAnimateUpdater;

.field private final mUpperSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

.field private mbStopped:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/sec/android/vector/hourglass/model/AbstractView;Lcom/sec/android/vector/hourglass/model/AbstractView;Lcom/sec/android/vector/hourglass/model/AbstractView;)V
    .locals 3
    .param p1, "layout"    # Landroid/view/View;
    .param p2, "upperSandView"    # Lcom/sec/android/vector/hourglass/model/AbstractView;
    .param p3, "lowerSandView"    # Lcom/sec/android/vector/hourglass/model/AbstractView;
    .param p4, "dropSandView"    # Lcom/sec/android/vector/hourglass/model/AbstractView;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x1f4

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->DROP_DURATION:I

    .line 18
    iput v1, p0, Lcom/sec/android/vector/hourglass/control/Controller;->ROTATE_DURATION:I

    .line 19
    iput v1, p0, Lcom/sec/android/vector/hourglass/control/Controller;->ROTATE_DELAY:I

    .line 21
    new-instance v0, Lcom/sec/android/vector/hourglass/easing/QuadEaseOut;

    invoke-direct {v0}, Lcom/sec/android/vector/hourglass/easing/QuadEaseOut;-><init>()V

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->QUADEASEOUT:Landroid/animation/TimeInterpolator;

    .line 22
    new-instance v0, Lcom/sec/android/vector/hourglass/easing/QuintEaseIn;

    invoke-direct {v0}, Lcom/sec/android/vector/hourglass/easing/QuintEaseIn;-><init>()V

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->QUINTEASEIN:Landroid/animation/TimeInterpolator;

    .line 39
    iput-object p1, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mLayout:Landroid/view/View;

    .line 40
    iput-object p2, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    .line 41
    iput-object p3, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    .line 42
    iput-object p4, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    .line 44
    new-instance v0, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;-><init>(Lcom/sec/android/vector/hourglass/control/Controller;Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;)V

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;

    .line 45
    new-instance v0, Lcom/sec/android/vector/hourglass/control/Controller$RotateAnimateUpdater;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/vector/hourglass/control/Controller$RotateAnimateUpdater;-><init>(Lcom/sec/android/vector/hourglass/control/Controller;Lcom/sec/android/vector/hourglass/control/Controller$RotateAnimateUpdater;)V

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateUpdateListener:Lcom/sec/android/vector/hourglass/control/Controller$RotateAnimateUpdater;

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/vector/hourglass/control/Controller;)Lcom/sec/android/vector/hourglass/model/AbstractView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/vector/hourglass/control/Controller;)Lcom/sec/android/vector/hourglass/model/AbstractView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/vector/hourglass/control/Controller;)Lcom/sec/android/vector/hourglass/model/AbstractView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/vector/hourglass/control/Controller;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mbStopped:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/vector/hourglass/control/Controller;II)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/sec/android/vector/hourglass/control/Controller;->playRotate(II)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/vector/hourglass/control/Controller;II)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/sec/android/vector/hourglass/control/Controller;->playDrop(II)V

    return-void
.end method

.method private playDrop(II)V
    .locals 3
    .param p1, "duration"    # I
    .param p2, "delay"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 80
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 82
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    .line 86
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    .line 87
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropUpdateListener:Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/sec/android/vector/hourglass/control/Controller;->QUADEASEOUT:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 91
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mDropAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 94
    return-void

    .line 86
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private playRotate(II)V
    .locals 3
    .param p1, "duration"    # I
    .param p2, "delay"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 101
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllUpdateListeners()V

    .line 103
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mLayout:Landroid/view/View;

    const-string/jumbo v1, "rotation"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    .line 108
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateUpdateListener:Lcom/sec/android/vector/hourglass/control/Controller$RotateAnimateUpdater;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/sec/android/vector/hourglass/control/Controller;->QUADEASEOUT:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 111
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mRotateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 114
    return-void

    .line 107
    nop

    :array_0
    .array-data 4
        0x0
        0x43340000    # 180.0f
    .end array-data
.end method


# virtual methods
.method public play(I)V
    .locals 1
    .param p1, "delay"    # I

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mbStopped:Z

    .line 51
    const/16 v0, 0x3e8

    invoke-direct {p0, v0, p1}, Lcom/sec/android/vector/hourglass/control/Controller;->playDrop(II)V

    .line 52
    return-void
.end method

.method public recoverView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setRotation(F)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/vector/hourglass/model/AbstractView;->render(F)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    invoke-virtual {v0, v2}, Lcom/sec/android/vector/hourglass/model/AbstractView;->render(F)V

    .line 122
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/vector/hourglass/control/Controller;->mbStopped:Z

    .line 73
    return-void
.end method
