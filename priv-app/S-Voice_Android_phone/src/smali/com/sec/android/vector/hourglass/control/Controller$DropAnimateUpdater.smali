.class Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;
.super Ljava/lang/Object;
.source "Controller.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/vector/hourglass/control/Controller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DropAnimateUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/vector/hourglass/control/Controller;


# direct methods
.method private constructor <init>(Lcom/sec/android/vector/hourglass/control/Controller;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/vector/hourglass/control/Controller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/vector/hourglass/control/Controller;Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;-><init>(Lcom/sec/android/vector/hourglass/control/Controller;)V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 138
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/16 v1, 0x1f4

    .line 143
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/vector/hourglass/control/Controller;

    # getter for: Lcom/sec/android/vector/hourglass/control/Controller;->mbStopped:Z
    invoke-static {v0}, Lcom/sec/android/vector/hourglass/control/Controller;->access$3(Lcom/sec/android/vector/hourglass/control/Controller;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/vector/hourglass/control/Controller;

    # invokes: Lcom/sec/android/vector/hourglass/control/Controller;->playRotate(II)V
    invoke-static {v0, v1, v1}, Lcom/sec/android/vector/hourglass/control/Controller;->access$4(Lcom/sec/android/vector/hourglass/control/Controller;II)V

    .line 146
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 151
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 156
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 128
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 130
    .local v0, "ratio":F
    iget-object v1, p0, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/vector/hourglass/control/Controller;

    # getter for: Lcom/sec/android/vector/hourglass/control/Controller;->mUpperSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;
    invoke-static {v1}, Lcom/sec/android/vector/hourglass/control/Controller;->access$0(Lcom/sec/android/vector/hourglass/control/Controller;)Lcom/sec/android/vector/hourglass/model/AbstractView;

    move-result-object v1

    sub-float v2, v3, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/vector/hourglass/model/AbstractView;->render(F)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/vector/hourglass/control/Controller;

    # getter for: Lcom/sec/android/vector/hourglass/control/Controller;->mLowerSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;
    invoke-static {v1}, Lcom/sec/android/vector/hourglass/control/Controller;->access$1(Lcom/sec/android/vector/hourglass/control/Controller;)Lcom/sec/android/vector/hourglass/model/AbstractView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/vector/hourglass/model/AbstractView;->render(F)V

    .line 132
    iget-object v1, p0, Lcom/sec/android/vector/hourglass/control/Controller$DropAnimateUpdater;->this$0:Lcom/sec/android/vector/hourglass/control/Controller;

    # getter for: Lcom/sec/android/vector/hourglass/control/Controller;->mDropSandView:Lcom/sec/android/vector/hourglass/model/AbstractView;
    invoke-static {v1}, Lcom/sec/android/vector/hourglass/control/Controller;->access$2(Lcom/sec/android/vector/hourglass/control/Controller;)Lcom/sec/android/vector/hourglass/model/AbstractView;

    move-result-object v1

    sub-float v2, v3, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/vector/hourglass/model/AbstractView;->render(F)V

    .line 133
    return-void
.end method
