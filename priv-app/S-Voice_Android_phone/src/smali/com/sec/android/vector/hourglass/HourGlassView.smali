.class public Lcom/sec/android/vector/hourglass/HourGlassView;
.super Landroid/widget/RelativeLayout;
.source "HourGlassView.java"


# static fields
.field private static CONTENT_HEIGHT:F

.field private static CONTENT_WIDTH:F


# instance fields
.field private mController:Lcom/sec/android/vector/hourglass/control/Controller;

.field private mSandDropView:Lcom/sec/android/vector/hourglass/model/AbstractView;

.field private mSandLowerView:Lcom/sec/android/vector/hourglass/model/AbstractView;

.field private mSandUpperView:Lcom/sec/android/vector/hourglass/model/AbstractView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/high16 v0, 0x42080000    # 34.0f

    sput v0, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_WIDTH:F

    .line 16
    const/high16 v0, 0x421c0000    # 39.0f

    sput v0, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 26
    invoke-direct {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->init()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    invoke-direct {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->init()V

    .line 32
    return-void
.end method

.method private addDropSand()Lcom/sec/android/vector/hourglass/model/AbstractView;
    .locals 4

    .prologue
    .line 91
    new-instance v0, Lcom/sec/android/vector/hourglass/model/SandDropView;

    invoke-virtual {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_WIDTH:F

    sget v3, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/vector/hourglass/model/SandDropView;-><init>(Landroid/content/Context;FF)V

    .line 93
    .local v0, "view":Lcom/sec/android/vector/hourglass/model/SandDropView;
    invoke-virtual {p0, v0}, Lcom/sec/android/vector/hourglass/HourGlassView;->addView(Landroid/view/View;)V

    .line 95
    return-object v0
.end method

.method private addFrame()Lcom/sec/android/vector/hourglass/model/AbstractView;
    .locals 4

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/vector/hourglass/model/ClockFrameView;

    invoke-virtual {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_WIDTH:F

    sget v3, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/vector/hourglass/model/ClockFrameView;-><init>(Landroid/content/Context;FF)V

    .line 66
    .local v0, "view":Lcom/sec/android/vector/hourglass/model/ClockFrameView;
    invoke-virtual {p0, v0}, Lcom/sec/android/vector/hourglass/HourGlassView;->addView(Landroid/view/View;)V

    .line 68
    return-object v0
.end method

.method private addLowerSand()Lcom/sec/android/vector/hourglass/model/AbstractView;
    .locals 4

    .prologue
    .line 82
    new-instance v0, Lcom/sec/android/vector/hourglass/model/SandLowerView;

    invoke-virtual {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_WIDTH:F

    sget v3, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/vector/hourglass/model/SandLowerView;-><init>(Landroid/content/Context;FF)V

    .line 84
    .local v0, "view":Lcom/sec/android/vector/hourglass/model/SandLowerView;
    invoke-virtual {p0, v0}, Lcom/sec/android/vector/hourglass/HourGlassView;->addView(Landroid/view/View;)V

    .line 86
    return-object v0
.end method

.method private addUpperSand()Lcom/sec/android/vector/hourglass/model/AbstractView;
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lcom/sec/android/vector/hourglass/model/SandUpperView;

    invoke-virtual {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_WIDTH:F

    sget v3, Lcom/sec/android/vector/hourglass/HourGlassView;->CONTENT_HEIGHT:F

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/vector/hourglass/model/SandUpperView;-><init>(Landroid/content/Context;FF)V

    .line 75
    .local v0, "view":Lcom/sec/android/vector/hourglass/model/SandUpperView;
    invoke-virtual {p0, v0}, Lcom/sec/android/vector/hourglass/HourGlassView;->addView(Landroid/view/View;)V

    .line 77
    return-object v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->addFrame()Lcom/sec/android/vector/hourglass/model/AbstractView;

    .line 37
    invoke-direct {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->addUpperSand()Lcom/sec/android/vector/hourglass/model/AbstractView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mSandUpperView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->addLowerSand()Lcom/sec/android/vector/hourglass/model/AbstractView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mSandLowerView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    .line 39
    invoke-direct {p0}, Lcom/sec/android/vector/hourglass/HourGlassView;->addDropSand()Lcom/sec/android/vector/hourglass/model/AbstractView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mSandDropView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    .line 41
    return-void
.end method


# virtual methods
.method public play(I)V
    .locals 4
    .param p1, "startDelay"    # I

    .prologue
    .line 45
    new-instance v0, Lcom/sec/android/vector/hourglass/control/Controller;

    iget-object v1, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mSandUpperView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    iget-object v2, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mSandLowerView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    iget-object v3, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mSandDropView:Lcom/sec/android/vector/hourglass/model/AbstractView;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/vector/hourglass/control/Controller;-><init>(Landroid/view/View;Lcom/sec/android/vector/hourglass/model/AbstractView;Lcom/sec/android/vector/hourglass/model/AbstractView;Lcom/sec/android/vector/hourglass/model/AbstractView;)V

    iput-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mController:Lcom/sec/android/vector/hourglass/control/Controller;

    .line 46
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mController:Lcom/sec/android/vector/hourglass/control/Controller;

    invoke-virtual {v0, p1}, Lcom/sec/android/vector/hourglass/control/Controller;->play(I)V

    .line 47
    return-void
.end method

.method public recoverView()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mController:Lcom/sec/android/vector/hourglass/control/Controller;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mController:Lcom/sec/android/vector/hourglass/control/Controller;

    invoke-virtual {v0}, Lcom/sec/android/vector/hourglass/control/Controller;->recoverView()V

    .line 60
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mController:Lcom/sec/android/vector/hourglass/control/Controller;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/vector/hourglass/HourGlassView;->mController:Lcom/sec/android/vector/hourglass/control/Controller;

    invoke-virtual {v0}, Lcom/sec/android/vector/hourglass/control/Controller;->stop()V

    .line 54
    :cond_0
    return-void
.end method
