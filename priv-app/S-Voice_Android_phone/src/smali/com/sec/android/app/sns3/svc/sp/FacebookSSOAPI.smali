.class public Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;
.super Ljava/lang/Object;
.source "FacebookSSOAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
    }
.end annotation


# static fields
.field public static final FACEBOOK_MARKET_URL:Ljava/lang/String; = "market://details?id=com.facebook.katana"

.field public static final FACEBOOK_PACKAGE:Ljava/lang/String; = "com.facebook.katana"

.field public static final GOOGLE_ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field public static final SAMSUNG_FB_ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.facebook"

.field private static final TAG:Ljava/lang/String;

.field public static final TYPE_INTENT:I = 0x8

.field private static final loginFbAccountType:Ljava/lang/String; = "com.facebook.auth.login"

.field private static mFbForAuthtokenConnection:Landroid/content/ServiceConnection;

.field private static mSnsFacebookForAuthToken:Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

.field private static volatile ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

.field private static updateAfterBackToApp:Z

.field private static wasFacebookAccountLogged:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->TAG:Ljava/lang/String;

    .line 322
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mSnsFacebookForAuthToken:Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;)Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    .prologue
    .line 42
    sput-object p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mSnsFacebookForAuthToken:Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    return-object p0
.end method

.method public static backFromFacebookAppToLogin()Z
    .locals 1

    .prologue
    .line 349
    sget-boolean v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->updateAfterBackToApp:Z

    return v0
.end method

.method public static declared-synchronized bindFacebookService(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    const-class v1, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "bindFacebookService()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 258
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountCreated(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    const-string/jumbo v0, "facebook_token"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string/jumbo v0, "facebook_expires"

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 261
    const-string/jumbo v0, "facebook_account"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 262
    const-string/jumbo v0, "facebook_picture"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 263
    const-string/jumbo v0, "facebook_picture_url"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mFbForAuthtokenConnection:Landroid/content/ServiceConnection;

    if-nez v0, :cond_1

    .line 267
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$2;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mFbForAuthtokenConnection:Landroid/content/ServiceConnection;

    .line 279
    :cond_1
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mSnsFacebookForAuthToken:Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    if-nez v0, :cond_4

    if-eqz p0, :cond_4

    .line 280
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mFbForAuthtokenConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 287
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountCreated(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 288
    const-string/jumbo v0, "facebook_account"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 297
    :cond_2
    :goto_1
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountLogged(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->wasFacebookAccountLogged:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    :cond_3
    monitor-exit v1

    return-void

    .line 284
    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->getFbAccountInfo(Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 289
    :cond_5
    :try_start_2
    sget-boolean v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->updateAfterBackToApp:Z

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountLogged(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->wasFacebookAccountLogged:Z

    if-nez v0, :cond_2

    .line 295
    const/4 v0, 0x1

    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->startFacebookSSO(Landroid/content/Context;ZZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public static continueSocialUpdateAll(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 341
    if-eqz p0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->updateAfterBackToApp:Z

    if-eqz v0, :cond_0

    .line 342
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/midas/util/SocialUtils;->loginIntent(Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 345
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->init()V

    .line 346
    return-void
.end method

.method public static facebookSSO()Z
    .locals 5

    .prologue
    .line 68
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 69
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.sec.android.app.sns3"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getFbAccountInfo(Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;)V
    .locals 12
    .param p0, "callback"    # Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    .prologue
    .line 154
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 155
    .local v4, "context":Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->resetFacebookSocialFlowIfNotLoggedIn(Landroid/content/Context;)V

    .line 156
    sput-object p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    .line 157
    sget-object v8, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mSnsFacebookForAuthToken:Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    .line 158
    .local v8, "localFacebookForAuthToken":Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;
    if-eqz v8, :cond_2

    .line 161
    :try_start_0
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "getFbAccountInfo Start..."

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    invoke-interface {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;->getAuthTokenNExpires()Ljava/util/Map;

    move-result-object v7

    .line 163
    .local v7, "info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v9, "app_id"

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 164
    .local v1, "AppIdForSSO":Ljava/lang/String;
    const-string/jumbo v9, "secret_key"

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 165
    .local v3, "SecretKey":Ljava/lang/String;
    const-string/jumbo v9, "access_token"

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 166
    .local v0, "AccessToken":Ljava/lang/String;
    const-string/jumbo v9, "expires"

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 168
    .local v2, "Expires":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 169
    const-string/jumbo v9, "expires"

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    .line 170
    .local v6, "expire":Ljava/lang/Long;
    const-string/jumbo v9, "facebook_expires"

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 173
    .end local v6    # "expire":Ljava/lang/Long;
    :cond_0
    const-string/jumbo v10, "facebook_token"

    const-string/jumbo v9, "access_token"

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v10, v9}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "fbAccount iscreated  = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountCreated(Landroid/content/Context;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountCreated(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 177
    const-string/jumbo v9, "facebook_token"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string/jumbo v9, "facebook_expires"

    const-wide/16 v10, 0x0

    invoke-static {v9, v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 179
    const-string/jumbo v9, "facebook_account"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 180
    const-string/jumbo v9, "facebook_picture"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 181
    const-string/jumbo v9, "facebook_picture_url"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    if-eqz v9, :cond_1

    .line 184
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    invoke-interface {v9}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;->onChangeAccountInfo()V

    .line 188
    :cond_1
    new-instance v9, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1;

    invoke-direct {v9}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1;-><init>()V

    invoke-interface {v8, v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;->getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookCallbackMyAccountInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    .end local v0    # "AccessToken":Ljava/lang/String;
    .end local v1    # "AppIdForSSO":Ljava/lang/String;
    .end local v2    # "Expires":Ljava/lang/String;
    .end local v3    # "SecretKey":Ljava/lang/String;
    .end local v7    # "info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    :goto_0
    return-void

    .line 237
    :catch_0
    move-exception v5

    .line 239
    .local v5, "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getLoginToNetworkErrorMsg()Ljava/lang/String;
    .locals 4

    .prologue
    .line 326
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 327
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->core_social_login_to_facebook_msg:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 328
    .local v1, "str":Ljava/lang/String;
    return-object v1
.end method

.method public static getPackageVersionCode(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 78
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 79
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    .end local v0    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v1

    .line 80
    :catch_0
    move-exception v1

    .line 82
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static init()V
    .locals 0

    .prologue
    .line 332
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 333
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->resetAfterBackToAppUpdate()V

    .line 334
    return-void
.end method

.method public static isFacebookAccountCreated(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 106
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 107
    .local v1, "am":Landroid/accounts/AccountManager;
    if-eqz v1, :cond_0

    .line 108
    const-string/jumbo v3, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 109
    .local v0, "account":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-lez v3, :cond_0

    const/4 v2, 0x1

    .line 111
    .end local v0    # "account":[Landroid/accounts/Account;
    :cond_0
    return v2
.end method

.method private static isFacebookAccountLogged(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 302
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 303
    .local v1, "am":Landroid/accounts/AccountManager;
    if-eqz v1, :cond_0

    .line 304
    const-string/jumbo v3, "com.facebook.auth.login"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 305
    .local v0, "account":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-lez v3, :cond_0

    const/4 v2, 0x1

    .line 307
    .end local v0    # "account":[Landroid/accounts/Account;
    :cond_0
    return v2
.end method

.method public static isFacebookLoggedIn()Z
    .locals 2

    .prologue
    .line 63
    const-string/jumbo v0, "facebook_account"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static logoutFacebookSSO()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 129
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 130
    .local v4, "context":Landroid/content/Context;
    if-eqz v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 135
    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 136
    .local v2, "am":Landroid/accounts/AccountManager;
    if-eqz v2, :cond_1

    .line 137
    const-string/jumbo v7, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v2, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 138
    .local v1, "accountList":[Landroid/accounts/Account;
    move-object v3, v1

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v6, v3

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v0, v3, v5

    .line 139
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {v2, v0, v8, v8}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 138
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 142
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    sget-object v7, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    if-eqz v7, :cond_1

    .line 143
    sget-object v7, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    invoke-interface {v7}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;->onChangeAccountInfo()V

    .line 147
    .end local v1    # "accountList":[Landroid/accounts/Account;
    .end local v2    # "am":Landroid/accounts/AccountManager;
    .end local v3    # "arr$":[Landroid/accounts/Account;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_1
    return-void
.end method

.method private static resetAfterBackToAppUpdate()V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->updateAfterBackToApp:Z

    .line 338
    return-void
.end method

.method private static resetFacebookSocialFlowIfNotLoggedIn(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 245
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getUpdateType()Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->FACEBOOK_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    if-ne v0, v1, :cond_0

    .line 246
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 247
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountCreated(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->resetAfterBackToAppUpdate()V

    .line 249
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 252
    :cond_0
    return-void
.end method

.method public static startFacebookSSO(Landroid/content/Context;Z)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flowContinue"    # Z

    .prologue
    .line 86
    invoke-static {p0, p1, p1}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->startFacebookSSO(Landroid/content/Context;ZZ)V

    .line 87
    return-void
.end method

.method public static startFacebookSSO(Landroid/content/Context;ZZ)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flowContinue"    # Z
    .param p2, "newTask"    # Z

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 91
    .local v0, "fbIntent":Landroid/content/Intent;
    const-string/jumbo v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    if-eqz p2, :cond_0

    .line 93
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 96
    :cond_0
    const-string/jumbo v1, "account_types"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "com.sec.android.app.sns3.facebook"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 100
    sput-boolean p1, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->updateAfterBackToApp:Z

    .line 101
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountLogged(Landroid/content/Context;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->wasFacebookAccountLogged:Z

    .line 103
    return-void
.end method

.method public static declared-synchronized unbindFacebookService(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 311
    const-class v1, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "unbindFacebookService()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mFbForAuthtokenConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 313
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mFbForAuthtokenConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 314
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mFbForAuthtokenConnection:Landroid/content/ServiceConnection;

    .line 315
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mSnsFacebookForAuthToken:Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    .line 319
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    :cond_1
    monitor-exit v1

    return-void

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
