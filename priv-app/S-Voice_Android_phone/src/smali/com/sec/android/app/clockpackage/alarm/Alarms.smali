.class public Lcom/sec/android/app/clockpackage/alarm/Alarms;
.super Ljava/lang/Object;
.source "Alarms.java"


# static fields
.field public static final PREF_START_DAY_OF_WEEK:Ljava/lang/String; = "preferences_week_start_day"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getStartDayOfWeek(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const-string/jumbo v2, "AlarmClock"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 33
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v2, "preferences_week_start_day"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 35
    .local v1, "startDay":I
    return v1
.end method
