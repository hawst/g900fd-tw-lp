.class public Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;
.super Ljava/lang/Object;
.source "AlarmProvider.java"


# static fields
.field static final AUTHORITY:Ljava/lang/String; = "com.samsung.sec.android.clockpackage"

.field public static final COLUMN_ALARMACTIVE:I = 0x1

.field public static final COLUMN_ALARMALERTTIME:I = 0x3

.field public static final COLUMN_ALARMNAME:I = 0x14

.field public static final COLUMN_ALARMSOUNDTONE:I = 0x10

.field public static final COLUMN_ALARMSOUNDTYPE:I = 0xf

.field public static final COLUMN_ALARMSOUNDURI:I = 0x13

.field public static final COLUMN_ALARMSOUNDVOLUME:I = 0x11

.field public static final COLUMN_ALARMTIME:I = 0x4

.field public static final COLUMN_CREATETIME:I = 0x2

.field public static final COLUMN_DAILYBRIEF:I = 0xb

.field public static final COLUMN_ID:I = 0x0

.field public static final COLUMN_NOTIFICATIONTYPE:I = 0x6

.field public static final COLUMN_REPEATTYPE:I = 0x5

.field public static final COLUMN_SNOOZEACTIVE:I = 0x7

.field public static final COLUMN_SNOOZEDONECOUNT:I = 0xa

.field public static final COLUMN_SNOOZEDURATION:I = 0x8

.field public static final COLUMN_SNOOZEREPEAT:I = 0x9

.field public static final COLUMN_SUBDUEACTIVE:I = 0xc

.field public static final COLUMN_SUBDUEDURATION:I = 0xd

.field public static final COLUMN_SUBDUETONE:I = 0xe

.field public static final COLUMN_SUBDUEURI:I = 0x12

.field public static final CONTENT_URI:Landroid/net/Uri;

.field static final DATABASE_NAME:Ljava/lang/String; = "alarm.db"

.field static final DATABASE_VERSION:I = 0x1

.field private static final DEBUG:Z = false

.field private static final DM12:Ljava/lang/String; = "E h:mm aa"

.field private static final DM24:Ljava/lang/String; = "E k:mm"

.field public static final PARAMETER_NOTIFY:Ljava/lang/String; = "notify"

.field static final TABLE_NAME:Ljava/lang/String; = "alarm"

.field private static final TAG:Ljava/lang/String; = "AlarmProvider"

.field public static final TAG_ACTIVATION:Ljava/lang/String; = "active"

.field public static final TAG_ALARMALERTTIME:Ljava/lang/String; = "alerttime"

.field public static final TAG_ALARMNAME:Ljava/lang/String; = "name"

.field public static final TAG_ALARMSOUNDTONE:Ljava/lang/String; = "alarmtone"

.field public static final TAG_ALARMSOUNDTYPE:Ljava/lang/String; = "alarmsound"

.field public static final TAG_ALARMSOUNDURI:Ljava/lang/String; = "alarmuri"

.field public static final TAG_ALARMTIME:Ljava/lang/String; = "alarmtime"

.field public static final TAG_ALARMVOLUME:Ljava/lang/String; = "volume"

.field public static final TAG_CREATETIME:Ljava/lang/String; = "createtime"

.field public static final TAG_DAILYBRIEFING:Ljava/lang/String; = "dailybrief"

.field public static final TAG_ID:Ljava/lang/String; = "_id"

.field public static final TAG_NOTIFICATIONTYPE:Ljava/lang/String; = "notitype"

.field public static final TAG_REPEATTYPE:Ljava/lang/String; = "repeattype"

.field public static final TAG_SNOOZEACTIVATION:Ljava/lang/String; = "snzactive"

.field public static final TAG_SNOOZEDONECOUNT:Ljava/lang/String; = "snzcount"

.field public static final TAG_SNOOZEDURATION:Ljava/lang/String; = "snzduration"

.field public static final TAG_SNOOZEREPEAT:Ljava/lang/String; = "snzrepeat"

.field public static final TAG_SUBDUEACTIVATION:Ljava/lang/String; = "sbdactive"

.field public static final TAG_SUBDUEDURATION:Ljava/lang/String; = "sbdduration"

.field public static final TAG_SUBDUETONE:Ljava/lang/String; = "sbdtone"

.field public static final TAG_SUBDUEURI:Ljava/lang/String; = "sbduri"

.field private static mIsTablet:Z

.field private static mPendingIntent:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-string/jumbo v0, "content://com.samsung.sec.android.clockpackage/alarm"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 93
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mIsTablet:Z

    .line 20
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static enableNextAlert(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v8, 0x10000000

    const/4 v9, 0x0

    .line 170
    invoke-static {p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->getNextAlarm(Landroid/content/Context;)Lcom/sec/android/app/clockpackage/alarm/AlarmItem;

    move-result-object v3

    .line 174
    .local v3, "item":Lcom/sec/android/app/clockpackage/alarm/AlarmItem;
    if-eqz v3, :cond_2

    .line 176
    const-string/jumbo v6, "alarm"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 175
    check-cast v0, Landroid/app/AlarmManager;

    .line 177
    .local v0, "am":Landroid/app/AlarmManager;
    sget-object v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v6, :cond_0

    .line 178
    sget-object v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 182
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v6, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 183
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 184
    .local v4, "out":Landroid/os/Parcel;
    invoke-virtual {v3, v4}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->writeToParcel(Landroid/os/Parcel;)V

    .line 185
    invoke-virtual {v4, v9}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 186
    const-string/jumbo v6, "com.samsung.sec.android.clockpackage.alarm.ALARM_DATA"

    invoke-virtual {v4}, Landroid/os/Parcel;->marshall()[B

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 187
    invoke-static {p0, v9, v2, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    sput-object v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    .line 189
    iget-wide v6, v3, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    sget-object v8, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v9, v6, v7, v8}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 190
    const/4 v6, 0x1

    invoke-static {p0, v6}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->setStatusBarIcon(Landroid/content/Context;Z)V

    .line 191
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 192
    .local v1, "c":Ljava/util/Calendar;
    new-instance v6, Ljava/util/Date;

    iget-wide v7, v3, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 193
    invoke-static {p0, v1}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->formatDayAndTime(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    .line 194
    .local v5, "timeString":Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->saveNextAlarm(Landroid/content/Context;Ljava/lang/String;)V

    .line 195
    sget-boolean v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mIsTablet:Z

    if-nez v6, :cond_1

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string/jumbo v7, "CscFeature_Clock_EnableAutoPowerOnOffMenu"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 196
    iget-wide v6, v3, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/clockpackage/alarm/AutoPowerUp;->sendData(Landroid/content/Context;J)V

    .line 217
    .end local v1    # "c":Ljava/util/Calendar;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v4    # "out":Landroid/os/Parcel;
    .end local v5    # "timeString":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/clockpackage/alarm/Utilities;->updateIndicatorAlarm(Landroid/content/Context;)V

    .line 218
    return-void

    .line 199
    .end local v0    # "am":Landroid/app/AlarmManager;
    :cond_2
    const-string/jumbo v6, "alarm"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 198
    check-cast v0, Landroid/app/AlarmManager;

    .line 200
    .restart local v0    # "am":Landroid/app/AlarmManager;
    sget-object v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v6, :cond_3

    .line 201
    sget-object v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 211
    :goto_1
    const/4 v6, 0x0

    sput-object v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    .line 212
    invoke-static {p0, v9}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->setStatusBarIcon(Landroid/content/Context;Z)V

    .line 213
    const-string/jumbo v6, ""

    invoke-static {p0, v6}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->saveNextAlarm(Landroid/content/Context;Ljava/lang/String;)V

    .line 214
    sget-boolean v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mIsTablet:Z

    if-nez v6, :cond_1

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string/jumbo v7, "CscFeature_Clock_EnableAutoPowerOnOffMenu"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 215
    const-wide/16 v6, -0x1

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/clockpackage/alarm/AutoPowerUp;->sendData(Landroid/content/Context;J)V

    goto :goto_0

    .line 204
    :cond_3
    const-string/jumbo v6, "TAG"

    const-string/jumbo v7, "mPendingIntent == null am.cancel"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v6, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 206
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-static {p0, v9, v2, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    sput-object v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    .line 208
    sget-object v6, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method private static formatDayAndTime(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "c"    # Ljava/util/Calendar;

    .prologue
    .line 232
    invoke-static {p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->get24HourMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "E k:mm"

    .line 233
    .local v0, "format":Ljava/lang/String;
    :goto_0
    if-nez p1, :cond_1

    const-string/jumbo v1, ""

    :goto_1
    return-object v1

    .line 232
    .end local v0    # "format":Ljava/lang/String;
    :cond_0
    const-string/jumbo v0, "E h:mm aa"

    goto :goto_0

    .line 233
    .restart local v0    # "format":Ljava/lang/String;
    :cond_1
    invoke-static {v0, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_1
.end method

.method static get24HourMode(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static final getNextAlarm(Landroid/content/Context;)Lcom/sec/android/app/clockpackage/alarm/AlarmItem;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 97
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 98
    const-string/jumbo v3, "active > 0"

    .line 99
    const-string/jumbo v5, "alerttime ASC"

    move-object v4, v2

    .line 97
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 100
    .local v6, "c1":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 102
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    .line 103
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 104
    const/4 v6, 0x0

    .line 166
    :cond_0
    :goto_0
    return-object v2

    .line 113
    :cond_1
    const/4 v1, 0x3

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 114
    .local v13, "time":J
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 115
    const/4 v6, 0x0

    .line 116
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 117
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "alerttime = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 118
    const-string/jumbo v5, "createtime DESC"

    move-object v4, v2

    .line 116
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 119
    .local v7, "c2":Landroid/database/Cursor;
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 120
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 121
    .local v9, "itemCount":I
    new-array v10, v9, [Lcom/sec/android/app/clockpackage/alarm/AlarmItem;

    .line 122
    .local v10, "items":[Lcom/sec/android/app/clockpackage/alarm/AlarmItem;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-lt v8, v9, :cond_2

    .line 126
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 127
    const/4 v7, 0x0

    .line 128
    const/4 v11, -0x1

    .line 129
    .local v11, "next":I
    const/16 v12, 0x100

    .line 130
    .local v12, "nextNotiType":I
    add-int/lit8 v8, v9, -0x1

    :goto_2
    if-gez v8, :cond_3

    .line 163
    if-ltz v11, :cond_0

    .line 164
    aget-object v2, v10, v11

    goto :goto_0

    .line 123
    .end local v11    # "next":I
    .end local v12    # "nextNotiType":I
    :cond_2
    invoke-static {v7}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->createItemFromCursor(Landroid/database/Cursor;)Lcom/sec/android/app/clockpackage/alarm/AlarmItem;

    move-result-object v1

    aput-object v1, v10, v8

    .line 124
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    .line 122
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 131
    .restart local v11    # "next":I
    .restart local v12    # "nextNotiType":I
    :cond_3
    aget-object v1, v10, v8

    iget v1, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    if-lez v1, :cond_5

    .line 132
    aget-object v1, v10, v8

    iget v1, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    if-eqz v1, :cond_4

    .line 133
    aget-object v1, v10, v8

    iget v1, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    const/16 v3, 0x10

    if-ne v1, v3, :cond_6

    .line 134
    :cond_4
    const/4 v12, 0x0

    .line 135
    move v11, v8

    .line 130
    :cond_5
    :goto_3
    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    .line 136
    :cond_6
    aget-object v1, v10, v8

    iget v1, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    and-int/lit8 v1, v1, 0x1

    .line 137
    const/4 v3, 0x1

    .line 136
    if-ne v1, v3, :cond_7

    .line 138
    if-eqz v12, :cond_5

    .line 140
    const/16 v12, 0x100

    .line 141
    move v11, v8

    goto :goto_3

    .line 142
    :cond_7
    aget-object v1, v10, v8

    iget v1, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    and-int/lit16 v1, v1, 0x100

    .line 143
    const/16 v3, 0x100

    .line 142
    if-ne v1, v3, :cond_5

    .line 144
    aget-object v1, v10, v8

    iget v1, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    if-nez v1, :cond_8

    .line 145
    const/4 v12, 0x0

    .line 146
    move v11, v8

    goto :goto_3

    .line 148
    :cond_8
    if-eqz v12, :cond_5

    .line 150
    const/16 v12, 0x100

    .line 151
    move v11, v8

    goto :goto_3
.end method

.method static saveNextAlarm(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeString"    # Ljava/lang/String;

    .prologue
    .line 245
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 246
    const-string/jumbo v1, "next_alarm_formatted"

    .line 245
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 250
    return-void
.end method

.method private static setStatusBarIcon(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enabled"    # Z

    .prologue
    .line 253
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.ALARM_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 254
    .local v0, "alarmChanged":Landroid/content/Intent;
    const-string/jumbo v1, "alarmSet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 255
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 256
    return-void
.end method
