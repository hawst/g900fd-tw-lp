.class public Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;
.super Ljava/lang/Object;
.source "TwitterSSOAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;
    }
.end annotation


# static fields
.field public static final SAMSUNG_TW_ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.twitter"

.field private static final TAG:Ljava/lang/String;

.field public static final TYPE_INTENT:I = 0x4

.field private static lastSuccessfullUpdate:J

.field private static mSnsTwitterForAuthToken:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

.field private static mTwForAuthtokenConnection:Landroid/content/ServiceConnection;

.field private static volatile ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

.field private static updateAfterBackToApp:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->lastSuccessfullUpdate:J

    .line 43
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;

    .line 299
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mSnsTwitterForAuthToken:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 33
    sput-wide p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->lastSuccessfullUpdate:J

    return-wide p0
.end method

.method static synthetic access$200()Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;)Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    .prologue
    .line 33
    sput-object p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mSnsTwitterForAuthToken:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    return-object p0
.end method

.method public static backFromTwitterAppToLogin()Z
    .locals 1

    .prologue
    .line 325
    sget-boolean v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->updateAfterBackToApp:Z

    return v0
.end method

.method public static declared-synchronized bindTwitterService(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 250
    const-class v1, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "bindTwitterService()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterAccountCreated(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    const-string/jumbo v0, "twitter_picture"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 254
    const-string/jumbo v0, "twitter_user_secret"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string/jumbo v0, "twitter_user_token"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string/jumbo v0, "twitter_account"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 257
    const-string/jumbo v0, "twitter_picture_url"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mTwForAuthtokenConnection:Landroid/content/ServiceConnection;

    if-nez v0, :cond_1

    .line 261
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$2;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mTwForAuthtokenConnection:Landroid/content/ServiceConnection;

    .line 273
    :cond_1
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mSnsTwitterForAuthToken:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    if-nez v0, :cond_3

    if-eqz p0, :cond_3

    .line 274
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mTwForAuthtokenConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 283
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterAccountCreated(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    const-string/jumbo v0, "twitter_account"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    :cond_2
    monitor-exit v1

    return-void

    .line 280
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->getTwAccountInfo(Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static continueSocialUpdateAll(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 318
    if-eqz p0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->updateAfterBackToApp:Z

    if-eqz v0, :cond_0

    .line 319
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/midas/util/SocialUtils;->loginIntent(Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 320
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->resetAfterBackToAppUpdate()V

    .line 322
    :cond_0
    return-void
.end method

.method public static getLoginToNetworkErrorMsg()Ljava/lang/String;
    .locals 4

    .prologue
    .line 303
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 304
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->core_social_login_to_twitter_msg:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 305
    .local v1, "str":Ljava/lang/String;
    return-object v1
.end method

.method public static getPackageVersionCode(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 69
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 70
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .end local v0    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v1

    .line 71
    :catch_0
    move-exception v1

    .line 73
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static getTwAccountInfo(Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;)V
    .locals 14
    .param p0, "callback"    # Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    .prologue
    .line 144
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 145
    .local v6, "context":Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->resetTwitterSocialFlowIfNotLoggedIn(Landroid/content/Context;)V

    .line 146
    sput-object p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    .line 147
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mSnsTwitterForAuthToken:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    .line 148
    .local v9, "localTwitterForAuthToken":Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;
    if-eqz v9, :cond_0

    .line 151
    :try_start_0
    sget-object v10, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;

    const-string/jumbo v11, "getTwAccountInfo Start..."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-interface {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;->getAuthTokenInfo()Ljava/util/Map;

    move-result-object v8

    .line 153
    .local v8, "info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v10, "consumer_key"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 154
    .local v2, "ConsumerKey":Ljava/lang/String;
    const-string/jumbo v10, "consumer_secret"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 155
    .local v3, "ConsumerSecret":Ljava/lang/String;
    const-string/jumbo v10, "access_token"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 156
    .local v0, "AccessToken":Ljava/lang/String;
    const-string/jumbo v10, "access_token_secret"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 157
    .local v1, "AccessTokenSecret":Ljava/lang/String;
    const-string/jumbo v10, "user_id"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 158
    .local v5, "UserId":Ljava/lang/String;
    const-string/jumbo v10, "screen_name"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 166
    .local v4, "ScreenName":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string/jumbo v10, "twitter_user_token"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sget-wide v12, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->lastSuccessfullUpdate:J

    sub-long/2addr v10, v12

    const-wide/32 v12, 0x493e0

    cmp-long v10, v10, v12

    if-gez v10, :cond_1

    .line 168
    sget-object v10, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "twAccount iscreated  = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterAccountCreated(Landroid/content/Context;)Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    .end local v0    # "AccessToken":Ljava/lang/String;
    .end local v1    # "AccessTokenSecret":Ljava/lang/String;
    .end local v2    # "ConsumerKey":Ljava/lang/String;
    .end local v3    # "ConsumerSecret":Ljava/lang/String;
    .end local v4    # "ScreenName":Ljava/lang/String;
    .end local v5    # "UserId":Ljava/lang/String;
    .end local v8    # "info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 172
    .restart local v0    # "AccessToken":Ljava/lang/String;
    .restart local v1    # "AccessTokenSecret":Ljava/lang/String;
    .restart local v2    # "ConsumerKey":Ljava/lang/String;
    .restart local v3    # "ConsumerSecret":Ljava/lang/String;
    .restart local v4    # "ScreenName":Ljava/lang/String;
    .restart local v5    # "UserId":Ljava/lang/String;
    .restart local v8    # "info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    const-string/jumbo v10, "twitter_consumer_key"

    invoke-static {v10, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string/jumbo v10, "twitter_consumer_secret"

    invoke-static {v10, v3}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string/jumbo v10, "twitter_user_token"

    invoke-static {v10, v0}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string/jumbo v10, "twitter_user_secret"

    invoke-static {v10, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string/jumbo v10, "twitter_username"

    invoke-static {v10, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    sget-object v10, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "twAccount iscreated  = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterAccountCreated(Landroid/content/Context;)Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterAccountCreated(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 183
    const-string/jumbo v10, "twitter_account"

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 186
    :cond_2
    new-instance v10, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1;

    invoke-direct {v10}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1;-><init>()V

    invoke-interface {v9, v10}, Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;->getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterCallbackMyAccountInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 230
    .end local v0    # "AccessToken":Ljava/lang/String;
    .end local v1    # "AccessTokenSecret":Ljava/lang/String;
    .end local v2    # "ConsumerKey":Ljava/lang/String;
    .end local v3    # "ConsumerSecret":Ljava/lang/String;
    .end local v4    # "ScreenName":Ljava/lang/String;
    .end local v5    # "UserId":Ljava/lang/String;
    .end local v8    # "info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v7

    .line 232
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static init()V
    .locals 0

    .prologue
    .line 309
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 310
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->resetAfterBackToAppUpdate()V

    .line 311
    return-void
.end method

.method public static isTwitterAccountCreated(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 97
    .local v1, "am":Landroid/accounts/AccountManager;
    if-eqz v1, :cond_0

    .line 98
    const-string/jumbo v3, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 99
    .local v0, "account":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-lez v3, :cond_0

    const/4 v2, 0x1

    .line 101
    .end local v0    # "account":[Landroid/accounts/Account;
    :cond_0
    return v2
.end method

.method public static isTwitterLoggedIn()Z
    .locals 2

    .prologue
    .line 50
    const-string/jumbo v0, "twitter_account"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static logoutTwitterSSO()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 107
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 108
    .local v4, "context":Landroid/content/Context;
    if-eqz v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 111
    const-string/jumbo v7, "twitter_picture"

    invoke-static {v7, v9}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 113
    const-string/jumbo v7, "twitter_consumer_key"

    const-string/jumbo v8, "AGv8Ps3AlFKrf2C1YoFkQ"

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string/jumbo v7, "twitter_consumer_secret"

    const-string/jumbo v8, "qeX5TCXa9HPDlpNmhPACOT7sUerHPmD91Oq9nYuw6Q"

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string/jumbo v7, "twitter_user_secret"

    invoke-static {v7, v9}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string/jumbo v7, "twitter_user_token"

    invoke-static {v7, v9}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string/jumbo v7, "twitter_username"

    invoke-static {v7, v9}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string/jumbo v7, "twitter_picture_url"

    invoke-static {v7, v9}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string/jumbo v7, "twitter_account"

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 120
    const-string/jumbo v7, "twitter_account_name"

    invoke-static {v7, v9}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 126
    .local v2, "am":Landroid/accounts/AccountManager;
    if-eqz v2, :cond_1

    .line 127
    const-string/jumbo v7, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v2, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 128
    .local v1, "accountList":[Landroid/accounts/Account;
    move-object v3, v1

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v6, v3

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v0, v3, v5

    .line 129
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {v2, v0, v9, v9}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 128
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 132
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    sget-object v7, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    if-eqz v7, :cond_1

    .line 133
    sget-object v7, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    invoke-interface {v7}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;->onChangeAccountInfo()V

    .line 137
    .end local v1    # "accountList":[Landroid/accounts/Account;
    .end local v2    # "am":Landroid/accounts/AccountManager;
    .end local v3    # "arr$":[Landroid/accounts/Account;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_1
    return-void
.end method

.method private static resetAfterBackToAppUpdate()V
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->updateAfterBackToApp:Z

    .line 315
    return-void
.end method

.method private static resetTwitterSocialFlowIfNotLoggedIn(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 239
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getUpdateType()Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->TWITTER_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    if-ne v0, v1, :cond_0

    .line 240
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 241
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterAccountCreated(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->resetAfterBackToAppUpdate()V

    .line 243
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 246
    :cond_0
    return-void
.end method

.method public static startTwitterSSO(Landroid/content/Context;Z)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flowContinue"    # Z

    .prologue
    .line 77
    invoke-static {p0, p1, p1}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->startTwitterSSO(Landroid/content/Context;ZZ)V

    .line 78
    return-void
.end method

.method public static startTwitterSSO(Landroid/content/Context;ZZ)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flowContinue"    # Z
    .param p2, "newTask"    # Z

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 82
    .local v0, "twIntent":Landroid/content/Intent;
    const-string/jumbo v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    if-eqz p2, :cond_0

    .line 84
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 87
    :cond_0
    const-string/jumbo v1, "account_types"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "com.sec.android.app.sns3.twitter"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 91
    sput-boolean p1, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->updateAfterBackToApp:Z

    .line 93
    return-void
.end method

.method public static twitterSSO()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 55
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 56
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string/jumbo v5, "com.sec.android.app.sns3"

    const/16 v6, 0x80

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 58
    const/4 v2, 0x0

    .line 59
    .local v2, "version":I
    const-string/jumbo v4, "com.sec.android.app.sns3"

    invoke-static {v0, v4}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->getPackageVersionCode(Landroid/content/Context;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 60
    const/16 v4, 0x15

    if-ge v2, v4, :cond_0

    .line 64
    .end local v2    # "version":I
    :goto_0
    return v3

    .line 61
    :catch_0
    move-exception v1

    .line 62
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0

    .line 64
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "version":I
    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static declared-synchronized unbindTwitterService(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 290
    const-class v1, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "unbindTwitterService()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mTwForAuthtokenConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 292
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mTwForAuthtokenConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 293
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mTwForAuthtokenConnection:Landroid/content/ServiceConnection;

    .line 294
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mSnsTwitterForAuthToken:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    .line 296
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    :cond_1
    monitor-exit v1

    return-void

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
