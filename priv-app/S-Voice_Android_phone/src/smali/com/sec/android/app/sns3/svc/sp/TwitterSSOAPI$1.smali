.class final Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterCallbackMyAccountInfo$Stub;
.source "TwitterSSOAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->getTwAccountInfo(Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterCallbackMyAccountInfo$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;)V
    .locals 3
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "account"    # Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 192
    if-nez p6, :cond_0

    .line 193
    # getter for: Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "reqID  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bSuccess="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " httpStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " errorCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;

    invoke-direct {v1, p0, p6}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 228
    return-void
.end method
